package mx.gob.segalmex.common.core.util;

import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.NonUniqueResultException;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;

@Slf4j
public class QueryBuilder {

    private static final String EQ = "=";

    private static final String NOT_EQ = "!=";

    private static final String IN = "IN";

    private static final String NOT_IN = "NOT IN";

    private final StringBuilder sb;

    private final Map<String, Object> params;

    private final AtomicBoolean first;

    public QueryBuilder(String s) {
        sb = new StringBuilder(StringUtils.stripToNull(s));
        sb.append(" ");
        params = new HashMap<>();
        first = new AtomicBoolean(true);
    }

    public void andEq(String condition, String name, Object value) {
        if (Objects.nonNull(value)) {
            addWhereAnd();
            addCondition(condition, EQ, name, value);
        }
    }

    public void andNotEq(String condition, String name, Object value) {
        if (Objects.nonNull(value)) {
            addWhereAnd();
            addCondition(condition, NOT_EQ, name, value);
        }
    }

    public void andIn(String condition, String name, Collection value) {
        if (Objects.nonNull(value)) {
            addWhereAnd();
            addCondition(condition, IN, name, value);
        }
    }

    public void andNotIn(String condition, String name, Collection value) {
        if (Objects.nonNull(value)) {
            addWhereAnd();
            addCondition(condition, NOT_IN, name, value);
        }
    }

    public <T> List<T> getList(EntityManager em, Class<T> clazz) {
        TypedQuery<T> q = em.createQuery(sb.toString(), clazz);
        params.entrySet().forEach(e -> {
            q.setParameter(e.getKey(), e.getValue());
        });

        long start = System.currentTimeMillis();
        List<T> results = q.getResultList();
        LOGGER.info("Executing {} in {}ms", sb, System.currentTimeMillis() - start);
        return results;
    }

    public static <T> T getUniqueResult(List<T> results) {
        if (results.isEmpty()) {
            throw new NoResultException("Can't find the TOpsRun with the specified parameters");
        }
        if (results.size() > 1) {
            throw new NonUniqueResultException("Incorrect result size, should be 1.");
        }
        return results.get(0);
    }

    private void addWhereAnd() {
        sb.append(first.getAndSet(false) ? "WHERE " : "AND ");
    }

    private void addCondition(String condition, String op, String name, Object value) {
        sb.append(condition);
        sb.append(" ");
        sb.append(op);
        sb.append(" :");
        sb.append(name);
        sb.append(" ");
        params.put(name, value);
    }

}