/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package mx.gob.segalmex.common.core.util;

import java.util.Map;
import javax.persistence.Query;
import org.apache.commons.lang3.StringUtils;

/**
 * @author ismael
 */
public class QueryUtils {

    protected QueryUtils() {
    }

    public static Query setParametros(Query q, Map<String, Object> parametros) {
        for (Map.Entry<String, Object> e : parametros.entrySet()) {
            q.setParameter(e.getKey(), e.getValue());
        }
        return q;
    }

    public static boolean agregaWhereAnd(boolean first, StringBuilder sb) {
        sb.append(first ? " WHERE " : " AND ");
        // Siempre regresa false, dado que ya no es la primera condición
        return false;
    }

    public static void and(StringBuilder sb, String propiedad, String nombre, Object valor, Map<String, Object> params) {
        sb.append(StringUtils.trimToEmpty(propiedad));
        sb.append(" = :");
        sb.append(StringUtils.trimToEmpty(nombre));
        sb.append(" ");
        params.put(nombre, valor);
    }

}
