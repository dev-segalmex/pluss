/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.time.DateUtils;

/**
 *
 * @author ismael
 */
@Slf4j
public class SegalmexDateUtils {

    public static String format(Calendar calendar) {
        return format(calendar, "dd/MM/yyyy");
    }

    public static String format(Calendar calendar, String formato) {
        Objects.requireNonNull(calendar, "El calendar no puede ser nulo.");
        return new SimpleDateFormat(formato).format(calendar.getTime());
    }

    /**
     * Convierte una cadena que representa una fecha y en un formato dado a un
     * <code>Calendar</code>.
     *
     * @param date la cadena con la representación de la fecha.
     * @param formato la representación del formato de la fecha.
     * @return un <code>Calendar</code> que corresponde a la representación de
     * la fecha en el formato dado.
     */
    public static Calendar parseCalendar(String date, String formato) {
        Calendar calendar = null;
        try {
            Date toDate = DateUtils.parseDate(date, new String[]{formato});

            if (toDate != null) {
                calendar = Calendar.getInstance();
                calendar.setTime(toDate);
            }
        } catch (ParseException ouch) {
            LOGGER.error("No se pudo obtener la fecha de: " + date, ouch);
        }

        return calendar;
    }

    /**
     * A partir de los Calendar pasado y futuro calcula los años transcurridos
     * entre las 2 fechas.
     *
     * @param pasado El año de inicio para el calculo.
     * @param futuro El año de fin para el calculo.
     * @return La cantidad de años transcurridos entre las 2 fechas.
     */
    public static int aniosTranscurridos(Calendar pasado, Calendar futuro) {
        int edad = futuro.get(Calendar.YEAR) - pasado.get(Calendar.YEAR);
        if (futuro.get(Calendar.MONTH) < pasado.get(Calendar.MONTH)) {
            edad--;
        }
        if (futuro.get(Calendar.MONTH) == pasado.get(Calendar.MONTH)) {
            if (futuro.get(Calendar.DATE) < pasado.get(Calendar.DATE)) {
                edad--;
            }
        }
        return edad;
    }

}
