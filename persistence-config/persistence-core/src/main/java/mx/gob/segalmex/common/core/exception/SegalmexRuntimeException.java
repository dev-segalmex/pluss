/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.exception;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author ismael
 */
public class SegalmexRuntimeException extends RuntimeException {

    /**
     * La lista con los motivos de la excepcion.
     */
    private List<String> motivos;

    public List<String> getMotivos() {
        return motivos;
    }

    public void setMotivos(List<String> motivos) {
        this.motivos = motivos;
    }

    /**
     * Constructor que crea una excepción dada una lista de motivos y un mensaje.
     *
     * @param mensaje el mensaje de la excepción.
     * @param motivos los motivos por los cuales ocurrió la excepción.
     */
    public SegalmexRuntimeException(String mensaje, List<String> motivos) {
        super(mensaje);
        this.motivos = motivos;
    }

    /**
     * Constructor que crea una excepción con un mensaje y un único motivo.
     *
     * @param mensaje el mensaje de la excepción.
     * @param motivo los motivos por los cuales ocurrió la excepción.
     */
    public SegalmexRuntimeException(String mensaje, String motivo) {
        super(mensaje);
        motivos = new ArrayList<>();
        motivos.add(motivo);
    }
}
