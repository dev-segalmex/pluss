/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.persistencia;

import java.util.Calendar;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ismael
 */
@Repository("registroEntidad")
public class RegistroEntidad {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    public void guarda(AbstractEntidad entidad) {
        if (entidad.getId() != null) {
            throw new IllegalArgumentException("La entidad ya se encuentra persistida.");
        }
        entidad.setFechaCreacion(DateUtils.truncate(Calendar.getInstance(), Calendar.SECOND));
        entidad.setFechaActualizacion(DateUtils.truncate(Calendar.getInstance(), Calendar.SECOND));
        entityManager.persist(entidad);
    }

    @Transactional
    public void actualiza(AbstractEntidad entidad) {
        if (entidad.getId() == null) {
            throw new IllegalArgumentException("La entidad no se encuentra persistida.");
        }
        entidad.setFechaActualizacion(DateUtils.truncate(Calendar.getInstance(), Calendar.SECOND));
        entityManager.merge(entidad);
    }

    @Transactional
    public void guardaActualiza(AbstractEntidad entidad) {
        if (Objects.isNull(entidad.getId())) {
            guarda(entidad);
        } else {
            actualiza(entidad);
        }
    }

    public void flush() {
        entityManager.flush();
    }

    public void clear() {
        entityManager.clear();
    }

}
