package mx.gob.segalmex.pluss.cultivos;

import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorPreRegistroProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricolaEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.modelo.productor.PreRegistroProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.List;

/**
 *
 * @author oscar
 */
@SpringBootApplication
@Slf4j
public class PreRegistrosActualizadorEstadoApplication implements CommandLineRunner {

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;
    @Autowired
    private BuscadorPreRegistroProductor buscadorPreRegistroProductor;
    @Autowired
    private PreRegistrosActualizadorEstadoHelper actualizadorEstadoHelper;

    public static void main(String[] args) {
        SpringApplication.run(PreRegistrosActualizadorEstadoApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        log.info("Ejecutando PreRegistrosActualizadorEstadoApplication...");

        for (CultivoEnum c : CultivoEnum.values()) {
            for (CicloAgricolaEnum ca : CicloAgricolaEnum.values()) {
                CicloAgricola ciclo = buscadorCatalogo.buscaElemento(CicloAgricola.class, ca.getClave());
                Cultivo cultivo = buscadorCatalogo.buscaElemento(Cultivo.class, c.getClave());
                ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
                parametros.setCiclo(ciclo);
                parametros.setCultivo(cultivo);
                parametros.setIncluirEstadosNulos(true);
                parametros.setFechaCancelacionNull(false);
                parametros.setLimite(1000);
                boolean buscar = true;
                while (buscar) {
                    List<PreRegistroProductor> pres = buscadorPreRegistroProductor.busca(parametros);
                    log.info("Pre registros de {} {} por procesar: {}", c.getNombre(), ciclo.getNombre(), pres.size());
                    actualizadorEstadoHelper.asignaEstado(pres);
                    buscar = !pres.isEmpty();
                }
            }
        }

    }

}
