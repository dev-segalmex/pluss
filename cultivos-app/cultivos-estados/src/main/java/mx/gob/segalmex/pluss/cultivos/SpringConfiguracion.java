package mx.gob.segalmex.pluss.cultivos;

import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 *
 * @author oscar
 */
@ComponentScan({"mx.gob.segalmex.common.core.catalogos.busqueda",
"mx.gob.segalmex.granos.core.productor.busqueda",
"mx.gob.segalmex.common.core.persistencia"})
@EntityScan("mx.gob.segalmex")
@Configuration
public class SpringConfiguracion {

}
