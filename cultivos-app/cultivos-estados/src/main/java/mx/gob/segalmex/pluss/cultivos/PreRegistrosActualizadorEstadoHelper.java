package mx.gob.segalmex.pluss.cultivos;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.granos.core.productor.EstadoProductorHelper;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorPreRegistroProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PreRegistroProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author oscar
 */
@Component
@Slf4j
public class PreRegistrosActualizadorEstadoHelper {

    @Autowired
    private RegistroEntidad registroEntidad;
    @Autowired
    private BuscadorPreRegistroProductor buscadorPreRegistroProductor;

    @Transactional
    public void asignaEstado(List<PreRegistroProductor> pres) {
        for (PreRegistroProductor pre : pres) {
            log.info("Actualizando pre registro: {}", pre.getFolio());
            ParametrosInscripcionProductor params = new ParametrosInscripcionProductor();
            params.setUuid(pre.getUuid());
            params.setFechaCancelacionNull(false);
            pre = buscadorPreRegistroProductor.buscaElemento(params);
            pre.setEstado(EstadoProductorHelper.getEstado(pre.getPredios()));
            log.info("Actualizando {} con estado: {}", pre.getFolio(), pre.getEstado());
            registroEntidad.actualiza(pre);
        }
        registroEntidad.flush();
        registroEntidad.clear();
    }

}
