/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.busqueda;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersona;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.Productor;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ismael
 */
@Repository("buscadorProductor")
@Slf4j
public class BuscadorProductorJpa implements BuscadorProductor {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Productor busca(String rfc, TipoPersona tipo) {
        Objects.requireNonNull(rfc, "El RFC no puede ser nulo.");
        Objects.requireNonNull(tipo, "El TipoPersona no puede ser nulo.");

        List<Productor> productores
                = entityManager.createQuery("SELECT p FROM Productor p "
                        + "WHERE "
                        + "p.rfc = :rfc "
                        + "AND p.tipoPersona = :tipo "
                        + "ORDER BY p.id ", Productor.class)
                        .setParameter("rfc", rfc)
                        .setParameter("tipo", tipo)
                        .getResultList();

        if (productores.isEmpty()) {
            throw new EmptyResultDataAccessException("Se esperaba al menos un productor.", 1);
        }

        if (productores.size() > 1) {
            LOGGER.warn("Existe más de un productor con los datos especificados: {}", productores.size());
        }
        return productores.get(0);
    }

    @Override
    public ProductorCiclo busca(Productor productor, CicloAgricola ciclo, Cultivo cultivo, boolean fetch) {
        Objects.requireNonNull(productor, "El Productor no puede ser nulo.");
        Objects.requireNonNull(ciclo, "El CicloAgricola no puede ser nulo.");
        Objects.requireNonNull(cultivo, "El Cultivo no puede ser nulo.");

        return entityManager.createQuery("SELECT pc FROM ProductorCiclo pc "
                + (fetch ? "LEFT JOIN FETCH pc.cultivos " : "")
                + "WHERE "
                + "pc.productor = :productor "
                + "AND pc.ciclo = :ciclo "
                + "AND pc.cultivo = :cultivo "
                + "AND pc.activo = :activo ", ProductorCiclo.class)
                .setParameter("productor", productor)
                .setParameter("ciclo", ciclo)
                .setParameter("cultivo", cultivo)
                .setParameter("activo", Boolean.TRUE)
                .getSingleResult();
    }

    @Override
    public ProductorCiclo busca(String folio, boolean fetch) {
        Objects.requireNonNull(folio, "El folio no puede ser nulo.");

        return entityManager.createQuery("SELECT pc FROM ProductorCiclo pc "
                + (fetch ? "LEFT JOIN FETCH pc.cultivos " : "")
                + "WHERE "
                + "pc.folio = :folio ", ProductorCiclo.class)
                .setParameter("folio", folio)
                .getSingleResult();
    }

    @Override
    public ProductorCiclo busca(InscripcionProductor inscripcion) {
        // TODO: no debería buscarse por CURP?
        Productor p = busca(inscripcion.getDatosProductor().getRfc(),
                inscripcion.getDatosProductor().getTipoPersona());
        return busca(p, inscripcion.getCiclo(), inscripcion.getCultivo(), false);
    }

    @Override
    public List<Productor> busca(ParametrosInscripcionProductor parametros) {

        StringBuilder sb = new StringBuilder("SELECT p FROM Productor p ");

        Map<String, Object> params = new HashMap<>();
        boolean first = true;

        if (Objects.nonNull(parametros.getClave())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "p.clave", "clave", parametros.getClave(), params);
        }

        if (Objects.nonNull(parametros.getCurp())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "p.curp", "curp", parametros.getCurp(), params);
        }

        if (Objects.nonNull(parametros.getRfcProductor())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "p.rfc", "rfc", parametros.getRfcProductor(), params);
        }

        if (Objects.nonNull(parametros.getNombre())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append(" p.persona.nombre like :nombre ");
            params.put("nombre", parametros.getNombre() + "%");
        }

        if (Objects.nonNull(parametros.getPapellido())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "p.persona.primerApellido", "primerApellido", parametros.getPapellido(), params);
        }

        if (Objects.nonNull(parametros.getSapellido())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "p.persona.segundoApellido", "segundoApellido", parametros.getSapellido(), params);
        }

        if (Objects.nonNull(parametros.getTipoPersona())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "p.tipoPersona", "tipoPersona", parametros.getTipoPersona(), params);
        }

        if (Objects.nonNull(parametros.getUuid())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "p.uuid", "uuid", parametros.getUuid(), params);
        }

        if (Objects.nonNull(parametros.getActivo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "p.activo", "activo", parametros.getActivo(), params);
        }

        sb.append("ORDER BY p.id ");
        LOGGER.debug("Ejecutando el query: {}", sb);
        TypedQuery<Productor> q
                = entityManager.createQuery(sb.toString(), Productor.class);
        QueryUtils.setParametros(q, params);

        long inicio = System.currentTimeMillis();
        List<Productor> productores = q.getResultList();
        LOGGER.debug("Se encontraron {} resultados en {} ms.", productores.size(),
                System.currentTimeMillis() - inicio);

        return productores;

    }

    @Override
    public List<ProductorCiclo> buscaProductorCiclo(ParametrosInscripcionProductor parametros) {

        StringBuilder sb = new StringBuilder("SELECT pc FROM ProductorCiclo pc ");

        Map<String, Object> params = new HashMap<>();
        boolean first = true;

        if (Objects.nonNull(parametros.getFolio())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "pc.folio", "folio", parametros.getFolio(), params);
        }

        if (Objects.nonNull(parametros.getClave())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "pc.productor.clave", "clave", parametros.getClave(), params);
        }

        if (Objects.nonNull(parametros.getCurp())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "pc.productor.curp", "curp", parametros.getCurp(), params);
        }

        if (Objects.nonNull(parametros.getRfcProductor())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "pc.productor.rfc", "rfc", parametros.getRfcProductor(), params);
        }

        if (Objects.nonNull(parametros.getUuidProductor())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "pc.productor.uuid", "uuid", parametros.getUuidProductor(), params);
        }

        if (Objects.nonNull(parametros.getNombre())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("pc.productor.persona.nombre like :nombre ");
            params.put("nombre", parametros.getNombre() + "%");
        }

        if (Objects.nonNull(parametros.getPapellido())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "pc.productor.persona.primerApellido", "primerApellido", parametros.getPapellido(), params);
        }

        if (Objects.nonNull(parametros.getSapellido())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "pc.productor.persona.segundoApellido", "segundoApellido", parametros.getSapellido(), params);
        }

        if (Objects.nonNull(parametros.getTipoPersona())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "pc.productor.tipoPersona", "tipoPersona", parametros.getTipoPersona(), params);
        }

        if (Objects.nonNull(parametros.getCultivo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "pc.cultivo", "cultivo", parametros.getCultivo(), params);
        }

        if (Objects.nonNull(parametros.getCiclo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "pc.ciclo", "ciclo", parametros.getCiclo(), params);
        }

        if (Objects.nonNull(parametros.getActivo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "pc.activo", "activo", parametros.getActivo(), params);
        }

        if (Objects.nonNull(parametros.getTipoProductor())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "pc.tipoProductor.clave", "tipoProductor", parametros.getTipoProductor(), params);
        }

        sb.append("ORDER BY pc.id ");
        LOGGER.debug("Ejecutando el query: {}", sb);
        TypedQuery<ProductorCiclo> q
                = entityManager.createQuery(sb.toString(), ProductorCiclo.class);
        QueryUtils.setParametros(q, params);

        long inicio = System.currentTimeMillis();
        List<ProductorCiclo> productores = q.getResultList();
        LOGGER.debug("Se encontraron {} resultados en {} ms.", productores.size(),
                System.currentTimeMillis() - inicio);

        return productores;

    }

    @Override
    public Productor busca(String clave) {
        Objects.requireNonNull(clave, "La clave no puede ser nula.");
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setClave(clave);
        List<Productor> resultados = busca(parametros);
        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontraron resultados.", 1);
        }
        return resultados.get(0);
    }

    @Override
    public Productor buscaElemento(ParametrosInscripcionProductor parametros) {
        List<Productor> productores = busca(parametros);
        if (productores.isEmpty()) {
            throw new EmptyResultDataAccessException("Sin resultados.", 1);
        }
        if (productores.size() > 1) {
            throw new IncorrectResultSizeDataAccessException("Se esperaba 1 resultado.", 1);
        }
        return productores.get(0);
    }

    @Override
    public List<ProductorCiclo> busca(Productor productor) {
        Objects.requireNonNull(productor, "El Productor no puede ser nulo.");

        return entityManager.createQuery("SELECT pc FROM ProductorCiclo pc "
                + "WHERE "
                + "pc.productor = :productor ", ProductorCiclo.class)
                .setParameter("productor", productor)
                .getResultList();
    }

    @Override
    public ProductorCiclo buscaElemento(String folio) {
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setFolio(folio);
        parametros.setActivo(Boolean.TRUE);

        List<ProductorCiclo> resultados = buscaProductorCiclo(parametros);
        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("Sin resultados.", 1);
        }
        if (resultados.size() > 1) {
            throw new IncorrectResultSizeDataAccessException("Se esperaba 1 resultado.", 1);
        }
        return resultados.get(0);
    }

    @Override
    public ProductorCiclo buscaElementoPc(ParametrosInscripcionProductor parametros) {

        List<ProductorCiclo> resultados = buscaProductorCiclo(parametros);
        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("Sin resultados.", 1);
        }
        if (resultados.size() > 1) {
            throw new IncorrectResultSizeDataAccessException("Se esperaba 1 resultado.", 1);
        }
        return resultados.get(0);
    }

}
