/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.inscripcion.ContratoProductor;

/**
 *
 * @author jurgen
 */
public interface BuscadorContratoProductor {

    List<ContratoProductor> busca(ParametrosContratoProductor parametros);

}
