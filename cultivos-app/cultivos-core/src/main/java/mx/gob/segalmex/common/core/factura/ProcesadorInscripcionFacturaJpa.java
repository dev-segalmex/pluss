/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura;

import java.math.BigDecimal;
import java.util.Calendar;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorFactura;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorUsoFactura;
import mx.gob.segalmex.common.core.factura.busqueda.ParametrosUsoFactura;
import mx.gob.segalmex.common.core.factura.validador.ValidadorCfdi;
import mx.gob.segalmex.common.core.folio.GeneradorFolio;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import mx.gob.segalmex.pluss.modelo.factura.Cfdi;
import mx.gob.segalmex.pluss.modelo.factura.ComprobanteRecepcion;
import mx.gob.segalmex.pluss.modelo.factura.ConceptoCfdi;
import mx.gob.segalmex.pluss.modelo.factura.EstatusConceptoCfdiEnum;
import mx.gob.segalmex.pluss.modelo.factura.EstatusFacturaEnum;
import mx.gob.segalmex.pluss.modelo.factura.EstatusUsoFactura;
import mx.gob.segalmex.pluss.modelo.factura.EstatusUsoFacturaEnum;
import mx.gob.segalmex.pluss.modelo.factura.Factura;
import mx.gob.segalmex.pluss.modelo.factura.FacturaRelacionada;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import mx.gob.segalmex.pluss.modelo.factura.UsoFactura;
import mx.gob.segalmex.pluss.modelo.secuencias.TipoFolioEnum;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.granos.core.contrato.busqueda.BuscadorInscripcionContrato;
import mx.gob.segalmex.granos.core.contrato.busqueda.ParametrosInscripcionContrato;
import mx.gob.segalmex.granos.core.historico.BuscadorHistoricoRegistro;
import mx.gob.segalmex.granos.core.historico.ParametrosHistoricoRegistro;
import mx.gob.segalmex.granos.core.historico.ProcesadorHistoricoRegistro;
import mx.gob.segalmex.granos.core.inscripcion.ActualizadorEstatusHelper;
import mx.gob.segalmex.granos.core.inscripcion.ProcesadorDatoCapturado;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.modelo.catalogos.Banco;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricolaEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivoEnum;
import mx.gob.segalmex.pluss.modelo.factura.TipoUsoFactura;
import mx.gob.segalmex.pluss.modelo.historico.HistoricoRegistro;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCicloGrupo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.remoting.RemoteAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ismael
 */
@Service("procesadorInscripcionFactura")
@Slf4j
public class ProcesadorInscripcionFacturaJpa implements ProcesadorInscripcionFactura {

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private BuscadorFactura buscadorFactura;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private GeneradorFolio generadorFolio;

    @Autowired
    private ProcesadorHistoricoRegistro procesadorHistorico;

    @Autowired
    private BuscadorHistoricoRegistro buscadorHistorico;

    @Autowired
    private ProcesadorCfdi procesadorCfdi;

    @Autowired
    private ProcesadorDatoCapturado procesadorDatoCapturado;

    @Autowired
    @Qualifier("validadorCfdiSat")
    private ValidadorCfdi validadorCfdi;

    @Autowired
    @Qualifier("validadorCfdiDuplicado")
    private ValidadorCfdi validadorCfdiDuplicado;

    @Autowired
    private FacturaProductorHelper facturaProductorHelper;

    @Autowired
    private FacturaGlobalHelper facturaGlobalHelper;

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Autowired
    private BuscadorInscripcionProductor buscadorInscripcionProductor;

    @Autowired
    private BuscadorInscripcionContrato buscadorContrato;

    @Autowired
    private BuscadorUsoFactura buscadorUsoFactura;

    @Autowired
    private BuscadorProductor buscadorProductor;

    @Autowired
    private ActualizadorEstatusHelper actualizadorEstatusHelper;

    @Transactional
    @Override
    public InscripcionFactura procesa(InscripcionFactura inscripcion, List<Factura> facturas) {
        verificaCierre(inscripcion);
        Cfdi cfdi = inscripcion.getCfdi();
        try {
            inscripcion.setFactura(buscadorFactura
                    .buscaElementoPorUuidTfd(cfdi.getUuidTimbreFiscalDigital()));
        } catch (EmptyResultDataAccessException ouch) {
        }
        inscripcion.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.PENDIENTE));
        inscripcion.setEstatusPrevio(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.DESCONOCIDO));
        inscripcion.setUuid(UUID.randomUUID().toString());
        inscripcion.setRfcEmpresa(inscripcion.getSucursal().getEmpresa().getRfc());
        if (Objects.isNull(inscripcion.getCantidad())) {
            inscripcion.setCantidad(cfdi.getTotalToneladas());
        }
        inscripcion.setFolio(generadorFolio.genera(TipoFolioEnum.SEQ_FOLIO_FACTURA, 6));
        inscripcion.setFechaPagoEstimulo(cfdi.getFecha());
        if (isPagoFechaComprobante(inscripcion)) {
            inscripcion.setFechaPagoEstimulo(inscripcion.getComprobantes().get(0).getFechaComprobante());
        }
        registroEntidad.guarda(inscripcion);
        for (Factura f : facturas) {
            FacturaRelacionada fr = new FacturaRelacionada();
            fr.setInscripcion(inscripcion);
            fr.setFacturaGlobal(inscripcion.getFactura());
            fr.setFacturaProductor(f);
            fr.setEstatus(EstatusFacturaEnum.SELECCIONADA.getClave());
            registroEntidad.guarda(fr);

            f.setEstatus(EstatusFacturaEnum.SELECCIONADA.getClave());
            registroEntidad.actualiza(f);
        }

        if (Objects.nonNull(inscripcion.getComprobantes())) {
            int i = 0;
            for (ComprobanteRecepcion cr : inscripcion.getComprobantes()) {
                cr.setInscripcionFactura(inscripcion);
                cr.setEstatus("nuevo");
                cr.setFolio(generadorFolio.genera(TipoFolioEnum.SEQ_FOLIO_COMPROBANTE_RECEPCION, 6));
                cr.setOrden(i++);
                registroEntidad.guarda(cr);
            }
        }

        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.CREACION,
                inscripcion.getUsuarioRegistra(), false, getEtiquetaGrupo(inscripcion));
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.VALIDACION_SAT,
                inscripcion.getUsuarioRegistra(), true, getEtiquetaGrupo(inscripcion));

        return inscripcion;
    }

    @Transactional
    @Override
    public void verifica(InscripcionFactura inscripcion) {
        Boolean valido = validadorCfdi.valida(inscripcion);

        List<HistoricoRegistro> historicos = buscadorHistorico.busca(inscripcion,
                buscadorCatalogo.buscaElemento(TipoHistoricoInscripcion.class,
                        TipoHistoricoInscripcionEnum.VALIDACION_SAT.getClave()), true);

        // Cerramos
        boolean verificacionInicial = false;
        for (HistoricoRegistro h : historicos) {
            verificacionInicial = true;
            procesadorHistorico.finaliza(h, h.getUsuarioRegistra());
        }

        if (verificacionInicial) {
            validadorCfdiDuplicado.valida(inscripcion);
            inscripcion.setValida(valido);
            generaHistoricoValidacion(inscripcion);
            procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.ASIGNACION,
                    inscripcion.getUsuarioRegistra(), true, getEtiquetaGrupo(inscripcion));
            actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, EstatusInscripcionEnum.NUEVO);
        } else {
            if (!valido.equals(inscripcion.getValida())) {
                LOGGER.warn("La factura cambió de estatus.");
                inscripcion.setValida(valido);
                generaHistoricoValidacion(inscripcion);
                procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.VALIDACION_SAT_MODIFICADA,
                        inscripcion.getUsuarioRegistra(), false, getEtiquetaGrupo(inscripcion));
            }
        }
        registroEntidad.actualiza(inscripcion);
    }

    private void generaHistoricoValidacion(InscripcionFactura inscripcion) {
        if (inscripcion.getValida()) {
            procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.VALIDACION_SAT_POSITIVA,
                    inscripcion.getUsuarioRegistra(), false, getEtiquetaGrupo(inscripcion));
            inscripcion.getCfdi().setEstatus("Válida");
        } else {
            procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.VALIDACION_SAT_NEGATIVA,
                    inscripcion.getUsuarioRegistra(), false, getEtiquetaGrupo(inscripcion));
            inscripcion.getCfdi().setEstatus("No válida");
        }
    }

    @Transactional
    @Override
    public void asigna(InscripcionFactura inscripcion, Usuario usuario, Usuario validador) {
        inscripcion.setUsuarioValidador(validador);
        inscripcion.setUsuarioAsignado(validador);
        actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, EstatusInscripcionEnum.VALIDACION);

        ParametrosHistoricoRegistro parametros = new ParametrosHistoricoRegistro();
        parametros.setEntidad(inscripcion);
        parametros.setTipo(buscadorCatalogo.buscaElemento(TipoHistoricoInscripcion.class,
                TipoHistoricoInscripcionEnum.ASIGNACION.getClave()));

        HistoricoRegistro asignacion = buscadorHistorico.busca(parametros).get(0);
        procesadorHistorico.finaliza(asignacion, usuario);
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.VALIDACION,
                usuario, true, getEtiquetaGrupo(inscripcion));
        inscripcion.setFechaValidacion(Calendar.getInstance());
        registroEntidad.actualiza(inscripcion);

        // Generamos datos capturados
        procesadorDatoCapturado.procesa(inscripcion);
    }

    @Transactional
    @Override
    public void validaPositivo(InscripcionFactura inscripcion, Usuario usuario, List<DatoCapturado> capturados) {
        if (Objects.isNull(inscripcion.getValida()) || !inscripcion.getValida()) {
            throw new SegalmexRuntimeException("El registro de la factura no es válido.",
                    "El CFDI no es válido según el SAT.");
        }
        validaCfdi(inscripcion);
        validaContrato(inscripcion);
        if (Objects.nonNull(inscripcion.getProductorCiclo())) {
            ProductorCiclo pc = inscripcion.getProductorCiclo();
            validaInscripcionProductor(pc);
            BigDecimal totales = pc.getToneladasTotales();
            BigDecimal facturadas = pc.getToneladasFacturadas();
            BigDecimal restantes = totales.subtract(facturadas);
            BigDecimal cantidad = inscripcion.getCantidad();
            if (inscripcion.getCultivo().getClave().equals(CultivoEnum.ARROZ.getClave())) {
                cantidad = cantidad.min(inscripcion.getCantidadComprobada());
            }
            BigDecimal facturables = restantes.min(cantidad);
            if (Objects.nonNull(inscripcion.getFactura())) {
                facturables = facturables.min(inscripcion.getFactura().getToneladasDisponibles());
            }
            if (facturables.compareTo(BigDecimal.ZERO) == 0) {
                throw new SegalmexRuntimeException("El registro de la factura no puede ser positivo.",
                        "La cantidad de toneladas facturables es 0.");
            }
            inscripcion.setFacturables(facturables);
            setCantidadComprobada(inscripcion);
        }

        // Cancelamos los históricos de validación y revalidación
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.REVALIDACION);
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.VALIDACION);

        // Actualizamos la inscripción y los datos capturados
        actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, EstatusInscripcionEnum.CORRECTO);
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.FINALIZACION,
                usuario, true, true, getEtiquetaGrupo(inscripcion));
        inscripcion.setFechaFinalizacion(Calendar.getInstance());
        procesadorDatoCapturado.validaEstatus(inscripcion, capturados, EstatusInscripcionEnum.POSITIVA);
        registroEntidad.actualiza(inscripcion);
    }

    @Transactional
    @Override
    public void validaNegativo(InscripcionFactura inscripcion, Usuario usuario, List<DatoCapturado> capturados) {
        actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, EstatusInscripcionEnum.CORRECCION);
        procesadorDatoCapturado.solicitaCorreccion(inscripcion, capturados);

        // Cancelamos los históricos de validación y revalidación
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.REVALIDACION);
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.VALIDACION);

        // Generamos el histórico de corrección
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.CORRECCION,
                usuario, true, true, getEtiquetaGrupo(inscripcion));

        // Lo reasignamos al usuario que registró
        inscripcion.setUsuarioAsignado(inscripcion.getUsuarioRegistra());
        registroEntidad.actualiza(inscripcion);
    }

    @Transactional
    @Override
    public void corrige(InscripcionFactura inscripcion, Usuario usuario, List<DatoCapturado> capturados) {
        actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, EstatusInscripcionEnum.SOLVENTADA);
        verificaCierreCorreccion(inscripcion);
        // Validamos los valores modificados
        procesadorDatoCapturado.corrige(inscripcion, capturados);

        // Cancelamos históricos de corrección
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.CORRECCION);

        // Generamos el histórico de revalidación
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.REVALIDACION,
                usuario, true, true, getEtiquetaGrupo(inscripcion));

        // Lo reasignamos al usuario que validó
        inscripcion.setUsuarioAsignado(inscripcion.getUsuarioValidador());

        // Actualizamos las entidades
        registroEntidad.actualiza(inscripcion);
    }

    @Transactional
    @Override
    public void cancela(InscripcionFactura inscripcion, Usuario usuario) {
        ParametrosUsoFactura parametros = new ParametrosUsoFactura();
        parametros.setFolio(inscripcion.getFolio());
        List<UsoFactura> usos = buscadorUsoFactura.busca(parametros);
        if (!usos.isEmpty()) {
            cancelaUsos(inscripcion, usos);
        }
        if (Objects.nonNull(inscripcion.getFactura())) {
            Factura f = inscripcion.getFactura();
            f.setToneladasDisponibles(f.getToneladasDisponibles().add(inscripcion.getFacturables()));
        }

        switch (inscripcion.getEstatus().getClave()) {
            case "cancelada":
            case "positiva":
                throw new SegalmexRuntimeException("Error", "La inscripción no se puede cancelar ya que se "
                        + "encuentra en estado: " + inscripcion.getEstatus().getNombre());
        }

        switch (inscripcion.getTipoFactura()) {
            case "productor":
                break;
            case "global":
                List<FacturaRelacionada> relacionadas = buscadorFactura.busca(inscripcion);
                for (FacturaRelacionada rf : relacionadas) {
                    // Al cancelar, permitimos que la factura se use en otra global
                    Factura productor = rf.getFacturaProductor();
                    productor.setEstatus(EstatusFacturaEnum.NUEVA.getClave());
                    registroEntidad.actualiza(productor);
                }
                break;
            default:
                throw new IllegalArgumentException("Tipo de factura desconocido.");
        }
        actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, EstatusInscripcionEnum.CANCELADA);
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.CANCELADO,
                usuario, false, true, getEtiquetaGrupo(inscripcion));
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.FINALIZACION,
                usuario, false, true, getEtiquetaGrupo(inscripcion));
        inscripcion.setFechaFinalizacion(Calendar.getInstance());
        registroEntidad.actualiza(inscripcion);
    }

    private boolean cancelaHistoricos(InscripcionFactura inscripcion, Usuario usuario, TipoHistoricoInscripcionEnum tipo) {
        List<HistoricoRegistro> historicos = buscadorHistorico.busca(inscripcion,
                buscadorCatalogo.buscaElemento(TipoHistoricoInscripcion.class,
                        tipo.getClave()), true);
        for (HistoricoRegistro abierto : historicos) {
            procesadorHistorico.finaliza(abierto, usuario);
        }

        return !historicos.isEmpty();
    }

    @Transactional
    @Override
    public void finalizaPositivo(InscripcionFactura inscripcion, Usuario usuario) {
        Factura f = procesadorCfdi.procesa(inscripcion.getCfdi(), inscripcion.getTipoFactura());
        inscripcion.setFactura(f);

        switch (inscripcion.getTipoFactura()) {
            case "productor":
                List<InformacionEstimulo> infos
                        = facturaProductorHelper.getInformaciones(inscripcion);
                ProductorCiclo pc = buscadorProductor.buscaElemento(inscripcion.getProductorCiclo().getFolio());

                // Generamos los usos factura
                List<UsoFactura> usos
                        = facturaProductorHelper.finalizaPositivo(inscripcion, pc, infos);
                // Si no se generaron pagos, salimos
                if (usos.isEmpty()) {
                    break;
                }
                for (UsoFactura uso : usos) {
                    registroEntidad.guarda(uso);
                }
                if (CultivoEnum.ARROZ.getClave().equals(inscripcion.getCultivo().getClave())) {
                    actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, EstatusInscripcionEnum.POSITIVA);
                }
                registroEntidad.actualiza(pc);
                registroEntidad.actualiza(f);
                break;
            case "global":
                facturaGlobalHelper.finalizaPositivo(inscripcion, f);
                break;
        }
        registroEntidad.actualiza(inscripcion);
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.FINALIZACION);
    }

    @Override
    @Transactional
    public void ignoraConcepto(InscripcionFactura inscripcion, int index) {
        BigDecimal totalToneladas = BigDecimal.ZERO;
        Cfdi cfdi = inscripcion.getCfdi();
        for (ConceptoCfdi c : cfdi.getConceptos()) {
            BigDecimal toneladas = c.getCantidad();
            if (index != c.getOrden() && c.getEstatus().equals(EstatusConceptoCfdiEnum.POSITIVO.getClave())) {
                switch (c.getClaveUnidad()) {
                    case "KGM":
                    case "58":
                        toneladas = c.getCantidad().divide(new BigDecimal(1000), 3, RoundingMode.HALF_UP);
                        break;
                }
                totalToneladas = totalToneladas.add(toneladas);
            } else {
                c.setEstatus(EstatusConceptoCfdiEnum.IGNORADO.getClave());
            }
            registroEntidad.actualiza(c);
        }
        cfdi.setTotalToneladas(totalToneladas);
        cfdi.setSubtotal(CfdiHelper.calculaSubtotal(cfdi));
        registroEntidad.actualiza(cfdi);
        inscripcion.setCantidad(cfdi.getTotalToneladas());
        inscripcion.setPrecioTonelada(BigDecimal.ZERO);
        inscripcion.setPrecioToneladaReal(BigDecimal.ZERO);
        inscripcion.setPrecioAjuste(BigDecimal.ZERO);
        if (cfdi.getTotalToneladas().compareTo(BigDecimal.ZERO) > 0) {
            inscripcion.setPrecioTonelada(cfdi.getSubtotal().divide(cfdi.getTotalToneladas(), 3, RoundingMode.HALF_UP));
            inscripcion.setPrecioToneladaReal(inscripcion.getPrecioTonelada());
        }
        registroEntidad.actualiza(inscripcion);
    }

    @Override
    @Transactional
    public void revalida(InscripcionFactura inscripcion, Usuario usuario) {
        switch (inscripcion.getEstatus().getClave()) {
            case "correccion":
                break;
            default:
                throw new SegalmexRuntimeException("Error", "El registro no se puede revalidar ya que se "
                        + "encuentra en estatus: " + inscripcion.getEstatus().getNombre());
        }

        inscripcion.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.SOLVENTADA.getClave()));

        // Cancelamos históricos de corrección (por si estaba en corrección)
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.CORRECCION);

        // Generamos el histórico de revalidación
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.REVALIDACION,
                usuario, true, true, getEtiquetaGrupo(inscripcion));

        // Lo reasignamos al usuario que validó
        inscripcion.setUsuarioAsignado(inscripcion.getUsuarioValidador());
        registroEntidad.actualiza(inscripcion);
    }

    @Override
    @Transactional
    public void duplica(UsoFactura base, UsoFactura nuevo, Usuario usuario) {
        nuevo.setUuid(UUID.randomUUID().toString());
        nuevo.setFactura(base.getFactura());
        nuevo.setProductor(base.getProductor());
        nuevo.setProductorCiclo(base.getProductorCiclo());
        nuevo.setEstatus(base.getEstatus());
        nuevo.setToneladas(base.getToneladas());
        nuevo.setFolio(base.getFolio());
        nuevo.setTipoCultivo(base.getTipoCultivo());
        //nuevo.setEstimuloTonelada(ya viene del front);
        nuevo.setEstimuloTotal(nuevo.getEstimuloTonelada().multiply(base.getToneladas())
                .setScale(2, RoundingMode.HALF_UP));
        nuevo.setTipo(buscadorCatalogo.buscaElemento(TipoUsoFactura.class,
                nuevo.getTipo().getId())); //Viene del front
        nuevo.setEstado(base.getEstado());
        nuevo.setComentarioEstatus("Generado por " + usuario.getClave());
        nuevo.setOrdenEstimulo(base.getOrdenEstimulo());
        //banco viene del front
        nuevo.setBanco(buscadorCatalogo.buscaElemento(Banco.class, base.getBanco().getId()));
        //nuevo.setClabe(viene del front);
        nuevo.setSuperficiePredio(base.getSuperficiePredio());
        nuevo.setContrato(base.getContrato());
        nuevo.setTipoPrecio(base.getTipoPrecio());
        nuevo.setFechaEstimulo(base.getFechaEstimulo());
        registroEntidad.guarda(nuevo);
    }

    /**
     * Se encarga de armar la etiquetaGrupo que debe llevar su Histórico.
     *
     * @param inscripcion
     * @return
     */
    private String getEtiquetaGrupo(InscripcionFactura inscripcion) {
        return inscripcion.getCultivo().getClave() + ":" + inscripcion.getCiclo().getClave();
    }

    /**
     * Solo para el caso de maíz se asigna la cantidad comprobada igual a la
     * cantidad facturable.
     *
     * @param inscripcion
     */
    private void setCantidadComprobada(InscripcionFactura inscripcion) {
        if (inscripcion.getCultivo().getClave().equals(CultivoEnum.MAIZ_COMERCIAL.getClave())) {
            inscripcion.setCantidadComprobada(inscripcion.getFacturables());
        }
    }

    private void verificaCierreCorreccion(InscripcionFactura inscripcion) {
        Parametro parametro = buscadorParametro.buscaElementoOpcional(inscripcion.getCultivo()
                .getClave() + ":" + inscripcion.getCiclo().getClave() + ":cierre-correccion-facturas");
        if (Objects.nonNull(parametro)) {
            Calendar cierre = SegalmexDateUtils
                    .parseCalendar(parametro.getValor(), "yyyy-MM-dd'T'HH:mm:ss");
            if (Calendar.getInstance().after(cierre)) {
                throw new SegalmexRuntimeException("Error al corregir la factura.",
                        "La corrección de facturas cerró el " + parametro.getValor());
            }
        }
    }

    private void verificaCierre(InscripcionFactura inscripcion) {
        Parametro parametro = buscadorParametro.buscaElementoOpcional(inscripcion.getCultivo()
                .getClave() + ":" + inscripcion.getCiclo().getClave() + ":facturas:cierre");
        if (Objects.nonNull(parametro)) {
            Calendar cierre = SegalmexDateUtils
                    .parseCalendar(parametro.getValor(), "yyyy-MM-dd'T'HH:mm:ss");
            if (Calendar.getInstance().after(cierre)) {
                throw new SegalmexRuntimeException("Error al registrar la factura.",
                        "El registro de facturas cerró el " + parametro.getValor());
            }
        }
    }

    private void validaInscripcionProductor(ProductorCiclo pc) {
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setRfcProductor(pc.getProductor().getRfc());
        parametros.setCiclo(pc.getCiclo());
        parametros.setCultivo(pc.getCultivo());
        parametros.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.POSITIVA));
        try {
            buscadorInscripcionProductor.buscaElemento(parametros);
        } catch (EmptyResultDataAccessException ouch) {
            LOGGER.error("No existe InscripcionProductor con el RFC: {} y ciclo: {} en positivo.",
                    pc.getProductor().getRfc(), pc.getCiclo().getClave());
            throw new SegalmexRuntimeException("Error al validar.",
                    "El productor asociado a esta factura, no se encuentre en estatus positivo.");
        }
    }

    private void validaContrato(InscripcionFactura inscripcion) {
        String contrato = StringUtils.trimToNull(inscripcion.getContrato());
        if (Objects.nonNull(contrato)) {
            try {
                ParametrosInscripcionContrato parametros = new ParametrosInscripcionContrato();
                parametros.setCultivo(inscripcion.getCultivo());
                parametros.setNumeroContrato(contrato);
                parametros.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.POSITIVA));
                buscadorContrato.buscaElemento(parametros);
            } catch (EmptyResultDataAccessException ouch) {
                LOGGER.error("El contrato: {} no está como positivo.", contrato);
                throw new SegalmexRuntimeException("Error al validar.", "El contrato: "
                        + contrato + " no está en estatus positivo.");
            }
        }
    }

    /**
     * Se encarga de volver a validar contra el SAT el CFDI de la
     * InscripcionFactura.
     *
     * @param inscripcion
     */
    private void validaCfdi(InscripcionFactura inscripcion) {
        try {
            Boolean valido = validadorCfdi.valida(inscripcion);
            if (!valido) {
                throw new SegalmexRuntimeException("El registro de la factura no es válido.",
                        "El CFDI no es válido según el SAT.");
            }
        } catch (RemoteAccessException ouch) {
            throw new SegalmexRuntimeException("Error al verificar validez en el SAT.",
                    "No se puede conectar con el SAT, inténtelo más tarde.");
        }
    }

    /**
     * Se encarga de cancelar los usos de la InscripcionFactura, siempre que
     * estos esten en estatus de nuevo o pendiente. Esto implica regresar a
     * donde corresponde las toneladas de cada UsoFactura.
     *
     * @param i la inscripcionFactura que se cancelará
     * @param usos los usos a cancelar.
     */
    private void cancelaUsos(InscripcionFactura i, List<UsoFactura> usos) {
        ProductorCiclo pc = usos.get(0).getProductorCiclo();
        Map<String, ProductorCicloGrupo> grupos = getGrupos(pc.getGrupos());
        for (UsoFactura uf : usos) {
            switch (EstatusUsoFacturaEnum.getInstance(uf.getEstatus().getClave())) {
                case NUEVO:
                case PENDIENTE:
                    break;
                default:
                    throw new SegalmexRuntimeException("La factura no se puede cancelar.",
                            "La factura tiene toneladas con estímulo en pagos.");
            }
            pc.setToneladasFacturadas(pc.getToneladasFacturadas().subtract(uf.getToneladas()));
            ProductorCicloGrupo pcg = grupos.get(i.getTipoCultivo().getGrupoEstimulo());
            pcg.setToneladasFacturadas(pcg.getToneladasFacturadas().subtract(uf.getToneladas()));
            uf.setEstatus(buscadorCatalogo.buscaElemento(EstatusUsoFactura.class, EstatusUsoFacturaEnum.NO_VALIDO));
            registroEntidad.actualiza(uf);
        }
        registroEntidad.actualiza(pc);
    }

    private Map<String, ProductorCicloGrupo> getGrupos(List<ProductorCicloGrupo> grupos) {
        Map<String, ProductorCicloGrupo> map = new HashMap<>();
        for (ProductorCicloGrupo pcg : grupos) {
            map.put(pcg.getGrupoEstimulo(), pcg);
        }
        return map;
    }

    private boolean isPagoFechaComprobante(InscripcionFactura inscripcion) {
        return inscripcion.getTipoFactura().equals("productor")
                && inscripcion.getTipoCultivo().getClave().equals(TipoCultivoEnum.ARROZ_LARGO.getClave())
                && inscripcion.getEstado().getClave().equals("18")
                && inscripcion.getCiclo().getClave().equals(CicloAgricolaEnum.OI2021.getClave());

    }
}
