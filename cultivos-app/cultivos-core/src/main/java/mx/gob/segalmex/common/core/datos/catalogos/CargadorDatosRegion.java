/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.datos.catalogos;

import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Region;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author oscar
 */
public class CargadorDatosRegion extends AbstractCargadorDatosCatalogo<Region> {

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Override
    public Region getInstance(String[] elementos) {
        Region r = new Region();
        r.setClave(elementos[0]);
        r.setNombre(elementos[1]);
        r.setOrden(Integer.parseInt(elementos[2]));
        r.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, elementos[3]));
        r.setEstado(buscadorCatalogo.buscaElemento(Estado.class, elementos[4]));
        r.setEstadoEstimulo(buscadorCatalogo.buscaElemento(Estado.class, elementos[5]));
        r.setTipoCultivo(buscadorCatalogo.buscaElemento(TipoCultivo.class, elementos[6]));
        r.setActivo(true);
        return r;
    }

}
