/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato.pdf;

import com.lowagie.text.Font;
import java.awt.Color;

/**
 *
 * @author ismael
 */
public class PdfConstants {

    /**
     * Color negro institucional definido para GobMX.
     */
    public static final Color BACKGROUND_GOBMX_BLACK = Color.BLACK;

    /**
     * Color blanco institucional definido para GobMX.
     */
    public static final Color BACKGROUND_GOBMX_WHITE = Color.WHITE;

    /**
     * Color gris institucional definido para GobMX.
     */
    public static final Color BACKGROUND_GOBMX_GRAY = new Color(242, 242, 242);

    /**
     * Color gris institucional definido para GobMX.
     */
    public static final Color BACKGROUND_GOBMX_DARKGRAY = new Color(166, 166, 169);

    /**
     * Color de fondo utilizado por el logotipo de GobMX.
     */
    public static final Color BACKGROUND_GOBMX_LOGO = new Color(56, 60, 62);

    /**
     * Color de fondo utilizado por el footer de GobMx.
     */
    public static final Color BACKGROUND_GOBMX_FOOTER = new Color(234, 234, 232);

    /**
     * Fuente definida por Gob MX para lo siguiente:
     * <ul>
     * <li>Títulos</li>
     * </ul>
     */
    public static final Font SOBERANA_LIGHT_10_BLACK = PdfFontHelper.getSoberanaSansLight(9, Font.NORMAL, Color.BLACK);

    /**
     * Fuente definida por Gob MX para lo siguiente:
     * <ul>
     * <li>Footer</li>
     * </ul>
     */
    public static final Font SOBERANA_LIGHT_8_BLACK = PdfFontHelper.getSoberanaSansLight(8, Font.NORMAL, Color.BLACK);

    /**
     * Valor del padding de las celdas en centímetros.
     */
    public static final float CELDA_PADDING_CM = 0.1f;

    /**
     * Valor del padding izquierdo de las celdas.
     */
    public static final float CELDA_LEFT_PADDING_CM = 0.1f;

    /**
     * Valor de la altura mínima de la celda en centímetros.
     */
    public static final float CELDA_ALTURA_MINIMA_CM = 0.6f;

    /**
     * Valor de la altura mínima de los headers.
     */
    public static final float CELDA_HEADER_ALTURA_MINIMA_CM = 1.5f;

    /**
     * Valor del borde de la celda.
     */
    public static final float CELDA_TAMANO_BORDE = 1f;

    /**
     * Título oficial del Comisión Mexicana de Ayuda a Refugiados.
     */
    public static final String TITULO_INSTITUCION = "Seguridad Alimentaria Mexicana";

}
