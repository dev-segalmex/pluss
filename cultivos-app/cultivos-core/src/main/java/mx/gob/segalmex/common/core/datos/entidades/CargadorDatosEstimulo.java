/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.datos.entidades;

import java.math.BigDecimal;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosEntidad;
import mx.gob.segalmex.common.core.estimulo.busqueda.BuscadorEstimulo;
import mx.gob.segalmex.common.core.estimulo.busqueda.ParametrosEstimulo;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.pago.Estimulo;
import mx.gob.segalmex.pluss.modelo.pago.TipoEstimuloEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.productor.TipoProductorEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;

/**
 *
 * @author oscar
 */
public class CargadorDatosEstimulo extends AbstractCargadorDatosEntidad<Estimulo> {

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorEstimulo buscadorEstimulo;

    @Override
    public Estimulo getInstance(String[] elementos) {
        Estimulo estimulo = new Estimulo();
        estimulo.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, elementos[0]));
        estimulo.setEstado(buscadorCatalogo.buscaElemento(Estado.class, elementos[1]));
        estimulo.setFechaInicio(SegalmexDateUtils.parseCalendar(elementos[2], "yyyy-MM-dd'T'HH:mm:ss"));
        estimulo.setFechaFin(SegalmexDateUtils.parseCalendar(elementos[3], "yyyy-MM-dd'T'HH:mm:ss"));
        estimulo.setCantidad(new BigDecimal(elementos[4]));
        estimulo.setOrden(Integer.parseInt(elementos[5]));
        estimulo.setAplicaEstimulo(elementos[6]);
        estimulo.setTipo(elementos[7]);
        estimulo.setContrato(elementos[8]);
        estimulo.setTipoProductor(TipoProductorEnum.MEDIANO.getClave());
        return estimulo;
    }

    @Override
    public boolean existe(Estimulo elemento) {
        ParametrosEstimulo params = new ParametrosEstimulo();
        params.setAplicaEstimulo(elemento.getAplicaEstimulo());
        params.setCiclo(elemento.getCiclo());
        params.setEstado(elemento.getEstado());
        params.setFechaRango(elemento.getFechaInicio());
        params.setOrden(elemento.getOrden());
        params.setCantidad(elemento.getCantidad());
        params.setTipo(elemento.getTipo());
        if (elemento.getTipo().equals(TipoEstimuloEnum.CONTRATO.getClave())) {
            params.setContrato(StringUtils.trimToNull(elemento.getContrato()));
        }
        try {
            buscadorEstimulo.buscaElemento(params);
            return true;
        } catch (EmptyResultDataAccessException ouch) {
            return false;
        }
    }

    @Override
    public void registra(Estimulo elemento) {
        registroEntidad.guarda(elemento);
    }

}
