package mx.gob.segalmex.common.core.datos.catalogos;

import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.productor.TipoPosesion;

/**
 *
 * @author ismael
 */
public class CargadorDatosTipoPosesion extends AbstractCargadorDatosCatalogo<TipoPosesion> {

    @Override
    public TipoPosesion getInstance(String[] elementos) {
        TipoPosesion tipo = new TipoPosesion();
        tipo.setClave(elementos[0]);
        tipo.setNombre(elementos[1]);
        tipo.setActivo(true);
        tipo.setOrden(Integer.parseInt(elementos[2]));
        return tipo;
    }

}
