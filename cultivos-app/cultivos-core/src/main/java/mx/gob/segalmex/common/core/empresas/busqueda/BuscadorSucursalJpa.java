/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.empresas.busqueda;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorUsoCatalogo;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.catalogos.Empresa;
import mx.gob.segalmex.pluss.modelo.catalogos.PrefijoEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jurgen
 */
@Repository("buscadorSucursal")
public class BuscadorSucursalJpa implements BuscadorSucursal {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private BuscadorUsoCatalogo buscadorUso;

    @Override
    public List<Sucursal> getSucursales(Empresa empresa) {
        StringBuilder sb = new StringBuilder("SELECT s FROM Sucursal s ");
        Map<String, Object> params = new HashMap<>();
        boolean first = true;
        if (Objects.nonNull(empresa)) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "s.empresa", "empresa", empresa, params);
        }
        sb.append("ORDER BY s.id ");
        TypedQuery<Sucursal> q
                = entityManager.createQuery(sb.toString(), Sucursal.class);
        QueryUtils.setParametros(q, params);
        long inicio = System.currentTimeMillis();
        List<Sucursal> sucursales = q.getResultList();
        return sucursales;
    }

    @Override
    public Sucursal buscaElemento(Usuario usuario) {
        List<Sucursal> sucursales = buscadorUso.busca(Sucursal.class,
                PrefijoEnum.SUCURSAL_USUARIO.getClave() + usuario.getClave());
        if (sucursales.isEmpty()) {
            throw new EmptyResultDataAccessException(1);
        }
        return sucursales.get(0);
    }

    @Override
    public List<Sucursal> busca(Usuario usuario) {
        return buscadorUso.busca(Sucursal.class,
                PrefijoEnum.SUCURSAL_USUARIO.getClave() + usuario.getClave());
    }

}
