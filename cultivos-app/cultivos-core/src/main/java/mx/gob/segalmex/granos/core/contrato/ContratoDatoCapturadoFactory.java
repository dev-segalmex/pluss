/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato;

import mx.gob.segalmex.granos.core.inscripcion.DatoCapturadoFactory;
import java.util.List;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.granos.core.inscripcion.InscripcionDatoCapturado;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivoEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;
import mx.gob.segalmex.pluss.modelo.contrato.CoberturaInscripcionContrato;

/**
 * Clase que genera instancias de <code>DatoCapturado</code> a partir del modelo
 * de datos de <code>InscripcionContrato</code>.
 *
 * @author ismael
 */
public class ContratoDatoCapturadoFactory extends DatoCapturadoFactory {

    private static final String IAR = "Instrumentos de administración de riesgos";

    /**
     * Obtiene la lista de datos capturados de una inscripción de contrato.
     *
     * @param inscripcion la inscripción de la que se obtienen los datos
     * capturados.
     * @return la lista de datos capturados.
     */
    public static List<DatoCapturado> getInstance(InscripcionContrato inscripcion) {
        InscripcionDatoCapturado idc = new InscripcionDatoCapturado();

        idc.agrega(getInstance(inscripcion, DatoCapturadoContratoEnum.TIPO_CULTIVO,
                inscripcion.getTipoCultivo()));
        idc.agrega(getInstance(inscripcion, DatoCapturadoContratoEnum.NO_CONTRATO,
                inscripcion.getNumeroContrato()));
        idc.agrega(getInstance(inscripcion, DatoCapturadoContratoEnum.FECHA_FIRMA,
                SegalmexDateUtils.format(inscripcion.getFechaFirma())));
        idc.agrega(getInstance(inscripcion, DatoCapturadoContratoEnum.NO_PRODUCTORES,
                String.valueOf(inscripcion.getNumeroProductores())));

        idc.agrega(getInstance(inscripcion, DatoCapturadoContratoEnum.TIPO_EMPRESA_COMPRADOR,
                inscripcion.getTipoEmpresaComprador()));
        idc.agrega(getInstance(inscripcion, DatoCapturadoContratoEnum.NOMBRE_COMPRADOR,
                inscripcion.getNombreComprador()));
        idc.agrega(getInstance(inscripcion, DatoCapturadoContratoEnum.RFC_COMPRADOR,
                inscripcion.getRfcComprador()));

        idc.agrega(getInstance(inscripcion, DatoCapturadoContratoEnum.TIPO_EMPRESA_VENDEDOR,
                inscripcion.getTipoEmpresaVendedor()));
        idc.agrega(getInstance(inscripcion, DatoCapturadoContratoEnum.NOMBRE_VENDEDOR,
                inscripcion.getNombreVendedor()));
        idc.agrega(getInstance(inscripcion, DatoCapturadoContratoEnum.RFC_VENDEDOR,
                inscripcion.getRfcVendedor()));

        DatoCapturado dc = getInstance(inscripcion, DatoCapturadoContratoEnum.ENTIDAD_PRODUCE,
                inscripcion.getEstado());
        dc.setClave(dc.getClave());
        dc.setNombre(dc.getNombre());
        idc.agrega(dc);

        dc = getInstance(inscripcion, DatoCapturadoContratoEnum.SUPERFICIE_TOTAL, String.valueOf(inscripcion.getSuperficie()));
        dc.setClave(dc.getClave());
        dc.setNombre(dc.getNombre());
        idc.agrega(dc);

        dc = getInstance(inscripcion, DatoCapturadoContratoEnum.VOLUMEN_TOTAL, String.valueOf(inscripcion.getVolumenTotal()));
        dc.setClave(dc.getClave());
        dc.setNombre(dc.getNombre());
        idc.agrega(dc);

        dc = getInstance(inscripcion, DatoCapturadoContratoEnum.TIPO_PRECIO, inscripcion.getTipoPrecio());
        dc.setClave(dc.getClave());
        dc.setNombre(dc.getNombre());
        idc.agrega(dc);

        dc = getInstance(inscripcion, DatoCapturadoContratoEnum.PRECIO_FUTURO, String.valueOf(inscripcion.getPrecioFuturo()));
        dc.setClave(dc.getClave());
        dc.setNombre(dc.getNombre());
        idc.agrega(dc);

        dc = getInstance(inscripcion, DatoCapturadoContratoEnum.BASE_ACORDADA, String.valueOf(inscripcion.getBaseAcordada()));
        dc.setClave(dc.getClave());
        dc.setNombre(dc.getNombre());
        idc.agrega(dc);

        dc = getInstance(inscripcion, DatoCapturadoContratoEnum.PRECIO_FIJO, String.valueOf(inscripcion.getPrecioFijo()));
        dc.setClave(dc.getClave());
        dc.setNombre(dc.getNombre());
        idc.agrega(dc);

        DatoCapturado contrato = getInstanceArchivo(inscripcion, DatoCapturadoContratoEnum.CONTRATO_PDF,
                DatoCapturadoContratoEnum.CONTRATO_PDF.getClave() + ".pdf", null);
        idc.agrega(contrato);

        DatoCapturado adendum = getInstanceArchivo(inscripcion, DatoCapturadoContratoEnum.CONTRATO_ADENDUM_PDF,
                DatoCapturadoContratoEnum.CONTRATO_ADENDUM_PDF.getClave() + ".pdf", null);
        idc.agrega(adendum);

        DatoCapturado curps = getInstanceArchivo(inscripcion, DatoCapturadoContratoEnum.CURPS_XLSX,
                DatoCapturadoContratoEnum.CURPS_XLSX.getClave() + ".xlsx", null);
        idc.agrega(curps);

        DatoCapturado curpspProdAdendum = getInstanceArchivo(inscripcion, DatoCapturadoContratoEnum.CURPS_PRODUCTORES_ADENDUM_XLSX,
                DatoCapturadoContratoEnum.CURPS_PRODUCTORES_ADENDUM_XLSX.getClave() + ".xlsx", null);
        idc.agrega(curpspProdAdendum);

        //Si el tipo de cultivo es semilla certificada creamos el PDF de constancia SNICS.
        if (inscripcion.getTipoCultivo().getClave().equals(TipoCultivoEnum.TRIGO_SEMILLA.getClave())) {
            DatoCapturado snics = getInstanceArchivo(inscripcion, DatoCapturadoContratoEnum.CONSTANCIA_SNICS_PDF,
                    DatoCapturadoContratoEnum.CONSTANCIA_SNICS_PDF.getClave() + ".pdf", null);
            idc.agrega(snics);
        }

        generaIar(inscripcion, idc);
        return idc.getDatos();
    }

    public static List<DatoCapturado> getInstanceIar(InscripcionContrato inscripcion) {
        InscripcionDatoCapturado idc = new InscripcionDatoCapturado();
        generaIar(inscripcion, idc);
        return idc.getDatos();
    }

    private static void generaIar(InscripcionContrato inscripcion, InscripcionDatoCapturado idc) {
        if (!inscripcion.getCoberturas().isEmpty()) {
            int index = 1;
            for (CoberturaInscripcionContrato c : inscripcion.getCoberturas()) {
                String suffix = "(" + index++ + ")";
                int orden = c.getOrden();

                idc.agrega(getInstance(inscripcion, DatoCapturadoContratoEnum.NUMERO,
                        c.getNumero(), orden, suffix, IAR));
                idc.agrega(getInstance(inscripcion, DatoCapturadoContratoEnum.ENTIDAD_COBERTURA,
                        c.getEntidadCobertura(), orden, suffix, IAR));
                idc.agrega(getInstance(inscripcion, DatoCapturadoContratoEnum.TIPO_COMPRADOR,
                        c.getTipoComprador(), orden, suffix, IAR));
                idc.agrega(getInstance(inscripcion, DatoCapturadoContratoEnum.NOMBRE_COMPRADOR_COBERTURA,
                        c.getNombreComprador(), orden, suffix, IAR));
                idc.agrega(getInstance(inscripcion, DatoCapturadoContratoEnum.FECHA_COMPRA,
                        SegalmexDateUtils.format(c.getFechaCompra()), orden, suffix, IAR));
                idc.agrega(getInstance(inscripcion, DatoCapturadoContratoEnum.FECHA_VENCIMIENTO,
                        SegalmexDateUtils.format(c.getFechaVencimiento()), orden, suffix, IAR));
                idc.agrega(getInstance(inscripcion, DatoCapturadoContratoEnum.TIPO_OPERACION,
                        c.getTipoOperacionCobertura(), orden, suffix, IAR));
                idc.agrega(getInstance(inscripcion, DatoCapturadoContratoEnum.NUMERO_CONTRATOS,
                        String.valueOf(c.getNumeroContratos()), orden, suffix, IAR));
                idc.agrega(getInstance(inscripcion, DatoCapturadoContratoEnum.PRECIO_EJERCICIO_DOLARES,
                        String.valueOf(c.getPrecioEjercicioDolares()), orden, suffix, IAR));
                idc.agrega(getInstance(inscripcion, DatoCapturadoContratoEnum.PRECIO_EJERCICIO_PESOS,
                        String.valueOf(c.getPrecioEjercicioPesos()), orden, suffix, IAR));
                idc.agrega(getInstance(inscripcion, DatoCapturadoContratoEnum.PRIMA_DOLARES,
                        String.valueOf(c.getPrimaDolares()), orden, suffix, IAR));
                idc.agrega(getInstance(inscripcion, DatoCapturadoContratoEnum.PRIMA_PESOS,
                        String.valueOf(c.getPrimaPesos()), orden, suffix, IAR));
                idc.agrega(getInstance(inscripcion, DatoCapturadoContratoEnum.COMISIONES_DOLARES,
                        String.valueOf(c.getComisionesDolares()), orden, suffix, IAR));
                idc.agrega(getInstance(inscripcion, DatoCapturadoContratoEnum.COMISIONES_PESOS,
                        String.valueOf(c.getComisionesPesos()), orden, suffix, IAR));

                idc.agrega(getInstanceArchivo(inscripcion, DatoCapturadoContratoEnum.CONTRATO_MARCO_PDF,
                        orden + "_" + DatoCapturadoContratoEnum.CONTRATO_MARCO_PDF.getClave() + ".pdf",
                        orden, suffix, IAR));
                idc.agrega(getInstanceArchivo(inscripcion, DatoCapturadoContratoEnum.ESTADO_CUENTA_PDF,
                        orden + "_" + DatoCapturadoContratoEnum.ESTADO_CUENTA_PDF.getClave() + ".pdf",
                        orden, suffix, IAR));
                idc.agrega(getInstanceArchivo(inscripcion, DatoCapturadoContratoEnum.CARTA_CONFIRMACION_PDF,
                        orden + "_" + DatoCapturadoContratoEnum.CARTA_CONFIRMACION_PDF.getClave() + ".pdf",
                        orden, suffix, IAR));
                idc.agrega(getInstanceArchivo(inscripcion, DatoCapturadoContratoEnum.PODER_PDF,
                        orden + "_" + DatoCapturadoContratoEnum.PODER_PDF.getClave() + ".pdf",
                        orden, suffix, IAR));
            }
        }
    }

}
