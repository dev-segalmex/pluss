/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.historico;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.historico.EntidadHistorico;
import mx.gob.segalmex.pluss.modelo.historico.HistoricoRegistro;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcion;

/**
 *
 * @author ismael
 */
public interface BuscadorHistoricoRegistro {

    List<HistoricoRegistro> busca(ParametrosHistoricoRegistro parametros);

    List<HistoricoRegistro> busca(EntidadHistorico entidad);

    List<HistoricoRegistro> busca(EntidadHistorico entidad, TipoHistoricoInscripcion tipo, boolean abierto);

}
