/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.busqueda;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.factura.FacturaRestringida;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ismael
 */
@Repository("buscadorFacturaRestringida")
public class BuscadorFacturaRestringidaJpa implements BuscadorFacturaRestringida {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public boolean existe(String uuid, CicloAgricola ciclo, Cultivo cultivo) {
        ParametrosFacturaRestringida params = new ParametrosFacturaRestringida();
        params.setActivo(Boolean.TRUE);
        params.setUuidTimbreFiscalDigital(uuid);
        params.setCiclo(ciclo);
        params.setCultivo(cultivo);
        List<FacturaRestringida> resultados = busca(params);

        if (resultados.size() > 0) {
            return true;
        }
        return false;
    }

    @Override
    public List<FacturaRestringida> busca(ParametrosFacturaRestringida parametros) {
        StringBuilder sb = new StringBuilder("SELECT f FROM FacturaRestringida f ");
        Map<String, Object> params = new HashMap<>();
        boolean first = true;

        if (Objects.nonNull(parametros.getId())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "f.id", "id", parametros.getId(), params);
        }

        if (Objects.nonNull(parametros.getActivo()) && parametros.getActivo()) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "f.activo", "activo", parametros.getActivo(), params);
        }

        if (Objects.nonNull(parametros.getCiclo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "f.ciclo", "ciclo", parametros.getCiclo(), params);
        }

        if (Objects.nonNull(parametros.getCultivo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "f.cultivo", "cultivo", parametros.getCultivo(), params);
        }

        if (Objects.nonNull(parametros.getUuidTimbreFiscalDigital())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "f.uuidTimbreFiscalDigital", "uuid", parametros.getUuidTimbreFiscalDigital(), params);
        }

        TypedQuery<FacturaRestringida> q
                = entityManager.createQuery(sb.toString(), FacturaRestringida.class);
        QueryUtils.setParametros(q, params);

        List<FacturaRestringida> resultados = q.getResultList();

        return resultados;

    }

    @Override
    public FacturaRestringida buscaElemento(ParametrosFacturaRestringida parametros) {
        Objects.requireNonNull(parametros, "Los parámetros no pueden ser nulos.");
        List<FacturaRestringida> resultados = busca(parametros);
        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontraron resultados.", 1);
        }

        if (resultados.size() > 1) {
            throw new IncorrectResultSizeDataAccessException("Se encontró más "
                    + "de un elemento.", 1);
        }

        return resultados.get(0);
    }

    @Override
    public FacturaRestringida buscaElemento(String uuidTimbreFiscalDigital) {
        Objects.requireNonNull(uuidTimbreFiscalDigital, "El timbre no puede ser nulo.");
        ParametrosFacturaRestringida parametros = new ParametrosFacturaRestringida();
        parametros.setUuidTimbreFiscalDigital(uuidTimbreFiscalDigital);
        List<FacturaRestringida> resultados = busca(parametros);
        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontraron resultados.", 1);
        }

        if (resultados.size() > 1) {
            throw new IncorrectResultSizeDataAccessException("Se encontró más "
                    + "de un elemento.", 1);
        }

        return resultados.get(0);
    }

}
