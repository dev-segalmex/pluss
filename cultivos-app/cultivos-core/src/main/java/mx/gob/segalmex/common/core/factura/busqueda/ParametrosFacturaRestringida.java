/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.busqueda;

import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;

/**
 *
 * @author oscar
 */
@Getter
@Setter
public class ParametrosFacturaRestringida {

    private Integer id;

    private Boolean activo;

    private String uuidTimbreFiscalDigital;

    private Cultivo cultivo;

    private CicloAgricola ciclo;
}
