/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.pre.registro.pdf;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import java.math.RoundingMode;
import java.util.Objects;
import mx.gob.segalmex.granos.core.contrato.pdf.PdfConstants;
import mx.gob.segalmex.granos.core.contrato.pdf.PdfHelper;
import mx.gob.segalmex.granos.core.contrato.pdf.SolicitudGobMxPdfHelper;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoPrecioEnum;
import mx.gob.segalmex.pluss.modelo.productor.CoberturaSeleccionada;
import mx.gob.segalmex.pluss.modelo.productor.PreRegistroProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioPreRegistro;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author jurgen
 */
public class MaizPreRegistroPdfHelper extends AbstractPreRegistroPdfHelper {

    @Override
    public void generaContenido(PreRegistroProductor inscripcion, Document d, PdfWriter writer) throws DocumentException {
        PdfPTable contenido = generaTablaContenido();
        agregaDatosPrograma(contenido, inscripcion);
        agregaDatosProductor(contenido, inscripcion);
        agregaContratos(contenido, inscripcion);
        agregaDatosPredios(contenido, inscripcion);
        agregaCoberturas(contenido, inscripcion);
        // agregaFechaCancelacion(contenido, inscripcion);
        agregaDatosBeneficiario(contenido, inscripcion);
        agregaTerminos(contenido, inscripcion);
        PdfContentByte contentByte = writer.getDirectContent();
        SolicitudGobMxPdfHelper.writeColumns(d, contentByte, contenido, false);
    }

    protected void agregaDatosProductor(PdfPTable contenido, PreRegistroProductor inscripcion) {
        contenido.addCell(generaCeldaEncabezado("Datos del productor"));
        contenido.addCell(PdfHelper.generaCeldaVacia(1));

        PdfPTable seccion = generaTabla(1, TAMANO_CONTENIDO_DOCUMENTO);
        seccion.addCell(generaCeldaEncabezado("Nombre del productor"));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, construyeNombreCompleto(inscripcion), false, false));
        contenido.addCell(seccion);

        seccion = generaTabla(4, TAMANO_CONTENIDO_DOCUMENTO);
        seccion.addCell(generaCeldaEncabezado("CURP"));
        seccion.addCell(generaCeldaEncabezado("RFC"));
        seccion.addCell(generaCeldaEncabezado("Teléfono"));
        seccion.addCell(generaCeldaEncabezado("Correo electrónico"));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, inscripcion.getCurp(), false, false));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, inscripcion.getRfc(), false, false));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, inscripcion.getTelefono(), false, false));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, inscripcion.getCorreoElectronico(), false, false));
        contenido.addCell(seccion);
        contenido.addCell(PdfHelper.generaCeldaVacia(1));
    }

    protected void agregaContratos(PdfPTable contenido, PreRegistroProductor pre) {
        if (pre.getCantidadContratos() > 0) {
            contenido.addCell(generaCeldaEncabezado("Contratos del productor"));
            contenido.addCell(PdfHelper.generaCeldaVacia(1));
            PdfPTable seccion = generaTabla(2, TAMANO_CONTENIDO_DOCUMENTO);
            seccion.addCell(generaCeldaEncabezado("Cantidad"));
            seccion.addCell(generaCeldaEncabezado("Tipo de precio"));
            if (pre.getPrecioAbierto() > 0) {
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, String.valueOf(pre.getPrecioAbierto()), false, false));
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, TipoPrecioEnum.PRECIO_ABIERTO_DOLARES.getNombre(), false, false));
            }
            if (pre.getPrecioCerradoDolares() > 0) {
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, String.valueOf(pre.getPrecioCerradoDolares()), false, false));
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, TipoPrecioEnum.PRECIO_CERRADO_DOLARES.getNombre(), false, false));
            }
            if (pre.getPrecioCerradoPesos() > 0) {
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, String.valueOf(pre.getPrecioCerradoPesos()), false, false));
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, TipoPrecioEnum.PRECIO_CERRADO_PESOS.getNombre(), false, false));
            }
            if (pre.getPrecioReferencia() > 0) {
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, String.valueOf(pre.getPrecioReferencia()), false, false));
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, TipoPrecioEnum.PRECIO_REFERENCIA.getNombre(), false, false));
            }
            if (pre.getPrecioAbiertoPesos() > 0) {
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, String.valueOf(pre.getPrecioAbiertoPesos()), false, false));
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, TipoPrecioEnum.PRECIO_ABIERTO_PESOS.getNombre(), false, false));
            }
            contenido.addCell(seccion);
            contenido.addCell(PdfHelper.generaCeldaVacia(1));
        }
    }

    protected void agregaDatosPredios(PdfPTable contenido, PreRegistroProductor inscripcion) {
        contenido.addCell(generaCeldaEncabezado("Información de predios"));
        contenido.addCell(PdfHelper.generaCeldaVacia(1));
        PdfPTable seccion = generaTabla(8, TAMANO_CONTENIDO_DOCUMENTO);
        seccion.addCell(generaCeldaEncabezado("Tipo de cultivo"));
        seccion.addCell(generaCeldaEncabezado("Tipo de posesión"));
        seccion.addCell(generaCeldaEncabezado("Régimen Hídrico"));
        seccion.addCell(generaCeldaEncabezado("Volumen (toneladas)"));
        seccion.addCell(generaCeldaEncabezado("Superficie (hectáreas)"));
        seccion.addCell(generaCeldaEncabezado("Rendimiento (t/ha)"));
        seccion.addCell(generaCeldaEncabezado("Estado"));
        seccion.addCell(generaCeldaEncabezado("Municipio"));

        for (PredioPreRegistro p : inscripcion.getPredios()) {
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, p.getTipoCultivo().getNombre(), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, p.getTipoPosesion().getNombre(), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, p.getRegimenHidrico().getNombre(), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, getFormatoNumero(p.getVolumen()), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, getFormatoNumero(p.getSuperficie()), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY,
                    getFormatoNumero(p.getVolumen().divide(p.getSuperficie(), 3, RoundingMode.HALF_UP)), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, p.getEstado().getNombre(), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, p.getMunicipio().getNombre(), false, false));
        }

        contenido.addCell(seccion);
        contenido.addCell(PdfHelper.generaCeldaVacia(1));
    }

    protected void agregaCoberturas(PdfPTable contenido, PreRegistroProductor pre) {
        contenido.addCell(generaCeldaEncabezado("Instrumento de Administración de riesgos (IAR)"));
        contenido.addCell(PdfHelper.generaCeldaVacia(1));
        if (Objects.nonNull(pre.getCoberturas()) && !pre.getCoberturas().isEmpty()) {
            PdfPTable seccion = generaTabla(3, TAMANO_CONTENIDO_DOCUMENTO);
            seccion.addCell(generaCeldaEncabezado("Tipo de IAR"));
            seccion.addCell(generaCeldaEncabezado("Cantidad"));
            seccion.addCell(generaCeldaEncabezado("Correduría"));
            for (CoberturaSeleccionada cs : pre.getCoberturas()) {
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, cs.getTipoIar().getNombre(), false, false));
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, String.valueOf(cs.getCantidad()), false, false));
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, cs.getEntidadCobertura().getNombre(), false, false));
            }
            contenido.addCell(seccion);
            contenido.addCell(PdfHelper.generaCeldaVacia(1));
        }
    }

    protected void agregaTerminos(PdfPTable contenido, PreRegistroProductor inscripcion) {
        PdfPTable seccion = generaTablaContenido();

        seccion.addCell(generaCeldaEncabezado("Términos y condiciones"));

        PdfPCell legales = null;

        switch (inscripcion.getCiclo().getClave()) {
            case "oi-2022":
                legales = PdfHelper.creaCeldaAviso(
                        "Con el presente pre registro acepto que, SEGALMEX no está obligado a brindarme el "
                        + "incentivo, pues es solo un requisito. \n"
                        + "Ante el Programa Precios de Garantía " + inscripcion.getCiclo().getNombre() + " deberé cumplir con los criterios "
                        + "de elegibilidad, los requisitos y los pasos de la mecánica operativa, incluyendo la adquisición de un "
                        + "instrumento de administración de riesgo, como una cobertura, de forma obligatoria.\n"
                        + "Finalmente, la información proporcionada la he verificado y confirmado, ya que en caso de que exista algún "
                        + "error en la CURP, RFC, nombre, tipo de cultivo, superficie o volumen no podrá ser modificada en el registro.\n"
                        + "Los datos personales recabados serán protegidos, incorporados y tratados en el marco de la Ley General de "
                        + "Protección de Documentos Personales en Posesión de Sujetos Obligados, con fundamento en los "
                        + "artículos 3,27 y 28. \n\n"
                        + "Este programa es público, ajeno a cualquier partido político. Queda prohibido el uso para "
                        + "fines distintos a los establecidos en el programa",
                        PdfConstants.BACKGROUND_GOBMX_WHITE,
                        PdfConstants.BACKGROUND_GOBMX_DARKGRAY);
                legales.setHorizontalAlignment(PdfPCell.ALIGN_JUSTIFIED);
                break;
            default:

                legales = PdfHelper.creaCeldaAviso(
                        "El presente pre registro es el inicio de un proceso que se deberá "
                        + "realizar para participar en el Programa Precios de Garantía a Productos Alimentarios "
                        + "Básicos, en " + inscripcion.getCultivo().getNombre().toLowerCase() + " comercializado por medianos productores, para el Ciclo "
                        + inscripcion.getCiclo().getNombre() + ". Este documento no obliga a SEGALMEX a brindar "
                        + "el incentivo a productores que respalda este, a menos que cada uno cumpla con el "
                        + "registro firmado, los requisitos y la normativa establecida en la mecánica "
                        + "operativa y en las Reglas de Operación 2020, publicadas en el DOF el 24 de febrero "
                        + "del 2020.\n\n"
                        + "Los datos recabados serán protegidos, incorporados y tratados en el marco de la Ley "
                        + "General de Protección de Documentos Personales en Posesión de Sujetos Obligados, con "
                        + "fundamento en los artículos 3, 27 y 28.", PdfConstants.BACKGROUND_GOBMX_WHITE,
                        PdfConstants.BACKGROUND_GOBMX_DARKGRAY);
                legales.setHorizontalAlignment(PdfPCell.ALIGN_JUSTIFIED);
        }

        seccion.addCell(legales);
        contenido.addCell(seccion);
        contenido.addCell(PdfHelper.generaCeldaVacia(1));
    }
}
