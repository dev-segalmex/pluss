/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.inscripcion;

import java.util.List;
import java.util.Map;
import mx.gob.segalmex.common.core.validador.ValidadorRfcHelper;
import mx.gob.segalmex.granos.core.rfc.busqueda.BuscadorRfcExcepcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.Inscripcion;
import mx.gob.segalmex.pluss.modelo.productor.EmpresaAsociada;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Component("modificadorInscripcionSociedadRfc")
public class ModificadorInscripcionSociedadRfc implements ModificadorInscripcion {

    @Autowired
    private BuscadorRfcExcepcion buscadorRfcExcepcion;

    @Override
    public void modifica(Inscripcion inscripcion, Map<String, DatoCapturado> datos) {
        InscripcionProductor ip = (InscripcionProductor) inscripcion;
        for (EmpresaAsociada ea : ip.getEmpresasActivas()) {
            if (!ea.isCorregida()) {
                continue;
            }
            List<String> excepciones = buscadorRfcExcepcion.getExcepciones(ea.getRfc(), true);
            ValidadorRfcHelper.valida(ea.getRfc(), excepciones, ea.getRfc() + " de la sociedad");
        }
    }

}
