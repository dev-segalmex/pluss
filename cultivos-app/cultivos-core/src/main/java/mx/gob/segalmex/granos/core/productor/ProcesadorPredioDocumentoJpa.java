/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.JAXBException;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.archivos.BuscadorArchivoRelacionado;
import mx.gob.segalmex.common.core.archivos.ManejadorArchivo;
import mx.gob.segalmex.common.core.validador.ValidadorDocumento;
import mx.gob.segalmex.common.core.factura.CfdiFactory;
import mx.gob.segalmex.common.core.factura.CfdiParser;
import mx.gob.segalmex.common.core.factura.modelo.CfdiComprobante;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.pluss.modelo.archivos.ArchivoRelacionado;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorPredioDocumento;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioDocumento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author oscar
 */
@Service("procesadorPredioDocumento")
@Slf4j
public class ProcesadorPredioDocumentoJpa implements ProcesadorPredioDocumento {

    /**
     * La clave que contiene en su tipo un archivo relacionado para permiso de siembra xml.
     */
    private static final String CLAVE_SIEMBRA = "permiso-siembra-xml";

    /**
     * La clave que contiene en su tipo un archivo relacionado para pago riego xml.
     */
    private static final String CLAVE_RIEGO = "pago-riego-xml";

    private static final String SIN_VALOR = "--";

    @Autowired
    private BuscadorArchivoRelacionado buscadorArchivoRelacionado;

    @Autowired
    private CfdiParser cfdiParser;

    @Autowired
    private ManejadorArchivo manejadorArchivo;

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private BuscadorPredioDocumento buscadorPredioDocumento;

    @Autowired
    @Qualifier("validadorDocumentoSat")
    private ValidadorDocumento validadorDocumentoSat;

    @Autowired
    @Qualifier("validadorDocumentoDuplicado")
    private ValidadorDocumento validadorDocumentoDuplicado;

    @Transactional
    @Override
    public List<PredioDocumento> procesa(InscripcionProductor inscripcion) throws IOException {
        LOGGER.info("Procesando predios documento...");
        List<ArchivoRelacionado> archivosRelacionados = buscadorArchivoRelacionado.
                busca(inscripcion.getClass(), inscripcion.getUuid());
        List<ArchivoRelacionado> archivosXml = new ArrayList<>();
        archivosXml.addAll(getArchivos(archivosRelacionados, CLAVE_SIEMBRA));
        archivosXml.addAll(getArchivos(archivosRelacionados, CLAVE_RIEGO));
        List<PredioDocumento> prediosDocumento = new ArrayList<>();

        LOGGER.info("Predios documentos por generar: {}", archivosXml.size());
        for (ArchivoRelacionado ar : archivosXml) {
            PredioDocumento pd = getInstance(ar);
            pd.setInscripcionProductor(inscripcion);
            registroEntidad.guarda(pd);
            prediosDocumento.add(pd);
        }

        return prediosDocumento;
    }

    @Override
    @Transactional
    public void verifica(PredioDocumento pd) {
        Boolean valido = validadorDocumentoSat.valida(pd);
        pd.setValidacionSat(valido);
        validadorDocumentoDuplicado.valida(pd);
        pd.setMonitoreable(SegalmexDateUtils.format(Calendar.getInstance(), "yyyy-MM-dd"));
        registroEntidad.actualiza(pd);
    }

    private List<ArchivoRelacionado> getArchivos(List<ArchivoRelacionado> archivosRelacionados, String tipo) {
        List<ArchivoRelacionado> archivos = new ArrayList<>();
        archivosRelacionados.stream().filter((ar) -> (ar.getTipo().contains(tipo))).forEachOrdered((ar) -> {
            archivos.add(ar);
        });
        return archivos;
    }

    private PredioDocumento getInstance(ArchivoRelacionado ar) throws IOException {
        PredioDocumento pd = getPredioEmpty(ar);
        try {
            CfdiComprobante cc = cfdiParser.parse(manejadorArchivo.obtenStream(ar.getArchivo()));
            pd.setRfcEmisor(Objects.nonNull(cc.getEmisor().getRfc()) ? cc.getEmisor().getRfc() : SIN_VALOR);
            pd.setRfcReceptor(Objects.nonNull(cc.getReceptor().getRfc()) ? cc.getReceptor().getRfc() : SIN_VALOR);
            pd.setTotal(Objects.nonNull(cc.getTotal()) ? cc.getTotal() : BigDecimal.ZERO);
            if (Objects.nonNull(cc.getComplemento()) && Objects.nonNull(cc.getEmisor().getRfc())) {
                pd.setUuidTimbreFiscalDigital(CfdiFactory.formatoCfdi(cc.getComplemento().
                        getTimbreFiscalDigital().getUuid()));
            } else {
                pd.setUuidTimbreFiscalDigital("--");
                pd.setMonitoreable(SegalmexDateUtils.format(Calendar.getInstance(), "yyyy-MM-dd"));
            }

            return pd;
        } catch (JAXBException ouch) {
            LOGGER.error("Ocurrió un error al convertir el xml, se genera vacío.");
            pd.setMonitoreable(SegalmexDateUtils.format(Calendar.getInstance(), "yyyy-MM-dd"));
            return pd;
        } catch (RuntimeException re) {
            LOGGER.error("Ocurrió un error al leer el uuid, se genera vacío.");
            pd.setMonitoreable(SegalmexDateUtils.format(Calendar.getInstance(), "yyyy-MM-dd"));
            pd.setRfcEmisor(SIN_VALOR);
            pd.setRfcReceptor(SIN_VALOR);
            pd.setTotal(BigDecimal.ZERO);
            pd.setUuidTimbreFiscalDigital("--");
            return pd;
        }
    }

    private PredioDocumento getPredioEmpty(ArchivoRelacionado ar) {
        PredioDocumento pd = new PredioDocumento();
        pd.setRfcEmisor(SIN_VALOR);
        pd.setRfcReceptor(SIN_VALOR);
        pd.setUuidTimbreFiscalDigital(SIN_VALOR);
        pd.setNumero(ar.getTipo().split("-")[0]);
        pd.setTipo(ar.getTipo().contains(CLAVE_RIEGO) ? "riego" : "siembra");
        pd.setTotal(BigDecimal.ZERO);
        return pd;
    }

    @Override
    @Transactional
    public void desactiva(InscripcionProductor inscripcion) {
        LOGGER.info("Desactivando predios documento del registro : {}", inscripcion.getFolio());
        List<PredioDocumento> prediosDocumentos = buscadorPredioDocumento.busca(inscripcion);
        desactiva(prediosDocumentos);
    }

    @Override
    @Transactional
    public void desactiva(List<PredioDocumento> prediosDocumento) {
        for (PredioDocumento pd : prediosDocumento) {
            pd.setActivo(false);
            pd.setMonitoreable(SegalmexDateUtils.format(Calendar.getInstance(), "yyyy-MM-dd"));
            registroEntidad.actualiza(pd);
        }
    }

}
