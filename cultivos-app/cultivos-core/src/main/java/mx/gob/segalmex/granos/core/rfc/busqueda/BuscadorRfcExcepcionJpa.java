/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.rfc.busqueda;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.common.core.validador.ValidadorRfcHelper;
import mx.gob.segalmex.pluss.modelo.productor.RfcExcepcion;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author oscar
 */
@Repository("buscadorRfcExcepcion")
@Slf4j
public class BuscadorRfcExcepcionJpa implements BuscadorRfcExcepcion {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<RfcExcepcion> busca(ParametrosRfcExcepcion parametros) {
        StringBuilder sb = new StringBuilder("SELECT re FROM RfcExcepcion re ");
        Map<String, Object> params = getQuery(sb, parametros);

        TypedQuery<RfcExcepcion> q
                = entityManager.createQuery(sb.toString(), RfcExcepcion.class);
        QueryUtils.setParametros(q, params);

        long inicio = System.currentTimeMillis();
        List<RfcExcepcion> rfcs = q.getResultList();
        LOGGER.debug("Se encontraron {} resultados en {} ms.", rfcs.size(),
                System.currentTimeMillis() - inicio);

        return rfcs;
    }

    @Override
    public RfcExcepcion buscaElemento(Integer id) {
        Objects.requireNonNull(id, "El id no puede ser nulo.");
        ParametrosRfcExcepcion parametros = new ParametrosRfcExcepcion();
        parametros.setActivo(Boolean.TRUE);
        parametros.setId(id);

        List<RfcExcepcion> resultados = busca(parametros);
        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontraron resultados.", 1);
        }

        return resultados.get(0);
    }

    @Override
    public RfcExcepcion buscaElemento(String rfc) {
        Objects.requireNonNull(rfc, "El rfc no puede ser nulo.");
        ParametrosRfcExcepcion parametros = new ParametrosRfcExcepcion();
        parametros.setActivo(Boolean.TRUE);
        parametros.setRfc(rfc);

        List<RfcExcepcion> resultados = busca(parametros);
        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontraron resultados.", 1);
        }

        return resultados.get(0);
    }

    @Override
    public List<String> buscaRfc(ParametrosRfcExcepcion parametros) {
        StringBuilder sb = new StringBuilder("SELECT re.rfc FROM RfcExcepcion re ");
        Map<String, Object> params = getQuery(sb, parametros);
        TypedQuery<String> q
                = entityManager.createQuery(sb.toString(), String.class);
        QueryUtils.setParametros(q, params);

        long inicio = System.currentTimeMillis();
        List<String> rfcs = q.getResultList();

        LOGGER.debug("Se encontraron {} resultados en {} ms.", rfcs.size(),
                System.currentTimeMillis() - inicio);

        return rfcs;
    }

    @Override
    public List<String> getExcepciones(String rfc, boolean genericos) {
        Objects.requireNonNull(rfc, "El rfc no puede ser nulo.");
        ParametrosRfcExcepcion parametros = new ParametrosRfcExcepcion();
        parametros.setActivo(Boolean.TRUE);
        parametros.setRfcAproximado(rfc);
        List<String> excepciones = buscaRfc(parametros);
        if (genericos) {
            excepciones.addAll(ValidadorRfcHelper.GENERICOS);
        }
        return excepciones;
    }

    private Map<String, Object> getQuery(StringBuilder sb, ParametrosRfcExcepcion parametros) {

        Map<String, Object> params = new HashMap<>();
        boolean first = true;

        if (Objects.nonNull(parametros.getId())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "re.id", "id", parametros.getId(), params);
        }

        if (Objects.nonNull(parametros.getRfcAproximado())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append(" re.rfc like :rfc ");
            String rfc = parametros.getRfcAproximado().substring(0, parametros.getRfcAproximado().length() - 1);
            params.put("rfc", rfc + "%");
        }

        if (Objects.nonNull(parametros.getRfc())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "re.rfc", "rfc", parametros.getRfc(), params);
        }

        if (Objects.nonNull(parametros.getUsuarioRegistra())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "re.usuarioRegistra", "usuarioRegistra", parametros.getUsuarioRegistra(), params);
        }

        if (Objects.nonNull(parametros.getActivo()) && parametros.getActivo()) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "re.activo", "activo", parametros.getActivo(), params);
        }

        if (Objects.nonNull(parametros.getUsuarioBaja())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "re.usuarioBaja", "usuarioBaja", parametros.getUsuarioBaja(), params);
        }

        sb.append("ORDER BY re.id ");
        LOGGER.info("Ejecutando el query: {}", sb);
        return params;
    }

}
