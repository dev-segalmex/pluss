/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.folio;

import mx.gob.segalmex.pluss.modelo.secuencias.TipoFolioEnum;

/**
 *
 * @author oscar
 */
public interface GeneradorFolio {
    /**
     *
     * @param tipo el tipo de folio que se desea
     * @return el el siguiente valor del tipo de folio deseado.
     */
    String genera(TipoFolioEnum tipo, int padding);
}
