/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.datos.catalogos;

import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoEmpresa;

/**
 *
 * @author ismael
 */
public class CargadorDatosTipoEmpresa extends AbstractCargadorDatosCatalogo<TipoEmpresa> {

    @Override
    public TipoEmpresa getInstance(String[] elementos) {
        TipoEmpresa tipo = new TipoEmpresa();
        tipo.setClave(elementos[0]);
        tipo.setNombre(elementos[1]);
        tipo.setActivo(true);
        tipo.setOrden(Integer.parseInt(elementos[2]));

        return tipo;
    }

}