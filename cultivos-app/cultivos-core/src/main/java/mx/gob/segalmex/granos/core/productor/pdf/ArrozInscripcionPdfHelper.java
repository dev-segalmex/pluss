/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.pdf;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.granos.core.contrato.pdf.PdfConstants;
import mx.gob.segalmex.granos.core.contrato.pdf.PdfHelper;
import mx.gob.segalmex.granos.core.contrato.pdf.SolicitudGobMxPdfHelper;
import mx.gob.segalmex.pluss.modelo.productor.EstatusPredioProductorEnum;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionMolino;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author oscar
 */
@Slf4j
public class ArrozInscripcionPdfHelper extends AbstractInscripcionPdfHelper {

    @Override
    public void generaContenido(InscripcionProductor inscripcion, Document d, PdfWriter writer) throws DocumentException {
        PdfPTable contenido = generaTablaContenido();
        LOGGER.info("Creando pdf productor Arroz.");
        agregaDatosPrograma(contenido, inscripcion);
        agregaDatosVentanilla(contenido, inscripcion);
        agregaDatosBodega(contenido, inscripcion);
        agregaDatosProductor(contenido, inscripcion);
        agregaDatosContacto(contenido, inscripcion);
        agregaDomicilioProductor(contenido, inscripcion);
        agregaDatosBancarios(contenido, inscripcion);
        agregaPredios(contenido, inscripcion);
        agregaSociedades(contenido, inscripcion);
        agregaBeneficiario(contenido, inscripcion);
        agregaTerminos(contenido, inscripcion);
        agregaFirmas(contenido, inscripcion);

        PdfContentByte contentByte = writer.getDirectContent();
        SolicitudGobMxPdfHelper.writeColumns(d, contentByte, contenido, false);
    }

    protected void agregaDatosBodega(PdfPTable contenido, InscripcionProductor inscripcion) {

        if (Objects.nonNull(inscripcion.getMolinos())) {
            if (inscripcion.getMolinos().size() > 1) {
                contenido.addCell(generaCeldaEncabezado("Datos del molino(s) o comprador"));

                PdfPTable seccion = generaTabla(5, TAMANO_CONTENIDO_DOCUMENTO);
                seccion.addCell(generaCeldaEncabezado("Nombre de la bodega"));
                seccion.addCell(generaCeldaEncabezado("RFC"));
                seccion.addCell(generaCeldaEncabezado("Entidad"));
                seccion.addCell(generaCeldaEncabezado("Municipio"));
                seccion.addCell(generaCeldaEncabezado("Localidad"));
                for (InscripcionMolino a : inscripcion.getMolinos()) {
                    seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, a.getMolino().getEmpresa().getNombre(), false, false));
                    seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, a.getMolino().getEmpresa().getRfc(), false, false));
                    seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, a.getMolino().getEstado().getNombre(), false, false));
                    seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, a.getMolino().getMunicipio().getNombre(), false, false));
                    seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, a.getMolino().getLocalidad(), false, false));
                }
                contenido.addCell(seccion);
                contenido.addCell(PdfHelper.generaCeldaVacia(1));
            } else {
                contenido.addCell(generaCeldaEncabezado("Datos del molino(s) o comprador"));

                PdfPTable seccion = generaTabla(1, TAMANO_CONTENIDO_DOCUMENTO);
                seccion.addCell(generaCeldaSimple("Nombre de la bodega", inscripcion.getBodega().getEmpresa().getNombre(), false, false));
                contenido.addCell(seccion);

                seccion = generaTabla(2, TAMANO_CONTENIDO_DOCUMENTO);
                seccion.addCell(generaCeldaSimple("RFC", inscripcion.getBodega().getEmpresa().getRfc(), false, false));
                seccion.addCell(generaCeldaSimple("Entidad", inscripcion.getBodega().getEstado().getNombre(), false, false));
                seccion.addCell(generaCeldaSimple("Municipio", inscripcion.getBodega().getMunicipio().getNombre(), false, false));
                seccion.addCell(generaCeldaSimple("Localidad", inscripcion.getBodega().getLocalidad(), false, false));
                contenido.addCell(seccion);
                contenido.addCell(PdfHelper.generaCeldaVacia(1));
            }
        } else {
            contenido.addCell(generaCeldaEncabezado("Datos de la bodega"));

            PdfPTable seccion = generaTabla(1, TAMANO_CONTENIDO_DOCUMENTO);
            seccion.addCell(generaCeldaSimple("Nombre de la bodega", inscripcion.getBodega().getEmpresa().getNombre(), false, false));
            contenido.addCell(seccion);

            seccion = generaTabla(2, TAMANO_CONTENIDO_DOCUMENTO);
            seccion.addCell(generaCeldaSimple("RFC", inscripcion.getBodega().getEmpresa().getRfc(), false, false));
            seccion.addCell(generaCeldaSimple("Entidad", inscripcion.getBodega().getEstado().getNombre(), false, false));
            seccion.addCell(generaCeldaSimple("Municipio", inscripcion.getBodega().getMunicipio().getNombre(), false, false));
            seccion.addCell(generaCeldaSimple("Localidad", inscripcion.getBodega().getLocalidad(), false, false));
            contenido.addCell(seccion);
            contenido.addCell(PdfHelper.generaCeldaVacia(1));
        }
    }

    @Override
    public void agregaPredios(PdfPTable contenido, InscripcionProductor inscripcion) {
        contenido.addCell(generaCeldaEncabezado("Predios del productor"));

        PdfPTable seccion = generaTabla(10, TAMANO_CONTENIDO_DOCUMENTO);
        seccion.addCell(generaCeldaEncabezado("Folio"));
        seccion.addCell(generaCeldaEncabezado("Cultivo"));
        seccion.addCell(generaCeldaEncabezado("Posesión y acreditación"));
        seccion.addCell(generaCeldaEncabezado("Régimen"));
        seccion.addCell(generaCeldaEncabezado("Vol (t)"));
        seccion.addCell(generaCeldaEncabezado("Sup (ha)"));
        seccion.addCell(generaCeldaEncabezado("Rnd (t/ha)"));
        seccion.addCell(generaCeldaEncabezado("Municipo y Estado"));
        seccion.addCell(generaCeldaEncabezado("Localidad"));
        seccion.addCell(generaCeldaEncabezado("Ubicación"));

        for (PredioProductor p : inscripcion.getPrediosActivos()) {
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, p.getFolio(), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, p.getTipoCultivo().getNombre(), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, p.getTipoPosesion().getNombre() + ",\n" + p.getTipoDocumentoPosesion().getNombre(), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, p.getRegimenHidrico().getNombre(), false, false));

            String volumen = "";
            String rendimiento = "";
            if (p.getSolicitado().equals(EstatusPredioProductorEnum.SOLICITADO.getClave())) {
                volumen = getFormatoNumero(p.getVolumen())
                        + "\n(" + getFormatoNumero(p.getVolumenSolicitado()) + ")";
                rendimiento = getFormatoNumero(p.getRendimiento())
                        + "\n(" + getFormatoNumero(p.getRendimientoSolicitado()) + ")";
            } else {
                volumen = getFormatoNumero(p.getVolumen());
                rendimiento = getFormatoNumero(p.getRendimiento());
            }

            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, volumen, false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, getFormatoNumero(p.getSuperficie()), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, rendimiento, false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, p.getMunicipio().getNombre() + ", " + p.getEstado().getNombre(), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, p.getLocalidad(), false, false));
            if (Objects.nonNull(p.getLatitud())) {
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, "[" + getFormatoNumero(p.getLatitud())
                        + ", " + getFormatoNumero(p.getLongitud()) + "]", false, false));
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("[");
                sb.append(getFormatoNumero(p.getLatitud1()));
                sb.append(", ");
                sb.append(getFormatoNumero(p.getLongitud1()));
                sb.append("],\n[");
                sb.append(getFormatoNumero(p.getLatitud2()));
                sb.append(", ");
                sb.append(getFormatoNumero(p.getLongitud2()));
                sb.append("],\n[");
                sb.append(getFormatoNumero(p.getLatitud3()));
                sb.append(", ");
                sb.append(getFormatoNumero(p.getLongitud3()));
                sb.append("],\n[");
                sb.append(getFormatoNumero(p.getLatitud4()));
                sb.append(", ");
                sb.append(getFormatoNumero(p.getLongitud4()));
                sb.append("]");
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, sb.toString(), false, false));
            }
        }

        contenido.addCell(seccion);
        contenido.addCell(PdfHelper.generaCeldaVacia(1));
    }

    @Override
    public void agregaTerminos(PdfPTable contenido, InscripcionProductor inscripcion) {
        PdfPTable seccion = generaTablaContenido();

        seccion.addCell(generaCeldaEncabezado("Términos y condiciones"));
        PdfPCell legales = null;
        switch (inscripcion.getCiclo().getClave()) {
            case "oi-2022":
                legales = PdfHelper.creaCeldaAviso(
                        "A través del presente registro, autorizo que los predios referidos e información productiva "
                        + "y personal, sean considerados en el Programa de Precios de Garantía a Productos Alimentarios "
                        + "Básicos. Me obligo a proporcionar la información y documentación que me sea requerida "
                        + "por la SADER-SEGALMEX y a notificar cualquier cambio que sufra la información "
                        + "proporcionada. A la vez, manifiesto bajo protesta de, decir la verdad que los datos "
                        + "contenidos en este registro son ciertos. Acepto la responsabilidad, en la veracidad "
                        + "de la información proporcionada a SEGALMEX. En caso de incumplimiento total o parcial, así "
                        + "como de SIMULACIÓN, me comprometo a devolver sin reserva alguna el incentivo recibido y "
                        + "a aceptar la sanción administrativa que conforme a derecho proceda. Soy consciente que, en "
                        + "caso de alteración de documentación y firmas, mi registro será cancelado. También me "
                        + "comprometo a que en caso de que me realicen algún pago en DEMASÍA, lo reintegraré completamente, "
                        + "a través de un depósito bancario.\n\n Estoy de acuerdo en beneficiarme con el precio de "
                        + "Garantía del ciclo vigente, publicados en el Diario Oficial de la federación 2022 y la "
                        + "mecánica operativa de Arroz del ciclo correspondiente. Envío mi registro que ampara el "
                        + "total de toneladas de arroz (grano o semilla certificada) que obtendré en la cosecha del "
                        + "ciclo mencionado, y estarán reflejadas en el sistema SEGALMEX. Lo anterior también "
                        + "aplica para semilla de arroz certificada por el SNICS. Los datos personales recabados "
                        + "serán protegidos, incorporados y tratados en el marco de la Ley General de Protección "
                        + "de Documentos Personales en Posesión de Sujetos Obligados, con fundamento en los "
                        + "artículos 3, 27 y 28.", PdfConstants.BACKGROUND_GOBMX_WHITE,
                        PdfConstants.BACKGROUND_GOBMX_DARKGRAY);
                break;
            case "pv-2021":
                legales = PdfHelper.creaCeldaAviso(
                        "A través del presente registro, autorizo que los predios referidos e información productiva y personal, sean considerados en el "
                        + "Programa de Precios de Garantía a Productos Alimentarios Básicos. Me obligo a proporcionar la información y documentación que me "
                        + "sea requerida por la SADER-SEGALMEX y a notificar cualquier cambio que sufra la información proporcionada. A la vez, manifiesto bajo "
                        + "protesta de, decir la verdad que los datos contenidos en este registro son ciertos. Acepto la responsabilidad, en la veracidad de la "
                        + "información proporcionada a SEGALMEX. En caso de incumplimiento total o parcial, así como de SIMULACIÓN, me comprometo a devolver "
                        + "sin reserva alguna el incentivo recibido y a aceptar la sanción administrativa que conforme a derecho proceda. Soy consciente que, "
                        + "en caso de alteración de documentación y firmas, mi registro será cancelado. También me comprometo a que en caso de que me realicen "
                        + "algún pago en DEMASÍA, lo reintegraré completamente, a través de un depósito bancario. Estoy de acuerdo en beneficiarme con el "
                        + "precio de Garantía del ciclo agrícola vigente, publicados en el Diario Oficial de la federación 2022 y la mecánica operativa de "
                        + "Arroz del ciclo correspondiente. Envío mi registro que ampara el total de toneladas de arroz (grano o semilla certificada) que "
                        + "obtendré en la cosecha del ciclo mencionado, y estarán reflejadas en el sistema SEGALMEX. Existen precios de garantía diferenciados "
                        + "de acuerdo a la superficie (pequeños y medianos). Al productor pequeño, de hasta 8 hectáreas, se le apoyarán máximo 80 ton. Al "
                        + "productor mediano se le incentivarán máximo 300 toneladas. En medianos, de 1 a 120 toneladas recibiré el incentivo completo (100%), "
                        + "y de 180 toneladas adicionales podré recibir el 50% del incentivo. Lo anterior también aplica para semilla de arroz certificada por el SNICS.\n\n"
                        + "Los datos personales recabados serán protegidos, incorporados y tratados en el marco de la Ley General de Protección de Documentos "
                        + "Personales en Posesión de Sujetos Obligados, con fundamento en los artículos 3, 27 y 28."
                        + "", PdfConstants.BACKGROUND_GOBMX_WHITE,
                        PdfConstants.BACKGROUND_GOBMX_DARKGRAY);
                break;
            default:
                legales = PdfHelper.creaCeldaAviso(
                        "A través del presente registro, autorizo que los predios referidos sean considerados en el Programa de Precios de Garantía a Productos Alimentarios Básicos. "
                        + "Me obligo a proporcionar la información y/o documentación que me sea requerida por la SADER-SEGALMEX y a notificar cualquier cambio que sufra la información proporcionada. "
                        + "A la vez, manifiesto bajo protesta de decir la verdad que los datos contenidos en este registro son ciertos y reales, por lo que acepto mi responsabilidad, "
                        + "en la veracidad de la información proporcionada a SEGALMEX. En caso de incumplimiento total o parcial, así como de SIMULACIÓN, me comprometo a devolver "
                        + "sin reserva alguna el incentivo recibido y a aceptar la sanción administrativa que conforme a derecho proceda. Soy consciente que, en caso de alteración de documentación "
                        + "y/o firmas, mi registro será cancelado. También me comprometo a que en caso de que me realicen algún pago en DEMASÍA, lo reintegraré completamente, a través de un depósito bancario. "
                        + "Estoy de acuerdo en beneficiarme con el Precio de Garantía del ciclo agrícola " + inscripcion.getCiclo().getNombre() + ", publicados en el Diario Oficial de la federación 2020 "
                        + "y la mecánica operativa de arroz " + inscripcion.getCiclo().getNombre() + ". Envío mi registro que ampara el total de toneladas de arroz que obtendré en la cosecha del ciclo mencionado, "
                        + "y estarán reflejadas en el sistema SEGALMEX, cuyo monto del incentivo es la diferencia entre el Precio de Garantía de $6,120 por tonelada y el precio de mercado establecido "
                        + "como referencia por SEGALMEX. Tengo en conocimiento que de hasta 120 toneladas recibiré el incentivo completo(100 %), y de 180 toneladas adicionales a las primeras 120 toneladas "
                        + "podré recibir el 50% del incentivo.\n\n"
                        + "Los datos personales recabados serán protegidos, incorporados y tratados en el marco de la Ley General de Protección de Documentos Personales en "
                        + "Posesión de Sujetos Obligados, con fundamento en los artículos 3, 27 y 28.",
                        PdfConstants.BACKGROUND_GOBMX_WHITE,
                        PdfConstants.BACKGROUND_GOBMX_DARKGRAY);
        }

        legales.setHorizontalAlignment(PdfPCell.ALIGN_JUSTIFIED);

        seccion.addCell(legales);

        contenido.addCell(seccion);
        contenido.addCell(PdfHelper.generaCeldaVacia(1));
    }

}
