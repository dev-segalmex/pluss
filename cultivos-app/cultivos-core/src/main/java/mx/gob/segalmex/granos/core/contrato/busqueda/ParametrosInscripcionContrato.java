/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato.busqueda;

import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.cultivos.core.inscripcion.busqueda.ParametrosInscripcion;

/**
 *
 * @author ismael
 */
@Getter
@Setter
public class ParametrosInscripcionContrato extends ParametrosInscripcion {

    private String numeroContrato;
}
