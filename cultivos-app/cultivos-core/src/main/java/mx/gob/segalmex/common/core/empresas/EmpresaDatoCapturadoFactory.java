/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.empresas;

import java.util.ArrayList;
import java.util.List;
import mx.gob.segalmex.pluss.modelo.empresas.DatosComercializacion;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroEmpresa;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroSucursal;
import mx.gob.segalmex.granos.core.inscripcion.DatoCapturadoFactory;
import mx.gob.segalmex.granos.core.inscripcion.InscripcionDatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.productor.Domicilio;

/**
 *
 * @author jurgen
 */
public class EmpresaDatoCapturadoFactory extends DatoCapturadoFactory {

    private static final String DATOS_VENTANILLA = "Datos de la ventanilla";

    private static final String DOMICILIO_FISCAL = "Domicilio fiscal";

    private static final String ENLACE_SEGALMEX = "Enlace con SEGALMEX";

    private static final String INFRAESTRUCTURA_EMPRESA = "Infraestructura de la empresa";

    private static final String DATOS_COMERCIALIZACION_COMPRA = "Datos de comercialización - Compra";

    private static final String DATOS_COMERCIALIZACION_VENTA = "Datos de comercialización - Venta";

    private static final String SUCURSALES = "Sucursales";

    private static final String ANEXOS = "Anexos";

    /**
     * Obtiene la lista de datos capturados de un registro de empresa.
     *
     * @param r el registro donde se obtienen los datos capturados.
     * @return la lista de datos capturados.
     */
    public static List<DatoCapturado> getInstance(RegistroEmpresa r) {
        InscripcionDatoCapturado idc = new InscripcionDatoCapturado();
        agregaDatosVentanilla(idc, r);
        agregaDomicilioFiscal(idc, r);
        agregaEnlaceSegalmex(idc, r);
        agregaInfraestructuraEmpresa(idc, r);
        agregaDatosComercializacion(idc, r);
        agregaSucursales(idc, r);
        agregaAnexos(idc, r);
        return idc.getDatos();
    }

    private static void agregaDatosVentanilla(InscripcionDatoCapturado idc, RegistroEmpresa registro) {
        idc.agrega(getInstance(registro, DatoCapturadoEmpresaEnum.NOMBRE_EMPRESA,
                registro.getNombre(), DATOS_VENTANILLA));
        idc.agrega(getInstance(registro, DatoCapturadoEmpresaEnum.TIPO_EMPRESA,
                registro.getTipoSucursal(), DATOS_VENTANILLA));
        idc.agrega(getInstance(registro, DatoCapturadoEmpresaEnum.NOMBRE_RESPONSABLE,
                registro.getNombreResponsable(), DATOS_VENTANILLA));
        idc.agrega(getInstance(registro, DatoCapturadoEmpresaEnum.TELEFONO_OFICINA,
                registro.getTelefonoOficina(), DATOS_VENTANILLA));
        idc.agrega(getInstance(registro, DatoCapturadoEmpresaEnum.EXTENSION_ADICIONAL,
                registro.getExtensionAdicional(), DATOS_VENTANILLA));
    }

    private static void agregaDomicilioFiscal(InscripcionDatoCapturado idc, RegistroEmpresa r) {
        Domicilio d = r.getDomicilio();
        idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.CALLE_EMPRESA,
                d.getCalle(), DOMICILIO_FISCAL));
        idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.NUMERO_EXT_EMPRESA,
                d.getNumeroExterior(), DOMICILIO_FISCAL));
        idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.NUMERO_INT_EMPRESA,
                d.getNumeroInterior(), DOMICILIO_FISCAL));
        idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.LOCALIDAD_EMPRESA,
                d.getLocalidad(), DOMICILIO_FISCAL));
        idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.CODIGO_POSTAL_EMPRESA,
                d.getCodigoPostal(), DOMICILIO_FISCAL));
        idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.ESTADO_EMPRESA,
                d.getEstado(), DOMICILIO_FISCAL));
        idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.MUNICIPIO_EMPRESA,
                d.getMunicipio(), DOMICILIO_FISCAL));
    }

    private static void agregaEnlaceSegalmex(InscripcionDatoCapturado idc, RegistroEmpresa r) {
        idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.NOMBRE_ENLACE,
                r.getNombreEnlace(), ENLACE_SEGALMEX));
        idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.CELULAR_ENLACE,
                r.getNumeroCelularEnlace(), ENLACE_SEGALMEX));
        idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.CORREO_ENLACE,
                r.getCorreoEnlace(), ENLACE_SEGALMEX));
    }

    private static void agregaInfraestructuraEmpresa(InscripcionDatoCapturado idc, RegistroEmpresa r) {
        idc.agrega(getInstanceBoolean(r, DatoCapturadoEmpresaEnum.EQUIPO_ANALISIS,
                String.valueOf(r.isExisteEquipoAnalisis()), INFRAESTRUCTURA_EMPRESA));
        idc.agrega(getInstanceBoolean(r, DatoCapturadoEmpresaEnum.APLICA_NORMAS,
                String.valueOf(r.isAplicaNormasCalidad()), INFRAESTRUCTURA_EMPRESA));
        idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.TIPO_POSESION,
                r.getTipoPosesion(), INFRAESTRUCTURA_EMPRESA));
        idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.TIPO_ALMACENAMIENTO,
                r.getTipoAlmacenamiento(), INFRAESTRUCTURA_EMPRESA));
        idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.LATITUD_EMPRESA,
                String.valueOf(r.getLatitud()), INFRAESTRUCTURA_EMPRESA));
        idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.LONGITUD_EMPRESA,
                String.valueOf(r.getLongitud()), INFRAESTRUCTURA_EMPRESA));
        idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.NUMERO_ALMACENAMIENTO,
                String.valueOf(r.getNumeroAlmacenes()), INFRAESTRUCTURA_EMPRESA));
        idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.CAPACIDAD_ALMACENAMIENTO,
                String.valueOf(r.getCapacidad()), INFRAESTRUCTURA_EMPRESA));
        idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.VOLUMEN_ALMACENAMIENTO,
                String.valueOf(r.getVolumenActual()), INFRAESTRUCTURA_EMPRESA));
        idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.SUPERFICIE_ALMACENAMIENTO,
                String.valueOf(r.getSuperficie()), INFRAESTRUCTURA_EMPRESA));
        idc.agrega(getInstanceBoolean(r, DatoCapturadoEmpresaEnum.APLICA_ALMACENADORA,
                String.valueOf(r.isHabilitaAlmacenadora()), INFRAESTRUCTURA_EMPRESA));
        idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.NOMBRE_ALMACENADORA,
                String.valueOf(r.getNombreAlmacenadora()), INFRAESTRUCTURA_EMPRESA));
    }

    private static void agregaDatosComercializacion(InscripcionDatoCapturado idc, RegistroEmpresa r) {
        List<DatosComercializacion> dcompra = getDatosPorTipo(r, "compra");
        int index = 1;
        for (DatosComercializacion dc : dcompra) {
            String suffix = "(" + index++ + ")";
            idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.ACTOR_COMPRA,
                    dc.getNombreActor(), dc.getOrden(), suffix, DATOS_COMERCIALIZACION_COMPRA));
            idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.ESTADO_COMPRA,
                    dc.getEstado(), dc.getOrden(), suffix, DATOS_COMERCIALIZACION_COMPRA));
            idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.MUNICIPIO_COMPRA,
                    dc.getMunicipio(), dc.getOrden(), suffix, DATOS_COMERCIALIZACION_COMPRA));
            idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.ANIO_COMPRA,
                    String.valueOf(dc.getAnio()), dc.getOrden(), suffix, DATOS_COMERCIALIZACION_COMPRA));
            idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.CANTIDAD_COMPRA,
                    String.valueOf(dc.getCantidad()), dc.getOrden(), suffix, DATOS_COMERCIALIZACION_COMPRA));
        }

        List<DatosComercializacion> dventa = getDatosPorTipo(r, "venta");
        index = 1;
        for (DatosComercializacion dc : dventa) {
            String suffix = "(" + index++ + ")";
            idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.ACTOR_VENTA,
                    dc.getNombreActor(), dc.getOrden(), suffix, DATOS_COMERCIALIZACION_VENTA));
            idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.ESTADO_VENTA,
                    dc.getEstado(), dc.getOrden(), suffix, DATOS_COMERCIALIZACION_VENTA));
            idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.MUNICIPIO_VENTA,
                    dc.getMunicipio(), dc.getOrden(), suffix, DATOS_COMERCIALIZACION_VENTA));
            idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.ANIO_VENTA,
                    String.valueOf(dc.getAnio()), dc.getOrden(), suffix, DATOS_COMERCIALIZACION_VENTA));
            idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.CANTIDAD_VENTA,
                    String.valueOf(dc.getCantidad()), dc.getOrden(), suffix, DATOS_COMERCIALIZACION_VENTA));
        }
    }

    private static List<DatosComercializacion> getDatosPorTipo(RegistroEmpresa r, String tipo) {
        List<DatosComercializacion> filtrados = new ArrayList<>();
        for (DatosComercializacion d : r.getDatosComercializacion()) {
            if (d.getTipo().equals(tipo)) {
                filtrados.add(d);
            }
        }
        return filtrados;
    }

    private static void agregaSucursales(InscripcionDatoCapturado idc, RegistroEmpresa r) {
        int index = 1;
        for (RegistroSucursal s : r.getSucursales()) {
            String suffix = "(" + index++ + ")";
            int orden = s.getOrden();
            idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.NOMBRE_SUCURSAL,
                    s.getNombre(), orden, suffix, SUCURSALES));

            Domicilio d = s.getDomicilio();
            idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.CALLE_SUCURSAL,
                    d.getCalle(), orden, suffix, SUCURSALES));
            idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.NUMERO_EXT_SUCURSAL,
                    d.getNumeroExterior(), orden, suffix, SUCURSALES));
            idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.NUMERO_INT_SUCURSAL,
                    d.getNumeroInterior(), orden, suffix, SUCURSALES));
            idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.LOCALIDAD_SUCURSAL,
                    d.getLocalidad(), orden, suffix, SUCURSALES));
            idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.CP_SUCURSAL,
                    d.getCodigoPostal(), orden, suffix, SUCURSALES));
            idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.ESTADO_SUCURSAL,
                    d.getEstado(), orden, suffix, SUCURSALES));
            idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.MUNICIPIO_SUCURSAL,
                    d.getMunicipio(), orden, suffix, SUCURSALES));

            idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.LATITUD_SUCURSAL,
                    String.valueOf(s.getLatitud()), orden, suffix, SUCURSALES));
            idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.LONGITUD_SUCURSAL,
                    String.valueOf(s.getLongitud()), orden, suffix, SUCURSALES));

            idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.ENLACE_SUCURSAL,
                    s.getNombreEnlace(), orden, suffix, SUCURSALES));
            idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.CELULAR_ENLACE_SUCURSAL,
                    s.getNumeroCelularEnlace(), orden, suffix, SUCURSALES));
            idc.agrega(getInstance(r, DatoCapturadoEmpresaEnum.CORREO_ENLACE_SUCURSAL,
                    s.getCorreoEnlace(), orden, suffix, SUCURSALES));

            idc.agrega(getInstanceArchivo(r, DatoCapturadoEmpresaEnum.COMPROBANTE_DOMICILIO_SUCURSAL_PDF,
                    orden + "_" + DatoCapturadoEmpresaEnum.COMPROBANTE_DOMICILIO_SUCURSAL_PDF.getClave() + ".pdf",
                    orden, suffix, SUCURSALES));
            idc.agrega(getInstanceArchivo(r, DatoCapturadoEmpresaEnum.DOCUMENTO_POSESION_SUCURSAL_PDF,
                    orden + "_" + DatoCapturadoEmpresaEnum.DOCUMENTO_POSESION_SUCURSAL_PDF.getClave() + ".pdf",
                    orden, suffix, SUCURSALES));
            idc.agrega(getInstanceArchivo(r, DatoCapturadoEmpresaEnum.CERTIFICADO_BASCULA_SUCURSAL_PDF,
                    orden + "_" + DatoCapturadoEmpresaEnum.CERTIFICADO_BASCULA_SUCURSAL_PDF.getClave() + ".pdf",
                    orden, suffix, SUCURSALES));
            idc.agrega(getInstanceArchivo(r, DatoCapturadoEmpresaEnum.JUSTIFICACION_GRANO_SUCURSAL_PDF,
                    orden + "_" + DatoCapturadoEmpresaEnum.JUSTIFICACION_GRANO_SUCURSAL_PDF.getClave() + ".pdf",
                    orden, suffix, SUCURSALES));
        }
    }

    private static void agregaAnexos(InscripcionDatoCapturado idc, RegistroEmpresa registro) {
        idc.agrega(getInstanceArchivo(registro, DatoCapturadoEmpresaEnum.ACTA_CONSTITUTIVA_PDF,
                DatoCapturadoEmpresaEnum.ACTA_CONSTITUTIVA_PDF.getClave() + ".pdf", ANEXOS));
        idc.agrega(getInstanceArchivo(registro, DatoCapturadoEmpresaEnum.RFC_PDF,
                DatoCapturadoEmpresaEnum.RFC_PDF.getClave() + ".pdf", ANEXOS));
        idc.agrega(getInstanceArchivo(registro, DatoCapturadoEmpresaEnum.DOCUMENTO_POSESION_PDF,
                DatoCapturadoEmpresaEnum.DOCUMENTO_POSESION_PDF.getClave() + ".pdf", ANEXOS));
        idc.agrega(getInstanceArchivo(registro, DatoCapturadoEmpresaEnum.CARTA_ACUERDO_PDF,
                DatoCapturadoEmpresaEnum.CARTA_ACUERDO_PDF.getClave() + ".pdf", ANEXOS));
        idc.agrega(getInstanceArchivo(registro, DatoCapturadoEmpresaEnum.CERTIFICADO_BASCULA_PDF,
                DatoCapturadoEmpresaEnum.CERTIFICADO_BASCULA_PDF.getClave() + ".pdf", ANEXOS));
        idc.agrega(getInstanceArchivo(registro, DatoCapturadoEmpresaEnum.JUSTIFICACION_GRANO_ACOPIADO_PDF,
                DatoCapturadoEmpresaEnum.JUSTIFICACION_GRANO_ACOPIADO_PDF.getClave() + ".pdf", ANEXOS));
        idc.agrega(getInstanceArchivo(registro, DatoCapturadoEmpresaEnum.COMPROBANTE_DOMICILIO_PDF,
                DatoCapturadoEmpresaEnum.COMPROBANTE_DOMICILIO_PDF.getClave() + ".pdf", ANEXOS));
        idc.agrega(getInstanceArchivo(registro, DatoCapturadoEmpresaEnum.PODER_VENTA_COMPRA_PDF,
                DatoCapturadoEmpresaEnum.PODER_VENTA_COMPRA_PDF.getClave() + ".pdf", ANEXOS));
    }
}
