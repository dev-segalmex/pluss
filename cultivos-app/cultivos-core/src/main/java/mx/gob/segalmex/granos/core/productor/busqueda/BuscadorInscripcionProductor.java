/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;

/**
 *
 * @author ismael
 */
public interface BuscadorInscripcionProductor {

    InscripcionProductor buscaElemento(Integer id);

    InscripcionProductor buscaElemento(String uuid);

    List<InscripcionProductor> busca(ParametrosInscripcionProductor parametros);

    List<Object[]> buscaAgrupada(ParametrosInscripcionProductor parametros);

    List<Object[]> buscaResumen(ParametrosInscripcionProductor parametros);

    InscripcionProductor buscaElemento(ParametrosInscripcionProductor parametros);

}
