/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato.pdf;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Objects;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricolaEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.ContratoProductor;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;
import mx.gob.segalmex.pluss.modelo.contrato.CoberturaInscripcionContrato;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author ismael
 */
public class InscripcionContratoPdfHelper {

    private static final float TAMANO_CONTENIDO_DOCUMENTO
            = PageSize.LETTER.getWidth() - PtConversion.toPt(1.5f) - PtConversion.toPt(1.5f);

    public void generaContenido(InscripcionContrato inscripcion, Document d, PdfWriter writer)
            throws DocumentException {
        PdfPTable contenido = generaTablaContenido();

        agregaDatosPrograma(contenido, inscripcion);
        agregaDatosEmpresaSucursal(contenido, inscripcion);
        agregaDatosContrato(contenido, inscripcion);
        agregaEstadosDestino(contenido, inscripcion);
        agregaDatosEntidadPrecio(contenido, inscripcion);
        if (Objects.nonNull(inscripcion.getCoberturas()) && !inscripcion.getCoberturas().isEmpty()) {
            agregaDatosCobertura(contenido, inscripcion);
        }
        agregaInformacionSemilla(contenido, inscripcion);
        agregaDatosProductores(contenido, inscripcion);
        agregaTerminos(contenido, inscripcion);

        PdfContentByte contentByte = writer.getDirectContent();
        SolicitudGobMxPdfHelper.writeColumns(d, contentByte, contenido, false);
    }

    protected PdfPTable generaTablaContenido() {
        PdfPTable table = new PdfPTable(1);
        table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        table.getDefaultCell().setPadding(0);
        table.setTotalWidth(TAMANO_CONTENIDO_DOCUMENTO);
        table.setLockedWidth(true);
        return table;
    }

    private String getFechaFormat(Calendar fecha) {
        return Objects.nonNull(fecha) ? SegalmexDateUtils.format(fecha) : "--";
    }

    private void agregaDatosPrograma(PdfPTable contenido, InscripcionContrato inscripcion) {
        contenido.addCell(generaCeldaEncabezado("Registro de Contrato - Programa de Precios de Garantía a Productos Alimentarios Básicos"));
        contenido.addCell(PdfHelper.generaCeldaVacia(1));

        PdfPTable seccion = generaTabla(3, TAMANO_CONTENIDO_DOCUMENTO);
        seccion.addCell(generaCeldaEncabezado("Ciclo agrícola"));
        seccion.addCell(generaCeldaEncabezado("Folio"));
        seccion.addCell(generaCeldaEncabezado("Fecha de captura"));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, inscripcion.getCiclo().getNombre(), false, false));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, "I-" + inscripcion.getCiclo().getAbreviatura() + "-" + inscripcion.getFolio(), false, false));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, getFechaFormat(inscripcion.getFechaCreacion()), false, false));
        contenido.addCell(seccion);

        contenido.addCell(PdfHelper.generaCeldaVacia(1));
    }

    private void agregaDatosEmpresaSucursal(PdfPTable contenido, InscripcionContrato inscripcion) {
        contenido.addCell(generaCeldaEncabezado("Datos de la ventanilla que registra el contrato"));
        contenido.addCell(PdfHelper.generaCeldaVacia(1));

        PdfPTable seccion = generaTabla(1, TAMANO_CONTENIDO_DOCUMENTO);
        seccion.addCell(generaCeldaEncabezado("Nombre de la ventanilla"));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, inscripcion.getSucursal().getEmpresa().getNombre(), false, false));
        contenido.addCell(seccion);

        seccion = generaTabla(4, TAMANO_CONTENIDO_DOCUMENTO);
        seccion.addCell(generaCeldaEncabezado("RFC"));
        seccion.addCell(generaCeldaEncabezado("Entidad"));
        seccion.addCell(generaCeldaEncabezado("Municipio"));
        seccion.addCell(generaCeldaEncabezado("Localidad"));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, inscripcion.getSucursal().getEmpresa().getRfc(), false, false));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, inscripcion.getSucursal().getEstado().getNombre(), false, false));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, inscripcion.getSucursal().getMunicipio().getNombre(), false, false));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, inscripcion.getSucursal().getLocalidad(), false, false));
        contenido.addCell(seccion);
        contenido.addCell(PdfHelper.generaCeldaVacia(1));
    }

    private void agregaDatosContrato(PdfPTable contenido, InscripcionContrato inscripcion) {
        contenido.addCell(generaCeldaEncabezado("Acerca del Contrato de Compra - Venta (CCV)"));
        contenido.addCell(PdfHelper.generaCeldaVacia(1));

        PdfPTable seccion = generaTabla(4, TAMANO_CONTENIDO_DOCUMENTO);
        seccion.addCell(generaCeldaEncabezado("Cultivo"));
        seccion.addCell(generaCeldaEncabezado("Tipo"));
        seccion.addCell(generaCeldaEncabezado("No. de contrato"));
        seccion.addCell(generaCeldaEncabezado("Fecha de firma"));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, inscripcion.getCultivo().getNombre(), false, false));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, inscripcion.getTipoCultivo().getNombre(), false, false));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, inscripcion.getNumeroContrato(), false, false));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, getFechaFormat(inscripcion.getFechaFirma()), false, false));
        contenido.addCell(seccion);
        contenido.addCell(PdfHelper.generaCeldaVacia(1));

        PdfPTable comprador = generaTabla(1, TAMANO_CONTENIDO_DOCUMENTO / 2 - 7f);
        comprador.addCell(generaCeldaEncabezado("Comprador del " + inscripcion.getCultivo().getNombre().toLowerCase()));
        comprador.addCell(generaCeldaSimple("Tipo de empresa", inscripcion.getTipoEmpresaComprador().getNombre(), false, false));
        comprador.addCell(generaCeldaSimple("Nombre del comprador", inscripcion.getNombreComprador(), false, false));
        comprador.addCell(generaCeldaSimple("RFC del comprador", inscripcion.getRfcComprador(), false, false));

        PdfPTable vendedor = generaTabla(1, TAMANO_CONTENIDO_DOCUMENTO / 2 - 7f);
        vendedor.addCell(generaCeldaEncabezado("Vendedor del " + inscripcion.getCultivo().getNombre().toLowerCase()));
        vendedor.addCell(generaCeldaSimple("Tipo de empresa", inscripcion.getTipoEmpresaVendedor().getNombre(), false, false));
        vendedor.addCell(generaCeldaSimple("Nombre del vendedor", inscripcion.getNombreVendedor(), false, false));
        vendedor.addCell(generaCeldaSimple("RFC del vendedor", inscripcion.getRfcVendedor(), false, false));

        PdfPCell compradorCell = new PdfPCell();
        compradorCell.setPadding(0);
        compradorCell.setBorder(0);
        compradorCell.setPaddingRight(7f);
        compradorCell.addElement(comprador);

        PdfPCell vendedorCell = new PdfPCell();
        vendedorCell.setPadding(0);
        vendedorCell.setBorder(0);
        vendedorCell.setPaddingLeft(7f);
        vendedorCell.addElement(vendedor);

        PdfPTable vc = generaTabla(2, TAMANO_CONTENIDO_DOCUMENTO);
        vc.addCell(compradorCell);
        vc.addCell(vendedorCell);

        contenido.addCell(vc);
        contenido.addCell(PdfHelper.generaCeldaVacia(1));
    }

    private void agregaDatosEntidadPrecio(PdfPTable contenido, InscripcionContrato inscripcion) {
        contenido.addCell(generaCeldaEncabezado("Información de entidad y precio contratado"));
        contenido.addCell(PdfHelper.generaCeldaVacia(1));

        PdfPTable seccion = generaTabla(6, TAMANO_CONTENIDO_DOCUMENTO);
        seccion.addCell(generaCeldaEncabezado("Entidad"));
        seccion.addCell(generaCeldaEncabezado("Tipo de IAR"));
        seccion.addCell(generaCeldaEncabezado("Tipo de precio"));
        seccion.addCell(generaCeldaEncabezado("PFT"));
        seccion.addCell(generaCeldaEncabezado("B"));
        seccion.addCell(generaCeldaEncabezado("PFT + B"));

        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, inscripcion.getEstado().getNombre(), false, false));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, inscripcion.getCoberturas().isEmpty()
                ? "Contrato a precio fijo" : "Cobertura", false, false));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, inscripcion.getTipoPrecio().getNombre(), false, false));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, getFormatoNumero(inscripcion.getPrecioFuturo()), false, false));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, getFormatoNumero(inscripcion.getBaseAcordada()), false, false));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, getFormatoNumero(inscripcion.getPrecioFijo()), false, false));
        contenido.addCell(seccion);
        contenido.addCell(PdfHelper.generaCeldaVacia(1));
    }

    private void agregaDatosCobertura(PdfPTable contenido, InscripcionContrato inscripcion) {
        contenido.addCell(generaCeldaEncabezado("Instrumentos de administración de riesgo (I.A.R.)"));
        contenido.addCell(PdfHelper.generaCeldaVacia(1));

        PdfPTable seccion = generaTabla(9, TAMANO_CONTENIDO_DOCUMENTO);
        seccion.addCell(generaCeldaEncabezado("No."));
        seccion.addCell(generaCeldaEncabezado("Correduría u operador"));
        seccion.addCell(generaCeldaEncabezado("Comprador"));
        seccion.addCell(generaCeldaEncabezado("Fechas"));
        seccion.addCell(generaCeldaEncabezado("Tipo de operación"));
        seccion.addCell(generaCeldaEncabezado("No. contratos"));
        seccion.addCell(generaCeldaEncabezado("Precio de ejercicio"));
        seccion.addCell(generaCeldaEncabezado("Prima"));
        seccion.addCell(generaCeldaEncabezado("Comisiones"));

        for (CoberturaInscripcionContrato c : inscripcion.getCoberturas()) {
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, c.getNumero(), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, c.getEntidadCobertura().getNombre(), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, c.getTipoComprador().getNombre() + " - " + c.getNombreComprador(), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, "Compra: " + getFechaFormat(c.getFechaCompra())
                    + "\nVencimiento: " + getFechaFormat(c.getFechaVencimiento()), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, c.getTipoOperacionCobertura().getNombre(), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, getFormatoNumero(c.getNumeroContratos(), "0.0"), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, getDolaresPesos(c.getPrecioEjercicioDolares(), c.getPrecioEjercicioPesos()), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, getDolaresPesos(c.getPrimaDolares(), c.getPrimaPesos()), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, getDolaresPesos(c.getComisionesDolares(), c.getComisionesPesos()), false, false));
        }

        contenido.addCell(seccion);
        contenido.addCell(PdfHelper.generaCeldaVacia(1));
    }

    private void agregaDatosProductores(PdfPTable contenido, InscripcionContrato inscripcion) {
        if (inscripcion.getCiclo().getClave().equals(CicloAgricolaEnum.OI2022.getClave())) {
            int productores = 0;
            BigDecimal superficie = BigDecimal.ZERO;
            BigDecimal volumenCcv = BigDecimal.ZERO;
            BigDecimal volumenIar = BigDecimal.ZERO;
            for (ContratoProductor cp : inscripcion.getContratosProductor()) {
                if (cp.getEstatusFechaBaja().equals("--")) {
                    productores++;
                    superficie = superficie.add(cp.getSuperficie());
                    volumenCcv = volumenCcv.add(cp.getVolumenContrato());
                    volumenIar = volumenIar.add(cp.getVolumenIar());
                }
            }
            if (productores > 0) {
                contenido.addCell(generaCeldaEncabezado("Datos de los productores que participan en el contrato"));
                contenido.addCell(PdfHelper.generaCeldaVacia(1));
                PdfPTable seccion = generaTabla(4, TAMANO_CONTENIDO_DOCUMENTO);
                seccion.addCell(generaCeldaEncabezado("Productores"));
                seccion.addCell(generaCeldaEncabezado("Total superficie (ha)"));
                seccion.addCell(generaCeldaEncabezado("Total volumen CCV"));
                seccion.addCell(generaCeldaEncabezado("Total volumen IAR"));
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, String.valueOf(productores), false, false));
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, getFormatoNumero(superficie), false, false));
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, getFormatoNumero(volumenCcv), false, false));
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, getFormatoNumero(volumenIar), false, false));
                contenido.addCell(seccion);
                contenido.addCell(PdfHelper.generaCeldaVacia(1));
            }
        } else {
            contenido.addCell(generaCeldaEncabezado("Datos de los productores que participan en el contrato"));
            contenido.addCell(PdfHelper.generaCeldaVacia(1));
            PdfPTable seccion = generaTabla(3, TAMANO_CONTENIDO_DOCUMENTO);
            seccion.addCell(generaCeldaEncabezado("Productores"));
            seccion.addCell(generaCeldaEncabezado("Superficie"));
            seccion.addCell(generaCeldaEncabezado("Volumen"));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, String.valueOf(inscripcion.getNumeroProductores()), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, getFormatoNumero(inscripcion.getSuperficie()), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, getFormatoNumero(inscripcion.getVolumenTotal()), false, false));
            contenido.addCell(seccion);
            contenido.addCell(PdfHelper.generaCeldaVacia(1));
        }

    }

    private void agregaInformacionSemilla(PdfPTable contenido, InscripcionContrato inscripcion) {
        PdfPTable seccion = null;
        if (Objects.nonNull(inscripcion.getFolioSnics())) {
            contenido.addCell(generaCeldaEncabezado("Datos semilla certificada"));
            seccion = generaTabla(1, TAMANO_CONTENIDO_DOCUMENTO);
            seccion.addCell(generaCeldaSimple("No. de folio de registro en el DPOCS", inscripcion.getFolioSnics(), false, false));
            contenido.addCell(seccion);
            contenido.addCell(PdfHelper.generaCeldaVacia(1));
        }
    }

    private String getDolaresPesos(BigDecimal dolares, BigDecimal pesos) {
        StringBuilder sb = new StringBuilder();
        sb.append(getFormatoNumero(dolares, "0.00"));
        sb.append(" USD/t\n");
        sb.append(getFormatoNumero(pesos, "0.00"));
        sb.append(" MXN/t");
        return sb.toString();
    }

    private String getFormatoNumero(BigDecimal bd) {
        return getFormatoNumero(bd, "0.000");
    }

    private String getFormatoNumero(BigDecimal bd, String formato) {
        if (Objects.isNull(bd)) {
            return "--";
        }

        NumberFormat nf = new DecimalFormat(formato);
        return nf.format(bd);
    }

    private void agregaTerminos(PdfPTable contenido, InscripcionContrato inscripcion) {
        PdfPTable seccion = generaTablaContenido();

        seccion.addCell(generaCeldaEncabezado("Términos y condiciones"));
        PdfPCell legales = null;
        switch (inscripcion.getCiclo().getClave()) {
            case "oi-2022":
                legales = PdfHelper.creaCeldaAviso(
                        "El presente registro de contrato es el inicio de un proceso que se realiza "
                        + "para participar en el Programa Precios de Garantía a Productos Alimentarios Básicos, "
                        + "en " + CultivoEnum.getInstance(inscripcion.getCultivo().getClave()).getNombre()
                        + " de medianos productores, para el ciclo " + inscripcion.getCiclo().getNombre() + ". Este documento no "
                        + "obliga a SEGALMEX a brindar el incentivo a productores que respalda este, a menos "
                        + "que cada uno cumpla con el registro firmado, los requisitos y la normativa "
                        + "establecida en la mecánica operativa y en las Reglas de Operación 2022, publicadas "
                        + "en el DOF el 31 de diciembre del 2021.\n\n"
                        + "Los datos recabados serán protegidos, incorporados y tratados en el marco de "
                        + "la Ley General de Protección de Documentos Personales en Posesión de Sujetos "
                        + "Obligados, con fundamento en los artículos 3, 27 y 28.", PdfConstants.BACKGROUND_GOBMX_WHITE,
                        PdfConstants.BACKGROUND_GOBMX_DARKGRAY);
                legales.setHorizontalAlignment(PdfPCell.ALIGN_JUSTIFIED);
                break;
            default:
                legales = PdfHelper.creaCeldaAviso(
                        "El presente registro de contrato es el inicio de un proceso que se deberá "
                        + "realizar para participar en el Programa Precios de Garantía a Productos Alimentarios "
                        + "Básicos, en "
                        + CultivoEnum.getInstance(inscripcion.getCultivo().getClave()).getNombre()
                        + " comercializado por medianos productores, para el Ciclo "
                        + inscripcion.getCiclo().getNombre() + ". Este documento no obliga a SEGALMEX a brindar "
                        + "el incentivo a productores que respalda este, a menos que cada uno cumpla con el "
                        + "registro firmado, los requisitos y la normativa establecida en la mecánica "
                        + "operativa y en las Reglas de Operación 2020, publicadas en el DOF el 24 de febrero "
                        + "del 2020.\n\n"
                        + "Los datos recabados serán protegidos, incorporados y tratados en el marco de la Ley "
                        + "General de Protección de Documentos Personales en Posesión de Sujetos Obligados, con "
                        + "fundamento en los artículos 3, 27 y 28.", PdfConstants.BACKGROUND_GOBMX_WHITE,
                        PdfConstants.BACKGROUND_GOBMX_DARKGRAY);
                legales.setHorizontalAlignment(PdfPCell.ALIGN_JUSTIFIED);
        }

        seccion.addCell(legales);
        contenido.addCell(seccion);
        contenido.addCell(PdfHelper.generaCeldaVacia(1));
    }

    public static PdfPTable generaTabla(int colspan, float width) {
        PdfPTable table = new PdfPTable(colspan);
        table.setTotalWidth(width);
        table.setLockedWidth(true);
        table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        table.getDefaultCell().setPadding(0);

        return table;
    }

    public static PdfPCell generaCeldaEncabezado(String encabezado) {
        PdfPCell celda = PdfHelper.creaCeldaBackground(encabezado, 1,
                PdfConstants.SOBERANA_LIGHT_8_BLACK, PdfConstants.BACKGROUND_GOBMX_GRAY);
        celda.setBorderColor(PdfConstants.BACKGROUND_GOBMX_DARKGRAY);
        celda.setBorder(Rectangle.BOX);
        celda.setBorderWidth(PdfConstants.CELDA_TAMANO_BORDE);
        celda.setMinimumHeight(PtConversion.toPt(PdfConstants.CELDA_ALTURA_MINIMA_CM));
        celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
        celda.setPadding(PtConversion.toPt(PdfConstants.CELDA_PADDING_CM));
        celda.setPaddingTop(PtConversion.toPt(0.10f));

        return celda;
    }

    /**
     * Genera una celda utilizada en las tablas de las secciones de las
     * solicitudes.
     *
     * @param etiqueta la etiqueta de la celda.
     * @param valor el valor correspondiente a la etiqueta.
     * @param transform indica si debe de transformarse el parámetro
     * <code>valor</code> a UpperCase.
     * @param blank true si debe de usarse una cadena vacía como valor cuando
     * éste es nulo, false para usar <code>--</code>.
     *
     * @return una celda utilizada en las tablas de secciones de las
     * solicitudes.
     */
    private static PdfPCell generaCeldaSimple(String etiqueta, String valor, boolean transform, boolean blank) {
        StringBuilder sb = new StringBuilder();
        if (StringUtils.trimToNull(etiqueta) != null) {
            sb.append(etiqueta);
            sb.append(": ");
        }

        String nValor;
        if (Objects.isNull(StringUtils.trimToNull(valor))) {
            nValor = !blank ? "--" : StringUtils.EMPTY;
        } else {
            nValor = valor;
        }

        nValor = transform ? StringUtils.upperCase(nValor) : nValor;
        sb.append(nValor);

        PdfPCell celda = PdfHelper.creaCeldaBackground(sb.toString(), 1, PdfConstants.SOBERANA_LIGHT_8_BLACK, PdfConstants.BACKGROUND_GOBMX_WHITE);
        celda.setBorderColor(PdfConstants.BACKGROUND_GOBMX_DARKGRAY);
        celda.setBorder(Rectangle.BOX);
        celda.setBorderWidth(PdfConstants.CELDA_TAMANO_BORDE);
        celda.setMinimumHeight(PtConversion.toPt(PdfConstants.CELDA_ALTURA_MINIMA_CM));
        celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
        celda.setPadding(PtConversion.toPt(PdfConstants.CELDA_PADDING_CM));
        celda.setPaddingLeft(PtConversion.toPt(PdfConstants.CELDA_LEFT_PADDING_CM));

        return celda;
    }

    /**
     * Agrega la lista de estados de destino del grano.
     *
     * @param contenido
     * @param inscripcion
     */
    private void agregaEstadosDestino(PdfPTable contenido, InscripcionContrato inscripcion) {
        contenido.addCell(generaCeldaEncabezado("Entidad destino del cultivo"));
        contenido.addCell(PdfHelper.generaCeldaVacia(1));

        PdfPTable seccion = generaTabla(1, TAMANO_CONTENIDO_DOCUMENTO);
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, inscripcion.getEstadosDestinoTexto(), false, false));
        contenido.addCell(seccion);
        contenido.addCell(PdfHelper.generaCeldaVacia(1));
    }
}
