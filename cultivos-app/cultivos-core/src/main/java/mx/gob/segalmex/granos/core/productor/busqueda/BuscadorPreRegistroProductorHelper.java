package mx.gob.segalmex.granos.core.productor.busqueda;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.productor.PreRegistroAgrupado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Component
public class BuscadorPreRegistroProductorHelper {

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorPreRegistroProductor buscadorPreRegistroProductor;

    public List<PreRegistroAgrupado> busca(ParametrosInscripcionProductor parametros) {
        List<Object[]> registros = buscadorPreRegistroProductor.buscaAgrupado(parametros);
        List<PreRegistroAgrupado> agrupadas = new ArrayList<>();
        if (Objects.nonNull(registros)) {
            for (Object[] obj : registros) {
                agrupadas.add(generaPreAgrupado(obj));
            }
        }
        return agrupadas;
    }

    private PreRegistroAgrupado generaPreAgrupado(Object[] obj) {
        PreRegistroAgrupado pa = new PreRegistroAgrupado();
        pa.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, (Integer) obj[0]));
        pa.setEstado(buscadorCatalogo.buscaElemento(Estado.class, (Integer) obj[1]));
        pa.setTotal((Math.toIntExact((Long) obj[2])));
        return pa;
    }

}
