package mx.gob.segalmex.common.core.validador;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;
import mx.gob.segalmex.pluss.modelo.productor.TipoProductorEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar Al momento no se está ocupando. NOTA: Es posible que se borre o se reutilice más
 * tarde.
 */
@Component("validadorInscripcionProductorPequenio")
@Slf4j
public class ValidadorInscripcionProductorPequenio implements ValidadorInscripcion {

    private static final String OMITE_PEQUENOS = ":omite-productores-pequenos";

    @Autowired
    private BuscadorProductor buscadorProductor;

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Override
    public void valida(InscripcionProductor inscripcion) {

        if (omitir(inscripcion.getCultivo(), inscripcion.getCiclo(), OMITE_PEQUENOS)) {
            LOGGER.info("Se omite la validación de productor pequeño {} - {}",
                    inscripcion.getCultivo().getClave(), inscripcion.getCiclo().getClave());
            return;
        }

        // Todos los medianos se valen (no importa el previo)
        if (inscripcion.getTipoProductor().getClave().equals(TipoProductorEnum.MEDIANO.getClave())) {
            return;
        }
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setCultivo(inscripcion.getCultivo());
        parametros.setCurp(inscripcion.getDatosProductor().getCurp());
        List<ProductorCiclo> productores = buscadorProductor.buscaProductorCiclo(parametros);
        if (productores.isEmpty()) {
            throw new SegalmexRuntimeException("Error en el registro.",
                    "No se permite el registro de un productor pequeño o cambio de un productor mediano a pequeño.");
        }
        Collections.sort(productores, (p1, p2) -> {
            return new Integer(p1.getCiclo().getOrden()).compareTo(p2.getCiclo().getOrden());
        });
        ProductorCiclo ultimo = productores.get(productores.size() - 1);

        if (!ultimo.getTipoProductor().getClave().equals(inscripcion.getTipoProductor().getClave())) {
            throw new SegalmexRuntimeException("Error al realizar el registro.",
                    "No se puede cambiar el tipo de productor con el registro previo.");
        }
    }

    private boolean omitir(Cultivo c, CicloAgricola ciclo, String clave) {
        Parametro p = buscadorParametro.buscaElementoOpcional(c.getClave()
                + ":" + ciclo.getClave() + clave);
        return Objects.nonNull(p) && Boolean.valueOf(p.getValor());
    }
}
