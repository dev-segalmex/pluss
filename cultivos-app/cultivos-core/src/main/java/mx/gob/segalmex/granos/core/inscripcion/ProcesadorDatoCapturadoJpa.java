/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.inscripcion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.archivos.BuscadorArchivoRelacionado;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.empresas.EmpresaDatoCapturadoFactory;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.factura.FacturaDatoCapturadoFactory;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorComprobanteRecepcion;
import mx.gob.segalmex.common.core.factura.busqueda.ParametrosComprobanteRecepcion;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.pluss.modelo.archivos.ArchivoRelacionado;
import mx.gob.segalmex.pluss.modelo.catalogos.Banco;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroEmpresa;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import mx.gob.segalmex.granos.core.contrato.ContratoDatoCapturadoFactory;
import mx.gob.segalmex.granos.core.contrato.busqueda.BuscadorDatoCapturado;
import mx.gob.segalmex.granos.core.productor.DatoCapturadoProductorEnum;
import mx.gob.segalmex.granos.core.productor.ProductorDatoCapturadoFactory;
import mx.gob.segalmex.pluss.modelo.historico.EntidadHistorico;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.Inscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ismael
 */
@Service("procesadorDatoCapturado")
@Slf4j
public class ProcesadorDatoCapturadoJpa implements ProcesadorDatoCapturado {

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private BuscadorDatoCapturado buscadorDatoCapturado;

    @Autowired
    private DatoCapturadoHelper datoCapturadoHelper;

    @Autowired
    private BuscadorArchivoRelacionado buscadorArchivoRelacionado;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorComprobanteRecepcion buscadorComprobante;

    @Autowired
    private ProductorDatoCapturadoFactory productorDatoCapturadoFactory;

    @Autowired
    private FacturaDatoCapturadoFactory facturaDatoCapturadoFactory;

    @Transactional
    @Override
    public List<DatoCapturado> procesa(EntidadHistorico entidad) {
        List<DatoCapturado> datos = getNuevos(entidad);
        for (DatoCapturado dc : datos) {
            LOGGER.debug("Campo {}: {}", dc.getClave(), dc.getValor());
            registroEntidad.guarda(dc);
        }

        return datos;
    }

    @Transactional
    @Override
    public void validaEstatus(EntidadHistorico entidad, List<DatoCapturado> datos, EstatusInscripcionEnum estatus) {
        List<DatoCapturado> registrados
                = buscadorDatoCapturado.busca(entidad.getClass(), entidad.getUuid(), true);
        Map<String, DatoCapturado> map = getMap(registrados);

        for (DatoCapturado dc : datos) {
            valida(dc, estatus);
            DatoCapturado registrado = map.get(dc.getClave());
            registrado.setCorrecto(dc.getCorrecto());
            registrado.setComentario(dc.getComentario());
        }
        actualiza(registrados);
    }

    @Transactional
    @Override
    public void solicitaCorreccion(EntidadHistorico entidad, List<DatoCapturado> datos) {
        List<DatoCapturado> registrados
                = buscadorDatoCapturado.busca(entidad.getClass(), entidad.getUuid(), true);
        Map<String, DatoCapturado> mregistrados = getMap(registrados);

        List<DatoCapturado> nuevos = getNuevos(entidad);
        Map<String, DatoCapturado> mnuevos = getMap(nuevos);

        for (DatoCapturado dc : datos) {
            valida(dc, EstatusInscripcionEnum.VALIDACION);
            DatoCapturado registrado = mregistrados.get(dc.getClave());

            registrado.setCorrecto(dc.getCorrecto());
            registrado.setComentario(dc.getComentario());
            registrado.setActual(false);

            // Actualizamos los nuevos para que sean editables sin perder los valores originales
            DatoCapturado nuevo = mnuevos.get(dc.getClave());
            LOGGER.info(dc.getClave() + " - " + registrado.getValor());

            nuevo.setValor(registrado.getValor());
            nuevo.setCorrecto(Objects.nonNull(dc.getCorrecto()) ? dc.getCorrecto() : false);
            nuevo.setComentario(dc.getComentario());
        }
        actualiza(registrados);
        guarda(nuevos);
    }

    private List<DatoCapturado> getNuevos(EntidadHistorico entidad) {
        switch (entidad.getClass().getSimpleName()) {
            case "InscripcionContrato":
                return ContratoDatoCapturadoFactory.getInstance((InscripcionContrato) entidad);
            case "InscripcionProductor":
                return productorDatoCapturadoFactory.getInstance((InscripcionProductor) entidad);
            case "InscripcionFactura":
                List<ArchivoRelacionado> archivos
                        = buscadorArchivoRelacionado.busca(entidad.getClass(), entidad.getUuid());
                InscripcionFactura inscripcionFactura = (InscripcionFactura) entidad;
                ParametrosComprobanteRecepcion parametros = new ParametrosComprobanteRecepcion();
                parametros.setInscripcionFactura(inscripcionFactura);
                inscripcionFactura.setComprobantes(buscadorComprobante.busca(parametros));
                return facturaDatoCapturadoFactory.getInstance(inscripcionFactura, archivos);
            case "RegistroEmpresa":
                return EmpresaDatoCapturadoFactory.getInstance((RegistroEmpresa) entidad);
            default:
                throw new IllegalArgumentException("Clase desconocida: "
                        + entidad.getClass().getSimpleName());
        }
    }

    @Transactional
    @Override
    public void corrige(EntidadHistorico entidad, List<DatoCapturado> datos) {
        List<DatoCapturado> registrados
                = buscadorDatoCapturado.busca(entidad.getClass(), entidad.getUuid(), true);
        Map<String, DatoCapturado> corregidos = getMap(datos);
        Map<String, DatoCapturado> actuales = getMap(registrados);

        for (DatoCapturado registrado : registrados) {
            if (registrado.getTipo().equals("archivo")) {
                LOGGER.warn("Se omite corrección de dato capturado: {}", registrado.getClave());
                continue;
            }
            DatoCapturado corregido = corregidos.get(registrado.getClave());
            LOGGER.debug("El dato para {}: {}", registrado.getClave(), corregido);
            // Solo permitimos los incorrectos
            if (Objects.nonNull(registrado.getCorrecto()) && !registrado.getCorrecto()) {
                if (!registrado.getClave().equals(DatoCapturadoProductorEnum.BANCO_PRODUCTOR.getClave())) {
                    registrado.setValor(corregido.getValor());
                    registrado.setValorTexto(corregido.getValorTexto());
                }

                if (registrado.getClave().equals(DatoCapturadoProductorEnum.CLABE_PRODUCTOR.getClave())) {
                    registrado.setValor(corregido.getValor());
                    registrado.setValorTexto(corregido.getValorTexto());
                    DatoCapturado banco = actuales.get(DatoCapturadoProductorEnum.BANCO_PRODUCTOR.getClave());
                    banco.setValor(corregido.getValor().substring(0, 3));
                    banco.setValorTexto(buscadorCatalogo.buscaElemento(Banco.class,
                            banco.getValor()).getNombre());
                }
            }
        }
        actualiza(registrados);
        datoCapturadoHelper.actualizaDatosCapturados(entidad, registrados, false);
    }

    @Transactional
    @Override
    public void corrige(InscripcionContrato inscripcion, List<DatoCapturado> datos) {
        List<DatoCapturado> registrados
                = buscadorDatoCapturado.busca(inscripcion.getClass(), inscripcion.getUuid(), true);
        Map<String, DatoCapturado> corregidos = getMap(datos);

        for (DatoCapturado registrado : registrados) {
            if (registrado.getTipo().equals("archivo")) {
                LOGGER.warn("Se omite corrección de dato capturado: {}", registrado.getClave());
                continue;
            }
            DatoCapturado corregido = corregidos.get(registrado.getClave());
            LOGGER.debug("El dato para {}: {}", registrado.getClave(), corregido);
            // Solo permitimos los incorrectos
            if (Objects.nonNull(registrado.getCorrecto()) && !registrado.getCorrecto()) {
                registrado.setValor(corregido.getValor());
                registrado.setValorTexto(corregido.getValorTexto());
            }
        }
        actualiza(registrados);
        datoCapturadoHelper.actualizaDatosCapturados(inscripcion, registrados, false);
    }

    private void valida(DatoCapturado dc, EstatusInscripcionEnum estatus) {
        switch (estatus) {
            case POSITIVA:
                if (Objects.isNull(dc.getCorrecto()) || !dc.getCorrecto()) {
                    List<String> motivos = new ArrayList<>();
                    motivos.add("Los datos capturados deben ser correctos.");
                    throw new SegalmexRuntimeException("Error al validar positivamente.", motivos);
                }
                break;
            case CANCELADA:
            case VALIDACION:
                if (Objects.isNull(dc.getCorrecto())) {
                    List<String> motivos = new ArrayList<>();
                    motivos.add("Los datos capturados deben tener estatus.");
                    throw new SegalmexRuntimeException("Error al validar negativamente.", motivos);
                }
                break;
        }
    }

    private Map<String, DatoCapturado> getMap(List<DatoCapturado> datos) {
        Map<String, DatoCapturado> map = new HashMap<>();
        for (DatoCapturado d : datos) {
            map.put(d.getClave(), d);
        }
        return map;
    }

    private void actualiza(List<DatoCapturado> datos) {
        for (DatoCapturado d : datos) {
            registroEntidad.actualiza(d);
        }
    }

    private void guarda(List<DatoCapturado> datos) {
        for (DatoCapturado d : datos) {
            registroEntidad.guarda(d);
        }
    }

    @Override
    @Transactional
    public void actualizaCampos(InscripcionContrato inscripcion) {
        List<DatoCapturado> registrados = buscadorDatoCapturado
                .busca(InscripcionContrato.class, inscripcion.getUuid(), true);
        datoCapturadoHelper.actualizaDatosCapturados(inscripcion, registrados, false);

        // Actualizamos las entidades
        registroEntidad.actualiza(inscripcion);
    }

    @Transactional
    @Override
    public void eliminaDuplicados(Inscripcion inscripcion) {
        List<DatoCapturado> registrados = buscadorDatoCapturado
                .busca(inscripcion.getClass(), inscripcion.getUuid(), true);
        Map<String, DatoCapturado> map = new HashMap<>();
        List<DatoCapturado> duplicados = new ArrayList<>();
        for (DatoCapturado d : registrados) {
            DatoCapturado registrado = map.get(d.getClave());
            map.put(d.getClave(), d);
            if (Objects.isNull(registrado)) {
                continue;
            }
            registrado.setActual(false);
            duplicados.add(registrado);
        }
        actualiza(duplicados);
    }

    @Transactional
    @Override
    public void elimina(Inscripcion inscripcion) {
        List<DatoCapturado> registrados = buscadorDatoCapturado
                .busca(inscripcion.getClass(), inscripcion.getUuid(), true);
        for (DatoCapturado d : registrados) {
            d.setActual(false);
        }
        actualiza(registrados);
    }

    @Override
    @Transactional
    public List<DatoCapturado> procesaIar(EntidadHistorico entidad) {
        List<DatoCapturado> datos = ContratoDatoCapturadoFactory.getInstanceIar((InscripcionContrato) entidad);
        int orden = 400;
        for (DatoCapturado dc : datos) {
            LOGGER.debug("Campo {}: {}", dc.getClave(), dc.getValor());
            dc.setOrden(orden);
            registroEntidad.guarda(dc);
            orden += 10;
        }

        return datos;
    }

}
