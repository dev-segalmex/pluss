/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.validador;

import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Component("validadorInscripcionProductorCurp")
@Slf4j
public class ValidadorInscripcionProductorCurp implements ValidadorInscripcion {

    @Override
    public void valida(InscripcionProductor inscripcion) {
        LOGGER.debug("Validando CURP del productor.");
        String curp = inscripcion.getDatosProductor().getCurp();
        ValidadorCurpHelper.valida(curp, "del productor");
    }
}
