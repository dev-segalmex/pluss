package mx.gob.segalmex.common.core.datos.catalogos;

import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.productor.RegimenHidrico;

/**
 *
 * @author ismael
 */
public class CargadorDatosRegimenHidrico extends AbstractCargadorDatosCatalogo<RegimenHidrico> {

    @Override
    public RegimenHidrico getInstance(String[] elementos) {
        RegimenHidrico regimen = new RegimenHidrico();
        regimen.setClave(elementos[0]);
        regimen.setNombre(elementos[1]);
        regimen.setActivo(true);
        regimen.setOrden(Integer.parseInt(elementos[2]));
        return regimen;
    }

}
