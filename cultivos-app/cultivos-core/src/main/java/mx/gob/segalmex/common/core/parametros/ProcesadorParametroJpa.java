/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.parametros;

import java.util.Calendar;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jurgen
 */
@Service("procesadorParametro")
@Slf4j
public class ProcesadorParametroJpa implements ProcesadorParametro {

    @Autowired
    private RegistroEntidad registroEntidad;

    @Transactional
    @Override
    public void procesa(Parametro parametro) {
        if (Objects.isNull(parametro.getClave())) {
            throw new SegalmexRuntimeException("Error al registrar.", "La clave no puede ser nula.");
        }

        if (Objects.isNull(parametro.getGrupo())) {
            throw new SegalmexRuntimeException("Error al registrar.", "El grupo no puede ser nulo.");
        }
        LOGGER.info("Agregando nuevo parámetro con clave: {}", parametro.getClave());
        registroEntidad.guarda(parametro);
    }

    @Transactional
    @Override
    public void modifica(Parametro parametro) {
        LOGGER.info("Actualizando parámetro: {}", parametro.getClave());
        registroEntidad.actualiza(parametro);
    }

    @Override
    @Transactional
    public void elimina(Parametro parametro) {
        LOGGER.info("Eliminando parámetro...");
        parametro.setEstatusFechaBaja(SegalmexDateUtils.format(Calendar.getInstance(), "yyyy-MM-dd'T'HH:mm:ss"));
        registroEntidad.actualiza(parametro);
    }
}
