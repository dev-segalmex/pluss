/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor;

import java.util.Objects;
import java.util.UUID;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.common.core.persona.procesador.ProcesadorPersona;
import mx.gob.segalmex.pluss.modelo.catalogos.Pais;
import mx.gob.segalmex.pluss.modelo.personas.Persona;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersona;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersonaEnum;
import mx.gob.segalmex.pluss.modelo.productor.DatosProductor;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.Productor;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCicloCultivo;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCicloGrupo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ismael
 */
@Service("registroProductor")
public class RegistroProductorJpa implements RegistroProductor {

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private ProcesadorPersona procesadorPersona;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private ProductorCicloGrupoFactory productorCicloGrupoFactory;

    @Transactional
    @Override
    public Productor registra(DatosProductor dp) {
        Productor p = new Productor();
        p.setActivo(true);
        p.setClave(dp.getRfc());
        p.setRfc(dp.getRfc());
        p.setCurp(dp.getCurp());
        p.setUuid(UUID.randomUUID().toString());
        p.setTipoPersona(dp.getTipoPersona());
        Persona persona = getInstance(dp);

        TipoPersona tp = null;
        String nombre = null;
        switch (TipoPersonaEnum.getInstance(dp.getTipoPersona().getClave())) {
            case FISICA:
                tp = buscadorCatalogo.buscaElemento(TipoPersona.class,
                        TipoPersonaEnum.PRODUCTOR.getClave());
                nombre = persona.getNombre() + " " + persona.getApellidos();
                break;
            case MORAL:
                tp = buscadorCatalogo.buscaElemento(TipoPersona.class,
                        TipoPersonaEnum.REPRESENTANTE.getClave());
                nombre = dp.getNombreMoral();
                break;
        }

        p.setNombre(nombre);
        p.setPersona(procesadorPersona.procesa(persona, tp));
        registroEntidad.guarda(p);

        return p;
    }

    private Persona getInstance(DatosProductor dp) {
        Persona p = new Persona();
        p.setClave(dp.getCurp());
        p.setFechaNacimiento(dp.getFechaNacimiento());
        p.setNombre(StringUtils.trimToNull(dp.getNombre()));
        p.setPrimerApellido(StringUtils.trimToNull(dp.getPrimerApellido()));
        p.setSegundoApellido(StringUtils.trimToNull(dp.getSegundoApellido()));
        p.setSexo(dp.getSexo());
        p.setApellidos(getApellidos(p.getPrimerApellido(), p.getSegundoApellido()));
        p.setNacionalidad(buscadorCatalogo.buscaElemento(Pais.class, "MX"));

        return p;
    }

    private String getApellidos(String primerApellido, String segundoApellido) {
        StringBuilder apellidos = new StringBuilder();
        if (Objects.nonNull(primerApellido)) {
            apellidos.append(primerApellido);
        }
        if (Objects.nonNull(segundoApellido)) {
            apellidos.append(" ");
            apellidos.append(segundoApellido);
        }
        return apellidos.toString();
    }

    @Transactional
    @Override
    public ProductorCiclo registra(Productor productor, InscripcionProductor inscripcion) {
        ProductorCiclo pc = new ProductorCiclo();
        pc.setProductor(productor);
        pc.setCiclo(inscripcion.getCiclo());
        pc.setCultivo(inscripcion.getCultivo());
        pc.setFolio(inscripcion.getFolio());
        pc.setCultivos(ProductorCicloCultivoFactory.getInstance(inscripcion, pc));
        pc.setGrupos(productorCicloGrupoFactory.getInstance(inscripcion, pc));
        pc.setTipoProductor(inscripcion.getTipoProductor());
        CalculoTotalesHelper.calcula(pc, inscripcion);
        registroEntidad.guarda(pc);

        for (ProductorCicloCultivo pcc : pc.getCultivos()) {
            registroEntidad.guarda(pcc);
        }
        for (ProductorCicloGrupo pcg : pc.getGrupos()) {
            registroEntidad.guarda(pcg);
        }

        return pc;
    }

}
