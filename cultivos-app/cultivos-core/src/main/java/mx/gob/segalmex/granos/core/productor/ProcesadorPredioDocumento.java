/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor;

import java.io.IOException;
import java.util.List;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioDocumento;

/**
 *
 * @author oscar
 */
public interface ProcesadorPredioDocumento {

    /**
     * Se encarga de obtener una lista de {@link PredioDocumento} y procesarla.
     *
     * @param inscripcion la inscripción a la que pertenecen los
     * {@link PredioDocumento}.
     * @return la lista de {@link PredioDocumento} procesada.
     * @throws java.io.IOException
     */
    List<PredioDocumento> procesa(InscripcionProductor inscripcion) throws IOException;

    /**
     * Se encarga de verificar si el {@link PredioDocumento} es valido ante el
     * SAT y si está o no duplicado.
     *
     * @param pd
     */
    void verifica(PredioDocumento pd);

    /**
     * Desactiva todos los {@link PredioDocumento} de la inscripción recibida.
     *
     * @param inscripcion la inscripción de los {@link PredioDocumento}.
     */
    void desactiva(InscripcionProductor inscripcion);

    /**
     * Se encgarga de desactivar todos los {@link PredioDocumento} de la lista recibida.
     * @param prediosDocumento los {@link PredioDocumento} a eliminar.
     */
    void desactiva(List<PredioDocumento> prediosDocumento);
}
