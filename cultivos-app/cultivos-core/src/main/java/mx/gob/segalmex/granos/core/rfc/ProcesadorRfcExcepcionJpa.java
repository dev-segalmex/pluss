/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.rfc;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.archivos.ArchivoUtils;
import mx.gob.segalmex.common.core.archivos.ManejadorArchivo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.common.core.validador.ValidadorRfcHelper;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.granos.core.rfc.busqueda.BuscadorRfcExcepcion;
import mx.gob.segalmex.pluss.modelo.productor.RfcExcepcion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author oscar
 */
@Service("procesadorRfcExcepcion")
@Slf4j
public class ProcesadorRfcExcepcionJpa implements ProcesadorRfcExcepcion {

    @Autowired
    private ManejadorArchivo manejadorArchivo;

    @Autowired
    private BuscadorRfcExcepcion buscadorRfcExcepcion;

    @Autowired
    private RegistroEntidad registroEntidad;

    @Override
    @Transactional
    public RfcExcepcion procesa(String rfc, Usuario usuario, InputStream is,
            String nombreArchivo) {
        rfc = rfc.toUpperCase();
        LOGGER.info("Guardando excepción para el rfc: {}", rfc);
        List<String> motivos = new ArrayList<>();
        validaExiste(rfc, motivos);
        validaIncorrecto(rfc, motivos);

        if (!motivos.isEmpty()) {
            throw new SegalmexRuntimeException("Error al registrar la excepción", motivos);
        }
        String fecha = SegalmexDateUtils.format(Calendar.getInstance(), "yyy-MM-dd");
        Archivo archivo = ArchivoUtils.getInstance(nombreArchivo, "cultivos", "/rfc/"
                + fecha, false);
        manejadorArchivo.guarda(archivo, is);

        RfcExcepcion re = new RfcExcepcion();
        re.setConstancia(archivo);
        re.setRfc(rfc);
        re.setUsuarioRegistra(usuario);
        registroEntidad.guarda(re);
        return re;
    }

    @Override
    @Transactional
    public void elimina(RfcExcepcion re, Usuario usuarioBaja) {
        LOGGER.debug("Eliminando/desactivando excepción para el RFC: ", re.getRfc());
        re.setActivo(false);
        re.setFechaBaja(Calendar.getInstance());
        re.setUsuarioBaja(usuarioBaja);
        registroEntidad.actualiza(re);
    }

    private void validaExiste(String rfc, List<String> motivos) {
        try {
            buscadorRfcExcepcion.buscaElemento(rfc);
            motivos.add("Ya existe una excepción para el RFC " + rfc + ".");
            LOGGER.error("Ya existe una excepción para el RFC " + rfc + ".");
        } catch (EmptyResultDataAccessException ouch) {
            LOGGER.debug("No existe excepción para el RFC.");
        }
    }

    private void validaIncorrecto(String rfc, List<String> motivos) {
        try {
            ValidadorRfcHelper.valida(rfc, null, "registrando");
            motivos.add("El RFC " + rfc + " no representa una excepción.");
            LOGGER.error("El RFC " + rfc + " no representa una excepción.");
        } catch (SegalmexRuntimeException ouch) {
            LOGGER.info("El RFC es incorrecto, se puede considerar para excepción.");
        }
    }

}
