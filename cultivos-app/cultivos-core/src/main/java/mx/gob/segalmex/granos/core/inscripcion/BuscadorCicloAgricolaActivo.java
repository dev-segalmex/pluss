/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.inscripcion;

import java.util.List;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorUsoCatalogo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component
@Slf4j
public class BuscadorCicloAgricolaActivo {

    /**
     * El prefijo para indicar el uso del ciclo activo.
     */
    private static final String PREFIX = "ciclo-activo";

    /**
     * El separador de campos para el uso de ciclo activo.
     */
    private static final String SEPARADOR = ":";

    /**
     * El valor del campo para indicar que es un valor global.
     */
    private static final String GLOBAL = "--";

    /**
     * El buscador de usos catálogo.
     */
    @Autowired
    private BuscadorUsoCatalogo buscadorUso;

    /**
     * Busca la lista de CicloAgricola basado en el cultivo, la inscripción que se hace y el
     * usuario.
     *
     * @param cultivo el cultivo del cual se busca el ciclo.
     * @param inscripcion el tipo de la inscripción: contrato, productor, factura.
     * @param u el usuario del que se busca el ciclo.
     * @return la lista de ciclos agrícolas activos.
     */
    public List<CicloAgricola> busca(Cultivo cultivo, String inscripcion, Usuario u) {
        Objects.requireNonNull(cultivo, "El cultivo no puede ser nulo.");
        Objects.requireNonNull(u, "El usuario no puede ser nulo.");
        if (Objects.isNull(inscripcion)) {
            inscripcion = GLOBAL;
        }

        String uso = PREFIX + SEPARADOR
                + cultivo.getClave() + SEPARADOR
                + inscripcion + SEPARADOR
                + u.getId();
        LOGGER.debug("Buscando ciclos por: {}", uso);
        List<CicloAgricola> ciclos = buscadorUso.busca(CicloAgricola.class, uso);
        if (!ciclos.isEmpty()) {
            return ciclos;
        }

        uso = PREFIX + SEPARADOR
                + cultivo.getClave() + SEPARADOR
                + inscripcion + SEPARADOR
                + GLOBAL;
        LOGGER.debug("Buscando ciclos por: {}", uso);
        ciclos = buscadorUso.busca(CicloAgricola.class, uso);
        if (!ciclos.isEmpty()) {
            return ciclos;
        }

        uso = PREFIX + SEPARADOR
                + cultivo.getClave() + SEPARADOR
                + GLOBAL + SEPARADOR
                + GLOBAL;
        LOGGER.debug("Buscando ciclos por: {}", uso);
        return buscadorUso.busca(CicloAgricola.class, uso);
    }

    public CicloAgricola getCicloSeleccionado(Cultivo c, List<CicloAgricola> ciclos, Usuario u, boolean estricto) {
        List<CicloAgricola> seleccionados = buscadorUso.busca(CicloAgricola.class,
                "ciclo-seleccionado:" + c.getClave() + ":" + u.getId());
        if (!seleccionados.isEmpty()) {
            CicloAgricola seleccionado = seleccionados.get(0);
            if (ciclos.contains(seleccionado)) {
                return seleccionado;
            }
        }
        throw new SegalmexRuntimeException("Error de ciclo seleccionado.",
                "El usuario no tiene un ciclo seleccionado válido.");
    }

    public CicloAgricola getCicloSeleccionadoActivo(Cultivo c, Usuario u) {
        Objects.requireNonNull(c, "El cultivo no puede ser nulo.");
        Objects.requireNonNull(u, "El usuario no puede ser nulo.");
        List<CicloAgricola> ciclosActivos = busca(c, "--", u);
        return getCicloSeleccionado(c, ciclosActivos, u, true);
    }
}
