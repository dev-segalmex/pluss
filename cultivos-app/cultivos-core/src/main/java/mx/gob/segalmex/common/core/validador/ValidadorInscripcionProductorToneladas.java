/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.validador;

import java.math.BigDecimal;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Component("validadorInscripcionProductorToneladas")
public class ValidadorInscripcionProductorToneladas implements ValidadorInscripcion {

    private static final String MAXIMO_VOLUMEN = ":maximo-toneladas";

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Override
    public void valida(InscripcionProductor inscripcion) {
        Parametro p = buscadorParametro.buscaElemento(inscripcion.getCultivo().getClave()
                + MAXIMO_VOLUMEN);
        BigDecimal maximo = new BigDecimal(p.getValor());
        // Si el máximo es menor o igual a cero, no validamos
        if (maximo.compareTo(BigDecimal.ZERO) <= 0) {
            return;
        }

        // Sumamos las toneladas de los predios
        BigDecimal volumen = BigDecimal.ZERO;
        for (PredioProductor pp : inscripcion.getPrediosActivos()) {
            volumen = volumen.add(pp.getVolumen());
        }

        if (volumen.compareTo(maximo) > 0) {
            throw new SegalmexRuntimeException("Error al realizar el registro.", "Volumen obtenido es mayor al número máximo");
        }
    }
}
