/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioBasico;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;

/**
 *
 * @author jurgen
 */
public class EstadoProductorHelper {

    /**
     * Calcula el Estado con mayor superficie para asignarlo a la Inscripción.
     *
     * @param i la inscripción del productor.
     * @return el estado con mayor superficie.
     */
    public static Estado getEstado(InscripcionProductor i) {
        List<PredioProductor> predios = i.getPrediosActivos().isEmpty()
                ? i.getPredios() : i.getPrediosActivos();
        return getEstado(predios);
    }

    /**
     * Calcula el Estado con mayor superficie para asignarlo al pre registro.
     *
     * @param predios el pre registro del productor.
     * @return el estado con mayor superficie del pre registro.
     */
    public static Estado getEstado(List<? extends PredioBasico> predios) {
        Map<Estado, BigDecimal> map = predios.stream()
                .collect(Collectors.groupingBy(PredioBasico::getEstado,
                        Collectors.mapping(
                                PredioBasico::getSuperficie,
                                Collectors.reducing(BigDecimal.ZERO, BigDecimal::add))));
        return getEstadoMayor(map);
    }

    private static Estado getEstadoMayor(Map<Estado, BigDecimal> map) {
        return map.entrySet().stream()
                .max(Comparator.comparing(e -> e.getValue()))
                .orElseThrow(() -> new IllegalArgumentException("No existe un estado."))
                .getKey();
    }
}
