/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.validador;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorPreRegistroProductor;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import org.apache.poi.ss.usermodel.Row;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Component
@Slf4j
public class ValidadorPreRegistroContrato {

    @Autowired
    private BuscadorPreRegistroProductor buscadorPreRegistroProductor;

    public void verificaPreRegistro(String curp, Cultivo cultivo, CicloAgricola ciclo, Row row) {
        if (getPreregistros(curp, cultivo, ciclo).isEmpty()) {
            LOGGER.error("[fila: " + (row.getRowNum() + 1) + "] del archivo");
            throw new SegalmexRuntimeException("Error", "El productor con CURP: " + curp
                    + " no tiene un pre registro. [fila: " + (row.getRowNum() + 1) + "] del archivo");
        }
    }

    public void verificaPreRegistro(String curp, Cultivo cultivo, CicloAgricola ciclo) {
        if (getPreregistros(curp, cultivo, ciclo).isEmpty()) {
            throw new SegalmexRuntimeException("Error", "El productor con CURP: " + curp
                    + " no tiene un pre registro.");
        }
    }

    private List<Integer> getPreregistros(String curp, Cultivo cultivo, CicloAgricola ciclo) {
        return buscadorPreRegistroProductor.busca(curp, cultivo, ciclo);
    }
}
