package mx.gob.segalmex.granos.core.productor.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.productor.InformacionAlerta;

/**
 *
 * @author oscar
 */
public interface BuscadorInformacionAlerta {

    List<InformacionAlerta> busca(ParametrosInformacionAlerta parametros);

    InformacionAlerta buscaElemento(ParametrosInformacionAlerta parametros);

    InformacionAlerta buscaElemento(String folio);

}
