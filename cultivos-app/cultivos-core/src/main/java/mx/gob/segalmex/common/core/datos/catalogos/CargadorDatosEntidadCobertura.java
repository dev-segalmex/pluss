package mx.gob.segalmex.common.core.datos.catalogos;

import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.productor.EntidadCobertura;

/**
 *
 * @author ismael
 */
public class CargadorDatosEntidadCobertura extends AbstractCargadorDatosCatalogo<EntidadCobertura> {

    @Override
    public EntidadCobertura getInstance(String[] elementos) {
        EntidadCobertura ec = new EntidadCobertura();
        ec.setClave(elementos[0]);
        ec.setNombre(elementos[1]);
        ec.setActivo(!elementos[0].equals("no-aplica"));
        ec.setOrden(Integer.parseInt(elementos[2]));
        return ec;
    }

}
