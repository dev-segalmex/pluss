/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.busqueda;

import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;

/**
 *
 * @author oscar
 */
@Getter
@Setter
public class ParametrosPredioDocumento {

    private InscripcionProductor inscripcionProductor;

    private Boolean monitoreable;

    private String numero;

    private String tipo;

    private String rfcEmisor;

    private String rfcReceptor;

    private String uuidTimbreFiscalDigital;

    private Boolean activo = Boolean.TRUE;

}
