/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.datos.catalogos;

import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;

/**
 *
 * @author ismael
 */
public class CargadorDatosEstatusInscripcion extends AbstractCargadorDatosCatalogo<EstatusInscripcion> {

    @Override
    public EstatusInscripcion getInstance(String[] elementos) {
        EstatusInscripcion estatus = new EstatusInscripcion();
        estatus.setClave(elementos[0]);
        estatus.setNombre(elementos[1]);
        estatus.setActivo(true);
        estatus.setOrden(Integer.parseInt(elementos[2]));

        return estatus;
    }

}