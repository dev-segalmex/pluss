/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato.pdf;

import com.kitfox.svg.SVGDiagram;
import com.kitfox.svg.SVGException;
import com.kitfox.svg.SVGUniverse;
import com.lowagie.text.BadElementException;
import com.lowagie.text.Image;
import com.lowagie.text.ImgTemplate;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Graphics2D;
import java.io.InputStreamReader;
import java.io.Reader;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class SvgUtil {

    public static Image getSVGImage(String svg, PdfWriter writer) {
        Reader r = new InputStreamReader(SvgUtil.class.getResourceAsStream(svg));

        SVGUniverse universe = new SVGUniverse();
        SVGDiagram diagram = universe.getDiagram(universe.loadSVG(r, svg));

        PdfTemplate template
                = PdfTemplate.createTemplate(writer, diagram.getWidth(), diagram.getHeight());
        Graphics2D graph = template.createGraphics(diagram.getWidth(), diagram.getHeight());
        //graph.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);

        try {
            diagram.render(graph);
            graph.dispose();

            return new ImgTemplate(template);
        } catch (SVGException svge) {
            LOGGER.error("Error al dibujar el SVG.", svge);
            throw new IllegalStateException(svge);
        } catch (BadElementException bee) {
            LOGGER.error("Error al procesar el archivo SVG.", bee);
            throw new IllegalStateException(bee);
        }
    }

}
