package mx.gob.segalmex.common.core.validador;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Validaro para saber si un productor ya está registrado como beneficiario.
 *
 * @author oscar
 */
@Component("validadorInscripcionProductorEnBeneficiario")
@Slf4j
public class ValidadorInscripcionProductorEnBeneficiario implements ValidadorInscripcion {

    @Autowired
    private BuscadorInscripcionProductor buscadorInscripcion;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Override
    public void valida(InscripcionProductor inscripcion) {
        LOGGER.info("Buscando si productor ya es beneficiario...");
        String curp = inscripcion.getDatosProductor().getCurp();
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setCiclo(inscripcion.getCiclo());
        parametros.setCurpBeneficiario(curp);
        parametros.setNoEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.CANCELADA.getClave()));
        List<InscripcionProductor> inscripciones = buscadorInscripcion.busca(parametros);
        if (!inscripciones.isEmpty()) {
            throw new SegalmexRuntimeException("Error al realizar el registro.",
                    "El productor con CURP " + curp + " ya se encuentra registrado como beneficiario en "
                    + "el ciclo " + inscripcion.getCiclo().getNombre());
        }

    }
}
