/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.inscripcion;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.Municipio;
import mx.gob.segalmex.pluss.modelo.catalogos.Parentesco;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoDocumento;
import mx.gob.segalmex.pluss.modelo.empresas.DatosComercializacion;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroEmpresa;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroSucursal;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoEmpresa;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoPrecio;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoSucursal;
import mx.gob.segalmex.pluss.modelo.historico.EntidadHistorico;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;
import mx.gob.segalmex.pluss.modelo.inscripcion.Predio;
import mx.gob.segalmex.pluss.modelo.productor.ContratoFirmado;
import mx.gob.segalmex.pluss.modelo.productor.CuentaBancaria;
import mx.gob.segalmex.pluss.modelo.productor.DatosProductor;
import mx.gob.segalmex.pluss.modelo.productor.EmpresaAsociada;
import mx.gob.segalmex.pluss.modelo.productor.EntidadCobertura;
import mx.gob.segalmex.pluss.modelo.productor.InformacionSemilla;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import mx.gob.segalmex.pluss.modelo.productor.RegimenHidrico;
import mx.gob.segalmex.pluss.modelo.productor.TipoPosesion;
import mx.gob.segalmex.pluss.modelo.contrato.CoberturaInscripcionContrato;
import mx.gob.segalmex.pluss.modelo.contrato.TipoCompradorCobertura;
import mx.gob.segalmex.pluss.modelo.contrato.TipoOperacionCobertura;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component("datoCapturadoHelper")
@Slf4j
public class DatoCapturadoHelper {

    /**
     * El tipo de dato capturado para archivo.
     */
    private static final String TIPO_ARCHIVO = "archivo";

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    private List<ModificadorInscripcion> modificadoresProductor;

    private List<ModificadorInscripcion> modificadoresFactura;

    public void setModificadoresProductor(List<ModificadorInscripcion> modificadoresProductor) {
        this.modificadoresProductor = modificadoresProductor;
    }

    public void setModificadoresFactura(List<ModificadorInscripcion> modificadoresFactura) {
        this.modificadoresFactura = modificadoresFactura;
    }

    public void actualizaDatosCapturados(EntidadHistorico entidad, List<DatoCapturado> datos, boolean todos) {
        switch (entidad.getClass().getSimpleName()) {
            case "InscripcionContrato":
                actualizaDatosCapturadosContrato((InscripcionContrato) entidad, datos, todos);
                break;
            case "InscripcionProductor":
                actualizaDatosCapturadosProductor((InscripcionProductor) entidad, datos, todos);
                break;
            case "InscripcionFactura":
                actualizaDatosCapturadosFactura((InscripcionFactura) entidad, datos, todos);
                break;
            case "RegistroEmpresa":
                actualizaDatosCapturadosRegistroEmpresa((RegistroEmpresa) entidad, datos, todos);
                break;
            default:
                throw new IllegalArgumentException("Clase desconocida: "
                        + entidad.getClass().getSimpleName());
        }

    }

    /**
     * Actualiza los datos capturados directamente en la inscripción.
     *
     * @param inscripcion la inscripcin a actualizar.
     * @param datos los datos capturados.
     * @param todos indica si se actualizan todos o solo aquellos que fueron corregidos.
     */
    private void actualizaDatosCapturadosContrato(InscripcionContrato inscripcion, List<DatoCapturado> datos, boolean todos) {
        Map<Integer, CoberturaInscripcionContrato> mapIars = getMapCoberturas(inscripcion.getCoberturas());

        for (DatoCapturado d : datos) {
            // Los tipos de dato capturado archivo, no se modifican aquí
            if (d.getTipo().equals(TIPO_ARCHIVO)) {
                LOGGER.warn("Omitiendo el dato capturado de archivo: {}", d.getTipo());
                continue;
            }
            // Solo actualizamos aquellos valores que no son correctos
            if (todos || (Objects.nonNull(d.getCorrecto()) && !d.getCorrecto())) {
                String[] tmp = d.getClave().split("_");
                String clave = tmp.length > 1 ? tmp[1] : tmp[0];
                String valor = d.getValor();
                LOGGER.debug("Actualizando {} con: {}", clave, valor);
                Predio p;
                CoberturaInscripcionContrato c;

                switch (clave) {
                    case "tipo":
                        inscripcion.setTipoCultivo(buscadorCatalogo.buscaElemento(TipoCultivo.class, valor));
                        break;
                    case "numero-contrato":
                        inscripcion.setNumeroContrato(valor);
                        break;
                    case "fecha-firma":
                        inscripcion.setFechaFirma(SegalmexDateUtils.parseCalendar(valor, "dd/MM/yyyy"));
                        break;
                    case "numero-productores":
                        inscripcion.setNumeroProductores(new Integer(valor));
                        break;
                    case "tipo-empresa-comprador":
                        inscripcion.setTipoEmpresaComprador(buscadorCatalogo.buscaElemento(TipoEmpresa.class, valor));
                        break;
                    case "nombre-comprador":
                        inscripcion.setNombreComprador(valor);
                        break;
                    case "rfc-comprador":
                        inscripcion.setRfcComprador(valor);
                        break;
                    case "tipo-empresa-vendedor":
                        inscripcion.setTipoEmpresaVendedor(buscadorCatalogo.buscaElemento(TipoEmpresa.class, valor));
                        break;
                    case "nombre-vendedor":
                        inscripcion.setNombreVendedor(valor);
                        break;
                    case "rfc-vendedor":
                        inscripcion.setRfcVendedor(valor);
                        break;
                    case "entidad-produce":
                        inscripcion.setEstado(buscadorCatalogo.buscaElemento(Estado.class, valor));
                        break;
                    case "superficie-total":
                        inscripcion.setSuperficie(new BigDecimal(valor));
                        break;
                    case "volumen-total":
                        inscripcion.setVolumenTotal(new BigDecimal(valor));
                        break;
                    case "tipo-precio":
                        inscripcion.setTipoPrecio(buscadorCatalogo.buscaElemento(TipoPrecio.class, valor));
                        break;
                    case "precio-futuro":
                        inscripcion.setPrecioFuturo(new BigDecimal(valor));
                        break;
                    case "base-acordada":
                        inscripcion.setBaseAcordada(new BigDecimal(valor));
                        break;
                    case "precio-fijo":
                        inscripcion.setPrecioFijo(new BigDecimal(valor));
                        break;
                    //DATOS DE LAS COBERTURAS
                    case "numero":
                        c = mapIars.get(new Integer(tmp[0]));
                        c.setNumero(valor);
                        break;
                    case "entidad-cobertura":
                        c = mapIars.get(new Integer(tmp[0]));
                        c.setEntidadCobertura(buscadorCatalogo.buscaElemento(EntidadCobertura.class, valor));
                        break;
                    case "tipo-comprador":
                        c = mapIars.get(new Integer(tmp[0]));
                        c.setTipoComprador(buscadorCatalogo.buscaElemento(TipoCompradorCobertura.class, valor));
                        break;
                    case "nombre-comprador-cobertura":
                        c = mapIars.get(new Integer(tmp[0]));
                        c.setNombreComprador(valor);
                        break;
                    case "fecha-compra":
                        c = mapIars.get(new Integer(tmp[0]));
                        c.setFechaCompra(SegalmexDateUtils.parseCalendar(valor, "dd/MM/yyyy"));
                        break;
                    case "fecha-vencimiento":
                        c = mapIars.get(new Integer(tmp[0]));
                        c.setFechaVencimiento(SegalmexDateUtils.parseCalendar(valor, "dd/MM/yyyy"));
                        break;
                    case "tipo-operacion":
                        c = mapIars.get(new Integer(tmp[0]));
                        c.setTipoOperacionCobertura(buscadorCatalogo.buscaElemento(TipoOperacionCobertura.class, valor));
                        break;
                    case "numero-contratos":
                        c = mapIars.get(new Integer(tmp[0]));
                        c.setNumeroContratos(new BigDecimal(valor));
                        break;
                    case "precio-ejercicio-dolares":
                        c = mapIars.get(new Integer(tmp[0]));
                        c.setPrecioEjercicioDolares(new BigDecimal(valor));
                        break;
                    case "precio-ejercicio-pesos":
                        c = mapIars.get(new Integer(tmp[0]));
                        c.setPrecioEjercicioPesos(new BigDecimal(valor));
                        break;
                    case "prima-dolares":
                        c = mapIars.get(new Integer(tmp[0]));
                        c.setPrimaDolares(new BigDecimal(valor));
                        break;
                    case "prima-pesos":
                        c = mapIars.get(new Integer(tmp[0]));
                        c.setPrimaPesos(new BigDecimal(valor));
                        break;
                    case "comisiones-dolares":
                        c = mapIars.get(new Integer(tmp[0]));
                        c.setComisionesDolares(new BigDecimal(valor));
                        break;
                    case "comisiones-pesos":
                        c = mapIars.get(new Integer(tmp[0]));
                        c.setComisionesPesos(new BigDecimal(valor));
                        break;
                    default:
                        throw new IllegalArgumentException("Los datos contienen valores desconocidos");
                }
            }
        }

    }

    /**
     * Actualiza los datos capturados directamente en la inscripción.
     *
     * @param inscripcion la inscripcin a actualizar.
     * @param datos los datos capturados.
     * @param todos indica si se actualizan todos o solo aquellos que fueron corregidos.
     */
    private void actualizaDatosCapturadosProductor(InscripcionProductor inscripcion, List<DatoCapturado> datos, boolean todos) {
        Map<Integer, PredioProductor> ppMap = getMapPredioProductor(inscripcion.getPredios());
        Map<Integer, ContratoFirmado> cfMap = getMapContratos(inscripcion.getContratos());
        Map<Integer, EmpresaAsociada> eaMap = getMapEmpresasAsociadas(inscripcion.getEmpresas());

        DatosProductor dp = inscripcion.getDatosProductor();
        CuentaBancaria cb = inscripcion.getCuentaBancaria();
        InformacionSemilla snics = inscripcion.getInformacionSemilla();

        for (DatoCapturado d : datos) {
            // Los tipos de dato capturado archivo, no se modifican aquí
            if (d.getTipo().equals(TIPO_ARCHIVO)) {
                LOGGER.warn("Omitiendo el dato capturado de archivo: {}", d.getTipo());
                continue;
            }
            // Solo actualizamos aquellos valores que no son correctos
            if (todos || (Objects.nonNull(d.getCorrecto()) && !d.getCorrecto())) {
                String[] tmp = d.getClave().split("_");
                String clave = tmp.length > 1 ? tmp[1] : tmp[0];
                String valor = d.getValor();
                LOGGER.debug("Actualizando {} con: {}", clave, valor);
                PredioProductor p;
                ContratoFirmado cf;
                EmpresaAsociada ea;

                switch (clave) {
                    case "nombre-sociedad":
                        dp.setNombreMoral(valor);
                        break;
                    case "rfc-sociedad":
                    case "rfc-productor":
                        break;
                    case "curp-representante":
                        break;
                    case "nombre-representante":
                    case "nombre-productor":
                        dp.setNombre(valor);
                        break;
                    case "papellido-representante":
                    case "papellido-productor":
                        dp.setPrimerApellido(valor);
                        break;
                    case "sapellido-representante":
                    case "sapellido-productor":
                        dp.setSegundoApellido(valor);
                        break;
                    case "tipo-identificacion-representante":
                    case "tipo-identificacion-productor":
                        dp.setTipoDocumento(buscadorCatalogo.buscaElemento(TipoDocumento.class, valor));
                        break;
                    case "numero-identificacion-representante":
                    case "numero-identificacion-productor":
                        dp.setNumeroDocumento(valor);
                        break;
                    case "curp-productor":
                        break;
                    case "curp-beneficiario":
                        break;
                    case "nombre-beneficiario":
                        inscripcion.setNombreBeneficiario(valor);
                        break;
                    case "apellidos-beneficiario":
                        inscripcion.setApellidosBeneficiario(valor);
                        break;
                    case "parentesco-beneficiario":
                        inscripcion.setParentesco(buscadorCatalogo.buscaElemento(Parentesco.class, valor));
                        break;
                    case "numero-contrato":
                        cf = cfMap.get(new Integer(tmp[0]));
                        cf.setNumeroContrato(valor);
                        cf.setCorregido(true);
                        break;
                    case "cantidad-contratada":
                        cf = cfMap.get(new Integer(tmp[0]));
                        cf.setCantidadContratada(new BigDecimal(valor));
                        break;
                    case "cantidad-contratada-cobertura":
                        cf = cfMap.get(new Integer(tmp[0]));
                        cf.setCantidadContratadaCobertura(new BigDecimal(valor));
                        break;
                    case "factura-rfc-sociedad":
                        ea = eaMap.get(new Integer(tmp[0]));
                        ea.setRfc(valor);
                        ea.setCorregida(true);
                        break;
                    case "folio-predio":
                        p = ppMap.get(new Integer(tmp[0]));
                        p.setFolio(valor);
                        break;
                    case "tipo-cultivo-predio":
                        p = ppMap.get(new Integer(tmp[0]));
                        p.setTipoCultivo(buscadorCatalogo.buscaElemento(TipoCultivo.class, valor));
                        break;
                    case "tipo-posesion-predio":
                        p = ppMap.get(new Integer(tmp[0]));
                        p.setTipoPosesion(buscadorCatalogo.buscaElemento(TipoPosesion.class, valor));
                        break;
                    case "documento-posesion-predio":
                        p = ppMap.get(new Integer(tmp[0]));
                        p.setTipoDocumentoPosesion(buscadorCatalogo.buscaElemento(TipoDocumento.class, valor));
                        break;
                    case "regimen-hidrico-predio":
                        p = ppMap.get(new Integer(tmp[0]));
                        p.setRegimenHidrico(buscadorCatalogo.buscaElemento(RegimenHidrico.class, valor));
                        p.setCorregido(true);
                        break;
                    case "superficie-predio":
                        p = ppMap.get(new Integer(tmp[0]));
                        p.setSuperficie(new BigDecimal(valor));
                        p.setCorregido(true);
                        break;
                    case "volumen-obtenido":
                        p = ppMap.get(new Integer(tmp[0]));
                        p.setVolumen(new BigDecimal(valor));
                        p.setCorregido(true);
                        p.setVolumenCorregido(true);
                        break;
                    case "rendimiento": // Rendimiento no se actualiza por captura
                        break;
                    case "estado-predio":
                        p = ppMap.get(new Integer(tmp[0]));
                        p.setEstado(buscadorCatalogo.buscaElemento(Estado.class, valor));
                        break;
                    case "municipio-predio":
                        p = ppMap.get(new Integer(tmp[0]));
                        p.setMunicipio(buscadorCatalogo.buscaElemento(Municipio.class, valor));
                        break;
                    case "localidad-predio":
                        p = ppMap.get(new Integer(tmp[0]));
                        p.setLocalidad(valor);
                        break;
                    case "latitud-predio":
                        p = ppMap.get(new Integer(tmp[0]));
                        p.setLatitud(new BigDecimal(valor));
                        break;
                    case "longitud-predio":
                        p = ppMap.get(new Integer(tmp[0]));
                        p.setLongitud(new BigDecimal(valor));
                        break;
                    case "latitud-1-predio":
                        p = ppMap.get(new Integer(tmp[0]));
                        p.setLatitud1(new BigDecimal(valor));
                        break;
                    case "longitud-1-predio":
                        p = ppMap.get(new Integer(tmp[0]));
                        p.setLongitud1(new BigDecimal(valor));
                        break;
                    case "latitud-2-predio":
                        p = ppMap.get(new Integer(tmp[0]));
                        p.setLatitud2(new BigDecimal(valor));
                        break;
                    case "longitud-2-predio":
                        p = ppMap.get(new Integer(tmp[0]));
                        p.setLongitud2(new BigDecimal(valor));
                        break;
                    case "latitud-3-predio":
                        p = ppMap.get(new Integer(tmp[0]));
                        p.setLatitud3(new BigDecimal(valor));
                        break;
                    case "longitud-3-predio":
                        p = ppMap.get(new Integer(tmp[0]));
                        p.setLongitud3(new BigDecimal(valor));
                        break;
                    case "latitud-4-predio":
                        p = ppMap.get(new Integer(tmp[0]));
                        p.setLatitud4(new BigDecimal(valor));
                        break;
                    case "longitud-4-predio":
                        p = ppMap.get(new Integer(tmp[0]));
                        p.setLongitud4(new BigDecimal(valor));
                        break;
                    case "banco-productor":
                        break;
                    case "clabe-productor":
                        break;
                    //Semilla certificada
                    case "snics":
                        snics.setSnics(valor);
                        break;
                    case "toneladas-snics":
                        snics.setToneladas(new BigDecimal(valor));
                        break;
                    case "folios-snics":
                        snics.setCantidadFolios(valor);
                        break;
                    case "razon-social-snics":
                        snics.setNombre(valor);
                        break;
                    case "rfc-snics":
                        snics.setRfc(valor);
                        break;
                    default:
                        throw new IllegalArgumentException("Los datos contienen valores desconocidos");
                }
            }
        }

        Map<String, DatoCapturado> mapDatos = getMapDatosCapturados(datos);
        for (ModificadorInscripcion m : modificadoresProductor) {
            m.modifica(inscripcion, mapDatos);
        }
    }

    private void actualizaDatosCapturadosFactura(InscripcionFactura inscripcion, List<DatoCapturado> datos, boolean todos) {
        for (DatoCapturado d : datos) {
            // Los tipos de dato capturado archivo, no se modifican aquí
            if (d.getTipo().equals(TIPO_ARCHIVO)) {
                LOGGER.warn("Omitiendo el dato capturado de archivo: {}", d.getTipo());
                continue;
            }
            // Solo actualizamos aquellos valores que no son correctos
            if (todos || (Objects.nonNull(d.getCorrecto()) && !d.getCorrecto())) {
                String[] tmp = d.getClave().split("_");
                String clave = tmp.length > 1 ? tmp[1] : tmp[0];
                String valor = d.getValor();
                LOGGER.debug("Actualizando {} con: {}", clave, valor);
                switch (clave) {
                    case "numero-contrato":
                        inscripcion.setContrato(valor);
                        break;
                    case "tipo-cultivo":
                        inscripcion.setTipoCultivo(buscadorCatalogo.buscaElemento(TipoCultivo.class, valor));
                        break;
                    case "estado-factura":
                        inscripcion.setEstado(buscadorCatalogo.buscaElemento(Estado.class, valor));
                        break;
                    case "cantidad-comprobante-pago":
                        inscripcion.setCantidadComprobantePago(new BigDecimal(valor));
                        break;
                    case "precio-tonelada-real":
                        inscripcion.setPrecioToneladaReal(new BigDecimal(valor));
                        break;
                    case "fecha-pago":
                        inscripcion.setFechaPago(SegalmexDateUtils.parseCalendar(valor, "dd/MM/yyyy"));
                        break;
                    default:
                        throw new IllegalArgumentException("Los datos contienen valores desconocidos.");
                }
            }
        }

        Map<String, DatoCapturado> mapDatos = getMapDatosCapturados(datos);
        for (ModificadorInscripcion m : modificadoresFactura) {
            m.modifica(inscripcion, mapDatos);
        }
    }

    public void actualizaDatosCapturadosRegistroEmpresa(RegistroEmpresa registro, List<DatoCapturado> datos, boolean todos) {
        Map<Integer, DatosComercializacion> mapCompra = getDatosComercializacionCompra(registro.getDatosComercializacion());
        Map<Integer, DatosComercializacion> mapVenta = getDatosComercializacionVenta(registro.getDatosComercializacion());
        Map<Integer, RegistroSucursal> mapSucursal = getRegistroSucursal(registro.getSucursales());
        for (DatoCapturado d : datos) {
            /// Los tipos de dato capturado archivo, no se modifican aquí
            if (d.getTipo().equals(TIPO_ARCHIVO)) {
                LOGGER.warn("Omitiendo el dato capturado de archivo: {}", d.getTipo());
                continue;
            }
            if (todos || (Objects.nonNull(d.getCorrecto()) && !d.getCorrecto())) {
                String[] tmp = d.getClave().split("_");
                String clave = tmp.length > 1 ? tmp[1] : tmp[0];
                String valor = d.getValor();
                DatosComercializacion dCompra;
                DatosComercializacion dVenta;
                RegistroSucursal sucursal;
                LOGGER.debug("Actualizando {} con: {}", clave, valor);
                switch (clave) {
                    case "nombre-empresa":
                        registro.setNombre(valor);
                        break;
                    case "tipo-empresa":
                        registro.setTipoSucursal(buscadorCatalogo.buscaElemento(TipoSucursal.class, valor));
                        break;
                    case "responsable-empresa":
                        registro.setNombreResponsable(valor);
                        break;
                    case "telefono-oficina-empresa":
                        registro.setTelefonoOficina(valor);
                        break;
                    case "extension-adicional":
                        registro.setExtensionAdicional(valor);
                        break;
                    case "calle-empresa":
                        registro.getDomicilio().setCalle(valor);
                        break;
                    case "numero-exterior-empresa":
                        registro.getDomicilio().setNumeroExterior(valor);
                        break;
                    case "numero-interior-empresa":
                        registro.getDomicilio().setNumeroInterior(valor);
                        break;
                    case "localidad-empresa":
                        registro.getDomicilio().setLocalidad(valor);
                        break;
                    case "cp-empresa":
                        registro.getDomicilio().setCodigoPostal(valor);
                        break;
                    case "estado-empresa":
                        registro.getDomicilio().setEstado(buscadorCatalogo.buscaElemento(Estado.class, valor));
                        break;
                    case "municipio-empresa":
                        registro.getDomicilio().setMunicipio(buscadorCatalogo.buscaElemento(Municipio.class, valor));
                        break;
                    case "enlace-empresa":
                        registro.setNombreEnlace(valor);
                        break;
                    case "celular-enlace-empresa":
                        registro.setNumeroCelularEnlace(valor);
                        break;
                    case "email-enlace-empresa":
                        registro.setCorreoEnlace(valor);
                        break;
                    case "existe-equipo-empresa":
                        registro.setExisteEquipoAnalisis(Boolean.valueOf(valor));
                        break;
                    case "normas-calidad-empresa":
                        registro.setAplicaNormasCalidad(Boolean.valueOf(valor));
                        break;
                    case "tipo-posesion-empresa":
                        registro.setTipoPosesion(buscadorCatalogo.buscaElemento(TipoPosesion.class, valor));
                        break;
                    case "tipo-almacenamiento-empresa":
                        registro.setTipoAlmacenamiento(valor);
                        break;
                    case "latitud-empresa":
                        registro.setLatitud(new BigDecimal(valor));
                        break;
                    case "longitud-empresa":
                        registro.setLongitud(new BigDecimal(valor));
                        break;
                    case "numero-almacenamiento":
                        registro.setNumeroAlmacenes(Integer.parseInt(valor));
                        break;
                    case "capacidad-almacenamiento":
                        registro.setCapacidad(new BigDecimal(valor));
                        break;
                    case "volumen-actual-almacenamiento":
                        registro.setVolumenActual(new BigDecimal(valor));
                        break;
                    case "superficie-almacenamiento":
                        registro.setSuperficie(new BigDecimal(valor));
                        break;
                    case "aplica-almacenadora-almacenamiento":
                        registro.setHabilitaAlmacenadora(Boolean.valueOf(valor));
                        break;
                    case "almacenadora-almacenamiento":
                        registro.setNombreAlmacenadora(valor);
                        break;
                    /* Datos de comercialización */
                    case "actores-compra":
                        dCompra = mapCompra.get(new Integer(tmp[0]));
                        dCompra.setNombreActor(valor);
                        break;
                    case "estado-compra":
                        dCompra = mapCompra.get(new Integer(tmp[0]));
                        dCompra.setEstado(buscadorCatalogo.buscaElemento(Estado.class, valor));
                        break;
                    case "municipio-compra":
                        dCompra = mapCompra.get(new Integer(tmp[0]));
                        dCompra.setMunicipio(buscadorCatalogo.buscaElemento(Municipio.class, valor));
                        break;
                    case "anio-compra":
                        dCompra = mapCompra.get(new Integer(tmp[0]));
                        dCompra.setAnio(Integer.parseInt(valor));
                        break;
                    case "cantidad-compra":
                        dCompra = mapCompra.get(new Integer(tmp[0]));
                        dCompra.setCantidad(new BigDecimal(valor));
                        break;
                    case "actores-venta":
                        dVenta = mapVenta.get(new Integer(tmp[0]));
                        dVenta.setNombreActor(valor);
                        break;
                    case "estado-venta":
                        dVenta = mapVenta.get(new Integer(tmp[0]));
                        dVenta.setEstado(buscadorCatalogo.buscaElemento(Estado.class, valor));
                        break;
                    case "municipio-venta":
                        dVenta = mapVenta.get(new Integer(tmp[0]));
                        dVenta.setMunicipio(buscadorCatalogo.buscaElemento(Municipio.class, valor));
                        break;
                    case "anio-venta":
                        dVenta = mapVenta.get(new Integer(tmp[0]));
                        dVenta.setAnio(Integer.parseInt(valor));
                        break;
                    case "cantidad-venta":
                        dVenta = mapVenta.get(new Integer(tmp[0]));
                        dVenta.setCantidad(new BigDecimal(valor));
                        break;
                    /* Bodegas o centros de acopio */
                    case "nombre-sucursal":
                        sucursal = mapSucursal.get(new Integer(tmp[0]));
                        sucursal.setNombre(valor);
                        break;
                    case "calle-sucursal":
                        sucursal = mapSucursal.get(new Integer(tmp[0]));
                        sucursal.getDomicilio().setCalle(valor);
                        break;
                    case "numero-exterior-sucursal":
                        sucursal = mapSucursal.get(new Integer(tmp[0]));
                        sucursal.getDomicilio().setNumeroExterior(valor);
                        break;
                    case "numero-interior-sucursal":
                        sucursal = mapSucursal.get(new Integer(tmp[0]));
                        sucursal.getDomicilio().setNumeroInterior(valor);
                        break;
                    case "localidad-sucursal":
                        sucursal = mapSucursal.get(new Integer(tmp[0]));
                        sucursal.getDomicilio().setLocalidad(valor);
                        break;
                    case "cp-sucursal":
                        sucursal = mapSucursal.get(new Integer(tmp[0]));
                        sucursal.getDomicilio().setCodigoPostal(valor);
                        break;
                    case "estado-sucursal":
                        sucursal = mapSucursal.get(new Integer(tmp[0]));
                        sucursal.getDomicilio().setEstado(buscadorCatalogo.buscaElemento(Estado.class, valor));
                        break;
                    case "municipio-sucursal":
                        sucursal = mapSucursal.get(new Integer(tmp[0]));
                        sucursal.getDomicilio().setMunicipio(buscadorCatalogo.buscaElemento(Municipio.class, valor));
                        break;
                    case "latitud-sucursal":
                        sucursal = mapSucursal.get(new Integer(tmp[0]));
                        sucursal.setLatitud(new BigDecimal(valor));
                        break;
                    case "longitud-sucursal":
                        sucursal = mapSucursal.get(new Integer(tmp[0]));
                        sucursal.setLongitud(new BigDecimal(valor));
                        break;
                    case "enlace-sucursal":
                        sucursal = mapSucursal.get(new Integer(tmp[0]));
                        sucursal.setNombreEnlace(valor);
                        break;
                    case "celular-enlace-sucursal":
                        sucursal = mapSucursal.get(new Integer(tmp[0]));
                        sucursal.setNumeroCelularEnlace(valor);
                        break;
                    case "correo-enlace-sucursal":
                        sucursal = mapSucursal.get(new Integer(tmp[0]));
                        sucursal.setCorreoEnlace(valor);
                        break;
                    default:
                        throw new IllegalArgumentException("Los datos contienen valores desconocidos.");
                }
            }
        }
    }

    public Map<Integer, PredioProductor> getMapPredioProductor(List<PredioProductor> predios) {
        Map<Integer, PredioProductor> map = new HashMap<>();
        for (PredioProductor p : predios) {
            map.put(p.getOrden(), p);
        }
        return map;
    }

    public Map<Integer, ContratoFirmado> getMapContratos(List<ContratoFirmado> contratos) {
        Map<Integer, ContratoFirmado> map = new HashMap<>();
        for (ContratoFirmado cf : contratos) {
            map.put(cf.getOrden(), cf);
        }
        return map;
    }

    public Map<Integer, DatosComercializacion> getDatosComercializacionCompra(List<DatosComercializacion> datosComercializacion) {
        Map<Integer, DatosComercializacion> map = new HashMap<>();
        for (DatosComercializacion dc : datosComercializacion) {
            if (dc.getTipo().equals("compra")) {
                map.put(dc.getOrden(), dc);
            }
        }
        return map;
    }

    public Map<Integer, DatosComercializacion> getDatosComercializacionVenta(List<DatosComercializacion> datosComercializacion) {
        Map<Integer, DatosComercializacion> map = new HashMap<>();
        for (DatosComercializacion dc : datosComercializacion) {
            if (dc.getTipo().equals("venta")) {
                map.put(dc.getOrden(), dc);
            }
        }
        return map;
    }

    public Map<Integer, RegistroSucursal> getRegistroSucursal(List<RegistroSucursal> sucursales) {
        Map<Integer, RegistroSucursal> map = new HashMap<>();
        for (RegistroSucursal rs : sucursales) {
            map.put(rs.getOrden(), rs);
        }
        return map;
    }

    public Map<Integer, CoberturaInscripcionContrato> getMapCoberturas(List<CoberturaInscripcionContrato> coberturas) {
        Map<Integer, CoberturaInscripcionContrato> map = new HashMap<>();
        for (CoberturaInscripcionContrato c : coberturas) {
            map.put(c.getOrden(), c);
        }
        return map;
    }

    public Map<Integer, EmpresaAsociada> getMapEmpresasAsociadas(List<EmpresaAsociada> empresasAsociadas) {
        Map<Integer, EmpresaAsociada> map = new HashMap<>();
        for (EmpresaAsociada ea : empresasAsociadas) {
            map.put(ea.getOrden(), ea);
        }
        return map;
    }

    private Map<String, DatoCapturado> getMapDatosCapturados(List<DatoCapturado> datos) {
        Map<String, DatoCapturado> map = new HashMap<>();
        for (DatoCapturado d : datos) {
            map.put(d.getClave(), d);
        }
        return map;
    }
}
