/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorFacturaRestringida;
import mx.gob.segalmex.common.core.factura.busqueda.ParametrosFacturaRestringida;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.pluss.modelo.factura.FacturaRestringida;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

/**
 *
 * @author jurgen
 */
@Service("procesadorFacturaRestringida")
@Slf4j
public class ProcesadorFacturaRestringidaJpa implements ProcesadorFacturaRestringida {

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private BuscadorFacturaRestringida buscadorFacturaRestringida;

    @Override
    public FacturaRestringida procesa(FacturaRestringida facturaRestringida) {
        LOGGER.info("Guardando nueva FacturaRestringida con UUID: {}", facturaRestringida.getUuidTimbreFiscalDigital());
        List<String> motivos = new ArrayList<>();
        validaExiste(facturaRestringida.getUuidTimbreFiscalDigital(), motivos);
        if (!motivos.isEmpty()) {
            throw new SegalmexRuntimeException("Error al registrar la Factura", motivos);
        }
        registroEntidad.guarda(facturaRestringida);
        return facturaRestringida;
    }

    @Override
    public void elimina(FacturaRestringida fr, Usuario usuarioBaja) {
        LOGGER.debug("Eliminando excepción de Factura con UUID: ", fr.getUuidTimbreFiscalDigital());
        fr.setActivo(false);
        fr.setFechaBaja(Calendar.getInstance());
        fr.setUsuarioBaja(usuarioBaja);
        registroEntidad.actualiza(fr);
    }

    private void validaExiste(String uuid, List<String> motivos) {
        try {
            ParametrosFacturaRestringida params = new ParametrosFacturaRestringida();
            params.setUuidTimbreFiscalDigital(uuid);
            params.setActivo(Boolean.TRUE);
            buscadorFacturaRestringida.buscaElemento(params);
            motivos.add("Esta factura ya ha sido registrada.");
        } catch (EmptyResultDataAccessException ouch) {
            LOGGER.debug("No existe Factura para ese UUID.");
        }
    }
}
