/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.inscripcion;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.inscripcion.ComentarioInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.Inscripcion;

/**
 *
 * @author ismael
 */
public interface BuscadorComentarioInscripcion {

    List<ComentarioInscripcion> busca(Inscripcion inscripcion);
}
