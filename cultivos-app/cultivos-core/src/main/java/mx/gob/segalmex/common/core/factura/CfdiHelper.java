/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura;

import java.math.BigDecimal;
import mx.gob.segalmex.pluss.modelo.factura.Cfdi;
import mx.gob.segalmex.pluss.modelo.factura.ConceptoCfdi;
import mx.gob.segalmex.pluss.modelo.factura.EstatusConceptoCfdiEnum;

/**
 *
 * @author oscar
 */
public class CfdiHelper {

    private CfdiHelper() {
    }

    public static BigDecimal calculaSubtotal(Cfdi cfdi) {

        BigDecimal subtotal = BigDecimal.ZERO;
        for (ConceptoCfdi c : cfdi.getConceptos()) {
            if (!c.getEstatus().equals(EstatusConceptoCfdiEnum.IGNORADO.getClave())) {
                subtotal = subtotal.add(c.getImporte());
            }
        }
        return subtotal;
    }

}
