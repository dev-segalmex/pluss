/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato.xls;

import java.math.BigDecimal;
import java.util.List;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoPrecio;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;
import mx.gob.segalmex.pluss.modelo.inscripcion.Predio;

/**
 *
 * @author oscar
 */
public abstract class AbstractConstratoXlsHelper {

    public abstract byte[] exporta(List<InscripcionContrato> inscripciones);

    protected Predio getPredio(String tipoCultivo, List<Predio> predios) {

        Predio predioRegreso = new Predio();
        for (Predio p : predios) {
            if (p.getTipoCultivo().getClave().equals(tipoCultivo)) {
                predioRegreso.setTipoPrecio(new TipoPrecio());
                predioRegreso.getTipoPrecio().setNombre(p.getTipoPrecio().getNombre());
                predioRegreso.setPrecioFuturo(p.getPrecioFuturo());
                predioRegreso.setBaseAcordada(p.getBaseAcordada());
                predioRegreso.setPrecioFijo(p.getPrecioFijo());

            } else {
                predioRegreso.setTipoPrecio(new TipoPrecio());
                predioRegreso.getTipoPrecio().setNombre("--");
                predioRegreso.setPrecioFuturo(BigDecimal.ZERO);
                predioRegreso.setBaseAcordada(BigDecimal.ZERO);
                predioRegreso.setPrecioFijo(BigDecimal.ZERO);

            }
        }
        return predioRegreso;

    }
}
