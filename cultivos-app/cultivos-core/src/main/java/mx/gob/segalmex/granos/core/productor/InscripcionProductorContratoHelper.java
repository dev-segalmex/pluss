/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor;

import java.util.ArrayList;
import java.util.Objects;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.granos.core.contrato.busqueda.BuscadorInscripcionContrato;
import mx.gob.segalmex.granos.core.contrato.busqueda.ParametrosInscripcionContrato;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;
import mx.gob.segalmex.pluss.modelo.productor.ContratoFirmado;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component
public class InscripcionProductorContratoHelper {

    @Autowired
    private BuscadorInscripcionContrato buscadorInscripcionContrato;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    public void modifica(InscripcionProductor origen, InscripcionProductor modificado) {
        if (origen.getCultivo().getClave().equals(CultivoEnum.ARROZ.getClave())) {
            return;
        }

        if (Objects.isNull(origen.getContratos())) {
            origen.setContratos(new ArrayList<>());
        }
        if (origen.getNumeroContratos() == origen.getContratosActivos().size()) {
            return;
        }

        if (Objects.isNull(modificado.getContratos())) {
            modificado.setContratos(new ArrayList<>());
        }
        if (origen.getNumeroContratos() != origen.getContratosActivos().size() + modificado.getContratos().size()) {
            throw new SegalmexRuntimeException("Error en el registro del productor.",
                    "El número de contratos no coincide con el total especificado.");
        }

        origen.getContratos().addAll(modificado.getContratos());
        int i = 0;
        for (ContratoFirmado c : origen.getContratos()) {
            inicializa(c);
            c.setInscripcionProductor(origen);
            c.setOrden(i++);
        }
        origen.setNumeroContratos(origen.getContratosActivos().size());
    }

    public void inicializa(ContratoFirmado cf) {
        ParametrosInscripcionContrato params = new ParametrosInscripcionContrato();
        params.setNumeroContrato(cf.getNumeroContrato());
        params.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.POSITIVA.getClave()));
        InscripcionContrato ic = buscadorInscripcionContrato.buscaElemento(params);
        cf.setFolio(ic.getFolio());
        cf.setEmpresa(ic.getSucursal().getEmpresa().getNombre());
        cf.setTipoPrecio(ic.getTipoPrecio());
        cf.setTipoCultivo(ic.getTipoCultivo());
    }
}
