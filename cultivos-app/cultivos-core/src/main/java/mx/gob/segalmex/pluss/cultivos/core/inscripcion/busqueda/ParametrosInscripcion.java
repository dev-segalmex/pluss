/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.core.inscripcion.busqueda;

import java.util.Calendar;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.Empresa;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;

/**
 *
 * @author ismael
 */
@Getter
@Setter
public class ParametrosInscripcion {

    /**
     * El identificador de la inscripción en la base de datos.
     */
    private Integer id;

    /**
     * El uuid de inscripción de un contrato.
     */
    private String uuid;

    /**
     * El folio de inscripción.
     */
    private String folio;

    /**
     * La fecha de inicio para el rango de búsqueda de las inscripciones (inclusive).
     */
    private Calendar fechaInicio;

    /**
     * La fecha de fin para el rango de búsqueda de las inscripciones (no incluída).
     */
    private Calendar fechaFin;

    /**
     * El RFC de la empresa que hace la inscripción.
     */
    private String rfcEmpresa;

    /**
     * El RFC de la bodega/molino.
     */
    private String rfcBodega;

    /**
     * El estatus de la inscripción.
     */
    private EstatusInscripcion estatus;

    /**
     * La empresa que hace la inscripción.
     */
    private Empresa empresa;

    /**
     * La sucursal que hace la inscripción.
     */
    private Sucursal sucursal;

    /**
     * El estatus de la inscripción que no se busca.
     */
    private EstatusInscripcion noEstatus;

    /**
     * El validador de la inscripción.
     */
    private Usuario validador;

    /**
     * Indica si se requiere simplificar la inscripción al momento de la respuesta.
     */
    private Boolean simplifica;

    /**
     * El cultivo al cual pertenece la inscripción.
     */
    private Cultivo cultivo;

    /**
     * El ciclo agrícola al cual pertenece la inscripción.
     */
    private CicloAgricola ciclo;

    /**
     * El estado al cual pertenece la inscripción.
     */
    private Estado estado;
}
