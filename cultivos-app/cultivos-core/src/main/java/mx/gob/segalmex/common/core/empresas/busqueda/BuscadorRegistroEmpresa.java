/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.empresas.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroEmpresa;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroEmpresaBasico;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroSucursal;

/**
 *
 * @author ismael
 */
public interface BuscadorRegistroEmpresa {

    RegistroEmpresa buscaElemento(String rfc, String uuid);

    RegistroEmpresa buscaElemento(ParametrosRegistroEmpresa parametros);

    List<RegistroEmpresa> busca(ParametrosRegistroEmpresa parametros);

    RegistroEmpresaBasico buscaElementoBasico(String rfc, String uuid);

    List<RegistroEmpresaBasico> buscaBasico(ParametrosRegistroEmpresa parametros);

    RegistroEmpresaBasico buscaElementoBasico(ParametrosRegistroEmpresa parametros);

    List<RegistroSucursal> busca(List<String> nombres);
}
