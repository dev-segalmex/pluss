/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioDocumento;

/**
 *
 * @author oscar
 */
public interface BuscadorPredioDocumento {

    /**
     * Se encarga de obtener una lista de {@link PredioDocumento} dada la
     * {@link InscripcionProductor} a la que correspondan.
     *
     * @param ip la {@link InscripcionProductor} a la que correspnden.
     * @return una lista de {@link PredioDocumento} con los encontrados.
     */
    List<PredioDocumento> busca(InscripcionProductor ip);

    /**
     * Obtiene una lista de {@link PredioDocumento} que cumplen con los
     * parámetros recibidos.
     *
     * @param parametros
     * @return la lista de {@link PredioDocumento} encontrada.
     */
    List<PredioDocumento> busca(ParametrosPredioDocumento parametros);

}
