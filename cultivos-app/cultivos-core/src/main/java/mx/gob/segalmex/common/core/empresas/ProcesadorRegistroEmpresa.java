/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.empresas;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroEmpresa;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroEmpresaBasico;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;

/**
 *
 * @author ismael
 */
public interface ProcesadorRegistroEmpresa {

    RegistroEmpresaBasico registra(RegistroEmpresaBasico registro, Usuario u);

    RegistroEmpresa procesa(RegistroEmpresaBasico basico, RegistroEmpresa registro);

    void asigna(RegistroEmpresa registro, Usuario asigna, Usuario validador);

    void validaPositivo(RegistroEmpresa registro, Usuario usuario, List<DatoCapturado> capturados);

    void validaNegativo(RegistroEmpresa registro, Usuario usuario, List<DatoCapturado> capturados);

    void corrige(RegistroEmpresa registro, List<DatoCapturado> capturados);
}
