/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.busqueda;

import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;

/**
 *
 * @author jurgen
 */
@Getter
@Setter
public class ParametrosProductor {
    
    private ProductorCiclo productorCiclo;

    private String grupoEstimulo;
}
