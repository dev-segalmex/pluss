package mx.gob.segalmex.common.core.validador;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorPreRegistroProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PreRegistroProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Se encarga ve verificar que la curp del productor no este ya registrado como
 * beneficiario y que la curp del beneficiario no este ya registrado como
 * productor.
 *
 * @author oscar
 */
@Component("validadorPreRegistroProductorBeneficiario")
@Slf4j
public class ValidadorPreRegistroProductorBeneficiario implements ValidadorPreRegistro {

    @Autowired
    private BuscadorPreRegistroProductor buscadorPreRegistro;

    @Override
    public void valida(PreRegistroProductor preRegistro) {
        LOGGER.info("Validando curp pruductor en beneficiario y viceversa.");
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setCultivo(preRegistro.getCultivo());
        parametros.setCiclo(preRegistro.getCiclo());
        parametros.setCurp(null);
        parametros.setCurpBeneficiario(preRegistro.getCurp());
        List<PreRegistroProductor> preRegistros = buscadorPreRegistro.busca(parametros);
        if (!preRegistros.isEmpty()) {
            throw new SegalmexRuntimeException("Error al realizar el Pre Registro.",
                    "El productor con CURP " + preRegistro.getCurp()
                    + " ya se encuentra registrado como beneficiario.");
        }
        parametros.setCurp(preRegistro.getCurpBeneficiario());
        parametros.setCurpBeneficiario(null);
        preRegistros = buscadorPreRegistro.busca(parametros);
        if (!preRegistros.isEmpty()) {
            throw new SegalmexRuntimeException("Error al realizar el Pre Registro.",
                    "El beneficiario con CURP " + preRegistro.getCurpBeneficiario()
                    + " ya se encuentra registrado como productor.");
        }
    }
}
