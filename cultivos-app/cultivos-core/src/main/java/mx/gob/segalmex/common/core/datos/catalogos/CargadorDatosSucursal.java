package mx.gob.segalmex.common.core.datos.catalogos;

import java.math.BigDecimal;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.Municipio;
import mx.gob.segalmex.pluss.modelo.catalogos.Empresa;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoSucursal;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ismael
 */
public class CargadorDatosSucursal extends AbstractCargadorDatosCatalogo<Sucursal> {

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Override
    public Sucursal getInstance(String[] elementos) {
        Sucursal sucursal = new Sucursal();
        sucursal.setClave(elementos[0]);
        sucursal.setNombre(elementos[1]);
        sucursal.setActivo(true);
        sucursal.setOrden(Integer.parseInt(elementos[2]));
        sucursal.setLocalidad(StringUtils.trimToNull(elementos[3]));
        sucursal.setEstado(buscadorCatalogo.buscaElemento(Estado.class, elementos[4]));
        sucursal.setMunicipio(buscadorCatalogo.buscaElemento(Municipio.class, elementos[5]));
        sucursal.setEmpresa(buscadorCatalogo.buscaElemento(Empresa.class, elementos[6]));
        sucursal.setTipo(buscadorCatalogo.buscaElemento(TipoSucursal.class, elementos[7]));
        sucursal.setDireccion(elementos[8]);
        sucursal.setLongitud(new BigDecimal(elementos[9]));
        sucursal.setLatitud(new BigDecimal(elementos[10]));
        return sucursal;
    }

}
