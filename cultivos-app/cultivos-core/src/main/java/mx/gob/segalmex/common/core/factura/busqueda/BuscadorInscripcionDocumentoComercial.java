/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionDocumentoComercial;

/**
 *
 * @author ismael
 */
public interface BuscadorInscripcionDocumentoComercial {

    InscripcionDocumentoComercial buscaElemento(Integer id);

    InscripcionDocumentoComercial buscaElemento(String uuid);

    List<InscripcionDocumentoComercial> busca(ParametrosInscripcionDocumentoComercial parametros);
}
