package mx.gob.segalmex.common.core.datos.catalogos;

import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.Empresa;

/**
 *
 * @author ismael
 */
public class CargadorDatosEmpresa extends AbstractCargadorDatosCatalogo<Empresa> {

    @Override
    public Empresa getInstance(String[] elementos) {
        Empresa empresa = new Empresa();
        empresa.setClave(elementos[0]);
        empresa.setNombre(elementos[1]);
        empresa.setActivo(true);
        empresa.setOrden(Integer.parseInt(elementos[2]));
        empresa.setRfc(elementos[3]);
        return empresa;
    }

}
