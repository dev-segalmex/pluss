/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.inscripcion;

import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.Inscripcion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Component
public class ActualizadorEstatusHelper {

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    public void cambiaEstatusInscripcion(Inscripcion inscripcion, EstatusInscripcion nuevo) {
        inscripcion.setEstatusPrevio(inscripcion.getEstatus());
        inscripcion.setEstatus(nuevo);
    }

    public void cambiaEstatusInscripcion(Inscripcion inscripcion, EstatusInscripcionEnum nuevo) {
        EstatusInscripcion estatus = buscadorCatalogo.buscaElemento(EstatusInscripcion.class, nuevo);
        cambiaEstatusInscripcion(inscripcion, estatus);
    }
}
