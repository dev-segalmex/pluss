/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato;

import mx.gob.segalmex.granos.core.inscripcion.DatoCapturadoEnum;

/**
 *
 * @author ismael
 */
public enum DatoCapturadoContratoEnum implements DatoCapturadoEnum {

    /**
     * El tipo de cultivo.
     */
    TIPO_CULTIVO("tipo", "Tipo"),
    /**
     * El número de contrato.
     */
    NO_CONTRATO("numero-contrato", "No. de contrato"),
    /**
     * La fecha de firma.
     */
    FECHA_FIRMA("fecha-firma", "Fecha de firma"),
    /**
     * El número de productores que amapar el contrato.
     */
    NO_PRODUCTORES("numero-productores", "No. de productores"),
    /**
     * La entidad de destino del grano.
     */
    ENTIDAD_DESTINO("entidad-destino", "Entidad destino del grano"),
    /**
     * El tipo de empresa del comprador.
     */
    TIPO_EMPRESA_COMPRADOR("tipo-empresa-comprador", "Tipo de empresa del comprador"),
    /**
     * El nombre del comprador.
     */
    NOMBRE_COMPRADOR("nombre-comprador", "Nombre del comprador"),
    /**
     * El RFC del comprador.
     */
    RFC_COMPRADOR("rfc-comprador", "RFC del comprador"),
    /**
     * El tipo de empresa del vendedor.
     */
    TIPO_EMPRESA_VENDEDOR("tipo-empresa-vendedor", "Tipo de empresa del vendedor"),
    /**
     * El nombre del vendedor.
     */
    NOMBRE_VENDEDOR("nombre-vendedor", "Nombre del vendedor"),
    /**
     * El RFC del vendedor.
     */
    RFC_VENDEDOR("rfc-vendedor", "RFC del vendedor"),
    /**
     * La entidad donde se produce el grano.
     */
    ENTIDAD_PRODUCE("entidad-produce", "Entidad donde se produce el grano"),
    /**
     * La superficie total contratada.
     */
    SUPERFICIE_TOTAL("superficie-total", "Superficie total contratada"),
    /**
     * El volumen total contratado.
     */
    VOLUMEN_TOTAL("volumen-total", "Volumen total contratado"),
    /**
     * El tipo de precio.
     */
    TIPO_PRECIO("tipo-precio", "Tipo de precio"),

    PRECIO_FUTURO("precio-futuro", "Precio futuro (PFT)"),

    BASE_ACORDADA("base-acordada", "Base acordada (B)"),

    PRECIO_FIJO("precio-fijo", "Precio fijo (PFT + B)"),

    CONTRATO_ADENDUM_PDF("contrato-adendum-pdf", "Contrato adendum PDF"),

    CONTRATO_PDF("contrato-pdf", "Contrato PDF"),

    CURPS_XLSX("curps-xlsx-tmp", "CURPs de productores (XLSX)"),

    CURPS_PRODUCTORES_ADENDUM_XLSX("curps-productores-adendum-xlsx-tmp", "CURPs de productores adendum (XLSX)"),

    /* COBERTURAS IAR*/
    NUMERO("numero", "Número de cuenta o folio"),

    ENTIDAD_COBERTURA("entidad-cobertura", "Correduría u operador"),

    TIPO_COMPRADOR("tipo-comprador", "Comprador de la cobertura"),

    NOMBRE_COMPRADOR_COBERTURA("nombre-comprador-cobertura", "Nombre del comprador de la cobertura"),

    FECHA_COMPRA("fecha-compra", "Fecha de compra"),

    FECHA_VENCIMIENTO("fecha-vencimiento", "Fecha de vencimiento"),

    TIPO_OPERACION("tipo-operacion", "Tipo de operación"),

    NUMERO_CONTRATOS("numero-contratos", "Número de contratos"),

    PRECIO_EJERCICIO_DOLARES("precio-ejercicio-dolares", "Precio de ejercicio (dólares)"),

    PRECIO_EJERCICIO_PESOS("precio-ejercicio-pesos", "Precio de ejercicio (pesos)"),

    PRIMA_DOLARES("prima-dolares", "Prima por cobertura (dólares)"),

    PRIMA_PESOS("prima-pesos", "Prima por cobertura (pesos)"),

    COMISIONES_DOLARES("comisiones-dolares", "Comisiones (dólares)"),

    COMISIONES_PESOS("comisiones-pesos", "Comisiones (pesos)"),
    /* ANEXOS DE LAS COBERTURAS*/
    CONTRATO_MARCO_PDF("contrato-marco-iar-pdf", "Contrato marco PDF"),

    ESTADO_CUENTA_PDF("estado-cuenta-iar-pdf", "Estado de cuenta o posiciones PDF"),

    CARTA_CONFIRMACION_PDF("carta-confirmacion-iar-pdf", "Carta confirmación de compra PDF"),

    PODER_PDF("poder-mandato-iar-pdf", "Poder o mandato simple para la toma de cobertura PDF"),

    CONSTANCIA_SNICS_PDF("snics-pdf","Constancia SNICS (PDF)");

    /**
     * La clave del dato capturado.
     */
    private final String clave;

    /**
     * El nombre del datos capturado.
     */
    private final String nombre;

    private DatoCapturadoContratoEnum(String clave, String nombre) {
        this.clave = clave;
        this.nombre = nombre;
    }

    @Override
    public String getClave() {
        return clave;
    }

    @Override
    public String getNombre() {
        return nombre;
    }

}
