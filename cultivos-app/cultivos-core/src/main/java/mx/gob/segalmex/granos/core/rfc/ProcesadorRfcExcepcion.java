/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.rfc;

import java.io.InputStream;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.productor.RfcExcepcion;

/**
 * Definición de las acciones que se pueden realizar sobre un ejemplar de
 * {@link RfcExcepcion}.
 *
 * @author oscar
 */
public interface ProcesadorRfcExcepcion {

    /**
     * Se encarga de procesar y guardar el rfc con su constancia en un ejemplar
     * de {@link RfcExcepcion}.
     *
     * @param rfc el rfc a guardar como excepción.
     * @param usuario el usuario que registra el {@link RfcExcepcion}.
     * @param is la constancia del RFC.
     * @param nombreArchivo el nombre del archivo de la constancia.
     * @return regresa el {@link RfcExcepcion} generado.
     */
    RfcExcepcion procesa(String rfc, Usuario usuario, InputStream is, String nombreArchivo);

    /**
     *
     * @param re
     * @param usuarioBaja
     */
    void elimina(RfcExcepcion re, Usuario usuarioBaja);
}
