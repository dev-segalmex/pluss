/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.validador;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.granos.core.historico.BuscadorHistoricoRegistro;
import mx.gob.segalmex.granos.core.historico.ParametrosHistoricoRegistro;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInformacionAlerta;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInformacionAlerta;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.productor.InformacionAlerta;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


/**
 *
 * @author jurgen
 */
@Slf4j
@Component
public class ValidadorProductorDeudor {

    @Autowired
    private BuscadorInformacionAlerta buscadorInformacionAlerta;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorHistoricoRegistro buscadorHistoricoRegistro;

    public void valida(ParametrosInformacionAlerta parametros, InscripcionProductor ip) {
        if (permitePositivoDeudor(ip)) {
            return;
        }
        List<String> motivos = buscadorInformacionAlerta.busca(parametros).stream()
                .filter(ia -> isCoincidencia(ia, ip))
                .map(ia -> ia.getFolio() + " - " + ia.getComentario())
                .collect(Collectors.toList());
        if (!motivos.isEmpty()) {
            throw new SegalmexRuntimeException("No fue posible validar positivamente "
                    + "por las siguientes razones:", motivos);
        }
    }

    private boolean isCoincidencia(InformacionAlerta ia, InscripcionProductor ip) {
        return (Objects.isNull(ia.getCultivo()) || ia.getCultivo().equals(ip.getCultivo()))
                && (Objects.isNull(ia.getCiclo()) || ia.getCiclo().equals(ip.getCiclo()));
    }

    private boolean permitePositivoDeudor(InscripcionProductor ip) {
        ParametrosHistoricoRegistro parametros = new ParametrosHistoricoRegistro();
        parametros.setEntidad(ip);
        parametros.setTipo(buscadorCatalogo.buscaElemento(TipoHistoricoInscripcion.class,
                TipoHistoricoInscripcionEnum.AUTORIZA_POSITIVO_DEUDOR.getClave()));
        return !buscadorHistoricoRegistro.busca(parametros).isEmpty();
    }
}
