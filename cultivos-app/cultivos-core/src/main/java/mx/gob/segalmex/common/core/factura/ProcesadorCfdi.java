/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura;

import java.io.IOException;
import java.io.InputStream;
import mx.gob.segalmex.pluss.modelo.factura.Cfdi;
import mx.gob.segalmex.pluss.modelo.factura.Factura;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;

/**
 *
 * @author ismael
 */
public interface ProcesadorCfdi {

    Cfdi procesa(InputStream is, String nombre, Usuario usuario, Sucursal sucursal,
            boolean invertir, Cultivo c) throws IOException;

    Factura procesa(Cfdi cfdi, String tipoFactura);
}
