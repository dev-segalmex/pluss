/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.validador;

import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;

/**
 *
 * @author ismael
 */
public interface ValidadorCfdi {

    boolean valida(InscripcionFactura inscripcion);

}
