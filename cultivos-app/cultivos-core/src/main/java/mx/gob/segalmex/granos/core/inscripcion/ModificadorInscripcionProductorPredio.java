/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.inscripcion;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Map;
import java.util.Objects;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.validador.ValidadorInscripcion;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.granos.core.productor.EstadoProductorHelper;
import mx.gob.segalmex.granos.core.productor.TipoProductorHelper;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.Inscripcion;
import mx.gob.segalmex.pluss.modelo.productor.EstatusPredioProductorEnum;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component("modificadorInscripcionProductorPredio")
public class ModificadorInscripcionProductorPredio implements ModificadorInscripcion {

    @Autowired
    @Qualifier("validadorInscripcionProductorTemporalHectareas")
    private ValidadorInscripcion validadorTemporalHectareas;

    @Autowired
    @Qualifier("validadorPreRegistroSuperficie")
    private ValidadorInscripcion validadorPreRegistroSuperficie;

    @Autowired
    private TipoProductorHelper tipoProductorHelper;

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Override
    public void modifica(Inscripcion inscripcion, Map<String, DatoCapturado> datos) {
        InscripcionProductor ip = (InscripcionProductor) inscripcion;
        BigDecimal hectareasTotales = BigDecimal.ZERO;
        BigDecimal toneladasTotales = BigDecimal.ZERO;
        for (PredioProductor pp : ip.getPredios()) {
            hectareasTotales = hectareasTotales.add(pp.getSuperficie());
            toneladasTotales = toneladasTotales.add(pp.getVolumen());
            if (!pp.isCorregido()) {
                continue;
            }

            determinaSolicitado(pp);
            if (EstatusPredioProductorEnum.NO_SOLICITADO.getClave().equals(pp.getSolicitado())) {
                pp.setRendimiento(pp.getVolumen().divide(pp.getSuperficie(), 5, RoundingMode.HALF_UP));
                pp.setVolumenSolicitado(pp.getVolumen());
                pp.setRendimientoSolicitado(pp.getRendimiento());
            } else {
                calculaPredioSolicitado(pp);
            }

        }
        Estado e = EstadoProductorHelper.getEstado(ip);
        ip.setEstado(e);
        validadorTemporalHectareas.valida(ip);
        validadorPreRegistroSuperficie.valida(ip);
        tipoProductorHelper.determinaTipoProductor(ip, hectareasTotales, toneladasTotales);
    }

    void calculaPredioSolicitado(PredioProductor pp) {
        pp.setVolumenSolicitado(pp.getVolumen());
        pp.setRendimientoSolicitado(pp.getVolumenSolicitado().divide(pp.getSuperficie(), 5, RoundingMode.HALF_UP));

        pp.setRendimiento(getRendimientoMaximo(pp));
        pp.setVolumen(pp.getSuperficie().multiply(pp.getRendimiento()));
    }

    private BigDecimal getRendimientoMaximo(PredioProductor predio) {
        InscripcionProductor i = predio.getInscripcionProductor();
        String clave = i.getCultivo().getClave()
                + ":" + predio.getTipoCultivo().getClave()
                + ":" + i.getCiclo().getClave()
                + ":rendimiento:" + predio.getEstado().getClave();
        Parametro parametro = buscadorParametro.buscaElementoOpcional(clave);
        return Objects.nonNull(parametro) ? new BigDecimal(parametro.getValor())
                : BigDecimal.ZERO;

    }

    void determinaSolicitado(PredioProductor pp) {
        BigDecimal rendimiento = getRendimientoMaximo(pp);
        BigDecimal rendimientoPredio = pp.getVolumen().divide(pp.getSuperficie(), 5, RoundingMode.HALF_UP);
        pp.setSolicitado(EstatusPredioProductorEnum.NO_SOLICITADO.getClave());
        if (rendimientoPredio.compareTo(rendimiento) > 0) {
            pp.setSolicitado(EstatusPredioProductorEnum.SOLICITADO.getClave());
        }

    }
}
