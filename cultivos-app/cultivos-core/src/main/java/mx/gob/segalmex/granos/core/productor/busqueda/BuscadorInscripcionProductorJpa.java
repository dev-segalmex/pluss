/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.busqueda;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ismael
 */
@Repository("buscadorInscripcionProductor")
@Slf4j
public class BuscadorInscripcionProductorJpa implements BuscadorInscripcionProductor {

    private static final int LIMITE = 10000;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public InscripcionProductor buscaElemento(Integer id) {
        Objects.requireNonNull(id, "El id no puede ser nulo.");
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setId(id);

        List<InscripcionProductor> resultados = busca(parametros);
        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontraron resultados.", 1);
        }

        return resultados.get(0);
    }

    @Override
    public InscripcionProductor buscaElemento(String uuid) {
        Objects.requireNonNull(uuid, "El uuid no puede ser nulo.");
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setUuid(uuid);

        List<InscripcionProductor> resultados = busca(parametros);
        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontraron resultados.", 1);
        }

        return resultados.get(0);
    }

    @Override
    public List<InscripcionProductor> busca(ParametrosInscripcionProductor parametros) {
        StringBuilder sb = new StringBuilder("SELECT i FROM InscripcionProductor i ");

        Map<String, Object> params = constuyeParametros(parametros, sb);

        sb.append("ORDER BY i.folio ");
        LOGGER.debug("Ejecutando el query: {}", sb);
        TypedQuery<InscripcionProductor> q
                = entityManager.createQuery(sb.toString(), InscripcionProductor.class);
        QueryUtils.setParametros(q, params);
        if (Objects.nonNull(parametros.getMaximoResultados())) {
            if (parametros.getMaximoResultados() > 0) {
                q.setMaxResults(parametros.getMaximoResultados());
            }
        } else {
            q.setMaxResults(LIMITE);
        }

        long inicio = System.currentTimeMillis();
        List<InscripcionProductor> inscripciones = q.getResultList();
        LOGGER.debug("Se encontraron {} resultados en {} ms.", inscripciones.size(),
                System.currentTimeMillis() - inicio);

        return inscripciones;
    }

    @Override
    public InscripcionProductor buscaElemento(ParametrosInscripcionProductor parametros) {
        Objects.requireNonNull(parametros, "Los parámetros no pueden ser nulos.");
        List<InscripcionProductor> resultados = busca(parametros);
        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontraron resultados.", 1);
        }

        if (resultados.size() > 1) {
            throw new IncorrectResultSizeDataAccessException("Se encontró más "
                    + "de un elemento.", 1);
        }

        return resultados.get(0);
    }

    @Override
    public List<Object[]> buscaAgrupada(ParametrosInscripcionProductor parametros) {
        StringBuilder sb = new StringBuilder("SELECT i.ciclo.id, i.estado.id, i.estatus.id,"
                + " COUNT(*) FROM InscripcionProductor i ");

        Map<String, Object> params = constuyeParametros(parametros, sb);
        sb.append(" GROUP BY i.ciclo.id, i.estado.id, i.estatus.id ");
        LOGGER.debug("Ejecutando query {} ", sb);

        TypedQuery<Object[]> q = entityManager.createQuery(sb.toString(), Object[].class);
        QueryUtils.setParametros(q, params);

        return q.getResultList();

    }

    @Override
    public List<Object[]> buscaResumen(ParametrosInscripcionProductor parametros) {
        StringBuilder sb = new StringBuilder("SELECT i.cultivo.nombre, i.estatus.nombre, "
                + " i.ciclo.nombre, COUNT(*) FROM InscripcionProductor i ");

        Map<String, Object> params = constuyeParametros(parametros, sb);
        sb.append(" GROUP BY i.cultivo.nombre, i.estatus.nombre, "
        + "i.ciclo.nombre "
        + "order by i.cultivo.nombre, i.ciclo.nombre, i.estatus.nombre ");
        LOGGER.debug("Ejecutando query {} ", sb);

        TypedQuery<Object[]> q = entityManager.createQuery(sb.toString(), Object[].class);
        QueryUtils.setParametros(q, params);

        return q.getResultList();

    }

    private Map<String, Object> constuyeParametros(ParametrosInscripcionProductor parametros, StringBuilder sb) {
        Map<String, Object> params = new HashMap<>();
        boolean first = true;
        if (Objects.nonNull(parametros.getId())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.id", "id", parametros.getId(), params);
        }

        if (Objects.nonNull(parametros.getUuid())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.uuid", "uuid", parametros.getUuid(), params);
        }

        if (Objects.nonNull(parametros.getFolio())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.folio", "folio", parametros.getFolio(), params);
        }

        if (Objects.nonNull(parametros.getEstatus())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.estatus", "estatus", parametros.getEstatus(), params);
        }

        if (Objects.nonNull(parametros.getIncluirEstatus()) && !parametros.getIncluirEstatus().isEmpty()) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("i.estatus IN :estatusIncluidos ");
            params.put("estatusIncluidos", parametros.getIncluirEstatus());
        }

        if (Objects.nonNull(parametros.getRfcEmpresa())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.rfcEmpresa", "rfcEmpresa", parametros.getRfcEmpresa(), params);
        }

        if (Objects.nonNull(parametros.getRfcBodega())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.bodega.empresa.rfc", "rfcBodega", parametros.getRfcBodega(), params);
        }

        if (Objects.nonNull(parametros.getFechaInicio())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("i.fechaCreacion >= :fechaInicio ");
            params.put("fechaInicio", parametros.getFechaInicio());
        }

        if (Objects.nonNull(parametros.getFechaFin())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("i.fechaCreacion < :fechaFin ");
            params.put("fechaFin", parametros.getFechaFin());
        }

        if (Objects.nonNull(parametros.getEmpresa())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.sucursal.empresa", "empresa", parametros.getEmpresa(), params);
        }

        if (Objects.nonNull(parametros.getSucursal())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.sucursal", "sucursal", parametros.getSucursal(), params);
        }

        if (Objects.nonNull(parametros.getNoEstatus())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("i.estatus != :noEstatus ");
            params.put("noEstatus", parametros.getNoEstatus());
        }

        if (Objects.nonNull(parametros.getValidador())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.usuarioValidador", "usuarioValidador", parametros.getValidador(), params);
        }

        if (Objects.nonNull(parametros.getCurp())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.datosProductor.curp", "curp", parametros.getCurp(), params);
        }

        if (Objects.nonNull(parametros.getRfcProductor())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.datosProductor.rfc", "rfc", parametros.getRfcProductor(), params);
        }

        if (Objects.nonNull(parametros.getTipoPersona())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.datosProductor.tipoPersona", "tipoPersona", parametros.getTipoPersona(), params);
        }
        if (Objects.nonNull(parametros.getNombre())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.datosProductor.nombre", "nombre", parametros.getNombre(), params);
        }
        if (Objects.nonNull(parametros.getPapellido())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.datosProductor.primerApellido", "primerApellido", parametros.getPapellido(), params);
        }
        if (Objects.nonNull(parametros.getSapellido())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.datosProductor.segundoApellido", "segundoApellido", parametros.getSapellido(), params);
        }

        if (Objects.nonNull(parametros.getCiclo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.ciclo", "ciclo", parametros.getCiclo(), params);
        }

        if (Objects.nonNull(parametros.getCultivo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.cultivo", "cultivo", parametros.getCultivo(), params);
        }

        if (Objects.nonNull(parametros.getTipoRegistro())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.tipoRegistro", "tipoRegistro", parametros.getTipoRegistro(), params);
        }

        if (Objects.nonNull(parametros.getClaveArchivos())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.claveArchivos", "claveArchivos", parametros.getClaveArchivos(), params);
        }

        if (Objects.nonNull(parametros.getEstado())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.estado", "estado", parametros.getEstado(), params);
        }

        if (Objects.nonNull(parametros.getClabe())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.cuentaBancaria.clabe", "clabe", parametros.getClabe(), params);
        }

        if (Objects.nonNull(parametros.getCurpBeneficiario())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.curpBeneficiario", "curpBeneficiario", parametros.getCurpBeneficiario(), params);
        }

        if (Objects.nonNull(parametros.getNoFolio())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("i.folio != :folio ");
            params.put("folio", parametros.getNoFolio());
        }

        if (Objects.nonNull(parametros.getTipoProductor())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.tipoProductor", "tipoProductor", parametros.getTipoProductor(), params);
        }

        return params;
    }

}
