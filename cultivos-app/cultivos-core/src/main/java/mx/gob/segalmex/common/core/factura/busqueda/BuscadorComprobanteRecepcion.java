/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.factura.ComprobanteRecepcion;

/**
 * Interfaz para definir busquedas sobre {@link ComprobanteRecepcion}.
 *
 * @author oscar
 */
public interface BuscadorComprobanteRecepcion {

    public ComprobanteRecepcion buscaElemento(Integer id);

    public ComprobanteRecepcion buscaElemento(String folio);

    public List<ComprobanteRecepcion> busca(ParametrosComprobanteRecepcion parametros);
}
