/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.busqueda;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ismael
 */
@Slf4j
@Repository("buscadorInscripcionFactura")
public class BuscadorInscripcionFacturaJpa implements BuscadorInscripcionFactura {

    private static final int LIMITE = 20000;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public InscripcionFactura buscaElemento(Integer id) {
        Objects.requireNonNull(id, "El id no puede ser nulo.");
        ParametrosInscripcionFactura parametros = new ParametrosInscripcionFactura();
        parametros.setId(id);

        List<InscripcionFactura> resultados = busca(parametros);
        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontraron resultados.", 1);
        }

        return resultados.get(0);
    }

    @Override
    public InscripcionFactura buscaElemento(String uuid) {
        Objects.requireNonNull(uuid, "El uuid no puede ser nulo.");
        ParametrosInscripcionFactura parametros = new ParametrosInscripcionFactura();
        parametros.setUuid(uuid);

        List<InscripcionFactura> resultados = busca(parametros);
        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontraron resultados.", 1);
        }

        return resultados.get(0);
    }

    @Override
    public List<InscripcionFactura> busca(ParametrosInscripcionFactura parametros) {
        StringBuilder sb = new StringBuilder("SELECT i FROM InscripcionFactura i ");

        Map<String, Object> params = constuyeParametros(parametros, sb);
        sb.append("ORDER BY i.folio ");
        LOGGER.debug("Ejecutando el query: {}", sb);
        TypedQuery<InscripcionFactura> q
                = entityManager.createQuery(sb.toString(), InscripcionFactura.class);
        QueryUtils.setParametros(q, params);

        if (Objects.nonNull(parametros.getMaximoResultados())) {
            if (parametros.getMaximoResultados() > 0) {
                q.setMaxResults(parametros.getMaximoResultados());
            }
        } else {
            q.setMaxResults(LIMITE);
        }

        long inicio = System.currentTimeMillis();
        List<InscripcionFactura> inscripciones = q.getResultList();
        LOGGER.debug("Se encontraron {} resultados en {} ms.", inscripciones.size(),
                System.currentTimeMillis() - inicio);

        return inscripciones;

    }

    @Override
    public List<Object[]> buscaAgrupada(ParametrosInscripcionFactura parametros) {
        StringBuilder sb = new StringBuilder("SELECT i.ciclo.id, i.estado.id,"
                + " i.estatus.id, COUNT(*) FROM InscripcionFactura i ");

        Map<String, Object> params = constuyeParametros(parametros, sb);
        sb.append(" GROUP BY i.ciclo.id, i.estado.id, i.estatus.id ");
        LOGGER.debug("Ejecutando query {} ", sb);

        TypedQuery<Object[]> q = entityManager.createQuery(sb.toString(), Object[].class);
        QueryUtils.setParametros(q, params);

        return q.getResultList();
    }

    @Override
    public List<Object[]> buscaResumen(ParametrosInscripcionFactura parametros) {
        StringBuilder sb = new StringBuilder("SELECT i.cultivo.nombre, i.estatus.nombre, "
                + " i.ciclo.nombre, i.tipoFactura, COUNT(*) FROM InscripcionFactura i ");

        Map<String, Object> params = constuyeParametros(parametros, sb);
        sb.append(" GROUP BY i.cultivo.nombre, i.estatus.nombre, "
        + "i.ciclo.nombre, i.tipoFactura "
        + "order by i.cultivo.nombre, i.ciclo.nombre, i.estatus.nombre, i.tipoFactura ");
        LOGGER.debug("Ejecutando query {} ", sb);

        TypedQuery<Object[]> q = entityManager.createQuery(sb.toString(), Object[].class);
        QueryUtils.setParametros(q, params);

        return q.getResultList();

    }

    private Map<String, Object> constuyeParametros(ParametrosInscripcionFactura parametros, StringBuilder sb) {
        Map<String, Object> params = new HashMap<>();
        boolean first = true;
        if (Objects.nonNull(parametros.getId())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.id", "id", parametros.getId(), params);
        }

        if (Objects.nonNull(parametros.getUuid())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.uuid", "uuid", parametros.getUuid(), params);
        }

        if (Objects.nonNull(parametros.getFolio())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.folio", "folio", parametros.getFolio(), params);
        }

        if (Objects.nonNull(parametros.getEstatus())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.estatus", "estatus", parametros.getEstatus(), params);
        }

        if (Objects.nonNull(parametros.getRfcEmpresa())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.rfcEmpresa", "rfcEmpresa", parametros.getRfcEmpresa(), params);
        }

        if (Objects.nonNull(parametros.getFechaInicio())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("i.fechaCreacion >= :fechaInicio ");
            params.put("fechaInicio", parametros.getFechaInicio());
        }

        if (Objects.nonNull(parametros.getFechaFin())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("i.fechaCreacion < :fechaFin ");
            params.put("fechaFin", parametros.getFechaFin());
        }

        if (Objects.nonNull(parametros.getEmpresa())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.sucursal.empresa", "empresa", parametros.getEmpresa(), params);
        }

        if (Objects.nonNull(parametros.getSucursal())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.sucursal", "sucursal", parametros.getSucursal(), params);
        }

        if (Objects.nonNull(parametros.getNoEstatus())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("i.estatus != :noEstatus ");
            params.put("noEstatus", parametros.getNoEstatus());
        }

        if (Objects.nonNull(parametros.getRfcEmisor())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.cfdi.rfcEmisor", "rfcEmisor", parametros.getRfcEmisor(), params);
        }

        if (Objects.nonNull(parametros.getRfcReceptor())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.cfdi.rfcReceptor", "rfcReceptor", parametros.getRfcReceptor(), params);
        }

        if (Objects.nonNull(parametros.getUuidTimbreFiscalDigital())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.cfdi.uuidTimbreFiscalDigital", "uuidTimbreFiscalDigital",
                    parametros.getUuidTimbreFiscalDigital(), params);
        }

        if (Objects.nonNull(parametros.getValidador())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.usuarioValidador", "usuarioValidador", parametros.getValidador(), params);
        }

        if (Objects.nonNull(parametros.getTipoFactura())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.tipoFactura", "tipoFactura", parametros.getTipoFactura(), params);
        }

        if (Objects.nonNull(parametros.getCiclo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.ciclo", "ciclo", parametros.getCiclo(), params);
        }

        if (Objects.nonNull(parametros.getCultivo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.cultivo", "cultivo", parametros.getCultivo(), params);
        }

        if (Objects.nonNull(parametros.getProductor())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.productorCiclo.productor", "productor", parametros.getProductor(), params);
        }

        if (Objects.nonNull(parametros.getProductorCiclo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.productorCiclo", "productorCiclo", parametros.getProductorCiclo(), params);
        }

        if (Objects.nonNull(parametros.getEstado())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.estado", "estado", parametros.getEstado(), params);
        }

        if (Objects.nonNull(parametros.getEstatusIncluir()) && !parametros.getEstatusIncluir().isEmpty()) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("i.estatus IN (:estatusIncuidos)");
            params.put("estatusIncuidos", parametros.getEstatusIncluir());
        }
        return params;
    }

    @Override
    public InscripcionFactura buscaElemento(ParametrosInscripcionFactura parametros) {
        List<InscripcionFactura> inscripciones = busca(parametros);
        if (inscripciones.isEmpty()) {
            throw new EmptyResultDataAccessException("Sin resultados.", 1);
        }
        if (inscripciones.size() > 1) {
            throw new IncorrectResultSizeDataAccessException("Se esperaba 1 resultado.", 1);
        }
        return inscripciones.get(0);
    }
}
