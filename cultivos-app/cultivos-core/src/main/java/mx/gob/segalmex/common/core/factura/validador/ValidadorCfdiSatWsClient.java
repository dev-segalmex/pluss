/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.validador;

import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.nio.charset.Charset;
import java.util.Objects;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.pluss.modelo.factura.Cfdi;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import org.apache.commons.io.IOUtils;
import org.springframework.remoting.RemoteAccessException;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.SoapMessage;

/**
 *
 * @author ismael
 */
@Slf4j
public class ValidadorCfdiSatWsClient implements ValidadorCfdi {

    private WebServiceTemplate wsTemplate;

    private String url;

    public void setUrl(String url) {
        this.url = url;
    }

    public void setWsTemplate(WebServiceTemplate wsTemplate) {
        this.wsTemplate = wsTemplate;
    }

    @Override
    public boolean valida(InscripcionFactura inscripcion) {
        Cfdi cfdi = inscripcion.getCfdi();
        if (Objects.isNull(cfdi.getInvertida())) {
            cfdi.setInvertida(Boolean.FALSE);
        }
        try {
            Reader r = new InputStreamReader(ValidadorCfdiSatWsClient.class
                    .getResourceAsStream("/validacion-cfdi-sat.xml"), Charset.forName("utf-8"));
            String request = IOUtils.toString(r);
            request = request.replace("${RFC_EMISOR}", cfdi.getInvertida()
                    ? cfdi.getRfcReceptor() : cfdi.getRfcEmisor());
            request = request.replace("${RFC_RECEPTOR}", cfdi.getInvertida()
                    ? cfdi.getRfcEmisor() : cfdi.getRfcReceptor());
            request = request.replace("${TOTAL}", cfdi.getTotal().toString());
            request = request.replace("${UUID}", cfdi.getUuidTimbreFiscalDigital());
            LOGGER.debug("Request de validacion:\n{}", request);

            StreamSource source = new StreamSource(new StringReader(request));
            Writer w = new CharArrayWriter();
            StreamResult result = new StreamResult(w);

            LOGGER.info("URL de la petición: {}", url);
            wsTemplate.sendSourceAndReceiveToResult(url, source, (WebServiceMessage wsm) -> {
                ((SoapMessage) wsm).setSoapAction("http://tempuri.org/IConsultaCFDIService/Consulta");
            }, result);
            String response = w.toString();
            LOGGER.debug("Response de validación:\n{}", response);
            inscripcion.setValida(response.contains(">Vigente<"));
            return inscripcion.getValida();
        } catch (IOException ouch) {
            throw new RemoteAccessException("Error al enviar el request.", ouch);
        }
    }
}
