/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.validador;

import java.io.CharArrayWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.Writer;
import java.nio.charset.Charset;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.pluss.modelo.productor.PredioDocumento;
import org.apache.commons.io.IOUtils;
import org.springframework.remoting.RemoteAccessException;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.WebServiceTemplate;
import org.springframework.ws.soap.SoapMessage;

/**
 *
 * @author oscar
 */
@Slf4j
public class ValidadorDocumentoSatWsClient implements ValidadorDocumento {

    private WebServiceTemplate wsTemplate;

    private String url;

    @Override
    public boolean valida(PredioDocumento predioDocumento) {
        LOGGER.info("Validando predio documento contra el SAT.");
        try {
            Reader r = new InputStreamReader(ValidadorDocumentoSatWsClient.class
                    .getResourceAsStream("/validacion-cfdi-sat.xml"), Charset.forName("utf-8"));
            String request = IOUtils.toString(r);
            request = request.replace("${RFC_EMISOR}", predioDocumento.getRfcEmisor());
            request = request.replace("${RFC_RECEPTOR}", predioDocumento.getRfcReceptor());
            request = request.replace("${TOTAL}", predioDocumento.getTotal().toString());
            request = request.replace("${UUID}", predioDocumento.getUuidTimbreFiscalDigital());
            LOGGER.debug("Request de validacion:\n{}", request);

            StreamSource source = new StreamSource(new StringReader(request));
            Writer w = new CharArrayWriter();
            StreamResult result = new StreamResult(w);

            LOGGER.info("URL de la petición: {}", url);
            wsTemplate.sendSourceAndReceiveToResult(url, source, (WebServiceMessage wsm) -> {
                ((SoapMessage) wsm).setSoapAction("http://tempuri.org/IConsultaCFDIService/Consulta");
            }, result);
            String response = w.toString();
            LOGGER.debug("Response de validación:\n{}", response);
            predioDocumento.setValidacionSat(response.contains(">Vigente<"));
            return predioDocumento.getValidacionSat();
        } catch (IOException ouch) {
            throw new RemoteAccessException("Error al enviar el request del documento.", ouch);
        }

    }

    public void setWsTemplate(WebServiceTemplate wsTemplate) {
        this.wsTemplate = wsTemplate;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
