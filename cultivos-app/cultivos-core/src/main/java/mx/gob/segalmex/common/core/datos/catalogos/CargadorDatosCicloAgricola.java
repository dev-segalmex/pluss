package mx.gob.segalmex.common.core.datos.catalogos;

import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;

/**
 *
 * @author ismael
 */
public class CargadorDatosCicloAgricola extends AbstractCargadorDatosCatalogo<CicloAgricola> {

    @Override
    public CicloAgricola getInstance(String[] elementos) {
        CicloAgricola ciclo = new CicloAgricola();
        ciclo.setClave(elementos[0]);
        ciclo.setNombre(elementos[1]);
        ciclo.setActivo(true);
        ciclo.setAbreviatura(elementos[2]);
        ciclo.setOrden(Integer.parseInt(elementos[3]));

        return ciclo;
    }

}
