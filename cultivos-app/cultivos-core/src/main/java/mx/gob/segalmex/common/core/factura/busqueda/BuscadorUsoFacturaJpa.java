/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.busqueda;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.factura.EstatusUsoFactura;
import mx.gob.segalmex.pluss.modelo.factura.EstatusUsoFacturaEnum;
import mx.gob.segalmex.pluss.modelo.factura.UsoFactura;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jurgen
 */
@Repository("buscadorUsoFactura")
@Slf4j
public class BuscadorUsoFacturaJpa implements BuscadorUsoFactura {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Override
    public List<UsoFactura> busca(ParametrosUsoFactura parametros) {
        StringBuilder sb = new StringBuilder("SELECT uf FROM UsoFactura uf ");

        Map<String, Object> params = new HashMap<>();
        boolean first = true;

        if (Objects.nonNull(parametros.getFolio())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "uf.folio", "folio", parametros.getFolio(), params);
        }

        if (Objects.nonNull(parametros.getUuid())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "uf.uuid", "uuid", parametros.getUuid(), params);
        }

        if (Objects.nonNull(parametros.getRfcProductor())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "uf.productor.rfc", "rfc", parametros.getRfcProductor(), params);
        }

        if (Objects.nonNull(parametros.getEstatusFactura())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "uf.factura.estatus", "estatus", parametros.getEstatusFactura(), params);
        }

        if (Objects.nonNull(parametros.getEstatus())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "uf.estatus", "estatus", parametros.getEstatus(), params);
        }

        if (Objects.nonNull(parametros.getProductor())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "uf.productor", "productor", parametros.getProductor(), params);
        }

        if (Objects.nonNull(parametros.getProductorCiclo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "uf.productorCiclo", "productorCiclo", parametros.getProductorCiclo(), params);
        }

        if (Objects.nonNull(parametros.getCiclo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "uf.productorCiclo.ciclo", "ciclo", parametros.getCiclo(), params);
        }

        if (Objects.nonNull(parametros.getCultivo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "uf.productorCiclo.cultivo", "cultivo", parametros.getCultivo(), params);
        }

        sb.append("ORDER BY uf.folio ");
        LOGGER.debug("Ejecutando el query: {}", sb);
        TypedQuery<UsoFactura> q
                = entityManager.createQuery(sb.toString(), UsoFactura.class);
        QueryUtils.setParametros(q, params);

        if (Objects.nonNull(parametros.getMaximoResultados())) {
            q.setMaxResults(parametros.getMaximoResultados());
        }

        long inicio = System.currentTimeMillis();
        List<UsoFactura> factura = q.getResultList();
        LOGGER.debug("Se encontraron {} resultados en {} ms.", factura.size(),
                System.currentTimeMillis() - inicio);

        return factura;

    }

    @Override
    public UsoFactura buscaElementoUuid(String uuid) {
        Objects.requireNonNull(uuid, "El uuid no puede ser nulo.");
        ParametrosUsoFactura parametros = new ParametrosUsoFactura();
        parametros.setUuid(uuid);

        List<UsoFactura> resultados = busca(parametros);
        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontraron resultados.", 1);
        }

        return resultados.get(0);
    }

    @Override
    public List<UsoFactura> buscaElementoFolio(String folio) {
        Objects.requireNonNull(folio, "El folio no puede ser nulo.");
        ParametrosUsoFactura parametros = new ParametrosUsoFactura();
        parametros.setFolio(folio);

        List<UsoFactura> resultados = busca(parametros);

        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontro elemento de uso factura", 1);
        }

        return resultados;
    }

    @Override
    public List<Object[]> buscaReportePlaneacion(CicloAgricola ciclo, Cultivo cultivo,
            EstatusUsoFacturaEnum... considerados) {
        Objects.requireNonNull(ciclo, "El ciclo no puede ser nulo.");
        Objects.requireNonNull(cultivo, "El cultivo no puede ser nulo.");
        List<EstatusUsoFactura> estatus = new ArrayList<>();
        for (EstatusUsoFacturaEnum e : considerados) {
            estatus.add(buscadorCatalogo.buscaElemento(EstatusUsoFactura.class, e));
        }

        return entityManager.createQuery("SELECT "
                + "i.productorCiclo.productor.rfc, i.productorCiclo.productor.curp, "
                + "i.folio, c.uuid, uf.toneladas, uf.estimuloTotal, uf.tipo "
                + "FROM UsoFactura uf "
                + "INNER JOIN InscripcionFactura i ON (uf.folio = i.folio) "
                + "INNER JOIN Cfdi c ON (i.cfdi.id = c.id) "
                + "WHERE "
                + "i.ciclo = :ciclo AND i.cultivo = :cultivo "
                + "AND uf.estatus IN (:estatus) "
                + "ORDER BY uf.folio ", Object[].class)
                .setParameter("ciclo", ciclo)
                .setParameter("cultivo", cultivo)
                .setParameter("estatus", estatus)
                .getResultList();
    }
}
