/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.busqueda;

import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;

/**
 *
 * @author oscar
 */
@Getter
@Setter
public class ParametrosComprobanteRecepcion {

    private Integer id;

    private String folio;

    private InscripcionFactura inscripcionFactura;

    private String estatus;
}
