/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.validador.ValidadorCurpHelper;
import mx.gob.segalmex.common.core.validador.ValidadorPreRegistroContrato;
import mx.gob.segalmex.common.core.validador.ValidadorRfcHelper;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorPreRegistroProductor;
import mx.gob.segalmex.granos.core.rfc.busqueda.BuscadorRfcExcepcion;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.inscripcion.ContratoProductor;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Slf4j
@Component
public class ProcesadorContratoProductorXls {

    private static final int COL_NO = 0;
    private static final int COL_NOMBRES = 1;
    private static final int COL_APELLIDO_PAT = 2;
    private static final int COL_APELLIDO_MAT = 3;
    private static final int COL_CURP = 4;
    private static final int COL_RFC = 5;
    private static final int COL_SUPERFICE = 6;
    private static final int COL_VOLUMEN = 7;
    private static final int COL_VOLUMEN_IAR = 8;

    private static final List<String> ENCABEZADO = Arrays.asList("N°", "Nombre (s) del productor",
            "Primer apellido del productor", "Segundo apellido del productor", "CURP", "RFC",
            "Superficie sembrada (Ha)", "Volumen contratado en el CCV", "Volumen cubierto por el IAR");

    public static final List<Integer> COLUMNAS = Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8);

    @Autowired
    private BuscadorRfcExcepcion buscadorRfcExcepcion;

    @Autowired
    private ValidadorPreRegistroContrato validadorPreRegistroContrato;

    public List<ContratoProductor> procesa(byte[] datos, Cultivo cultivo, CicloAgricola ciclo) {
        if (datos == null) {
            LOGGER.error("El archivo no tiene contenido.");
            throw new SegalmexRuntimeException("Error al validar el archivo.",
                    "El archivo no tiene contenido.");
        }
        ByteArrayInputStream inputStream = new ByteArrayInputStream(datos);
        Workbook workbook = null;
        try {
            workbook = WorkbookFactory.create(inputStream);
        } catch (InvalidFormatException | IOException e) {
            LOGGER.error("No se puede leer el archivo de Excel.");
            throw new SegalmexRuntimeException("Error al validar el archivo.",
                    "No se puede leer el archivo de Excel.");
        }

        Sheet sheet = workbook.getSheetAt(0);
        Iterator<Row> rowIterator = sheet.iterator();
        if (!rowIterator.hasNext()) {
            LOGGER.error("El archivo de Excel no tiene información.");
            throw new SegalmexRuntimeException("Error al validar el archivo.",
                    "El archivo de Excel no tiene información.");
        }
        verificaEncabezado(rowIterator.next());
        List<ContratoProductor> elementos = new ArrayList<>();
        while (rowIterator.hasNext()) {
            ContratoProductor elemento = new ContratoProductor();
            try {
                Row row = rowIterator.next();
                if (isCellEmpty(row.getCell(COL_NOMBRES))) {
                    LOGGER.warn("El renglón {} inicia en celda vacía, se omite.",
                            row.getRowNum() + 1);
                    continue;
                }
                cargaDatos(row, elemento, cultivo, ciclo);
            } catch (SegalmexRuntimeException ouch) {
                LOGGER.error("Error: {}", ouch.getMotivos().get(0));
                elemento.setError(ouch.getMotivos().get(0));

            }
            elementos.add(elemento);
        }

        return elementos;
    }

    private void verificaEncabezado(Row row) {
        if (row == null) {
            LOGGER.error("El archivo de Excel no tiene encabezado.");
            throw new SegalmexRuntimeException("Error al validar el archivo.",
                    "El archivo de Excel no tiene encabezado.");
        }
        Iterator<Cell> cellIterator = row.cellIterator();
        Cell currentCell = null;
        for (Integer columna : COLUMNAS) {
            if (!cellIterator.hasNext()) {
                LOGGER.error("El encabezado tiene menos columnas de las esperadas.");
                throw new SegalmexRuntimeException("Error al validar el archivo.",
                        "El encabezado tiene menos columnas de las esperadas.");
            }

            currentCell = row.getCell(columna, Row.RETURN_BLANK_AS_NULL);
            if (currentCell == null || Cell.CELL_TYPE_STRING != currentCell.getCellType()) {
                LOGGER.error("Se esperaba una celda con texto " + ENCABEZADO.get(columna) + " en {}",
                        localizacion(columna, row));
                throw new SegalmexRuntimeException("Error al validar el archivo.",
                        "Se esperaba una celda con texto [" + ENCABEZADO.get(columna) + "] en "
                        + localizacion(columna, row));
            }
            String cellValue = currentCell.getStringCellValue();
            if (!ENCABEZADO.get(columna).equalsIgnoreCase(cellValue)) {
                List<String> motivos = new ArrayList<>();
                LOGGER.error("Columna esperada: [" + ENCABEZADO.get(columna) + "] columna encontrada: ["
                        + cellValue + "] en " + localizacion(currentCell.getColumnIndex(), row));
                motivos.add("Columna esperada: [" + ENCABEZADO.get(columna) + "] columna encontrada: ["
                        + cellValue + "] en " + localizacion(currentCell.getColumnIndex(), row));
                throw new SegalmexRuntimeException("Error al validar el archivo.", motivos);

            }
        }
    }

    private void cargaDatos(Row row, ContratoProductor elemento, Cultivo cultivo, CicloAgricola ciclo) {
        elemento.setOrden(row.getRowNum());
        validaCeldaNoVacia(row, COL_NOMBRES);
        setCellType(row, COL_NOMBRES, Cell.CELL_TYPE_STRING);
        elemento.setNombre(StringUtils.strip(row.getCell(COL_NOMBRES).getStringCellValue().toUpperCase()));

        validaCeldaNoVacia(row, COL_APELLIDO_PAT);
        setCellType(row, COL_APELLIDO_PAT, Cell.CELL_TYPE_STRING);
        elemento.setPrimerApellido(StringUtils.strip(row.getCell(COL_APELLIDO_PAT).getStringCellValue().toUpperCase()));

        validaCeldaNoVacia(row, COL_APELLIDO_MAT);
        setCellType(row, COL_APELLIDO_MAT, Cell.CELL_TYPE_STRING);
        elemento.setSegundoApellido(StringUtils.strip(row.getCell(COL_APELLIDO_MAT).getStringCellValue().toUpperCase()));

        validaCeldaNoVacia(row, COL_CURP);
        setCellType(row, COL_CURP, Cell.CELL_TYPE_STRING);
        elemento.setCurp(StringUtils.strip(row.getCell(COL_CURP).getStringCellValue().toUpperCase()));

        validaCeldaNoVacia(row, COL_RFC);
        setCellType(row, COL_RFC, Cell.CELL_TYPE_STRING);
        elemento.setRfc(StringUtils.strip(row.getCell(COL_RFC).getStringCellValue().toUpperCase()));

        validaCeldaNoVacia(row, COL_SUPERFICE);
        setCellType(row, COL_SUPERFICE, Cell.CELL_TYPE_STRING);

        validaCeldaNoVacia(row, COL_VOLUMEN);
        setCellType(row, COL_VOLUMEN, Cell.CELL_TYPE_STRING);

        validaCeldaNoVacia(row, COL_VOLUMEN_IAR);
        setCellType(row, COL_VOLUMEN_IAR, Cell.CELL_TYPE_STRING);
        int col_axu = COL_SUPERFICE;
        try {
            elemento.setSuperficie(new BigDecimal(StringUtils.strip(row.getCell(COL_SUPERFICE).getStringCellValue())));
            col_axu = COL_VOLUMEN;
            elemento.setVolumenContrato(new BigDecimal(StringUtils.strip(row.getCell(COL_VOLUMEN).getStringCellValue())));
            col_axu = COL_VOLUMEN_IAR;
            elemento.setVolumenIar(new BigDecimal(StringUtils.strip(row.getCell(COL_VOLUMEN_IAR).getStringCellValue())));
        } catch (IllegalStateException | NumberFormatException ouch) {
            throw new SegalmexRuntimeException("Error al validar el archivo.", "No es un número válio en "
                    + localizacion(col_axu, row));
        }
        ValidadorCurpHelper.valida(elemento.getCurp(), " en " + localizacion(COL_CURP, row));
        List<String> excepciones = buscadorRfcExcepcion.getExcepciones(elemento.getRfc(), true);
        ValidadorRfcHelper.valida(elemento.getRfc(), excepciones, " en " + localizacion(COL_RFC, row));
        validaVolumenContratado(elemento, row);
        validadorPreRegistroContrato.verificaPreRegistro(elemento.getCurp(), cultivo, ciclo, row);
        elemento.setCorrecto(true);
    }

    private static boolean isCellEmpty(Cell cell) {
        return cell == null || Cell.CELL_TYPE_BLANK == cell.getCellType();
    }

    private String localizacion(int columna, Row row) {
        return "[fila: " + (row.getRowNum() + 1) + ", columna: " + (columna + 1) + "] del archivo";
    }

    private void validaCeldaNoVacia(Row row, int col)
            throws SegalmexRuntimeException {
        if (row == null) {
            LOGGER.error("Renglón nulo.");
            throw new SegalmexRuntimeException("Error", "Renglón nulo.");
        }
        Cell cell = row.getCell(col);
        if (cell == null || Cell.CELL_TYPE_BLANK == cell.getCellType()) {
            LOGGER.error(localizacion(col, row) + " es vacía.");
            throw new SegalmexRuntimeException("Error", localizacion(col, row) + " es vacía.");
        }
    }

    private static void setCellType(Row row, int column, int cellType) {
        row.getCell(column).setCellType(cellType);
    }

    private void validaVolumenContratado(ContratoProductor elemento, Row row) {
        BigDecimal volumenCvv = elemento.getVolumenContrato();
        BigDecimal volumenIar = elemento.getVolumenIar();
        if (volumenCvv.compareTo(volumenIar) < 0) {
            LOGGER.error("[fila: " + (row.getRowNum() + 1) + "] del archivo");
            throw new SegalmexRuntimeException("Error", "El volumen contratado en "
                    + "el CCV no puede ser menor que el volumen cubierto por el "
                    + "I.A.R. [fila: " + (row.getRowNum() + 1) + "] del archivo");
        }
    }
}
