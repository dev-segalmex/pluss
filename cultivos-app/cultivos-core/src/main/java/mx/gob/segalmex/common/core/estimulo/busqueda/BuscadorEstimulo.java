/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.estimulo.busqueda;

import java.util.Calendar;
import java.util.List;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.pago.Estimulo;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;

/**
 *
 * @author jurgen
 */
public interface BuscadorEstimulo {

    List<Estimulo> busca(ParametrosEstimulo parametros);

    Estimulo buscaElemento(ParametrosEstimulo parametros);

    List<Estimulo> busca(CicloAgricola ciclo, Estado estado, TipoCultivo tipo,
            Calendar fecha, String tipoEstimulo, String tipoProductor);

    List<Estimulo> busca(CicloAgricola ciclo, String tipoEstimulo, String contrato);

}
