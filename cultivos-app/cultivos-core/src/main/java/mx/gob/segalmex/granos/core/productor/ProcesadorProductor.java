/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor;

import mx.gob.segalmex.pluss.modelo.historico.HistoricoRegistro;
import mx.gob.segalmex.pluss.modelo.productor.DatosProductor;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.Productor;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;

/**
 *
 * @author ismael
 */
public interface ProcesadorProductor {

    /**
     * Procesa los datos de un productor para generar un registro definitivo y único.
     *
     * @param dp
     * @return
     */
    Productor procesa(DatosProductor dp);

    void procesa(HistoricoRegistro historico);

    ProductorCiclo procesa(Productor productor, InscripcionProductor inscripcion);

    void procesaGrupos(ProductorCiclo pc);

    /**
     * Se encarga de generar o actualizar los valores del productorCiclo y de sus hijos.
     * @param inscripcion
     * @param productorCiclo
     */
    void actualizaProductorCiclo(InscripcionProductor inscripcion, ProductorCiclo productorCiclo);

}
