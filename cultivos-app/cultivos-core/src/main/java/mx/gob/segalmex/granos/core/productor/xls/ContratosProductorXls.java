/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.xls;

import java.util.List;
import mx.gob.segalmex.common.core.util.XlsUtils;
import mx.gob.segalmex.pluss.modelo.productor.ContratoFirmado;
import mx.gob.segalmex.pluss.modelo.productor.Domicilio;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.stereotype.Component;

/**
 *
 * @author erikcamacho
 */
@Component("contratosProductorXls")
public class ContratosProductorXls {

    public byte[] exporta(List<InscripcionProductor> inscripciones) {
        SXSSFWorkbook wb = new SXSSFWorkbook(100);
        CreationHelper ch = wb.getCreationHelper();
        Sheet s = wb.createSheet();

        String[] encabezados = new String[]{
            "No.",
            "Folio",
            "Nombre productor",
            "Primer apellido productor",
            "Segundo apellido productor",
            "RFC",
            "CURP",
            "Teléfono",
            "Correo electrónico",
            "Estado",
            "Municipio",
            "Localidad",
            "Estatus inscripción",
            "Folio contrato",
            "Número de contrato",
            "Cantidad contratada",
            "Cantidad contratada con cobertura",
            "Empresa contrato",
            "Tipo de precio"
        };
        XlsUtils.creaEncabezados(s, encabezados);

        CellStyle cs = wb.createCellStyle();
        cs.setDataFormat(ch.createDataFormat().getFormat("dd/MM/yyyy HH:mm"));

        int renglon = 1;
        for (InscripcionProductor i : inscripciones) {
            if (!i.getContratosActivos().isEmpty()) {
                for (ContratoFirmado con : i.getContratos()) {
                    int j = 0;
                    Row r = s.createRow(renglon);
                    XlsUtils.createCell(r, j++, renglon);
                    XlsUtils.createCell(r, j++, i.getFolio());
                    XlsUtils.createCell(r, j++, i.getDatosProductor().getNombre());
                    XlsUtils.createCell(r, j++, i.getDatosProductor().getPrimerApellido());
                    XlsUtils.createCell(r, j++, i.getDatosProductor().getSegundoApellido());
                    XlsUtils.createCell(r, j++, i.getDatosProductor().getRfc());
                    XlsUtils.createCell(r, j++, i.getDatosProductor().getCurp());
                    XlsUtils.createCell(r, j++, i.getNumeroTelefono());
                    XlsUtils.createCell(r, j++, i.getCorreoElectronico());
                    Domicilio dp = i.getDomicilio();
                    XlsUtils.createCell(r, j++, dp.getEstado().getNombre());
                    XlsUtils.createCell(r, j++, dp.getMunicipio().getNombre());
                    XlsUtils.createCell(r, j++, dp.getLocalidad());
                    XlsUtils.createCell(r, j++, i.getEstatus().getNombre());
                    XlsUtils.createCell(r, j++, con.getFolio());
                    XlsUtils.createCell(r, j++, con.getNumeroContrato());
                    XlsUtils.createCell(r, j++, con.getCantidadContratada());
                    XlsUtils.createCell(r, j++, con.getCantidadContratadaCobertura());
                    XlsUtils.createCell(r, j++, con.getEmpresa());
                    XlsUtils.createCell(r, j++, con.getTipoPrecio().getNombre());
                    renglon++;
                }
            }
        }

        for (int i = 0; i < encabezados.length; i++) {
            s.autoSizeColumn(i);
        }

        return XlsUtils.creaArchivo(wb);
    }
}
