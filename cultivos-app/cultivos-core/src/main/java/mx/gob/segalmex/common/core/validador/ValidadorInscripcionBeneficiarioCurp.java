/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.validador;

import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Component("validadorInscripcionBeneficiarioCurp")
@Slf4j
public class ValidadorInscripcionBeneficiarioCurp implements ValidadorInscripcion {

    @Override
    public void valida(InscripcionProductor inscripcion) {
        LOGGER.debug("Validando CURP del beneficiario.");
        String curp = inscripcion.getCurpBeneficiario();
        ValidadorCurpHelper.valida(curp, "del beneficiario");
    }

}
