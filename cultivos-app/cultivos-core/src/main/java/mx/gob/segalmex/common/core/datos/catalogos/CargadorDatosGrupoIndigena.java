package mx.gob.segalmex.common.core.datos.catalogos;

import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.GrupoIndigena;

/**
 *
 * @author Oscar
 */
public class CargadorDatosGrupoIndigena extends AbstractCargadorDatosCatalogo<GrupoIndigena> {

    @Override
    public GrupoIndigena getInstance(String[] elementos) {
        GrupoIndigena grupo = new GrupoIndigena();
        grupo.setClave(elementos[0]);
        grupo.setNombre(elementos[1]);
        grupo.setActivo(true);
        grupo.setOrden(Integer.parseInt(elementos[2]));
        return grupo;
    }

}
