/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.pago;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component
@Profile("prod")
@Slf4j
public class HttpRequestSender implements RequestSender {

    @Override
    public String send(String url, String json) {
        Objects.requireNonNull(url, "La url del servicio no puede ser nula.");
        OutputStream os = null;
        BufferedReader br = null;
        try {
            URL u = new URL(url);
            HttpURLConnection con = (HttpURLConnection) u.openConnection();
            con.setRequestMethod("POST");
            con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
            con.setRequestProperty("Accept", "application/json");
            con.setDoOutput(true);

            os = con.getOutputStream();
            byte[] input = json.getBytes("utf-8");
            os.write(input, 0, input.length);

            br = new BufferedReader(new InputStreamReader(con.getInputStream(), "utf-8"));
            StringBuilder response = new StringBuilder();
            String responseLine;
            while ((responseLine = br.readLine()) != null) {
                response.append(responseLine.trim());
            }

            os.close();
            br.close();
            con.disconnect();
            return response.toString();
        } catch (IOException ouch) {
            LOGGER.error("Error de entrada salida.", ouch);
            throw new RuntimeException(ouch);
        } finally {
            try {
                if (Objects.nonNull(os)) {
                    os.close();
                }
            } catch (IOException ouch) {
                LOGGER.error("Error al cerrar el output stream.", ouch);
            }
            try {
                if (Objects.nonNull(br)) {
                    br.close();
                }
            } catch (IOException ouch) {
                LOGGER.error("Error al cerrar el buffer reader.", ouch);
            }
        }
    }
}
