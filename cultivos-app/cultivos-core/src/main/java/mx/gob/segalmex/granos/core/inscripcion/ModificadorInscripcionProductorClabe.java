/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.inscripcion;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.validador.ValidadorClabeHelper;
import mx.gob.segalmex.pluss.modelo.catalogos.Banco;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import mx.gob.segalmex.granos.core.productor.DatoCapturadoProductorEnum;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.Inscripcion;
import mx.gob.segalmex.pluss.modelo.productor.CuentaBancaria;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component("modificadorInscripcionProductorClabe")
@Slf4j
public class ModificadorInscripcionProductorClabe implements ModificadorInscripcion {

    private static final String PERMITE_DUPLICADO = ":permite-duplicado";

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorInscripcionProductor buscadorInscripcion;

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Override
    public void modifica(Inscripcion inscripcion, Map<String, DatoCapturado> datos) {
        DatoCapturado d = datos.get(DatoCapturadoProductorEnum.CLABE_PRODUCTOR.getClave());
        if (Objects.isNull(d)) {
            return;
        }
        if (Objects.isNull(d.getCorrecto())) {
            return;
        }
        if (d.getCorrecto()) {
            return;
        }

        String clabe = d.getValor();
        ValidadorClabeHelper.valida(clabe);
        InscripcionProductor ip = (InscripcionProductor) inscripcion;
        CuentaBancaria cb = ip.getCuentaBancaria();

        if (!omitir(ip.getCultivo(), ip.getCiclo(), clabe + PERMITE_DUPLICADO)) {
            LOGGER.info("Se valida duplicidad clabe.");
            ParametrosInscripcionProductor params = new ParametrosInscripcionProductor();
            params.setCultivo(ip.getCultivo());
            params.setCiclo(ip.getCiclo());
            params.setClabe(clabe);
            List<InscripcionProductor> inscripciones = buscadorInscripcion.busca(params);
            if (!inscripciones.isEmpty()) {
                LOGGER.warn("La clabe bancaria del productor ya existe, no se actualiza la inscripcion: {}", ip.getFolio());
                return;
            }
        }
        cb.setClabe(clabe);
        cb.setBanco(buscadorCatalogo.buscaElemento(Banco.class, clabe.substring(0, 3)));
        cb.setNumero(clabe.substring(6, clabe.length() - 1));
    }

    private boolean omitir(Cultivo c, CicloAgricola ciclo, String clave) {
        Parametro p = buscadorParametro.buscaElementoOpcional(c.getClave() + ":" + ciclo.getClave() + ":" + clave);
        return Objects.nonNull(p) && Boolean.valueOf(p.getValor());
    }

}
