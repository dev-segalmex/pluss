/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato.xls;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;

/**
 * Creador de excel para las inscripciones de contratos.
 *
 * @author oscar
 */
public class InscripcionContratoXlsCreator {

    private AbstractConstratoXlsHelper helper;

    public byte[] exporta(List<InscripcionContrato> inscripciones, Cultivo c) {
        switch (CultivoEnum.getInstance(c.getClave())) {
            case MAIZ_COMERCIAL:
                helper = new InscripcionContratoMaizXls();
                break;
            case TRIGO:
                helper = new InscripcionContratoTrigoXls();
                break;
            default:
                throw new IllegalArgumentException("No existe xls para este cultivo.");
        }

        return helper.exporta(inscripciones);
    }

}
