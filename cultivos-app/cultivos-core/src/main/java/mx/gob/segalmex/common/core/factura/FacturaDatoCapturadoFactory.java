/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura;

import java.util.List;
import java.util.Objects;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.pluss.modelo.archivos.ArchivoRelacionado;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import mx.gob.segalmex.granos.core.inscripcion.DatoCapturadoFactory;
import mx.gob.segalmex.granos.core.inscripcion.InscripcionDatoCapturado;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivoEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Clase que genera instancias de <code>DatoCapturado</code> a partir del modelo
 * de datos de <code>InscripcionFactura</code>.
 *
 * @author ismael
 */
@Component
public class FacturaDatoCapturadoFactory extends DatoCapturadoFactory {

    private static final String PRECIO_TONELADA = "Precio por tonelada";

    private static final String APLICA_AJUSTE_FACTURA = ":aplica-ajuste-factura";

    private static final String APLICA_RECIBO_ETIQUETA = ":aplica-recibo-etiqueta";

    @Autowired
    private BuscadorParametro buscadorParametro;

    public List<DatoCapturado> getInstance(InscripcionFactura inscripcion, List<ArchivoRelacionado> archivos) {
        InscripcionDatoCapturado idc = new InscripcionDatoCapturado();
        switch (inscripcion.getTipoFactura()) {
            case "productor":
                if (Objects.nonNull(StringUtils.trimToNull(inscripcion.getContrato()))) {
                    idc.agrega(getInstance(inscripcion, DatoCapturadoFacturaEnum.NUMERO_CONTRATO,
                            StringUtils.trimToEmpty(inscripcion.getContrato())));
                }
                //Se agrega el tipo_cultivo y el estado
                idc.agrega(getInstance(inscripcion, DatoCapturadoFacturaEnum.TIPO_CULTIVO,
                        inscripcion.getTipoCultivo()));

                idc.agrega(getInstance(inscripcion, DatoCapturadoFacturaEnum.ESTADO_FACTURA,
                        inscripcion.getEstado()));

                break;
        }

        DatoCapturado archivo = getInstance(inscripcion, DatoCapturadoFacturaEnum.CFDI_PDF,
                DatoCapturadoFacturaEnum.CFDI_PDF.getClave());
        archivo.setTipo("archivo");
        idc.agrega(archivo);

        switch (CultivoEnum.getInstance(inscripcion.getCultivo().getClave())) {
            case ARROZ:
                archivo = getInstance(inscripcion, DatoCapturadoFacturaEnum.COMPROBANTE_RECEPCION_PDF,
                        DatoCapturadoFacturaEnum.COMPROBANTE_RECEPCION_PDF.getClave());
                archivo.setTipo("archivo");
                idc.agrega(archivo);
                archivo = getInstance(inscripcion, DatoCapturadoFacturaEnum.COMPROBANTE_PAGO_PDF,
                        DatoCapturadoFacturaEnum.COMPROBANTE_PAGO_PDF.getClave());
                archivo.setTipo("archivo");
                idc.agrega(archivo);

                idc.agrega(getInstance(inscripcion, DatoCapturadoFacturaEnum.CANTIDAD_COMPROBANTE_PAGO,
                        String.valueOf(inscripcion.getCantidadComprobantePago())));

                break;
            case TRIGO:
                if (inscripcion.getTipoFactura().equals("productor")) {

                    idc.agrega(getInstance(inscripcion, DatoCapturadoFacturaEnum.FECHA_PAGO,
                            SegalmexDateUtils.format(inscripcion.getFechaPago())));

                    if (Objects.nonNull(inscripcion.getComprobantes()) && inscripcion.getComprobantes().size() > 0) {
                        archivo = getInstance(inscripcion, DatoCapturadoFacturaEnum.COMPROBANTE_RECEPCION_PDF,
                                DatoCapturadoFacturaEnum.COMPROBANTE_RECEPCION_PDF.getClave());
                        archivo.setTipo("archivo");
                        idc.agrega(archivo);
                    }

                    if (inscripcion.getTipoCultivo().getClave().equals(TipoCultivoEnum.TRIGO_SEMILLA.getClave())
                            && aplicaParametro(inscripcion.getCultivo(), inscripcion.getCiclo(), APLICA_RECIBO_ETIQUETA)) {
                        archivo = getInstance(inscripcion, DatoCapturadoFacturaEnum.RECIBO_ETIQUETA_PDF,
                                DatoCapturadoFacturaEnum.RECIBO_ETIQUETA_PDF.getClave());
                        archivo.setTipo("archivo");
                        idc.agrega(archivo);
                    }
                }

                break;
        }

        if (inscripcion.getTipoFactura().equals("productor")
                && aplicaParametro(inscripcion.getCultivo(), inscripcion.getCiclo(), APLICA_AJUSTE_FACTURA)) {
            idc.agrega(getInstance(inscripcion, DatoCapturadoFacturaEnum.PRECIO_TONELADA_REAL,
                    String.valueOf(inscripcion.getPrecioToneladaReal()), PRECIO_TONELADA));
        }
        return idc.getDatos();
    }

    private boolean aplicaParametro(Cultivo c, CicloAgricola seleccionado, String clave) {
        Parametro p = buscadorParametro.buscaElementoOpcional(c.getClave() + ":" + seleccionado.getClave() + clave);
        return Objects.nonNull(p) && Boolean.valueOf(p.getValor());
    }

}
