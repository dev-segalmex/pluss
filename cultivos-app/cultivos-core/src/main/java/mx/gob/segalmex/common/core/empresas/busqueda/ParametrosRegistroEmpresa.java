/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.empresas.busqueda;

import java.util.Calendar;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;

/**
 *
 * @author ismael
 */
@Getter
@Setter
public class ParametrosRegistroEmpresa {

    private String rfc;

    private String uuid;

    private Cultivo cultivo;

    private CicloAgricola ciclo;

    private Integer id;

    private EstatusInscripcion estatus;

    private Usuario usuarioValidador;

    private Calendar fechaCreacion;

    private Calendar fechaFin;
}
