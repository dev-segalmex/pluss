/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.historico;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.historico.EntidadHistorico;
import mx.gob.segalmex.pluss.modelo.historico.HistoricoRegistro;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcionEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ismael
 */
@Service("procesadorHistoricoRegistro")
@Slf4j
public class ProcesadorHistoricoRegistroJpa implements ProcesadorHistoricoRegistro {

    /**
     * Formato para indicar que un historico ya no es monitoreable.
     */
    private static final String FORMATO_NO_MONITOREABLE = "yyyy-MM-dd";

    /**
     * Formato de historicos monitoreables.
     */
    private static final String FORMATO_MONITOREABLE = "--";

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private BuscadorHistoricoRegistro buscadorHistoricoRegistro;

    @Transactional
    @Override
    public void crea(EntidadHistorico entidad, TipoHistoricoInscripcionEnum tipo, Usuario usuario,
            boolean abierto, String etiquetaGrupo) {
        crea(entidad, tipo, usuario, abierto, false, etiquetaGrupo);
    }

    @Transactional
    @Override
    public void finaliza(HistoricoRegistro historico, Usuario usuario) {
        LOGGER.debug("finalizando histórico...");
        Objects.requireNonNull(usuario, "El usuario que finaliza no puede ser nulo.");

        historico.setFechaFinaliza(Calendar.getInstance());
        historico.setUsuarioFinaliza(usuario);
        historico.setMonitoreable(formatNoMonitoreable(historico.getFechaFinaliza()));

        registroEntidad.actualiza(historico);
    }

    @Transactional
    @Override
    public void crea(EntidadHistorico entidad, TipoHistoricoInscripcionEnum tipo, Usuario usuario,
            boolean abierto, boolean forzarCreacion, String etiquetaGrupo) {
        LOGGER.debug("Crea histórico {} forzado {} para registro con id {}.",
                tipo.getClave(), forzarCreacion, entidad.getId());
        ParametrosHistoricoRegistro parametros = new ParametrosHistoricoRegistro();
        parametros.setEntidad(entidad);
        parametros.setTipo(buscadorCatalogo.buscaElemento(TipoHistoricoInscripcion.class,
                tipo));

        if (forzarCreacion || buscadorHistoricoRegistro.busca(parametros).isEmpty()) {
            HistoricoRegistro h = new HistoricoRegistro();
            h.setEtiquetaGrupo(etiquetaGrupo);
            h.setTipo(parametros.getTipo());
            h.setClase(entidad.getClass().getName());
            h.setReferencia(entidad.getUuid());
            h.setUsuarioRegistra(usuario);
            h.setMonitoreable(FORMATO_MONITOREABLE);
            if (!abierto) {
                h.setFechaFinaliza(Calendar.getInstance());
                h.setUsuarioFinaliza(usuario);
                h.setMonitoreable(formatNoMonitoreable(h.getFechaFinaliza()));
            }
            registroEntidad.guarda(h);
            LOGGER.debug("Se creó histórico {} para inscripcion con id {}.", tipo.getClave(),
                    entidad.getId());
        }

    }

    private String formatNoMonitoreable(Calendar fecha) {
        DateFormat df = new SimpleDateFormat(FORMATO_NO_MONITOREABLE);
        return df.format(fecha.getTime());
    }

}
