/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.factura.EstatusUsoFacturaEnum;
import mx.gob.segalmex.pluss.modelo.factura.UsoFactura;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;

/**
 *
 * @author jurgen
 */
public interface BuscadorUsoFactura {

    UsoFactura buscaElementoUuid(String uuid);

    List<UsoFactura> buscaElementoFolio(String folio);

    List<UsoFactura> busca(ParametrosUsoFactura parametros);

    List<Object[]> buscaReportePlaneacion(CicloAgricola ciclo, Cultivo cultivo,
            EstatusUsoFacturaEnum... considerados);
}
