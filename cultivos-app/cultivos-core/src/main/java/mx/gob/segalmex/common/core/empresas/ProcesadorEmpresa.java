/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.empresas;

import mx.gob.segalmex.pluss.modelo.catalogos.UsoCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.Empresa;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;

/**
 *
 * @author jurgen
 */
public interface ProcesadorEmpresa {

    Empresa procesa(Empresa inscripcion);

    Sucursal procesa(Sucursal sucursal);

    UsoCatalogo procesa(UsoCatalogo uso);

    Sucursal actualiza(Sucursal actual, Sucursal nueva);
}
