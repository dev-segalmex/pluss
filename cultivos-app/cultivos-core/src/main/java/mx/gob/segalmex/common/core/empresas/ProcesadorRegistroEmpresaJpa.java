/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.empresas;

import java.util.List;
import java.util.Objects;
import java.util.UUID;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorUsoCatalogo;
import mx.gob.segalmex.common.core.empresas.busqueda.BuscadorRegistroEmpresa;
import mx.gob.segalmex.common.core.empresas.busqueda.ParametrosRegistroEmpresa;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.pluss.modelo.empresas.DatosComercializacion;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroEmpresa;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroEmpresaBasico;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroSucursal;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.granos.core.historico.BuscadorHistoricoRegistro;
import mx.gob.segalmex.granos.core.historico.ParametrosHistoricoRegistro;
import mx.gob.segalmex.granos.core.historico.ProcesadorHistoricoRegistro;
import mx.gob.segalmex.granos.core.inscripcion.ProcesadorDatoCapturado;
import mx.gob.segalmex.pluss.modelo.historico.HistoricoRegistro;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcionEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ismael
 */
@Service("procesadorRegistroEmpresa")
public class ProcesadorRegistroEmpresaJpa implements ProcesadorRegistroEmpresa {

    private static final String CLAVE_USUARIO = "anonimo@segalmex.gob.mx";

    private static final String CLAVE_USO = "usuario:registro-anonimo-default";

    @Autowired
    private BuscadorRegistroEmpresa buscadorRegistroEmpresa;

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private ProcesadorHistoricoRegistro procesadorHistorico;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorHistoricoRegistro buscadorHistorico;

    @Autowired
    private BuscadorUsoCatalogo buscadorUso;

    @Autowired
    private ProcesadorDatoCapturado procesadorDatoCapturado;

    @Transactional
    @Override
    public RegistroEmpresaBasico registra(RegistroEmpresaBasico basico, Usuario u) {
        Objects.requireNonNull(u, "El usuario no puede ser nulo.");
        if (Objects.nonNull(basico.getId())) {
            throw new IllegalArgumentException("El registro ya cuenta con id.");
        }
        ParametrosRegistroEmpresa parametros = new ParametrosRegistroEmpresa();
        parametros.setCiclo(basico.getCiclo());
        parametros.setCultivo(basico.getCultivo());
        parametros.setRfc(basico.getRfc());
        if (!buscadorRegistroEmpresa.buscaBasico(parametros).isEmpty()) {
            throw new SegalmexRuntimeException("Error al registrar.",
                    "Ya existe un registro con el mismo RFC, ciclo y cultivo.");
        }

        basico.setUuid(UUID.randomUUID().toString());
        basico.setEditable(true);
        basico.setUsuario(u);
        registroEntidad.guarda(basico);
        return basico;
    }

    @Transactional
    @Override
    public RegistroEmpresa procesa(RegistroEmpresaBasico basico, RegistroEmpresa registro) {
        if (Objects.nonNull(registro.getId())) {
            throw new IllegalArgumentException("El registro ya cuenta con id.");
        }
        ParametrosRegistroEmpresa parametros = new ParametrosRegistroEmpresa();
        parametros.setCiclo(basico.getCiclo());
        parametros.setCultivo(basico.getCultivo());
        parametros.setRfc(basico.getRfc());
        if (!buscadorRegistroEmpresa.busca(parametros).isEmpty()) {
            throw new SegalmexRuntimeException("Error al registrar.",
                    "Ya existe un registro con el mismo RFC, ciclo y cultivo.");
        }

        // Marcamos el básico como no actualizable
        basico.setEditable(false);
        registroEntidad.actualiza(basico);
        registroEntidad.guarda(registro.getDomicilio());
        registro.setEditable(false);
        registroEntidad.guarda(registro);
        Usuario usuarioRegistra = buscadorUso.buscaElemento(Usuario.class, CLAVE_USUARIO, CLAVE_USO);

        // Generamos históricos de creación cerrado y asignación abierto
        procesadorHistorico.crea(registro, TipoHistoricoInscripcionEnum.CREACION,
                usuarioRegistra, false, getEtiquetaGrupo(registro));
        procesadorHistorico.crea(registro, TipoHistoricoInscripcionEnum.ASIGNACION,
                usuarioRegistra, true, getEtiquetaGrupo(registro));

        int orden = 0;
        for (DatosComercializacion datos : registro.getDatosComercializacion()) {
            datos.setRegistroEmpresa(registro);
            datos.setOrden(orden++);
            registroEntidad.guarda(datos);
        }
        orden = 0;
        for (RegistroSucursal s : registro.getSucursales()) {
            s.setRegistroEmpresa(registro);
            s.setOrden(orden++);
            registroEntidad.guarda(s.getDomicilio());
            registroEntidad.guarda(s);
        }

        return registro;
    }

    @Transactional
    @Override
    public void asigna(RegistroEmpresa registro, Usuario asigna, Usuario validador) {
        registro.setUsuarioValidador(validador);
        registro.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.VALIDACION));

        ParametrosHistoricoRegistro parametros = new ParametrosHistoricoRegistro();
        parametros.setEntidad(registro);
        parametros.setTipo(buscadorCatalogo.buscaElemento(TipoHistoricoInscripcion.class,
                TipoHistoricoInscripcionEnum.ASIGNACION));

        HistoricoRegistro asignacion = buscadorHistorico.busca(parametros).get(0);
        procesadorHistorico.finaliza(asignacion, asigna);
        procesadorHistorico.crea(registro, TipoHistoricoInscripcionEnum.VALIDACION,
                asigna, true, getEtiquetaGrupo(registro));
        registroEntidad.actualiza(registro);
        procesadorDatoCapturado.procesa(registro);
    }

    @Transactional
    @Override
    public void validaPositivo(RegistroEmpresa registro, Usuario usuario, List<DatoCapturado> capturados) {
        switch (registro.getEstatus().getClave()) {
            case "correccion":
            case "finalizacion":
                throw new SegalmexRuntimeException("Error:", "El registro ya se encuentra validado: " + registro.getEstatus().getNombre());
        }

        procesadorDatoCapturado.validaEstatus(registro, capturados, EstatusInscripcionEnum.POSITIVA);
        // Cancelamos los históricos de validación.
        cancelaHistoricos(registro, usuario, TipoHistoricoInscripcionEnum.VALIDACION);
        // Actualizamos la inscripción y los datos capturados
        registro.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.CORRECTO.getClave()));
        procesadorHistorico.crea(registro, TipoHistoricoInscripcionEnum.FINALIZACION,
                usuario, true, true, getEtiquetaGrupo(registro));
        registroEntidad.actualiza(registro);
    }

    @Transactional
    @Override
    public void validaNegativo(RegistroEmpresa registro, Usuario usuario, List<DatoCapturado> capturados) {
        registro.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.CORRECCION));
        procesadorDatoCapturado.solicitaCorreccion(registro, capturados);

        // Cancelamos los históricos de validación.
        cancelaHistoricos(registro, usuario, TipoHistoricoInscripcionEnum.VALIDACION);
        // Generamos el histórico de corrección
        procesadorHistorico.crea(registro, TipoHistoricoInscripcionEnum.CORRECCION,
                usuario, true, true, getEtiquetaGrupo(registro));
        // Lo reasignamos al usuario que registró
        registroEntidad.actualiza(registro);
    }

    @Transactional
    @Override
    public void corrige(RegistroEmpresa registro, List<DatoCapturado> capturados) {
        switch (registro.getEstatus().getClave()) {
            case "revalidacion":
                throw new SegalmexRuntimeException("Error:", "El registro se encuentra en estatus: " + registro.getEstatus().getNombre());
        }
        // Validamos los valores modificados
        procesadorDatoCapturado.corrige(registro, capturados);

        // Cancelamos históricos de corrección
        Usuario usuario = buscadorUso.buscaElemento(Usuario.class, CLAVE_USUARIO, CLAVE_USO);
        cancelaHistoricos(registro, usuario, TipoHistoricoInscripcionEnum.CORRECCION);

        // Generamos el histórico de revalidación.
        procesadorHistorico.crea(registro, TipoHistoricoInscripcionEnum.REVALIDACION,
                usuario, true, true, getEtiquetaGrupo(registro));
        registro.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.SOLVENTADA));
        registroEntidad.actualiza(registro);
    }

    private int cancelaHistoricos(RegistroEmpresa registro, Usuario usuario, TipoHistoricoInscripcionEnum tipo) {
        List<HistoricoRegistro> historicos = buscadorHistorico.busca(registro,
                buscadorCatalogo.buscaElemento(TipoHistoricoInscripcion.class, tipo), false);
        for (HistoricoRegistro h : historicos) {
            if (Objects.isNull(h.getFechaFinaliza())) {
                procesadorHistorico.finaliza(h, usuario);
            }
        }
        return historicos.size();
    }

    /**
     * Se encarga de armar la etiquetaGrupo que debe llevar su Histórico.
     *
     * @param registro
     * @return
     */
    private String getEtiquetaGrupo(RegistroEmpresa registro) {
        return registro.getCultivo().getClave() + ":" + registro.getCiclo().getClave();
    }
}
