/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.busqueda;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.productor.Asociado;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ismael
 */
@Repository("buscadorSociedad")
public class BuscadorSociedadJpa implements BuscadorSociedad {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Asociado> busca(String rfc, Cultivo cultivo, CicloAgricola ciclo) {
        return entityManager.createQuery("SELECT a FROM Asociado a "
                + "WHERE "
                + "a.sociedad.clave = :rfc "
                + "AND a.cultivo = :cultivo "
                + "AND a.ciclo = :ciclo "
                + "AND a.activo = :activo ", Asociado.class)
                .setParameter("rfc", rfc)
                .setParameter("cultivo", cultivo)
                .setParameter("ciclo", ciclo)
                .setParameter("activo", Boolean.TRUE)
                .getResultList();
    }

    @Override
    public List<Asociado> busca(String rfc) {
        return entityManager.createQuery("SELECT a FROM Asociado a "
                + "WHERE "
                + "a.sociedad.clave = :rfc "
                + "AND a.activo = :activo ", Asociado.class)
                .setParameter("rfc", rfc)
                .setParameter("activo", Boolean.TRUE)
                .getResultList();
    }

}
