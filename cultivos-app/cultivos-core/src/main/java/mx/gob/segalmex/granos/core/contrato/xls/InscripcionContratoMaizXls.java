/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato.xls;

import java.util.List;
import java.util.Objects;
import mx.gob.segalmex.common.core.util.XlsUtils;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

/**
 *
 * @author jurgen
 */
public class InscripcionContratoMaizXls extends AbstractConstratoXlsHelper {

    @Override
    public byte[] exporta(List<InscripcionContrato> inscripciones) {
        SXSSFWorkbook wb = new SXSSFWorkbook(100);
        CreationHelper ch = wb.getCreationHelper();
        Sheet s = wb.createSheet();

        String[] encabezados = new String[]{
            "No.",
            "Folio",
            "Fecha de registro",
            "No. de contrato",
            "RFC de la empresa",
            "Empresa",
            "Estado",
            "Toneladas",
            "Tipo precio",
            "Precio futuro",
            "Base",
            "Precio fijo",
            "Tipo cultivo",
            "Estatus",
            "Comentario",
            "Tipo comprador",
            "RFC comprador",
            "Nombre comprador",
            "Tipo vendedor",
            "RFC vendedor",
            "Nombre vendedor",
            "Usuario registra",
            "Usuario validador"
        };

        XlsUtils.creaEncabezados(s, encabezados);

        CellStyle cs = wb.createCellStyle();
        cs.setDataFormat(ch.createDataFormat().getFormat("dd/MM/yyyy HH:mm"));

        int renglon = 1;
        for (InscripcionContrato i : inscripciones) {
            int j = 0;
            Row r = s.createRow(renglon);
            XlsUtils.createCell(r, j++, renglon);
            XlsUtils.createCell(r, j++, i.getFolio());
            XlsUtils.createCell(r, j++, i.getFechaCreacion(), cs);
            XlsUtils.createCell(r, j++, i.getNumeroContrato());
            XlsUtils.createCell(r, j++, i.getSucursal().getEmpresa().getRfc());
            XlsUtils.createCell(r, j++, i.getSucursal().getEmpresa().getNombre());
            XlsUtils.createCell(r, j++, i.getEstado().getNombre());
            XlsUtils.createCell(r, j++, i.getVolumenTotal());
            XlsUtils.createCell(r, j++, i.getTipoPrecio().getNombre());
            XlsUtils.createCell(r, j++, i.getPrecioFuturo());
            XlsUtils.createCell(r, j++, i.getBaseAcordada());
            XlsUtils.createCell(r, j++, i.getPrecioFijo());

            XlsUtils.createCell(r, j++, i.getTipoCultivo().getNombre());
            XlsUtils.createCell(r, j++, i.getEstatus().getNombre());
            XlsUtils.createCell(r, j++, Objects.nonNull(i.getComentarioEstatus())
                    ? i.getComentarioEstatus() : "--");
            XlsUtils.createCell(r, j++, i.getTipoEmpresaComprador().getNombre());
            XlsUtils.createCell(r, j++, i.getRfcComprador());
            XlsUtils.createCell(r, j++, i.getNombreComprador());
            XlsUtils.createCell(r, j++, i.getTipoEmpresaVendedor().getNombre());
            XlsUtils.createCell(r, j++, i.getRfcVendedor());
            XlsUtils.createCell(r, j++, i.getNombreVendedor());
            XlsUtils.createCell(r, j++, i.getUsuarioRegistra().getNombre());
            XlsUtils.createCell(r, j++, Objects.nonNull(i.getUsuarioValidador()) ? i.getUsuarioValidador().getNombre() : "--");
            renglon++;
        }
        for (int i = 0; i < encabezados.length; i++) {
            s.autoSizeColumn(i);
        }

        return XlsUtils.creaArchivo(wb);
    }

}
