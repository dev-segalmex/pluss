/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura;

import java.util.Objects;
import java.util.UUID;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.folio.GeneradorFolio;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionDocumentoComercial;
import mx.gob.segalmex.pluss.modelo.secuencias.TipoFolioEnum;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ismael
 */
@Service("procesadorInscripcionDocumentoComercial")
public class ProcesadorInscripcionDocumentoComercialJpa implements ProcesadorInscripcionDocumentoComercial {

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private GeneradorFolio generadorFolio;

    @Transactional
    @Override
    public InscripcionDocumentoComercial procesa(InscripcionDocumentoComercial inscripcion,
            Usuario usuario) {
        Objects.requireNonNull(inscripcion, "La inscripcion no puede ser nula.");
        Objects.requireNonNull(usuario, "El usuario no puede ser nulo.");
        inscripcion.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.PENDIENTE));
        inscripcion.setEstatusPrevio(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.DESCONOCIDO));
        inscripcion.setUuid(UUID.randomUUID().toString());
        inscripcion.setFolio(generadorFolio.genera(TipoFolioEnum.SEQ_FOLIO_DOCUMENTO_COMERCIAL, 6));
        inscripcion.setUsuarioRegistra(usuario);
        inscripcion.setRfcEmpresa(inscripcion.getSucursal().getEmpresa().getRfc());
        inscripcion.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class,
                inscripcion.getCiclo().getId()));
        registroEntidad.guarda(inscripcion);

        return inscripcion;
    }

}
