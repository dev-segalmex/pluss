/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.inscripcion;

import java.util.Map;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.Inscripcion;

/**
 *
 * @author ismael
 */
public interface ModificadorInscripcion {

    /**
     * Realiza la modificación de una inscripción que podría requerir cambios o verificaciones
     * adicionales.
     *
     * @param inscripcion la inscripción que se va a modificar.
     * @param datos los datos capturados modificados.
     */
    void modifica(Inscripcion inscripcion, Map<String, DatoCapturado> datos);
}
