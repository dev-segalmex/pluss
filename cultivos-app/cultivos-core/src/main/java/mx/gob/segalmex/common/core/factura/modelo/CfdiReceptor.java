/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.modelo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 *
 * @author ismael
 */
@Getter
@Setter
@XmlAccessorType(XmlAccessType.NONE)
public class CfdiReceptor {

    @XmlAttribute(name = "Rfc")
    private String rfc;

    @XmlAttribute(name = "Nombre")
    private String nombre;

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("rfc", rfc)
                .append("nombre", nombre)
                .toString();
    }
}
