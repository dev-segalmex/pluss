/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.validador;

import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;

/**
 *
 * @author jurgen
 */
public interface ValidadorInscripcion {

    void valida (InscripcionProductor inscripcion);
}
