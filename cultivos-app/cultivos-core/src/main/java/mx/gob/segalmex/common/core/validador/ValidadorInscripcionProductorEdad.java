/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.validador;

import java.util.Calendar;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Component("validadorInscripcionProductorEdad")
@Slf4j
public class ValidadorInscripcionProductorEdad implements ValidadorInscripcion {

    private static final int MAYOR_EDAD = 18;

    @Override
    public void valida(InscripcionProductor inscripcion) {
        LOGGER.debug("Validando mayoría de edad del productor.");
        Calendar fechaNacimientoProductor = (Calendar) inscripcion.getDatosProductor().
                getFechaNacimiento().clone();

        int edad = SegalmexDateUtils.aniosTranscurridos(fechaNacimientoProductor, Calendar.getInstance());
        LOGGER.debug("Edad del productor {}.", edad);
        if (edad < MAYOR_EDAD) {
            throw new SegalmexRuntimeException("Error al realizar el registro.",
                    "El productor no es mayor de edad.");
        }
    }

}
