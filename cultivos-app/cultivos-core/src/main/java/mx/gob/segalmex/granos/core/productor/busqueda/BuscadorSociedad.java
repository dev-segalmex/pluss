/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.productor.Asociado;

/**
 *
 * @author ismael
 */
public interface BuscadorSociedad {

    List<Asociado> busca(String rfc, Cultivo cultivo, CicloAgricola ciclo);

    List<Asociado> busca(String rfc);
}
