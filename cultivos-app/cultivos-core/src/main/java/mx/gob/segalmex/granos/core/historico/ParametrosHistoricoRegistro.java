/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.historico;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.historico.EntidadHistorico;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcion;

/**
 *
 * @author ismael
 */
public class ParametrosHistoricoRegistro {

    private Class clase;

    private EntidadHistorico entidad;

    private Cultivo cultivo;

    private TipoHistoricoInscripcion tipo;

    private Boolean monitoreable;

    private List<TipoHistoricoInscripcion> tipos;

    private EstatusInscripcion estatus;

    private Usuario usuarioAsignado;

    private Boolean activos = Boolean.TRUE;

    private String etiquetaGrupo;

    private int maximo = 0;

    private String tipoRegistro;

    private CicloAgricola ciclo;

    public Class getClase() {
        return clase;
    }

    public void setClase(Class clase) {
        this.clase = clase;
    }

    public EntidadHistorico getEntidad() {
        return entidad;
    }

    public void setEntidad(EntidadHistorico entidad) {
        this.entidad = entidad;
        this.clase = entidad.getClass();
    }

    public Cultivo getCultivo() {
        return cultivo;
    }

    public void setCultivo(Cultivo cultivo) {
        this.cultivo = cultivo;
    }

    public TipoHistoricoInscripcion getTipo() {
        return tipo;
    }

    public void setTipo(TipoHistoricoInscripcion tipo) {
        this.tipo = tipo;
    }

    public Boolean getMonitoreable() {
        return monitoreable;
    }

    public void setMonitoreable(Boolean monitoreable) {
        this.monitoreable = monitoreable;
    }

    public List<TipoHistoricoInscripcion> getTipos() {
        return tipos;
    }

    public void setTipos(List<TipoHistoricoInscripcion> tipos) {
        this.tipos = tipos;
    }

    public EstatusInscripcion getEstatus() {
        return estatus;
    }

    public void setEstatus(EstatusInscripcion estatus) {
        this.estatus = estatus;
    }

    public Usuario getUsuarioAsignado() {
        return usuarioAsignado;
    }

    public void setUsuarioAsignado(Usuario usuarioAsignado) {
        this.usuarioAsignado = usuarioAsignado;
    }

    public Boolean getActivos() {
        return activos;
    }

    public void setActivos(Boolean activos) {
        this.activos = activos;
    }

    public String getEtiquetaGrupo() {
        return etiquetaGrupo;
    }

    public void setEtiquetaGrupo(String etiquetaGrupo) {
        this.etiquetaGrupo = etiquetaGrupo;
    }

    public int getMaximo() {
        return maximo;
    }

    public void setMaximo(int maximo) {
        this.maximo = maximo;
    }

    public String getTipoRegistro() {
        return tipoRegistro;
    }

    public void setTipoRegistro(String tipoRegistro) {
        this.tipoRegistro = tipoRegistro;
    }

    public CicloAgricola getCiclo() {
        return ciclo;
    }

    public void setCiclo(CicloAgricola ciclo) {
        this.ciclo = ciclo;
    }
}
