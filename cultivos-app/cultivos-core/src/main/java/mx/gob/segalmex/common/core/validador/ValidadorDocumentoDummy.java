/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.validador;

import mx.gob.segalmex.pluss.modelo.productor.PredioDocumento;

/**
 *
 * @author oscar
 */
public class ValidadorDocumentoDummy implements ValidadorDocumento {

    /**
     * Valida como positivos todos los {@link PredioDocumento} que el uuid del
     * timbre fiscal no terminan en triple cero.
     *
     * @param predioDocumento el {@link PredioDocumento} a validar.
     * @return <code>true</code> cuando el uuid del timbre fiscal digital no
     * termina en triple cero.
     */
    @Override
    public boolean valida(PredioDocumento predioDocumento) {
        return !predioDocumento.getUuidTimbreFiscalDigital().endsWith("000");
    }

}
