/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.estimulo;

import java.util.Calendar;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.estimulo.busqueda.BuscadorEstimulo;
import mx.gob.segalmex.common.core.estimulo.busqueda.ParametrosEstimulo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.pluss.modelo.pago.Estimulo;
import mx.gob.segalmex.pluss.modelo.pago.TipoEstimuloEnum;
import mx.gob.segalmex.granos.core.contrato.busqueda.BuscadorInscripcionContrato;
import mx.gob.segalmex.granos.core.contrato.busqueda.ParametrosInscripcionContrato;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jurgen
 */
@Service("procesadorEstimulo")
@Slf4j
public class ProcesadorEstimuloJpa implements ProcesadorEstimulo {

    private static final String VACIO = "--";

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private BuscadorEstimulo buscadorEstimulo;

    @Autowired
    private BuscadorInscripcionContrato buscadorContrato;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Transactional
    @Override
    public void procesa(Estimulo estimulo) {
        if (Objects.isNull(estimulo.getCiclo())) {
            throw new SegalmexRuntimeException("Error al registrar.", "El Ciclo Agrícola no puede ser nulo.");
        }

        if (Objects.isNull(estimulo.getCantidad())) {
            throw new SegalmexRuntimeException("Error al registrar.", "La cantidad no puede ser nula.");
        }
        estimulo.setFechaFin(DateUtils.truncate(estimulo.getFechaFin(), Calendar.DATE));
        if (estimulo.getTipo().equals(TipoEstimuloEnum.FACTURA.getClave())) {
            estimulo.setContrato(VACIO);
        }
        validaContrato(estimulo);
        validaFechaInicioContraFinal(estimulo.getFechaInicio(), estimulo.getFechaFin());
        verificaExiste(estimulo);
        registroEntidad.guarda(estimulo);
        LOGGER.info("Registrando un nuevo estimulo para {}", estimulo.getAplicaEstimulo());
    }

    @Transactional
    @Override
    public void modifica(Estimulo actual, Estimulo actualiza) {
        LOGGER.info("Actualizando estímulo: {}", actual.getId());
        actual.setFechaFin(DateUtils.truncate(actual.getFechaFin(), Calendar.DATE));
        validaContrato(actualiza);
        validaFechaInicioContraFinal(actualiza.getFechaInicio(), actualiza.getFechaFin());
        if (actualiza.getTipo().equals(TipoEstimuloEnum.FACTURA.getClave())) {
            actualiza.setContrato(VACIO);
        }
        verificaExiste(actualiza);
        actual.setCiclo(actualiza.getCiclo());
        actual.setEstado(actualiza.getEstado());
        actual.setCantidad(actualiza.getCantidad());
        actual.setFechaInicio(actualiza.getFechaInicio());
        actual.setFechaFin(actualiza.getFechaFin());
        actual.setOrden(actualiza.getOrden());
        actual.setAplicaEstimulo(actualiza.getAplicaEstimulo());
        actual.setTipo(actualiza.getTipo());
        actual.setContrato(actualiza.getContrato());
        actual.setTipoProductor(actualiza.getTipoProductor());
        registroEntidad.actualiza(actual);
    }

    @Transactional
    @Override
    public void elimina(Estimulo estimulo) {
        LOGGER.info("Eliminando estímulo: {}", estimulo.getId());
        estimulo.setEstatusFechaBaja(SegalmexDateUtils.format(Calendar.getInstance(), "yyyy-MM-dd'T'HH:mm:ss"));
        registroEntidad.actualiza(estimulo);
    }

    private void validaFechaInicioContraFinal(Calendar fechaInicio, Calendar fechaFin) {
        if (fechaInicio.after(fechaFin)) {
            throw new SegalmexRuntimeException("Error.", "La fecha final no puede ser menor a la inicial.");
        }
    }

    private void verificaExiste(Estimulo estimulo) {
        ParametrosEstimulo params = new ParametrosEstimulo();
        params.setAplicaEstimulo(estimulo.getAplicaEstimulo());
        params.setCiclo(estimulo.getCiclo());
        params.setEstado(estimulo.getEstado());
        params.setFechaInicio(estimulo.getFechaInicio());
        params.setFechaFin(estimulo.getFechaFin());
        params.setOrden(estimulo.getOrden());
        params.setCantidad(estimulo.getCantidad());
        params.setTipo(TipoEstimuloEnum.FACTURA.getClave());
        params.setTipoProductor(estimulo.getTipoProductor());
        if (estimulo.getTipo().equals(TipoEstimuloEnum.CONTRATO.getClave())) {
            params.setTipo(TipoEstimuloEnum.CONTRATO.getClave());
            params.setContrato(StringUtils.trimToNull(estimulo.getContrato()));
        }

        if (!buscadorEstimulo.busca(params).isEmpty()) {
            throw new SegalmexRuntimeException("Error al guardar el estímulo.",
                    "Ya existe un estímulo con estos datos.");
        }
    }

    private void validaContrato(Estimulo estimulo) {
        if (estimulo.getTipo().equals(TipoEstimuloEnum.FACTURA.getClave())) {
            return;
        }

        String contrato = StringUtils.trimToNull(estimulo.getContrato());
        if (Objects.nonNull(contrato)) {
            try {
                ParametrosInscripcionContrato parametros = new ParametrosInscripcionContrato();
                parametros.setCiclo(estimulo.getCiclo());
                parametros.setNumeroContrato(contrato);
                parametros.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.POSITIVA));
                buscadorContrato.buscaElemento(parametros);
            } catch (EmptyResultDataAccessException ouch) {
                LOGGER.error("El contrato: {} no existe como positivo.", contrato);
                throw new SegalmexRuntimeException("Error al registrar estímulo.", "El contrato: "
                        + contrato + " no existe o no está en estatus positivo.");
            }
        }
    }

}
