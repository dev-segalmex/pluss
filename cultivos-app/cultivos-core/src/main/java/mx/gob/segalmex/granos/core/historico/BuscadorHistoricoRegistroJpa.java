/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.historico;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.historico.EntidadHistorico;
import mx.gob.segalmex.pluss.modelo.historico.HistoricoRegistro;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcion;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ismael
 */
@Repository("buscadorHistoricoRegistro")
@Slf4j
public class BuscadorHistoricoRegistroJpa implements BuscadorHistoricoRegistro {

    private static final String MONITOREABLE = "--";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<HistoricoRegistro> busca(ParametrosHistoricoRegistro parametros) {
        Objects.requireNonNull(parametros.getClase(), "La clase es requerida.");
        StringBuilder sb = new StringBuilder("SELECT h, e FROM HistoricoRegistro h, ");
        sb.append(parametros.getClase().getName());
        sb.append(" e ");
        Map<String, Object> params = new HashMap<>();
        boolean primero = true;

        primero = QueryUtils.agregaWhereAnd(primero, sb);
        sb.append("h.referencia = e.uuid ");
        primero = QueryUtils.agregaWhereAnd(primero, sb);
        sb.append("h.clase = :clase ");
        params.put("clase", parametros.getClase().getName());

        if (Objects.nonNull(parametros.getEntidad())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "e.id", "id", parametros.getEntidad().getId(), params);
        }

        if (Objects.nonNull(parametros.getMonitoreable()) && parametros.getMonitoreable()) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "h.monitoreable", "monitoreable",
                    MONITOREABLE, params);
        }

        if (Objects.nonNull(parametros.getTipo())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "h.tipo", "tipo", parametros.getTipo(), params);
        }

        if (Objects.nonNull(parametros.getTipos()) && !parametros.getTipos().isEmpty()) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            sb.append("h.tipo IN (:tipos)");
            params.put("tipos", parametros.getTipos());
        }

        if (Objects.nonNull(parametros.getEstatus())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "e.estatus", "estatus", parametros.getEstatus(), params);
        }

        if (Objects.nonNull(parametros.getUsuarioAsignado())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "e.usuarioAsignado", "usuarioAsignado",
                    parametros.getUsuarioAsignado(), params);
        }

        if (Objects.nonNull(parametros.getCultivo())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "e.cultivo", "cultivo", parametros.getCultivo(), params);
        }

        if (Objects.nonNull(parametros.getCiclo())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "e.ciclo", "ciclo", parametros.getCiclo(), params);
        }

        if (Objects.nonNull(parametros.getActivos())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "h.activo", "activo",
                    parametros.getActivos(), params);
        }

        if (Objects.nonNull(parametros.getEtiquetaGrupo())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "h.etiquetaGrupo", "etiquetaGrupo",
                    parametros.getEtiquetaGrupo(), params);
        }

        if (Objects.nonNull(parametros.getTipoRegistro())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "e.tipoRegistro", "tipoRegistro", parametros.getTipoRegistro(), params);
        }

        sb.append("ORDER BY h.fechaCreacion ");
        LOGGER.debug("Ejecutando query: {}", sb);

        TypedQuery<Object[]> q
                = entityManager.createQuery(sb.toString(), Object[].class);
        QueryUtils.setParametros(q, params);
        if (parametros.getMaximo() > 0) {
            q.setMaxResults(parametros.getMaximo());
        }
        long inicio = System.currentTimeMillis();
        List<Object[]> historicos = q.getResultList();
        LOGGER.debug("Se encontraron {} históricos en {} ms.", historicos.size(),
                System.currentTimeMillis() - inicio);

        List<HistoricoRegistro> resultados = new ArrayList<>();
        for (Object[] tmp : historicos) {
            HistoricoRegistro h = (HistoricoRegistro) tmp[0];
            h.setEntidad((EntidadHistorico) tmp[1]);
            resultados.add(h);
        }
        return resultados;
    }

    @Override
    public List<HistoricoRegistro> busca(EntidadHistorico entidad) {
        Objects.requireNonNull(entidad, "El registro no puede ser nulo.");

        ParametrosHistoricoRegistro parametros = new ParametrosHistoricoRegistro();
        parametros.setEntidad(entidad);
        return busca(parametros);
    }

    @Override
    public List<HistoricoRegistro> busca(EntidadHistorico entidad, TipoHistoricoInscripcion tipo, boolean abierto) {
        Objects.requireNonNull(entidad, "El registro no puede ser nulo.");
        Objects.requireNonNull(tipo, "El tipo no puede ser nulo.");

        ParametrosHistoricoRegistro parametros = new ParametrosHistoricoRegistro();
        parametros.setEntidad(entidad);
        parametros.setTipo(tipo);
        parametros.setMonitoreable(abierto ? Boolean.TRUE : null);

        return busca(parametros);
    }

}
