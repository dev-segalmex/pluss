/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato.busqueda;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ismael
 */
@Repository("buscadorInscripcionContrato")
@Slf4j
public class BuscadorInscripcionContratoJpa implements BuscadorInscripcionContrato {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public InscripcionContrato buscaElemento(Integer id) {
        Objects.requireNonNull(id, "El id no puede ser nulo.");
        ParametrosInscripcionContrato parametros = new ParametrosInscripcionContrato();
        parametros.setId(id);

        List<InscripcionContrato> resultados = busca(parametros);
        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontraron resultados.", 1);
        }

        return resultados.get(0);
    }

    @Override
    public InscripcionContrato buscaElemento(String uuid) {
        Objects.requireNonNull(uuid, "El uuid no puede ser nulo.");
        ParametrosInscripcionContrato parametros = new ParametrosInscripcionContrato();
        parametros.setUuid(uuid);

        List<InscripcionContrato> resultados = busca(parametros);
        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontraron resultados.", 1);
        }

        return resultados.get(0);
    }

    @Override
    public InscripcionContrato buscaElementoPorFolio(String folio) {
        Objects.requireNonNull(folio, "El folio no puede ser nulo.");
        ParametrosInscripcionContrato parametros = new ParametrosInscripcionContrato();
        parametros.setFolio(folio);

        List<InscripcionContrato> resultados = busca(parametros);
        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontraron resultados.", 1);
        }

        return resultados.get(0);
    }

    @Override
    public List<InscripcionContrato> busca(ParametrosInscripcionContrato parametros) {
        StringBuilder sb = new StringBuilder("SELECT i FROM InscripcionContrato i ");

        Map<String, Object> params = constuyeParametros(parametros, sb);

        sb.append("ORDER BY i.folio ");
        LOGGER.debug("Ejecutando el query: {}", sb);
        TypedQuery<InscripcionContrato> q
                = entityManager.createQuery(sb.toString(), InscripcionContrato.class);
        QueryUtils.setParametros(q, params);

        long inicio = System.currentTimeMillis();
        List<InscripcionContrato> inscripciones = q.getResultList();
        LOGGER.debug("Se encontraron {} resultados en {} ms.", inscripciones.size(),
                System.currentTimeMillis() - inicio);

        return inscripciones;

    }

    @Override
    public List<Object[]> buscaAgrupada(ParametrosInscripcionContrato parametros) {
        StringBuilder sb = new StringBuilder("SELECT i.ciclo.id, i.estado.id,"
                + " i.estatus.id, COUNT(*) FROM InscripcionContrato i ");

        Map<String, Object> params = constuyeParametros(parametros, sb);
        sb.append(" GROUP BY i.ciclo.id, i.estado.id, i.estatus.id ");
        LOGGER.debug("Ejecutando query {} ", sb);

        TypedQuery<Object[]> q = entityManager.createQuery(sb.toString(), Object[].class);
        QueryUtils.setParametros(q, params);

        return q.getResultList();
    }

    private Map<String, Object> constuyeParametros(ParametrosInscripcionContrato parametros, StringBuilder sb) {
        Map<String, Object> params = new HashMap<>();
        boolean first = true;
        if (Objects.nonNull(parametros.getId())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.id", "id", parametros.getId(), params);
        }

        if (Objects.nonNull(parametros.getUuid())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.uuid", "uuid", parametros.getUuid(), params);
        }

        if (Objects.nonNull(parametros.getFolio())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.folio", "folio", parametros.getFolio(), params);
        }

        if (Objects.nonNull(parametros.getEstatus())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.estatus", "estatus", parametros.getEstatus(), params);
        }

        if (Objects.nonNull(parametros.getNumeroContrato())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.numeroContrato", "numeroContrato", parametros.getNumeroContrato(), params);
        }

        if (Objects.nonNull(parametros.getRfcEmpresa())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.rfcEmpresa", "rfcEmpresa", parametros.getRfcEmpresa(), params);
        }

        if (Objects.nonNull(parametros.getFechaInicio())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("i.fechaCreacion >= :fechaInicio ");
            params.put("fechaInicio", parametros.getFechaInicio());
        }

        if (Objects.nonNull(parametros.getFechaFin())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("i.fechaCreacion < :fechaFin ");
            params.put("fechaFin", parametros.getFechaFin());
        }

        if (Objects.nonNull(parametros.getEmpresa())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.sucursal.empresa", "empresa", parametros.getEmpresa(), params);
        }

        if (Objects.nonNull(parametros.getSucursal())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.sucursal", "sucursal", parametros.getSucursal(), params);
        }

        if (Objects.nonNull(parametros.getNoEstatus())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("i.estatus != :noEstatus ");
            params.put("noEstatus", parametros.getNoEstatus());
        }

        if (Objects.nonNull(parametros.getValidador())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.usuarioValidador", "usuarioValidador", parametros.getValidador(), params);
        }

        if (Objects.nonNull(parametros.getCiclo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.ciclo", "ciclo", parametros.getCiclo(), params);
        }

        if (Objects.nonNull(parametros.getCultivo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.cultivo", "cultivo", parametros.getCultivo(), params);
        }

        if (Objects.nonNull(parametros.getEstado())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.estado", "estado", parametros.getEstado(), params);
        }
        return params;
    }

    @Override
    public InscripcionContrato buscaElemento(ParametrosInscripcionContrato parametros) {
        Objects.requireNonNull(parametros, "Los parámetros no pueden ser nulos.");
        List<InscripcionContrato> resultados = busca(parametros);
        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontraron resultados.", 1);
        }

        return resultados.get(0);
    }

}
