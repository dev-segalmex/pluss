/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.busqueda;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionAgrupada;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionResumen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Component
public class BuscadorInscripcionProductorHelper {

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorInscripcionProductor buscadorInscripcionProductor;

    public List<InscripcionAgrupada> busca(ParametrosInscripcionProductor parametros) {
        List<Object[]> registros = buscadorInscripcionProductor.buscaAgrupada(parametros);
        List<InscripcionAgrupada> agrupadas = new ArrayList<>();
        if (Objects.nonNull(registros)) {
            for (Object[] obj : registros) {
                agrupadas.add(generaInscripcionAgrupada(obj));
            }
        }
        return agrupadas;
    }

    private InscripcionAgrupada generaInscripcionAgrupada(Object[] obj) {
        InscripcionAgrupada ia = new InscripcionAgrupada();
        ia.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, (Integer) obj[0]));
        ia.setEstado(buscadorCatalogo.buscaElemento(Estado.class, (Integer) obj[1]));
        ia.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, (Integer) obj[2]));
        ia.setTotal((Math.toIntExact((Long) obj[3])));
        return ia;
    }

    public List<InscripcionResumen> buscaResumen(ParametrosInscripcionProductor parametros) {
        List<Object[]> registros = buscadorInscripcionProductor.buscaResumen(parametros);
        List<InscripcionResumen> resumen = new ArrayList<>();
        if (Objects.nonNull(registros)) {
            for (Object[] obj : registros) {
                resumen.add(generaResumen(obj));
            }
        }
        return resumen;
    }

    private InscripcionResumen generaResumen(Object[] obj) {
        InscripcionResumen ir = new InscripcionResumen();
        ir.setCultivo(String.valueOf(obj[0]));
        ir.setEstatus(String.valueOf(obj[1]));
        ir.setCiclo(String.valueOf(obj[2]));
        ir.setTotal((Math.toIntExact((Long) obj[3])));
        return ir;
    }

}
