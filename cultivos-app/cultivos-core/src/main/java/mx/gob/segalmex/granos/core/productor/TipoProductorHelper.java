/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoProductor;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;
import mx.gob.segalmex.pluss.modelo.productor.TipoProductorEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Component
@Slf4j
public class TipoProductorHelper {

    private static final String HECTAREAS_PROD_MEDIANO = ":hectareas-productor-mediano";

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Autowired
    private BuscadorProductor buscadorProductor;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    public void determinaTipoProductor(InscripcionProductor ip, BigDecimal h, BigDecimal t) {
        //Determinamos si un productor es pequenio o mediano de acuerdo a sus hectareas y toneladas.
        LOGGER.debug("Determinando tipo productor...");
        BigDecimal hectareas = getValorParametro(ip.getCultivo(), ip.getCiclo(), HECTAREAS_PROD_MEDIANO);
        switch (CultivoEnum.getInstance(ip.getCultivo().getClave())) {
            case ARROZ:
            case TRIGO:
                ip.setTipoProductor(buscadorCatalogo.buscaElemento(TipoProductor.class,
                        TipoProductorEnum.MEDIANO.getClave()));
                if (h.compareTo(hectareas) <= 0) {
                    ip.setTipoProductor(buscadorCatalogo.buscaElemento(TipoProductor.class,
                            TipoProductorEnum.PEQUENO.getClave()));
                }
                break;
            default:
                ip.setTipoProductor(buscadorCatalogo.buscaElemento(TipoProductor.class,
                        TipoProductorEnum.MEDIANO.getClave()));
                break;
        }
        if (ip.getTipoProductor().getClave().equals(TipoProductorEnum.PEQUENO.getClave())) {
            determinaProductorNoEmpadronado(ip);
        }
    }

    private BigDecimal getValorParametro(Cultivo c, CicloAgricola ciclo, String clave) {
        BigDecimal valor = buscadorParametro.getValorOpcional(c.getClave() + ":" + ciclo.getClave()
                + clave, BigDecimal.class);
        return Objects.isNull(valor) ? BigDecimal.ZERO : valor;
    }

    private void determinaProductorNoEmpadronado(InscripcionProductor ip) {
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setCultivo(ip.getCultivo());
        parametros.setCurp(ip.getDatosProductor().getCurp());
        List<ProductorCiclo> productores = buscadorProductor.buscaProductorCiclo(parametros);
        LOGGER.info("VALIDANDO REGISTRO DE : " + ip.getCultivo().getNombre());
        if (productores.isEmpty()) {
            //Nuevo productor pequeño no empadronado.
            ip.setTipoProductor(buscadorCatalogo.buscaElemento(TipoProductor.class,
                    TipoProductorEnum.PEQUENO_NO_EMPADRONADO.getClave()));
            return;
        }
        Collections.sort(productores, (p1, p2) -> {
            return new Integer(p1.getCiclo().getOrden()).compareTo(p2.getCiclo().getOrden());
        });
        ProductorCiclo ultimo = productores.get(productores.size() - 1);

        if (!ultimo.getTipoProductor().getClave().equals(ip.getTipoProductor().getClave())) {
            ip.setTipoProductor(buscadorCatalogo.buscaElemento(TipoProductor.class,
                    TipoProductorEnum.PEQUENO_NO_EMPADRONADO.getClave()));
        }
    }
}
