/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura;

import mx.gob.segalmex.granos.core.inscripcion.DatoCapturadoEnum;

/**
 *
 * @author ismael
 */
public enum DatoCapturadoFacturaEnum implements DatoCapturadoEnum {

    NUMERO_CONTRATO("numero-contrato", "Número de contrato"),
    CFDI_PDF("cfdi-pdf", "CFDI (PDF)"),
    COMPROBANTE_RECEPCION_PDF("comprobante-recepcion-pdf", "Comprobantes de recepción (PDF)"),
    RECIBO_ETIQUETA_PDF("recibo-etiqueta-pdf", "Recibos de etiquetas (PDF)"),
    COMPROBANTE_PAGO_PDF("comprobante-pago-pdf", "Comprobante de pago del productor (PDF)"),
    TIPO_CULTIVO("tipo-cultivo", "Tipo de cultivo"),
    CANTIDAD_COMPROBANTE_PAGO("cantidad-comprobante-pago", "Importe total del comprobante de pago"),
    ESTADO_FACTURA("estado-factura", "Estado"),
    FECHA_PAGO("fecha-pago", "Fecha de pago"),
    //Ajuste para calcular el Precio por tonelada real.
    PRECIO_TONELADA_REAL("precio-tonelada-real", "Precio por tonelada real");

    /**
     * La clave del dato capturado.
     */
    private final String clave;

    /**
     * El nombre del datos capturado.
     */
    private final String nombre;

    private DatoCapturadoFacturaEnum(String clave, String nombre) {
        this.clave = clave;
        this.nombre = nombre;
    }

    @Override
    public String getClave() {
        return clave;
    }

    @Override
    public String getNombre() {
        return nombre;
    }

}
