/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.validador;

import java.math.BigDecimal;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorPreRegistroProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PreRegistroProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioPreRegistro;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Se encarga de validar que la superficie de la InscripcionProductor no sea mayor a la superficie
 * (más un 20%) registrada en el PreRegistro.
 *
 * @author oscar
 */
@Component("validadorPreRegistroSuperficie")
@Slf4j
public class ValidadorPreRegistroSuperficie implements ValidadorInscripcion {

    private static final String SUPERFICIE_PREREGISTRO = ":superficie-limite-pre-registro";

    @Autowired
    private BuscadorPreRegistroProductor buscadorPreRegistro;

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Override
    public void valida(InscripcionProductor inscripcion) {

        if (Objects.isNull(inscripcion.getFolioPreRegistro())) {
            LOGGER.warn("El registro productor con curp {} no tiene pre-registro.",
                    inscripcion.getDatosProductor().getCurp());
            return;
        }
        if (inscripcion.getFolioPreRegistro().equals("--")) {
            LOGGER.warn("El registro productor con curp {} no tiene pre-registro.",
                    inscripcion.getDatosProductor().getCurp());
            return;
        }
        if (omiteValidacion(inscripcion.getCultivo(), inscripcion.getCiclo(), SUPERFICIE_PREREGISTRO)) {
            LOGGER.warn("El registro productor con curp {} no verifica limite de superficie.",
                    inscripcion.getDatosProductor().getCurp());
            return;
        }

        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setFolio(inscripcion.getFolioPreRegistro());
        PreRegistroProductor pre = buscadorPreRegistro.buscaElemento(parametros);
        BigDecimal limite = getSuperficieLimite(pre);
        BigDecimal superficie = BigDecimal.ZERO;
        for (PredioProductor pp : inscripcion.getPrediosActivos()) {
            superficie = superficie.add(pp.getSuperficie());
        }
        if (superficie.compareTo(limite) > 0) {
            throw new SegalmexRuntimeException("Error al realizar el registro.",
                    "La superficie excede a la permitida respecto al pre registro.");
        }
    }

    private BigDecimal getSuperficieLimite(PreRegistroProductor pre) {
        BigDecimal spr = BigDecimal.ZERO;
        for (PredioPreRegistro ppp : pre.getPredios()) {
            spr = spr.add(ppp.getSuperficie());
        }
        //Sumamos 20%:
        spr = spr.add(new BigDecimal("0.2").multiply(spr));
        return spr;
    }

    private boolean omiteValidacion(Cultivo c, CicloAgricola seleccionado, String clave) {
        Parametro p = buscadorParametro.buscaElementoOpcional(c.getClave() + ":" + seleccionado.getClave() + clave);
        return Objects.nonNull(p);
    }

}
