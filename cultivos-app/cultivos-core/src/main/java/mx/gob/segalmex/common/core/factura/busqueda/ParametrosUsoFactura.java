/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.busqueda;

import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.factura.EstatusUsoFactura;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.productor.Productor;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;

/**
 *
 * @author ismael
 */
@Getter
@Setter
public class ParametrosUsoFactura {

    private String folio;

    private String uuid;

    private String rfcProductor;

    private String estatusFactura;

    private Productor productor;

    private EstatusUsoFactura estatus;

    private CicloAgricola ciclo;

    private Cultivo cultivo;

    private Integer maximoResultados;

    private ProductorCiclo productorCiclo;
}
