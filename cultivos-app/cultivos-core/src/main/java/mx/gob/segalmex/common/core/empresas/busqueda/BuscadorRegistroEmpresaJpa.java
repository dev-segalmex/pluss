/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.empresas.busqueda;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroEmpresa;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroEmpresaBasico;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroSucursal;
import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ismael
 */
@Repository("buscadorRegistroEmpresa")
@Slf4j
public class BuscadorRegistroEmpresaJpa implements BuscadorRegistroEmpresa {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public RegistroEmpresaBasico buscaElementoBasico(String rfc, String uuid) {
        rfc = StringUtils.trimToNull(rfc);
        uuid = StringUtils.trimToNull(uuid);
        Objects.requireNonNull(rfc, "El RFC no puede ser nulo.");
        Objects.requireNonNull(uuid, "El uuid no puede ser nulo.");

        return entityManager.createQuery("SELECT r FROM RegistroEmpresaBasico r "
                + "WHERE "
                + "r.rfc = :rfc "
                + "AND r.uuid = :uuid ", RegistroEmpresaBasico.class)
                .setParameter("rfc", rfc)
                .setParameter("uuid", uuid)
                .getSingleResult();
    }

    @Override
    public RegistroEmpresa buscaElemento(String rfc, String uuid) {
        rfc = StringUtils.trimToNull(rfc);
        uuid = StringUtils.trimToNull(uuid);
        Objects.requireNonNull(rfc, "El RFC no puede ser nulo.");
        Objects.requireNonNull(uuid, "El uuid no puede ser nulo.");

        ParametrosRegistroEmpresa parametros = new ParametrosRegistroEmpresa();
        parametros.setRfc(rfc);
        parametros.setUuid(uuid);
        return buscaElemento(parametros);
    }

    @Override
    public RegistroEmpresa buscaElemento(ParametrosRegistroEmpresa parametros) {
        List<RegistroEmpresa> resultados = busca(parametros);

        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontró el elemento de catálogo.", 1);
        }

        if (resultados.size() > 1) {
            throw new IncorrectResultSizeDataAccessException("Se encontró más "
                    + "de un elemnto de catálogo.", 1);
        }

        return resultados.get(0);
    }

    @Override
    public List<RegistroEmpresa> busca(ParametrosRegistroEmpresa parametros) {
        StringBuilder sb = new StringBuilder("SELECT re FROM RegistroEmpresa re ");
        Map<String, Object> params = setParametros(parametros, sb);
        LOGGER.debug("Ejecutando el query: {}", sb);
        TypedQuery<RegistroEmpresa> q
                = entityManager.createQuery(sb.toString(), RegistroEmpresa.class);
        QueryUtils.setParametros(q, params);

        long inicio = System.currentTimeMillis();
        List<RegistroEmpresa> registros = q.getResultList();
        LOGGER.debug("Se encontraron {} resultados en {} ms.", registros.size(),
                System.currentTimeMillis() - inicio);

        return registros;
    }

    @Override
    public List<RegistroEmpresaBasico> buscaBasico(ParametrosRegistroEmpresa parametros) {
        StringBuilder sb = new StringBuilder("SELECT re FROM RegistroEmpresaBasico re ");
        Map<String, Object> params = setParametros(parametros, sb);
        LOGGER.debug("Ejecutando el query: {}", sb);
        TypedQuery<RegistroEmpresaBasico> q
                = entityManager.createQuery(sb.toString(), RegistroEmpresaBasico.class);
        QueryUtils.setParametros(q, params);

        long inicio = System.currentTimeMillis();
        List<RegistroEmpresaBasico> registros = q.getResultList();
        LOGGER.debug("Se encontraron {} resultados en {} ms.", registros.size(),
                System.currentTimeMillis() - inicio);

        return registros;
    }

    private Map<String, Object> setParametros(ParametrosRegistroEmpresa parametros, StringBuilder sb) {

        Map<String, Object> params = new HashMap<>();
        boolean first = true;
        if (Objects.nonNull(parametros.getUuid())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "re.uuid", "uuid", parametros.getUuid(), params);
        }

        if (Objects.nonNull(parametros.getCiclo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "re.ciclo", "ciclo", parametros.getCiclo(), params);
        }

        if (Objects.nonNull(parametros.getCultivo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "re.cultivo", "cultivo", parametros.getCultivo(), params);
        }

        if (Objects.nonNull(parametros.getRfc())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "re.rfc", "rfc", parametros.getRfc(), params);
        }

        if (Objects.nonNull(parametros.getId())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "re.id", "id", parametros.getId(), params);
        }

        if (Objects.nonNull(parametros.getEstatus())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "re.estatus", "estatus", parametros.getEstatus(), params);
        }

        if (Objects.nonNull(parametros.getUsuarioValidador())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "re.usuarioValidador", "usuarioValidador", parametros.getUsuarioValidador(), params);
        }

        if (Objects.nonNull(parametros.getFechaCreacion())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("re.fechaCreacion >= :fechaCreacion ");
            params.put("fechaCreacion", parametros.getFechaCreacion());
        }

        if (Objects.nonNull(parametros.getFechaFin())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("re.fechaCreacion < :fechaFin ");
            params.put("fechaFin", parametros.getFechaFin());
        }

        sb.append("ORDER BY re.rfc ");
        return params;
    }

    @Override
    public RegistroEmpresaBasico buscaElementoBasico(ParametrosRegistroEmpresa parametros) {
        List<RegistroEmpresaBasico> resultados = buscaBasico(parametros);

        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontró el elemento de catálogo.", 1);
        }

        if (resultados.size() > 1) {
            throw new IncorrectResultSizeDataAccessException("Se encontró más "
                    + "de un elemnto de catálogo.", 1);
        }

        return resultados.get(0);
    }

    @Override
    public List<RegistroSucursal> busca(List<String> nombres){
        StringBuilder sb = new StringBuilder("SELECT rs FROM RegistroSucursal rs ");
        Map<String, Object> params = new HashMap<>();
        boolean first = true;
        if (Objects.nonNull(nombres)) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("rs.nombre IN (:nombres)");
            params.put("nombres", nombres);
        }
        sb.append("ORDER BY rs.id ");
        LOGGER.debug("Ejecutando el query: {}", sb);
        TypedQuery<RegistroSucursal> q
                = entityManager.createQuery(sb.toString(), RegistroSucursal.class);
        QueryUtils.setParametros(q, params);

        long inicio = System.currentTimeMillis();
        List<RegistroSucursal> sucursales = q.getResultList();
        LOGGER.debug("Se encontraron {} resultados en {} ms.", sucursales.size(),
                System.currentTimeMillis() - inicio);

        return sucursales;
    }
}
