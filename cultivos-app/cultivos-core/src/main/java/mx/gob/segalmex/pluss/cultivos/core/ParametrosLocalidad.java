package mx.gob.segalmex.pluss.cultivos.core;

import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.Municipio;

@Getter
@Setter
public class ParametrosLocalidad {

    private String codigoPostal;

    private Municipio municipio;

    private Estado estado;
}
