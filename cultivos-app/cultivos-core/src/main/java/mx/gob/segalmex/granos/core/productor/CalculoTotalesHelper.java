/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoPrecioEnum;
import mx.gob.segalmex.pluss.modelo.productor.ContratoFirmado;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCicloCultivo;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCicloGrupo;

/**
 *
 * @author ismael
 */
@Slf4j
public class CalculoTotalesHelper {

    private CalculoTotalesHelper() {
    }

    /**
     * Calcula y actualiza los totales generales del productor en el ciclo actual.
     *
     * @param pc el productor ciclo.
     * @param i la inscripción del productor.
     */
    public static void calcula(ProductorCiclo pc, InscripcionProductor i) {
        BigDecimal toneladas = BigDecimal.ZERO;
        BigDecimal hectareas = BigDecimal.ZERO;
        for (PredioProductor pp : i.getPrediosActivos()) {
            toneladas = toneladas.add(pp.getVolumen());
            hectareas = hectareas.add(pp.getSuperficie());
        }
        pc.setToneladasTotales(toneladas);
        pc.setHectareasTotales(hectareas);

        BigDecimal toneladasCobertura = BigDecimal.ZERO;
        BigDecimal toneladasContratadas = BigDecimal.ZERO;
        for (ContratoFirmado cf : i.getContratosActivos()) {
            toneladasContratadas = toneladasContratadas.add(cf.getCantidadContratada());
            switch (TipoPrecioEnum.getInstance(cf.getTipoPrecio().getClave())) {
                case PRECIO_CERRADO_DOLARES:
                case PRECIO_CERRADO_PESOS:
                    toneladasCobertura = toneladasCobertura.add(cf.getCantidadContratada());
                    break;
                default:
                    toneladasCobertura = toneladasCobertura.add(cf.getCantidadContratadaCobertura());
            }

        }
        pc.setToneladasCobertura(toneladasCobertura);
        pc.setToneladasContratadas(toneladasContratadas);
        pc.setPredios(i.getPrediosActivos().size());
    }

    /**
     * Calcula y actualiza los totales por predio del productor.
     *
     * @param pc el productor ciclo.
     * @param nuevos los nuevos predios del productor en el ciclo.
     */
    public static void calculaPredios(ProductorCiclo pc, List<ProductorCicloCultivo> nuevos) {
        LOGGER.info("Calculando los totales de los predios del productor ciclo: {}", pc.getFolio());
        Map<String, ProductorCicloCultivo> registrados = new HashMap<>();
        List<ProductorCicloCultivo> nulos = new ArrayList<>();

        // Limpiamos valores de los predios
        for (ProductorCicloCultivo pcc : pc.getCultivos()) {
            pcc.setHectareas(BigDecimal.ZERO);
            pcc.setPredios(0);
            pcc.setToneladas(BigDecimal.ZERO);
            pcc.setToneladasFacturadas(BigDecimal.ZERO);

            if (Objects.nonNull(pcc.getEstado())) {
                String clave = pcc.getTipoCultivo().getClave() + ":" + pcc.getEstado().getClave();
                registrados.put(clave, pcc);
            } else {
                nulos.add(pcc);
            }
        }

        Iterator<ProductorCicloCultivo> it = nulos.iterator();
        for (ProductorCicloCultivo nuevo : nuevos) {
            String clave = nuevo.getTipoCultivo().getClave() + ":" + nuevo.getEstado().getClave();
            ProductorCicloCultivo registrado = registrados.get(clave);
            if (Objects.isNull(registrado)) {
                LOGGER.debug("No existe información del tipo cultivo y estado: {}", clave);
                if (it.hasNext()) {
                    registrado = it.next();
                    registrado.setEstado(nuevo.getEstado());
                    registrado.setCultivo(nuevo.getCultivo());
                    registrado.setTipoCultivo(nuevo.getTipoCultivo());
                    LOGGER.debug("Actualizando información: {}", registrado.getId());
                } else {
                    pc.getCultivos().add(nuevo);
                    registrado = nuevo;
                    LOGGER.debug("Nueva información.");
                }
                registrados.put(clave, registrado);
            }
            registrado.setHectareas(nuevo.getHectareas());
            registrado.setToneladas(nuevo.getToneladas());
            registrado.setToneladasFacturadas(nuevo.getToneladasFacturadas());
            registrado.setPredios(nuevo.getPredios());
        }
    }

    public static void calculaGrupos(ProductorCiclo pc, List<ProductorCicloGrupo> nuevos) {
        Map<String, ProductorCicloGrupo> registrados = new HashMap<>();
        for (ProductorCicloGrupo pcg : pc.getGrupos()) {
            pcg.setToneladasCobertura(BigDecimal.ZERO);
            pcg.setToneladasContratadas(BigDecimal.ZERO);
            pcg.setToneladasFacturadas(BigDecimal.ZERO);
            pcg.setToneladasTotales(BigDecimal.ZERO);
            registrados.put(pcg.getGrupoEstimulo(), pcg);
        }

        for (ProductorCicloGrupo nuevo : nuevos) {
            ProductorCicloGrupo registrado = registrados.get(nuevo.getGrupoEstimulo());
            if (Objects.isNull(registrado)) {
                registrado = nuevo;
                registrados.put(nuevo.getGrupoEstimulo(), registrado);
                pc.getGrupos().add(registrado);
            } else {
                registrado.setToneladasCobertura(nuevo.getToneladasCobertura());
                registrado.setToneladasContratadas(nuevo.getToneladasContratadas());
                registrado.setToneladasFacturadas(nuevo.getToneladasFacturadas());
                registrado.setToneladasTotales(nuevo.getToneladasTotales());
            }
        }

    }
}
