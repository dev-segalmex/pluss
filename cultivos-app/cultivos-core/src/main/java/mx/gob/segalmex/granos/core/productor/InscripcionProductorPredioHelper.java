/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor;

import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Objects;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component
public class InscripcionProductorPredioHelper {

    public void modifica(InscripcionProductor origen, InscripcionProductor modificado) {
        if (Objects.isNull(origen.getPredios())) {
            origen.setPredios(new ArrayList<>());
        }
        if (origen.getNumeroPredios() == origen.getPrediosActivos().size()) {
            return;
        }

        if (Objects.isNull(modificado.getPredios())) {
            modificado.setPredios(new ArrayList<>());
        }
        if (origen.getNumeroPredios() != origen.getPrediosActivos().size() + modificado.getPredios().size()) {
            throw new SegalmexRuntimeException("Error en el registro del productor.",
                    "El número de predios no coincide con el total especificado.");
        }

        origen.getPredios().addAll(modificado.getPredios());
        int i = 1;
        for (PredioProductor p : origen.getPredios()) {
            p.setInscripcionProductor(origen);
            p.setOrden(i++);
            p.setRendimiento(p.getVolumen().divide(p.getSuperficie(), 5, RoundingMode.HALF_UP));
            p.setRendimientoSolicitado(p.getVolumenSolicitado().divide(p.getSuperficie(), 5, RoundingMode.HALF_UP));
        }
        origen.setNumeroPredios(origen.getPrediosActivos().size());
    }

}
