package mx.gob.segalmex.common.core.datos.catalogos;

import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;

/**
 *
 * @author ismaelhz
 */
public class CargadorDatosTipoCultivo extends AbstractCargadorDatosCatalogo<TipoCultivo> {

    @Override
    public TipoCultivo getInstance(String[] elementos) {
        TipoCultivo tipo = new TipoCultivo();
        tipo.setClave(elementos[0]);
        tipo.setNombre(elementos[1]);
        tipo.setActivo(true);
        tipo.setAbreviatura(elementos[2]);
        tipo.setOrden(Integer.parseInt(elementos[3]));
        tipo.setGrupoEstimulo(elementos[4]);

        return tipo;
    }

}
