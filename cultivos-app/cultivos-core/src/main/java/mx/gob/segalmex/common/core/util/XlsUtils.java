/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Objects;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

/**
 *
 * @author ismael
 */
public class XlsUtils {

    public static void creaEncabezados(Sheet s, String[] encabezados) {
        Workbook wb = s.getWorkbook();

        CellStyle cs = wb.createCellStyle();
        cs.setAlignment(CellStyle.ALIGN_CENTER);
        cs.setFillForegroundColor(IndexedColors.DARK_RED.getIndex());
        cs.setFillPattern(CellStyle.SOLID_FOREGROUND);

        Row r = s.createRow(0);

        int i = 0;
        for (String e : encabezados) {
            Cell c = r.createCell(i);
            c.setCellStyle(cs);
            c.setCellValue(e);
            i++;
        }
    }

    public static byte[] creaArchivo(SXSSFWorkbook wb) {
        try {
            ByteArrayOutputStream os = new ByteArrayOutputStream();
            wb.write(os);
            os.close();
            wb.dispose();
            return os.toByteArray();
        } catch (IOException ouch) {
            return null;
        }
    }

    public static void createCell(Row r, int columna, String valor) {
        Cell c = r.createCell(columna);
        c.setCellValue(valor);
    }

    public static void createCell(Row r, int columna, double valor) {
        Cell c = r.createCell(columna);
        c.setCellValue(valor);
    }

    public static void createCell(Row r, int columna, BigDecimal valor) {
        Cell c = r.createCell(columna);
        if (Objects.nonNull(valor)) {
            c.setCellValue(valor.doubleValue());
        } else {
            c.setCellValue("--");
        }
    }

    public static void createCell(Row r, int columna, Calendar valor, CellStyle cs) {
        Cell c = r.createCell(columna);
        if (Objects.nonNull(valor)) {
            c.setCellStyle(cs);
            c.setCellValue(valor);
        } else {
            c.setCellValue("--");
        }
    }

}
