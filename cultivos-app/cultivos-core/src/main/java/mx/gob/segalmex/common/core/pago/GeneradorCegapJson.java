/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.pago;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.factura.UsoFactura;
import mx.gob.segalmex.pluss.modelo.pago.LayoutEnum;
import mx.gob.segalmex.pluss.modelo.pago.RequerimientoPago;
import mx.gob.segalmex.pluss.modelo.pago.RequerimientoUsoFactura;
import mx.gob.segalmex.pluss.modelo.personas.Persona;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 *
 * @author ismael
 */
@Slf4j
public class GeneradorCegapJson {

    public static final String LAYOUT = "layout";

    public static final String UNIDAD_OPERATIVA = "unidad-operativa";

    public static final String CUENTA_CARGO = "cuenta-cargo";

    public static final String AREA_PG = "area-pg";

    public static final String ENVIA_CORREO = "envia-correo";

    public static final String FECHA_CIERRE = "fecha-cierre";

    public static final String USUARIO = "usuario";

    public static final String PROGRAMA = "programa";

    public static final String CENTRO_ACOPIO = "centro-acopio";

    public JSONObject getJson(RequerimientoPago requerimiento, Map<String, InscripcionProductor> inscripciones) {
        JSONObject json = new JSONObject();
        json.put("LayOUT", requerimiento.getLayout());
        json.put("pk_unidad_operativa", requerimiento.getClaveUnidadOperativa());
        json.put("CuentaCargoUO", requerimiento.getNumeroCuenta());
        json.put("AreaPG", requerimiento.getClaveArea());
        json.put("EnviaCorreo", "SI");
        json.put("PGCIERREFECHA", SegalmexDateUtils.format(requerimiento.getFechaCreacion(), "yyyy-MM-dd"));
        json.put("USUARIO", requerimiento.getClaveUsuario());
        json.put("PGPROGRAMANOMBRE", requerimiento.getPrograma());

        json.put("Recepcion", getRecepcion(requerimiento, inscripciones));
        return json;
    }

    private JSONArray getRecepcion(RequerimientoPago requerimiento, Map<String, InscripcionProductor> inscripciones) {
        JSONArray ja = new JSONArray();
        int i = 0;
        for (RequerimientoUsoFactura ufr : requerimiento.getRequerimientos()) {
            if (i % 200 == 0) {
                LOGGER.info("Generando {} primeros productores en formato JSON", i);
            }
            ja.put(getUso(ufr, inscripciones.get(ufr.getUsoFactura().getProductorCiclo().getFolio())));
            i++;
        }
        return ja;
    }

    private JSONObject getUso(RequerimientoUsoFactura ufr, InscripcionProductor ip) {
        UsoFactura uso = ufr.getUsoFactura();
        JSONObject json = new JSONObject();
        json.put("FL_IDENTIFICACION", ufr.getRequerimiento().getClaveCentroAcopio());
        json.put("FL_MONTO_FACTURA", "0.00");
        json.put("PK_PRODUCTORES", uso.getProductorCiclo().getFolio());

        Persona p = uso.getProductor().getPersona();
        String segundoApellido = StringUtils.trimToEmpty(p.getSegundoApellido());
        json.put("FL_CURP", p.getClave());
        json.put("tb_productores_cfFL_NOMBRE", p.getNombreIcao());
        json.put("FL_APELLIDO_PATERNO", p.getApellidosIcao().substring(0, p.getPrimerApellido().length()));
        json.put("FL_APELLIDO_MATERNO", segundoApellido.length() > 0
                ? p.getApellidosIcao().substring(p.getPrimerApellido().length() + 1) : "");
        json.put("FL_RFC", uso.getProductor().getRfc());

        json.put("tb_recepcion_cfFK_USUARIO", "");
        json.put("FL_PESO_BRUTO", uso.getToneladas().toString());
        json.put("FL_FOLIO_FISCAL", "NA");
        json.put("FL_FECHA_RECEPCION", SegalmexDateUtils.format(uso.getFechaCreacion(),
                "yyyy-MM-dd'T'HH:mm:ss"));
        if (!uso.getClabe().startsWith(uso.getBanco().getClave())) {
            LOGGER.error("Error en la CLABE {} del productor {}", uso.getClabe(), uso.getProductorCiclo().getFolio());
            throw new SegalmexRuntimeException("Error en CLABE",
                    "La CLABE no coincide con la clave del banco: " + uso.getBanco().getClave());
        }
        json.put("FL_CLABE", uso.getClabe());
        json.put("NombreAbrevBanco", uso.getBanco().getNombre());
        json.put("FL_MONTO", uso.getEstimuloTotal());
        String referencia = uso.getFolio();
        if (ufr.getRequerimiento().getLayout().equals(LayoutEnum.ORDENES_PAGO.getClave())) {
            NumberFormat nf = new DecimalFormat("00");
            referencia = referencia + nf.format(uso.getOrdenEstimulo()) + SegalmexDateUtils.format(uso.getFechaCreacion(), "yyMMdd");
        }
        json.put("PK_RECEPCION", referencia);

        List<PredioProductor> predios = getPrediosPorEstado(ip);
        json.put("FL_DESCRIPCION", getDescripcion(predios));

        Estado e = getEstado(predios);
        json.put("FK_ESTADO", Objects.nonNull(e) ? e.getClave() : "");
        json.put("ESTADO", Objects.nonNull(e) ? e.getNombre() : "");
        json.put("PROGRAMA", ufr.getRequerimiento().getPrograma());
        return json;
    }

    private String getDescripcion(List<PredioProductor> predios) {
        for (PredioProductor p : predios) {
            return p.getEstado().getNombre()
                    + "|"
                    + p.getMunicipio().getNombre()
                    + "|"
                    + p.getLocalidad();
        }
        return "Estado|Municipio|Localidad";
    }

    private Estado getEstado(List<PredioProductor> predios) {
        return predios.size() > 0 ? predios.get(0).getEstado() : null;
    }

    /**
     * Obtiene los predios del estado más recurrente.
     *
     * @param i la inscripción del productor.
     * @return los predios del estado más recurrente.
     */
    private List<PredioProductor> getPrediosPorEstado(InscripcionProductor i) {
        Map<String, List<PredioProductor>> map = new HashMap<>();
        for (PredioProductor p : i.getPrediosActivos()) {
            List<PredioProductor> l = map.get(p.getEstado().getClave());
            if (Objects.isNull(l)) {
                l = new ArrayList<>();
                map.put(p.getEstado().getClave(), l);
            }
            l.add(p);
        }
        List<PredioProductor> principal = new ArrayList<>();
        for (List<PredioProductor> predios : map.values()) {
            if (predios.size() > principal.size()) {
                principal = predios;
            }
        }
        return principal;
    }
}
