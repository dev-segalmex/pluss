/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato.busqueda;

import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ismael
 */
@Repository("buscadorDatoCapturado")
public class BuscadorDatoCapturadoJpa implements BuscadorDatoCapturado {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<DatoCapturado> busca(Class clase, String referencia, boolean actual) {
        Objects.requireNonNull(clase, "La clase no puede ser nula.");
        Objects.requireNonNull(referencia, "La referencia no puede ser nula.");
        return entityManager.createQuery("SELECT d FROM DatoCapturado d "
                + "WHERE "
                + "d.clase = :clase "
                + "AND d.referencia = :referencia "
                + (actual ? "AND d.actual = :actual " : "")
                + "ORDER BY d.orden ", DatoCapturado.class)
                .setParameter("clase", clase.getName())
                .setParameter("referencia", referencia)
                .setParameter("actual", true)
                .getResultList();
    }

    @Override
    public List<DatoCapturado> buscaAproximado(Class clase, String referencia,
            String expresion, boolean ordenDesc) {
        Objects.requireNonNull(clase, "La clase no puede ser nula.");
        Objects.requireNonNull(referencia, "La referencia no puede ser nula.");
        Objects.requireNonNull(expresion, "La expresión no puede ser nula.");
        return entityManager.createQuery("SELECT d FROM DatoCapturado d "
                + "WHERE "
                + "d.clase = :clase "
                + "AND d.referencia = :referencia "
                + "AND d.actual = :actual "
                + "AND d.clave like :expresion "
                + "ORDER BY d.orden " + (ordenDesc ? "desc" : ""), DatoCapturado.class)
                .setParameter("clase", clase.getName())
                .setParameter("referencia", referencia)
                .setParameter("actual", true)
                .setParameter("expresion", expresion)
                .getResultList();
    }
}
