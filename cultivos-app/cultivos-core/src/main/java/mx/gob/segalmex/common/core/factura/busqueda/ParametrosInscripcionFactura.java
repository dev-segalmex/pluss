/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.busqueda;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.productor.Productor;
import mx.gob.segalmex.pluss.cultivos.core.inscripcion.busqueda.ParametrosInscripcion;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;

/**
 *
 * @author ismael
 */
@Getter
@Setter
public class ParametrosInscripcionFactura extends ParametrosInscripcion {

    private String rfcEmisor;

    private String rfcReceptor;

    private String uuidTimbreFiscalDigital;

    private String tipoFactura;

    private String estatusFactura;

    private Productor productor;

    private String estatusUsoFactura;

    private Integer maximoResultados;

    private List<EstatusInscripcion> estatusIncluir = new ArrayList<>();

    private ProductorCiclo productorCiclo;

}
