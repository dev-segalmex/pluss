/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;

/**
 *
 * @author ismael
 */
public interface BuscadorDatoCapturado {

    List<DatoCapturado> busca(Class clase, String referencia, boolean actual);

    /**
     * Se encarga de obtener una lista de datos capturados con la clase u
     * referencia especificada pero en especial que contengan en su clave la
     * expresión recibida.
     *
     * @param clase la clase del dato capturado.
     * @param referencia la referencia del dato capturado.
     * @param expresion la expresión con la que debe cumplir la clave del dato
     * capturado.
     * @param ordenDesc
     * @return una lista de los datos capturados que cumplen con los filtros.
     */
    List<DatoCapturado> buscaAproximado(Class clase, String referencia,
            String expresion, boolean ordenDesc);

}
