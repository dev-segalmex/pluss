/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.xls;

import java.math.BigDecimal;
import java.util.List;
import mx.gob.segalmex.common.core.util.XlsUtils;
import mx.gob.segalmex.pluss.modelo.factura.TipoUsoFactura;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.stereotype.Component;

/**
 *
 * @author erikcam
 */
@Component("pagosProductorPlaneacionXls")
public class PagosProductorPlaneacionXls {

    public byte[] exporta(List<Object[]> resultados) {
        SXSSFWorkbook wb = new SXSSFWorkbook(100);
        CreationHelper ch = wb.getCreationHelper();
        Sheet s = wb.createSheet();

        String[] encabezados = new String[]{
            "No.",
            "RFC",
            "CURP",
            "UIID de factura",
            "Importe pagado por SEGALMEX",
            "Cantidad pagada por SEGALMEX",
            "Tipo"
        };
        XlsUtils.creaEncabezados(s, encabezados);

        CellStyle cs = wb.createCellStyle();
        cs.setDataFormat(ch.createDataFormat().getFormat("dd/MM/yyyy HH:mm"));

        int renglon = 1;
        for (Object[] i : resultados) {
                int j = 0;
                Row r = s.createRow(renglon);
                XlsUtils.createCell(r, j++, renglon);
                XlsUtils.createCell(r, j++, (String) i[0]);
                XlsUtils.createCell(r, j++, (String) i[1]);
                XlsUtils.createCell(r, j++, (String) i[3]);
                XlsUtils.createCell(r, j++, (BigDecimal) i[5]);
                XlsUtils.createCell(r, j++, (BigDecimal) i[4]);
                XlsUtils.createCell(r, j++, ((TipoUsoFactura) i[6]).getNombre());
                renglon++;
        }

        for (int i = 0; i < encabezados.length; i++) {
            s.autoSizeColumn(i);
        }

        return XlsUtils.creaArchivo(wb);
    }
}