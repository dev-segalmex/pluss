/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.busqueda;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioDocumento;
import org.springframework.stereotype.Repository;

/**
 *
 * @author oscar
 */
@Repository("buscadorPredioDocumento")
@Slf4j
public class BuscadorPredioDocumentoJpa implements BuscadorPredioDocumento {

    private static final String FORMATO_MONITOREABLE = "--";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<PredioDocumento> busca(InscripcionProductor ip) {
        Objects.requireNonNull(ip, "La inscripción no puede ser nula.");

        return entityManager.createQuery("SELECT pd FROM PredioDocumento pd WHERE "
                + "pd.inscripcionProductor = :inscripcion AND "
                + " pd.activo = :activo"
                + " ORDER BY pd.numero ", PredioDocumento.class)
                .setParameter("inscripcion", ip)
                .setParameter("activo", Boolean.TRUE)
                .getResultList();
    }

    @Override
    public List<PredioDocumento> busca(ParametrosPredioDocumento parametros) {
        StringBuilder sb = new StringBuilder("SELECT pd FROM PredioDocumento pd ");

        Map<String, Object> params = new HashMap<>();
        boolean first = true;

        if (Objects.nonNull(parametros.getInscripcionProductor())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "pd.inscripcionProductor", "inscripcionProductor", parametros.getInscripcionProductor(), params);
        }

        if (Objects.nonNull(parametros.getMonitoreable()) && parametros.getMonitoreable()) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "pd.monitoreable", "monitoreable", FORMATO_MONITOREABLE, params);
        }

        if (Objects.nonNull(parametros.getRfcEmisor())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "pd.rfcEmisor", "rfcEmisor", parametros.getRfcEmisor(), params);
        }

        if (Objects.nonNull(parametros.getRfcReceptor())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "pd.rfcReceptor", "rfcReceptor", parametros.getRfcReceptor(), params);
        }

        if (Objects.nonNull(parametros.getTipo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "pd.tipo", "tipo", parametros.getTipo(), params);
        }

        if (Objects.nonNull(parametros.getNumero())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "pd.numero", "numero", parametros.getNumero(), params);
        }

        if (Objects.nonNull(parametros.getUuidTimbreFiscalDigital())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "pd.uuidTimbreFiscalDigital", "uuid",
                    parametros.getUuidTimbreFiscalDigital(), params);
        }

        if (Objects.nonNull(parametros.getActivo()) && parametros.getActivo()) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "pd.activo", "activo", parametros.getActivo(), params);
        }

        sb.append(" ORDER BY pd.id ");
        LOGGER.debug("Ejecutando el query: {}", sb);
        TypedQuery<PredioDocumento> q
                = entityManager.createQuery(sb.toString(), PredioDocumento.class);
        QueryUtils.setParametros(q, params);

        long inicio = System.currentTimeMillis();
        List<PredioDocumento> resultados = q.getResultList();
        LOGGER.debug("Se encontraron {} resultados en {} ms.", resultados.size(),
                System.currentTimeMillis() - inicio);

        return resultados;
    }

}
