/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato.pdf;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Element;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.ColumnText;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfTemplate;
import com.lowagie.text.pdf.PdfWriter;

/**
 *
 * @author ismael
 */
public class SolicitudGobMxPdfHelper {

    /**
     * Método para generar los encabezados de las solicitudes. El encabezado contempla: La barra que
     * contiene los logotiops de migración a la izquierda, el texto sobre el tipo de forma y el
     * logotipo de gobernación al extremo derecho.
     *
     * @param writer en donde se va a escribir.
     * @param document el objeto que contiene las propiedades del documento.
     */
    public static void setHeaders(PdfWriter writer, Document document) {
        PdfPTable header = PdfHelper.creaHeaderPieza(writer);
        header.setTotalWidth(PageSize.LETTER.getWidth() - document.leftMargin()
                - document.rightMargin());
        header.writeSelectedRows(0, -1, document.leftMargin(),
                document.getPageSize().getHeight() - document.topMargin(),
                writer.getDirectContent());
    }

    /**
     * Método para generar el footer de las solicitudes. El footer contempla: La imagen de SEGOB, la
     * imagen de COFEMER, la imagen del COMAR e información de contacto.
     *
     * @param writer en donde se va a escribir.
     * @param document el objeto que contiene las propiedades del documento.
     * @param totalPags el total de páginas del documento.
     */
    public static void setFooters(PdfWriter writer, Document document, int totalPags) {
        PdfPTable footer = PdfHelper.creaFooterSolicitud(writer);

        footer.setTotalWidth(document.getPageSize().getWidth() - document.leftMargin()
                - document.rightMargin());
        footer.writeSelectedRows(0, -1,
                document.leftMargin(),
                document.bottomMargin() + PtConversion.toPt(1.5f)
                + PtConversion.toPt(0.65f),
                writer.getDirectContent());

        PdfPTable paginacion = PdfHelper.creaPaginacion(document.getPageNumber(), totalPags);
        paginacion.writeSelectedRows(0, -1,
                document.getPageSize().getWidth() - document.rightMargin()
                - PtConversion.toPt(2f),
                document.bottomMargin(),
                writer.getDirectContent());
    }

    /**
     * Método que se encarga de escribir el contenido en el documento PDF.
     *
     * @param document la representación del documento PDF.
     * @param cb el contenido del documento PDF.
     * @param contenido una tabla con el contenido.
     * @param protegido indica si el contenido debe estar protegido.
     * @throws DocumentException si ocurrió un error al escribir el contenido.
     */
    public static void writeColumns(Document document, PdfContentByte cb, PdfPTable contenido, boolean protegido) throws DocumentException {

        ColumnText ct = new ColumnText(cb);
        ct.setAlignment(Element.ALIGN_LEFT);

        float subheaderSpace = PtConversion.toPt(3.4f);
        float bottomSpace = PtConversion.toPt(2.0f);

        PdfTemplate t = cb.createTemplate(PageSize.LETTER.getWidth(), PageSize.LETTER.getHeight());
        //Aqui se tiene que generar el ColumnText e imprimir con respecto a los bordes del documento.
        float leftLowerX = document.leftMargin();
        float leftLowerY = PageSize.LETTER.getHeight() - document.topMargin() - subheaderSpace;
        float rightUperX = (PageSize.LETTER.getWidth() - document.rightMargin());
        float rightUperY = document.bottomMargin() + bottomSpace;

        int status = ColumnText.START_COLUMN;
        ct.addElement(contenido);
        while (ColumnText.hasMoreText(status)) {
            cb.addTemplate(t, 0, 0);
            ct.setSimpleColumn(leftLowerX, leftLowerY, rightUperX, rightUperY);
            status = ct.go();
            document.newPage();
        }

        document.newPage();
    }

}
