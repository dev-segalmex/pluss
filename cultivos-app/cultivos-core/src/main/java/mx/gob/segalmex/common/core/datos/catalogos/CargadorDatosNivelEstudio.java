package mx.gob.segalmex.common.core.datos.catalogos;

import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.NivelEstudio;

/**
 *
 * @author oscar
 */
public class CargadorDatosNivelEstudio extends AbstractCargadorDatosCatalogo<NivelEstudio> {

    @Override
    public NivelEstudio getInstance(String[] elementos) {
        NivelEstudio grupo = new NivelEstudio();
        grupo.setClave(elementos[0]);
        grupo.setNombre(elementos[1]);
        grupo.setActivo(true);
        grupo.setOrden(Integer.parseInt(elementos[2]));
        return grupo;
    }

}
