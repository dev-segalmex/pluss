/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.pago.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.pago.RequerimientoPago;

/**
 *
 * @author ismael
 */
public interface BuscadorRequerimientoPago {

    RequerimientoPago buscaElemento(Integer id);

    RequerimientoPago buscaElemento(String uuid);

    RequerimientoPago buscaElemento(ParametrosRequerimientoPago parametros);

    List<RequerimientoPago> busca(ParametrosRequerimientoPago parametros);
}
