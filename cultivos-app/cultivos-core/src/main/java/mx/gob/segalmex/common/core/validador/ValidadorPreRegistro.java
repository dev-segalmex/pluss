package mx.gob.segalmex.common.core.validador;

import mx.gob.segalmex.pluss.modelo.productor.PreRegistroProductor;

/**
 *
 * @author oscar
 */
public interface ValidadorPreRegistro {

    void valida(PreRegistroProductor preRegistro);

}
