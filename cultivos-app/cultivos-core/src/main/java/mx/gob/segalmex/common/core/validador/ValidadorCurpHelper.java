/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.validador;

import java.util.Calendar;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author oscar
 */
@Slf4j
public class ValidadorCurpHelper {

    private static final String PATTERN_CURP
            = "^([A-Z][AEIOUX][A-Z]{2}\\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\\d])(\\d)$";

    private static final String DICCIONARIO = "0123456789ABCDEFGHIJKLMNÑOPQRSTUVWXYZ";

    /**
     * Se encarga de validar un CURP.
     *
     * @param curp el curp a validar.
     * @param from Texto para poner en el mensaje de error en caso de no ser valido el curp.
     */
    public static void valida(String curp, String from) {
        Objects.requireNonNull(curp, "El curp no puede ser nulo.");
        LOGGER.debug("Validando CURP {}.", curp);

        Pattern pattern = Pattern.compile(PATTERN_CURP);
        Matcher matcher = pattern.matcher(curp.toUpperCase());
        if (!matcher.matches()) {
            throw new SegalmexRuntimeException("Error al validar CURP.",
                    "La CURP " + from + " no es válida.");

        }
        validaDigitoVerificador(curp, from);
    }

    public static Calendar getFechaNacimiento(String curp) {
        String anio = StringUtils.isNumeric(curp.substring(16, 17)) ? "19" : "20";
        String fechaString = curp.substring(8, 10) + "/" + curp.substring(6, 8) + "/" + anio + curp.substring(4, 6);
        return SegalmexDateUtils.parseCalendar(fechaString, "dd/MM/yyyy");
    }

    public static String getSexo(String curp) {
        switch (curp.substring(10, 11).toUpperCase()) {
            case "H":
                return "masculino";
            case "M":
                return "femenino";
            default:
                return "";
        }
    }

    private static void validaDigitoVerificador(String curp, String from) {
        LOGGER.info("Validando dígitos verificadores...");
        // Primeras 17 posiciones del CURP.
        String base = curp.substring(0, curp.length() - 1);
        //El último digito del CURP
        int dv = Integer.parseInt(curp.substring(curp.length() - 1));
        int suma = 0;
        int digito = 0;
        for (int i = 0; i < 17; i++) {
            suma = suma + DICCIONARIO.indexOf(base.charAt(i)) * (18 - i);
        }
        digito = 10 - (suma % 10);
        if (digito == 10) {
            digito = 0;
        }

        if (dv != digito) {
            throw new SegalmexRuntimeException("Error al validar CURP.",
                    "La CURP " + from + " no es válida.");
        }
    }

}
