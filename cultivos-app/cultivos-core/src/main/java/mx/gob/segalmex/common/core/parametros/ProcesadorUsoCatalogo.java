/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.parametros;

import mx.gob.segalmex.pluss.modelo.catalogos.UsoCatalogo;

/**
 * Interfaz con la definición de métodos a realizar sobre ejemplares de
 * {@link UsoCatalogo}.
 *
 * @author oscar
 */
public interface ProcesadorUsoCatalogo {

    void procesa(UsoCatalogo uso);

    void modifica(UsoCatalogo actual, UsoCatalogo actualizado);

    void elimina(UsoCatalogo uso);

}
