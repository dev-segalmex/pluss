/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.xls;

import mx.gob.segalmex.common.core.util.XlsUtils;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.productor.DatosProductor;
import mx.gob.segalmex.pluss.modelo.productor.Domicilio;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component("inscripcionProductorXls")
public class InscripcionProductorXls {

    public class PredioAgrupado {

        private int total = 0;

        private Estado estado;

        private TipoCultivo tipoCultivo;

        private BigDecimal toneladas = BigDecimal.ZERO;

        private BigDecimal hectareas = BigDecimal.ZERO;

        public PredioAgrupado() {
        }

        public int getTotal() {
            return total;
        }

        public void setTotal(int total) {
            this.total = total;
        }

        public Estado getEstado() {
            return estado;
        }

        public void setEstado(Estado estado) {
            this.estado = estado;
        }

        public TipoCultivo getTipoCultivo() {
            return tipoCultivo;
        }

        public void setTipoCultivo(TipoCultivo tipoCultivo) {
            this.tipoCultivo = tipoCultivo;
        }

        public BigDecimal getToneladas() {
            return toneladas;
        }

        public void setToneladas(BigDecimal toneladas) {
            this.toneladas = toneladas;
        }

        public BigDecimal getHectareas() {
            return hectareas;
        }

        public void setHectareas(BigDecimal hectareas) {
            this.hectareas = hectareas;
        }

    }

    public byte[] exporta(List<InscripcionProductor> inscripciones) {
        SXSSFWorkbook wb = new SXSSFWorkbook(100);
        CreationHelper ch = wb.getCreationHelper();
        Sheet s = wb.createSheet();

        String[] encabezados = new String[]{
            "No.",
            "Folio",
            "Fecha de registro",
            "Cultivo",
            "Ciclo agrícola",
            "Nombre de la empresa",
            "RFC de la empresa",
            "Tipo de sucursal",
            "Nombre productor / representante",
            "Primer apellido productor / representante",
            "Segundo apellido productor / representante",
            "CURP",
            "RFC",
            "Teléfono",
            "Correo electrónico",
            "Estado",
            "Municipio",
            "Localidad",
            "Nombre persona moral",
            "Tipo de productor",
            "Tipo",
            "Número de predios",
            "Estado predio",
            "Tipo de cultivo predio",
            "Volumen (t)",
            "Superficie (ha)",
            "Nombre beneficiario",
            "Apellidos beneficiario",
            "Usuario registra",
            "Usuario validador",
            "Fecha validación",
            "Fecha finalización",
            "Estatus",
            "Comentario"
        };
        XlsUtils.creaEncabezados(s, encabezados);

        CellStyle cs = wb.createCellStyle();
        cs.setDataFormat(ch.createDataFormat().getFormat("dd/MM/yyyy HH:mm"));

        int renglon = 1;
        for (InscripcionProductor i : inscripciones) {
            List<PredioAgrupado> predios = getPredioAgrupado(i.getPrediosActivos());
            for (PredioAgrupado pa : predios) {
                int j = 0;
                Row r = s.createRow(renglon);
                XlsUtils.createCell(r, j++, renglon);
                XlsUtils.createCell(r, j++, i.getFolio());
                XlsUtils.createCell(r, j++, i.getFechaCreacion(), cs);
                XlsUtils.createCell(r, j++, i.getCultivo().getNombre());
                XlsUtils.createCell(r, j++, i.getCiclo().getNombre());
                XlsUtils.createCell(r, j++, i.getSucursal().getEmpresa().getNombre());
                XlsUtils.createCell(r, j++, i.getSucursal().getEmpresa().getRfc());
                XlsUtils.createCell(r, j++, i.getSucursal().getTipo().getNombre());
                DatosProductor datos = i.getDatosProductor();
                XlsUtils.createCell(r, j++, datos.getNombre());
                XlsUtils.createCell(r, j++, datos.getPrimerApellido());
                XlsUtils.createCell(r, j++, datos.getSegundoApellido());
                XlsUtils.createCell(r, j++, datos.getCurp());
                XlsUtils.createCell(r, j++, datos.getRfc());
                XlsUtils.createCell(r, j++, i.getNumeroTelefono());
                XlsUtils.createCell(r, j++, i.getCorreoElectronico());
                Domicilio dp = i.getDomicilio();
                XlsUtils.createCell(r, j++, dp.getEstado().getNombre());
                XlsUtils.createCell(r, j++, dp.getMunicipio().getNombre());
                XlsUtils.createCell(r, j++, dp.getLocalidad());
                XlsUtils.createCell(r, j++, StringUtils.defaultIfEmpty(datos.getNombreMoral(), "--"));
                XlsUtils.createCell(r, j++, datos.getTipoPersona().getNombre());
                XlsUtils.createCell(r, j++, i.getTipoProductor().getNombre());
                XlsUtils.createCell(r, j++, pa.getTotal());
                XlsUtils.createCell(r, j++, pa.getEstado().getNombre());
                XlsUtils.createCell(r, j++, pa.getTipoCultivo().getNombre());
                XlsUtils.createCell(r, j++, pa.getToneladas().doubleValue());
                XlsUtils.createCell(r, j++, pa.getHectareas().doubleValue());
                XlsUtils.createCell(r, j++, i.getNombreBeneficiario());
                XlsUtils.createCell(r, j++, i.getApellidosBeneficiario());
                XlsUtils.createCell(r, j++, i.getUsuarioRegistra().getNombre());
                XlsUtils.createCell(r, j++, Objects.nonNull(i.getUsuarioValidador())
                        ? i.getUsuarioValidador().getNombre() : "--");
                XlsUtils.createCell(r, j++, i.getFechaValidacion(), cs);
                XlsUtils.createCell(r, j++, i.getFechaFinalizacion(), cs);
                XlsUtils.createCell(r, j++, i.getEstatus().getNombre());
                XlsUtils.createCell(r, j++, Objects.nonNull(i.getComentarioEstatus())
                        ? i.getComentarioEstatus() : "--");
                renglon++;
            }
        }

        for (int i = 0; i < encabezados.length; i++) {
            s.autoSizeColumn(i);
        }

        return XlsUtils.creaArchivo(wb);
    }

    private List<PredioAgrupado> getPredioAgrupado(List<PredioProductor> predios) {
        List<PredioAgrupado> agrupados = new ArrayList<>();
        if (predios.isEmpty()) {
            PredioAgrupado pa = new PredioAgrupado();
            pa.setEstado(new Estado());
            pa.getEstado().setNombre("--");
            pa.setTipoCultivo(new TipoCultivo());
            pa.getTipoCultivo().setNombre("--");
            agrupados.add(pa);
        } else {
            Map<String, PredioAgrupado> map = new HashMap<>();
            for (PredioProductor p : predios) {
                String clave = p.getEstado().getClave() + "|" + p.getTipoCultivo().getClave();
                PredioAgrupado pa = map.get(clave);
                if (Objects.isNull(pa)) {
                    pa = new PredioAgrupado();
                    pa.setEstado(p.getEstado());
                    pa.setTipoCultivo(p.getTipoCultivo());
                    map.put(clave, pa);
                }
                pa.setTotal(pa.getTotal() + 1);
                pa.setToneladas(pa.getToneladas().add(p.getVolumen()));
                pa.setHectareas(pa.getHectareas().add(p.getSuperficie()));
            }
            agrupados.addAll(map.values());
        }
        return agrupados;
    }

}
