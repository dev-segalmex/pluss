/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.modelo;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 *
 * @author ismael
 */
@Getter
@Setter
@XmlAccessorType(XmlAccessType.NONE)
public class CfdiComplemento {

    @XmlElement(name = "TimbreFiscalDigital", namespace = "http://www.sat.gob.mx/TimbreFiscalDigital")
    private CfdiTimbreFiscalDigital timbreFiscalDigital;

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("timbreFiscalDigital", timbreFiscalDigital)
                .toString();

    }

}
