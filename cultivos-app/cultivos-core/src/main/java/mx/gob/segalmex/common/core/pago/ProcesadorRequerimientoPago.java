/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.pago;

import java.io.IOException;
import java.util.List;
import mx.gob.segalmex.pluss.modelo.factura.EstatusUsoFacturaEnum;
import mx.gob.segalmex.pluss.modelo.factura.UsoFactura;
import mx.gob.segalmex.pluss.modelo.pago.RequerimientoPago;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;

/**
 *
 * @author ismael
 */
public interface ProcesadorRequerimientoPago {

    RequerimientoPago procesa(List<UsoFactura> usos, Usuario usuario, RequerimientoPago rp);

    RequerimientoPago getInstance(List<UsoFactura> usos, Usuario usuario, RequerimientoPago rp);

    /**
     * Actualiza los totales de un requerimiento pago con base en los estatus de uso factura.
     *
     * @param requerimiento el requerimiento a actualizar.
     * @return el requerimiento actualizado.
     */
    RequerimientoPago actualizaTotales(RequerimientoPago requerimiento);

    /**
     * Calcula los totales del requerimiento con base en los UsoFactura.
     *
     * @param requerimiento el requerimiento al que se calculan los totales.
     */
    void calculaTotales(RequerimientoPago requerimiento);

    void envia(RequerimientoPago requerimiento);

    void aprueba(RequerimientoPago requerimiento);

    void marcaPago(RequerimientoPago requerimiento);

    void cancela(RequerimientoPago requerimiento);

    void generaXlsx(RequerimientoPago requerimientoPago) throws IOException;

    /**
     * Se encarga de cancelar el usoFactura del requerimiento recibido.
     * @param requerimiento
     * @param uf
     * @param estatus
     * @param comentario
     */
    void cancela(RequerimientoPago requerimiento, UsoFactura uf, EstatusUsoFacturaEnum estatus, String comentario);

}
