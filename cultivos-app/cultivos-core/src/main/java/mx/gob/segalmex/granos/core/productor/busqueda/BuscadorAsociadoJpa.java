/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.busqueda;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import mx.gob.segalmex.pluss.modelo.productor.Asociado;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ismael
 */
@Repository("buscadorAsociado")
public class BuscadorAsociadoJpa implements BuscadorAsociado {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Asociado buscaElemento(Integer id) {
        return entityManager.createQuery("SELECT a FROM Asociado a WHERE a.id = :id ", Asociado.class)
                .setParameter("id", id)
                .getSingleResult();
    }

}
