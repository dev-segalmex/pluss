/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.folio;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.pluss.modelo.secuencias.Secuencia;
import mx.gob.segalmex.pluss.modelo.secuencias.TipoFolioEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ismael
 */
@Repository("generadorFolioSecuenciaMysql")
@Slf4j
public class GeneradorFolioSecuenciaMysql implements GeneradorFolio {

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional(propagation = Propagation.REQUIRES_NEW)
    @Override
    public String genera(TipoFolioEnum tipo, int padding) {
        LOGGER.debug("Generando secuencia {}.", tipo);
        Secuencia seq = SecuenciaFactory.getInstance(tipo);
        entityManager.persist(seq);
        Integer id = seq.getId();
        return StringUtils.leftPad(id.toString(), padding, "0");
    }

}
