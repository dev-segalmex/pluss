/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.historico;

import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.historico.EntidadHistorico;
import mx.gob.segalmex.pluss.modelo.historico.HistoricoRegistro;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcionEnum;

/**
 *
 * @author ismael
 */
public interface ProcesadorHistoricoRegistro {

    void crea(EntidadHistorico entidad, TipoHistoricoInscripcionEnum tipo, Usuario usuario,
            boolean abierto, String etiquetaGrupo);

    void finaliza(HistoricoRegistro historico, Usuario usuario);

    void crea(EntidadHistorico entidad, TipoHistoricoInscripcionEnum tipo, Usuario usuario,
            boolean abierto, boolean forzarCreacion, String etiquetaGrupo);
}
