/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor;

import java.util.Objects;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import static mx.gob.segalmex.granos.core.inscripcion.DatoCapturadoFactory.getInstance;
import mx.gob.segalmex.granos.core.inscripcion.InscripcionDatoCapturado;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.productor.EstatusPredioProductorEnum;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Component("generadorDatoCapturadoPredioMaiz")
public class GeneradorDatoCapturadoPredioMaiz implements GeneradorDatoCapturadoPredio {

    private static final String PREDIO = "Predios";

    private static final String ARCHIVOS_PREDIO = ":archivos-predio";

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Override
    public void generaDatosCapturadosPredio(InscripcionProductor inscripcion, InscripcionDatoCapturado idc, PredioProductor p) {

        boolean documentosAnexos = aplicaParametro(inscripcion.getCiclo(), ARCHIVOS_PREDIO);

        String suffix = inscripcion.getPredios().size() > 1 ? " (" + (p.getOrden()) + ")" : "";
        int orden = p.getOrden();

        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.FOLIO_PREDIO, p.getFolio(), orden, suffix, PREDIO));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.TIPO_CULTIVO_PREDIO, p.getTipoCultivo(), orden, suffix, PREDIO));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.TIPO_POSESION_PREDIO, p.getTipoPosesion(), orden, suffix, PREDIO));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.DOCUMENTO_POSESION_PREDIO, p.getTipoDocumentoPosesion(), orden, suffix, PREDIO));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.REGIMEN_HIDRICO_PREDIO, p.getRegimenHidrico(), orden, suffix, PREDIO));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.VOLUMEN_PREDIO, String.valueOf(p.getVolumen()), orden, suffix, PREDIO));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.SUPERFICIE_PREDIO, String.valueOf(p.getSuperficie()), orden, suffix, PREDIO));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.RENDIMIENTO_PREDIO, String.valueOf(p.getRendimiento()), orden, suffix, PREDIO));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.ESTADO_PREDIO, p.getEstado(), orden, suffix, PREDIO));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.MUNICIPIO_PREDIO, p.getMunicipio(), orden, suffix, PREDIO));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.LOCALIDAD_PREDIO, p.getLocalidad(), orden, suffix, PREDIO));

        if (Objects.nonNull(p.getLatitud())) {
            idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.LATITUD_PREDIO, String.valueOf(p.getLatitud()), orden, suffix, PREDIO));
            idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.LONGITUD_PREDIO, String.valueOf(p.getLongitud()), orden, suffix, PREDIO));
        } else {
            idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.LATITUD1_PREDIO, String.valueOf(p.getLatitud1()), orden, suffix, PREDIO));
            idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.LONGITUD1_PREDIO, String.valueOf(p.getLongitud1()), orden, suffix, PREDIO));
            idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.LATITUD2_PREDIO, String.valueOf(p.getLatitud2()), orden, suffix, PREDIO));
            idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.LONGITUD2_PREDIO, String.valueOf(p.getLongitud2()), orden, suffix, PREDIO));
            idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.LATITUD3_PREDIO, String.valueOf(p.getLatitud3()), orden, suffix, PREDIO));
            idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.LONGITUD3_PREDIO, String.valueOf(p.getLongitud3()), orden, suffix, PREDIO));
            idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.LATITUD4_PREDIO, String.valueOf(p.getLatitud4()), orden, suffix, PREDIO));
            idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.LONGITUD4_PREDIO, String.valueOf(p.getLongitud4()), orden, suffix, PREDIO));
        }

        DatoCapturado anexo;

        anexo = getInstance(inscripcion, DatoCapturadoProductorEnum.DOCUMENTACION_PREDIO_PDF,
                orden + "_" + DatoCapturadoProductorEnum.DOCUMENTACION_PREDIO_PDF.getClave() + ".pdf", orden, suffix, PREDIO);
        anexo.setTipo("archivo");
        idc.agrega(anexo);
        if (documentosAnexos) {
            //Si el predio cuenta con documentos de Siembra y Riego Xml se crean de acuerdo al numero indicado.
            //Permiso de siembra (XML)
            if (p.getCantidadSiembra() > 0) {
                for (int i = 0; i < p.getCantidadSiembra(); i++) {
                    anexo = getInstance(inscripcion, DatoCapturadoProductorEnum.PERMISO_SIEMBRA_XML,
                            orden + "_" + (i +1) + "_" + DatoCapturadoProductorEnum.PERMISO_SIEMBRA_XML.getClave() + ".xml", orden, suffix, PREDIO);
                    anexo.setTipo("archivo");
                    anexo.setClave(orden + "-" + (i +1) + "_" + DatoCapturadoProductorEnum.PERMISO_SIEMBRA_XML.getClave());
                    idc.agrega(anexo);
                }
            }
            //Pago de riego (XML)
            if (p.getCantidadRiego() > 0) {
                for (int i = 0; i < p.getCantidadRiego(); i++) {
                    anexo = getInstance(inscripcion, DatoCapturadoProductorEnum.PAGO_RIEGO_XML,
                            orden + "_" + (i +1) + "_" + DatoCapturadoProductorEnum.PAGO_RIEGO_XML.getClave() + ".xml", orden, suffix, PREDIO);
                    anexo.setTipo("archivo");
                    anexo.setClave(orden + "-" + (i +1) + "_" + DatoCapturadoProductorEnum.PAGO_RIEGO_XML.getClave());
                    idc.agrega(anexo);
                }
            }
        }

        if (p.getSolicitado().equals(EstatusPredioProductorEnum.SOLICITADO.getClave())) {
            anexo = getInstance(inscripcion, DatoCapturadoProductorEnum.DOCUMENTO_PERMISO_RENDIMIENTO_SUPERIOR_PDF,
                    orden + "_" + DatoCapturadoProductorEnum.DOCUMENTO_PERMISO_RENDIMIENTO_SUPERIOR_PDF.getClave() + ".pdf", orden, suffix, PREDIO);
            anexo.setTipo("archivo");
            idc.agrega(anexo);
        }
    }

    private boolean aplicaParametro(CicloAgricola seleccionado, String clave) {
        Parametro p = buscadorParametro.buscaElementoOpcional(CultivoEnum.MAIZ_COMERCIAL.getClave() + ":" + seleccionado.getClave() + clave);
        return Objects.nonNull(p) && Boolean.valueOf(p.getValor());
    }

}
