/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoPrecioEnum;
import mx.gob.segalmex.pluss.modelo.productor.ContratoFirmado;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCicloGrupo;
import mx.gob.segalmex.pluss.modelo.productor.TipoProductorEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component("ProductorCicloGrupoFactory")
public class ProductorCicloGrupoFactory {

    @Autowired
    private BuscadorParametro buscadorParametro;

    public List<ProductorCicloGrupo> getInstance(InscripcionProductor i, ProductorCiclo pc) {
        Map<String, ProductorCicloGrupo> map = new HashMap<>();
        for (PredioProductor pp : i.getPrediosActivos()) {
            ProductorCicloGrupo pcg = map.get(pp.getTipoCultivo().getGrupoEstimulo());
            if (Objects.isNull(pcg)) {
                pcg = new ProductorCicloGrupo();
                pcg.setGrupoEstimulo(pp.getTipoCultivo().getGrupoEstimulo());
                pcg.setProductorCiclo(pc);
                String tipoProductor = i.getTipoProductor().getClave();
                if (tipoProductor.equals(TipoProductorEnum.PEQUENO_NO_EMPADRONADO.getClave())) {
                    tipoProductor = TipoProductorEnum.PEQUENO.getClave();
                }

                String clave = i.getCultivo().getClave() + ":"
                        + i.getCiclo().getClave() + ":grupo-estimulo-maximo:" + tipoProductor + ":" + pcg.getGrupoEstimulo();
                pcg.setLimite(buscadorParametro.getValorOpcional(clave, BigDecimal.class));
                map.put(pcg.getGrupoEstimulo(), pcg);
            }

            pcg.setToneladasTotales(pcg.getToneladasTotales().add(pp.getVolumen()));
        }

        for (ContratoFirmado cf : i.getContratosActivos()) {
            ProductorCicloGrupo pcg = map.get(cf.getTipoCultivo().getGrupoEstimulo());
            if (Objects.nonNull(pcg)) {
                pcg.setToneladasContratadas(pcg.getToneladasContratadas().add(cf.getCantidadContratada()));
                BigDecimal toneladasCobertura = cf.getCantidadContratadaCobertura();
                switch (TipoPrecioEnum.getInstance(cf.getTipoPrecio().getClave())) {
                    case PRECIO_CERRADO_DOLARES:
                    case PRECIO_CERRADO_PESOS:
                        toneladasCobertura = cf.getCantidadContratada();
                        break;
                }
                pcg.setToneladasCobertura(pcg.getToneladasCobertura().add(toneladasCobertura));
            }
        }
        return new ArrayList<>(map.values());
    }
}
