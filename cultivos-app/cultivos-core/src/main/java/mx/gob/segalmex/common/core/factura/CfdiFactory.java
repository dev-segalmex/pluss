/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura;

import java.util.ArrayList;
import java.util.Objects;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.factura.modelo.CfdiComprobante;
import mx.gob.segalmex.common.core.factura.modelo.CfdiConcepto;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.pluss.modelo.factura.Cfdi;
import mx.gob.segalmex.pluss.modelo.factura.ConceptoCfdi;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author ismael
 */
public class CfdiFactory {

    private CfdiFactory() {
    }

    public static Cfdi getInstance(CfdiComprobante comprobante, boolean invertir) {
        Cfdi cfdi = new Cfdi();
        cfdi.setMoneda(comprobante.getMoneda());
        if (invertir) {
            cfdi.setNombreEmisor(comprobante.getReceptor().getNombre());
            cfdi.setRfcEmisor(comprobante.getReceptor().getRfc());
            cfdi.setNombreReceptor(comprobante.getEmisor().getNombre());
            cfdi.setRfcReceptor(comprobante.getEmisor().getRfc());
            cfdi.setInvertida(Boolean.TRUE);
        } else {
            cfdi.setNombreEmisor(comprobante.getEmisor().getNombre());
            cfdi.setRfcEmisor(comprobante.getEmisor().getRfc());
            cfdi.setNombreReceptor(comprobante.getReceptor().getNombre());
            cfdi.setRfcReceptor(comprobante.getReceptor().getRfc());
        }
        cfdi.setTotal(comprobante.getTotal());
        cfdi.setUuidTimbreFiscalDigital(formatoCfdi(comprobante.getComplemento()
                .getTimbreFiscalDigital().getUuid()));
        cfdi.setConceptos(new ArrayList<>());
        cfdi.setMetodoPago(comprobante.getMetodoPago());
        cfdi.setFecha(SegalmexDateUtils.parseCalendar(comprobante.getFecha(),
                "yyyy-MM-dd'T'HH:mm:ss"));
        cfdi.setFechaTimbrado(SegalmexDateUtils.parseCalendar(comprobante.getComplemento()
                .getTimbreFiscalDigital().getFechaTimbrado(), "yyyy-MM-dd'T'HH:mm:ss"));

        for (CfdiConcepto cc : comprobante.getConceptos()) {
            ConceptoCfdi concepto = new ConceptoCfdi();
            concepto.setCantidad(cc.getCantidad());
            concepto.setCfdi(cfdi);
            concepto.setDescripcion(cc.getDescripcion());
            concepto.setImporte(cc.getImporte());
            concepto.setClaveUnidad(cc.getClaveUnidad());
            concepto.setValorUnitario(cc.getValorUnitario());
            concepto.setClaveProductoServicio(cc.getClaveProductoServicio());
            cfdi.getConceptos().add(concepto);

        }

        if (Objects.isNull(cfdi.getFechaTimbrado())) {
            throw new SegalmexRuntimeException("Error del CFDI.",
                    "La fecha de timbrado es inválida.");
        }

        return cfdi;
    }

    public static String formatoCfdi(String cfdi) {
        String cfdiValido = StringUtils.trimToEmpty(cfdi.replaceAll("-", "").toLowerCase());
        if (cfdiValido.length() == 32) {
            String modificado = "";
            modificado += cfdiValido.substring(0, 8);
            modificado += "-" + cfdiValido.substring(8, 12);
            modificado += "-" + cfdiValido.substring(12, 16);
            modificado += "-" + cfdiValido.substring(16, 20);
            modificado += "-" + cfdiValido.substring(20, cfdiValido.length());
            return modificado;
        } else {
            throw new RuntimeException("El UUID del CFDI no es válido");
        }
    }
}
