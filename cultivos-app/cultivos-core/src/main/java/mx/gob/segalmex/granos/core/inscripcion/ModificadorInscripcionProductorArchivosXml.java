/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.inscripcion;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.xml.bind.JAXBException;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.archivos.BuscadorArchivoRelacionado;
import mx.gob.segalmex.common.core.archivos.ManejadorArchivo;
import mx.gob.segalmex.common.core.factura.CfdiFactory;
import mx.gob.segalmex.common.core.factura.CfdiParser;
import mx.gob.segalmex.common.core.factura.modelo.CfdiComprobante;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.pluss.modelo.archivos.ArchivoRelacionado;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorPredioDocumento;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosPredioDocumento;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.Inscripcion;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioDocumento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Component("modificadorInscripcionProductorArchivosXml")
@Slf4j
public class ModificadorInscripcionProductorArchivosXml implements ModificadorInscripcion {

    private static final String CLAVE_SIEMBRA = "permiso-siembra-xml";

    private static final String CLAVE_RIEGO = "pago-riego-xml";

    private static final String TIPO_SIEMBRA = "siembra";

    private static final String TIPO_RIEGO = "riego";

    private static final String SIN_VALOR = "--";

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private BuscadorPredioDocumento buscadorPredioDocumento;

    @Autowired
    private BuscadorArchivoRelacionado buscadorArchivoRelacionado;

    @Autowired
    private CfdiParser cfdiParser;

    @Autowired
    private ManejadorArchivo manejadorArchivo;

    @Override
    public void modifica(Inscripcion inscripcion, Map<String, DatoCapturado> datos) {

        InscripcionProductor ip = (InscripcionProductor) inscripcion;
        if (!ip.getCultivo().getClave().equals(CultivoEnum.MAIZ_COMERCIAL.getClave())) {
            return;
        }

        List<DatoCapturado> datosSiembra = getArchivoXml(datos, CLAVE_SIEMBRA);
        List<DatoCapturado> datosRiego = getArchivoXml(datos, CLAVE_RIEGO);

        List<PredioDocumento> prediosDocs = new ArrayList<>();
        List<ArchivoRelacionado> archivosXml = new ArrayList<>();
        List<ArchivoRelacionado> archivosRelacionados = buscadorArchivoRelacionado.
                busca(inscripcion.getClass(), inscripcion.getUuid());

        ParametrosPredioDocumento params = new ParametrosPredioDocumento();
        if (!datosSiembra.isEmpty()) {
            String siguiente = "";
            for (DatoCapturado datoSiembra : datosSiembra) {
                LOGGER.info("Archivo siembra por corregir : {}", datoSiembra.getClave());
                String numero = datoSiembra.getClave().split("-")[0];
                if (numero.equals(siguiente)) {
                    LOGGER.info("Siembra - Es el mismo número de predio, ya no se hace nada.");
                    continue;
                }
                siguiente = numero;
                params.setNumero(numero);
                params.setTipo(TIPO_SIEMBRA);
                params.setInscripcionProductor(ip);
                prediosDocs.addAll(buscadorPredioDocumento.busca(params));
                archivosXml.addAll(getArchivos(archivosRelacionados, numero, CLAVE_SIEMBRA));
            }
        }

        if (!datosRiego.isEmpty()) {
            String siguiente = "";
            for (DatoCapturado datoRiego : datosRiego) {
                LOGGER.info("Archivo riego por corregir: {}", datoRiego.getClave());
                String numero = datoRiego.getClave().split("-")[0];
                if (numero.equals(siguiente)) {
                    LOGGER.info("Riego - Es el mismo número de predio, ya no se hace nada.");
                    continue;
                }
                siguiente = numero;
                params.setNumero(numero);
                params.setTipo(TIPO_RIEGO);
                params.setInscripcionProductor(ip);
                prediosDocs.addAll(buscadorPredioDocumento.busca(params));
                archivosXml.addAll(getArchivos(archivosRelacionados, numero, CLAVE_RIEGO));
            }
        }
        cancelaPredioDocumento(prediosDocs);

        for (ArchivoRelacionado ar : archivosXml) {
            PredioDocumento pd = getInstance(ar);
            pd.setInscripcionProductor(ip);
            registroEntidad.guarda(pd);
        }
    }

    private List<DatoCapturado> getArchivoXml(Map<String, DatoCapturado> datos, String claveArchivo) {
        List<DatoCapturado> ds = new ArrayList<>();

        for (Map.Entry<String, DatoCapturado> entry : datos.entrySet()) {
            DatoCapturado dc = entry.getValue();
            if (Objects.nonNull(dc.getCorrecto()) && !dc.getCorrecto() && dc.getClave().contains(claveArchivo)) {
                ds.add(dc);
            }
        }
        Collections.sort(ds, (d1, d2) -> {
            return d1.getClave().compareTo(d2.getClave());
        });
        return ds;
    }

    private void cancelaPredioDocumento(List<PredioDocumento> prediosDocumento) {
        LOGGER.debug("PrediosDocumento por eliminar: {}", prediosDocumento.size());
        for (PredioDocumento pd : prediosDocumento) {
            pd.setActivo(false);
            pd.setMonitoreable(SegalmexDateUtils.format(Calendar.getInstance(), "yyyy-MM-dd"));
            registroEntidad.actualiza(pd);
        }
    }

    private List<ArchivoRelacionado> getArchivos(List<ArchivoRelacionado> archivosRelacionados, String numeroPredio, String tipo) {
        List<ArchivoRelacionado> archivosFiltrados = new ArrayList<>();
        List<ArchivoRelacionado> archivos = new ArrayList<>();
        archivosRelacionados.stream().filter((ar) -> (ar.getTipo().contains(tipo))).forEachOrdered((ar) -> {
            archivosFiltrados.add(ar);
        });
        for (ArchivoRelacionado ar : archivosFiltrados) {
            String numero = ar.getTipo().split("-")[0];
            String nombre = ar.getTipo().split("_")[1];
            if (numero.equals(numeroPredio) && nombre.contains(tipo)) {
                archivos.add(ar);
            }
        }
        LOGGER.info("Archivos relacionados del predio {} y tipo {}, son {}", numeroPredio, tipo, archivos.size());
        return archivos;
    }

    private PredioDocumento getInstance(ArchivoRelacionado ar) {
        PredioDocumento pd = getPredioEmpty(ar);
        try {
            CfdiComprobante cc = cfdiParser.parse(manejadorArchivo.obtenStream(ar.getArchivo()));
            pd.setRfcEmisor(cc.getEmisor().getRfc());
            pd.setRfcReceptor(cc.getReceptor().getRfc());
            pd.setTotal(cc.getTotal());
            if (Objects.nonNull(cc.getComplemento()) && Objects.nonNull(cc.getComplemento().getTimbreFiscalDigital())) {
                pd.setUuidTimbreFiscalDigital(CfdiFactory.formatoCfdi(cc.getComplemento().
                        getTimbreFiscalDigital().getUuid()));
            } else {
                pd.setUuidTimbreFiscalDigital("--");
                pd.setMonitoreable(SegalmexDateUtils.format(Calendar.getInstance(), "yyyy-MM-dd"));
            }
            return pd;
        } catch (JAXBException | IOException ouch) {
            LOGGER.error("Ocurrió un error al convertir el xml, se genera vacío.");
            pd.setMonitoreable(SegalmexDateUtils.format(Calendar.getInstance(), "yyyy-MM-dd"));
            return pd;
        }
    }

    private PredioDocumento getPredioEmpty(ArchivoRelacionado ar) {
        PredioDocumento pd = new PredioDocumento();
        pd.setRfcEmisor(SIN_VALOR);
        pd.setRfcReceptor(SIN_VALOR);
        pd.setUuidTimbreFiscalDigital(SIN_VALOR);
        pd.setTipo(ar.getTipo().contains(CLAVE_RIEGO) ? "riego" : "siembra");
        pd.setNumero(ar.getTipo().split("-")[0]);
        pd.setTotal(BigDecimal.ZERO);
        return pd;
    }
}
