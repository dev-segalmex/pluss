/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor;

import java.util.ArrayList;
import java.util.Objects;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.pluss.modelo.productor.EmpresaAsociada;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component
public class InscripcionProductorEmpresaHelper {

    public void modifica(InscripcionProductor origen, InscripcionProductor modificado) {
        if (Objects.isNull(origen.getEmpresas())) {
            origen.setEmpresas(new ArrayList<>());
        }
        if (origen.getNumeroEmpresas() == origen.getEmpresasActivas().size()) {
            return;
        }

        if (Objects.isNull(modificado.getEmpresas())) {
            modificado.setEmpresas(new ArrayList<>());
        }
        if (origen.getNumeroEmpresas() != origen.getEmpresasActivas().size() + modificado.getEmpresas().size()) {
            throw new SegalmexRuntimeException("Error en el registro del productor.",
                    "El número de empresas asociadas no coincide con el total especificado.");
        }

        origen.getEmpresas().addAll(modificado.getEmpresas());
        int i = 0;
        for (EmpresaAsociada ea : origen.getEmpresas()) {
            ea.setInscripcionProductor(origen);
            ea.setOrden(i++);
        }
        origen.setNumeroEmpresas(origen.getEmpresasActivas().size());
    }

}
