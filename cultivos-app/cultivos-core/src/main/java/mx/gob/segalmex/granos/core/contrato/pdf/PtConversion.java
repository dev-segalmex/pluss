/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato.pdf;

/**
 *
 * @author ismael
 */
public class PtConversion {

    /**
     * Factor de conversion entre centimetros y puntos
     */
    private static final float FACTOR_CM_TO_PT = 28.35f;

    private PtConversion() {
    }

    /**
     * Método auxiliar que se encarga de convertir centímetros en puntos.
     *
     * @param centimetros los centímetros a convertir.
     * @return el equivalente en puntos de los centímetros.
     */
    public static float toPt(float centimetros) {
        return FACTOR_CM_TO_PT * centimetros;
    }

}
