package mx.gob.segalmex.granos.core.productor.busqueda;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;

/**
 *
 * @author oscar
 */
@Getter
@Setter
public class ParametrosInformacionAlerta {

    private Integer id;

    private String folio;

    private String tipo;

    private String valor;

    private CicloAgricola ciclo;

    private Cultivo cultivo;

    private String noFolio;

    private List<String> valores = new ArrayList<>();
}
