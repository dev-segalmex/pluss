/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.validador;

import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Component("validadorInscripcionProductorClabe")
public class ValidadorInscripcionProductorClabe implements ValidadorInscripcion {

    @Override
    public void valida(InscripcionProductor inscripcion) {
        ValidadorClabeHelper.valida(inscripcion.getCuentaBancaria().getClabe());
    }
}
