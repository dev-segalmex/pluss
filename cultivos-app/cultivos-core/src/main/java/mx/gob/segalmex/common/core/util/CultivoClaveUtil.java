/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.util;

import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;

/**
 * Un util para obtener clave corta para cada sistema.
 * @author oscar
 */
public class CultivoClaveUtil {

    /**
     * La clave del sistema es la clave que regresa este método, excepto en maíz
     * ya que en maíz la clave es maiz-comercial y ahí se regresa solo maiz.
     * @param c
     * @return
     */
    public static String getClaveCorta(Cultivo c) {
        switch (CultivoEnum.getInstance(c.getClave())) {
            case ARROZ:
                return CultivoEnum.ARROZ.getClave();
            case MAIZ_COMERCIAL:
                return "maiz";
            case TRIGO:
                return CultivoEnum.TRIGO.getClave();
            default:
                throw new IllegalArgumentException("No existe el cultivo: " + c.getNombre());
        }
    }

}
