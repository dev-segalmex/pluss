/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.util;

import java.util.Map;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component
public class AmbienteUtils {

    public static final String AMBIENTE = "ambiente";

    private Map<String, String> variables;

    public Map<String, String> getVariables() {
        return variables;
    }

    public void setVariables(Map<String, String> variables) {
        this.variables = variables;
    }

    public String getValor(String variable) {
        return variables.get(variable);
    }

}
