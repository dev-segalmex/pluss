/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.parametros;

import java.util.Calendar;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorUsoCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.ParametrosUsoCatalogo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.pluss.modelo.catalogos.UsoCatalogo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author oscar
 */
@Service("procesadorUsoCatalogo")
@Slf4j
public class ProcesadorUsoCatalogoJpa implements ProcesadorUsoCatalogo {

    @Autowired
    private BuscadorUsoCatalogo buscadorUso;

    @Autowired
    private RegistroEntidad registroEntidad;

    @Transactional
    @Override
    public void procesa(UsoCatalogo uso) {
        try {
            verifica(uso);
            existe(uso);
        } catch (ClassNotFoundException ex) {
            LOGGER.error("La clase del uso es inválida.");
            throw new SegalmexRuntimeException("Error registrando el uso.", "La clase es inválida.");
        } catch (EmptyResultDataAccessException ouch) {
            LOGGER.debug("Registrando uso: {}", uso.getUso());
            registroEntidad.guarda(uso);
        }
    }

    @Transactional
    @Override
    public void modifica(UsoCatalogo actual, UsoCatalogo actualizado) {
        LOGGER.debug("Actualizando uso...");

        try {
            verifica(actualizado);
            existe(actualizado);
        } catch (ClassNotFoundException ex) {
            LOGGER.error("La clase del uso es inválida.");
            throw new SegalmexRuntimeException("Error registrando el uso.", "La clase es inválida.");
        } catch (EmptyResultDataAccessException ouch) {
            LOGGER.debug("Actualizando uso...");
            actual.setClase(actualizado.getClase());
            actual.setClave(actualizado.getClave());
            actual.setUso(actualizado.getUso());
            actual.setOrden(actualizado.getOrden());
            registroEntidad.actualiza(actual);
        }
    }

    @Transactional
    @Override
    public void elimina(UsoCatalogo uso) {
        LOGGER.info("Eliminando uso...");
        uso.setEstatusFechaBaja(SegalmexDateUtils.format(Calendar.getInstance(), "yyyy-MM-dd'T'HH:mm:ss"));
        registroEntidad.actualiza(uso);
    }

    private void existe(UsoCatalogo uso) throws ClassNotFoundException, SegalmexRuntimeException {
        ParametrosUsoCatalogo params = new ParametrosUsoCatalogo();
        params.setClase(Class.forName(uso.getClase()));
        params.setClave(uso.getClave());
        params.setUso(uso.getUso());
        params.setOrden(uso.getOrden());
        buscadorUso.buscaElementoUso(params);
        throw new SegalmexRuntimeException("Error.", "El uso ya existe.");
    }

    private void verifica(UsoCatalogo uso) {
        Objects.requireNonNull(uso.getClase(), "La clase no puede ser nula.");
        Objects.requireNonNull(uso.getClave(), "La clave no puede ser nula.");
        Objects.requireNonNull(uso.getUso(), "El uso no puede ser nulo.");
    }
}
