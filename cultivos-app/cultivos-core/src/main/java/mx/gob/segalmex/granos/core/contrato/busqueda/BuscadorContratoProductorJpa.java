/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato.busqueda;

import java.util.Map;
import java.util.List;
import java.util.Objects;
import java.util.HashMap;
import lombok.extern.slf4j.Slf4j;
import javax.persistence.TypedQuery;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import org.springframework.stereotype.Repository;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.inscripcion.ContratoProductor;

/**
 *
 * @author jurgen
 */
@Repository("buscadorContratoProductor")
@Slf4j
public class BuscadorContratoProductorJpa implements BuscadorContratoProductor {

    private static final String ACTIVO = "--";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<ContratoProductor> busca(ParametrosContratoProductor parametros) {
        StringBuilder sb = new StringBuilder("SELECT cp FROM ContratoProductor cp ");

        Map<String, Object> params = new HashMap<>();
        boolean first = true;

        if (Objects.nonNull(parametros.getInscripcion())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "cp.inscripcionContrato", "inscripcionContrato", parametros.getInscripcion(), params);
        }

        if (Objects.nonNull(parametros.getCultivo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "cp.inscripcionContrato.cultivo", "cultivo", parametros.getCultivo(), params);
        }

        if (Objects.nonNull(parametros.getCiclo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "cp.inscripcionContrato.ciclo", "ciclo", parametros.getCiclo(), params);
        }

        if (Objects.nonNull(parametros.getEstatus())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "cp.inscripcionContrato.estatus", "estatus", parametros.getEstatus(), params);
        }

        if (Objects.nonNull(parametros.getCurp())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "cp.curp", "curp", parametros.getCurp(), params);
        }

        if (Objects.nonNull(parametros.getRfc())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "cp.rfc", "rfc", parametros.getRfc(), params);
        }

        first = QueryUtils.agregaWhereAnd(first, sb);
        QueryUtils.and(sb, "cp.estatusFechaBaja", "estatusFechaBaja", ACTIVO, params);

        LOGGER.debug("Ejecutando el query: {}", sb);
        TypedQuery<ContratoProductor> q
                = entityManager.createQuery(sb.toString(), ContratoProductor.class);
        QueryUtils.setParametros(q, params);

        long inicio = System.currentTimeMillis();
        List<ContratoProductor> contratosProductor = q.getResultList();
        LOGGER.debug("Se encontraron {} resultados en {} ms.", contratosProductor.size(),
                System.currentTimeMillis() - inicio);

        return contratosProductor;
    }
}
