/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCicloCultivo;

/**
 *
 * @author ismael
 */
public class ProductorCicloCultivoFactory {

    public static List<ProductorCicloCultivo> getInstance(InscripcionProductor inscripcion, ProductorCiclo pc) {
        Map<String, ProductorCicloCultivo> cultivos = new HashMap<>();
        for (PredioProductor pp : inscripcion.getPrediosActivos()) {
            String clave = pp.getTipoCultivo().getClave() + ":" + pp.getEstado().getClave();
            ProductorCicloCultivo pcc = cultivos.get(clave);
            if (Objects.isNull(pcc)) {
                pcc = getInstance(pc, inscripcion.getCultivo(), pp.getTipoCultivo(), pp.getEstado());
                cultivos.put(clave, pcc);
            }
            pcc.setPredios(pcc.getPredios() + 1);
            pcc.setToneladas(pcc.getToneladas().add(pp.getVolumen()));
            pcc.setHectareas(pcc.getHectareas().add(pp.getSuperficie()));
        }

        return new ArrayList<>(cultivos.values());
    }

    private static ProductorCicloCultivo getInstance(ProductorCiclo pc, Cultivo cultivo, TipoCultivo tc, Estado estado) {
        ProductorCicloCultivo pcc = new ProductorCicloCultivo();
        pcc.setProductorCiclo(pc);
        pcc.setCultivo(cultivo);
        pcc.setTipoCultivo(tc);
        pcc.setEstado(estado);

        return pcc;
    }

}
