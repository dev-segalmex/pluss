/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato.pdf;

import com.lowagie.text.BadElementException;
import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.PageSize;
import com.lowagie.text.Paragraph;
import com.lowagie.text.Phrase;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import java.awt.Color;
import java.io.IOException;
import java.io.InputStream;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author ismael
 */
@Slf4j
public final class PdfHelper {

    private static final double REPEAT = 0.5;

    /**
     * Constructor por omisión.
     */
    private PdfHelper() {

    }

    /**
     * Método auxiliar que se encarga de crear el encabezado definido en los estilos de GobMX. El
     * encabezado incluye una imagen con el logotipo de GobMX y el nombre de la institución.
     *
     * @return una tabla con el encabezado definido en los estilos de GobMX.
     */
    public static PdfPTable creaHeaderPieza(PdfWriter writer) {
        // Se construye un encabezado con el logo y fondo negro.
        // Se usa el tamaño de 2 cm porque de otra forma, no se cumplen las distancias superiores e inferiores.
        PdfPCell gobmxCell = creaImageCell(SvgUtil.getSVGImage("/mx/gob/images/logo_gobmx.svg", writer),
                PdfPCell.ALIGN_LEFT, PdfConstants.BACKGROUND_GOBMX_LOGO, 2.0f);
        gobmxCell.setMinimumHeight(PtConversion.toPt(PdfConstants.CELDA_HEADER_ALTURA_MINIMA_CM));
        gobmxCell.setBorderColor(PdfConstants.BACKGROUND_GOBMX_LOGO);
        gobmxCell.setBorder(Rectangle.BOX);
        // Se usa este padding porque la imagen ya tiene un padding a la izquierda.
        gobmxCell.setPaddingLeft(PtConversion.toPt(0.3f));

        PdfPCell dummy = new PdfPCell();
        dummy.setBackgroundColor(PdfConstants.BACKGROUND_GOBMX_LOGO);
        dummy.setBorderColor(PdfConstants.BACKGROUND_GOBMX_LOGO);
        dummy.setBorder(Rectangle.BOX);
        dummy.setMinimumHeight(PtConversion.toPt(PdfConstants.CELDA_HEADER_ALTURA_MINIMA_CM));
        dummy.setPadding(0);

        // Hasta aquí la parte del encabezado con el logo.
        Paragraph dependencia = new Paragraph(PdfConstants.TITULO_INSTITUCION, PdfConstants.SOBERANA_LIGHT_10_BLACK);
        dependencia.setAlignment(Paragraph.ALIGN_CENTER);

        PdfPCell titleCell = new PdfPCell();
        titleCell.addElement(dependencia);
        titleCell.setBorderColor(PdfConstants.BACKGROUND_GOBMX_DARKGRAY);
        titleCell.setBackgroundColor(PdfConstants.BACKGROUND_GOBMX_GRAY);
        titleCell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        titleCell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
        titleCell.setBorder(Rectangle.BOX);
        titleCell.setBorderWidth(PdfConstants.CELDA_TAMANO_BORDE);
        titleCell.setColspan(2);
        titleCell.setMinimumHeight(PtConversion.toPt(PdfConstants.CELDA_HEADER_ALTURA_MINIMA_CM));
        titleCell.setPaddingBottom(PtConversion.toPt(0.4f));

        // Se genera el header.
        // Tabla de dos columnas para el encabezado con la siguiente forma:
        //
        // *************************************
        // * LOGO *    ESPACIO EN BLANCO       *
        // *************************************
        //
        PdfPTable header = new PdfPTable(2);
        header.getDefaultCell().setFixedHeight(PtConversion.toPt(PdfConstants.CELDA_HEADER_ALTURA_MINIMA_CM));
        header.getDefaultCell().setMinimumHeight(PtConversion.toPt(PdfConstants.CELDA_HEADER_ALTURA_MINIMA_CM));
        header.setLockedWidth(true);
        header.setTotalWidth(PageSize.LETTER.getWidth());

        float tamLogo = 3.5f;
        try {
            header.setWidths(new float[]{PtConversion.toPt(tamLogo), header.getTotalWidth() - PtConversion.toPt(tamLogo)});
        } catch (DocumentException de) {
            LOGGER.error("Ocurrió un error al generar el encabezado.", de);
        }

        header.addCell(gobmxCell);
        header.addCell(dummy);
        header.addCell(titleCell);

        return header;
    }

    /**
     * Método que se encarga de generar una tabla con la información de la paginación del documento.
     *
     * @param actual el número de la página actual.
     * @param total el total de páginas.
     *
     * @return una tabla con la paginación del documento.
     */
    public static PdfPTable creaPaginacion(int actual, int total) {
        StringBuilder sb = new StringBuilder();
        sb.append("Página ");
        sb.append(actual);
        sb.append(" de ");
        sb.append(total);

        Chunk chunk = new Chunk(sb.toString(), PdfConstants.SOBERANA_LIGHT_8_BLACK);

        PdfPCell cell = new PdfPCell();
        cell.setBorder(PdfPCell.NO_BORDER);
        cell.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        cell.addElement(chunk);

        PdfPTable paginacion = new PdfPTable(1);
        paginacion.setTotalWidth(PtConversion.toPt(3f));
        paginacion.addCell(cell);

        return paginacion;
    }
//

    /**
     * Método que se encarga de generar el footer utilizado por las piezas de las solicitudes. El
     * footer contiene los logotipos de las instancias de gobierno y la información de contacto.
     *
     * @param writer el <code>PdfWriter</code> con la representación del PDF.
     *
     * @return una tabla con el footer de las solicitudes.
     */
    public static PdfPTable creaFooterSolicitud(PdfWriter writer) {

        float minHeight = 0.1f;
        float maxHeight = PdfConstants.CELDA_HEADER_ALTURA_MINIMA_CM;
        float logosWidthCM = 9.6f;
        int numberCols = 4;

        PdfPCell breakLine = new PdfPCell();
        breakLine.setColspan(numberCols);
        breakLine.setBackgroundColor(PdfConstants.BACKGROUND_GOBMX_LOGO);
        breakLine.setFixedHeight(PtConversion.toPt(minHeight));
        breakLine.setBorder(Rectangle.NO_BORDER);

        PdfPCell logos = creaImageCell(
                SvgUtil.getSVGImage("/mx/gob/images/gob_sader_segalmex.svg", writer),
                PdfPCell.ALIGN_LEFT,
                PdfConstants.BACKGROUND_GOBMX_GRAY,
                logosWidthCM);

        PdfPCell celdaVacia = new PdfPCell();
        celdaVacia.setBorder(PdfPCell.NO_BORDER);
        celdaVacia.setBackgroundColor(PdfConstants.BACKGROUND_GOBMX_GRAY);
        celdaVacia.setColspan(1);

        PdfPCell contacto = creaCeldaBloqueTexto(PdfConstants.BACKGROUND_GOBMX_GRAY, maxHeight,
                "Contacto:", new String[]{
                    "Av. Insurgentes Sur 3483",
                    "Villa Olímpica",
                    "Tlalpan",
                    "Ciudad de México, CDMX.",
                    "Tel."});
        contacto.setFixedHeight(maxHeight);
        contacto.setMinimumHeight(maxHeight);
        contacto.setPaddingRight(PtConversion.toPt(0.15f));

        PdfPTable footer = new PdfPTable(numberCols);
        footer.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        footer.getDefaultCell().setBackgroundColor(PdfConstants.BACKGROUND_GOBMX_FOOTER);
        footer.getDefaultCell().setMinimumHeight(PtConversion.toPt(minHeight));
        footer.getDefaultCell().setFixedHeight(PtConversion.toPt(maxHeight));

        float space = PtConversion.toPt(0.7f);
        float ajuste = PtConversion.toPt(5.0f);
        float contactoCell = writer.getPageSize().getWidth()
                - (PtConversion.toPt(3.0f))
                - ajuste
                - PtConversion.toPt(logosWidthCM);

        try {
            footer.setWidths(new float[]{
                space, PtConversion.toPt(logosWidthCM), ajuste, contactoCell
            });
        } catch (DocumentException ouch) {
            LOGGER.error("Error al definir los anchos relativos de la columna.", ouch);
        }

        footer.addCell(breakLine);
        footer.addCell(celdaVacia);
        footer.addCell(logos);
        footer.addCell(celdaVacia);
        footer.addCell(contacto);
        footer.addCell(celdaVacia);

        return footer;
    }

    /**
     * Genera la marca de agua para los pdf, esta puede ser una imagen centrada en el documento o
     * repetida dentro del documento
     *
     * @param writer el <code>PdfWriter</code> donde se insertará la marca de agua.
     * @param document el <code>Document</code> del que se obtendrán los valores para calcular las
     * posiciones.
     * @param imagen El path de donde se obtendra la marca de agua
     * @param repetir true si queremos que la imagen se repita o false si la queremos centrada
     * @throws DocumentException problemas al generar la marca de agua
     */
    public static void addWatermark(PdfWriter writer, Document document,
            String imagen, boolean repetir) throws DocumentException {
        try {
            InputStream is = PdfHelper.class.getResourceAsStream(imagen);
            Image image = Image.getInstance(IOUtils.toByteArray(is));
            if (repetir) {
                marcaRepetida(image, writer, document);
            } else {
                marcaCentrada(image, writer);
            }
        } catch (BadElementException bee) {
            LOGGER.warn("No se pudo obtener la marca de agua");
            throw new DocumentException("No se pudo generar la marca de agua");
        } catch (IOException ioe) {
            LOGGER.warn("No se pudo obtener la marca de agua");
            throw new DocumentException("No se pudo generar la marca de agua");
        }
    }

    /**
     * Genera la marca de agua centrada en el documento
     *
     * @param imagen la imagen que vamos a poner.
     * @param writer el <code>PdfWriter</code> donde se insertará la marca de agua.
     * @param document el <code>Document</code> del que se obtendrán los valores para calcular las
     * posiciones.
     * @throws DocumentException si todo sale mal.
     */
    private static void marcaCentrada(Image imagen, PdfWriter writer)
            throws DocumentException {
        float docTop = writer.getPageSize().getHeight();
        float docRight = writer.getPageSize().getWidth();
        float w = imagen.getWidth();
        float h = imagen.getHeight();

        float posX = (docRight - w) / 2;
        imagen.setAbsolutePosition(posX, ((docTop - h) / 2));
        PdfContentByte content = writer.getDirectContent();
        content.addImage(imagen);
    }

    /**
     * Genera la marca de agua repetida dentro del documento.
     *
     * @param imagen la imagen para calcular posiciones.
     * @param writer el <code>PdfWriter</code> donde se insertará la marca de agua.
     * @param document el <code>Document</code> del que se obtendrán los valores para calcular las
     * posiciones.
     * @throws DocumentException si todo sale mal.
     */
    private static void marcaRepetida(Image imagen, PdfWriter writer, Document document)
            throws DocumentException {
        float w = imagen.getWidth();
        float h = imagen.getHeight();
        Rectangle pagina = document.getPageSize();
        int repeatX = (int) ((pagina.getWidth() / w) + REPEAT);
        int repeatY = (int) ((pagina.getHeight() / h) + REPEAT);
        float posicionX = 0;
        float posicionY = 0;
        PdfContentByte content = writer.getDirectContent();
        for (int x = 0; x < repeatX; x++) {
            posicionY = pagina.getHeight();
            for (int y = 0; y < repeatY; y++) {
                Image imagenTemp = Image.getInstance(imagen);
                imagenTemp.setAbsolutePosition(posicionX, posicionY);
                content.addImage(imagenTemp);
                posicionY = posicionY - h;
            }
            posicionX = posicionX + w;
        }
    }

    /**
     * Método auxiliar que agrega una marca de agua en los archivos generados. La marca de agua sólo
     * se despliega en los archivos generados que no se encuentren en el tipo de ambiente de
     * producción.
     *
     * @param tipoAmbiente el tipo de ambiente en que se produce el PDF.
     * @param writer el objeto que se encarga de escribir en el PDF.
     * @param document la representación del documento en PDF.
     */
    public static void setWatermark(String tipoAmbiente, PdfWriter writer, Document document) {
        try {
            if (!tipoAmbiente.equals("prod")) {
                PdfHelper.addWatermark(writer, document, "/mx/gob/images/marca-agua.png", false);
            }
        } catch (DocumentException ouch) {
            LOGGER.error("Error al generar la marca de agua: " + ouch);
        }
    }

    /**
     * Crea una <code>PdfPCell</code> de longitud <code>colspan</code> con el texto indicado con la
     * fuente proporcionada. El texto indicado se encuentra alineado verticalmente al centro.
     *
     * @param texto el texto a mostrarse en la celda.
     * @param colspan la longitud en columnas de la celda.
     * @param font la fuente a utilizar en el texto.
     * @return una <code>PdfPCell</code> con la longitud en columnas indicada, con el texto
     * proporcionado en la fuente establecida.
     */
    public static PdfPCell creaCelda(String texto, Integer colspan, Font font) {
        PdfPCell cell = new PdfPCell(new Paragraph(texto, font));
        cell.setColspan(colspan != null ? colspan : 1);
        cell.setBorder(0);
        cell.setPaddingBottom(1);
        cell.setPaddingTop(0);
        cell.setMinimumHeight(6);
        cell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
        return cell;
    }

    /**
     * Crea una <code>PdfPCell</code> de longitud <code>colspan</code> con el texto indicado con la
     * fuente proporcionada y con color de fondo establecido.
     *
     * @param texto el texto a mostrarse en la celda.
     * @param colspan la longitud en columnas de la celda.
     * @param font la fuente a utilizar en el texto.
     * @param background el color de fondo que debe tener la celda.
     * @return una <code>PdfPCell</code> con la longitud en columnas indicada, con el texto
     * proporcionado en la fuente establecida y con color de fondo indicado.
     */
    public static PdfPCell creaCeldaBackground(String texto, Integer colspan,
            Font font, Color background) {
        PdfPCell cell = creaCelda(texto, colspan, font);
        cell.setBackgroundColor(background);
        cell.setPaddingBottom(3);
        cell.setPaddingTop(3);
        cell.setBorder(0);

        return cell;
    }

    /**
     * Crea una <code>PdfPCell</code> de longitud <code>colspan</code> con el texto indicado con la
     * fuente proporcionada y con color de fondo establecido. El texto establecido se encuentra
     * alineado horizontalmente al centro.
     *
     * @param texto el texto a mostrarse en la celda.
     * @param colspan la longitud en columnas de la celda.
     * @param font la fuente a utilizar en el texto.
     * @param background el color de fondo que debe tener la celda.
     * @return una <code>PdfPCell</code> con la longitud en columnas indicada, con el texto
     * proporcionado en la fuente establecida y con color de fondo indicado.
     */
    public static PdfPCell creaCeldaBackgroundCenter(String texto, Integer colspan,
            Font font, Color background) {
        PdfPCell cell = creaCeldaBackground(texto, colspan, font, background);
        cell.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        return cell;
    }

    /**
     * Crea una celda de texto con el tipo de fuente <code>SoberanaSans Light</code> en color negro
     * y tamaño 10. La celda tiene una altura de 0.5 cm, sin borde y color de fondo
     * <code>PdfConstants.BACKGROUND_GOBMX_WHITE</code>.
     *
     * @param texto el texto contenido en la celda.
     * @param colspan el tamaño de columnas que ocupa la celda.
     *
     * @return una celda con el texto y las características previamente indicadas.
     */
    public static PdfPCell creaCeldaTextoGobMx(String texto, int colspan) {

        Paragraph paragraph = new Paragraph(texto, PdfConstants.SOBERANA_LIGHT_8_BLACK);
        paragraph.setAlignment(Paragraph.ALIGN_CENTER);

        PdfPCell celda = new PdfPCell();
        celda.addElement(paragraph);
        celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
        celda.setBorderColor(PdfConstants.BACKGROUND_GOBMX_DARKGRAY);
        celda.setBorder(Rectangle.BOX);
        celda.setBorderWidth(1f);
        celda.setMinimumHeight(PtConversion.toPt(PdfConstants.CELDA_ALTURA_MINIMA_CM));
        celda.setPaddingBottom(PtConversion.toPt(PdfConstants.CELDA_PADDING_CM));
        celda.setColspan(colspan);

        return celda;
    }

    /**
     * Crea una celda para un encabezado con el tipo de fuente <code>SoberanaSans Light</code> en
     * color negro y tamaño 10. La celda tiene como color de fondo
     * <code>PdfConstants.BACKGROUND_GOBMX_GRAY</code>.
     *
     * @param texto el texto del encabezado.
     *
     * @return una celda con el texto y las características previamente indicadas.
     */
    public static PdfPCell creaCeldaEncabezadoGobMx(String texto) {
        Paragraph paragraph = new Paragraph(texto, PdfConstants.SOBERANA_LIGHT_8_BLACK);
        paragraph.setAlignment(Paragraph.ALIGN_CENTER);

        PdfPCell celda = new PdfPCell();
        celda.addElement(paragraph);
        celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
        celda.setBorder(Rectangle.BOX);
        celda.setColspan(1);
        celda.setMinimumHeight(PtConversion.toPt(PdfConstants.CELDA_ALTURA_MINIMA_CM));
        celda.setBackgroundColor(PdfConstants.BACKGROUND_GOBMX_GRAY);
        celda.setBorderColor(PdfConstants.BACKGROUND_GOBMX_DARKGRAY);
        celda.setBorderWidth(PdfConstants.CELDA_TAMANO_BORDE);
        celda.setPadding(PtConversion.toPt(PdfConstants.CELDA_PADDING_CM));

        // Lo siguiente es para alinear el texto de forma verticar al centro.
        celda.setPaddingTop(PtConversion.toPt(0));
        celda.setPaddingBottom(PtConversion.toPt(0.18f));

        return celda;
    }

    /**
     * Crea una celda para un encabezado con el tipo de fuente <code>SoberanaSans Light</code> en
     * color negro y tamaño 10.
     *
     * @param texto el texto del encabezado.
     * @param colspan el tamaño de columnas que ocupa la celda.
     * @param height la altura en puntos que debe de tener la celda.
     * @param color el color de fondo de la celda.
     *
     * @return una celda con el texto y las características previamente indicadas.
     */
    public static PdfPCell creaCeldaEncabezadoGobMx(String texto, int colspan, float height, Color color) {
        Paragraph paragraph = new Paragraph(texto, PdfConstants.SOBERANA_LIGHT_10_BLACK);
        paragraph.setAlignment(Paragraph.ALIGN_CENTER);

        PdfPCell celda = new PdfPCell();
        celda.addElement(paragraph);
        celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
        celda.setBorder(Rectangle.NO_BORDER);
        celda.setColspan(colspan);
        celda.setBackgroundColor(color);
        celda.setPadding(PtConversion.toPt(0.1f));
        celda.setPaddingBottom(PtConversion.toPt(0.3f));
        celda.setMinimumHeight(height);
        return celda;
    }

    /**
     *
     * @param texto
     * @param background
     * @param border
     * @return
     */
    public static PdfPCell creaCeldaAviso(String texto, Color background, Color border) {
        PdfPCell celda = creaCeldaBackgroundCenter(texto, 1, PdfConstants.SOBERANA_LIGHT_8_BLACK, background);
        celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda.setPadding(PtConversion.toPt(0.3f));

        if (border != null) {
            celda.setBorderColor(border);
            celda.setBorder(Rectangle.BOX);
            celda.setBorderWidth(1);
        }

        return celda;
    }

    /**
     * Método auxiliar que construye una celda con una imagen, alineación y color de fondo
     * indicados.
     *
     * @param path la ruta de la imagen.
     * @param alignment la alineación de la celda.
     * @param background el color de fondo de la celda.
     *
     * @return una celda con una imagen, alineación y color de fondo indicados.
     */
    public static PdfPCell creaImageCell(String path, int alignment, Color background) {
        return creaImageCell(path, alignment, background, null);
    }

    /**
     * Método auxiliar que construye una celda con una imagen, alineación horizontal, color de fondo
     * y escala la imagen con el tamaño indicado.
     *
     * @param path la ruta de la imagen.
     * @param alignment la alineación horizontal de la celda.
     * @param background el color de fondo de la celda.
     * @param widthCm el ancho de la imagen en centímetros. En caso de ser nulo, utiliza el tamaño
     * por omisión de la imagen.
     *
     * @return una celda con una imagen, alineación, color de fondo y ancho de la imagen en
     * centímetros.
     */
    public static PdfPCell creaImageCell(String path, int alignment, Color background, Float widthCm) {
        try {
            InputStream image = PdfHelper.class.getResourceAsStream(path);
            Image logo = Image.getInstance(IOUtils.toByteArray(image));

            return creaImageCell(logo, alignment, background, widthCm);
        } catch (BadElementException bee) {
            LOGGER.error("Elemento inválido para el elemento especificado.", bee);
            throw new IllegalStateException(bee);
        } catch (IOException ioe) {
            LOGGER.error("Error al abrir el elemento.", ioe);
            throw new IllegalStateException(ioe);
        }
    }

    /**
     * Método auxiliar que construye una celda con una imagen, alineación, color de fondo y escala
     * la imagen con el tamaño indicado.
     *
     * @param image la imagen a utilizar.
     * @param alignment la alineación de la celda.
     * @param background el color de fondo de la celda.
     * @param widthCm el ancho de la imagen en centímetros. En caso de ser nulo, utiliza el tamaño
     * por omisión de la imagen.
     *
     * @return una celda con una imagen, alineación, color de fondo y ancho de la imagen en
     * centímetros.
     */
    public static PdfPCell creaImageCell(Image image, int alignment, Color background, Float widthCm) {
        PdfPCell imageCell = new PdfPCell();
        imageCell.setBackgroundColor(background);
        imageCell.setHorizontalAlignment(alignment);
        imageCell.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
        imageCell.setBorder(Rectangle.NO_BORDER);

        if (widthCm != null) {
            image.scaleAbsoluteWidth(PtConversion.toPt(widthCm.floatValue()));
        }

        imageCell.addElement(image);

        return imageCell;
    }

    /**
     * Construye una celda con un bloque de texto. Cada elemento estará separado por un salto de
     * línea.
     *
     * @param background el color de fondo de la celda.
     * @param heightCM la altura de la celda.
     * @param title el título dentro de la celda.
     * @param info la información a mostrar.
     *
     * @return una celda con un bloque de texto.
     */
    public static PdfPCell creaCeldaBloqueTexto(Color background, float heightCM, String title, String[] info) {
        Phrase bloque = new Phrase(StringUtils.EMPTY);

        if (StringUtils.trimToNull(title) != null) {
            bloque.add(new Chunk(title, PdfConstants.SOBERANA_LIGHT_8_BLACK));
            bloque.add(Chunk.NEWLINE);
        }

        if (info != null) {
            if (info.length > 1) {
                for (int i = 0; i < info.length - 1; i++) {
                    bloque.add(new Chunk(info[i], PdfConstants.SOBERANA_LIGHT_8_BLACK));
                    bloque.add(Chunk.NEWLINE);
                }
            }

            bloque.add(new Chunk(info[info.length - 1], PdfConstants.SOBERANA_LIGHT_8_BLACK));
        }

        PdfPCell cellBloque = new PdfPCell(bloque);
        cellBloque.setBorder(Rectangle.NO_BORDER);
        cellBloque.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        cellBloque.setBackgroundColor(background);
        cellBloque.setFixedHeight(PtConversion.toPt(heightCM));
        cellBloque.setPadding(PtConversion.toPt(PdfConstants.CELDA_PADDING_CM));

        return cellBloque;
    }

    /**
     * Genera una celda vacía utilizada como separador.
     *
     * @param colspan el colspan de la celda.
     *
     * @return una celda sin borde, con el tamaño de altura mínima de las celdas y con el colspan
     * indicado.
     */
    public static PdfPCell generaCeldaVacia(int colspan) {
        PdfPCell vacio = new PdfPCell();
        vacio.setBorder(Rectangle.NO_BORDER);
        vacio.setColspan(colspan);
        vacio.setMinimumHeight(PtConversion.toPt(0.25f));
        vacio.setPadding(0);

        return vacio;
    }

}
