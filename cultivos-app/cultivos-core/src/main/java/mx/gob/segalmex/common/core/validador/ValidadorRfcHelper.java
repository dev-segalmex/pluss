/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.validador;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author oscar
 */
@Slf4j
public class ValidadorRfcHelper {

    private static final String[] RFCS_GENERICOS = {"XAXX010101000", "XEXX010101000"};

    public static final List<String> GENERICOS = Arrays.asList(RFCS_GENERICOS);

    private static final String PATTERN_RFC
            = "^([A-ZÑ&]{3,4})\\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])([A-Z0-9]{2})([A0-9])$";

    private static final String DICCIONARIO = "0123456789ABCDEFGHIJKLMN&OPQRSTUVWXYZ Ñ";

    public static void valida(String rfc, List<String> excepciones, String from) {
        Objects.requireNonNull(rfc, "El rfc no puede ser nulo.");
        rfc = rfc.toUpperCase();
        LOGGER.info("Validando RFC {}.", rfc);
        if (Objects.nonNull(excepciones) && excepciones.contains(rfc)) {
            LOGGER.info("El RFC {} se encuentra en las excepciones.", rfc);
            return;
        }
        Pattern pattern = Pattern.compile(PATTERN_RFC);
        Matcher matcher = pattern.matcher(rfc);
        if (!matcher.matches()) {
            throw new SegalmexRuntimeException("Error al validar RFC.",
                    "El RFC " + from + " no es válido.");
        }
        String rfcSinDigito = rfc.substring(0, rfc.length() - 1);
        String dv = rfc.substring(rfc.length() - 1);
        dv = dv.equals("A") ? "10" : dv;
        if (!StringUtils.isNumeric(dv)) {
            throw new SegalmexRuntimeException("Error al válidar RFC.",
                    "El RFC " + from + " no es válido.");
        }
        int digitoVerificador = Integer.parseInt(dv);
        int len = rfcSinDigito.length();
        int indice = len + 1;
        int suma = 0;
        int digitoEsperado = 0;
        if (len == 12) {
            suma = 0;
        } else {
            suma = 481;  //Ajuste para persona moral
        }

        for (int i = 0; i < len; i++) {
            suma += DICCIONARIO.indexOf(rfcSinDigito.charAt(i)) * (indice - i);
        }
        digitoEsperado = 11 - suma % 11;

        if (digitoEsperado == 11) {
            digitoEsperado = 0;
        }

        if (digitoVerificador != digitoEsperado) {
            throw new SegalmexRuntimeException("Error al validar RFC.",
                    "El RFC " + from + " no es válido.");
        }

    }

}
