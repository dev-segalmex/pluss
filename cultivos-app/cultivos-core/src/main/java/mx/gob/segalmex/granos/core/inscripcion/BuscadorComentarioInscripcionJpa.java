/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.inscripcion;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import mx.gob.segalmex.pluss.modelo.inscripcion.ComentarioInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.Inscripcion;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ismael
 */
@Repository("buscadorComentarioInscripcion")
public class BuscadorComentarioInscripcionJpa implements BuscadorComentarioInscripcion {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<ComentarioInscripcion> busca(Inscripcion inscripcion) {
        return entityManager.createQuery("SELECT c FROM ComentarioInscripcion c "
                + "WHERE c.clase = :clase "
                + "AND c.referencia = :referencia "
                + "AND c.activo = :activo "
                + "ORDER BY c.id ", ComentarioInscripcion.class)
                .setParameter("clase", inscripcion.getClass().getName())
                .setParameter("referencia", inscripcion.getUuid())
                .setParameter("activo", Boolean.TRUE)
                .getResultList();
    }

}
