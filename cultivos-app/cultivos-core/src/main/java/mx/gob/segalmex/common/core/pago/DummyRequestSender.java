/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.pago;

import org.apache.commons.lang3.RandomStringUtils;
import org.json.JSONObject;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component
@Profile("!prod")
public class DummyRequestSender implements RequestSender {

    @Override
    public String send(String url, String json) {
        JSONObject jo = new JSONObject();
        jo.put("Mensajes", "0," + RandomStringUtils.randomNumeric(5));
        return jo.toString();
    }

}
