/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.inscripcion;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.validador.ValidadorCurpHelper;
import mx.gob.segalmex.common.core.validador.ValidadorInscripcionProductorEnBeneficiario;
import mx.gob.segalmex.pluss.modelo.personas.Sexo;
import mx.gob.segalmex.granos.core.productor.DatoCapturadoProductorEnum;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.Inscripcion;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component("modificadorInscripcionProductorCurp")
@Slf4j
public class ModificadorInscripcionProductorCurp implements ModificadorInscripcion {

    @Autowired
    private BuscadorInscripcionProductor buscadorInscripcion;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    @Qualifier("validadorInscripcionProductorEnBeneficiario")
    private ValidadorInscripcionProductorEnBeneficiario validadorInscripcionProductorEnBeneficiario;

    @Override
    public void modifica(Inscripcion inscripcion, Map<String, DatoCapturado> datos) {
        DatoCapturado d = datos.get(DatoCapturadoProductorEnum.CURP_PRODUCTOR.getClave());
        if (Objects.isNull(d)) {
            return;
        }
        if (Objects.isNull(d.getCorrecto())) {
            return;
        }
        if (d.getCorrecto()) {
            return;
        }

        ValidadorCurpHelper.valida(d.getValor(), "del productor");
        ParametrosInscripcionProductor p = new ParametrosInscripcionProductor();
        p.setCurp(d.getValor());
        p.setCiclo(inscripcion.getCiclo());
        p.setCultivo(inscripcion.getCultivo());

        List<InscripcionProductor> inscripciones = buscadorInscripcion.busca(p);
        if (!inscripciones.isEmpty()) {
            LOGGER.warn("El CURP del productor ya existe, no se actualiza la inscripcion: {}", inscripcion.getFolio());
            return;
        }
        InscripcionProductor ip = (InscripcionProductor) inscripcion;
        ip.getDatosProductor().setCurp(d.getValor());
        ip.getDatosProductor().setFechaNacimiento(ValidadorCurpHelper.getFechaNacimiento(d.getValor()));
        ip.getDatosProductor().setSexo(buscadorCatalogo.buscaElemento(Sexo.class, ValidadorCurpHelper.getSexo(d.getValor())));

        validadorInscripcionProductorEnBeneficiario.valida(ip);
    }

}
