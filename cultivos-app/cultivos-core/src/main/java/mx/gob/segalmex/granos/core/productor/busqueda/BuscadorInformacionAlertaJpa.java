package mx.gob.segalmex.granos.core.productor.busqueda;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.productor.InformacionAlerta;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author oscar
 */
@Repository("buscadorInformacionAlerta")
@Slf4j
public class BuscadorInformacionAlertaJpa implements BuscadorInformacionAlerta {

    private static final String ACTIVO = "--";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<InformacionAlerta> busca(ParametrosInformacionAlerta parametros) {
        StringBuilder sb = new StringBuilder("SELECT a FROM InformacionAlerta a ");

        Map<String, Object> params = new HashMap<>();
        boolean first = true;

        if (Objects.nonNull(parametros.getId())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "a.id", "id", parametros.getId(), params);
        }

        if (Objects.nonNull(parametros.getFolio())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "a.folio", "folio", parametros.getFolio(), params);
        }

        if (Objects.nonNull(parametros.getTipo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "a.tipo", "tipo", parametros.getTipo(), params);
        }

        if (Objects.nonNull(parametros.getValor())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "a.valor", "valor", parametros.getValor(), params);
        }

        if (Objects.nonNull(parametros.getCiclo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "a.ciclo", "ciclo", parametros.getCiclo(), params);
        }

        if (Objects.nonNull(parametros.getCultivo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "a.cultivo", "cultivo", parametros.getCultivo(), params);
        }

        if (Objects.nonNull(parametros.getNoFolio())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("a.folio != :folio ");
            params.put("folio", parametros.getNoFolio());
        }

        if (!parametros.getValores().isEmpty()) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("a.valor IN (:valores)");
            params.put("valores", parametros.getValores());
        }

        first = QueryUtils.agregaWhereAnd(first, sb);
        QueryUtils.and(sb, "a.estatusFechaBaja", "estatusFechaBaja", ACTIVO, params);

        LOGGER.debug("Ejecutando el query: {}", sb);
        TypedQuery<InformacionAlerta> q
                = entityManager.createQuery(sb.toString(), InformacionAlerta.class);
        QueryUtils.setParametros(q, params);

        long inicio = System.currentTimeMillis();
        List<InformacionAlerta> informaciones = q.getResultList();
        LOGGER.debug("Se encontraron {} resultados en {} ms.", informaciones.size(),
                System.currentTimeMillis() - inicio);

        return informaciones;

    }

    @Override
    public InformacionAlerta buscaElemento(ParametrosInformacionAlerta parametros) {
        List<InformacionAlerta> resultados = busca(parametros);

        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontró el elemento en InformacionAlerta.", 1);
        }

        if (resultados.size() > 1) {
            throw new IncorrectResultSizeDataAccessException("Se encontró más "
                    + "de un elemnto de catálogo.", 1);
        }

        return resultados.get(0);
    }

    @Override
    public InformacionAlerta buscaElemento(String folio) {
        Objects.requireNonNull(folio, "El folio no puede ser nulo.");
        ParametrosInformacionAlerta parametros = new ParametrosInformacionAlerta();
        parametros.setFolio(folio);
        return buscaElemento(parametros);
    }

}
