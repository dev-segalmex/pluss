/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.pdf;

import mx.gob.segalmex.granos.core.contrato.pdf.*;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoRegistroEnum;
import mx.gob.segalmex.pluss.modelo.productor.ContratoFirmado;
import mx.gob.segalmex.pluss.modelo.productor.CuentaBancaria;
import mx.gob.segalmex.pluss.modelo.productor.DatosProductor;
import mx.gob.segalmex.pluss.modelo.productor.Domicilio;
import mx.gob.segalmex.pluss.modelo.productor.EmpresaAsociada;
import mx.gob.segalmex.pluss.modelo.productor.EstatusPredioProductorEnum;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import mx.gob.segalmex.pluss.modelo.productor.SocioDatosProductor;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author ismael
 */
@Slf4j
public abstract class AbstractInscripcionPdfHelper {

    protected static final float TAMANO_CONTENIDO_DOCUMENTO
            = PageSize.LETTER.getWidth() - PtConversion.toPt(1.5f) - PtConversion.toPt(1.5f);

    public abstract void generaContenido(InscripcionProductor inscripcion, Document d, PdfWriter writer)
            throws DocumentException;

    protected PdfPTable generaTablaContenido() {
        PdfPTable table = new PdfPTable(1);
        table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        table.getDefaultCell().setPadding(0);
        table.setTotalWidth(TAMANO_CONTENIDO_DOCUMENTO);
        table.setLockedWidth(true);
        return table;
    }

    protected String getFechaFormat(Calendar fecha) {
        return Objects.nonNull(fecha) ? SegalmexDateUtils.format(fecha) : "--";
    }

    protected void agregaDatosPrograma(PdfPTable contenido, InscripcionProductor inscripcion) {
        contenido.addCell(generaCeldaEncabezado("Registro de Productor - Programa de Precios de Garantía a Productos Alimentarios Básicos"));

        PdfPTable seccion = generaTabla(2, TAMANO_CONTENIDO_DOCUMENTO);
        seccion.addCell(generaCeldaSimple("Folio", inscripcion.getFolio(), false, false));
        seccion.addCell(generaCeldaSimple("Fecha de captura", getFechaFormat(inscripcion.getFechaCreacion()), false, false));
        contenido.addCell(seccion);

        seccion = generaTabla(3, TAMANO_CONTENIDO_DOCUMENTO);
        seccion.addCell(generaCeldaSimple("Cultivo", inscripcion.getCultivo().getNombre(), false, false));
        seccion.addCell(generaCeldaSimple("Ciclo agrícola", inscripcion.getCiclo().getNombre(), false, false));
        seccion.addCell(generaCeldaSimple("Tipo", inscripcion.getEtiquetaTipoCultivo(), false, false));

        contenido.addCell(seccion);
        contenido.addCell(PdfHelper.generaCeldaVacia(1));
    }

    protected void agregaDatosVentanilla(PdfPTable contenido, InscripcionProductor inscripcion) {
        contenido.addCell(generaCeldaEncabezado("Datos de la ventanilla"));

        PdfPTable seccion = generaTabla(1, TAMANO_CONTENIDO_DOCUMENTO);
        seccion.addCell(generaCeldaSimple("Nombre de la ventanilla", inscripcion.getSucursal().getEmpresa().getNombre(), false, false));
        contenido.addCell(seccion);

        seccion = generaTabla(2, TAMANO_CONTENIDO_DOCUMENTO);
        seccion.addCell(generaCeldaSimple("RFC", inscripcion.getSucursal().getEmpresa().getRfc(), false, false));
        seccion.addCell(generaCeldaSimple("Entidad", inscripcion.getSucursal().getEstado().getNombre(), false, false));
        seccion.addCell(generaCeldaSimple("Municipio", inscripcion.getSucursal().getMunicipio().getNombre(), false, false));
        seccion.addCell(generaCeldaSimple("Localidad", inscripcion.getSucursal().getLocalidad(), false, false));
        contenido.addCell(seccion);
        contenido.addCell(PdfHelper.generaCeldaVacia(1));
    }

    protected void agregaDatosProductor(PdfPTable contenido, InscripcionProductor inscripcion) {
        DatosProductor dp = inscripcion.getDatosProductor();
        PdfPTable seccion = null;
        switch (inscripcion.getDatosProductor().getTipoPersona().getClave()) {
            case "moral":
                contenido.addCell(generaCeldaEncabezado("Datos del productor (persona moral)"));
                //contenido.addCell(PdfHelper.generaCeldaVacia(1));

                seccion = generaTabla(2, TAMANO_CONTENIDO_DOCUMENTO);
                seccion.addCell(generaCeldaSimple("Nombre de la sociedad", dp.getNombreMoral(), false, false));
                seccion.addCell(generaCeldaSimple("RFC de la sociedad", dp.getRfc(), false, false));
                contenido.addCell(seccion);
                contenido.addCell(PdfHelper.generaCeldaVacia(1));

                contenido.addCell(generaCeldaEncabezado("Representante legal"));
                seccion = generaTabla(2, TAMANO_CONTENIDO_DOCUMENTO);
                seccion.addCell(generaCeldaSimple("CURP", dp.getCurp(), false, false));
                seccion.addCell(generaCeldaSimple("Nombre(s)", dp.getNombre(), false, false));
                seccion.addCell(generaCeldaSimple("Primer apellido", dp.getPrimerApellido(), false, false));
                seccion.addCell(generaCeldaSimple("Segundo apellido", dp.getSegundoApellido(), false, false));
                seccion.addCell(generaCeldaSimple("Fecha de nacimiento", SegalmexDateUtils.format(dp.getFechaNacimiento()), false, false));
                seccion.addCell(generaCeldaSimple("Sexo", dp.getSexo().getNombre(), false, false));
                seccion.addCell(generaCeldaSimple("Tipo de identificación", dp.getTipoDocumento().getNombre(), false, false));
                seccion.addCell(generaCeldaSimple("Número de identificación", dp.getNumeroDocumento(), false, false));
                contenido.addCell(seccion);
                contenido.addCell(PdfHelper.generaCeldaVacia(1));

                seccion = generaTabla(1, TAMANO_CONTENIDO_DOCUMENTO);
                seccion.addCell(generaCeldaEncabezado("Socios"));
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, getSocios(inscripcion.getDatosProductor().getSocios()), false, false));
                contenido.addCell(seccion);
                break;
            case "fisica":
                if (inscripcion.getTipoRegistro().equals(TipoRegistroEnum.ASOCIADO.getClave())) {
                    contenido.addCell(generaCeldaEncabezado("Datos del productor (persona física) / Asociado"));
                } else {
                    contenido.addCell(generaCeldaEncabezado("Datos del productor (persona física)"));
                }

                seccion = generaTabla(2, TAMANO_CONTENIDO_DOCUMENTO);
                seccion.addCell(generaCeldaSimple("CURP", dp.getCurp(), false, false));
                seccion.addCell(generaCeldaSimple("RFC", dp.getRfc(), false, false));
                contenido.addCell(seccion);

                seccion = generaTabla(3, TAMANO_CONTENIDO_DOCUMENTO);
                seccion.addCell(generaCeldaSimple("Nombre(s)", dp.getNombre(), false, false));
                seccion.addCell(generaCeldaSimple("Primer apellido", dp.getPrimerApellido(), false, false));
                seccion.addCell(generaCeldaSimple("Segundo apellido", dp.getSegundoApellido(), false, false));
                contenido.addCell(seccion);

                seccion = generaTabla(2, TAMANO_CONTENIDO_DOCUMENTO);
                seccion.addCell(generaCeldaSimple("Fecha de nacimiento", Objects.nonNull(dp.getFechaNacimiento())
                        ? SegalmexDateUtils.format(dp.getFechaNacimiento()) : "", false, false));
                seccion.addCell(generaCeldaSimple("Sexo", dp.getSexo().getNombre(), false, false));
                seccion.addCell(generaCeldaSimple("Tipo de identificación", dp.getTipoDocumento().getNombre(), false, false));
                seccion.addCell(generaCeldaSimple("Número de identificación", dp.getNumeroDocumento(), false, false));
                contenido.addCell(seccion);

                seccion = generaTabla(3, TAMANO_CONTENIDO_DOCUMENTO);
                seccion.addCell(generaCeldaSimple("Grupo indígena", inscripcion.getGrupoIndigena().getNombre(), false, false));
                seccion.addCell(generaCeldaSimple("Tipo productor", inscripcion.getTipoProductor().getNombre(), false, false));
                seccion.addCell(generaCeldaSimple("Grado de estudios", inscripcion.getNivelEstudio().getNombre(), false, false));
                contenido.addCell(seccion);
                break;
        }
        contenido.addCell(PdfHelper.generaCeldaVacia(1));
    }

    protected String getSocios(List<SocioDatosProductor> socios) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < socios.size(); i++) {
            sb.append(socios.get(i).getCurp());
            sb.append(i == socios.size() - 1 ? "" : ", ");
        }
        return sb.toString();
    }

    protected void agregaDatosContacto(PdfPTable contenido, InscripcionProductor inscripcion) {
        contenido.addCell(generaCeldaEncabezado("Datos de contacto"));
        //contenido.addCell(PdfHelper.generaCeldaVacia(1));

        PdfPTable seccion = generaTabla(2, TAMANO_CONTENIDO_DOCUMENTO);
        seccion.addCell(generaCeldaSimple("Número de teléfono / celular", inscripcion.getNumeroTelefono(), false, false));
        seccion.addCell(generaCeldaSimple("Correo electrónico", inscripcion.getCorreoElectronico(), false, false));

        contenido.addCell(seccion);
        contenido.addCell(PdfHelper.generaCeldaVacia(1));
    }

    protected void agregaDomicilioProductor(PdfPTable contenido, InscripcionProductor inscripcion) {
        contenido.addCell(generaCeldaEncabezado("Domicilio del productor"));
        //contenido.addCell(PdfHelper.generaCeldaVacia(1));

        PdfPTable seccion = generaTabla(3, TAMANO_CONTENIDO_DOCUMENTO);
        Domicilio d = inscripcion.getDomicilio();
        seccion.addCell(generaCeldaSimple("Calle", d.getCalle(), false, false));
        seccion.addCell(generaCeldaSimple("No. exterior e interior", d.getNumeroExterior()
                + (StringUtils.isEmpty(d.getNumeroInterior()) ? "" : ", " + d.getNumeroInterior()), false, false));
        seccion.addCell(generaCeldaSimple("Localidad", d.getLocalidad(), false, false));
        seccion.addCell(generaCeldaSimple("Código postal", d.getCodigoPostal(), false, false));
        seccion.addCell(generaCeldaSimple("Municipio", d.getMunicipio().getNombre(), false, false));
        seccion.addCell(generaCeldaSimple("Estado", d.getEstado().getNombre(), false, false));

        contenido.addCell(seccion);
        contenido.addCell(PdfHelper.generaCeldaVacia(1));

    }

    protected void agregaContratos(PdfPTable contenido, InscripcionProductor inscripcion) {

        if (!inscripcion.getContratosActivos().isEmpty()) {
            contenido.addCell(generaCeldaEncabezado("Contratos del productor"));
            //contenido.addCell(PdfHelper.generaCeldaVacia(1));

            PdfPTable seccion = generaTabla(5, TAMANO_CONTENIDO_DOCUMENTO);
            seccion.addCell(generaCeldaEncabezado("No. contrato"));
            seccion.addCell(generaCeldaEncabezado("Folio"));
            seccion.addCell(generaCeldaEncabezado("Empresa"));
            seccion.addCell(generaCeldaEncabezado("Cantidad contratada"));
            seccion.addCell(generaCeldaEncabezado("Cantidad contratada con cobertura"));

            for (ContratoFirmado c : inscripcion.getContratosActivos()) {
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, c.getNumeroContrato(), false, false));
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, c.getFolio(), false, false));
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, c.getEmpresa(), false, false));
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, getFormatoNumero(c.getCantidadContratada()), false, false));
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, getFormatoNumero(c.getCantidadContratadaCobertura()), false, false));
            }

            contenido.addCell(seccion);
            contenido.addCell(PdfHelper.generaCeldaVacia(1));
        }
    }

    protected void agregaSociedades(PdfPTable contenido, InscripcionProductor inscripcion) {
        if (!inscripcion.getEmpresasActivas().isEmpty()) {
            contenido.addCell(generaCeldaEncabezado("Sociedades"));

            PdfPTable seccion = generaTabla(2, TAMANO_CONTENIDO_DOCUMENTO);
            seccion.addCell(generaCeldaEncabezado("RFC"));
            seccion.addCell(generaCeldaEncabezado("Nombre de la sociedad"));

            for (EmpresaAsociada e : inscripcion.getEmpresas()) {
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, e.getRfc(), false, false));
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, e.getNombre(), false, false));
            }
            contenido.addCell(seccion);
            contenido.addCell(PdfHelper.generaCeldaVacia(1));
        }
    }

    public void agregaPredios(PdfPTable contenido, InscripcionProductor inscripcion) {
        contenido.addCell(generaCeldaEncabezado("Predios del productor"));

        PdfPTable seccion = generaTabla(10, TAMANO_CONTENIDO_DOCUMENTO);
        seccion.addCell(generaCeldaEncabezado("Folio"));
        seccion.addCell(generaCeldaEncabezado("Cultivo"));
        seccion.addCell(generaCeldaEncabezado("Posesión y acreditación"));
        seccion.addCell(generaCeldaEncabezado("Régimen"));
        seccion.addCell(generaCeldaEncabezado("Vol (t)"));
        seccion.addCell(generaCeldaEncabezado("Sup (ha)"));
        seccion.addCell(generaCeldaEncabezado("Rnd (t/ha)"));
        seccion.addCell(generaCeldaEncabezado("Municipo y Estado"));
        seccion.addCell(generaCeldaEncabezado("Localidad"));
        seccion.addCell(generaCeldaEncabezado("Ubicación"));

        for (PredioProductor p : inscripcion.getPrediosActivos()) {
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, p.getFolio(), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, p.getTipoCultivo().getNombre(), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, p.getTipoPosesion().getNombre() + ",\n" + p.getTipoDocumentoPosesion().getNombre(), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, p.getRegimenHidrico().getNombre(), false, false));
            String volumen = "";
            String rendimiento = "";
            if (p.getSolicitado().equals(EstatusPredioProductorEnum.SOLICITADO.getClave())) {
                volumen = getFormatoNumero(p.getVolumen()) + "\n(" + getFormatoNumero(p.getVolumenSolicitado()) + ")";
                rendimiento = getFormatoNumero(p.getRendimiento()) + "\n(" + getFormatoNumero(p.getRendimientoSolicitado()) + ")";
            } else {
                volumen = getFormatoNumero(p.getVolumen());
                rendimiento = getFormatoNumero(p.getRendimiento());
            }
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, volumen, false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, getFormatoNumero(p.getSuperficie()), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, rendimiento, false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, p.getMunicipio().getNombre() + ", " + p.getEstado().getNombre(), false, false));
            seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, p.getLocalidad(), false, false));
            if (Objects.nonNull(p.getLatitud())) {
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, "[" + getFormatoNumero(p.getLatitud())
                        + ", " + getFormatoNumero(p.getLongitud()) + "]", false, false));
            } else {
                StringBuilder sb = new StringBuilder();
                sb.append("[");
                sb.append(getFormatoNumero(p.getLatitud1()));
                sb.append(", ");
                sb.append(getFormatoNumero(p.getLongitud1()));
                sb.append("],\n[");
                sb.append(getFormatoNumero(p.getLatitud2()));
                sb.append(", ");
                sb.append(getFormatoNumero(p.getLongitud2()));
                sb.append("],\n[");
                sb.append(getFormatoNumero(p.getLatitud3()));
                sb.append(", ");
                sb.append(getFormatoNumero(p.getLongitud3()));
                sb.append("],\n[");
                sb.append(getFormatoNumero(p.getLatitud4()));
                sb.append(", ");
                sb.append(getFormatoNumero(p.getLongitud4()));
                sb.append("]");
                seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, sb.toString(), false, false));
            }
        }

        contenido.addCell(seccion);
        contenido.addCell(PdfHelper.generaCeldaVacia(1));
    }

    protected void agregaDatosBancarios(PdfPTable contenido, InscripcionProductor inscripcion) {
        contenido.addCell(generaCeldaEncabezado("Datos bancarios"));

        PdfPTable seccion = generaTabla(2, TAMANO_CONTENIDO_DOCUMENTO);
        CuentaBancaria cb = inscripcion.getCuentaBancaria();
        seccion.addCell(generaCeldaSimple("Banco", cb.getBanco().getNombre(), false, false));
        seccion.addCell(generaCeldaSimple("CLABE", cb.getClabe(), false, false));

        contenido.addCell(seccion);
        contenido.addCell(PdfHelper.generaCeldaVacia(1));
    }

    protected void agregaBeneficiario(PdfPTable contenido, InscripcionProductor inscripcion) {
        contenido.addCell(generaCeldaEncabezado("Beneficiario"));

        PdfPTable seccion = generaTabla(4, TAMANO_CONTENIDO_DOCUMENTO);
        seccion.addCell(generaCeldaSimple("Nombre", inscripcion.getNombreBeneficiario(), false, false));
        seccion.addCell(generaCeldaSimple("Apellidos", inscripcion.getApellidosBeneficiario(), false, false));
        seccion.addCell(generaCeldaSimple("Parentesco", inscripcion.getParentesco().getNombre(), false, false));
        seccion.addCell(generaCeldaSimple("CURP", inscripcion.getCurpBeneficiario(), false, false));

        contenido.addCell(seccion);
        contenido.addCell(PdfHelper.generaCeldaVacia(1));
    }

    public abstract void agregaTerminos(PdfPTable contenido, InscripcionProductor inscripcion);

    protected void agregaFirmas(PdfPTable contenido, InscripcionProductor inscripcion) {
        PdfPTable seccion = generaTabla(2, TAMANO_CONTENIDO_DOCUMENTO);
        seccion.addCell(generaCeldaEncabezado("Nombre y firma del productor o representante legal"));
        seccion.addCell(generaCeldaEncabezado("Nombre y firma del encargado del registro"));
        PdfPCell productor = generaCeldaSimple(StringUtils.EMPTY, "\n\n\n\n\n\n" + inscripcion.getDatosProductor().getNombre()
                + " " + inscripcion.getDatosProductor().getPrimerApellido()
                + " " + StringUtils.defaultIfBlank(inscripcion.getDatosProductor().getSegundoApellido(), ""), false, false);
        productor.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        seccion.addCell(productor);
        PdfPCell encargado = generaCeldaSimple(StringUtils.EMPTY, "\n\n\n\n\n\n" + inscripcion.getEncargado(), false, false);
        encargado.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        seccion.addCell(encargado);
        contenido.addCell(seccion);
    }

    public static PdfPTable generaTabla(int colspan, float width) {
        PdfPTable table = new PdfPTable(colspan);
        table.setTotalWidth(width);
        table.setLockedWidth(true);
        table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        table.getDefaultCell().setPadding(0);

        return table;
    }

    public static PdfPCell generaCeldaEncabezado(String encabezado) {
        PdfPCell celda = PdfHelper.creaCeldaBackground(encabezado, 1,
                PdfConstants.SOBERANA_LIGHT_8_BLACK, PdfConstants.BACKGROUND_GOBMX_GRAY);
        celda.setBorderColor(PdfConstants.BACKGROUND_GOBMX_DARKGRAY);
        celda.setBorder(Rectangle.BOX);
        celda.setBorderWidth(PdfConstants.CELDA_TAMANO_BORDE);
        celda.setMinimumHeight(PtConversion.toPt(PdfConstants.CELDA_ALTURA_MINIMA_CM));
        celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
        celda.setPadding(PtConversion.toPt(PdfConstants.CELDA_PADDING_CM));
        celda.setPaddingTop(PtConversion.toPt(0.10f));

        return celda;
    }

    protected String getFormatoNumero(BigDecimal bd) {
        if (Objects.isNull(bd)) {
            return "--";
        }

        NumberFormat nf = new DecimalFormat("0.#####");
        return nf.format(bd);
    }

    /**
     * Genera una celda utilizada en las tablas de las secciones de las
     * solicitudes.
     *
     * @param etiqueta la etiqueta de la celda.
     * @param valor el valor correspondiente a la etiqueta.
     * @param transform indica si debe de transformarse el parámetro
     * <code>valor</code> a UpperCase.
     * @param blank true si debe de usarse una cadena vacía como valor cuando
     * éste es nulo, false para usar <code>--</code>.
     *
     * @return una celda utilizada en las tablas de secciones de las
     * solicitudes.
     */
    protected static PdfPCell generaCeldaSimple(String etiqueta, String valor, boolean transform, boolean blank) {
        StringBuilder sb = new StringBuilder();
        if (StringUtils.trimToNull(etiqueta) != null) {
            sb.append(etiqueta);
            sb.append(": ");
        }

        String nValor;
        if (Objects.isNull(StringUtils.trimToNull(valor))) {
            nValor = !blank ? "--" : StringUtils.EMPTY;
        } else {
            nValor = valor;
        }

        nValor = transform ? StringUtils.upperCase(nValor) : nValor;
        sb.append(nValor);

        PdfPCell celda = PdfHelper.creaCeldaBackground(sb.toString(), 1, PdfConstants.SOBERANA_LIGHT_8_BLACK, PdfConstants.BACKGROUND_GOBMX_WHITE);
        celda.setBorderColor(PdfConstants.BACKGROUND_GOBMX_DARKGRAY);
        celda.setBorder(Rectangle.BOX);
        celda.setBorderWidth(PdfConstants.CELDA_TAMANO_BORDE);
        celda.setMinimumHeight(PtConversion.toPt(PdfConstants.CELDA_ALTURA_MINIMA_CM));
        celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
        celda.setPadding(PtConversion.toPt(PdfConstants.CELDA_PADDING_CM));
        celda.setPaddingLeft(PtConversion.toPt(PdfConstants.CELDA_LEFT_PADDING_CM));

        return celda;
    }
}
