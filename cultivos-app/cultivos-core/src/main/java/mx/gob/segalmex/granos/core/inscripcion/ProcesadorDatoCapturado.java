/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.inscripcion;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.historico.EntidadHistorico;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.Inscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;

/**
 *
 * @author ismael
 */
public interface ProcesadorDatoCapturado {

    List<DatoCapturado> procesa(EntidadHistorico entidad);

    void validaEstatus(EntidadHistorico entidad, List<DatoCapturado> datos, EstatusInscripcionEnum estatus);

    void solicitaCorreccion(EntidadHistorico entidad, List<DatoCapturado> datos);

    void corrige(EntidadHistorico entidad, List<DatoCapturado> datos);

    void corrige(InscripcionContrato inscripcion, List<DatoCapturado> datos);

    void actualizaCampos(InscripcionContrato inscripcion);

    void eliminaDuplicados(Inscripcion inscripcion);

    void elimina(Inscripcion inscripcion);

    List<DatoCapturado> procesaIar(EntidadHistorico entidad);
}
