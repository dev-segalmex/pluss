/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura;

import mx.gob.segalmex.pluss.modelo.factura.InscripcionDocumentoComercial;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;

/**
 *
 * @author ismael
 */
public interface ProcesadorInscripcionDocumentoComercial {

    /**
     * Procesa una inscripción de documento comercial.
     *
     * @param inscripcion la inscripción a procesar.
     * @param usuario el usuario que realiza el registro inicial.
     * @return la inscripción procesada.
     */
    InscripcionDocumentoComercial procesa(InscripcionDocumentoComercial inscripcion, Usuario usuario);
}
