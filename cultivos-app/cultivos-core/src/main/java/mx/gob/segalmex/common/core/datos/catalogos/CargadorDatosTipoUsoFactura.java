package mx.gob.segalmex.common.core.datos.catalogos;

import java.math.BigDecimal;
import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.factura.TipoUsoFactura;

/**
 *
 * @author oscar
 */
public class CargadorDatosTipoUsoFactura extends AbstractCargadorDatosCatalogo<TipoUsoFactura> {

    @Override
    public TipoUsoFactura getInstance(String[] elementos) {
        TipoUsoFactura tuf = new TipoUsoFactura();
        tuf.setClave(elementos[0]);
        tuf.setNombre(elementos[1]);
        tuf.setActivo(true);
        tuf.setOrden(Integer.parseInt(elementos[2]));
        tuf.setFactor(new BigDecimal(elementos[3]));

        return tuf;
    }

}
