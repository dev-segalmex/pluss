/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFacturaSimple;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersonaEnum;
import mx.gob.segalmex.pluss.modelo.productor.Productor;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author oscar
 */
public class InscripcionFacturaSimpleFactory {

    private static final String VACIO = "--";

    public static InscripcionFacturaSimple getInstance(InscripcionFactura i) {
        InscripcionFacturaSimple simple = new InscripcionFacturaSimple();
        simple.setFolio(i.getFolio());
        simple.setFechaCreacion(i.getFechaCreacion());
        simple.setCultivo(i.getCultivo().getNombre());
        simple.setCiclo(i.getCiclo().getNombre());
        simple.setEstado(Objects.nonNull(i.getEstado()) ? i.getEstado().getNombre() : VACIO);
        simple.setNombreEmpresa(i.getSucursal().getEmpresa().getNombre());
        simple.setRfcEmpresa(i.getSucursal().getEmpresa().getRfc());
        simple.setTipoSucursal(i.getSucursal().getTipo().getNombre());
        simple.setContrato(StringUtils.defaultIfEmpty(i.getContrato(), VACIO));
        simple.setTipoFactura(i.getTipoFactura());
        simple.setProductorCiclo(Objects.nonNull(i.getProductorCiclo()));
        if (Objects.nonNull(i.getProductorCiclo())) {
            Productor p = i.getProductorCiclo().getProductor();
            simple.setClaveTipoPersona(p.getTipoPersona().getClave());
            simple.setNombreTipoPersona(p.getTipoPersona().getNombre());
            if (p.getTipoPersona().getClave().equals(TipoPersonaEnum.FISICA.getClave())) {
                simple.setPersonaClave(StringUtils.defaultIfBlank(p.getPersona().getClave(), VACIO));
                simple.setPersonaRfc(StringUtils.defaultIfBlank(p.getRfc(), VACIO));
                simple.setPersonaNombre(StringUtils.defaultIfBlank(p.getPersona().getNombre(), VACIO));
                simple.setPersonaApellido(StringUtils.defaultIfBlank(p.getPersona().getApellidos(), VACIO));
            } else {
                simple.setPersonaRfc(StringUtils.defaultIfBlank(p.getRfc(), VACIO));
                simple.setProductorNombre(StringUtils.defaultIfBlank(p.getNombre(), VACIO));
            }
        }
        simple.setCfdiNombreEmisor(StringUtils.trimToEmpty(i.getCfdi().getNombreEmisor()));
        simple.setCfdiRfcEmisor(i.getCfdi().getRfcEmisor());
        simple.setCfdiNombreReceptor(StringUtils.trimToEmpty(i.getCfdi().getNombreReceptor()));
        simple.setCfdiRfcReceptor(i.getCfdi().getRfcReceptor());
        simple.setCfdiFecha(i.getCfdi().getFecha());
        simple.setCfdiMoneda(i.getCfdi().getMoneda());
        simple.setCfdiTotal(i.getCfdi().getTotal());
        simple.setCfdiFechaTimbrado(i.getCfdi().getFechaTimbrado());
        simple.setCfdiUuidTimbreFiscal(i.getCfdi().getUuidTimbreFiscalDigital());
        simple.setCfdiTotalToneladas(i.getCfdi().getTotalToneladas());
        simple.setEstatusNombre(i.getEstatus().getNombre());
        simple.setComentarioEstatus(Objects.nonNull(i.getComentarioEstatus())
                ? i.getComentarioEstatus() : VACIO);
        simple.setNombreUsuarioRegistra(i.getUsuarioRegistra().getNombre());
        simple.setNombreUsuarioValidador(Objects.nonNull(i.getUsuarioValidador())
                ? i.getUsuarioValidador().getNombre() : VACIO);
        return simple;
    }

    public static List<InscripcionFacturaSimple> getInstance(List<InscripcionFactura> inscripciones) {
        List<InscripcionFacturaSimple> simples = new ArrayList<>();
        for (InscripcionFactura i : inscripciones) {
            simples.add(getInstance(i));
        }
        return simples;
    }
}
