package mx.gob.segalmex.common.core.datos.catalogos;

import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;

/**
 *
 * @author ismaelhz
 */
public class CargadorDatosCultivo extends AbstractCargadorDatosCatalogo<Cultivo> {

    @Override
    public Cultivo getInstance(String[] elementos) {
        Cultivo cultivo = new Cultivo();
        cultivo.setClave(elementos[0]);
        cultivo.setNombre(elementos[1]);
        cultivo.setActivo(true);
        cultivo.setOrden(Integer.parseInt(elementos[2]));

        return cultivo;
    }

}
