/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.rfc.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.productor.RfcExcepcion;

/**
 * Defiición de los metodos de búsqueda sobre ejemplares de
 * {@link RfcExcepcion}.
 *
 * @author oscar
 */
public interface BuscadorRfcExcepcion {

    /**
     * Obtiene una lista de {@link RfcExcepcion} dados los parámetros recibidos.
     *
     * @param parametros
     * @return
     */
    List<RfcExcepcion> busca(ParametrosRfcExcepcion parametros);

    /**
     * Obtiene una lista String de los RFCs dados los parámetros recibidos.
     *
     * @param parametros
     * @return
     */
    List<String> buscaRfc(ParametrosRfcExcepcion parametros);

    /**
     * Obtiene una lista de String con los RFCs que pueden ser considerados como
     * excepciones incluyendo el RFC recibido.
     *
     * @param rfc el primero considerado como excepción
     * @param genericos se incluye XAXX010101000 y XEXX010101000 si es true.
     * @return la lista en String de los rfcs considerados excepciones.
     */
    List<String> getExcepciones(String rfc, boolean genericos);

    /**
     * Obtiene un {@link RfcExcepcion} por su id.
     *
     * @param id
     * @return el {@link RfcExcepcion} encontrado.
     */
    RfcExcepcion buscaElemento(Integer id);

    /**
     * Obtiene una {@link RfcExcepcion} dado su rfc.
     *
     * @param rfc
     * @return el {@link RfcExcepcion} recuperado.
     */
    RfcExcepcion buscaElemento(String rfc);

}
