/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.factura.Cfdi;

/**
 *
 * @author ismael
 */
public interface BuscadorCfdi {

    Cfdi buscaElemento(ParametrosCfdi parametros);

    public Cfdi buscaElemento(String uuid);

    public List<Cfdi> buscaPorEmisor(String rfc);

    public List<Cfdi> buscaPorReceptor(String rfc);
}
