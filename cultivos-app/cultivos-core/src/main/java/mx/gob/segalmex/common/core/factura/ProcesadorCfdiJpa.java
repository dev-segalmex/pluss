/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import javax.xml.bind.JAXBException;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.archivos.ArchivoUtils;
import mx.gob.segalmex.common.core.archivos.ManejadorArchivo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorUsoCatalogo;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorFactura;
import mx.gob.segalmex.common.core.factura.modelo.CfdiComprobante;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.common.core.util.CultivoClaveUtil;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import mx.gob.segalmex.pluss.modelo.catalogos.UsoCatalogo;
import mx.gob.segalmex.pluss.modelo.factura.Cfdi;
import mx.gob.segalmex.pluss.modelo.factura.ConceptoCfdi;
import mx.gob.segalmex.pluss.modelo.factura.EstatusCfdiEnum;
import mx.gob.segalmex.pluss.modelo.factura.EstatusConceptoCfdiEnum;
import mx.gob.segalmex.pluss.modelo.factura.EstatusFacturaEnum;
import mx.gob.segalmex.pluss.modelo.factura.Factura;
import mx.gob.segalmex.pluss.modelo.factura.UnidadCfdi;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.granos.core.inscripcion.BuscadorCicloAgricolaActivo;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ismael
 */
@Service("cfdiProcesador")
@Slf4j
public class ProcesadorCfdiJpa implements ProcesadorCfdi {

    private static final String USO_CLAVE_TONELADAS = "uso-clave-toneladas";

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private CfdiParser cfdiParser;

    @Autowired
    private ManejadorArchivo manejadorArchivo;

    @Autowired
    private BuscadorFactura buscadorFactura;

    @Autowired
    private BuscadorUsoCatalogo buscadorUsoCatalogo;

    @Autowired
    private BuscadorCicloAgricolaActivo buscadorCicloAgricola;

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Transactional
    @Override
    public Cfdi procesa(InputStream is, String nombre, Usuario usuario,
            Sucursal sucursal, boolean invertir, Cultivo cultivo) throws IOException {
        try {
            String fecha = SegalmexDateUtils.format(Calendar.getInstance(), "yyy-MM-dd");
            Archivo archivo = ArchivoUtils.getInstance(nombre, CultivoClaveUtil.getClaveCorta(cultivo),
                    "/cfdi/" + sucursal.getEmpresa().getRfc() + "/" + fecha, false);
            manejadorArchivo.guarda(archivo, is);

            CfdiComprobante cc = cfdiParser.parse(manejadorArchivo.obtenStream(archivo));
            Cfdi cfdi = CfdiFactory.getInstance(cc, invertir);
            cfdi.setUuid(UUID.randomUUID().toString());
            cfdi.setUsuario(usuario);
            cfdi.setEstatus(EstatusCfdiEnum.PENDIENTE.getClave());
            cfdi.setArchivo(archivo);

            List<String> claves = getClavesToneladas(buscadorUsoCatalogo
                    .buscaUsos(UnidadCfdi.class, USO_CLAVE_TONELADAS));
            BigDecimal totalToneladas = BigDecimal.ZERO;
            for (ConceptoCfdi c : cfdi.getConceptos()) {
                c.setEstatus(getEstatus(c.getClaveProductoServicio(), cultivo, usuario));
                if (c.getEstatus().equals(EstatusConceptoCfdiEnum.IGNORADO.getClave())) {
                    continue;
                }
                BigDecimal toneladas = c.getCantidad();
                switch (c.getClaveUnidad()) {
                    case "KGM":
                    case "58":
                        toneladas = c.getCantidad().divide(new BigDecimal(1000), 3, RoundingMode.HALF_UP);
                        c.setUnidad("Kilogramos");
                        break;
                    default:
                        if (claves.contains(c.getClaveUnidad())) {
                            c.setUnidad("Toneladas");
                            break;
                        }
                        LOGGER.warn("Los conceptos del CFDI no están en toneladas o kilogramos.");
                        toneladas = null;
                }
                if (Objects.isNull(toneladas)) {
                    totalToneladas = null;
                    break;
                }
                totalToneladas = totalToneladas.add(toneladas);
            }
            cfdi.setTotalToneladas(totalToneladas);
            cfdi.setSubtotal(CfdiHelper.calculaSubtotal(cfdi));
            registroEntidad.guarda(cfdi);
            int i = 0;
            for (ConceptoCfdi concepto : cfdi.getConceptos()) {
                concepto.setOrden(i++);
                registroEntidad.guarda(concepto);
            }

            return cfdi;
        } catch (JAXBException ouch) {
            throw new RuntimeException(ouch);
        }
    }

    private List<String> getClavesToneladas(List<UsoCatalogo> usos) {
        List<String> claves = new ArrayList<>();
        for (UsoCatalogo u : usos) {
            claves.add(u.getClave());
        }
        return claves;
    }

    @Transactional
    @Override
    public Factura procesa(Cfdi cfdi, String tipoFactura) {
        // Buscamos la factura
        try {
            return buscadorFactura.buscaElementoPorUuidTfd(cfdi.getUuidTimbreFiscalDigital());
        } catch (EmptyResultDataAccessException ouch) {
        }

        // Generamos una nueva factura
        Factura f = FacturaFactory.getInstance(cfdi);
        f.setTipo(tipoFactura);
        f.setEstatus(EstatusFacturaEnum.NUEVA.getClave());
        registroEntidad.guarda(f);
        return f;
    }

    private String getEstatus(String claveServicio, Cultivo cultivo, Usuario usuario) {
        List<CicloAgricola> ciclosActivos = buscadorCicloAgricola.busca(cultivo, "--", usuario);
        CicloAgricola ciclo = buscadorCicloAgricola.getCicloSeleccionado(cultivo, ciclosActivos, usuario, true);
        String grupo = cultivo.getClave() + ":" + ciclo.getClave();
        List<String> positivas = buscadorParametro.getValores(grupo + ":clave-servicio-positivas", String.class);
        for (String cp : positivas) {
            LOGGER.info("Positiva: {}", cp);
        }
        List<String> ignoradas = buscadorParametro.getValores(grupo + ":clave-servicio-ignoradas", String.class);
        String estatus = EstatusConceptoCfdiEnum.NEGATIVO.getClave();
        for (String ci : ignoradas) {
            LOGGER.info("Ignorada: {}", ci);
        }
        LOGGER.info("Clave a validar: {}", claveServicio);

        if (positivas.contains(claveServicio)) {
            estatus = EstatusConceptoCfdiEnum.POSITIVO.getClave();
        } else if (ignoradas.contains(claveServicio)) {
            estatus = EstatusConceptoCfdiEnum.IGNORADO.getClave();
        }
        return estatus;
    }

}
