/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.productor.PreRegistroProductor;

/**
 *
 * @author ismael
 */
public interface BuscadorPreRegistroProductor {

    PreRegistroProductor buscaElementoUuid(String uuid);

    List<PreRegistroProductor> buscaElementoCurp(String curp);

    List<PreRegistroProductor> busca(ParametrosInscripcionProductor parametros);

    List<Integer> busca(String curp, Cultivo cultivo, CicloAgricola ciclo);

    PreRegistroProductor buscaElemento(ParametrosInscripcionProductor parametros);

    List<Object[]> buscaAgrupado(ParametrosInscripcionProductor parametros);
}
