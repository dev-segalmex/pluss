package mx.gob.segalmex.granos.core.productor;

import mx.gob.segalmex.pluss.modelo.productor.InformacionAlerta;

/**
 *
 * @author oscar
 */
public interface ProcesadorInformacionAlerta {

    void procesa(InformacionAlerta alerta);

    void modifica(InformacionAlerta actual, InformacionAlerta nuevo);

    void elimina(InformacionAlerta alerta);

}
