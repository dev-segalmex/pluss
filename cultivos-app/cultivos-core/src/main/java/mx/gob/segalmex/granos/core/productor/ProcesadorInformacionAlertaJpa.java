package mx.gob.segalmex.granos.core.productor;

import java.util.Calendar;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.folio.GeneradorFolio;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.pluss.modelo.secuencias.TipoFolioEnum;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInformacionAlerta;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInformacionAlerta;
import mx.gob.segalmex.pluss.modelo.productor.InformacionAlerta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author oscar
 */
@Service("procesadorInformacionAlerta")
@Slf4j
public class ProcesadorInformacionAlertaJpa implements ProcesadorInformacionAlerta {

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private GeneradorFolio generadorFolio;

    @Autowired
    private BuscadorInformacionAlerta buscadorInformacionAlerta;

    @Override
    @Transactional
    public void procesa(InformacionAlerta alerta) {
        if (Objects.isNull(alerta.getValor())) {
            throw new SegalmexRuntimeException("Error al registrar.", "El valor no puede ser nulo.");
        }
        verificaExiste(alerta);
        alerta.setFolio(generadorFolio.genera(TipoFolioEnum.SEQ_FOLIO_ALERTA, 6));
        registroEntidad.guarda(alerta);
    }

    @Override
    @Transactional
    public void modifica(InformacionAlerta actual, InformacionAlerta nuevo) {
        verificaExiste(nuevo);
        actual = buscadorInformacionAlerta.buscaElemento(actual.getFolio());
        actual.setTipo(nuevo.getTipo());
        actual.setValor(nuevo.getValor());
        actual.setComentario(nuevo.getComentario());
        actual.setCiclo(nuevo.getCiclo());
        actual.setCultivo(nuevo.getCultivo());
        registroEntidad.actualiza(actual);
    }

    @Override
    @Transactional
    public void elimina(InformacionAlerta alerta) {
        LOGGER.info("Eliminando InformacionAlerta: {}", alerta.getFolio());
        alerta.setEstatusFechaBaja(SegalmexDateUtils.format(Calendar.getInstance(), "yyyy-MM-dd'T'HH:mm:ss"));
        registroEntidad.actualiza(alerta);
    }

    private void verificaExiste(InformacionAlerta alerta) {
        ParametrosInformacionAlerta parametros = new ParametrosInformacionAlerta();
        parametros.setTipo(alerta.getTipo());
        parametros.setValor(alerta.getValor());
        parametros.setCiclo(alerta.getCiclo());
        parametros.setCultivo(alerta.getCultivo());
        parametros.setNoFolio(alerta.getFolio());
        if (!buscadorInformacionAlerta.busca(parametros).isEmpty()) {
            throw new SegalmexRuntimeException("Error al guardar la información alerta.",
                    "Ya existe una con estos datos.");
        }
    }

}
