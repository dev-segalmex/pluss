/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.accion.util;

import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.util.CultivoClaveUtil;
import mx.gob.segalmex.pluss.modelo.seguridad.Accion;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Component
@Slf4j
public class AccionConsultaHelper {

    private static final String GB = "_";

    private static final String GLOBAL = "global";

    private static final String EMPRESA = "empresa";

    private static final String SUCURSAL = "sucursal";

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    public Accion getAccion(Cultivo cultivo, String tipo, String clave) {
        Objects.requireNonNull(clave, "La clave es necesaria.");
        String claveAccion = null;
        tipo = Objects.isNull(tipo) ? StringUtils.EMPTY : tipo;
        switch (tipo) {
            case GLOBAL:
            case EMPRESA:
            case SUCURSAL:
                claveAccion = getClaveCultivo(cultivo) + clave + GB + tipo;
                LOGGER.info("Buscando acción: {}", claveAccion);
                return buscadorCatalogo.buscaElemento(Accion.class, claveAccion);
            default:
                claveAccion = getClaveCultivo(cultivo) + clave;
                LOGGER.info("Buscando acción: {}", claveAccion);
                return buscadorCatalogo.buscaElemento(Accion.class, claveAccion);
        }
    }

    private String getClaveCultivo(Cultivo c) {
        if (Objects.isNull(c)) {
            return StringUtils.EMPTY;
        }

        try {
            return CultivoClaveUtil.getClaveCorta(c) + GB;
        } catch (IllegalArgumentException ouch) {
            return StringUtils.EMPTY;
        }
    }
}
