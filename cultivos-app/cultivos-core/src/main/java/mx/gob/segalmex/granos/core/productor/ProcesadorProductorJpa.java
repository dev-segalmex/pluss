/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.granos.core.historico.ProcesadorHistoricoRegistro;
import mx.gob.segalmex.granos.core.inscripcion.ActualizadorEstatusHelper;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.modelo.historico.HistoricoRegistro;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.productor.DatosProductor;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.Productor;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCicloCultivo;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCicloGrupo;
import mx.gob.segalmex.pluss.modelo.productor.TipoProductorEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ismael
 */
@Service("procesadorProductor")
@Slf4j
public class ProcesadorProductorJpa implements ProcesadorProductor {

    @Autowired
    private BuscadorProductor buscadorProductor;

    @Autowired
    private RegistroProductor registroProductor;

    @Autowired
    private ProcesadorHistoricoRegistro procesadorHistoricoRegistro;

    @Autowired
    private ProcesadorSociedad procesadorSociedad;

    @Autowired
    private BuscadorInscripcionProductor buscadorInscripcionProductor;

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private ProductorCicloGrupoFactory productorCicloGrupoFactory;

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Autowired
    private ActualizadorEstatusHelper actualizadorEstatusHelper;

    @Transactional
    @Override
    public Productor procesa(DatosProductor dp) {
        try {
            return buscadorProductor.busca(dp.getRfc(), dp.getTipoPersona());
        } catch (EmptyResultDataAccessException ouch) {
            LOGGER.info("El Productor no existe, registrando uno nuevo...");
        }
        return registroProductor.registra(dp);
    }

    @Transactional
    @Override
    public ProductorCiclo procesa(Productor productor, InscripcionProductor inscripcion) {
        try {
            ProductorCiclo pc = buscadorProductor.busca(inscripcion.getFolio(), true);
            LOGGER.info("El ProductorCiclo existe con {} predios, actualizando...",
                    pc.getCultivos().size());
            pc.setProductor(productor);
            pc.setActivo(true);
            procesaProductorCiclo(inscripcion, pc);
            return pc;
        } catch (EmptyResultDataAccessException ouch) {
            LOGGER.info("El ProductorCiclo no existe, registrando uno nuevo...");
        }
        return registroProductor.registra(productor, inscripcion);
    }

    @Transactional
    @Override
    public void procesa(HistoricoRegistro historico) {
        InscripcionProductor i
                = buscadorInscripcionProductor.buscaElemento(historico.getEntidad().getId());
        switch (EstatusInscripcionEnum.getInstance(i.getEstatus().getClave())) {
            case CORRECTO:
                actualizadorEstatusHelper.cambiaEstatusInscripcion(i, EstatusInscripcionEnum.POSITIVA);
                registroEntidad.actualiza(i);

                Productor p = procesa(i.getDatosProductor());
                procesa(p, i);
                procesadorSociedad.procesa(i);
                break;
        }
        procesadorHistoricoRegistro.finaliza(historico, historico.getUsuarioRegistra());
    }

    @Transactional
    @Override
    public void procesaGrupos(ProductorCiclo pc) {
        pc = buscadorProductor.buscaElemento(pc.getFolio());
        if (!pc.getGrupos().isEmpty()) {
            LOGGER.info("Ya cuenta con ProductorCicloGrupo.");
            return;
        }
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setFolio(pc.getFolio());
        parametros.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.POSITIVA.getClave()));
        InscripcionProductor ip = buscadorInscripcionProductor.buscaElemento(parametros);
        List<ProductorCicloGrupo> grupos = productorCicloGrupoFactory.getInstance(ip, pc);
        for (ProductorCicloGrupo pcg : grupos) {
            registroEntidad.guarda(pcg);
        }
    }

    private void procesaProductorCiclo(InscripcionProductor inscripcion, ProductorCiclo pc) {
        CalculoTotalesHelper.calcula(pc, inscripcion);

        // Calculamos totales por ciclo, cultivo y estado
        List<ProductorCicloCultivo> nuevos = ProductorCicloCultivoFactory.getInstance(inscripcion, pc);
        CalculoTotalesHelper.calculaPredios(pc, nuevos);
        for (ProductorCicloCultivo pcc : pc.getCultivos()) {
            registroEntidad.guardaActualiza(pcc);
        }
        // Calculamos totales por grupo
        List<ProductorCicloGrupo> grupos = productorCicloGrupoFactory.getInstance(inscripcion, pc);
        CalculoTotalesHelper.calculaGrupos(pc, grupos);
        String tipoProductor = inscripcion.getTipoProductor().getClave();
        if (tipoProductor.equals(TipoProductorEnum.PEQUENO_NO_EMPADRONADO.getClave())) {
            tipoProductor = TipoProductorEnum.PEQUENO.getClave();
        }
        for (ProductorCicloGrupo grupo : pc.getGrupos()) {
            String clave = inscripcion.getCultivo().getClave() + ":"
                    + inscripcion.getCiclo().getClave() + ":grupo-estimulo-maximo:" + tipoProductor + ":" + grupo.getGrupoEstimulo();
            grupo.setLimite(buscadorParametro.getValorOpcional(clave, BigDecimal.class));
            registroEntidad.guardaActualiza(grupo);
        }

        registroEntidad.actualiza(pc);
    }

    @Override
    @Transactional
    public void actualizaProductorCiclo(InscripcionProductor inscripcion, ProductorCiclo pc) {
        Map<String, BigDecimal> facturados = getFacturadas(pc);
        procesaProductorCiclo(inscripcion, pc);

        for (ProductorCicloCultivo pcc : pc.getCultivos()) {
            BigDecimal facturadas = facturados.get(pc.getClass().getSimpleName() + ":" + pcc.getId());
            if (Objects.nonNull(facturadas)) {
                pcc.setToneladasFacturadas(facturadas);
            }
            registroEntidad.guardaActualiza(pcc);
        }

        for (ProductorCicloGrupo grupo : pc.getGrupos()) {
            BigDecimal facturadas = facturados.get(grupo.getClass().getSimpleName() + ":" + grupo.getId());
            if (Objects.nonNull(facturadas)) {
                grupo.setToneladasFacturadas(facturadas);
            }
            if (!inscripcion.getEstatus().getClave().equals(EstatusInscripcionEnum.POSITIVA.getClave())) {
                grupo.setToneladasTotales(BigDecimal.ZERO);
                grupo.setToneladasCobertura(BigDecimal.ZERO);
                grupo.setToneladasContratadas(BigDecimal.ZERO);
            }

            registroEntidad.guardaActualiza(grupo);
        }

        pc.setToneladasFacturadas(facturados.get(pc.getClass().getSimpleName() + ":" + pc.getId()));

        if (!inscripcion.getEstatus().getClave().equals(EstatusInscripcionEnum.POSITIVA.getClave())) {
            pc.setHectareasTotales(BigDecimal.ZERO);
            pc.setToneladasTotales(BigDecimal.ZERO);
            pc.setToneladasCobertura(BigDecimal.ZERO);
            pc.setToneladasContratadas(BigDecimal.ONE);
            pc.setActivo(false);
        }

        registroEntidad.actualiza(pc);

    }

    private Map<String, BigDecimal> getFacturadas(ProductorCiclo pc) {
        Map<String, BigDecimal> facturadas = new HashMap<>();
        facturadas.put(pc.getClass().getSimpleName() + ":" + pc.getId(), pc.getToneladasFacturadas());

        for (ProductorCicloGrupo pcg : pc.getGrupos()) {
            facturadas.put(pcg.getClass().getSimpleName() + ":" + pcg.getId(), pcg.getToneladasFacturadas());
        }

        for (ProductorCicloCultivo pcc : pc.getCultivos()) {
            facturadas.put(pcc.getClass().getSimpleName() + ":" + pcc.getId(), pcc.getToneladasFacturadas());
        }
        return facturadas;
    }

}
