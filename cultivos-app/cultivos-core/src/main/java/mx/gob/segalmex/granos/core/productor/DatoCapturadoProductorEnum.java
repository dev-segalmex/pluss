/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor;

import mx.gob.segalmex.granos.core.inscripcion.DatoCapturadoEnum;

/**
 *
 * @author ismael
 */
public enum DatoCapturadoProductorEnum implements DatoCapturadoEnum {
    /**
     * El tipo de cultivo.
     */
    ETIQUETA_TIPO_CULTIVO("etiqueta-tipo-cultivo", "Tipo cultivo"),

    NOMBRE_SOCIEDAD("nombre-sociedad", "Nombre sociedad"),
    RFC_SOCIEDAD("rfc-sociedad", "RFC sociedad"),
    CURP_REPRESENTANTE("curp-representante", "CURP representante"),
    NOMBRE_REPRESENTANTE("nombre-representante", "Nombre representante"),
    PAPELLIDO_REPRESENANTE("papellido-representante", "Primer apellido representante"),
    SAPELLIDO_REPRESENTANTE("sapellido-representante", "Segundo apellido representante"),
    TIPO_IDENTIFICACION_REPRESENTANTE("tipo-identificacion-representante", "Tipo identificación representante"),
    NUMERO_IDENTIFICACION_REPRESENTANTE("numero-identificacion-representante", "Número identificación representante"),

    CURP_PRODUCTOR("curp-productor", "CURP productor"),
    RFC_PRODUCTOR("rfc-productor", "RFC productor"),
    NOMBRE_PRODUCTOR("nombre-productor", "Nombre productor"),
    PAPELLIDO_PRODUCTOR("papellido-productor", "Primer apellido productor"),
    SAPELLIDO_PRODUCTOR("sapellido-productor", "Segundo apellido productor"),
    TIPO_IDENTIFICACION_PRODUCTOR("tipo-identificacion-productor", "Tipo identificación productor"),
    NUMERO_IDENTIFICACION_PRODUCTOR("numero-identificacion-productor", "Número identificación productor"),

    NUMERO_TELEFONO("numero-telefono", "Número de teléfono / celular"),
    CORREO_ELECTRONICO("correo-electronico", "Correo electrónico"),

    CALLE_DOMICILIO("calle-domicilio", "Calle"),
    NUMERO_EXTERIOR_DOMICILIO("numero-exterior-domicilio", "Número exterior"),
    NUMERO_INTERIOR_DOMICILIO("numero-interior-domicilio", "Número interior"),
    LOCALIDAD_DOMICILIO("localidad-domicilio", "Localidad"),
    CODIGO_POSTAL_DOMICILIO("codigo-postal-domicilio", "Código postal"),
    ESTADO_DOMICILIO("estado-domicilio", "Estado"),
    MUNICIPIO_DOMICILIO("municipio-domicilio", "Municipio"),

    NO_CONTRATO("numero-contrato", "No. de contrato"),
    CANTIDAD_CONTRATADA("cantidad-contratada", "Cantidad contratada (t)"),
    CANTIDAD_CONTRATADA_COBERTURA("cantidad-contratada-cobertura", "Cantidad contratada con cobertura (t)"),

    FOLIO_PREDIO("folio-predio", "Folio predio"),
    TIPO_CULTIVO_PREDIO("tipo-cultivo-predio", "Tipo cultivo predio"),
    TIPO_POSESION_PREDIO("tipo-posesion-predio", "Tipo posesión predio"),
    DOCUMENTO_POSESION_PREDIO("documento-posesion-predio", "Documento posesión predio"),
    REGIMEN_HIDRICO_PREDIO("regimen-hidrico-predio", "Régimen hídrico predio"),
    VOLUMEN_PREDIO("volumen-obtenido", "Volumen predio"),
    SUPERFICIE_PREDIO("superficie-predio", "Superficie predio"),
    RENDIMIENTO_PREDIO("rendimiento", "Rendimiento predio"),
    ESTADO_PREDIO("estado-predio", "Estado predio"),
    MUNICIPIO_PREDIO("municipio-predio", "Municipio predio"),
    LOCALIDAD_PREDIO("localidad-predio", "Localidad predio"),

    LATITUD_PREDIO("latitud-predio", "Latitud punto medio"),
    LONGITUD_PREDIO("longitud-predio", "Longitud punto medio"),

    LATITUD1_PREDIO("latitud-1-predio", "Latitud 1 polígono"),
    LONGITUD1_PREDIO("longitud-1-predio", "Longitud 1 polígono"),
    LATITUD2_PREDIO("latitud-2-predio", "Latitud 2 polígono"),
    LONGITUD2_PREDIO("longitud-2-predio", "Longitud 2 polígono"),
    LATITUD3_PREDIO("latitud-3-predio", "Latitud 3 polígono"),
    LONGITUD3_PREDIO("longitud-3-predio", "Longitud 3 polígono"),
    LATITUD4_PREDIO("latitud-4-predio", "Latitud 4 polígono"),
    LONGITUD4_PREDIO("longitud-4-predio", "Longitud 4 polígono"),

    DOCUMENTACION_PREDIO_PDF("documentacion-predio-pdf", "Documentación predio (PDF)"),
    PERMISO_SIEMBRA_XML("permiso-siembra-xml-tmp", "Permiso de siembra (XML)"),
    PAGO_RIEGO_XML("pago-riego-xml-tmp", "Pago de riego (XML)"),

    BANCO_PRODUCTOR("banco-productor", "Banco productor"),
    CLABE_PRODUCTOR("clabe-productor", "CLABE productor"),

    RFC_EMPRESA_ASOCIADA("factura-rfc-sociedad", "RFC de la sociedad"),
    ACTA_CONSTITUTIVA_SOCIEDAD_PDF("acta-constitutiva-sociedad-pdf", "Acta constitutiva sociedad (PDF)"),
    LISTA_SOCIOS_PDF("lista-socios-pdf", "Lista de socios (PDF)"),

    NOMBRE_BENEFICIARIO("nombre-beneficiario", "Nombre beneficiario"),
    APELLIDOS_BENEFICIARIO("apellidos-beneficiario", "Apellidos beneficiario"),
    PARENTESCO_BENEFICIARIO("parentesco-beneficiario", "Parentesco beneficiario"),
    CURP_BENEFICIARIO("curp-beneficiario", "CURP beneficiario"),

    SNICS("snics","No. de folio de registro en el DPOCS"),
    TONELADAS_SNICS("toneladas-snics","Cantidad (Toneladas)"),
    FOLIOS_SNICS("folios-snics","No. de folios"),
    RAZON_SOCIAL_SNICS("razon-social-snics","Nombre y razon sociaL"),
    RFC_SNICS("rfc-snics","RFC"),
    CONSTANCIA_SNICS("constancia-snics-pdf","Constancia semilla certificada SNICS (PDF)"),

    ACTA_CONSTITUTIVA_PDF("acta-constitutiva-pdf", "Acta constitutiva productor (PDF)"),
    CURP_REPRESENTANTE_PDF("curp-representante-pdf", "CURP representante (PDF)"),
    DOCUMENTO_IDENTIFICACION_REPRESENTANTE_PDF("documento-identificacion-representante-pdf", "Documento identificación representante (PDF)"),

    CURP_PRODUCTOR_PDF("curp-productor-pdf", "CURP productor (PDF)"),
    RFC_PRODUCTOR_PDF("rfc-productor-pdf", "RFC productor (PDF)"),
    DOCUMENTO_IDENTIFICACION_PRODUCTOR_PDF("documento-identificacion-productor-pdf", "Documento identificación (PDF)"),

    REFERENCIA_PRODUCTIVA_PDF("referencia-productiva-pdf", "Anexo 1 / Carta del productor (PDF)"),
    DATOS_BANCARIOS_PDF("datos-bancarios-pdf", "Datos bancarios (PDF)"),
    INSCRIPCION_PRODUCTOR_PDF("inscripcion-productor-pdf", "Registro productor (PDF)"),
    CARTA_ACUERDO_PDF("carta-acuerdo-pdf", "Carta acuerdo de distribución de superficie (PDF)"),

    //Documento permiso de rendimiento superior
    DOCUMENTO_PERMISO_RENDIMIENTO_SUPERIOR_PDF("documentacion-rendimiento-solicitado-pdf", "Documento permiso de rendimiento superior (PDF)");

    /**
     * La clave del dato capturado.
     */
    private final String clave;

    /**
     * El nombre del datos capturado.
     */
    private final String nombre;

    private DatoCapturadoProductorEnum(String clave, String nombre) {
        this.clave = clave;
        this.nombre = nombre;
    }

    @Override
    public String getClave() {
        return clave;
    }

    @Override
    public String getNombre() {
        return nombre;
    }

}
