/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.pago;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.archivos.ArchivoUtils;
import mx.gob.segalmex.common.core.archivos.RegistroArchivoRelacionado;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.pago.busqueda.BuscadorRequerimientoPago;
import mx.gob.segalmex.common.core.pago.busqueda.ParametrosRequerimientoPago;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import mx.gob.segalmex.pluss.modelo.archivos.ArchivoRelacionado;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import mx.gob.segalmex.pluss.modelo.factura.EstatusUsoFactura;
import mx.gob.segalmex.pluss.modelo.factura.EstatusUsoFacturaEnum;
import mx.gob.segalmex.pluss.modelo.factura.Factura;
import mx.gob.segalmex.pluss.modelo.factura.UsoFactura;
import mx.gob.segalmex.pluss.modelo.pago.EstatusRequerimientoPagoEnum;
import mx.gob.segalmex.pluss.modelo.pago.RequerimientoPago;
import mx.gob.segalmex.pluss.modelo.pago.RequerimientoUsoFactura;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.granos.core.requerimiento.xls.RequerimientoPagoXls;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ismael
 */
@Service("procesadorRequerimientoPago")
@Slf4j
public class ProcesadorRequerimientoPagoJpa implements ProcesadorRequerimientoPago {

    private static final String CLAVE_USUARIO = "anonimo@segalmex.gob.mx";

    private static final String UNIDAD_OPERATIVA = "unidad-operativa";

    private static final String CUENTA_CARGO = "cuenta-cargo";

    private static final String AREA_PG = "area-pg";

    private static final String USUARIO = "usuario";

    private static final String PROGRAMA = "programa";

    private static final String REPORTE_PENDIENTE = "--";

    private static final String URL_SERVICIO_CEGAP = "cultivos:servicio:cegap.url";

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private BuscadorRequerimientoPago buscadorRequerimientoPago;

    @Autowired
    private EnvioRequerimientoPagoHelper envioRequerimientoPagoHelper;

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private RequestSender requestSender;

    @Autowired
    private RequerimientoPagoXls requerimientoPagoXls;

    @Autowired
    private RegistroArchivoRelacionado registroArchivoRelacionado;

    @Transactional
    @Override
    public RequerimientoPago procesa(List<UsoFactura> usos, Usuario usuario, RequerimientoPago rp) {
        RequerimientoPago r = getInstance(usos, usuario, rp);
        existe(r);
        registroEntidad.guarda(r);

        for (RequerimientoUsoFactura ruf : r.getRequerimientos()) {
            // Guardamos la relación
            registroEntidad.guarda(ruf);
            // Actualizamos el estatus
            registroEntidad.actualiza(ruf.getUsoFactura());
        }

        return r;
    }

    @Transactional
    @Override
    public RequerimientoPago actualizaTotales(RequerimientoPago requerimiento) {
        requerimiento = buscadorRequerimientoPago.buscaElemento(requerimiento.getUuid());
        calculaTotales(requerimiento);
        return requerimiento;
    }

    private void existe(RequerimientoPago r) {
        ParametrosRequerimientoPago parametros = new ParametrosRequerimientoPago();
        parametros.setFechaInicio(DateUtils.truncate(r.getFechaCreacion(), Calendar.DATE));
        parametros.setFechaFin(DateUtils.truncate(r.getFechaCreacion(), Calendar.DATE));
        parametros.getFechaFin().add(Calendar.DATE, 1);
        parametros.setCultivo(r.getCultivo());

        List<RequerimientoPago> requerimientos = buscadorRequerimientoPago.busca(parametros);
        if (!requerimientos.isEmpty()) {
            throw new SegalmexRuntimeException("Error al registrar el requerimiento.",
                    "Ya existe un requerimiento de pago en la misma fecha para "
                    + r.getCultivo().getNombre() + ".");
        }
    }

    @Override
    public RequerimientoPago getInstance(List<UsoFactura> usos, Usuario usuario, RequerimientoPago rp) {
        Objects.requireNonNull(usos, "Los usos de factura no pueden ser nulos.");
        Objects.requireNonNull(usuario, "El usuario no puede ser nulo.");
        if (usos.isEmpty()) {
            throw new IllegalArgumentException("La lista de usos no puede ser vacía.");
        }
        EstatusUsoFactura estatus = buscadorCatalogo.buscaElemento(EstatusUsoFactura.class,
                EstatusUsoFacturaEnum.SELECCIONADO);
        validaEstatusUsoFactura(usos, estatus);

        RequerimientoPago r = new RequerimientoPago();
        r.setFechaCreacion(Calendar.getInstance());
        r.setUuid(UUID.randomUUID().toString());
        r.setUsuario(usuario);
        r.setEstatus(EstatusRequerimientoPagoEnum.NUEVO.getClave());
        r.setFechaEnvio(Calendar.getInstance());
        r.setCultivo(rp.getCultivo());
        r.setCiclo(rp.getCiclo());
        r.setLayout(rp.getLayout());
        String cp = r.getCultivo().getClave() + ":requerimiento-pago:";
        r.setClaveUnidadOperativa(buscadorParametro.getValorOpcional(cp + UNIDAD_OPERATIVA, String.class));
        r.setNumeroCuenta(buscadorParametro.getValorOpcional(cp + CUENTA_CARGO, String.class));
        r.setClaveArea(buscadorParametro.getValorOpcional(cp + AREA_PG, String.class));
        r.setClaveCentroAcopio(buscadorParametro.getValorOpcional(cp + AREA_PG, String.class));
        r.setClaveUsuario(buscadorParametro.getValorOpcional(cp + USUARIO, String.class));
        r.setPrograma(buscadorParametro.getValorOpcional(cp + PROGRAMA, String.class));
        r.setMonitoreable("--");
        r.setReporteGenerado(REPORTE_PENDIENTE);

        r.setRequerimientos(usos.stream()
                .map(uf -> getInstance(uf, r))
                .collect(Collectors.toList()));
        calculaTotales(r);

        return r;
    }

    private RequerimientoUsoFactura getInstance(UsoFactura u, RequerimientoPago r) {
        RequerimientoUsoFactura ufr = new RequerimientoUsoFactura();
        ufr.setRequerimiento(r);
        ufr.setUsoFactura(u);
        return ufr;
    }

    @Override
    public void calculaTotales(RequerimientoPago requerimiento) {
        // Totales generales
        BigDecimal toneladasTotales = BigDecimal.ZERO;
        BigDecimal montoTotal = BigDecimal.ZERO;
        // Totales no pagados
        BigDecimal toneladasNoPago = BigDecimal.ZERO;
        BigDecimal montoTotalNoPago = BigDecimal.ZERO;
        int totalNoPagado = 0;

        for (RequerimientoUsoFactura ruf : requerimiento.getRequerimientos()) {
            UsoFactura u = ruf.getUsoFactura();
            toneladasTotales = toneladasTotales.add(u.getToneladas());
            montoTotal = montoTotal.add(u.getEstimuloTotal());
            if (u.getEstatus().getFactor().compareTo(BigDecimal.ZERO) <= 0) {
                toneladasNoPago = toneladasNoPago.add(u.getToneladas());
                montoTotalNoPago = montoTotalNoPago.add(u.getEstimuloTotal());
                totalNoPagado++;
            }
        }

        requerimiento.setMontoTotal(montoTotal);
        requerimiento.setToneladasTotales(toneladasTotales);
        requerimiento.setFacturas(requerimiento.getRequerimientos().size());
        requerimiento.setMontoTotalNoPago(montoTotalNoPago);
        requerimiento.setToneladasNoPago(toneladasNoPago);
        requerimiento.setFacturasNoPago(totalNoPagado);
    }

    private List<UsoFactura> validaEstatusUsoFactura(List<UsoFactura> usosFactura, EstatusUsoFactura estatus) {
        for (UsoFactura uso : usosFactura) {
            switch (EstatusUsoFacturaEnum.getInstance(uso.getEstatus().getClave())) {
                case NUEVO:
                    uso.setEstatus(estatus);
                    break;
                default:
                    throw new SegalmexRuntimeException("Error al validar.",
                            "La Factura se encuentra en estatus: " + uso.getEstatus());
            }
        }
        return usosFactura;
    }

    @Override
    public void envia(RequerimientoPago requerimiento) {
        Parametro parametro = buscadorParametro.buscaElemento(URL_SERVICIO_CEGAP);
        String url = parametro.getValor();
        Objects.requireNonNull(url, "La URL del servicio no puede ser nula.");
        JSONObject json = envioRequerimientoPagoHelper.generaJson(requerimiento);

        // Una vez generado el json, lo marcamos como no monitoreable
        requerimiento.setMonitoreable(SegalmexDateUtils.format(Calendar.getInstance(), "yyyy-MM-dd"));
        requerimiento.setReporteGenerado(REPORTE_PENDIENTE);
        registroEntidad.actualiza(requerimiento);

        // Envíamos realmente el registro (intentamos solo una vez automáticamente)
        String response = null;
        try {
            response = requestSender.send(url, json.toString());
            LOGGER.info("Response de envío a CEGAP: {}", response);
        } catch (RuntimeException ouch) {
            LOGGER.error("Error al intentar enviar el requerimiento.", ouch);
        }

        // En caso de que se genere el json, obtenemos el CEGAP, marcamos como enviado todo
        envioRequerimientoPagoHelper.marcaEnvio(requerimiento, response);
    }

    @Override
    @Transactional
    public void aprueba(RequerimientoPago requerimiento) {
        LOGGER.info("Aprobando requerimiento: {}", requerimiento.getUuid());
        if (!requerimiento.getEstatus().equals(EstatusRequerimientoPagoEnum.NUEVO.getClave())) {
            LOGGER.error("El requerimiento no se puede aprobar, esta en estatus: {}", requerimiento.getEstatus());
            throw new SegalmexRuntimeException("Error al aprobar.",
                    "El requerimiento se encuentra en un estatus incorrecto para ser aprobado.");
        }
        requerimiento.setEstatus(EstatusRequerimientoPagoEnum.APROBADO.getClave());
        requerimiento.setReporteGenerado(REPORTE_PENDIENTE);
        registroEntidad.actualiza(requerimiento);
    }

    @Override
    @Transactional
    public void marcaPago(RequerimientoPago requerimiento) {
        LOGGER.info("Actualizando estatus pagado para el requerimiento: {}", requerimiento.getUuid());
        if (!requerimiento.getEstatus().equals(EstatusRequerimientoPagoEnum.ENVIADO.getClave())) {
            LOGGER.error("El requerimiento no se puede actualizar como pagado, esta en estatus: {}", requerimiento.getEstatus());
            throw new SegalmexRuntimeException("Error al pagar.",
                    "El requerimiento se encuentra en un estatus incorrecto para ser pagado.");
        }
        requerimiento.setEstatus(EstatusRequerimientoPagoEnum.PAGADO.getClave());
        EstatusUsoFactura pagado = buscadorCatalogo.buscaElemento(EstatusUsoFactura.class,
                EstatusUsoFacturaEnum.PAGADO);
        requerimiento.getRequerimientos().stream()
                .filter(ruf -> ruf.getUsoFactura().getEstatus().getClave().equals(EstatusUsoFacturaEnum.EN_PROCESO.getClave()))
                .map(RequerimientoUsoFactura::getUsoFactura)
                .forEach(u -> {
                    u.setEstatus(pagado);
                    registroEntidad.actualiza(u);
                });
        registroEntidad.actualiza(requerimiento);
    }

    @Override
    @Transactional
    public void cancela(RequerimientoPago requerimiento) {
        switch (EstatusRequerimientoPagoEnum.getInstance(requerimiento.getEstatus())) {
            case APROBADO:
            case NUEVO:
                break;
            default:
                throw new SegalmexRuntimeException("Error al cancelar.",
                        "El requerimiento se encuentra en un estatus incorrecto para ser cancelado.");
        }
        requerimiento.setEstatus(EstatusRequerimientoPagoEnum.CANCELADO.getClave());
        EstatusUsoFactura nuevo = buscadorCatalogo.buscaElemento(EstatusUsoFactura.class,
                EstatusUsoFacturaEnum.NUEVO);
        for (RequerimientoUsoFactura ru : requerimiento.getRequerimientos()) {
            ru.getUsoFactura().setEstatus(nuevo);
            registroEntidad.actualiza(ru.getUsoFactura());
        }
        eliminaUsosFactura(requerimiento);
        registroEntidad.actualiza(requerimiento);
    }

    private void eliminaUsosFactura(RequerimientoPago requerimiento) {
        entityManager
                .createQuery("DELETE FROM RequerimientoUsoFactura ruf WHERE ruf.requerimiento = :requerimiento")
                .setParameter("requerimiento", requerimiento)
                .executeUpdate();
    }

    @Override
    @Transactional
    public void generaXlsx(RequerimientoPago r) throws IOException {
        r = buscadorRequerimientoPago.buscaElemento(r.getUuid());
        byte[] row = requerimientoPagoXls.exporta(r);
        InputStream is = new ByteArrayInputStream(row);
        ArchivoRelacionado ar = getArchivoRelacionado(r);
        registroArchivoRelacionado.registra(ar.getArchivo(), ar, is);
        r.setReporteGenerado(SegalmexDateUtils.format(Calendar.getInstance(), "yyyy-MM-dd HH:mm:ss"));
        registroEntidad.actualiza(r);
        entityManager.flush();
        entityManager.clear();
    }

    private ArchivoRelacionado getArchivoRelacionado(RequerimientoPago r) {
        ArchivoRelacionado ar = new ArchivoRelacionado();
        String fecha = SegalmexDateUtils.format(r.getFechaCreacion(), "yyyyMMdd");
        String nombre = "requerimiento-pago" + "-" + fecha + ".xlsx";
        Archivo archivo = ArchivoUtils.getInstance(nombre,
                "cultivos", "/requerimientos-pago/reportes/" + fecha, false);
        ar.setArchivo(archivo);
        ar.setUsuarioRegistra(buscadorCatalogo.buscaElemento(Usuario.class, CLAVE_USUARIO));
        ar.setClase(r.getClass().getSimpleName());
        ar.setReferencia(r.getUuid());
        ar.setTipo("requerimiento-pago");
        return ar;
    }

    @Override
    @Transactional
    public void cancela(RequerimientoPago requerimiento, UsoFactura uf,
            EstatusUsoFacturaEnum estatus, String comentario) {
        LOGGER.info("Cambiando a {} el uso factura: {}", estatus.getClave(), uf.getUuid());
        if (!uf.getEstatus().getClave().equals(EstatusUsoFacturaEnum.EN_PROCESO.getClave())) {
            throw new SegalmexRuntimeException("Error al marcar como no pagado.",
                    "El pago se encuentra en un estatus incorrecto para realizar esta acción.");
        }
        Factura factura = uf.getFactura();
        factura.setToneladasDisponibles(factura.getToneladasDisponibles().add(uf.getToneladas()));
        registroEntidad.actualiza(factura);
        EstatusUsoFactura euf = buscadorCatalogo.buscaElemento(EstatusUsoFactura.class, estatus);
        uf.setEstatus(euf);
        uf.setComentarioEstatus(StringUtils.trimToNull(comentario));
        registroEntidad.actualiza(uf);
        requerimiento.setFacturas(requerimiento.getFacturas() - 1);
        requerimiento.setFacturasNoPago(requerimiento.getFacturasNoPago() + 1);
        requerimiento.setToneladasTotales(requerimiento.getToneladasTotales().subtract(uf.getToneladas()));
        requerimiento.setMontoTotal(requerimiento.getMontoTotal().subtract(uf.getEstimuloTotal()));
        requerimiento.setToneladasNoPago(requerimiento.getToneladasNoPago().add(uf.getToneladas()));
        requerimiento.setMontoTotalNoPago(requerimiento.getMontoTotalNoPago().add(uf.getEstimuloTotal()));
        requerimiento.setReporteGenerado(REPORTE_PENDIENTE);
        registroEntidad.actualiza(requerimiento);
    }

}
