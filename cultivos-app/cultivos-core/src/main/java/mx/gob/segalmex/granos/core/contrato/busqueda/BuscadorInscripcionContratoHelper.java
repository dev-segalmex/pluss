/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato.busqueda;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionAgrupada;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Component
public class BuscadorInscripcionContratoHelper {

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorInscripcionContrato buscadorInscripcionContrato;

    public List<InscripcionAgrupada> busca(ParametrosInscripcionContrato parametros) {
        List<Object[]> registros = buscadorInscripcionContrato.buscaAgrupada(parametros);
        List<InscripcionAgrupada> agrupadas = new ArrayList<>();
        if (Objects.nonNull(registros)) {
            for (Object[] obj : registros) {
                agrupadas.add(generaInscripcionAgrupada(obj));
            }
        }
        return agrupadas;
    }

    private InscripcionAgrupada generaInscripcionAgrupada(Object[] obj) {
        InscripcionAgrupada ia = new InscripcionAgrupada();
        ia.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, (Integer) obj[0]));
        ia.setEstado(buscadorCatalogo.buscaElemento(Estado.class, (Integer) obj[1]));
        ia.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, (Integer) obj[2]));
        ia.setTotal((Math.toIntExact((Long) obj[3])));
        return ia;
    }
}
