package mx.gob.segalmex.pluss.cultivos.core;

import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import mx.gob.segalmex.common.core.util.QueryBuilder;
import mx.gob.segalmex.pluss.modelo.catalogos.Localidad;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Repository;

/**
 * @author ismael
 */
@Repository
public class BuscadorLocalidadJpa implements BuscadorLocalidad {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Localidad> busca(String codigoPostal) {
        Objects.requireNonNull(StringUtils.stripToNull(codigoPostal));
        ParametrosLocalidad parametros = new ParametrosLocalidad();
        parametros.setCodigoPostal(codigoPostal);
        return busca(parametros);
    }

    @Override
    public List<Localidad> busca(ParametrosLocalidad parametros) {
        QueryBuilder qb = new QueryBuilder("SELECT l FROM Localidad l");
        qb.andEq("l.municipio.estado", "estado", parametros.getEstado());
        qb.andEq("l.municipio", "municipio", parametros.getMunicipio());
        qb.andEq("l.codigoPostal", "codigoPostal", StringUtils.stripToNull(parametros.getCodigoPostal()));
        return qb.getList(entityManager, Localidad.class);
    }
}
