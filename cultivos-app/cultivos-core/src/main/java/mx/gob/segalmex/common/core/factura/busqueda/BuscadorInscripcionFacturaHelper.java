/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.busqueda;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionAgrupada;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionResumen;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Component
public class BuscadorInscripcionFacturaHelper {

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorInscripcionFactura buscadorInscripcionFactura;

    public List<InscripcionAgrupada> busca(ParametrosInscripcionFactura parametros) {
        List<Object[]> registros = buscadorInscripcionFactura.buscaAgrupada(parametros);
        List<InscripcionAgrupada> agrupadas = new ArrayList<>();
        if (Objects.nonNull(registros)) {
            for (Object[] obj : registros) {
                agrupadas.add(generaInscripcionAgrupada(obj));
            }
        }
        return agrupadas;
    }

    private InscripcionAgrupada generaInscripcionAgrupada(Object[] obj) {
        InscripcionAgrupada ia = new InscripcionAgrupada();
        ia.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, (Integer) obj[0]));
        Integer idEstado = (Integer) obj[1];
        ia.setEstado(Objects.nonNull(idEstado)
                ? buscadorCatalogo.buscaElemento(Estado.class, idEstado) : null);
        ia.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, (Integer) obj[2]));
        ia.setTotal((Math.toIntExact((Long) obj[3])));
        return ia;
    }

    public List<InscripcionResumen> buscaResumen(ParametrosInscripcionFactura parametros) {
        List<Object[]> registros = buscadorInscripcionFactura.buscaResumen(parametros);
        List<InscripcionResumen> resumen = new ArrayList<>();
        if (Objects.nonNull(registros)) {
            for (Object[] obj : registros) {
                resumen.add(generaResumen(obj));
            }
        }
        return resumen;
    }

    private InscripcionResumen generaResumen(Object[] obj) {
        InscripcionResumen ir = new InscripcionResumen();
        ir.setCultivo(String.valueOf(obj[0]));
        ir.setEstatus(String.valueOf(obj[1]));
        ir.setCiclo(String.valueOf(obj[2]));
        ir.setTipo(String.valueOf(obj[3]));
        ir.setTotal((Math.toIntExact((Long) obj[4])));
        return ir;
    }

}
