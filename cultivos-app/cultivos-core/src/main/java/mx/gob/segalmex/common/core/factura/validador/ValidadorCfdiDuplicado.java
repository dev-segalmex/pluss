/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.validador;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorInscripcionFactura;
import mx.gob.segalmex.common.core.factura.busqueda.ParametrosInscripcionFactura;
import mx.gob.segalmex.pluss.modelo.factura.Cfdi;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component("validadorCfdiDuplicado")
@Slf4j
public class ValidadorCfdiDuplicado implements ValidadorCfdi {

    @Autowired
    private BuscadorInscripcionFactura buscadorInscripcionFactura;

    @Override
    public boolean valida(InscripcionFactura inscripcion) {
        Cfdi cfdi = inscripcion.getCfdi();
        ParametrosInscripcionFactura parametros = new ParametrosInscripcionFactura();
        parametros.setUuidTimbreFiscalDigital(cfdi.getUuidTimbreFiscalDigital());
        List<InscripcionFactura> inscripciones = buscadorInscripcionFactura.busca(parametros);

        boolean duplicada = false;
        for (InscripcionFactura i : inscripciones) {
            // Si el ID coincide, no había una factura previa con el mismo UUID
            if (i.getId().equals(inscripcion.getId())) {
                break;
            }
            LOGGER.warn("La inscripcion {} usa una factura ya registrada por la inscripcion: {}",
                    inscripcion.getFolio(), i.getFolio());
            duplicada = true;
        }
        inscripcion.setDuplicada(duplicada);
        return !inscripcion.getDuplicada();
    }

}
