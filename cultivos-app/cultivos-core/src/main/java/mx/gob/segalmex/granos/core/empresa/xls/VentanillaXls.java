/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.empresa.xls;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import mx.gob.segalmex.common.core.util.XlsUtils;
import mx.gob.segalmex.pluss.modelo.empresas.DatosComercializacion;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroEmpresa;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroSucursal;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Component("ventanillaXls")
public class VentanillaXls {

    public byte[] exporta(List<RegistroEmpresa> registros) {
        SXSSFWorkbook wb = new SXSSFWorkbook(100);
        CreationHelper ch = wb.getCreationHelper();
        Sheet s = wb.createSheet();

        String[] encabezados = new String[]{
            "No.",
            "Fecha de registro",
            "RFC",
            "Nombre",
            "Tipo",
            "Nombre del responsable",
            "Teléfono oficina",
            "Extensión/Teléfono adicional",
            "Calle",
            "Número exterior",
            "Número interior",
            "Localidad",
            "Código postal",
            "Estado",
            "Municipio",
            "Nombre del enlace",
            "No. de celular enlace",
            "Correo electrónico enlace",
            "¿Existe equipo de análisis en operación?",
            "¿Aplica normas de calidad?",
            "Tipo de posesión",
            "Tipo de almacenamiento",
            "Latitud",
            "longitud",
            "Número de almacenes",
            "Capacidad instalada",
            "Volumen almacenado actual",
            "Superficie de la instalación",
            "¿Está habilitada por alguna almacenadora?",
            "Nombre de la almacenadora",
            "Actores Compra",
            "Total compra 2018",
            "Total compra 2019",
            "Actores Venta",
            "Total Venta 2018",
            "Total Venta 2019",
            "No. sucursal",
            "Nombre",
            "Dirección",
            "Estado",
            "Municipio",
            "Localidad",
            "Estatus",
            "Validador"
        };
        XlsUtils.creaEncabezados(s, encabezados);

        CellStyle cs = wb.createCellStyle();
        cs.setDataFormat(ch.createDataFormat().getFormat("dd/MM/yyyy HH:mm"));

        int renglon = 1;
        int index = 1;
        for (RegistroEmpresa registro : registros) {
            int noSucursal = 1;
            if (registro.getSucursales().isEmpty()) {
                Row r = s.createRow(renglon);
                int j = 0;
                XlsUtils.createCell(r, j++, index);
                XlsUtils.createCell(r, j++, registro.getFechaCreacion(), cs);
                XlsUtils.createCell(r, j++, registro.getRfc());
                XlsUtils.createCell(r, j++, registro.getNombre());
                XlsUtils.createCell(r, j++, registro.getTipoSucursal().getNombre());
                XlsUtils.createCell(r, j++, registro.getNombreResponsable());
                XlsUtils.createCell(r, j++, registro.getTelefonoOficina());
                XlsUtils.createCell(r, j++, registro.getExtensionAdicional());
                XlsUtils.createCell(r, j++, registro.getDomicilio().getCalle());
                XlsUtils.createCell(r, j++, registro.getDomicilio().getNumeroExterior());
                XlsUtils.createCell(r, j++, registro.getDomicilio().getNumeroInterior());
                XlsUtils.createCell(r, j++, registro.getDomicilio().getLocalidad());
                XlsUtils.createCell(r, j++, registro.getDomicilio().getCodigoPostal());
                XlsUtils.createCell(r, j++, registro.getDomicilio().getEstado().getNombre());
                XlsUtils.createCell(r, j++, registro.getDomicilio().getMunicipio().getNombre());
                XlsUtils.createCell(r, j++, registro.getNombreEnlace());
                XlsUtils.createCell(r, j++, registro.getNumeroCelularEnlace());
                XlsUtils.createCell(r, j++, registro.getCorreoEnlace());
                XlsUtils.createCell(r, j++, registro.isExisteEquipoAnalisis() ? "Si" : "No");
                XlsUtils.createCell(r, j++, registro.isAplicaNormasCalidad() ? "Si" : "No");
                XlsUtils.createCell(r, j++, registro.getTipoPosesion().getNombre());
                XlsUtils.createCell(r, j++, registro.getTipoAlmacenamiento());
                XlsUtils.createCell(r, j++, registro.getLatitud());
                XlsUtils.createCell(r, j++, registro.getLongitud());
                XlsUtils.createCell(r, j++, registro.getNumeroAlmacenes());
                XlsUtils.createCell(r, j++, registro.getCapacidad().doubleValue());
                XlsUtils.createCell(r, j++, registro.getVolumenActual().doubleValue());
                XlsUtils.createCell(r, j++, registro.getSuperficie().doubleValue());
                XlsUtils.createCell(r, j++, registro.isHabilitaAlmacenadora() ? "Si" : "No");
                XlsUtils.createCell(r, j++, registro.getNombreAlmacenadora());
                XlsUtils.createCell(r, j++, construyeActores(registro, 2018, "compra"));
                XlsUtils.createCell(r, j++, calculaTotal(registro, 2018, "compra").doubleValue());
                XlsUtils.createCell(r, j++, calculaTotal(registro, 2019, "compra").doubleValue());
                XlsUtils.createCell(r, j++, construyeActores(registro, 2018, "venta"));
                XlsUtils.createCell(r, j++, calculaTotal(registro, 2018, "venta").doubleValue());
                XlsUtils.createCell(r, j++, calculaTotal(registro, 2019, "venta").doubleValue());
                XlsUtils.createCell(r, j++, 0);
                XlsUtils.createCell(r, j++, "--");
                XlsUtils.createCell(r, j++, "--");
                XlsUtils.createCell(r, j++, "--");
                XlsUtils.createCell(r, j++, "--");
                XlsUtils.createCell(r, j++, "--");
                XlsUtils.createCell(r, j++, StringUtils.defaultIfEmpty(registro.getEstatus().getNombre(), "--"));
                XlsUtils.createCell(r, j++, Objects.nonNull(registro.getUsuarioValidador()) ? StringUtils.defaultIfEmpty(registro.getUsuarioValidador().getNombre(), "--") : "--");
                renglon++;
            } else {
                for (RegistroSucursal rs : registro.getSucursales()) {
                    Row r = s.createRow(renglon);
                    int j = 0;
                    XlsUtils.createCell(r, j++, index);
                    XlsUtils.createCell(r, j++, registro.getFechaCreacion(), cs);
                    XlsUtils.createCell(r, j++, registro.getRfc());
                    XlsUtils.createCell(r, j++, registro.getNombre());
                    XlsUtils.createCell(r, j++, registro.getTipoSucursal().getNombre());
                    XlsUtils.createCell(r, j++, registro.getNombreResponsable());
                    XlsUtils.createCell(r, j++, registro.getTelefonoOficina());
                    XlsUtils.createCell(r, j++, registro.getExtensionAdicional());
                    XlsUtils.createCell(r, j++, registro.getDomicilio().getCalle());
                    XlsUtils.createCell(r, j++, registro.getDomicilio().getNumeroExterior());
                    XlsUtils.createCell(r, j++, registro.getDomicilio().getNumeroInterior());
                    XlsUtils.createCell(r, j++, registro.getDomicilio().getLocalidad());
                    XlsUtils.createCell(r, j++, registro.getDomicilio().getCodigoPostal());
                    XlsUtils.createCell(r, j++, registro.getDomicilio().getEstado().getNombre());
                    XlsUtils.createCell(r, j++, registro.getDomicilio().getMunicipio().getNombre());
                    XlsUtils.createCell(r, j++, registro.getNombreEnlace());
                    XlsUtils.createCell(r, j++, registro.getNumeroCelularEnlace());
                    XlsUtils.createCell(r, j++, registro.getCorreoEnlace());
                    XlsUtils.createCell(r, j++, registro.isExisteEquipoAnalisis() ? "Si" : "No");
                    XlsUtils.createCell(r, j++, registro.isAplicaNormasCalidad() ? "Si" : "No");
                    XlsUtils.createCell(r, j++, registro.getTipoPosesion().getNombre());
                    XlsUtils.createCell(r, j++, registro.getTipoAlmacenamiento());
                    XlsUtils.createCell(r, j++, registro.getLatitud());
                    XlsUtils.createCell(r, j++, registro.getLongitud());
                    XlsUtils.createCell(r, j++, registro.getNumeroAlmacenes());
                    XlsUtils.createCell(r, j++, registro.getCapacidad().doubleValue());
                    XlsUtils.createCell(r, j++, registro.getVolumenActual().doubleValue());
                    XlsUtils.createCell(r, j++, registro.getSuperficie().doubleValue());
                    XlsUtils.createCell(r, j++, registro.isHabilitaAlmacenadora() ? "Si" : "No");
                    XlsUtils.createCell(r, j++, registro.getNombreAlmacenadora());
                    XlsUtils.createCell(r, j++, construyeActores(registro, 2018, "compra"));
                    XlsUtils.createCell(r, j++, calculaTotal(registro, 2018, "compra").doubleValue());
                    XlsUtils.createCell(r, j++, calculaTotal(registro, 2019, "compra").doubleValue());
                    XlsUtils.createCell(r, j++, construyeActores(registro, 2018, "venta"));
                    XlsUtils.createCell(r, j++, calculaTotal(registro, 2018, "venta").doubleValue());
                    XlsUtils.createCell(r, j++, calculaTotal(registro, 2019, "venta").doubleValue());
                    XlsUtils.createCell(r, j++, noSucursal++);
                    XlsUtils.createCell(r, j++, StringUtils.defaultIfEmpty(rs.getNombre(), "--"));
                    XlsUtils.createCell(r, j++, StringUtils.defaultIfEmpty(rs.getDomicilio().getCalle() + " " + rs.getDomicilio().getNumeroExterior() + ",C.P." + rs.getDomicilio().getCodigoPostal(), "--"));
                    XlsUtils.createCell(r, j++, StringUtils.defaultIfEmpty(rs.getDomicilio().getEstado().getNombre(), "--"));
                    XlsUtils.createCell(r, j++, StringUtils.defaultIfEmpty(rs.getDomicilio().getMunicipio().getNombre(), "--"));
                    XlsUtils.createCell(r, j++, StringUtils.defaultIfEmpty(rs.getDomicilio().getLocalidad(), "--"));
                    XlsUtils.createCell(r, j++, StringUtils.defaultIfEmpty(registro.getEstatus().getNombre(), "--"));
                    XlsUtils.createCell(r, j++, Objects.nonNull(registro.getUsuarioValidador()) ? StringUtils.defaultIfEmpty(registro.getUsuarioValidador().getNombre(), "--") : "--");
                    renglon++;
                }
                index++;
            }
            index++;
        }
        for (int i = 0; i < encabezados.length; i++) {
            s.autoSizeColumn(i);
        }
        return XlsUtils.creaArchivo(wb);
    }

    private String construyeActores(RegistroEmpresa datos, int anio, String tipo) {
        List<String> nombre = new ArrayList<>();
        String nombres = "";
        for (DatosComercializacion d : datos.getDatosComercializacion()) {
            if (d.getTipo().equals(tipo) && d.getAnio() == anio) {
                nombre.add(d.getNombreActor());
            }
        }
        return nombres.join(",", nombre);
    }

    private BigDecimal calculaTotal(RegistroEmpresa registro, int anio, String tipo) {
        BigDecimal total = BigDecimal.ZERO;
        for (DatosComercializacion d : registro.getDatosComercializacion()) {
            if (d.getAnio() == anio && d.getTipo().equals(tipo)) {
                total = d.getCantidad().add(total);
            }
        }
        return total;
    }
}
