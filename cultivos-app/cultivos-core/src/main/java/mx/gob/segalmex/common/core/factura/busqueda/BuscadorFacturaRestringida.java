/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.factura.FacturaRestringida;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;

/**
 *
 * @author ismael
 */
public interface BuscadorFacturaRestringida {

    boolean existe(String uuid, CicloAgricola ciclo, Cultivo cultivo);

    /**
     * Obtiene una lista de {@link FacturaRestringida} con los parámetros recibidos.
     * @param parametros los parámetros con los que se filtra la búsqueda.
     * @return una lista de {@link FacturaRestringida} encontradas.
     */
    List<FacturaRestringida> busca(ParametrosFacturaRestringida parametros);

    /**
     * Obtiene un solo elemento de {@link FacturaRestringida} dado los parámetros recibidos.
     * @param parametros los parámetros para filtrar la búsqueda.
     * @return la {@link FacturaRestringida} encontrada.
     */
    FacturaRestringida buscaElemento(ParametrosFacturaRestringida parametros);

    /**
     * Obtiene una {@link FacturaRestringida} por su timbre físcal.
     * @param uuidTimbreFiscalDigital el timbre de la {@link FacturaRestringida}.
     * @return la {@link FacturaRestringida} encontrada.
     */
    FacturaRestringida buscaElemento(String uuidTimbreFiscalDigital);

}
