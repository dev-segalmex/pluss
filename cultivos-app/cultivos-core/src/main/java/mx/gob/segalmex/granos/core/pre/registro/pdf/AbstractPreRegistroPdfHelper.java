/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.pre.registro.pdf;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import com.lowagie.text.Rectangle;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Objects;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.granos.core.contrato.pdf.PdfConstants;
import mx.gob.segalmex.granos.core.contrato.pdf.PdfHelper;
import mx.gob.segalmex.granos.core.contrato.pdf.PtConversion;
import mx.gob.segalmex.pluss.modelo.productor.PreRegistroProductor;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author jurgen
 */
public abstract class AbstractPreRegistroPdfHelper {

    protected static final float TAMANO_CONTENIDO_DOCUMENTO
            = PageSize.LETTER.getWidth() - PtConversion.toPt(1.5f) - PtConversion.toPt(1.5f);

    public abstract void generaContenido(PreRegistroProductor inscripcion, Document d, PdfWriter writer)
            throws DocumentException;

    protected PdfPTable generaTablaContenido() {
        PdfPTable table = new PdfPTable(1);
        table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        table.getDefaultCell().setPadding(0);
        table.setTotalWidth(TAMANO_CONTENIDO_DOCUMENTO);
        table.setLockedWidth(true);
        return table;
    }

    protected String getFechaFormat(Calendar fecha) {
        return Objects.nonNull(fecha) ? SegalmexDateUtils.format(fecha) : "--";
    }

    protected void agregaDatosPrograma(PdfPTable contenido, PreRegistroProductor inscripcion) {
        contenido.addCell(generaCeldaEncabezado("Pre registro - Programa de Precios de Garantía a Productos Alimentarios Básicos"));
        contenido.addCell(PdfHelper.generaCeldaVacia(1));

        PdfPTable seccion = generaTabla(2, TAMANO_CONTENIDO_DOCUMENTO);
        seccion.addCell(generaCeldaEncabezado("Folio"));
        seccion.addCell(generaCeldaEncabezado("Fecha de captura"));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, inscripcion.getFolio(), false, false));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, getFechaFormat(inscripcion.getFechaCreacion()), false, false));
        contenido.addCell(seccion);

        seccion = generaTabla(3, TAMANO_CONTENIDO_DOCUMENTO);
        seccion.addCell(generaCeldaEncabezado("Cultivo"));
        seccion.addCell(generaCeldaEncabezado("Ciclo agrícola"));
        seccion.addCell(generaCeldaEncabezado("Tipo"));

        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, inscripcion.getCultivo().getNombre(), false, false));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, inscripcion.getCiclo().getNombre(), false, false));
        seccion.addCell(generaCeldaSimple(StringUtils.EMPTY, inscripcion.getEtiquetaTipoCultivo(), false, false));

        contenido.addCell(seccion);
        contenido.addCell(PdfHelper.generaCeldaVacia(1));
    }

    protected void agregaDatosBeneficiario(PdfPTable contenido, PreRegistroProductor inscripcion) {
        contenido.addCell(generaCeldaEncabezado("Beneficiario"));

        PdfPTable seccion = generaTabla(4, TAMANO_CONTENIDO_DOCUMENTO);
        seccion.addCell(generaCeldaSimple("Nombre", inscripcion.getNombreBeneficiario(), false, false));
        seccion.addCell(generaCeldaSimple("Apellidos", inscripcion.getApellidosBeneficiario(), false, false));
        seccion.addCell(generaCeldaSimple("Parentesco", inscripcion.getParentesco().getNombre(), false, false));
        seccion.addCell(generaCeldaSimple("CURP", inscripcion.getCurpBeneficiario(), false, false));

        contenido.addCell(seccion);
        contenido.addCell(PdfHelper.generaCeldaVacia(1));
    }

    protected String getFormatoNumero(BigDecimal bd) {
        if (Objects.isNull(bd)) {
            return "--";
        }

        NumberFormat nf = new DecimalFormat("#,###,##0.00");
        return nf.format(bd);
    }

    public static PdfPTable generaTabla(int colspan, float width) {
        PdfPTable table = new PdfPTable(colspan);
        table.setTotalWidth(width);
        table.setLockedWidth(true);
        table.getDefaultCell().setBorder(Rectangle.NO_BORDER);
        table.getDefaultCell().setPadding(0);

        return table;
    }

    public static PdfPCell generaCeldaEncabezado(String encabezado) {
        PdfPCell celda = PdfHelper.creaCeldaBackground(encabezado, 1,
                PdfConstants.SOBERANA_LIGHT_8_BLACK, PdfConstants.BACKGROUND_GOBMX_GRAY);
        celda.setBorderColor(PdfConstants.BACKGROUND_GOBMX_DARKGRAY);
        celda.setBorder(Rectangle.BOX);
        celda.setBorderWidth(PdfConstants.CELDA_TAMANO_BORDE);
        celda.setMinimumHeight(PtConversion.toPt(PdfConstants.CELDA_ALTURA_MINIMA_CM));
        celda.setHorizontalAlignment(PdfPCell.ALIGN_CENTER);
        celda.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
        celda.setPadding(PtConversion.toPt(PdfConstants.CELDA_PADDING_CM));
        celda.setPaddingTop(PtConversion.toPt(0.10f));

        return celda;
    }

    /**
     * Genera una celda utilizada en las tablas de las secciones de las
     * solicitudes.
     *
     * @param etiqueta la etiqueta de la celda.
     * @param valor el valor correspondiente a la etiqueta.
     * @param transform indica si debe de transformarse el parámetro
     * <code>valor</code> a UpperCase.
     * @param blank true si debe de usarse una cadena vacía como valor cuando
     * éste es nulo, false para usar <code>--</code>.
     *
     * @return una celda utilizada en las tablas de secciones de las
     * solicitudes.
     */
    protected static PdfPCell generaCeldaSimple(String etiqueta, String valor, boolean transform, boolean blank) {
        StringBuilder sb = new StringBuilder();
        if (StringUtils.trimToNull(etiqueta) != null) {
            sb.append(etiqueta);
            sb.append(": ");
        }

        String nValor;
        if (Objects.isNull(StringUtils.trimToNull(valor))) {
            nValor = !blank ? "--" : StringUtils.EMPTY;
        } else {
            nValor = valor;
        }

        nValor = transform ? StringUtils.upperCase(nValor) : nValor;
        sb.append(nValor);

        PdfPCell celda = PdfHelper.creaCeldaBackground(sb.toString(), 1, PdfConstants.SOBERANA_LIGHT_8_BLACK, PdfConstants.BACKGROUND_GOBMX_WHITE);
        celda.setBorderColor(PdfConstants.BACKGROUND_GOBMX_DARKGRAY);
        celda.setBorder(Rectangle.BOX);
        celda.setBorderWidth(PdfConstants.CELDA_TAMANO_BORDE);
        celda.setMinimumHeight(PtConversion.toPt(PdfConstants.CELDA_ALTURA_MINIMA_CM));
        celda.setHorizontalAlignment(PdfPCell.ALIGN_LEFT);
        celda.setVerticalAlignment(PdfPCell.ALIGN_MIDDLE);
        celda.setPadding(PtConversion.toPt(PdfConstants.CELDA_PADDING_CM));
        celda.setPaddingLeft(PtConversion.toPt(PdfConstants.CELDA_LEFT_PADDING_CM));

        return celda;
    }

    protected String construyeNombreCompleto(PreRegistroProductor inscripcion) {
        String nombreCompleto = "";
        nombreCompleto += inscripcion.getNombre();
        nombreCompleto += " " + inscripcion.getPrimerApellido();
        nombreCompleto += " " + inscripcion.getSegundoApellido();
        return nombreCompleto;

    }


}
