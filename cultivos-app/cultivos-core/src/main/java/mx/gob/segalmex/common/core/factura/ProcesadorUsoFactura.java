/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.factura.EstatusUsoFacturaEnum;
import mx.gob.segalmex.pluss.modelo.factura.UsoFactura;

/**
 *
 * @author oscar
 */
public interface ProcesadorUsoFactura {

    /**
     * Se encarga de cambiar de estatus a la lista de usos factuas recibidos.
     *
     * @param usos
     * @param nuevo
     * @param actual
     * @param comentarioEstatus
     */
    public void cambiaEstatus(List<UsoFactura> usos, EstatusUsoFacturaEnum nuevo,
            EstatusUsoFacturaEnum actual, String comentarioEstatus);

}
