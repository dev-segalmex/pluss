/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.validador;

import java.math.BigDecimal;
import java.util.Objects;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Component("validadorInscripcionProductorTemporalHectareas")
public class ValidadorInscripcionProductorTemporalHectareas implements ValidadorInscripcion {

    private static final String MIN_SUPERFICIE_TMP = ":minimo-hectareas-temporal";

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Override
    public void valida(InscripcionProductor inscripcion) {

        BigDecimal minimo = buscadorParametro.getValorOpcional(inscripcion.getCultivo().getClave()
                + MIN_SUPERFICIE_TMP, BigDecimal.class);
        if (Objects.nonNull(minimo)) {
            boolean temporal = false;
            // Sumamos la superficie de los predios de temporal
            BigDecimal superficie = BigDecimal.ZERO;
            for (PredioProductor pp : inscripcion.getPrediosActivos()) {
                if (pp.getRegimenHidrico().getClave().equals("temporal")) {
                    temporal = true;
                    superficie = superficie.add(pp.getSuperficie());
                }
            }

            if (temporal && superficie.compareTo(minimo) <= 0) {
                throw new SegalmexRuntimeException("Error al realizar el registro.",
                        "La superficie de los predios de temporal no puede ser menor o igual a 5.5 ha.");
            }

        }
    }

}
