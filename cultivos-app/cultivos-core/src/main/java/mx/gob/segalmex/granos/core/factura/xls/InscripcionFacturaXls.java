/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.factura.xls;

import java.util.List;
import java.util.Objects;
import mx.gob.segalmex.common.core.util.XlsUtils;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFacturaSimple;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersonaEnum;
import mx.gob.segalmex.pluss.modelo.productor.Productor;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Component("inscripcionFacturaXls")
public class InscripcionFacturaXls {

    private static final String VACIO = "--";

    private String[] encabezados = new String[]{
        "No.",
        "Folio",
        "Fecha de registro",
        "Cultivo",
        "Ciclo agrícola",
        "Estado",
        "Nombre de la empresa",
        "RFC de la empresa",
        "Tipo de sucursal",
        "Número contrato",
        "Tipo persona",
        "CURP Productor",
        "RFC Productor",
        "Nombre(s) Productor",
        "Apellidos Productor",
        "Nombre Emisor",
        "RFC Emisor",
        "Nombre Receptor",
        "RFC Receptor",
        "Fecha de emisión",
        "Moneda",
        "Total",
        "Fecha timbrado",
        "UUID timbre fiscal digital",
        "Toneladas",
        "Tipo",
        "Estatus",
        "Comentario",
        "Usuario registra",
        "Usuario validador"
    };

    public byte[] exporta(List<InscripcionFactura> inscripciones) {
        SXSSFWorkbook wb = new SXSSFWorkbook(100);
        CreationHelper ch = wb.getCreationHelper();
        Sheet s = wb.createSheet();

        XlsUtils.creaEncabezados(s, encabezados);

        CellStyle cs = wb.createCellStyle();
        cs.setDataFormat(ch.createDataFormat().getFormat("dd/MM/yyyy HH:mm"));

        int renglon = 1;
        for (InscripcionFactura i : inscripciones) {
            int j = 0;
            Row r = s.createRow(renglon);
            XlsUtils.createCell(r, j++, renglon);
            XlsUtils.createCell(r, j++, i.getFolio());
            XlsUtils.createCell(r, j++, i.getFechaCreacion(), cs);
            XlsUtils.createCell(r, j++, i.getCultivo().getNombre());
            XlsUtils.createCell(r, j++, i.getCiclo().getNombre());
            XlsUtils.createCell(r, j++, Objects.nonNull(i.getEstado()) ? i.getEstado().getNombre() : VACIO);
            XlsUtils.createCell(r, j++, i.getSucursal().getEmpresa().getNombre());
            XlsUtils.createCell(r, j++, i.getSucursal().getEmpresa().getRfc());
            XlsUtils.createCell(r, j++, i.getSucursal().getTipo().getNombre());
            XlsUtils.createCell(r, j++, StringUtils.defaultIfEmpty(i.getContrato(), VACIO));
            if (Objects.nonNull(i.getProductorCiclo())) {
                Productor p = i.getProductorCiclo().getProductor();
                if (p.getTipoPersona().getClave().equals(TipoPersonaEnum.FISICA.getClave())) {
                    XlsUtils.createCell(r, j++, StringUtils.defaultIfBlank(p.getTipoPersona().getNombre(), VACIO));
                    XlsUtils.createCell(r, j++, StringUtils.defaultIfBlank(p.getPersona().getClave(), VACIO));
                    XlsUtils.createCell(r, j++, StringUtils.defaultIfBlank(p.getRfc(), VACIO));
                    XlsUtils.createCell(r, j++, StringUtils.defaultIfBlank(p.getPersona().getNombre(), VACIO));
                    XlsUtils.createCell(r, j++, StringUtils.defaultIfBlank(p.getPersona().getApellidos(), VACIO));
                } else {
                    XlsUtils.createCell(r, j++, StringUtils.defaultIfBlank(p.getTipoPersona().getNombre(), VACIO));
                    XlsUtils.createCell(r, j++, VACIO);
                    XlsUtils.createCell(r, j++, StringUtils.defaultIfBlank(p.getRfc(), VACIO));
                    XlsUtils.createCell(r, j++, StringUtils.defaultIfBlank(p.getNombre(), VACIO));
                    XlsUtils.createCell(r, j++, VACIO);
                }
            } else {
                XlsUtils.createCell(r, j++, VACIO);
                XlsUtils.createCell(r, j++, VACIO);
                XlsUtils.createCell(r, j++, VACIO);
                XlsUtils.createCell(r, j++, VACIO);
                XlsUtils.createCell(r, j++, VACIO);
            }
            XlsUtils.createCell(r, j++, StringUtils.trimToEmpty(i.getCfdi().getNombreEmisor()));
            XlsUtils.createCell(r, j++, i.getCfdi().getRfcEmisor());
            XlsUtils.createCell(r, j++, StringUtils.trimToEmpty(i.getCfdi().getNombreReceptor()));
            XlsUtils.createCell(r, j++, i.getCfdi().getRfcReceptor());
            XlsUtils.createCell(r, j++, i.getCfdi().getFecha(), cs);
            XlsUtils.createCell(r, j++, i.getCfdi().getMoneda());
            XlsUtils.createCell(r, j++, i.getCfdi().getTotal());
            XlsUtils.createCell(r, j++, i.getCfdi().getFechaTimbrado(), cs);
            XlsUtils.createCell(r, j++, i.getCfdi().getUuidTimbreFiscalDigital());
            XlsUtils.createCell(r, j++, i.getCfdi().getTotalToneladas());
            XlsUtils.createCell(r, j++, i.getTipoFactura());
            XlsUtils.createCell(r, j++, i.getEstatus().getNombre());
            XlsUtils.createCell(r, j++, Objects.nonNull(i.getComentarioEstatus())
                    ? i.getComentarioEstatus() : VACIO);
            XlsUtils.createCell(r, j++, i.getUsuarioRegistra().getNombre());
            XlsUtils.createCell(r, j++, Objects.nonNull(i.getUsuarioValidador())
                    ? i.getUsuarioValidador().getNombre() : VACIO);
            renglon++;
        }

        for (int i = 0; i < encabezados.length; i++) {
            s.autoSizeColumn(i);
        }

        return XlsUtils.creaArchivo(wb);
    }

    public byte[] exportaSimple(List<InscripcionFacturaSimple> inscripciones) {
        SXSSFWorkbook wb = new SXSSFWorkbook(100);
        CreationHelper ch = wb.getCreationHelper();
        Sheet s = wb.createSheet();

        XlsUtils.creaEncabezados(s, encabezados);

        CellStyle cs = wb.createCellStyle();
        cs.setDataFormat(ch.createDataFormat().getFormat("dd/MM/yyyy HH:mm"));

        int renglon = 1;
        for (InscripcionFacturaSimple i : inscripciones) {
            int j = 0;
            Row r = s.createRow(renglon);
            XlsUtils.createCell(r, j++, renglon);
            XlsUtils.createCell(r, j++, i.getFolio());
            XlsUtils.createCell(r, j++, i.getFechaCreacion(), cs);
            XlsUtils.createCell(r, j++, i.getCultivo());
            XlsUtils.createCell(r, j++, i.getCiclo());
            XlsUtils.createCell(r, j++, i.getEstado());
            XlsUtils.createCell(r, j++, i.getNombreEmpresa());
            XlsUtils.createCell(r, j++, i.getRfcEmpresa());
            XlsUtils.createCell(r, j++, i.getTipoSucursal());
            XlsUtils.createCell(r, j++, i.getContrato());
            if (i.isProductorCiclo()) {
                if (i.getClaveTipoPersona().equals(TipoPersonaEnum.FISICA.getClave())) {
                    XlsUtils.createCell(r, j++, i.getNombreTipoPersona());
                    XlsUtils.createCell(r, j++, i.getPersonaClave());
                    XlsUtils.createCell(r, j++, i.getPersonaRfc());
                    XlsUtils.createCell(r, j++, i.getPersonaNombre());
                    XlsUtils.createCell(r, j++, i.getPersonaApellido());
                } else {
                    XlsUtils.createCell(r, j++, i.getNombreTipoPersona());
                    XlsUtils.createCell(r, j++, VACIO);
                    XlsUtils.createCell(r, j++, i.getPersonaRfc());
                    XlsUtils.createCell(r, j++, i.getProductorNombre());
                    XlsUtils.createCell(r, j++, VACIO);
                }
            } else {
                XlsUtils.createCell(r, j++, VACIO);
                XlsUtils.createCell(r, j++, VACIO);
                XlsUtils.createCell(r, j++, VACIO);
                XlsUtils.createCell(r, j++, VACIO);
                XlsUtils.createCell(r, j++, VACIO);
            }
            XlsUtils.createCell(r, j++, i.getCfdiNombreEmisor());
            XlsUtils.createCell(r, j++, i.getCfdiRfcEmisor());
            XlsUtils.createCell(r, j++, i.getCfdiNombreReceptor());
            XlsUtils.createCell(r, j++, i.getCfdiRfcReceptor());
            XlsUtils.createCell(r, j++, i.getCfdiFecha(), cs);
            XlsUtils.createCell(r, j++, i.getCfdiMoneda());
            XlsUtils.createCell(r, j++, i.getCfdiTotal());
            XlsUtils.createCell(r, j++, i.getCfdiFechaTimbrado(), cs);
            XlsUtils.createCell(r, j++, i.getCfdiUuidTimbreFiscal());
            XlsUtils.createCell(r, j++, i.getCfdiTotalToneladas());
            XlsUtils.createCell(r, j++, i.getTipoFactura());
            XlsUtils.createCell(r, j++, i.getEstatusNombre());
            XlsUtils.createCell(r, j++, i.getComentarioEstatus());
            XlsUtils.createCell(r, j++, i.getNombreUsuarioRegistra());
            XlsUtils.createCell(r, j++, i.getNombreUsuarioValidador());
            renglon++;
        }

        for (int i = 0; i < encabezados.length; i++) {
            s.autoSizeColumn(i);
        }

        return XlsUtils.creaArchivo(wb);
    }

}
