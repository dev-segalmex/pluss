/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.datos.catalogos;

import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoProductor;

/**
 *
 * @author jurgen
 */
public class CargadorDatosTipoProductor extends AbstractCargadorDatosCatalogo<TipoProductor> {

    @Override
    public TipoProductor getInstance(String[] elementos) {
        TipoProductor tipoProductor = new TipoProductor();
        tipoProductor.setClave(elementos[0]);
        tipoProductor.setNombre(elementos[1]);
        tipoProductor.setActivo(true);
        tipoProductor.setOrden(Integer.parseInt(elementos[2]));
        return tipoProductor;
    }
}
    