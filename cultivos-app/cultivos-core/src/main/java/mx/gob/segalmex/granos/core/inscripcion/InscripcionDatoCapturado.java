/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.inscripcion;

import java.util.ArrayList;
import java.util.List;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;

/**
 * Clase auxiliar para almacenar temporalmente los datos capturados según se vayan agregando.
 *
 * @author ismael
 */
public class InscripcionDatoCapturado {

    /**
     * La lista de datos caputurados de algúna <code>Inscripcion</code>.
     */
    private final List<DatoCapturado> datos = new ArrayList<>();

    /**
     * La lista de datos capturados que se han agregado.
     *
     * @return
     */
    public List<DatoCapturado> getDatos() {
        return datos;
    }

    /**
     * Agrega un dato capturado en la lista estableciendo su orden.
     *
     * @param d el dato capturado para agregar.
     */
    public void agrega(DatoCapturado d) {
        d.setOrden(datos.size() * 10);
        datos.add(d);
    }
}
