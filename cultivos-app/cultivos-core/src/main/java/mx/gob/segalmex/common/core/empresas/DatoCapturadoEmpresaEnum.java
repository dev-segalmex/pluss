/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.empresas;

import mx.gob.segalmex.granos.core.inscripcion.DatoCapturadoEnum;

/**
 *
 * @author jurgen
 */
public enum DatoCapturadoEmpresaEnum implements DatoCapturadoEnum {

    /* Datos de la empresa */

    NOMBRE_EMPRESA("nombre-empresa", "Nombre empresa"),

    TIPO_EMPRESA("tipo-empresa", "Tipo empresa"),

    NOMBRE_RESPONSABLE("responsable-empresa", "Nombre responsable empresa"),

    TELEFONO_OFICINA("telefono-oficina-empresa", "Teléfono oficina"),

    EXTENSION_ADICIONAL("extension-adicional", "Extension/Teléfono adicional"),

    /* Domicilio fiscal */

    CALLE_EMPRESA("calle-empresa", "Calle empresa"),

    NUMERO_EXT_EMPRESA("numero-exterior-empresa", "Número exterior empresa"),

    NUMERO_INT_EMPRESA("numero-interior-empresa", "Número interior empresa"),

    LOCALIDAD_EMPRESA("localidad-empresa", "Localidad empresa"),

    CODIGO_POSTAL_EMPRESA("cp-empresa", "Código postal empresa"),

    ESTADO_EMPRESA("estado-empresa", "Estado empresa"),

    MUNICIPIO_EMPRESA("municipio-empresa", "Municipio empresa"),

    /* Enlace con SEGALMEX */

    NOMBRE_ENLACE("enlace-empresa", "Nombre del enlace"),

    CELULAR_ENLACE("celular-enlace-empresa", "No. celular enlace"),

    CORREO_ENLACE("email-enlace-empresa", "Correo electrónico enlace"),

    /* Infraestructura de la empresa */

    EQUIPO_ANALISIS("existe-equipo-empresa", "Existe equipo de análisis"),

    APLICA_NORMAS("normas-calidad-empresa", "Aplica normas calidad"),

    TIPO_POSESION("tipo-posesion-empresa", "Tipo de posesión"),

    TIPO_ALMACENAMIENTO("tipo-almacenamiento-empresa", "Tipo de almacenamiento"),

    LATITUD_EMPRESA("latitud-empresa", "Latitud"),

    LONGITUD_EMPRESA("longitud-empresa", "Longitud"),

    NUMERO_ALMACENAMIENTO("numero-almacenamiento", "Número de almacenes"),

    CAPACIDAD_ALMACENAMIENTO("capacidad-almacenamiento", "Capacidad instalada"),

    VOLUMEN_ALMACENAMIENTO("volumen-actual-almacenamiento", "Volumen almacenado actual"),

    SUPERFICIE_ALMACENAMIENTO("superficie-almacenamiento", "Superficie de la instalación"),

    APLICA_ALMACENADORA("aplica-almacenadora-almacenamiento", "Habilitado almacenadora"),

    NOMBRE_ALMACENADORA("almacenadora-almacenamiento", "Nombre de almacenadora"),

    /* Datos de comercialización */
    ACTOR_COMPRA("actores-compra", "Nombre actor (compra)"),

    ESTADO_COMPRA("estado-compra", "Estado (compra)"),

    MUNICIPIO_COMPRA("municipio-compra", "Municipio (compra)"),

    ANIO_COMPRA("anio-compra", "Año (compra)"),

    CANTIDAD_COMPRA("cantidad-compra", "Cantidad (compra)"),

    ACTOR_VENTA("actores-venta", "Nombre actor (venta)"),

    ESTADO_VENTA("estado-venta", "Estado (venta)"),

    MUNICIPIO_VENTA("municipio-venta", "Municipio ()"),

    ANIO_VENTA("anio-venta", "Año (venta)"),

    CANTIDAD_VENTA("cantidad-venta", "Cantidad (venta)"),

    /* Bodegas o centros de acopio */
    NOMBRE_SUCURSAL("nombre-sucursal", "Nombre sucursal"),

    CALLE_SUCURSAL("calle-sucursal", "Calle sucursal"),

    NUMERO_EXT_SUCURSAL("numero-exterior-sucursal", "Número ext. sucursal"),

    NUMERO_INT_SUCURSAL("numero-interior-sucursal", "Número int. sucursal"),

    LOCALIDAD_SUCURSAL("localidad-sucursal", "Localidad sucursal"),

    CP_SUCURSAL("cp-sucursal", "Código postal sucursal"),

    ESTADO_SUCURSAL("estado-sucursal", "Estado sucursal"),

    MUNICIPIO_SUCURSAL("municipio-sucursal", "Municipio sucursal"),

    LATITUD_SUCURSAL("latitud-sucursal", "Latitud sucursal"),

    LONGITUD_SUCURSAL("longitud-sucursal", "Longitud sucursal"),

    ENLACE_SUCURSAL("enlace-sucursal", "Enlace sucursal"),

    CELULAR_ENLACE_SUCURSAL("celular-enlace-sucursal", "Celular enlace sucursal"),

    CORREO_ENLACE_SUCURSAL("correo-enlace-sucursal", "Correo enlace sucursal"),

    COMPROBANTE_DOMICILIO_SUCURSAL_PDF("comprobane-domicilio-sucursal-pdf", "Comprobante domicilio sucursal (PDF)"),

    DOCUMENTO_POSESION_SUCURSAL_PDF("documento-posesion-sucursal-pdf", "Documento posesión sucursal (PDF)"),

    CERTIFICADO_BASCULA_SUCURSAL_PDF("certificado-bascula-sucursal-pdf", "Certificado báscula (sucursal)"),

    JUSTIFICACION_GRANO_SUCURSAL_PDF("justificacion-grano-sucursal-pdf", "Justificación grano acopiado sucursal (PDF)"),

    /* Archivos anexos */
    ACTA_CONSTITUTIVA_PDF("acta-constitutiva-pdf", "Acta constitutiva (PDF)"),

    RFC_PDF("rfc-pdf", "RFC (PDF)"),

    DOCUMENTO_POSESION_PDF("documento-posesion-pdf", "Documento posesión (PDF)"),

    CARTA_ACUERDO_PDF("carta-acuerdo-pdf", "Carta acuerdo (PDF)"),

    CERTIFICADO_BASCULA_PDF("certificado-bascula-pdf", "Certificado báscula (PDF)"),

    JUSTIFICACION_GRANO_ACOPIADO_PDF("justificacion-grano-acopiado-pdf", "Justificacion grano acopiado (PDF)"),

    COMPROBANTE_DOMICILIO_PDF("comprobante-domicilio-pdf", "Comprobante domicilio (PDF)"),

    PODER_VENTA_COMPRA_PDF("poder-venta-compra-pdf", "Poder venta / compra (PDF)");

    /**
     * La clave del dato capturado.
     */
    private final String clave;

    /**
     * El nombre del datos capturado.
     */
    private final String nombre;

    private DatoCapturadoEmpresaEnum(String clave, String nombre) {
        this.clave = clave;
        this.nombre = nombre;
    }

    @Override
    public String getClave() {
        return clave;
    }

    @Override
    public String getNombre() {
        return nombre;
    }

}
