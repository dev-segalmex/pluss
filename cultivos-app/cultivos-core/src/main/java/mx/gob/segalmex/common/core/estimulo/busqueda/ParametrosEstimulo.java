/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.estimulo.busqueda;

import java.math.BigDecimal;
import java.util.Calendar;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;

/**
 *
 * @author jurgen
 */
@Getter
@Setter
public class ParametrosEstimulo {

    private Integer id;

    private CicloAgricola ciclo;

    private Calendar fechaInicio;

    private Calendar fechaFin;

    private BigDecimal cantidad;

    private Estado estado;

    private Integer orden;

    private Calendar fechaRango;

    private String aplicaEstimulo;

    private String tipo;

    private String contrato;

    private String tipoProductor;
}
