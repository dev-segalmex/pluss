/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.inscripcion;

import mx.gob.segalmex.pluss.modelo.base.Catalogo;
import mx.gob.segalmex.pluss.modelo.historico.EntidadHistorico;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;

/**
 * Clase que genera instancias de <code>DatoCapturado</code> a partir del modelo de datos de
 * <code>InscripcionContrato</code>.
 *
 * @author ismael
 */
public class DatoCapturadoFactory {

    protected DatoCapturadoFactory() {
    }

    public static DatoCapturado getInstance(EntidadHistorico eh, DatoCapturadoEnum dce,
            String valor) {
        DatoCapturado dc = new DatoCapturado();
        dc.setClase(eh.getClass().getName());
        dc.setReferencia(eh.getUuid());
        dc.setNombre(dce.getNombre());
        dc.setClave(dce.getClave());
        dc.setValor(valor);
        dc.setTipo("texto");

        return dc;
    }

    public static DatoCapturado getInstance(EntidadHistorico eh, DatoCapturadoEnum dce,
            String valor, String grupo) {
        DatoCapturado dc = getInstance(eh, dce, valor);
        dc.setGrupo(grupo);
        return dc;
    }

    /**
     * Crea una instancia de un dato capturado de tipo catálogo.
     *
     * @param eh la inscripción a la que pertenece el dato capturado.
     * @param dce el enum del tipo de dato capturado.
     * @param catalogo el catálogo capturado.
     * @return el dato capturado con los valores indicados.
     */
    public static DatoCapturado getInstance(EntidadHistorico eh, DatoCapturadoEnum dce,
            Catalogo catalogo) {
        DatoCapturado dc = new DatoCapturado();
        dc.setClase(eh.getClass().getName());
        dc.setReferencia(eh.getUuid());
        dc.setNombre(dce.getNombre());
        dc.setClave(dce.getClave());
        dc.setValor(catalogo.getClave());
        dc.setValorTexto(catalogo.getNombre());
        dc.setTipo("catalogo");
        return dc;
    }

    public static DatoCapturado getInstance(EntidadHistorico eh, DatoCapturadoEnum dce,
            Catalogo catalogo, String grupo) {
        DatoCapturado dc = getInstance(eh, dce, catalogo);
        dc.setGrupo(grupo);

        return dc;
    }

    public static DatoCapturado getInstance(EntidadHistorico eh, DatoCapturadoEnum dce, Catalogo c,
            int orden, String suffix, String grupo) {
        DatoCapturado dc = getInstance(eh, dce, c, grupo);
        dc.setClave(orden + "_" + dc.getClave());
        dc.setNombre(dc.getNombre() + suffix);
        return dc;
    }

    public static DatoCapturado getInstance(EntidadHistorico eh, DatoCapturadoEnum dce, String valor,
            int orden, String suffix, String grupo) {
        DatoCapturado dc = getInstance(eh, dce, valor, grupo);
        dc.setClave(orden + "_" + dc.getClave());
        dc.setNombre(dc.getNombre() + suffix);

        return dc;
    }

    public static DatoCapturado getInstanceArchivo(EntidadHistorico eh, DatoCapturadoEnum dce,
            String valor, String grupo) {
        DatoCapturado dc = getInstance(eh, dce, valor);
        dc.setGrupo(grupo);
        dc.setTipo("archivo");
        return dc;
    }

    public static DatoCapturado getInstanceArchivo(EntidadHistorico eh, DatoCapturadoEnum dce,
            String valor, int orden, String suffix, String grupo) {
        DatoCapturado dc = getInstance(eh, dce, valor, orden, suffix, grupo);
        dc.setTipo("archivo");

        return dc;
    }

    public static DatoCapturado getInstanceBoolean(EntidadHistorico eh, DatoCapturadoEnum dce,
            String valor, String grupo) {
        DatoCapturado dc = getInstance(eh, dce, valor);
        dc.setGrupo(grupo);
        dc.setTipo("boolean");
        return dc;
    }
}
