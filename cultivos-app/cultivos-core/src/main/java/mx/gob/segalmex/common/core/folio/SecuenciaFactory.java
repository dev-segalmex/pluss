/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.folio;

import mx.gob.segalmex.pluss.modelo.secuencias.Secuencia;
import mx.gob.segalmex.pluss.modelo.secuencias.SecuenciaFolioComprobanteRecepcion;
import mx.gob.segalmex.pluss.modelo.secuencias.SecuenciaFolioDocumentoComercial;
import mx.gob.segalmex.pluss.modelo.secuencias.SecuenciaFolioEmpresa;
import mx.gob.segalmex.pluss.modelo.secuencias.SecuenciaFolioFactura;
import mx.gob.segalmex.pluss.modelo.secuencias.SecuenciaFolioInformacionAlerta;
import mx.gob.segalmex.pluss.modelo.secuencias.SecuenciaFolioMaiz;
import mx.gob.segalmex.pluss.modelo.secuencias.SecuenciaFolioPreRegistro;
import mx.gob.segalmex.pluss.modelo.secuencias.SecuenciaFolioProductorMaiz;
import mx.gob.segalmex.pluss.modelo.secuencias.SecuenciaFolioSucursal;
import mx.gob.segalmex.pluss.modelo.secuencias.TipoFolioEnum;

/**
 *
 * @author ismael
 */
public class SecuenciaFactory {

    public static Secuencia getInstance(TipoFolioEnum tipo) {
        switch (tipo) {
            case SEQ_FOLIO_MAIZ:
                return new SecuenciaFolioMaiz();
            case SEQ_FOLIO_PRODUCTOR_MAIZ:
                return new SecuenciaFolioProductorMaiz();
            case SEQ_FOLIO_FACTURA:
                return new SecuenciaFolioFactura();
            case SEQ_FOLIO_PRE_REGISTRO:
                return new SecuenciaFolioPreRegistro();
            case SEQ_FOLIO_DOCUMENTO_COMERCIAL:
                return new SecuenciaFolioDocumentoComercial();
            case SEQ_FOLIO_EMPRESA:
                return new SecuenciaFolioEmpresa();
            case SEQ_FOLIO_SUCURSAL:
                return new SecuenciaFolioSucursal();
            case SEQ_FOLIO_COMPROBANTE_RECEPCION:
                return new SecuenciaFolioComprobanteRecepcion();
            case SEQ_FOLIO_ALERTA:
                return new SecuenciaFolioInformacionAlerta();
            default:
                throw new IllegalArgumentException("No existe la secuencia para: " + tipo);
        }
    }
}
