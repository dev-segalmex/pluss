/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;

/**
 *
 * @author ismael
 */
public interface BuscadorInscripcionContrato {

    InscripcionContrato buscaElemento(Integer id);

    InscripcionContrato buscaElemento(String uuid);

    InscripcionContrato buscaElementoPorFolio(String folio);

    InscripcionContrato buscaElemento(ParametrosInscripcionContrato parametros);

    List<InscripcionContrato> busca(ParametrosInscripcionContrato parametros);

    List<Object[]> buscaAgrupada(ParametrosInscripcionContrato parametros);

}
