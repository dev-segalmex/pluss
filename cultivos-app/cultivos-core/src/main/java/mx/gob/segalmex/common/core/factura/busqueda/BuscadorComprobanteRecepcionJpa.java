/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.busqueda;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.factura.ComprobanteRecepcion;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author oscar
 */
@Slf4j
@Repository("buscadorComprobanteRecepcion")
public class BuscadorComprobanteRecepcionJpa implements BuscadorComprobanteRecepcion {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public ComprobanteRecepcion buscaElemento(Integer id) {
        Objects.requireNonNull(id, "El id no puede ser nulo.");
        ParametrosComprobanteRecepcion parametros = new ParametrosComprobanteRecepcion();
        parametros.setId(id);

        List<ComprobanteRecepcion> resultados = busca(parametros);
        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontraron resultados.", 1);
        }
        return resultados.get(0);
    }

    @Override
    public ComprobanteRecepcion buscaElemento(String folio) {
        Objects.requireNonNull(folio, "El folio no puede ser nulo.");
        ParametrosComprobanteRecepcion parametros = new ParametrosComprobanteRecepcion();
        parametros.setFolio(folio);

        List<ComprobanteRecepcion> resultados = busca(parametros);
        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontraron resultados.", 1);
        }
        return resultados.get(0);
    }

    @Override
    public List<ComprobanteRecepcion> busca(ParametrosComprobanteRecepcion parametros) {
        StringBuilder sb = new StringBuilder("SELECT cr FROM ComprobanteRecepcion cr ");

        Map<String, Object> params = new HashMap<>();
        boolean first = true;

        if (Objects.nonNull(parametros.getId())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "cr.id", "id", parametros.getId(), params);
        }

        if (Objects.nonNull(parametros.getFolio())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "cr.folio", "folio", parametros.getFolio(), params);
        }

        if (Objects.nonNull(parametros.getEstatus())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "cr.estatus", "estatus", parametros.getEstatus(), params);
        }

        if (Objects.nonNull(parametros.getInscripcionFactura())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "cr.inscripcionFactura", "inscripcionFactura", parametros.getInscripcionFactura(), params);
        }

        sb.append(" ORDER BY cr.folio ");
        LOGGER.debug("Ejecutando el query: {}", sb);
        TypedQuery<ComprobanteRecepcion> q
                = entityManager.createQuery(sb.toString(), ComprobanteRecepcion.class);
        QueryUtils.setParametros(q, params);

        long inicio = System.currentTimeMillis();
        List<ComprobanteRecepcion> comprobantes = q.getResultList();
        LOGGER.debug("Se encontraron {} resultados en {} ms.", comprobantes.size(),
                System.currentTimeMillis() - inicio);

        return comprobantes;

    }

}
