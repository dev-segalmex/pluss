/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.rfc.busqueda;

import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;

/**
 *
 * @author oscar
 */
@Getter
@Setter
public class ParametrosRfcExcepcion {

    private Integer id;

    private String rfc;

    private String rfcAproximado;

    private Usuario usuarioRegistra;

    private Boolean activo;

    private Usuario usuarioBaja;

}
