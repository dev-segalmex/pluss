package mx.gob.segalmex.common.core.datos.catalogos;

import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoIar;

/**
 *
 * @author oscar
 */
public class CargadorDatosTipoIar extends AbstractCargadorDatosCatalogo<TipoIar> {

@Override
    public TipoIar getInstance(String[] elementos) {
        TipoIar tipoIar = new TipoIar();
        tipoIar.setClave(elementos[0]);
        tipoIar.setNombre(elementos[1]);
        tipoIar.setOrden(Integer.parseInt(elementos[2]));
        tipoIar.setActivo(Boolean.valueOf(elementos[3]));

        return tipoIar;
    }


}
