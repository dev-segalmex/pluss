/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.pdf;

import mx.gob.segalmex.granos.core.contrato.pdf.*;
import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.PageSize;
import com.lowagie.text.pdf.PdfPageEventHelper;
import com.lowagie.text.pdf.PdfWriter;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;

/**
 *
 * @author ismael
 */
public class InscripcionProductorPdfCreator extends PdfPageEventHelper {

    private AbstractInscripcionPdfHelper helper;

    /**
     * El nombre del ambiente en el que se ejecuta la generación del PDF.
     */
    private String ambiente;

    public InscripcionProductorPdfCreator(String ambiente) {
        this.ambiente = ambiente;
    }

    /**
     * Genera un PDF con la inscripción del contrato.
     *
     * @param inscripcion la inscripción de la que se genera el PDF.
     * @return el PDF como arreglo de bytes.
     */
    public byte[] create(InscripcionProductor inscripcion) {
        switch (CultivoEnum.getInstance(inscripcion.getCultivo().getClave())) {
            case ARROZ:
                helper = new ArrozInscripcionPdfHelper();
                break;
            case MAIZ_COMERCIAL:
                helper = new MaizInscripcionPdfHelper();
                break;
            case TRIGO:
                helper = new TrigoInscripcionPdfHelper();
                break;
            default:
                throw new IllegalArgumentException("No existe pdf para este cultivo.");
        }

        try {
            Document document = new Document(PageSize.LETTER);
            document.setMargins(
                    PtConversion.toPt(1.5f),
                    PtConversion.toPt(1.5f),
                    PtConversion.toPt(1.5f),
                    PtConversion.toPt(1.5f));

            ByteArrayOutputStream os = new ByteArrayOutputStream();
            PdfWriter writer = PdfWriter.getInstance(document, os);
            writer.setPageEvent(this);
            document.open();

            helper.generaContenido(inscripcion, document, writer);
            document.close();
            os.close();

            return os.toByteArray();
        } catch (DocumentException | IOException ouch) {
            throw new RuntimeException("Error al generar el PDF.", ouch);
        }
    }

    @Override
    public void onEndPage(PdfWriter writer, Document document) {
        SolicitudGobMxPdfHelper.setHeaders(writer, document);
        SolicitudGobMxPdfHelper.setFooters(writer, document, 2);
        PdfHelper.setWatermark(this.ambiente, writer, document);
    }

}
