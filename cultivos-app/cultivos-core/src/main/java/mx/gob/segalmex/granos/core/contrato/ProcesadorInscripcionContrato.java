/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.inscripcion.ContratoProductor;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;

/**
 *
 * @author ismael
 */
public interface ProcesadorInscripcionContrato {

    void procesa(InscripcionContrato inscripcion);

    void asigna(InscripcionContrato inscripcion, Usuario usuario, Usuario validador);

    void validaPositivo(InscripcionContrato inscripcion, Usuario usuario, List<DatoCapturado> capturados);

    void cancela(InscripcionContrato inscripcion, Usuario usuario, EstatusInscripcion estatus);

    void solicitaCorreccion(InscripcionContrato inscripcion, Usuario usuario, List<DatoCapturado> capturados);

    void corrige(InscripcionContrato inscripcion, Usuario usuario, List<DatoCapturado> capturados);

    void actualiza(InscripcionContrato inscripcion, List<DatoCapturado> capturados);

    void revalida(InscripcionContrato inscripcion, Usuario usuario);

    void solicitaComplemento(InscripcionContrato inscripcion, Usuario usuario);

    void aceptaComplemento(InscripcionContrato inscripcion, Usuario usuario);

    void rechazaComplemento(InscripcionContrato inscripcion, Usuario usuario);

    void procesaIar(InscripcionContrato inscripcion, Usuario usuario);

    void solicitaCorreccionComplemento(InscripcionContrato inscripcion, Usuario usuario, List<DatoCapturado> capturados);

    void validaComplementoPositivo(InscripcionContrato inscripcion, Usuario usuario, List<DatoCapturado> capturados);

    void corrigeComplemento(InscripcionContrato inscripcion, Usuario usuario, List<DatoCapturado> capturados);

    void solicitaCorreccionContratoProductor(InscripcionContrato inscripcion, Usuario usuario);

    void corrigeContratoProductor(InscripcionContrato inscripcion, Usuario usuario, List<ContratoProductor> contratosProductor);
}
