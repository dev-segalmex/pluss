/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.factura.Factura;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import mx.gob.segalmex.pluss.modelo.factura.UsoFactura;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;

/**
 *
 * @author ismael
 */
public interface ProcesadorInscripcionFactura {

    /**
     * Procesa una inscripción de factura.
     *
     * @param inscripcion la inscripción a procesar.
     * @param facturas las facturas que respaldan a la factura global.
     * @return la inscripción procesada.
     */
    InscripcionFactura procesa(InscripcionFactura inscripcion, List<Factura> facturas);

    void verifica(InscripcionFactura inscripcion);

    void asigna(InscripcionFactura inscripcion, Usuario usuario, Usuario validador);

    void validaPositivo(InscripcionFactura inscripcion, Usuario usuario, List<DatoCapturado> capturados);

    void validaNegativo(InscripcionFactura inscripcion, Usuario usuario, List<DatoCapturado> capturados);

    void cancela(InscripcionFactura inscripcion, Usuario usuario);

    void corrige(InscripcionFactura inscripcion, Usuario usuario, List<DatoCapturado> capturados);

    void finalizaPositivo(InscripcionFactura inscripcion, Usuario usuario);

    void ignoraConcepto(InscripcionFactura inscripcion, int index);

    void revalida(InscripcionFactura inscripcion, Usuario usuario);

    void duplica(UsoFactura base, UsoFactura nuevo, Usuario usuario);
}
