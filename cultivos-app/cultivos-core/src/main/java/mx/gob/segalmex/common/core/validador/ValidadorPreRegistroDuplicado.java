package mx.gob.segalmex.common.core.validador;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorPreRegistroProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PreRegistroProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Se encarga de validar que la curp no este previamente registrada.
 *
 * @author oscar
 */
@Component("validadorPreRegistroDuplicado")
@Slf4j
public class ValidadorPreRegistroDuplicado implements ValidadorPreRegistro {

    @Autowired
    private BuscadorPreRegistroProductor buscadorPreRegistro;

    @Override
    public void valida(PreRegistroProductor preRegistro) {
        LOGGER.info("Validando si la CURP/RFC ya ha sido previamente registrado...");
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setCurp(preRegistro.getCurp());
        parametros.setCultivo(preRegistro.getCultivo());
        parametros.setCiclo(preRegistro.getCiclo());
        List<PreRegistroProductor> preRegistros = buscadorPreRegistro.busca(parametros);
        if (!preRegistros.isEmpty()) {
            throw new SegalmexRuntimeException("Error al realizar el Pre Registro.",
                    "Ya existe un pre registro con la misma CURP.");
        }
        parametros.setCurp(null);
        parametros.setRfcProductor(preRegistro.getRfc());
        preRegistros = buscadorPreRegistro.busca(parametros);
        if (!preRegistros.isEmpty()) {
            throw new SegalmexRuntimeException("Error al realizar el Pre Registro.",
                    "Ya existe un pre registro con el mismo RFC.");
        }
    }

}
