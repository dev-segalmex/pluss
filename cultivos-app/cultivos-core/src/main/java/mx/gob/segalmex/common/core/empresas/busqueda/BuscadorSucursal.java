/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.empresas.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.catalogos.Empresa;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;

/**
 *
 * @author jurgen
 */
public interface BuscadorSucursal {

    List<Sucursal> getSucursales(Empresa empresa);

    /**
     * Se encarga de obtener una sucursal dado el usuario.
     *
     * @param usuario el usuario que tiene la sucursal.
     * @return la sucursal encontrada o un EmptyResultDataAccessException si no
     * encuentra nada.
     */
    Sucursal buscaElemento(Usuario usuario);

    /**
     * Se encarga de obtener todas las sucursales del usuario recibido.
     *
     * @param usuario el usuario de las sucursales.
     * @return la lista de sucursales que tiene el usuario.
     */
    List<Sucursal> busca(Usuario usuario);

}
