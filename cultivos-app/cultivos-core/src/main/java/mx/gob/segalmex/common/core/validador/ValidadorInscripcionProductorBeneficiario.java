package mx.gob.segalmex.common.core.validador;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Component("validadorInscripcionProductorBeneficiario")
@Slf4j
public class ValidadorInscripcionProductorBeneficiario implements ValidadorInscripcion {

    @Autowired
    private BuscadorInscripcionProductor buscadorInscripcion;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Override
    public void valida(InscripcionProductor inscripcion) {
        LOGGER.info("Buscando si un beneficiario ya es productor...");
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        String curpBeneficiario = inscripcion.getCurpBeneficiario();
        parametros.setCiclo(inscripcion.getCiclo());
        parametros.setCurp(curpBeneficiario);
        parametros.setNoEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.CANCELADA.getClave()));
        List<InscripcionProductor> inscripciones = buscadorInscripcion.busca(parametros);
        if (!inscripciones.isEmpty()) {
            throw new SegalmexRuntimeException("Error al realizar el registro.",
                    "El beneficiario con CURP " + curpBeneficiario + " ya se encuentra registrado como "
                    + "productor en el ciclo " + inscripcion.getCiclo().getNombre());
        }
    }

}
