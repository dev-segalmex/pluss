/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.validador;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.granos.core.rfc.busqueda.BuscadorRfcExcepcion;
import mx.gob.segalmex.pluss.modelo.productor.EmpresaAsociada;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author erikcamacho
 */
@Component("validadorInscripcionProductorRfcAsociado")
@Slf4j
public class ValidadorInscripcionProductorRfcAsociado implements ValidadorInscripcion {

    @Autowired
    private BuscadorRfcExcepcion buscadorRfcExcepcion;

    @Override
    public void valida(InscripcionProductor inscripcion) {
        LOGGER.debug("Validando RFC's de asociados.");
        for (EmpresaAsociada e : inscripcion.getEmpresas()) {
            LOGGER.debug("Validando RFC de la asosiacion {}.", e.getRfc());
            String rfc = e.getRfc();
            List<String> excepciones = buscadorRfcExcepcion.getExcepciones(rfc, true);
            ValidadorRfcHelper.valida(rfc, excepciones, e.getRfc() + " de la sociedad");
        }
    }

}
