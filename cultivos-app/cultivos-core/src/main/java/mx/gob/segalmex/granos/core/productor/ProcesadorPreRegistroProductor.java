/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor;

import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.productor.PreRegistroProductor;

/**
 *
 * @author jurgen
 */
public interface ProcesadorPreRegistroProductor {

    void procesa(PreRegistroProductor preRegistro, Usuario u, boolean generaFolio);

    void sustituye(PreRegistroProductor original, PreRegistroProductor editado, Usuario u);

}
