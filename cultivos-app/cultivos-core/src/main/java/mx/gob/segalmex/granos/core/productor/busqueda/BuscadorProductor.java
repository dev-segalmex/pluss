/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersona;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.Productor;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;

/**
 *
 * @author ismael
 */
public interface BuscadorProductor {

    /**
     * Busca un productor basado en los datos de una inscripción.
     *
     * @param rfc el RFC del productor.
     * @param tipo el tipo de persona que es el productor.
     * @return el <code>Productor</code> en caso de que exista.
     */
    Productor busca(String rfc, TipoPersona tipo);

    ProductorCiclo busca(Productor productor, CicloAgricola ciclo, Cultivo cultivo, boolean fetch);

    ProductorCiclo busca(String folio, boolean fetch);

    ProductorCiclo busca(InscripcionProductor inscripcion);

    ProductorCiclo buscaElemento(String folio);

    /**
     * Obtiene tododos los {@link ProductorCiclo} dado el productor.
     *
     * @param productor el productor del que se quieren sus
     * {@link ProductorCiclo}.
     * @return una lista de {@link ProductorCiclo}.
     */
    List<ProductorCiclo> busca(Productor productor);

    List<Productor> busca(ParametrosInscripcionProductor parametros);

    List<ProductorCiclo> buscaProductorCiclo(ParametrosInscripcionProductor parametros);

    Productor busca(String clave);

    Productor buscaElemento(ParametrosInscripcionProductor parametros);

    ProductorCiclo buscaElementoPc(ParametrosInscripcionProductor parametros);
}
