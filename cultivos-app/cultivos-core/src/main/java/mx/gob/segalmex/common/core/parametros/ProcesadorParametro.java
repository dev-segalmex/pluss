/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.parametros;

import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;

/**
 *
 * @author jurgen
 */
public interface ProcesadorParametro {

    void procesa(Parametro parametro);

    void modifica(Parametro parametro);

    void elimina(Parametro parametro);

}
