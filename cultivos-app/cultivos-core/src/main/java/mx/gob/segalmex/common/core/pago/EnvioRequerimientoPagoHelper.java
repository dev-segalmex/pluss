/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.pago;

import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Function;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.pago.busqueda.BuscadorRequerimientoPago;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.pluss.modelo.factura.EstatusUsoFactura;
import mx.gob.segalmex.pluss.modelo.factura.EstatusUsoFacturaEnum;
import mx.gob.segalmex.pluss.modelo.pago.EstatusRequerimientoPagoEnum;
import mx.gob.segalmex.pluss.modelo.pago.LayoutEnum;
import mx.gob.segalmex.pluss.modelo.pago.RequerimientoPago;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.apache.commons.lang3.StringUtils;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ismael
 */
@Component("envioRequerimientoPagoHelper")
@Slf4j
public class EnvioRequerimientoPagoHelper {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private BuscadorRequerimientoPago buscadorRequerimientoPago;

    @Autowired
    private BuscadorInscripcionProductor buscadorInscripcionProductor;

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Transactional
    public JSONObject generaJson(RequerimientoPago requerimiento) {
        RequerimientoPago rp = buscadorRequerimientoPago.buscaElemento(requerimiento.getId());
        AtomicInteger i = new AtomicInteger(0);
        Map<String, InscripcionProductor> map = rp.getRequerimientos().stream()
                .map(ruf -> ruf.getUsoFactura().getProductorCiclo().getFolio())
                .distinct()
                .collect(Collectors.toMap(Function.identity(), folio -> getInscripcion(folio, i)));
        GeneradorCegapJson generador = new GeneradorCegapJson();
        JSONObject json = generador.getJson(rp, map);
        LOGGER.info("JSON generado: {}", json.toString());
        return json;
    }

    private InscripcionProductor getInscripcion(String folio, AtomicInteger i) {
        if (i.getAndIncrement() % 200 == 0) {
            LOGGER.info("Información de los primeros {} productores", i.get());
        }
        ParametrosInscripcionProductor pi = new ParametrosInscripcionProductor();
        pi.setFolio(folio);
        return buscadorInscripcionProductor.busca(pi).get(0);
    }

    @Transactional
    public void marcaEnvio(RequerimientoPago requerimiento, String response) {
        requerimiento = entityManager.getReference(RequerimientoPago.class, requerimiento.getId());
        EstatusUsoFactura seleccionado = buscadorCatalogo.buscaElemento(EstatusUsoFactura.class,
                EstatusUsoFacturaEnum.EN_PROCESO);
        requerimiento.getRequerimientos().forEach(ru -> {
            ru.getUsoFactura().setEstatus(seleccionado);
            registroEntidad.actualiza(ru.getUsoFactura());
        });

        requerimiento.setEstatus(EstatusRequerimientoPagoEnum.ENVIADO.getClave());
        requerimiento.setCegap(getCegap(requerimiento, response));
        registroEntidad.actualiza(requerimiento);
    }

    private String getCegap(RequerimientoPago requerimiento, String response) {
        JSONObject json = new JSONObject(response);
        String mensajes = json.getString("Mensajes");
        int index = 1;
        if (requerimiento.getLayout().equals(LayoutEnum.ORDENES_PAGO.getClave())) {
            index = 0;
        }
        String cegap = mensajes.split(",")[index];
        if (!StringUtils.isNumeric(cegap)) {
            throw new IllegalArgumentException("No se generó un CEGAP, mensaje: " + mensajes);
        }
        return cegap;
    }
}
