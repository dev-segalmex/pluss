/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.pago.busqueda;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.pago.EstatusRequerimientoPagoEnum;
import mx.gob.segalmex.pluss.modelo.pago.RequerimientoPago;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ismael
 */
@Repository("buscadorRequerimientoPago")
@Slf4j
public class BuscadorRequerimientoPagoJpa implements BuscadorRequerimientoPago {

    private static final String MONITOREABLE = "--";

    private static final String REPORTE_PENDIENTE = "--";

    /**
     * El entity manager de la clase.
     */
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public RequerimientoPago buscaElemento(Integer id) {
        Objects.requireNonNull(id, "El uuid no puede ser nulo.");
        ParametrosRequerimientoPago parametros = new ParametrosRequerimientoPago();
        parametros.setId(id);
        return buscaElemento(parametros);
    }

    @Override
    public RequerimientoPago buscaElemento(String uuid) {
        Objects.requireNonNull(uuid, "El uuid no puede ser nulo.");
        ParametrosRequerimientoPago parametros = new ParametrosRequerimientoPago();
        parametros.setUuid(uuid);
        return buscaElemento(parametros);
    }

    @Override
    public RequerimientoPago buscaElemento(ParametrosRequerimientoPago parametros) {
        List<RequerimientoPago> resultados = busca(parametros);
        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontraron resultados.", 1);
        }

        if (resultados.size() > 1) {
            throw new IncorrectResultSizeDataAccessException("Se esperaba un resultado.",
                    1, resultados.size());
        }

        return resultados.get(0);
    }

    @Override
    public List<RequerimientoPago> busca(ParametrosRequerimientoPago parametros) {
        StringBuilder sb = new StringBuilder("SELECT r FROM RequerimientoPago r ");

        Map<String, Object> params = new HashMap<>();
        boolean first = true;
        if (Objects.nonNull(parametros.getId())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "r.id", "id", parametros.getId(), params);
        }

        if (Objects.nonNull(parametros.getUuid())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "r.uuid", "uuid", parametros.getUuid(), params);
        }

        if (Objects.nonNull(parametros.getEstatus())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "r.estatus", "estatus", parametros.getEstatus(), params);
        }

        if (Objects.nonNull(parametros.getNoEstatus())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("r.estatus != :noEstatus ");
            params.put("noEstatus", parametros.getNoEstatus());
        }

        if (Objects.nonNull(parametros.getMonitoreable()) && parametros.getMonitoreable()) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "r.monitoreable", "monitoreable",
                    MONITOREABLE, params);
        }

        if (Objects.nonNull(parametros.getReportePendiente()) && parametros.getReportePendiente()) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "r.reporteGenerado", "reportePendiente",
                    REPORTE_PENDIENTE, params);
        }

        if (Objects.nonNull(parametros.getFechaInicio())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("r.fechaCreacion >= :fechaInicio ");
            params.put("fechaInicio", parametros.getFechaInicio());
        }

        if (Objects.nonNull(parametros.getFechaFin())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("r.fechaCreacion < :fechaFin ");
            params.put("fechaFin", parametros.getFechaFin());
        }

        if (Objects.nonNull(parametros.getCultivo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "r.cultivo", "cultivo", parametros.getCultivo(), params);
        }

        if (Objects.nonNull(parametros.getCegap())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "r.cegap", "cegap", parametros.getCegap(), params);
        }

        first = QueryUtils.agregaWhereAnd(first, sb);
        sb.append("r.estatus != :noEstatus ");
        params.put("noEstatus", EstatusRequerimientoPagoEnum.CANCELADO.getClave());

        sb.append("ORDER BY r.id ");
        LOGGER.debug("Ejecutando el query: {}", sb);
        TypedQuery<RequerimientoPago> q
                = entityManager.createQuery(sb.toString(), RequerimientoPago.class);
        QueryUtils.setParametros(q, params);

        if (Objects.nonNull(parametros.getMaximoResultados())) {
            if (parametros.getMaximoResultados() > 0) {
                q.setMaxResults(parametros.getMaximoResultados());
            }
        }

        long inicio = System.currentTimeMillis();
        List<RequerimientoPago> requerimientos = q.getResultList();
        LOGGER.debug("Se encontraron {} resultados en {} ms.", requerimientos.size(),
                System.currentTimeMillis() - inicio);

        return requerimientos;
    }

}
