/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.busqueda;

import java.util.ArrayList;
import java.util.List;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersona;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.cultivos.core.inscripcion.busqueda.ParametrosInscripcion;

/**
 *
 * @author ismael
 */
@Getter
@Setter
public class ParametrosInscripcionProductor extends ParametrosInscripcion {

    private String curp;

    private String rfcProductor;

    private TipoPersona tipoPersona;

    private String nombre;

    private String papellido;

    private String sapellido;

    private String clave;

    private Boolean activo;

    private String tipoRegistro;

    private String claveArchivos;

    private Integer maximoResultados;

    private String clabe;

    private String curpBeneficiario;

    private String noFolio;

    /**
     * Los estatus inscripción a incluir en la búqueda.
     */
    private List<EstatusInscripcion> incluirEstatus = new ArrayList<>();

    private boolean incluirEstadosNulos = false;

    private boolean fechaCancelacionNull = true;

    private int limite = 0;

    private String tipoProductor;

    private String uuidProductor;
}
