/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.busqueda;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.productor.PreRegistroProductor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ismael
 */
@Repository("buscadorPreRegistroProductor")
@Slf4j
public class BuscadorPreRegistroProductorJpa implements BuscadorPreRegistroProductor {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<PreRegistroProductor> busca(ParametrosInscripcionProductor parametros) {
        StringBuilder sb = new StringBuilder("SELECT p FROM PreRegistroProductor p ");

        Map<String, Object> params = constuyeParametros(parametros, sb);

        sb.append("ORDER BY p.folio ");
        LOGGER.debug("Ejecutando el query: {}", sb);
        TypedQuery<PreRegistroProductor> q
                = entityManager.createQuery(sb.toString(), PreRegistroProductor.class);
        QueryUtils.setParametros(q, params);
        if (parametros.getLimite() > 0) {
            q.setMaxResults(parametros.getLimite());
        }

        long inicio = System.currentTimeMillis();
        List<PreRegistroProductor> preRegistros = q.getResultList();
        LOGGER.debug("Se encontraron {} resultados en {} ms.", preRegistros.size(),
                System.currentTimeMillis() - inicio);

        return preRegistros;
    }

    @Override
    public PreRegistroProductor buscaElemento(ParametrosInscripcionProductor parametros) {
        List<PreRegistroProductor> registros = busca(parametros);
        if (registros.isEmpty()) {
            throw new EmptyResultDataAccessException("Sin resultados.", 1);
        }

        if (registros.size() > 1) {
            throw new IncorrectResultSizeDataAccessException("Se esperaba 1 resultado.", 1);
        }

        return registros.get(0);
    }

    @Override
    public PreRegistroProductor buscaElementoUuid(String uuid) {
        Objects.requireNonNull(uuid = StringUtils.trimToNull(uuid), "El UUID no puede ser nulo.");
        ParametrosInscripcionProductor params = new ParametrosInscripcionProductor();
        params.setUuid(uuid);
        return buscaElemento(params);
    }

    @Override
    public List<PreRegistroProductor> buscaElementoCurp(String curp) {
        Objects.requireNonNull(curp = StringUtils.trimToNull(curp), "El CURP no puede ser nulo.");
        ParametrosInscripcionProductor params = new ParametrosInscripcionProductor();
        params.setCurp(curp);

        return busca(params);
    }

    @Override
    public List<Object[]> buscaAgrupado(ParametrosInscripcionProductor parametros) {
        StringBuilder sb = new StringBuilder("SELECT p.ciclo.id, p.estado.id,"
                + " COUNT(*) FROM PreRegistroProductor p ");

        Map<String, Object> params = constuyeParametros(parametros, sb);
        sb.append(" GROUP BY p.ciclo.id, p.estado.id ");
        LOGGER.debug("Ejecutando query {} ", sb);

        TypedQuery<Object[]> q = entityManager.createQuery(sb.toString(), Object[].class);
        QueryUtils.setParametros(q, params);

        return q.getResultList();
    }

    private Map<String, Object> constuyeParametros(ParametrosInscripcionProductor parametros, StringBuilder sb) {
        Map<String, Object> params = new HashMap<>();
        boolean first = true;
        if (Objects.nonNull(parametros.getId())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "p.id", "id", parametros.getId(), params);
        }

        if (Objects.nonNull(parametros.getFechaInicio())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("p.fechaCreacion >= :fechaInicio ");
            params.put("fechaInicio", parametros.getFechaInicio());
        }

        if (Objects.nonNull(parametros.getFechaFin())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("p.fechaCreacion < :fechaFin ");
            params.put("fechaFin", parametros.getFechaFin());
        }

        if (Objects.nonNull(parametros.getUuid())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "p.uuid", "uuid", parametros.getUuid(), params);
        }

        if (Objects.nonNull(parametros.getFolio())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "p.folio", "folio", parametros.getFolio(), params);
        }

        if (Objects.nonNull(parametros.getCurp())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "p.curp", "curp", parametros.getCurp(), params);
        }

        if (Objects.nonNull(parametros.getCurpBeneficiario())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "p.curpBeneficiario", "curpBeneficiario",
                    parametros.getCurpBeneficiario(), params);
        }

        if (Objects.nonNull(parametros.getRfcProductor())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "p.rfc", "rfc", parametros.getRfcProductor(), params);
        }

        if (Objects.nonNull(parametros.getNombre())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "p.nombre", "nombre", parametros.getNombre(), params);
        }

        if (Objects.nonNull(parametros.getPapellido())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "p.primerApellido", "primerApellido", parametros.getPapellido(), params);
        }

        if (Objects.nonNull(parametros.getSapellido())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "p.segundoApellido", "segundoApellido", parametros.getSapellido(), params);
        }

        if (Objects.nonNull(parametros.getCultivo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "p.cultivo", "cultivo", parametros.getCultivo(), params);
        }

        if (Objects.nonNull(parametros.getCiclo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "p.ciclo", "ciclo", parametros.getCiclo(), params);
        }

        if (Objects.nonNull(parametros.getEstado())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "p.estado", "estado", parametros.getEstado(), params);
        }

        if (parametros.isIncluirEstadosNulos()) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("p.estado IS NULL ");
        }

        if (parametros.isFechaCancelacionNull()) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("p.fechaCancelacion IS NULL ");
        }
        return params;
    }

    @Override
    public List<Integer> busca(String curp, Cultivo cultivo, CicloAgricola ciclo) {
        return entityManager.createQuery("SELECT 1 FROM PreRegistroProductor p "
                + "WHERE p.cultivo = :cultivo AND p.ciclo = :ciclo AND p.curp = :curp", Integer.class)
                .setParameter("ciclo", ciclo)
                .setParameter("cultivo", cultivo)
                .setParameter("curp", curp)
                .getResultList();
    }
}
