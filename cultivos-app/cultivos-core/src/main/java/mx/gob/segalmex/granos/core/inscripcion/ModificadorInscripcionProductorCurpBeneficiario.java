/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.inscripcion;

import java.util.Map;
import java.util.Objects;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.validador.ValidadorCurpHelper;
import mx.gob.segalmex.common.core.validador.ValidadorInscripcionProductorBeneficiario;
import mx.gob.segalmex.granos.core.productor.DatoCapturadoProductorEnum;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.Inscripcion;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Component("modificadorInscripcionProductorCurpBeneficiario")
public class ModificadorInscripcionProductorCurpBeneficiario implements ModificadorInscripcion {

    @Autowired
    private BuscadorInscripcionProductor buscadorInscripcionProductor;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    @Qualifier("validadorInscripcionProductorBeneficiario")
    private ValidadorInscripcionProductorBeneficiario validadorInscripcionProductorBeneficiario;

    @Override
    public void modifica(Inscripcion inscripcion, Map<String, DatoCapturado> datos) {
        DatoCapturado d = datos.get(DatoCapturadoProductorEnum.CURP_BENEFICIARIO.getClave());
        if (Objects.isNull(d)) {
            return;
        }
        if (Objects.isNull(d.getCorrecto())) {
            return;
        }
        if (d.getCorrecto()) {
            return;
        }

        ValidadorCurpHelper.valida(d.getValor(), "del beneficiario");
        InscripcionProductor ip = (InscripcionProductor) inscripcion;

        if (ip.getDatosProductor().getCurp().equals(d.getValor())) {
            throw new SegalmexRuntimeException("Error en el registro.",
                    "La CURP del beneficiario no puede ser igual al del productor.");
        }

        if (!ip.getCurpBeneficiario().equals(d.getValor())) {
            ip.setBeneficiarioDuplicado(isBeneficiarioDuplicado(ip, d.getValor()));
        }
        ip.setCurpBeneficiario(d.getValor());

        validadorInscripcionProductorBeneficiario.valida(ip);
    }

    private Boolean isBeneficiarioDuplicado(InscripcionProductor inscripcion, String nuevo) {
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setCultivo(inscripcion.getCultivo());
        parametros.setCiclo(inscripcion.getCiclo());
        parametros.setCurpBeneficiario(nuevo);
        parametros.setNoEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.CANCELADA.getClave()));
        return !buscadorInscripcionProductor.busca(parametros).isEmpty();
    }
}
