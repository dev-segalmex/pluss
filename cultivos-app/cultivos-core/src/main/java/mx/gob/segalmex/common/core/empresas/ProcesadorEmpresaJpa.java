/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.empresas;

import mx.gob.segalmex.common.core.folio.GeneradorFolio;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.pluss.modelo.catalogos.UsoCatalogo;
import mx.gob.segalmex.pluss.modelo.secuencias.TipoFolioEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.Empresa;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jurgen
 */
@Service("procesadorEmpresa")
public class ProcesadorEmpresaJpa implements ProcesadorEmpresa {

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private GeneradorFolio generadorFolio;

    @Override
    public Empresa procesa(Empresa inscripcion) {
        inscripcion.setClave(generadorFolio.genera(TipoFolioEnum.SEQ_FOLIO_EMPRESA, 4));
        inscripcion.setOrden(0);
        inscripcion.setActivo(true);
        registroEntidad.guarda(inscripcion);
        return inscripcion;
    }

    @Override
    public Sucursal procesa(Sucursal sucursal) {
        sucursal.setClave(generadorFolio.genera(TipoFolioEnum.SEQ_FOLIO_SUCURSAL, 4));
        sucursal.setOrden(0);
        sucursal.setActivo(true);
        registroEntidad.guarda(sucursal);
        return sucursal;
    }

    @Override
    public UsoCatalogo procesa(UsoCatalogo uso) {
        uso.setOrden(0);
        registroEntidad.guarda(uso);
        return uso;
    }

    @Override
    @Transactional
    public Sucursal actualiza(Sucursal actual, Sucursal nueva) {
        actual.setDireccion(nueva.getDireccion());
        actual.setEstado(nueva.getEstado());
        actual.setLatitud(nueva.getLatitud());
        actual.setLocalidad(nueva.getLocalidad());
        actual.setLongitud(nueva.getLongitud());
        actual.setMunicipio(nueva.getMunicipio());
        actual.setNombre(nueva.getNombre());
        actual.setTipo(nueva.getTipo());
        registroEntidad.actualiza(actual);
        return actual;
    }
}
