/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.validador;

import mx.gob.segalmex.pluss.modelo.productor.PredioDocumento;

/**
 * Interfaz para definir las acciones a realizar sobre objetos de tipo
 * {@link PredioDocumento}.
 *
 * @author oscar
 */
public interface ValidadorDocumento {

    /**
     * Se encarga de válidar un {@link PredioDocumento}.
     *
     * @param predioDocumento el {@link PredioDocumento} a validar.
     * @return true si es válido respecto a la lógica y próposito que se
     * implemente, false en otro caso.
     */
    boolean valida(PredioDocumento predioDocumento);
}
