/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.inscripcion;

import java.util.List;
import java.util.Map;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.validador.ValidadorRfcHelper;
import mx.gob.segalmex.granos.core.productor.DatoCapturadoProductorEnum;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.granos.core.rfc.busqueda.BuscadorRfcExcepcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.Inscripcion;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component("modificadorInscripcionProductorRfc")
@Slf4j
public class ModificadorInscripcionProductorRfc implements ModificadorInscripcion {

    @Autowired
    private BuscadorInscripcionProductor buscadorInscripcion;

    @Autowired
    private BuscadorRfcExcepcion buscadorRfcExcepcion;

    @Override
    public void modifica(Inscripcion inscripcion, Map<String, DatoCapturado> datos) {
        DatoCapturado d = datos.get(DatoCapturadoProductorEnum.RFC_PRODUCTOR.getClave());
        if (Objects.isNull(d)) {
            return;
        }
        if (Objects.isNull(d.getCorrecto())) {
            return;
        }
        if (d.getCorrecto()) {
            return;
        }

        List<String> excepciones = buscadorRfcExcepcion.getExcepciones(d.getValor(), true);
        ValidadorRfcHelper.valida(d.getValor(), excepciones, "del productor");
        ParametrosInscripcionProductor p = new ParametrosInscripcionProductor();
        p.setRfcProductor(d.getValor());
        p.setCiclo(inscripcion.getCiclo());
        p.setCultivo(inscripcion.getCultivo());

        List<InscripcionProductor> inscripciones = buscadorInscripcion.busca(p);
        if (!inscripciones.isEmpty()) {
            LOGGER.warn("El RFC del productor ya existe, no se actualiza la inscripcion: {}", inscripcion.getFolio());
            return;
        }
        InscripcionProductor ip = (InscripcionProductor) inscripcion;
        ip.getDatosProductor().setRfc(d.getValor());
    }

}
