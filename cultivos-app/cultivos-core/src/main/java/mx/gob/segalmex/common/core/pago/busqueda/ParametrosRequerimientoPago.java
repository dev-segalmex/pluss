/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.pago.busqueda;

import java.util.Calendar;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;

/**
 *
 * @author ismael
 */
@Getter
@Setter
public class ParametrosRequerimientoPago {

    /**
     * El identificador del requerimiento en la base de datos.
     */
    private Integer id;

    /**
     * El uuid del requerimiento.
     */
    private String uuid;

    /**
     * El estatus del requerimiento.
     */
    private String estatus;

    /**
     * El estatus que no se quiere de los requerimientos.
     */
    private String noEstatus;

    /**
     * Indica si se buscan los monitoreables.
     */
    private Boolean monitoreable;

    /**
     * La fecha de inicio de creación del requerimiento.
     */
    private Calendar fechaInicio;

    /**
     * La fecha de fin de creación del requerimiento.
     */
    private Calendar fechaFin;

    /**
     * El cultivo del requerimiento.
     */
    private Cultivo cultivo;

    /**
     * Indica si se busca los requeriminetos que no tengan un reporte generado.
     */
    private Boolean reportePendiente;

    /**
     * Indica el máximo de resultados.
     */
    private Integer maximoResultados;

    /**
     * El CEGAP del requerimiento.
     */
    private String cegap;
}
