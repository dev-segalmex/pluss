/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.validador;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.granos.core.rfc.busqueda.BuscadorRfcExcepcion;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Component("validadorInscripcionProductorRfc")
@Slf4j
public class ValidadorInscripcionProductorRfc implements ValidadorInscripcion {

    @Autowired
    private BuscadorRfcExcepcion buscadorRfcExcepcion;

    @Override
    public void valida(InscripcionProductor inscripcion) {
        LOGGER.debug("Validando RFC del productor.");
        String rfc = inscripcion.getDatosProductor().getRfc();
        List<String> excepciones = buscadorRfcExcepcion.getExcepciones(rfc, true);
        ValidadorRfcHelper.valida(rfc, excepciones, "del productor");
    }

}
