/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.inscripcion;

import java.util.Map;
import java.util.Objects;
import mx.gob.segalmex.common.core.factura.DatoCapturadoFacturaEnum;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.Inscripcion;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Component("modificadorInscripcionFacturaPrecioReal")
public class ModificadorInscripcionFacturaPrecioReal implements ModificadorInscripcion {

    @Override
    public void modifica(Inscripcion inscripcion, Map<String, DatoCapturado> datos) {
        DatoCapturado d = datos.get(DatoCapturadoFacturaEnum.PRECIO_TONELADA_REAL.getClave());
        if (Objects.isNull(d)) {
            return;
        }
        if (Objects.isNull(d.getCorrecto())) {
            return;
        }
        if (d.getCorrecto()) {
            return;
        }
        InscripcionFactura ip = (InscripcionFactura) inscripcion;
        ip.setPrecioAjuste(ip.getPrecioToneladaReal().subtract(ip.getPrecioTonelada()));
    }

}
