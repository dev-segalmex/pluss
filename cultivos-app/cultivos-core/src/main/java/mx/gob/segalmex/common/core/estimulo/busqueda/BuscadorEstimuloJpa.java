/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.estimulo.busqueda;

import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.pago.Estimulo;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.productor.TipoProductorEnum;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author jurgen
 */
@Repository("buscadorEstimulo")
@Slf4j
public class BuscadorEstimuloJpa implements BuscadorEstimulo {

    private static final String ACTIVO = "--";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Estimulo> busca(ParametrosEstimulo parametros) {
        StringBuilder sb = new StringBuilder("SELECT e FROM Estimulo e ");

        Map<String, Object> params = new HashMap<>();
        boolean first = true;

        if (Objects.nonNull(parametros.getId())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "e.id", "id", parametros.getId(), params);
        }

        if (Objects.nonNull(parametros.getFechaInicio())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append(":fechaInicio = e.fechaInicio ");
            params.put("fechaInicio", parametros.getFechaInicio());
        }

        if (Objects.nonNull(parametros.getFechaFin())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append(":fechaFin = e.fechaFin ");
            params.put("fechaFin", parametros.getFechaFin());
        }

        if (Objects.nonNull(parametros.getFechaRango())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append(":fecha >= e.fechaInicio AND :fecha < e.fechaFin ");
            params.put("fecha", parametros.getFechaRango());
        }

        if (Objects.nonNull(parametros.getCiclo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "e.ciclo", "ciclo", parametros.getCiclo(), params);
        }

        if (Objects.nonNull(parametros.getCantidad())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "e.cantidad", "cantidad", parametros.getCantidad(), params);
        }

        if (Objects.nonNull(parametros.getEstado())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "e.estado", "estado", parametros.getEstado(), params);
        }

        if (Objects.nonNull(parametros.getOrden())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "e.orden", "orden", parametros.getOrden(), params);
        }

        if (Objects.nonNull(parametros.getAplicaEstimulo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "e.aplicaEstimulo", "aplicaEstimulo", parametros.getAplicaEstimulo(), params);
        }

        if (Objects.nonNull(parametros.getTipo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "e.tipo", "tipo", parametros.getTipo(), params);
        }

        if (Objects.nonNull(parametros.getContrato())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "e.contrato", "contrato", parametros.getContrato(), params);
        }

        if (Objects.nonNull(parametros.getTipoProductor())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "e.tipoProductor", "tipoProductor", parametros.getTipoProductor(), params);
        }

        first = QueryUtils.agregaWhereAnd(first, sb);
        QueryUtils.and(sb, "e.estatusFechaBaja", "estatusFechaBaja", ACTIVO, params);

        sb.append("ORDER BY e.orden ");
        LOGGER.debug("Ejecutando el query: {}", sb);
        TypedQuery<Estimulo> q
                = entityManager.createQuery(sb.toString(), Estimulo.class);
        QueryUtils.setParametros(q, params);

        long inicio = System.currentTimeMillis();
        List<Estimulo> estimulos = q.getResultList();
        LOGGER.debug("Se encontraron {} resultados en {} ms.", estimulos.size(),
                System.currentTimeMillis() - inicio);

        return estimulos;
    }

    @Override
    public Estimulo buscaElemento(ParametrosEstimulo parametros) {
        List<Estimulo> resultados = busca(parametros);

        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontró el elemento de catálogo.", 1);
        }

        if (resultados.size() > 1) {
            throw new IncorrectResultSizeDataAccessException("Se encontró más "
                    + "de un elemnto de catálogo.", 1);
        }

        return resultados.get(0);
    }

    @Override
    public List<Estimulo> busca(CicloAgricola ciclo, Estado estado, TipoCultivo tipo,
            Calendar fecha, String tipoEstimulo, String tipoProductor) {
        Objects.requireNonNull(ciclo, "El ciclo no puede ser nulo.");
        Objects.requireNonNull(estado, "El estado no puede ser nulo.");
        Objects.requireNonNull(tipo, "El tipo no puede ser nulo.");
        Objects.requireNonNull(fecha, "La fecha no puede ser nula.");
        Objects.requireNonNull(tipoEstimulo, "El tipo estímulo no puede ser nulo.");
        Objects.requireNonNull(tipoProductor, "El tipo productor no puede ser nulo.");

        if (tipoProductor.equals(TipoProductorEnum.PEQUENO_NO_EMPADRONADO.getClave())){
            tipoProductor = TipoProductorEnum.MEDIANO.getClave();
        }

        // Buscamos primero con estimulo
        ParametrosEstimulo parametros = new ParametrosEstimulo();
        parametros.setCiclo(ciclo);
        parametros.setEstado(estado);
        parametros.setFechaRango(fecha);
        parametros.setTipo(tipoEstimulo);
        parametros.setTipoProductor(tipoProductor);
        parametros.setAplicaEstimulo("tipo-cultivo:" + tipo.getClave());

        List<Estimulo> resultados = busca(parametros);
        if (!resultados.isEmpty()) {
            return resultados;
        }

        parametros.setAplicaEstimulo("grupo-estimulo:" + tipo.getGrupoEstimulo());
        return busca(parametros);
    }

    @Override
    public List<Estimulo> busca(CicloAgricola ciclo, String tipoEstimulo, String contrato) {
        Objects.requireNonNull(ciclo, "El ciclo no puede ser nulo.");
        Objects.requireNonNull(tipoEstimulo, "El tipo estímulo no puede ser nulo.");
        Objects.requireNonNull(contrato, "El tipo contrato no puede ser nulo.");
        ParametrosEstimulo parametros = new ParametrosEstimulo();
        parametros.setCiclo(ciclo);
        parametros.setTipo(tipoEstimulo);
        parametros.setContrato(contrato);
        return busca(parametros);
    }

}
