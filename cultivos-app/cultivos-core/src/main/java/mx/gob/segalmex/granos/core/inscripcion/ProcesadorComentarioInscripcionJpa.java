/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.inscripcion;

import java.util.Objects;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.inscripcion.ComentarioInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.Inscripcion;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author ismael
 */
@Service("procesadorComentarioInscripcion")
public class ProcesadorComentarioInscripcionJpa implements ProcesadorComentarioInscripcion {

    @Autowired
    private RegistroEntidad registroEntidad;

    @Override
    public void procesa(ComentarioInscripcion comentario, Inscripcion inscripcion, Usuario usuario) {
        Objects.requireNonNull(inscripcion, "El registro no puede ser nulo.");
        Objects.requireNonNull(usuario, "El usuario no puede ser nulo.");
        comentario.setClase(inscripcion.getClass().getName());
        comentario.setReferencia(inscripcion.getUuid());
        comentario.setUsuario(usuario);
        comentario.setContenido(StringUtils.stripToNull(comentario.getContenido()));

        registroEntidad.guarda(comentario);
    }

}
