/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura;

import java.math.BigDecimal;
import java.util.List;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorFactura;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorInscripcionFactura;
import mx.gob.segalmex.common.core.factura.busqueda.ParametrosInscripcionFactura;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.pluss.modelo.factura.EstatusFacturaEnum;
import mx.gob.segalmex.pluss.modelo.factura.Factura;
import mx.gob.segalmex.pluss.modelo.factura.FacturaRelacionada;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import mx.gob.segalmex.granos.core.inscripcion.ActualizadorEstatusHelper;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component
public class FacturaGlobalHelper {

    @Autowired
    private BuscadorFactura buscadorFactura;

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorInscripcionFactura buscadorInscripcionFactura;

    @Autowired
    private ActualizadorEstatusHelper actualizadorEstatusHelper;

    /**
     * Asigna el estatus a las facturas relacionadas con la factura global.
     *
     * @param inscripcion la inscripción de la factura global.
     * @param global la factura registrada.
     */
    public void finalizaPositivo(InscripcionFactura inscripcion, Factura global) {
        if (!inscripcion.getTipoFactura().equals("global")) {
            throw new SegalmexRuntimeException("Error al finalizar positivamente la factura.",
                    "La factura no es global.");
        }

        // Calculamos si la global alcanza para respaldar a las de productores
        List<FacturaRelacionada> relacionadas = buscadorFactura.busca(inscripcion);
        BigDecimal productores = BigDecimal.ZERO;
        for (FacturaRelacionada rf : relacionadas) {
            productores = productores.add(rf.getFacturaProductor().getToneladasTotales());
        }

        EstatusInscripcionEnum estatus = EstatusInscripcionEnum.POSITIVA;
        BigDecimal disponibles = global.getToneladasDisponibles().subtract(productores);
        if (disponibles.compareTo(BigDecimal.ZERO) >= 0) {
            // Cuando la global tiene toneladas disponibles, la modificamos
            global.setToneladasDisponibles(disponibles);
            global.setEstatus(EstatusFacturaEnum.ASIGNADA.getClave());
            registroEntidad.actualiza(global);
            actualizaFacturas(relacionadas, global, EstatusFacturaEnum.ASIGNADA,
                    EstatusFacturaEnum.ASIGNADA);
        } else {
            estatus = EstatusInscripcionEnum.CANCELADA;
            actualizaFacturas(relacionadas, global, EstatusFacturaEnum.SELECCIONADA,
                    EstatusFacturaEnum.NUEVA);
            inscripcion.setComentarioEstatus("Las toneladas disponibles de la factura global no son "
                    + "suficientes para respaldar la(s) factura(s) relacionada(s).");
        }
        actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, estatus);
    }

    /**
     * Actualiza las facturas con los estatus correspondientes.
     *
     * @param relacionadas la lista de facturas relacionadas.
     * @param global la factura global.
     * @param estatusRelacion el estatus asignado a la relación de facturas.
     * @param estatusProductor el estatus asignado a las facturas de
     * productores.
     */
    private void actualizaFacturas(List<FacturaRelacionada> relacionadas, Factura global,
            EstatusFacturaEnum estatusRelacion, EstatusFacturaEnum estatusProductor) {
        for (FacturaRelacionada rf : relacionadas) {
            rf.setFacturaGlobal(global);
            rf.setEstatus(estatusRelacion.getClave());
            registroEntidad.actualiza(rf);

            Factura facturaProductor = rf.getFacturaProductor();
            facturaProductor.setEstatus(estatusProductor.getClave());
            registroEntidad.actualiza(facturaProductor);
            switch (facturaProductor.getEstatus()) {
                case "asignada": // Cuando es asignada, hacemos positivas las inscripciones
                    asignaEstatusPositivoInscripciones(facturaProductor);
                    break;
            }
        }
    }

    /**
     * Asigna el estatus positivo a las inscripciones que hacen uso de la
     * factura <code>f</code> siempre y cuando estén en estatus
     * <code>CORRECTO</code>.
     *
     * @param facturaProductor la factura de productor.
     */
    private void asignaEstatusPositivoInscripciones(Factura facturaProductor) {
        ParametrosInscripcionFactura parametros = new ParametrosInscripcionFactura();
        parametros.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.CORRECTO.getClave()));
        parametros.setUuidTimbreFiscalDigital(facturaProductor.getUuidTimbreFiscalDigital());

        List<InscripcionFactura> inscripciones = buscadorInscripcionFactura.busca(parametros);
        for (InscripcionFactura inscripcion : inscripciones) {
            actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, EstatusInscripcionEnum.POSITIVA);
            registroEntidad.actualiza(inscripcion);
        }
    }
}
