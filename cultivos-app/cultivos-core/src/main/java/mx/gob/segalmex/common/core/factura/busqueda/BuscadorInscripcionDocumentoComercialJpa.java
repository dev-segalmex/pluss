/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.busqueda;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionDocumentoComercial;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ismael
 */
@Slf4j
@Repository("buscadorInscripcionDocumentoComercial")
public class BuscadorInscripcionDocumentoComercialJpa implements BuscadorInscripcionDocumentoComercial {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public InscripcionDocumentoComercial buscaElemento(Integer id) {
        Objects.requireNonNull(id, "El id no puede ser nulo.");
        ParametrosInscripcionDocumentoComercial parametros = new ParametrosInscripcionDocumentoComercial();
        parametros.setId(id);

        List<InscripcionDocumentoComercial> resultados = busca(parametros);
        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontraron resultados.", 1);
        }

        return resultados.get(0);
    }

    @Override
    public InscripcionDocumentoComercial buscaElemento(String uuid) {
        Objects.requireNonNull(uuid, "El uuid no puede ser nulo.");
        ParametrosInscripcionDocumentoComercial parametros = new ParametrosInscripcionDocumentoComercial();
        parametros.setUuid(uuid);

        List<InscripcionDocumentoComercial> resultados = busca(parametros);
        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontraron resultados.", 1);
        }

        return resultados.get(0);
    }

    @Override
    public List<InscripcionDocumentoComercial> busca(ParametrosInscripcionDocumentoComercial parametros) {
        StringBuilder sb = new StringBuilder("SELECT i FROM InscripcionDocumentoComercial i ");

        Map<String, Object> params = new HashMap<>();
        boolean first = true;
        if (Objects.nonNull(parametros.getId())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.id", "id", parametros.getId(), params);
        }

        if (Objects.nonNull(parametros.getCultivo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.cultivo", "cultivo", parametros.getCultivo(), params);
        }

        if (Objects.nonNull(parametros.getUuid())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.uuid", "uuid", parametros.getUuid(), params);
        }

        if (Objects.nonNull(parametros.getFolio())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.folio", "folio", parametros.getFolio(), params);
        }

        if (Objects.nonNull(parametros.getEstatus())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.estatus", "estatus", parametros.getEstatus(), params);
        }

        if (Objects.nonNull(parametros.getRfcEmpresa())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.rfcEmpresa", "rfcEmpresa", parametros.getRfcEmpresa(), params);
        }

        if (Objects.nonNull(parametros.getFechaInicio())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("i.fechaCreacion >= :fechaInicio ");
            params.put("fechaInicio", parametros.getFechaInicio());
        }

        if (Objects.nonNull(parametros.getFechaFin())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("i.fechaCreacion < :fechaFin ");
            params.put("fechaFin", parametros.getFechaFin());
        }

        if (Objects.nonNull(parametros.getEmpresa())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.sucursal.empresa", "empresa", parametros.getEmpresa(), params);
        }

        if (Objects.nonNull(parametros.getSucursal())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.sucursal", "sucursal", parametros.getSucursal(), params);
        }

        if (Objects.nonNull(parametros.getNoEstatus())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            sb.append("i.estatus != :noEstatus ");
            params.put("noEstatus", parametros.getNoEstatus());
        }

        if (Objects.nonNull(parametros.getValidador())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.usuarioValidador", "usuarioValidador", parametros.getValidador(), params);
        }

        sb.append("ORDER BY i.folio, i.fechaCreacion ");
        LOGGER.debug("Ejecutando el query: {}", sb);
        TypedQuery<InscripcionDocumentoComercial> q
                = entityManager.createQuery(sb.toString(), InscripcionDocumentoComercial.class);
        QueryUtils.setParametros(q, params);

        long inicio = System.currentTimeMillis();
        List<InscripcionDocumentoComercial> inscripciones = q.getResultList();
        LOGGER.debug("Se encontraron {} resultados en {} ms.", inscripciones.size(),
                System.currentTimeMillis() - inicio);

        return inscripciones;

    }
}
