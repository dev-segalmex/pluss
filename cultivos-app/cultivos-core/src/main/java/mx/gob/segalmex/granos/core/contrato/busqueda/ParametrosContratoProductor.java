/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato.busqueda;

import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;

/**
 *
 * @author jurgen
 */
@Getter
@Setter
public class ParametrosContratoProductor {

    private InscripcionContrato inscripcion;

    private boolean correcto;

    private String nombre;

    private String primerApellido;

    private String segundoApellido;

    private String curp;

    private String rfc;

    private Cultivo cultivo;

    private CicloAgricola ciclo;

    private EstatusInscripcion estatus;

}
