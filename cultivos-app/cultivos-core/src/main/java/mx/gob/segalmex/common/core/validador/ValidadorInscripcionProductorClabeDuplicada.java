/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.validador;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Component("validadorInscripcionProductorClabeDuplicada")
@Slf4j
public class ValidadorInscripcionProductorClabeDuplicada implements ValidadorInscripcion {

    private static final String PERMITE_DUPLICADO = ":permite-duplicado";

    @Autowired
    private BuscadorInscripcionProductor buscadorInscripcion;

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Override
    public void valida(InscripcionProductor inscripcion) {
        String clabe = inscripcion.getCuentaBancaria().getClabe();
        if (omitir(inscripcion.getCultivo(), inscripcion.getCiclo(), clabe + PERMITE_DUPLICADO)) {
            LOGGER.info("Se omite la validación de clabe duplicada para {}", clabe);
            return;
        }
        ParametrosInscripcionProductor params = new ParametrosInscripcionProductor();
        params.setCultivo(inscripcion.getCultivo());
        params.setCiclo(inscripcion.getCiclo());
        params.setClabe(clabe);
        List<InscripcionProductor> inscripciones = buscadorInscripcion.busca(params);
        if (!inscripciones.isEmpty()) {
            List<String> folios = new ArrayList<>();
            for (InscripcionProductor ip : inscripciones) {
                folios.add(ip.getFolio());
            }
            throw new SegalmexRuntimeException("Error al realizar el registro.",
                    "La CLABE del productor ya ha sido registrada en los folios: " + String.join(", ", folios));
        }
    }

    private boolean omitir(Cultivo c, CicloAgricola ciclo, String clave) {
        Parametro p = buscadorParametro.buscaElementoOpcional(c.getClave() + ":" + ciclo.getClave() + ":" + clave);
        return Objects.nonNull(p) && Boolean.valueOf(p.getValor());
    }

}
