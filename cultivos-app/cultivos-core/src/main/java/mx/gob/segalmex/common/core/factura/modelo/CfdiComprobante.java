/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.modelo;

import java.math.BigDecimal;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 *
 * @author ismael
 */
@Getter
@Setter
@XmlRootElement(name = "Comprobante", namespace = "http://www.sat.gob.mx/cfd/3")
@XmlAccessorType(XmlAccessType.NONE)
public class CfdiComprobante {

    @XmlAttribute(name = "Total")
    private BigDecimal total;

    @XmlAttribute(name = "Moneda")
    private String moneda;

    @XmlAttribute(name = "Fecha")
    private String fecha;

    @XmlAttribute(name = "MetodoPago")
    private String metodoPago;

    @XmlElement(name = "Emisor", namespace = "http://www.sat.gob.mx/cfd/3")
    private CfdiEmisor emisor;

    @XmlElement(name = "Receptor", namespace = "http://www.sat.gob.mx/cfd/3")
    private CfdiReceptor receptor;

    @XmlElementWrapper(name = "Conceptos", namespace = "http://www.sat.gob.mx/cfd/3")
    @XmlElement(name = "Concepto", namespace = "http://www.sat.gob.mx/cfd/3")
    private List<CfdiConcepto> conceptos;

    @XmlElement(name = "Complemento", namespace = "http://www.sat.gob.mx/cfd/3")
    private CfdiComplemento complemento;

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("total", total)
                .append("moneda", moneda)
                .append("emisor", emisor)
                .append("receptor", receptor)
                .append("conceptos", conceptos)
                .append("complemento", complemento)
                .toString();

    }

}
