package mx.gob.segalmex.pluss.cultivos.core;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.catalogos.Localidad;

public interface BuscadorLocalidad {

    List<Localidad> busca(ParametrosLocalidad parametros);

    List<Localidad> busca(String codigoPostal);
}
