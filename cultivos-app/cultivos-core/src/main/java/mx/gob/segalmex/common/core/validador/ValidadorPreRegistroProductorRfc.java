package mx.gob.segalmex.common.core.validador;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.granos.core.rfc.busqueda.BuscadorRfcExcepcion;
import mx.gob.segalmex.pluss.modelo.productor.PreRegistroProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Component("validadorPreRegistroProductorRfc")
@Slf4j
public class ValidadorPreRegistroProductorRfc implements ValidadorPreRegistro {

    @Autowired
    private BuscadorRfcExcepcion buscadorRfcExcepcion;

    @Override
    public void valida(PreRegistroProductor preRegistro) {
        LOGGER.info("Validando RFC del pre-registro...");
        List<String> excepciones = buscadorRfcExcepcion.getExcepciones(preRegistro.getRfc(), true);
        ValidadorRfcHelper.valida(preRegistro.getRfc(), excepciones, "del productor");
    }

}
