/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.folio.GeneradorFolio;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.common.core.validador.ValidadorPreRegistro;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import mx.gob.segalmex.pluss.modelo.secuencias.TipoFolioEnum;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.productor.CoberturaSeleccionada;
import mx.gob.segalmex.pluss.modelo.productor.PreRegistroProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioPreRegistro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author jurgen
 */
@Service("procesadorPreRegistro")
@Slf4j
public class ProcesadorPreRegistroProductorJpa implements ProcesadorPreRegistroProductor {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Autowired
    private GeneradorFolio generadorFolio;

    private List<ValidadorPreRegistro> validadadores;

    public void setValidadadores(List<ValidadorPreRegistro> validadadores) {
        this.validadadores = validadadores;
    }

    @Transactional
    @Override
    public void procesa(PreRegistroProductor preRegistro, Usuario u, boolean generaFolio) {
        LOGGER.debug("Procesando pre registro...");
        Objects.requireNonNull(u, "El usuario no puede ser nulo.");
        if (generaFolio) {
            verificaCierre(preRegistro);
            String folio = generadorFolio.genera(TipoFolioEnum.SEQ_FOLIO_PRE_REGISTRO, 6);
            preRegistro.setFolio(folio);
        }
        preRegistro.setUuid(UUID.randomUUID().toString());
        preRegistro.setUsuarioRegistra(u);
        preRegistro.setTiposCultivo(getTiposCultivo(preRegistro.getPredios()));
        preRegistro.setEtiquetaTipoCultivo(getEtiquetaTipoCultivo(preRegistro.getTiposCultivo()));
        preRegistro.setEstado(EstadoProductorHelper.getEstado(preRegistro.getPredios()));
        registroEntidad.guarda(preRegistro);

        int orden = 0;
        for (PredioPreRegistro p : preRegistro.getPredios()) {
            p.setPreRegistroProductor(preRegistro);
            p.setOrden(orden++);
            registroEntidad.guarda(p);
        }

        if (Objects.nonNull(preRegistro.getCoberturas())) {
            orden = 0;
            for (CoberturaSeleccionada cs : preRegistro.getCoberturas()) {
                cs.setPreRegistro(preRegistro);
                cs.setOrden(orden++);
                registroEntidad.guarda(cs);
            }
        }
    }

    @Transactional
    @Override
    public void sustituye(PreRegistroProductor original, PreRegistroProductor editado, Usuario u) {
        Objects.requireNonNull(u, "El usuario no puede ser nulo.");
        Calendar fechaCancelacion = Calendar.getInstance();
        String folio = original.getFolio();
        original.setFolio(folio + "." + SegalmexDateUtils.format(fechaCancelacion, "yyy-MM-dd-mm:hh:ss"));
        original.setFechaCancelacion(fechaCancelacion);
        registroEntidad.actualiza(original);
        entityManager.flush();
        for (ValidadorPreRegistro v : validadadores) {
            v.valida(editado);
        }
        editado.setId(null);
        editado.setFolio(folio);
        for (PredioPreRegistro p : editado.getPredios()) {
            p.setId(null);
        }
        procesa(editado, u, false);
    }

    /**
     * Se encarga de recolectar todos los tipos de cultivos seleccionados en los
     * predios.
     *
     * @param predios
     * @return
     */
    private List<TipoCultivo> getTiposCultivo(List<PredioPreRegistro> predios) {
        Map<String, TipoCultivo> tipos = new HashMap<>();

        predios.forEach((pp) -> {
            tipos.put(pp.getTipoCultivo().getClave(), pp.getTipoCultivo());
        });
        return new ArrayList<>(tipos.values());
    }

    /**
     * Se encarga de colocar la etiqueta dado los tipos de cultivos
     * seleccionados por el usuario pero se coloca sin repetir alguno.
     *
     * @param tiposCultivo los tipos cultivos seleccionados.
     * @return un string con la etiqueta adecuada.
     */
    private String getEtiquetaTipoCultivo(List<TipoCultivo> tiposCultivo) {
        List<String> tipos = new ArrayList<>();
        Collections.sort(tiposCultivo, (TipoCultivo p1, TipoCultivo p2) -> new Integer(p1.getOrden()).compareTo(new Integer(p2.getOrden())));
        for (TipoCultivo tc : tiposCultivo) {
            tipos.add(tc.getNombre());
        }
        return String.join(", ", tipos);
    }

    /**
     * Se encarga de verificar si se permite el pre-registro de productores dado
     * el cultivo, ciclo y fecha de registro.
     *
     * @param preRegistro
     */
    private void verificaCierre(PreRegistroProductor preRegistro) {
        Parametro parametro = buscadorParametro.buscaElementoOpcional(preRegistro.getCultivo()
                .getClave() + ":" + preRegistro.getCiclo().getClave() + ":pre-registro:cierre");
        if (Objects.nonNull(parametro)) {
            Calendar cierre = SegalmexDateUtils
                    .parseCalendar(parametro.getValor(), "yyyy-MM-dd'T'HH:mm:ss");
            if (Calendar.getInstance().after(cierre)) {
                throw new SegalmexRuntimeException("Error al guardar el pre registro.",
                        "El pre registro de productores cerró el " + parametro.getValor());
            }
        }
    }

}
