/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.factura.Factura;
import mx.gob.segalmex.pluss.modelo.factura.FacturaRelacionada;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;

/**
 *
 * @author ismael
 */
public interface BuscadorFactura {

    List<Factura> busca(ParametrosCfdi parametros);

    Factura buscaElemento(ParametrosCfdi parametros);

    Factura buscaElemento(String uuid);

    Factura buscaElementoPorUuidTfd(String uuid);

    List<FacturaRelacionada> busca(InscripcionFactura inscripcion);
}
