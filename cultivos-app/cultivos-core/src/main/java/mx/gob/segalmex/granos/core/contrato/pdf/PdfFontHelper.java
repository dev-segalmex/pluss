/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato.pdf;

import com.lowagie.text.Font;
import com.lowagie.text.FontFactory;
import java.awt.Color;

/**
 *
 * @author ismael
 */
public class PdfFontHelper {

    /**
     * Ruta de la fuente Soberana Sans Light.
     */
    private static final String SOBERANA_SANS_LIGHT_PATH = "/mx/gob/fonts/SoberanaSans-Light.otf";

    /**
     * El alias para la fuente SoberanaSans-Light.
     */
    private static final String SOBERANA_SANS_LIGHT = "SoberanaSans-Light";

    /**
     * Obtiene la fuente <code>SoberanaSans-Light</code> con el tamaño, estilo y color
     * proporcionados.
     *
     * @param size el tamaño de la fuente.
     * @param style el estilo de la fuente.
     * @param color el color de la fuente.
     *
     * @return la fuente <code>SoberanaSans-Light</code> con el tamaño, estilo y color
     * proporcionados.
     */
    public static Font getSoberanaSansLight(float size, int style, Color color) {
        if (!FontFactory.isRegistered(SOBERANA_SANS_LIGHT)) {
            FontFactory.register(SOBERANA_SANS_LIGHT_PATH, SOBERANA_SANS_LIGHT);
        }

        return FontFactory.getFont(SOBERANA_SANS_LIGHT, size, style, color);
    }

}
