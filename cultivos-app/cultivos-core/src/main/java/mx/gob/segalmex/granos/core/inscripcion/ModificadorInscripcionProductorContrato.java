/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.inscripcion;

import java.util.List;
import java.util.Map;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.granos.core.contrato.busqueda.BuscadorInscripcionContrato;
import mx.gob.segalmex.granos.core.contrato.busqueda.ParametrosInscripcionContrato;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.Inscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;
import mx.gob.segalmex.pluss.modelo.productor.ContratoFirmado;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component("modificadorInscripcionProductorContrato")
public class ModificadorInscripcionProductorContrato implements ModificadorInscripcion {

    @Autowired
    private BuscadorInscripcionContrato buscadorInscripcion;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Override
    public void modifica(Inscripcion inscripcion, Map<String, DatoCapturado> datos) {
        InscripcionProductor ip = (InscripcionProductor) inscripcion;
        for (ContratoFirmado cf : ip.getContratos()) {
            if (!cf.isCorregido()) {
                continue;
            }
            ParametrosInscripcionContrato p = new ParametrosInscripcionContrato();
            p.setCiclo(inscripcion.getCiclo());
            p.setCultivo(inscripcion.getCultivo());
            p.setNumeroContrato(cf.getNumeroContrato());
            p.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                    EstatusInscripcionEnum.POSITIVA));
            List<InscripcionContrato> inscripciones = buscadorInscripcion.busca(p);
            if (inscripciones.isEmpty()) {
                throw new SegalmexRuntimeException("Error en el registro.",
                        "El número de contrato no se encuentra en estatus positivo.");
            }
            InscripcionContrato ic = inscripciones.get(0);
            cf.setFolio(ic.getFolio());
            cf.setEmpresa(ic.getSucursal().getEmpresa().getNombre());
        }
    }

}
