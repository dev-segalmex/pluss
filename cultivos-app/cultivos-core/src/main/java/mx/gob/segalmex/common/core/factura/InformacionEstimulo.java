/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura;

import java.math.BigDecimal;
import lombok.Getter;

/**
 *
 * @author ismael
 */
@Getter
public class InformacionEstimulo {

    private final BigDecimal limite;

    private final BigDecimal estimulo;

    /**
     * El orden del estímulo.
     */
    private final int orden;

    public InformacionEstimulo(BigDecimal limite, BigDecimal estimulo, int orden) {
        this.limite = limite;
        this.estimulo = estimulo;
        this.orden = orden;
    }

}
