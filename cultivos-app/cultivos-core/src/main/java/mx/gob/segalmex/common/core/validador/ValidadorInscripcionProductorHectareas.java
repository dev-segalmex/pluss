/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.validador;

import java.math.BigDecimal;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Component("validadorInscripcionProductorHectareas")
public class ValidadorInscripcionProductorHectareas implements ValidadorInscripcion {

    private static final String MAXIMO_SUPERFICIE = ":maximo-hectareas";

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Override
    public void valida(InscripcionProductor inscripcion) {
        Parametro p = buscadorParametro.buscaElemento(inscripcion.getCultivo().getClave()
                + MAXIMO_SUPERFICIE);
        BigDecimal maximo = new BigDecimal(p.getValor());
        // Si el máximo es menor o igual a cero, no validamos
        if (maximo.compareTo(BigDecimal.ZERO) <= 0) {
            return;
        }

        // Sumamos la superficie de los predios (en hectáreas)
        BigDecimal superficie = BigDecimal.ZERO;
        for (PredioProductor pp : inscripcion.getPrediosActivos()) {
            superficie = superficie.add(pp.getSuperficie());
        }

        if (superficie.compareTo(maximo) > 0) {
            throw new SegalmexRuntimeException("Error al realizar el registro.", "Superficie sembrada es mayor al número máximo.");
        }
    }
}
