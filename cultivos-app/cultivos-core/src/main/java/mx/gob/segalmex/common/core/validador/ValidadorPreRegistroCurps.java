package mx.gob.segalmex.common.core.validador;

import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.pluss.modelo.productor.PreRegistroProductor;
import org.springframework.stereotype.Component;

/**
 * Se encarga de validar que la curp de productor y beneficiario sean válidas.
 *
 * @author oscar
 */
@Component("validadorPreRegistroCurps")
@Slf4j
public class ValidadorPreRegistroCurps implements ValidadorPreRegistro {

    @Override
    public void valida(PreRegistroProductor preRegistro) {
        LOGGER.info("Validando curps del pre registro...");
        ValidadorCurpHelper.valida(preRegistro.getCurp(), "del productor");
        ValidadorCurpHelper.valida(preRegistro.getCurpBeneficiario(), "del beneficiario");
    }

}
