/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.estimulo;

import mx.gob.segalmex.pluss.modelo.pago.Estimulo;

/**
 *
 * @author jurgen
 */
public interface ProcesadorEstimulo {

    void procesa(Estimulo estimulo);

    void modifica(Estimulo estimulo, Estimulo actualiza);

    void elimina(Estimulo estimulo);

}
