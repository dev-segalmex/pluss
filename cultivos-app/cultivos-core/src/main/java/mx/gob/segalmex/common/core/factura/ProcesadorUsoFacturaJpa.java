/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura;

import java.math.BigDecimal;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorUsoFactura;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.pluss.modelo.factura.EstatusUsoFactura;
import mx.gob.segalmex.pluss.modelo.factura.EstatusUsoFacturaEnum;
import mx.gob.segalmex.pluss.modelo.factura.UsoFactura;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author oscar
 */
@Service("procesadorUsoFactura")
@Slf4j
public class ProcesadorUsoFacturaJpa implements ProcesadorUsoFactura {

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorUsoFactura buscadorUsoFactura;

    @Autowired
    private BuscadorInscripcionProductor buscadorInscripcionProductor;

    @Override
    @Transactional
    public void cambiaEstatus(List<UsoFactura> usos, EstatusUsoFacturaEnum nuevo,
            EstatusUsoFacturaEnum actual, String comentarioEstatus) {
        validaEstatus(usos, actual);
        EstatusUsoFactura estatus = buscadorCatalogo.buscaElemento(EstatusUsoFactura.class, nuevo);
        for (UsoFactura uf : usos) {
            uf.setEstatus(estatus);
            uf.setComentarioEstatus(StringUtils.trimToNull(comentarioEstatus));
            registroEntidad.actualiza(uf);
        }
    }

    private void validaEstatus(List<UsoFactura> usos, EstatusUsoFacturaEnum valido) {
        for (UsoFactura uf : usos) {
            if (!uf.getEstatus().getClave().equals(valido.getClave())) {
                LOGGER.error("El usoFactura {} no se puede cambiar de estatus.", uf.getUuid());
                throw new SegalmexRuntimeException("Error al cambiar de estatus.",
                        "El pago no esta en un estatus correcto para realizar esta acción");
            }
        }
    }

    private BigDecimal getSuperficiePredio(InscripcionProductor prod, UsoFactura uso) {
        LOGGER.info("Inscripcion productor : {}", prod.getFolio());
        for (PredioProductor pre : prod.getPrediosActivos()) {
            if (pre.getEstado().getClave().equals(uso.getEstado().getClave()) && pre.getTipoCultivo().getClave().equals(uso.getTipoCultivo().getClave())) {
                return pre.getSuperficie();
            }
        }
        return BigDecimal.ZERO;
    }

}
