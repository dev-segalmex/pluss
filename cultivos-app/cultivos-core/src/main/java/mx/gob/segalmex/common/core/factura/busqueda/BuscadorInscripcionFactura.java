/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;

/**
 *
 * @author ismael
 */
public interface BuscadorInscripcionFactura {

    InscripcionFactura buscaElemento(Integer id);

    InscripcionFactura buscaElemento(String uuid);

    List<InscripcionFactura> busca(ParametrosInscripcionFactura parametros);

    InscripcionFactura buscaElemento(ParametrosInscripcionFactura parametros);

    List<Object[]> buscaAgrupada(ParametrosInscripcionFactura parametros);

    List<Object[]> buscaResumen(ParametrosInscripcionFactura parametros);
}
