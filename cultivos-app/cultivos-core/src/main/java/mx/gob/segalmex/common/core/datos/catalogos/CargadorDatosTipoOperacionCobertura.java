/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.datos.catalogos;

import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.contrato.TipoOperacionCobertura;

/**
 *
 * @author erikcam
 */
public class CargadorDatosTipoOperacionCobertura extends AbstractCargadorDatosCatalogo<TipoOperacionCobertura>{

    @Override
    public TipoOperacionCobertura getInstance(String[] elementos) {
        TipoOperacionCobertura tipo = new  TipoOperacionCobertura();

        tipo.setClave(elementos[0]);
        tipo.setNombre(elementos[1]);
        tipo.setOrden(Integer.parseInt(elementos[2]));
        tipo.setActivo(true);

        return tipo;
    }

}
