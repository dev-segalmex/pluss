/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.validador;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorPredioDocumento;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosPredioDocumento;
import mx.gob.segalmex.pluss.modelo.productor.PredioDocumento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Component("validadorDocumentoDuplicado")
@Slf4j
public class ValidadorDocumentoDuplicado implements ValidadorDocumento {

    @Autowired
    private BuscadorPredioDocumento buscadorPredioDocumento;

    @Override
    public boolean valida(PredioDocumento predioDocumento) {
        ParametrosPredioDocumento params = new ParametrosPredioDocumento();
        params.setUuidTimbreFiscalDigital(predioDocumento.getUuidTimbreFiscalDigital());
        List<PredioDocumento> documentos = buscadorPredioDocumento.busca(params);
        boolean duplicado = false;
        for (PredioDocumento pd : documentos) {
            // Si el ID coincide, no había un documento previo con el mismo UUID.
            if (pd.getId().equals(predioDocumento.getId())) {
                break;
            }
            LOGGER.warn("El predio documento {} usa un timbre ya registrado por el documento predio: {}",
                    predioDocumento.getId(), pd.getId());
            duplicado = true;
        }
        predioDocumento.setDuplicado(duplicado);
        return !predioDocumento.getDuplicado();
    }

}
