/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor;

import java.io.IOException;
import java.util.List;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;

/**
 *
 * @author ismael
 */
public interface ProcesadorInscripcionProductor {

    void procesa(InscripcionProductor inscripcion);

    void generaAsignacion(InscripcionProductor inscripcion, Usuario usuario) throws IOException;

    void asigna(InscripcionProductor inscripcion, Usuario usuario, Usuario validador) throws IOException;

    void validaPositivo(InscripcionProductor inscripcion, Usuario usuario, List<DatoCapturado> capturados);

    void validaNegativo(InscripcionProductor inscripcion, Usuario usuario, List<DatoCapturado> capturados);

    void cancela(InscripcionProductor inscripcion, Usuario usuario, EstatusInscripcion estatus);

    void corrige(InscripcionProductor inscripcion, Usuario usuario, List<DatoCapturado> capturados);

    void actualiza(InscripcionProductor inscripcion, List<DatoCapturado> capturados);

    void revalida(InscripcionProductor inscripcion, Usuario usuario);

    void enviaSupervision(InscripcionProductor inscripcion, Usuario usuario, List<DatoCapturado> capturados);

    /**
     * Crea la edición de una <code>InscripcionProductor</code>.
     *
     * @param inscripcion la inscripción a la que se permite la edición.
     * @param usuario el usuario que crea la edición.
     */
    void creaEdicion(InscripcionProductor inscripcion, Usuario usuario);

    /**
     * Modifica la inscripcion de un productor agregando la información que se
     * encuentra en la nueva con base en los contratos, predios y empresas
     * asociadas de la nueva.
     *
     * @param inscripcion la inscripción registrada.
     * @param nueva la nueva inscripción.
     * @param usuario el usuario que finaliza la edición.
     */
    void finalizaEdicion(InscripcionProductor inscripcion, InscripcionProductor nueva, Usuario usuario);

    /**
     *
     * @param i
     * @param index
     * @param usuario el usuario que realiza esta acción.
     */
    void eliminaPredio(InscripcionProductor i, int index, Usuario usuario);

    /**
     * Se encarga de eliminar/cancelar el contrato con orden index de la
     * inscripción recibida.
     *
     * @param i
     * @param index
     * @param usuario
     */
    void eliminaContrato(InscripcionProductor i, int index, Usuario usuario);

    /**
     * Se encarga de actualizar la propiedad cantidadSiembra del predio con
     * posición index de la inscripción i recibida.
     *
     * @param i la {@link InscripcionProductor}.
     * @param index el indice del {@link PredioProductor}.
     * @param total el total a setear en el {@link PredioProductor}.
     * @param usuario
     */
    void actualizaTotalSiembra(InscripcionProductor i, int index, int total, Usuario usuario);

    /**
     * Se encarga de actualizar la propiedad cantidadRiego del predio con
     * posición index de la inscripción i recibida.
     *
     * @param i
     * @param index
     * @param total
     * @param usuario
     */
    void actualizaTotalRiego(InscripcionProductor i, int index, int total, Usuario usuario);

    /**
     * Se encarga de indicar si la inscripción es forzada (true) o no (false).
     *
     * @param i la inscripción.
     * @param isForzada indica si es o no forzada.
     */
    void cambiaForzada(InscripcionProductor i, boolean isForzada);

    /**
     * Cambia la ubicacón del predio, de polígono a punto medio.
     *
     * @param i
     * @param index
     * @param usuario
     */
    void cambiaPuntoMedio(InscripcionProductor i, int index, Usuario usuario);

    /**
     * Cambia la ubicacón del predio, de punto medio a polígono.
     *
     * @param i
     * @param index
     * @param usuario
     */
    void cambiaPoligono(InscripcionProductor i, int index, Usuario usuario);

    /**
     * Permite validar positivamente un productor deudor.
     * @param ip
     */
    void permitePositivoDeudor(InscripcionProductor ip, Usuario usuario);
}
