/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.busqueda;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.factura.Cfdi;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ismael
 */
@Repository
@Slf4j
public class BuscadorCfdiJpa implements BuscadorCfdi {

    @PersistenceContext
    private EntityManager entityManager;

    public List<Cfdi> busca(ParametrosCfdi parametros) {
        StringBuilder sb = new StringBuilder("SELECT c FROM Cfdi c");

        Map<String, Object> params = new HashMap<>();
        boolean first = true;
        if (Objects.nonNull(parametros.getUuid())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "c.uuid", "uuid", parametros.getUuid(), params);
        }

        if (Objects.nonNull(parametros.getEstatus())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "c.estatus", "estatus", parametros.getEstatus(), params);
        }

        if (Objects.nonNull(parametros.getRfcEmisor())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "c.rfcEmisor", "rfcEmisor", parametros.getRfcEmisor(), params);
        }

        if (Objects.nonNull(parametros.getRfcReceptor())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "c.rfcReceptor", "rfcReceptor", parametros.getRfcReceptor(), params);
        }

        if (Objects.nonNull(parametros.getUuidFolioFiscalDigital())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "c.uuidFolioFiscalDigital", "uuidFolioFiscalDigital",
                    parametros.getUuidFolioFiscalDigital(), params);
        }

        LOGGER.debug("El query '{}' tiene parámetros: {}", sb, !first);
        TypedQuery<Cfdi> q = entityManager.createQuery(sb.toString(), Cfdi.class);
        QueryUtils.setParametros(q, params);

        return q.getResultList();
    }

    @Override
    public Cfdi buscaElemento(ParametrosCfdi parametros) {
        List<Cfdi> cfdis = busca(parametros);

        if (cfdis.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontraron CFDIs", 1);
        }

        if (cfdis.size() > 1) {
            throw new IncorrectResultSizeDataAccessException("Se encontraron más de un CFDI.", 1);
        }

        return cfdis.get(0);
    }

    @Override
    public Cfdi buscaElemento(String uuid) {
        Objects.requireNonNull(uuid, "El uuid no puede ser vacío.");
        ParametrosCfdi parametros = new ParametrosCfdi();
        parametros.setUuid(uuid);

        return buscaElemento(parametros);
    }

    @Override
    public List<Cfdi> buscaPorEmisor(String rfc) {
        Objects.requireNonNull(rfc, "El rfc no puede ser vacío.");
        ParametrosCfdi parametros = new ParametrosCfdi();
        parametros.setRfcEmisor(rfc);
        return busca(parametros);
    }

    @Override
    public List<Cfdi> buscaPorReceptor(String rfc) {
        Objects.requireNonNull(rfc, "El rfc no puede ser vacío.");
        ParametrosCfdi parametros = new ParametrosCfdi();
        parametros.setRfcReceptor(rfc);
        return busca(parametros);
    }
}
