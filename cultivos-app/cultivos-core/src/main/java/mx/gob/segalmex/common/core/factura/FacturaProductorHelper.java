/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.estimulo.busqueda.BuscadorEstimulo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import mx.gob.segalmex.pluss.modelo.factura.EstatusUsoFactura;
import mx.gob.segalmex.pluss.modelo.factura.EstatusUsoFacturaEnum;
import mx.gob.segalmex.pluss.modelo.factura.Factura;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import mx.gob.segalmex.pluss.modelo.factura.UsoFactura;
import mx.gob.segalmex.pluss.modelo.pago.Estimulo;
import mx.gob.segalmex.pluss.modelo.pago.TipoEstimuloEnum;
import mx.gob.segalmex.granos.core.contrato.busqueda.BuscadorInscripcionContrato;
import mx.gob.segalmex.granos.core.contrato.busqueda.ParametrosInscripcionContrato;
import mx.gob.segalmex.granos.core.inscripcion.ActualizadorEstatusHelper;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricolaEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivoEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoPrecio;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoPrecioEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;
import mx.gob.segalmex.pluss.modelo.productor.CuentaBancaria;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCicloGrupo;
import mx.gob.segalmex.pluss.modelo.productor.TipoProductorEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component
@Slf4j
public class FacturaProductorHelper {

    /**
     * El buscador de catálgoso.
     */
    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    /**
     * El buscador de parámetros.
     */
    @Autowired
    private BuscadorParametro buscadorParametro;

    /**
     * El buscador de estímulos.
     */
    @Autowired
    private BuscadorEstimulo buscadorEstimulo;

    /**
     * El buscador de inscripcion productor.
     */
    @Autowired
    private BuscadorInscripcionProductor buscadorInscripcionProductor;

    @Autowired
    private ActualizadorEstatusHelper actualizadorEstatusHelper;

    /**
     * Buscador de contratos.
     */
    @Autowired
    private BuscadorInscripcionContrato buscadorContrato;

    @Autowired
    private FacturaFactory facturaFactory;

    public List<UsoFactura> finalizaPositivo(InscripcionFactura inscripcion, ProductorCiclo pc,
            List<InformacionEstimulo> informaciones) {
        if (!inscripcion.getTipoFactura().equals("productor")) {
            throw new SegalmexRuntimeException("Error al finalizar positivamente la factura.",
                    "La factura no es de productor.");
        }

        Factura f = inscripcion.getFactura();
        List<UsoFactura> usos = new ArrayList<>();
        BigDecimal base = BigDecimal.ZERO;
        BigDecimal facturables = inscripcion.getFacturables();
        ProductorCicloGrupo pcg = getGrupo(inscripcion, pc);
        for (InformacionEstimulo ie : informaciones) {
            if (f.getToneladasDisponibles().compareTo(BigDecimal.ZERO) == 0) {
                LOGGER.warn("La factura no cuenta con toneladas disponibles.");
                break;
            }

            // Obtenemos el limite inicial de estímulo para el cultivo, estado y tipo
            BigDecimal limite = ie.getLimite().subtract(base);
            base = ie.getLimite();

            if (pcg.getToneladasFacturadas().compareTo(base) >= 0) {
                LOGGER.info("Toneladas facturadas {} superior al límite {}",
                        pcg.getToneladasFacturadas(), ie.getLimite());
                continue;
            }

            BigDecimal toneladas = getToneladasEstimulo(inscripcion, pcg, limite, facturables);
            if (Objects.nonNull(toneladas)) {
                if (toneladas.add(pcg.getToneladasFacturadas()).compareTo(base) > 0) {
                    toneladas = limite.subtract(pcg.getToneladasFacturadas());
                }
                EstatusUsoFactura nuevo = buscadorCatalogo.buscaElemento(EstatusUsoFactura.class,
                        EstatusUsoFacturaEnum.NUEVO);
                UsoFactura uso = facturaFactory.getInstance(inscripcion, pc, toneladas,
                        ie, nuevo, getCuentaBancaria(pc));
                uso.setSuperficiePredio(getSuperficie(pc, uso));
                uso.setTipoPrecio(getTipoPrecio(inscripcion));
                usos.add(uso);

                pc.setToneladasFacturadas(pc.getToneladasFacturadas().add(toneladas));
                pcg.setToneladasFacturadas(pcg.getToneladasFacturadas().add(toneladas));
                f.setToneladasDisponibles(f.getToneladasDisponibles().subtract(toneladas));
                facturables = facturables.subtract(toneladas);
            }

        }

        switch (f.getEstatus()) {
            case "asignada": // Factura productor asignada previamente, implica global aprobada
                actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, EstatusInscripcionEnum.POSITIVA);
                break;
        }

        return usos;
    }

    private BigDecimal getSuperficie(ProductorCiclo pc, UsoFactura uso) {
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setFolio(pc.getFolio());
        InscripcionProductor ip = buscadorInscripcionProductor.buscaElemento(parametros);
        for (PredioProductor pre : ip.getPredios()) {
            if (pre.getEstado().getClave().equals(uso.getEstado().getClave()) && pre.getTipoCultivo().getClave().equals(uso.getTipoCultivo().getClave())) {
                return pre.getSuperficie();
            }
        }
        return BigDecimal.ZERO;
    }

    private CuentaBancaria getCuentaBancaria(ProductorCiclo pc) {
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setFolio(pc.getFolio());
        InscripcionProductor ip = buscadorInscripcionProductor.buscaElemento(parametros);
        return ip.getCuentaBancaria();
    }

    /**
     * Obtiene las toneladas que se le puede dar estímulo con base en la
     * inscripción, el ciclo del productor, el límite de toneladas y las
     * toneladas facturables.
     *
     * @param inscripcion la inscripción de la factura.
     * @param pc el productor con la información del ciclo.
     * @param limite el límite de toneladas que se les puede dar estímulo.
     * @param facturables el límite de toneladas facturables de pago.
     * @return
     */
    private BigDecimal getToneladasEstimulo(InscripcionFactura inscripcion, ProductorCicloGrupo pcg,
            BigDecimal limite, BigDecimal facturables) {
        BigDecimal limiteCultivo = pcg.getLimite();
        BigDecimal limiteSuperior = limiteCultivo.min(pcg.getToneladasTotales());
        if (pcg.getToneladasCobertura().compareTo(BigDecimal.ZERO) > 0) {
            limiteSuperior = limiteSuperior.min(pcg.getToneladasCobertura());
        }
        if (pcg.getToneladasContratadas().compareTo(BigDecimal.ZERO) > 0) {
            limiteSuperior = limiteSuperior.min(pcg.getToneladasContratadas());
        }
        BigDecimal restantes = limiteSuperior.subtract(pcg.getToneladasFacturadas());
        LOGGER.debug("Toneladas restantes por facturar: {}", restantes);

        Factura f = inscripcion.getFactura();
        BigDecimal disponibles = f.getToneladasDisponibles();
        BigDecimal toneladas = restantes.min(limite).min(facturables).min(disponibles);

        if (toneladas.compareTo(BigDecimal.ZERO) > 0) {
            return toneladas;
        }
        return null;
    }

    public List<InformacionEstimulo> getInformaciones(InscripcionFactura inscripcion) {
        String grupo = inscripcion.getTipoCultivo().getGrupoEstimulo();
        String ciclo = inscripcion.getCiclo().getClave();
        String tipoProductor = inscripcion.getProductorCiclo().getTipoProductor().getClave();

        if (tipoProductor.equals(TipoProductorEnum.PEQUENO_NO_EMPADRONADO.getClave())) {
            tipoProductor = TipoProductorEnum.MEDIANO.getClave();
        }

        String limiteGrupo = grupo + ":" + ciclo + ":" + tipoProductor + ":limite";
        LOGGER.info("Límite grupo: {}", limiteGrupo);
        List<Parametro> limites = buscadorParametro.busca(limiteGrupo);

        List<Estimulo> estimulos = getEstimulos(inscripcion);

        if (limites.size() != estimulos.size()) {
            LOGGER.error("El tamaño de las listas de límites ({}) y estímulos ({}) no coincide.",
                    limites.size(), estimulos.size());
            throw new SegalmexRuntimeException("Error:",
                    "El tamaño de límites y estímulos no coincide.");
        }
        if (limites.isEmpty()) {
            LOGGER.error("Las listas de límites y estímulos son vacías.");
            throw new SegalmexRuntimeException("Error:",
                    "No existe información en las listas de límites y estímulos.");
        }
        List<InformacionEstimulo> informaciones = new ArrayList<>();
        for (int i = 0; i < limites.size(); i++) {
            Parametro l = limites.get(i);
            Estimulo e = estimulos.get(i);
            InformacionEstimulo ie = new InformacionEstimulo(new BigDecimal(l.getValor()),
                    e.getCantidad(), e.getOrden());
            informaciones.add(ie);
        }

        return informaciones;
    }

    /**
     * Obtenemos la lista de Estimulos a partir del una inscripcionFactura.
     */
    private List<Estimulo> getEstimulos(InscripcionFactura inscripcion) {

        if (isTrigoPrecioCerrado(inscripcion)) {
            LOGGER.debug("Se busca estímulo por tipo contrato...");
            return buscadorEstimulo.busca(inscripcion.getCiclo(), TipoEstimuloEnum.CONTRATO.getClave(),
                    inscripcion.getContrato());
        }
        return buscadorEstimulo.busca(inscripcion.getCiclo(), inscripcion.getEstadoEstimulo(),
                inscripcion.getTipoCultivo(), inscripcion.getFechaPagoEstimulo(), TipoEstimuloEnum.FACTURA.getClave(),
                inscripcion.getProductorCiclo().getTipoProductor().getClave());
    }

    private ProductorCicloGrupo getGrupo(InscripcionFactura inscripcion, ProductorCiclo pc) {
        for (ProductorCicloGrupo grupo : pc.getGrupos()) {
            if (inscripcion.getTipoCultivo().getGrupoEstimulo().equals(grupo.getGrupoEstimulo())) {
                return grupo;
            }
        }
        throw new SegalmexRuntimeException("Error en inscripción: " + inscripcion.getFolio(),
                "La inscripción es de un grupo no válido.");
    }

    private TipoPrecio getTipoPrecio(InscripcionFactura inscripcion) {
        String contrato = StringUtils.trimToNull(inscripcion.getContrato());
        if (Objects.nonNull(contrato)) {
            try {
                ParametrosInscripcionContrato parametros = new ParametrosInscripcionContrato();
                parametros.setCultivo(inscripcion.getCultivo());
                parametros.setNumeroContrato(contrato);
                parametros.setCiclo(inscripcion.getCiclo());
                return buscadorContrato.buscaElemento(parametros).getTipoPrecio();
            } catch (EmptyResultDataAccessException ouch) {
                LOGGER.warn("La factura: {} no tiene contrato.", inscripcion.getFolio());
            }
        }
        return null;
    }

    private boolean isTrigoPrecioCerrado(InscripcionFactura inscripcion) {
        if (inscripcion.getCultivo().getClave().equals(CultivoEnum.TRIGO.getClave())
                && inscripcion.getCiclo().getClave().equals(CicloAgricolaEnum.OI2021.getClave())
                && !inscripcion.getTipoCultivo().getClave().equals(TipoCultivoEnum.TRIGO_CRISTALINO.getClave())) {
            TipoPrecio tp = getContrato(inscripcion).getTipoPrecio();
            return tp.getClave().equals(TipoPrecioEnum.PRECIO_CERRADO_DOLARES.getClave())
                    || tp.getClave().equals(TipoPrecioEnum.PRECIO_CERRADO_PESOS.getClave());
        }
        return false;
    }

    private InscripcionContrato getContrato(InscripcionFactura inscripcion) {
        try {
            ParametrosInscripcionContrato parametros = new ParametrosInscripcionContrato();
            parametros.setCultivo(inscripcion.getCultivo());
            parametros.setNumeroContrato(inscripcion.getContrato());
            parametros.setCiclo(inscripcion.getCiclo());
            parametros.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.POSITIVA));
            return buscadorContrato.buscaElemento(parametros);
        } catch (EmptyResultDataAccessException ouch) {
            LOGGER.error("La incripción factura: {} no tiene contrato.", inscripcion.getFolio());
            throw new SegalmexRuntimeException("Error en inscripción: " + inscripcion.getFolio(),
                    "La inscripción no tiene contrato.");
        }
    }

}
