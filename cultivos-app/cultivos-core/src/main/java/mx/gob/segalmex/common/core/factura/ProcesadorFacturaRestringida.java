/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura;

import mx.gob.segalmex.pluss.modelo.factura.FacturaRestringida;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;

/**
 *
 * @author jurgen
 */
public interface ProcesadorFacturaRestringida {
    /**
     * Se encarga de procesar y guardar una nueva Excepción de Factura.
     *
     * @param facturaRestringida la entidad que se guardara.
     * @return regresa el {@link FacturaRestringida} generado.
     */

    FacturaRestringida procesa (FacturaRestringida facturaRestringida);

    /**
     *
     * @param fr
     * @param usuarioBaja
     */
    void elimina(FacturaRestringida fr, Usuario usuarioBaja);

}
