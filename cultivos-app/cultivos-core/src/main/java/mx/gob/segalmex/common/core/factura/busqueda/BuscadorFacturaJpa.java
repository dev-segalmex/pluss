/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.busqueda;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.factura.Factura;
import mx.gob.segalmex.pluss.modelo.factura.FacturaRelacionada;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ismael
 */
@Repository("buscadorFactura")
public class BuscadorFacturaJpa implements BuscadorFactura {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Factura> busca(ParametrosCfdi parametros) {
        StringBuilder sb = new StringBuilder(parametros.isJoinInscripcion()
                ? "SELECT f FROM InscripcionFactura i INNER JOIN i.factura f"
                : "SELECT f FROM Factura f");

        Map<String, Object> params = new HashMap<>();
        boolean first = true;
        if (Objects.nonNull(parametros.getUuid())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "f.uuid", "uuid", parametros.getUuid(), params);
        }

        if (Objects.nonNull(parametros.getEstatus())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "f.estatus", "estatus", parametros.getEstatus(), params);
        }

        if (Objects.nonNull(parametros.getRfcEmisor())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "f.rfcEmisor", "rfcEmisor", parametros.getRfcEmisor(), params);
        }

        if (Objects.nonNull(parametros.getRfcReceptor())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "f.rfcReceptor", "rfcReceptor", parametros.getRfcReceptor(), params);
        }

        if (Objects.nonNull(parametros.getUuidFolioFiscalDigital())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "f.uuidTimbreFiscalDigital", "uuidTimbreFiscalDigital",
                    parametros.getUuidFolioFiscalDigital(), params);
        }

        if (Objects.nonNull(parametros.getTipo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "f.tipo", "tipo",
                    parametros.getTipo(), params);
        }

        if (Objects.nonNull(parametros.getSucursal())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.sucursal", "sucursal", parametros.getSucursal(), params);
        }

        if (Objects.nonNull(parametros.getCiclo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.ciclo", "ciclo", parametros.getCiclo(), params);
        }

        if (Objects.nonNull(parametros.getCultivo())) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "i.cultivo", "cultivo", parametros.getCultivo(), params);
        }

        TypedQuery<Factura> q = entityManager.createQuery(sb.toString(), Factura.class);
        QueryUtils.setParametros(q, params);
        return q.getResultList();
    }

    @Override
    public Factura buscaElemento(ParametrosCfdi parametros) {
        List<Factura> cfdis = busca(parametros);

        if (cfdis.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontraron CFDIs", 1);
        }

        if (cfdis.size() > 1) {
            throw new IncorrectResultSizeDataAccessException("Se encontraron más de un CFDI.", 1);
        }

        return cfdis.get(0);
    }

    @Override
    public Factura buscaElemento(String uuid) {
        Objects.requireNonNull(uuid, "El uuid no puede ser vacío.");
        ParametrosCfdi parametros = new ParametrosCfdi();
        parametros.setUuid(uuid);

        return buscaElemento(parametros);
    }

    @Override
    public Factura buscaElementoPorUuidTfd(String uuid) {
        Objects.requireNonNull(uuid, "El uuid no puede ser vacío.");
        ParametrosCfdi parametros = new ParametrosCfdi();
        parametros.setUuidFolioFiscalDigital(uuid);

        return buscaElemento(parametros);
    }

    @Override
    public List<FacturaRelacionada> busca(InscripcionFactura inscripcion) {
        return entityManager.createQuery("SELECT fr FROM FacturaRelacionada fr "
                + "WHERE fr.inscripcion = :inscripcion ", FacturaRelacionada.class)
                .setParameter("inscripcion", inscripcion)
                .getResultList();
    }

}
