/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.busqueda;

import mx.gob.segalmex.pluss.modelo.productor.Asociado;

/**
 *
 * @author ismael
 */
public interface BuscadorAsociado {

    Asociado buscaElemento(Integer id);
}
