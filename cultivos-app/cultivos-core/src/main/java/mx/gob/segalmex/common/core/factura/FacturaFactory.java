/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Calendar;
import java.util.Objects;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.pluss.modelo.factura.Cfdi;
import mx.gob.segalmex.pluss.modelo.factura.EstatusUsoFactura;
import mx.gob.segalmex.pluss.modelo.factura.Factura;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import mx.gob.segalmex.pluss.modelo.factura.TipoUsoFactura;
import mx.gob.segalmex.pluss.modelo.factura.TipoUsoFacturaEnum;
import mx.gob.segalmex.pluss.modelo.factura.UsoFactura;
import mx.gob.segalmex.pluss.modelo.productor.CuentaBancaria;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component
@Slf4j
public class FacturaFactory {

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    public static Factura getInstance(Cfdi cfdi) {
        Factura f = new Factura();
        f.setNombreEmisor(cfdi.getNombreEmisor());
        f.setNombreReceptor(cfdi.getNombreReceptor());
        f.setRfcEmisor(cfdi.getRfcEmisor());
        f.setRfcReceptor(cfdi.getRfcReceptor());
        f.setTotal(cfdi.getTotal());
        f.setToneladasTotales(cfdi.getTotalToneladas());
        f.setToneladasDisponibles(cfdi.getTotalToneladas());
        f.setUuidTimbreFiscalDigital(cfdi.getUuidTimbreFiscalDigital());
        f.setReferencia(cfdi.getUuid());
        f.setUuid(UUID.randomUUID().toString());
        f.setFecha(DateUtils.truncate(cfdi.getFecha(), Calendar.SECOND));
        f.setFechaTimbrado(DateUtils.truncate(cfdi.getFechaTimbrado(), Calendar.SECOND));

        return f;
    }

    /**
     * Obtiene un <code>UsoFactura</code> basado en la factura, la inscripción,
     * el productor ciclo y el estímulo por tonelada.
     *
     * @param i la inscripción de la factura.
     * @param pc el productor ciclo.
     * @param toneladas las toneladas que se van a apoyar.
     * @param infoEstimulo la información del estímulo asignado.
     * @param estatus el estatus con el que se crea el uso factura.
     * @param cb los datos bancarios asociados a el UsoFactura.
     * @return
     */
    public UsoFactura getInstance(InscripcionFactura i, ProductorCiclo pc,
            BigDecimal toneladas, InformacionEstimulo infoEstimulo, EstatusUsoFactura estatus,
            CuentaBancaria cb) {
        UsoFactura uso = new UsoFactura();
        uso.setFactura(i.getFactura());
        uso.setProductor(i.getProductorCiclo().getProductor());
        uso.setFolio(i.getFolio());
        uso.setTipoCultivo(i.getTipoCultivo());
        uso.setUuid(UUID.randomUUID().toString());
        uso.setEstatus(estatus);
        uso.setProductorCiclo(pc);
        uso.setToneladas(toneladas);
        uso.setEstimuloTonelada(infoEstimulo.getEstimulo());
        uso.setTipo(buscadorCatalogo.buscaElemento(TipoUsoFactura.class,
                TipoUsoFacturaEnum.ESTIMULO.getClave()));
        uso.setEstado(i.getEstado());
        uso.setOrdenEstimulo(infoEstimulo.getOrden());
        uso.setBanco(cb.getBanco());
        uso.setClabe(cb.getClabe());
        uso.setContrato(StringUtils.trimToNull(i.getContrato()));
        if (Objects.nonNull(infoEstimulo.getEstimulo())) {
            uso.setEstimuloTotal(uso.getEstimuloTonelada().multiply(uso.getToneladas())
                    .setScale(2, RoundingMode.HALF_UP));
        }
        uso.setFechaEstimulo(i.getFechaPagoEstimulo());
        return uso;
    }

}
