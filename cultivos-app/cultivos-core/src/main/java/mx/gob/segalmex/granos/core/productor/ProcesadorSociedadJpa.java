/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.pluss.modelo.productor.Asociado;
import mx.gob.segalmex.pluss.modelo.productor.DatosProductor;
import mx.gob.segalmex.pluss.modelo.productor.EmpresaAsociada;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.Sociedad;
import mx.gob.segalmex.pluss.modelo.productor.SocioDatosProductor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ismael
 */
@Service("procesadorSociedad")
@Slf4j
public class ProcesadorSociedadJpa implements ProcesadorSociedad {

    private static final String RELACION_PRODUCTOR = "productor";

    private static final String RELACION_SOCIO = "socio";

    private static final String RELACION_REPRESENTANTE = "representante";

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private RegistroEntidad registroEntidad;

    @PersistenceContext
    private EntityManager entityManager;

    @Transactional
    @Override
    public void procesa(InscripcionProductor inscripcion) {
        switch (inscripcion.getDatosProductor().getTipoPersona().getClave()) {
            case "fisica":
                LOGGER.debug("Registrando socios de productor persona física.");
                procesaFisica(inscripcion);
                break;
            case "moral":
                LOGGER.debug("Registrando socios de productor persona moral.");
                procesaMoral(inscripcion);
                break;
            default:
                throw new IllegalArgumentException("El tipo de persona es incorrecto.");
        }
    }

    private void procesaFisica(InscripcionProductor inscripcion) {
        List<EmpresaAsociada> empresas = inscripcion.getEmpresasActivas();
        DatosProductor dp = inscripcion.getDatosProductor();
        for (EmpresaAsociada es : empresas) {
            Sociedad s;
            try {
                s = buscadorCatalogo.buscaElemento(Sociedad.class, es.getRfc());
            } catch (EmptyResultDataAccessException ouch) {
                s = new Sociedad();
                s.setActivo(true);
                s.setClave(es.getRfc());
                s.setNombre(es.getNombre());
                s.setFolio(inscripcion.getFolio());
                registroEntidad.guarda(s);
            }

            procesaAsociado(inscripcion, s, dp.getCurp(), dp.getRfc(),
                    construyeNombreCompleto(dp), RELACION_PRODUCTOR);
        }
    }

    private void procesaMoral(InscripcionProductor inscripcion) {
        Sociedad s;
        try {
            s = buscadorCatalogo.buscaElemento(Sociedad.class, inscripcion.getDatosProductor().getRfc());
        } catch (EmptyResultDataAccessException ouch) {
            s = new Sociedad();
            s.setActivo(true);
            s.setClave(inscripcion.getDatosProductor().getRfc());
            s.setNombre(inscripcion.getDatosProductor().getNombreMoral());
            s.setFolio(inscripcion.getFolio());
            registroEntidad.guarda(s);
        }

        for (SocioDatosProductor sdp : inscripcion.getDatosProductor().getSocios()) {
            procesaAsociado(inscripcion, s, sdp.getCurp(), null, null, RELACION_SOCIO);
        }

        DatosProductor dp = inscripcion.getDatosProductor();
        procesaAsociado(inscripcion, s, dp.getCurp(), dp.getRfc(),
                construyeNombreCompleto(dp), RELACION_REPRESENTANTE);
    }

    private Asociado procesaAsociado(InscripcionProductor i, Sociedad s, String curp, String rfc, String nombre, String relacion) {
        List<Asociado> asociados = entityManager
                .createQuery("SELECT a FROM Asociado a "
                        + "WHERE a.sociedad = :sociedad "
                        + "AND a.relacion = :relacion "
                        + "AND a.ciclo = :ciclo "
                        + "AND a.cultivo = :cultivo "
                        + "AND a.activo = :activo ", Asociado.class)
                .setParameter("sociedad", s)
                .setParameter("relacion", relacion)
                .setParameter("ciclo", i.getCiclo())
                .setParameter("cultivo", i.getCultivo())
                .setParameter("activo", Boolean.TRUE)
                .getResultList();

        Asociado registrado = null;
        for (Asociado a : asociados) {
            if (a.getCurp().equals(curp)) {
                LOGGER.debug("Asociado encontrado por CURP: {}", a);
                registrado = a;
                break;
            }

            if (Objects.nonNull(a.getRfc()) && a.getRfc().equals(rfc)) {
                LOGGER.debug("Asociado encontrado por RFC: {}", a);
                registrado = a;
                break;
            }
        }

        // Si no hay un asociado registrado, lo creamos
        if (Objects.isNull(registrado)) {
            LOGGER.info("Asociado no encontrado, registrando nuevo.");
            registrado = new Asociado();
            registrado.setActivo(true);
            registrado.setRelacion(relacion);
            registrado.setSociedad(s);
            registrado.setNombre(nombre);
            registrado.setCiclo(i.getCiclo());
            registrado.setCultivo(i.getCultivo());
            registrado.setFolio(i.getFolio());
        }

        boolean modificado = false;
        if (Objects.isNull(registrado.getCurp())) {
            registrado.setCurp(curp);
            modificado = true;
        }
        if (Objects.isNull(registrado.getRfc())) {
            registrado.setRfc(rfc);
            modificado = true;
        }

        if (Objects.isNull(registrado.getId())) {
            registroEntidad.guarda(registrado);
        } else if (modificado) {
            registroEntidad.actualiza(registrado);
        }
        return registrado;
    }

    private String construyeNombreCompleto(DatosProductor dp) {
        List<String> elementos = new ArrayList<>();
        if (Objects.nonNull(StringUtils.trimToNull(dp.getNombre()))) {
            elementos.add(dp.getNombre());
        }
        if (Objects.nonNull(StringUtils.trimToNull(dp.getPrimerApellido()))) {
            elementos.add(dp.getPrimerApellido());
        }
        if (Objects.nonNull(StringUtils.trimToNull(dp.getSegundoApellido()))) {
            elementos.add(dp.getSegundoApellido());
        }
        return String.join(" ", elementos);
    }
}
