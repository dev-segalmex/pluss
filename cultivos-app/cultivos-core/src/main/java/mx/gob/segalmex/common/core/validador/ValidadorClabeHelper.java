/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.validador;

import java.util.Objects;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;

/**
 *
 * @author ismael
 */
public class ValidadorClabeHelper {

    private static final int LONGITUD = 18;

    public static void valida(String clabe) {
        Objects.requireNonNull(clabe, "La clabe no puede ser nula.");

        if (clabe.length() != LONGITUD) {
            throw new SegalmexRuntimeException("Error al validar la CLABE.",
                    "La CLABE no es de longitud 18.");
        }

        String base = clabe.substring(0, LONGITUD - 1);
        int digitoVerificador = Integer.valueOf("" + clabe.charAt(LONGITUD - 1));
        int[] secuencia = {3, 7, 1};
        int suma = 0;
        int posicion = 0;
        for (int i = 0; i < base.length(); i++) {
            suma += (Character.getNumericValue(base.charAt(i)) * secuencia[posicion]) % 10;
            posicion++;
            posicion = posicion > 2 ? 0 : posicion;
        }
        int digitoControl = (10 - (suma % 10)) % 10;
        if (digitoVerificador != digitoControl) {
            throw new SegalmexRuntimeException("Error al realizar el registro.",
                    "La CLABE del productor no es válida.");
        }
    }
}
