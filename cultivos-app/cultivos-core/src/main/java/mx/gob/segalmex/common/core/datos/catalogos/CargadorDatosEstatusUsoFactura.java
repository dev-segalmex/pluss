/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.datos.catalogos;

import java.math.BigDecimal;
import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.factura.EstatusUsoFactura;

/**
 *
 * @author oscar
 */
public class CargadorDatosEstatusUsoFactura extends AbstractCargadorDatosCatalogo<EstatusUsoFactura> {

    @Override
    public EstatusUsoFactura getInstance(String[] elementos) {
        EstatusUsoFactura estatus = new EstatusUsoFactura();
        estatus.setClave(elementos[0]);
        estatus.setNombre(elementos[1]);
        estatus.setActivo(true);
        estatus.setOrden(Integer.parseInt(elementos[2]));
        estatus.setFactor(new BigDecimal(elementos[3]));

        return estatus;
    }

}
