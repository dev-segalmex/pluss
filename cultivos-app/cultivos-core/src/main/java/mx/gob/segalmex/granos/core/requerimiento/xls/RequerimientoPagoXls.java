/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.requerimiento.xls;

import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.util.XlsUtils;
import mx.gob.segalmex.pluss.modelo.factura.UsoFactura;
import mx.gob.segalmex.pluss.modelo.pago.RequerimientoPago;
import mx.gob.segalmex.pluss.modelo.pago.RequerimientoUsoFactura;
import mx.gob.segalmex.pluss.modelo.personas.Persona;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersonaEnum;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.stereotype.Component;

/**
 *
 * @author erikcamacho
 */
@Component("requerimientoPagoXls")
@Slf4j
public class RequerimientoPagoXls {

    private static final String VALOR_NULO = "--";

    public byte[] exporta(RequerimientoPago requerimiento) {
        SXSSFWorkbook wb = new SXSSFWorkbook(100);
        CreationHelper ch = wb.getCreationHelper();
        Sheet s = wb.createSheet();

        String[] encabezados = new String[]{
            "No.",
            "Banco",
            "F. solicitud",
            "ID de boleta",
            "ID de productor",
            "CLABE",
            "Folio",
            "Primer apellido",
            "Segundo apellido",
            "Nombre(s)",
            "Toneladas factura",
            "Estímulo por tonelada",
            "Toneladas",
            "Estímulo total",
            "Fecha estímulo",
            "CURP",
            "RFC",
            "Tipo productor",
            "Estado",
            "CLV EST",
            "CLV MUN",
            "CLV GEO",
            "Método",
            "Centro de acopio",
            "Tipo cultivo",
            "No. Contrato",
            "Tipo",
            "Referencia/Movimiento",
            "Denominación social",
            "Fecha de factura",
            "Fecha de timbrado",
            "Precio de factura",
            "Superficie (ha)"
        };

        XlsUtils.creaEncabezados(s, encabezados);

        CellStyle cs = wb.createCellStyle();
        cs.setDataFormat(ch.createDataFormat().getFormat("dd/MM/yyyy HH:mm"));

        int renglon = 1;
        for (RequerimientoUsoFactura u : requerimiento.getRequerimientos()) {
            int j = 0;
            Row r = s.createRow(renglon);

            UsoFactura uso = u.getUsoFactura();

            XlsUtils.createCell(r, j++, renglon);
            XlsUtils.createCell(r, j++, uso.getBanco().getNombre());
            XlsUtils.createCell(r, j++, u.getFechaCreacion(), cs);
            XlsUtils.createCell(r, j++, uso.getFolio());
            XlsUtils.createCell(r, j++, uso.getProductorCiclo().getFolio());
            XlsUtils.createCell(r, j++, uso.getClabe());
            XlsUtils.createCell(r, j++, uso.getFolio());
            Persona p = uso.getProductor().getPersona();
            if (uso.getProductor().getTipoPersona().getClave().equals(TipoPersonaEnum.FISICA.getClave())) {
                XlsUtils.createCell(r, j++, StringUtils.defaultIfBlank(p.getPrimerApellido(), VALOR_NULO));
                XlsUtils.createCell(r, j++, StringUtils.defaultIfBlank(p.getSegundoApellido(), VALOR_NULO));
                XlsUtils.createCell(r, j++, StringUtils.defaultIfBlank(p.getNombre(), VALOR_NULO));
            } else {
                XlsUtils.createCell(r, j++, VALOR_NULO);
                XlsUtils.createCell(r, j++, VALOR_NULO);
                XlsUtils.createCell(r, j++, uso.getProductor().getNombre());
            }
            XlsUtils.createCell(r, j++, uso.getFactura().getToneladasTotales());
            XlsUtils.createCell(r, j++, uso.getEstimuloTonelada());
            XlsUtils.createCell(r, j++, uso.getToneladas());
            XlsUtils.createCell(r, j++, uso.getEstimuloTotal());
            XlsUtils.createCell(r, j++, uso.getFechaEstimulo(), cs);
            XlsUtils.createCell(r, j++, StringUtils.defaultIfBlank(uso.getProductor().getCurp(), VALOR_NULO));
            XlsUtils.createCell(r, j++, StringUtils.defaultIfBlank(uso.getProductor().getRfc(), VALOR_NULO));
            XlsUtils.createCell(r, j++, uso.getProductorCiclo().getTipoProductor().getNombre());
            XlsUtils.createCell(r, j++, Objects.nonNull(uso.getEstado()) ? uso.getEstado().getNombre() : VALOR_NULO);
            XlsUtils.createCell(r, j++, Objects.nonNull(uso.getEstado()) ? uso.getEstado().getClave() : VALOR_NULO);
            XlsUtils.createCell(r, j++, VALOR_NULO);
            XlsUtils.createCell(r, j++, VALOR_NULO);
            XlsUtils.createCell(r, j++, uso.getOrdenEstimulo());
            XlsUtils.createCell(r, j++, VALOR_NULO);
            XlsUtils.createCell(r, j++, uso.getTipoCultivo().getNombre());
            XlsUtils.createCell(r, j++, StringUtils.defaultIfBlank(uso.getContrato(), VALOR_NULO));
            XlsUtils.createCell(r, j++, Objects.nonNull(uso.getTipoPrecio()) ? uso.getTipoPrecio().getNombre() : VALOR_NULO);
            XlsUtils.createCell(r, j++, VALOR_NULO);
            XlsUtils.createCell(r, j++, uso.getProductor().getTipoPersona().getNombre());
            XlsUtils.createCell(r, j++, uso.getFactura().getFechaCreacion(), cs);
            XlsUtils.createCell(r, j++, uso.getFactura().getFechaTimbrado(), cs);
            XlsUtils.createCell(r, j++, uso.getFactura().getTotal());
            if (Objects.nonNull(uso.getSuperficiePredio())) {
                XlsUtils.createCell(r, j++, uso.getSuperficiePredio());
            } else {
                XlsUtils.createCell(r, j++, VALOR_NULO);
            }

            renglon++;
        }

        for (int i = 0; i < encabezados.length; i++) {
            try {
                s.autoSizeColumn(i);
            } catch (RuntimeException ouch) {
                LOGGER.error("Error en la columna: {}", encabezados[i]);
                throw ouch;
            }
        }

        return XlsUtils.creaArchivo(wb);
    }

}
