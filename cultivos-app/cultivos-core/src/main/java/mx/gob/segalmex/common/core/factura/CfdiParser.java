/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura;

import mx.gob.segalmex.common.core.factura.modelo.CfdiComprobante;
import java.io.InputStream;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component
public class CfdiParser {

    public CfdiComprobante parse(InputStream is) throws JAXBException {
        JAXBContext context = JAXBContext.newInstance(CfdiComprobante.class);
        Unmarshaller um = context.createUnmarshaller();

        CfdiComprobante comprobante = (CfdiComprobante) um.unmarshal(is);
        return comprobante;
    }
}
