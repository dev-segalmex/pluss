/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.folio.GeneradorFolio;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.common.core.validador.ValidadorInscripcion;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import mx.gob.segalmex.pluss.modelo.secuencias.TipoFolioEnum;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.granos.core.contrato.busqueda.BuscadorDatoCapturado;
import mx.gob.segalmex.granos.core.historico.BuscadorHistoricoRegistro;
import mx.gob.segalmex.granos.core.historico.ProcesadorHistoricoRegistro;
import mx.gob.segalmex.granos.core.inscripcion.ActualizadorEstatusHelper;
import static mx.gob.segalmex.granos.core.inscripcion.DatoCapturadoFactory.getInstance;
import mx.gob.segalmex.granos.core.inscripcion.ProcesadorDatoCapturado;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorPredioDocumento;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosPredioDocumento;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoRegistroEnum;
import mx.gob.segalmex.pluss.modelo.historico.HistoricoRegistro;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.productor.ContratoFirmado;
import mx.gob.segalmex.pluss.modelo.productor.EmpresaAsociada;
import mx.gob.segalmex.pluss.modelo.productor.EstatusPredioProductorEnum;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionMolino;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCicloGrupo;
import mx.gob.segalmex.pluss.modelo.productor.SocioDatosProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ismael
 */
@Service
@Slf4j
public class ProcesadorInscripcionProductorJpa implements ProcesadorInscripcionProductor {

    private static final int MAXIMO_CORRECCION = 3;

    private static final String PREDIO = "Predios";

    private static final String CONTRATO = "Contratos";

    private static final String POLIGONO_A_MEDIO = "poligono-a-medio";

    private static final String MEDIO_A_POLIGONO = "medio-a-poligono";

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private GeneradorFolio generadorFolio;

    @Autowired
    private BuscadorHistoricoRegistro buscadorHistorico;

    @Autowired
    private ProcesadorHistoricoRegistro procesadorHistorico;

    @Autowired
    private ProcesadorDatoCapturado procesadorDatoCapturado;

    @Autowired
    private BuscadorDatoCapturado buscadorDatoCapturado;

    @Autowired
    private BuscadorProductor buscadorProductor;

    @Autowired
    private InscripcionProductorContratoHelper ipContratoHelper;

    @Autowired
    private InscripcionProductorEmpresaHelper ipEmpresaHelper;

    @Autowired
    private InscripcionProductorPredioHelper ipPredioHelper;

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Autowired
    private ProcesadorPredioDocumento procesadorPredioDocumento;

    @Autowired
    private BuscadorPredioDocumento buscadorPredioDocumento;

    private List<ValidadorInscripcion> validadadores;

    @Autowired
    private BuscadorInscripcionProductor buscadorInscripcionProductor;

    @Autowired
    private ActualizadorEstatusHelper actualizadorEstatusHelper;

    @Autowired
    private TipoProductorHelper tipoProductorHelper;

    @Transactional
    @Override
    public void procesa(InscripcionProductor inscripcion) {
        verificaCierre(inscripcion);
        List<String> motivos = new ArrayList<>();
        if (inscripcion.getPredios().isEmpty()) {
            motivos.add("La lista de predios no puede ser vacía.");
            throw new SegalmexRuntimeException("La lista de predios no puede ser vacía.", motivos);
        }
        validaProductorCiclo(inscripcion, "El productor ya se encuentra validado positivamente.");

        String folio = generadorFolio.genera(TipoFolioEnum.SEQ_FOLIO_PRODUCTOR_MAIZ, 6);
        inscripcion.setUuid(UUID.randomUUID().toString());
        inscripcion.setFolio(folio);
        inscripcion.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.PENDIENTE));
        inscripcion.setEstatusPrevio(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.DESCONOCIDO));
        inscripcion.setRfcEmpresa(inscripcion.getSucursal().getEmpresa().getRfc());
        inscripcion.setUsuarioAsignado(inscripcion.getUsuarioRegistra());
        inscripcion.setTiposCultivo(getTiposCultivo(inscripcion.getPredios()));
        inscripcion.setEtiquetaTipoCultivo(getEtiquetaTipoCultivo(inscripcion.getTiposCultivo()));
        inscripcion.setTipoRegistro(getTipoRegistro(inscripcion));
        registroEntidad.guarda(inscripcion.getCuentaBancaria());
        registroEntidad.guarda(inscripcion.getDomicilio());
        registroEntidad.guarda(inscripcion.getDatosProductor());
        if (Objects.nonNull(inscripcion.getInformacionSemilla())) {
            inscripcion.getInformacionSemilla().setCultivo(inscripcion.getCultivo());
            inscripcion.getInformacionSemilla().setCiclo(inscripcion.getCiclo());
            registroEntidad.guarda(inscripcion.getInformacionSemilla());
        }
        inscripcion.setEstado(EstadoProductorHelper.getEstado(inscripcion));
        inscripcion.setBeneficiarioDuplicado(isBeneficiarioDuplicado(inscripcion));
        registroEntidad.guarda(inscripcion);

        int i = 1;
        for (PredioProductor p : inscripcion.getPredios()) {
            p.setInscripcionProductor(inscripcion);
            p.setOrden(i++);
            p.setRendimiento(p.getVolumen().divide(p.getSuperficie(), 5, RoundingMode.HALF_UP));
            p.setRendimientoSolicitado(p.getVolumenSolicitado().divide(p.getSuperficie(), 5, RoundingMode.HALF_UP));
            p.setRendimientoMaximo(getRendimientoMaximo(p));
            registroEntidad.guarda(p);
        }
        inscripcion.setNumeroPredios(inscripcion.getPredios().size());

        if (Objects.nonNull(inscripcion.getContratos())) {
            i = 0;
            for (ContratoFirmado c : inscripcion.getContratos()) {
                ipContratoHelper.inicializa(c);
                c.setInscripcionProductor(inscripcion);
                c.setOrden(i++);
                registroEntidad.guarda(c);
            }
            inscripcion.setNumeroContratos(inscripcion.getContratos().size());
        }

        if (Objects.nonNull(inscripcion.getDatosProductor().getSocios())) {
            for (SocioDatosProductor s : inscripcion.getDatosProductor().getSocios()) {
                s.setDatosProductor(inscripcion.getDatosProductor());
                registroEntidad.guarda(s);
            }
        }

        if (Objects.nonNull(inscripcion.getEmpresas())) {
            i = 0;
            for (EmpresaAsociada e : inscripcion.getEmpresas()) {
                e.setInscripcionProductor(inscripcion);
                e.setOrden(i++);
                registroEntidad.guarda(e);
            }
            inscripcion.setNumeroEmpresas(inscripcion.getEmpresas().size());
        }

        if (Objects.nonNull(inscripcion.getMolinos())) {
            i = 0;
            for (InscripcionMolino m : inscripcion.getMolinos()) {
                m.setInscripcionProductor(inscripcion);
                m.setOrden(i++);
                registroEntidad.guarda(m);
            }
        }

        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.CREACION,
                inscripcion.getUsuarioRegistra(), false, getEtiquetaGrupo(inscripcion));
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.DOCUMENTACION,
                inscripcion.getUsuarioRegistra(), true, getEtiquetaGrupo(inscripcion));
    }

    @Transactional
    @Override
    public void generaAsignacion(InscripcionProductor inscripcion, Usuario usuario) throws IOException {
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.DOCUMENTACION);

        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.ASIGNACION,
                usuario, true, true, getEtiquetaGrupo(inscripcion));

        //cambiamos el estatus previo
        if (omiteAsignacion(inscripcion)) {
            LOGGER.info("Se omite la asignación del folio: {} y se va directo a validación", inscripcion.getFolio());
            asigna(inscripcion, usuario, inscripcion.getUsuarioValidador());
        } else {
            actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, EstatusInscripcionEnum.NUEVO);
        }
        registroEntidad.actualiza(inscripcion);
    }

    private boolean omiteAsignacion(InscripcionProductor inscripcion) {
        TipoHistoricoInscripcion tipo = buscadorCatalogo.buscaElemento(TipoHistoricoInscripcion.class,
                TipoHistoricoInscripcionEnum.EDICION);
        List<HistoricoRegistro> historicos = buscadorHistorico.busca(inscripcion, tipo, false);
        return Objects.nonNull(inscripcion.getUsuarioValidador()) && !historicos.isEmpty();
    }

    @Transactional
    @Override
    public void asigna(InscripcionProductor inscripcion, Usuario usuario, Usuario validador) throws IOException {
        inscripcion.setUsuarioValidador(validador);
        inscripcion.setUsuarioAsignado(validador);
        actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, EstatusInscripcionEnum.VALIDACION);

        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.ASIGNACION);
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.VALIDACION,
                usuario, true, true, getEtiquetaGrupo(inscripcion));
        inscripcion.setFechaValidacion(Calendar.getInstance());
        registroEntidad.actualiza(inscripcion);

        // Generamos datos capturados
        procesadorDatoCapturado.procesa(inscripcion);
        // Se generan los PredioDocumento
        procesadorPredioDocumento.procesa(inscripcion);
    }

    @Transactional
    @Override
    public void validaPositivo(InscripcionProductor inscripcion, Usuario usuario, List<DatoCapturado> capturados) {
        validaProductorCiclo(inscripcion, "El productor ya se había validado positivamente.");
        procesadorDatoCapturado.validaEstatus(inscripcion, capturados, EstatusInscripcionEnum.POSITIVA);
        determinaTipoProductor(inscripcion);
        // Cancelamos los históricos de validación y revalidación
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.REVALIDACION);
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.VALIDACION);
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.ESPERA);

        // Actualizamos la inscripción y los datos capturado
        actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, EstatusInscripcionEnum.CORRECTO);
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.FINALIZACION,
                usuario, true, true, getEtiquetaGrupo(inscripcion));
        inscripcion.setFechaFinalizacion(Calendar.getInstance());
        registroEntidad.actualiza(inscripcion);
    }

    private void validaProductorCiclo(InscripcionProductor i, String mensaje) {
        // Si es forzada, no se hace la validación
        if (i.isForzada()) {
            i.setForzada(false);
            return;
        }

        try {
            // Si el productor ya estaba registrado en el ciclo, lanzamos un error
            ProductorCiclo pc = buscadorProductor.busca(i);
            throw new SegalmexRuntimeException("Error de productor.", mensaje);
        } catch (EmptyResultDataAccessException ouch) {
        }
    }

    @Transactional
    @Override
    public void validaNegativo(InscripcionProductor inscripcion, Usuario usuario, List<DatoCapturado> capturados) {
        actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, EstatusInscripcionEnum.CORRECCION);
        procesadorDatoCapturado.solicitaCorreccion(inscripcion, capturados);

        // Cancelamos los históricos de validación y revalidación
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.REVALIDACION);
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.VALIDACION);
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.ESPERA);

        // Generamos el histórico de corrección
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.CORRECCION,
                usuario, true, true, getEtiquetaGrupo(inscripcion));

        // Lo reasignamos al usuario que registró
        inscripcion.setUsuarioAsignado(inscripcion.getUsuarioRegistra());
        registroEntidad.actualiza(inscripcion);
    }

    @Transactional
    @Override
    public void enviaSupervision(InscripcionProductor inscripcion, Usuario usuario, List<DatoCapturado> capturados) {
        switch (EstatusInscripcionEnum.getInstance(inscripcion.getEstatus().getClave())) {
            case VALIDACION:
            case SOLVENTADA:
                actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, EstatusInscripcionEnum.SUPERVISION);
                procesadorDatoCapturado.solicitaCorreccion(inscripcion, capturados);

                // Cancelamos los históricos de validación o revalidación
                cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.REVALIDACION);
                cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.VALIDACION);

                // Generamos el histórico de supervisión
                procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.SUPERVISION,
                        usuario, true, true, getEtiquetaGrupo(inscripcion));
                registroEntidad.actualiza(inscripcion);
                break;
            default:
                throw new IllegalArgumentException("El registro no se puede enviar a supervision ya que se "
                        + "encuentra en estatus: " + inscripcion.getEstatus().getNombre());
        }
    }

    @Transactional
    @Override
    public void cancela(InscripcionProductor inscripcion, Usuario usuario, EstatusInscripcion estatus) {
        switch (inscripcion.getEstatus().getClave()) {
            case "positiva":
                throw new SegalmexRuntimeException("Error", "El registro no se le "
                        + "puede asignar estatus " + estatus.getNombre() + " ya que se "
                        + "encuentra en estatus: " + inscripcion.getEstatus().getNombre());
        }

        String etiquetaGrupo = getEtiquetaGrupo(inscripcion);
        TipoHistoricoInscripcionEnum tipoHistorico = null;
        switch (EstatusInscripcionEnum.getInstance(estatus.getClave())) {
            case CANCELADA:
                tipoHistorico = TipoHistoricoInscripcionEnum.CANCELADO;
                break;
            case NEGATIVO:
                tipoHistorico = TipoHistoricoInscripcionEnum.NEGATIVO;
                break;
            case PENDIENTE_REVISION:
                tipoHistorico = TipoHistoricoInscripcionEnum.PENDIENTE;
                break;
            case NO_ELIGIBLE:
                tipoHistorico = TipoHistoricoInscripcionEnum.NO_ELEGIBLE;
                break;
            case NEGATIVO_DEUDOR:
                tipoHistorico = TipoHistoricoInscripcionEnum.NEGATIVO_DEUDOR;
                break;
        }
        procesadorHistorico.crea(inscripcion, tipoHistorico, usuario,
                false, true, etiquetaGrupo);

        actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, estatus);
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.FINALIZACION,
                usuario, false, true, etiquetaGrupo);
        inscripcion.setFechaFinalizacion(Calendar.getInstance());
        registroEntidad.actualiza(inscripcion);
    }

    @Transactional
    @Override
    public void revalida(InscripcionProductor inscripcion, Usuario usuario) {
        if (Objects.isNull(inscripcion.getUsuarioValidador())) {
            throw new IllegalArgumentException("El registro no se puede revalidar ya que "
                    + "no cuenta con un validador asignado.");
        }
        switch (EstatusInscripcionEnum.getInstance(inscripcion.getEstatus().getClave())) {
            case CANCELADA:
            case CORRECCION:
            case CORRECTO:
            case NO_ELIGIBLE:
            case SUPERVISION:
            case NEGATIVO:
                break;
            case POSITIVA:
                // Si hay históricos de finalización abiertos, los cerramos
                List<HistoricoRegistro> historicos = buscadorHistorico.busca(inscripcion,
                        buscadorCatalogo.buscaElemento(TipoHistoricoInscripcion.class,
                                TipoHistoricoInscripcionEnum.FINALIZACION.getClave()), true);
                if (!historicos.isEmpty()) {
                    cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.FINALIZACION);
                }

                // Se permite la revalidación de productores que no hayan facturado
                ProductorCiclo pc = buscadorProductor.busca(inscripcion);
                if (pc.getToneladasFacturadas().compareTo(BigDecimal.ZERO) == 0) {
                    //buscamos ProductorCicloGrupo para actualizar sus toneladas totales a 0.
                    for (ProductorCicloGrupo pg : pc.getGrupos()) {
                        pg.setToneladasTotales(BigDecimal.ZERO);
                        registroEntidad.actualiza(pg);
                    }
                    pc.setHectareasTotales(BigDecimal.ZERO);
                    pc.setToneladasTotales(BigDecimal.ZERO);
                    pc.setActivo(false);
                    registroEntidad.actualiza(pc);
                    break;
                } else {
                    throw new IllegalArgumentException("El registro no se puede revalidar ya "
                            + "que el productor tiene facturadas registradas.");
                }
            default:
                throw new IllegalArgumentException("El registro no se puede revalidar ya que se "
                        + "encuentra en estatus: " + inscripcion.getEstatus().getNombre());
        }
        // Se actualiza estatus solicitados de los predios
        revalidaRendimientoSolicitado(inscripcion);
        // Cancelamos históricos de corrección
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.CORRECCION);

        // Generamos el histórico de revalidación
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.REVALIDACION,
                usuario, true, true, getEtiquetaGrupo(inscripcion));

        // Lo reasignamos al usuario que validó
        inscripcion.setUsuarioAsignado(inscripcion.getUsuarioValidador());
        actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, EstatusInscripcionEnum.SOLVENTADA);
        inscripcion.setComentarioEstatus(null);
        inscripcion.setFechaFinalizacion(null);
        registroEntidad.actualiza(inscripcion);
    }

    public void revalidaRendimientoSolicitado(InscripcionProductor inscripcion) {
        String clave = "";
        for (PredioProductor pre : inscripcion.getPredios()) {
            if (pre.getSolicitado().equals(EstatusPredioProductorEnum.APROBADO.getClave())) {
                clave = inscripcion.getCultivo().getClave() + ":" + pre.getTipoCultivo().getClave()
                        + ":" + inscripcion.getCiclo().getClave() + ":rendimiento:" + pre.getEstado().getClave();
                Parametro parametro = buscadorParametro.buscaElemento(clave);
                BigDecimal valor = new BigDecimal(parametro.getValor());
                pre.setRendimiento(valor);
                pre.setVolumen(pre.getSuperficie().multiply(valor));
                pre.setSolicitado(EstatusPredioProductorEnum.SOLICITADO.getClave());
            } else if (pre.getSolicitado().equals(EstatusPredioProductorEnum.NO_APROBADO.getClave())) {
                pre.setSolicitado(EstatusPredioProductorEnum.SOLICITADO.getClave());
            }
        }
    }

    @Transactional
    @Override
    public void corrige(InscripcionProductor inscripcion, Usuario usuario, List<DatoCapturado> capturados) {
        verificaCierreCorreccion(inscripcion);
        // Validamos los valores modificados
        procesadorDatoCapturado.corrige(inscripcion, capturados);

        // Cancelamos históricos de corrección
        int correcciones = cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.CORRECCION);

        // Generamos el histórico de revalidación o espera
        TipoHistoricoInscripcionEnum tipo = correcciones > MAXIMO_CORRECCION
                ? TipoHistoricoInscripcionEnum.ESPERA : TipoHistoricoInscripcionEnum.REVALIDACION;
        procesadorHistorico.crea(inscripcion, tipo, usuario, true, true, getEtiquetaGrupo(inscripcion));

        // Lo reasignamos al usuario que validó
        inscripcion.setUsuarioAsignado(inscripcion.getUsuarioValidador());

        // Actualizamos las entidades
        actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, EstatusInscripcionEnum.SOLVENTADA);
        registroEntidad.actualiza(inscripcion);
        for (PredioProductor p : inscripcion.getPredios()) {
            registroEntidad.actualiza(p);
        }
    }

    @Transactional
    @Override
    public void actualiza(InscripcionProductor inscripcion, List<DatoCapturado> capturados) {
        verificaCierreCorreccion(inscripcion);
        // Validamos los valores modificados
        procesadorDatoCapturado.corrige(inscripcion, capturados);

        // Actualizamos las entidades
        registroEntidad.actualiza(inscripcion);
        for (PredioProductor p : inscripcion.getPredios()) {
            registroEntidad.actualiza(p);
        }
    }

    private int cancelaHistoricos(InscripcionProductor inscripcion, Usuario usuario, TipoHistoricoInscripcionEnum tipo) {
        List<HistoricoRegistro> historicos = buscadorHistorico.busca(inscripcion,
                buscadorCatalogo.buscaElemento(TipoHistoricoInscripcion.class,
                        tipo.getClave()), false);
        for (HistoricoRegistro h : historicos) {
            if (Objects.isNull(h.getFechaFinaliza())) {
                procesadorHistorico.finaliza(h, usuario);
            }
        }

        return historicos.size();
    }

    @Transactional
    @Override
    public void creaEdicion(InscripcionProductor inscripcion, Usuario usuario) {
        switch (EstatusInscripcionEnum.getInstance(inscripcion.getEstatus().getClave())) {
            case PENDIENTE:
            case NUEVO:
            case VALIDACION:
            case CORRECCION:
            case SOLVENTADA:
            case SUPERVISION:
                // Verificamos que los estatus sean los correctos
                break;
            default:
                throw new SegalmexRuntimeException("Error al editar el registro del productor.",
                        "El registro del productor se encuentra en estatus: "
                        + inscripcion.getEstatus().getNombre());
        }
        // Finalizamos todos los  históricos
        List<HistoricoRegistro> historicos = buscadorHistorico.busca(inscripcion);
        for (HistoricoRegistro h : historicos) {
            if (Objects.isNull(h.getFechaFinaliza())) {
                procesadorHistorico.finaliza(h, usuario);
            }
        }
        //Desactivamos todos los predioDocumento del registro.
        procesadorPredioDocumento.desactiva(inscripcion);

        // Creamos el histórico de edición
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.EDICION,
                usuario, true, true, getEtiquetaGrupo(inscripcion));

        // Eliminamos datos capturados
        procesadorDatoCapturado.elimina(inscripcion);

        actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, EstatusInscripcionEnum.PENDIENTE);
        inscripcion.setUsuarioAsignado(inscripcion.getUsuarioRegistra());
        registroEntidad.actualiza(inscripcion);
    }

    @Transactional
    @Override
    public void finalizaEdicion(InscripcionProductor inscripcion, InscripcionProductor nueva, Usuario usuario) {
        // Modificamos las listas
        ipContratoHelper.modifica(inscripcion, nueva);
        ipPredioHelper.modifica(inscripcion, nueva);
        ipEmpresaHelper.modifica(inscripcion, nueva);

        for (ValidadorInscripcion v : validadadores) {
            v.valida(inscripcion);
        }

        // Actualizamos la listas
        for (ContratoFirmado c : inscripcion.getContratos()) {
            registroEntidad.guardaActualiza(c);
        }

        for (PredioProductor p : inscripcion.getPredios()) {
            registroEntidad.guardaActualiza(p);
        }
        for (EmpresaAsociada e : inscripcion.getEmpresas()) {
            registroEntidad.guardaActualiza(e);
        }

        inscripcion.setEstado(EstadoProductorHelper.getEstado(inscripcion));
        inscripcion.setTipoRegistro(getTipoRegistro(inscripcion));
        inscripcion.setTiposCultivo(getTiposCultivo(inscripcion.getPrediosActivos()));
        inscripcion.setEtiquetaTipoCultivo(getEtiquetaTipoCultivo(inscripcion.getTiposCultivo()));
        determinaTipoProductor(inscripcion);
        registroEntidad.actualiza(inscripcion);
        // Cancelamos histórico de edición y generamos el nuevo histórico
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.EDICION);
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.DOCUMENTACION,
                usuario, true, true, getEtiquetaGrupo(inscripcion));
    }

    @Transactional
    @Override
    public void eliminaPredio(InscripcionProductor i, int index, Usuario usuario) {
        PredioProductor pp = i.getPredios().get(index);
        if (Objects.nonNull(pp.getFechaCancelacion())) {
            return;
        }
        pp.setFechaCancelacion(Calendar.getInstance());
        registroEntidad.actualiza(pp);

        List<DatoCapturado> datos = buscadorDatoCapturado.busca(i.getClass(), i.getUuid(), true);
        for (DatoCapturado d : datos) {
            if (PREDIO.equals(d.getGrupo()) && d.getClave().startsWith(String.valueOf((index + 1)))) {
                LOGGER.info("Eliminando dato capturado con clave: {}", d.getClave());
                d.setActual(false);
                registroEntidad.actualiza(d);
            }
        }
        if (i.getCultivo().getClave().equals(CultivoEnum.MAIZ_COMERCIAL.getClave())) {
            ParametrosPredioDocumento params = new ParametrosPredioDocumento();
            params.setInscripcionProductor(i);
            params.setNumero(String.valueOf(index + 1));
            procesadorPredioDocumento.desactiva(buscadorPredioDocumento.busca(params));
        }

        i.setNumeroPredios(i.getPrediosActivos().size());

        procesadorHistorico.crea(i, TipoHistoricoInscripcionEnum.ELIMINACION_PREDIO,
                usuario, false, true, getEtiquetaGrupo(i));
        determinaTipoProductor(i);
        registroEntidad.actualiza(i);
    }

    @Transactional
    @Override
    public void eliminaContrato(InscripcionProductor i, int index, Usuario usuario) {
        ContratoFirmado cf = i.getContratosActivos().get(index);
        if (Objects.nonNull(cf.getFechaCancelacion())) {
            return;
        }
        cf.setFechaCancelacion(Calendar.getInstance());
        registroEntidad.actualiza(cf);
        //Ahora eliminamos los datos capturados del contrato:
        List<DatoCapturado> datos = buscadorDatoCapturado.busca(i.getClass(), i.getUuid(), true);
        for (DatoCapturado d : datos) {
            if (CONTRATO.equals(d.getGrupo()) && d.getClave().startsWith(String.valueOf((index)))) {
                LOGGER.info("Eliminando dato capturado con clave: {}", d.getClave());
                d.setActual(false);
                registroEntidad.actualiza(d);
            }
        }

        i.setNumeroContratos(i.getContratosActivos().size());
        procesadorHistorico.crea(i, TipoHistoricoInscripcionEnum.ELIMINACION_CONTRATO,
                usuario, false, true, getEtiquetaGrupo(i));
        registroEntidad.actualiza(i);
    }

    @Override
    @Transactional
    public void actualizaTotalSiembra(InscripcionProductor i, int index, int total, Usuario usuario) {
        actualizaTotalXml(i, index, total, DatoCapturadoProductorEnum.PERMISO_SIEMBRA_XML, usuario);
    }

    @Override
    @Transactional
    public void actualizaTotalRiego(InscripcionProductor i, int index, int total, Usuario usuario) {
        actualizaTotalXml(i, index, total, DatoCapturadoProductorEnum.PAGO_RIEGO_XML, usuario);
    }

    @Override
    @Transactional
    public void cambiaForzada(InscripcionProductor i, boolean isForzada) {
        if (i.isForzada() != isForzada) {
            i.setForzada(isForzada);
            registroEntidad.actualiza(i);
        }
    }

    @Override
    @Transactional
    public void cambiaPuntoMedio(InscripcionProductor i, int index, Usuario usuario) {
        PredioProductor pp = i.getPredios().get(index);
        if (Objects.nonNull(pp.getLatitud())) {
            return;
        }
        //Cambiamos los valores de latitud.
        pp.setLatitud(pp.getLatitud1());
        pp.setLatitud1(null);
        pp.setLatitud2(null);
        pp.setLatitud3(null);
        pp.setLatitud4(null);
        //cambiamos los valores de longitud.
        pp.setLongitud(pp.getLongitud1());
        pp.setLongitud1(null);
        pp.setLongitud2(null);
        pp.setLongitud3(null);
        pp.setLongitud4(null);
        registroEntidad.actualiza(pp);
        procesadorHistorico.crea(i, TipoHistoricoInscripcionEnum.CAMBIO_GEORREFERECNIA,
                usuario, false, true, getEtiquetaGrupo(i));
        actualizaDatosCapturadosPredio(pp, POLIGONO_A_MEDIO);
    }

    @Override
    @Transactional
    public void cambiaPoligono(InscripcionProductor i, int index, Usuario usuario) {
        PredioProductor pp = i.getPredios().get(index);
        if (Objects.nonNull(pp.getLatitud1())) {
            return;
        }
        //Cambiamos los valores de latitud.
        pp.setLatitud1(pp.getLatitud());
        pp.setLatitud2(pp.getLatitud());
        pp.setLatitud3(pp.getLatitud());
        pp.setLatitud4(pp.getLatitud());
        pp.setLatitud(null);
        //cambiamos los valores de longitud.
        pp.setLongitud1(pp.getLongitud());
        pp.setLongitud2(pp.getLongitud());
        pp.setLongitud3(pp.getLongitud());
        pp.setLongitud4(pp.getLongitud());
        pp.setLongitud(null);
        registroEntidad.actualiza(pp);
        procesadorHistorico.crea(i, TipoHistoricoInscripcionEnum.CAMBIO_GEORREFERECNIA,
                usuario, false, true, getEtiquetaGrupo(i));
        actualizaDatosCapturadosPredio(pp, MEDIO_A_POLIGONO);
    }

    @Override
    @Transactional
    public void permitePositivoDeudor(InscripcionProductor ip, Usuario usuario) {
        procesadorHistorico.crea(ip, TipoHistoricoInscripcionEnum.AUTORIZA_POSITIVO_DEUDOR,
                usuario, false, false, getEtiquetaGrupo(ip));
    }

    /**
     * Se encarga de generar un string con los tipos cultivos seleccionados.
     *
     * @param tiposCultivo
     * @return
     */
    private String getEtiquetaTipoCultivo(List<TipoCultivo> tiposCultivo) {
        List<String> tipos = new ArrayList<>();
        Collections.sort(tiposCultivo, (TipoCultivo p1, TipoCultivo p2) -> new Integer(p1.getOrden()).compareTo(p2.getOrden()));
        for (TipoCultivo tc : tiposCultivo) {
            tipos.add(tc.getNombre());
        }
        return String.join(", ", tipos);
    }

    /**
     * Se encarga de recolectar todos los tipos de cultivos seleccionados en los
     * predios.
     *
     * @param predios
     * @return
     */
    private List<TipoCultivo> getTiposCultivo(List<PredioProductor> predios) {
        Map<String, TipoCultivo> tipos = new HashMap<>();

        predios.forEach((pp) -> {
            tipos.put(pp.getTipoCultivo().getClave(), pp.getTipoCultivo());
        });
        return new ArrayList<>(tipos.values());
    }

    /**
     * Se encarga de armar la etiquetaGrupo que debe llevar su Histórico.
     *
     * @param inscripcion
     * @return
     */
    private String getEtiquetaGrupo(InscripcionProductor inscripcion) {
        return inscripcion.getCultivo().getClave() + ":" + inscripcion.getCiclo().getClave();
    }

    private String getTipoRegistro(InscripcionProductor inscripcion) {
        if (inscripcion.getEmpresas().size() > 0) {
            return TipoRegistroEnum.ASOCIADO.getClave();
        } else {
            return inscripcion.getDatosProductor().getTipoPersona().getClave();
        }
    }

    /**
     * Se encarga de verificar si se permite el registro de productores dado el
     * cultivo, ciclo y fecha de registro.
     *
     * @param inscripcion
     */
    private void verificaCierre(InscripcionProductor inscripcion) {
        Parametro parametro = buscadorParametro.buscaElementoOpcional(inscripcion.getCultivo()
                .getClave() + ":" + inscripcion.getCiclo().getClave() + ":productores:cierre");
        if (Objects.nonNull(parametro)) {
            Calendar cierre = SegalmexDateUtils
                    .parseCalendar(parametro.getValor(), "yyyy-MM-dd'T'HH:mm:ss");
            if (Calendar.getInstance().after(cierre)) {
                throw new SegalmexRuntimeException("Error al registrar al productor.",
                        "El registro de productores cerró el " + parametro.getValor());
            }
        }
    }

    /**
     * Se encarga de obtener el máximo rendimiento para un predio dependiendo
     * del cultivo, ciclo y estado del predio.
     *
     * @param predio el predio al que se le va a obtener su rendimiento máximo.
     * @return el rendimiento máximo para un predio.
     */
    private BigDecimal getRendimientoMaximo(PredioProductor predio) {
        InscripcionProductor i = predio.getInscripcionProductor();
        String clave = i.getCultivo().getClave()
                + ":" + predio.getTipoCultivo().getClave()
                + ":" + i.getCiclo().getClave()
                + ":rendimiento:" + predio.getEstado().getClave();
        Parametro parametro = buscadorParametro.buscaElementoOpcional(clave);
        return Objects.nonNull(parametro) ? new BigDecimal(parametro.getValor())
                : BigDecimal.ZERO;

    }

    private void actualizaTotalXml(InscripcionProductor i, int index, int total,
            DatoCapturadoProductorEnum tipo, Usuario usuario) {
        LOGGER.info("Actualizando cantidad xml del tipo {}", tipo.getNombre());
        PredioProductor pp = i.getPredios().get(index);
        switch (tipo) {
            case PERMISO_SIEMBRA_XML:
                if (total > pp.getCantidadSiembra()) {
                    generaDatosCapturados(pp, DatoCapturadoProductorEnum.PERMISO_SIEMBRA_XML, total);
                    pp.setCantidadSiembra(total);
                    procesadorHistorico.crea(i, TipoHistoricoInscripcionEnum.ACTUALIZACION_SIEMBRA,
                            usuario, false, true, getEtiquetaGrupo(i));
                }
                break;
            case PAGO_RIEGO_XML:
                if (total > pp.getCantidadRiego()) {
                    generaDatosCapturados(pp, DatoCapturadoProductorEnum.PAGO_RIEGO_XML, total);
                    pp.setCantidadRiego(total);
                    procesadorHistorico.crea(i, TipoHistoricoInscripcionEnum.ACTUALIZACION_RIEGO,
                            usuario, false, true, getEtiquetaGrupo(i));
                }
                break;
        }
        registroEntidad.actualiza(pp);
    }

    private void generaDatosCapturados(PredioProductor pp, DatoCapturadoProductorEnum tipo, int total) {
        DatoCapturado anexo;
        InscripcionProductor inscripcion = pp.getInscripcionProductor();
        List<DatoCapturado> datos = new ArrayList<>();
        int orden = pp.getOrden();
        String suffix = inscripcion.getPredios().size() > 1 ? " (" + (pp.getOrden()) + ")" : "";
        switch (tipo) {
            case PERMISO_SIEMBRA_XML:
                LOGGER.info("Generando datos siembra.");
                for (int i = pp.getCantidadSiembra(); i < total; i++) {
                    anexo = getInstance(inscripcion, DatoCapturadoProductorEnum.PERMISO_SIEMBRA_XML,
                            orden + "_" + (i + 1) + "_" + DatoCapturadoProductorEnum.PERMISO_SIEMBRA_XML.getClave() + ".xml", orden, suffix, PREDIO);
                    anexo.setTipo("archivo");
                    anexo.setClave(orden + "-" + (i + 1) + "_" + DatoCapturadoProductorEnum.PERMISO_SIEMBRA_XML.getClave());
                    anexo.setOrden(getOrden(pp, DatoCapturadoProductorEnum.PERMISO_SIEMBRA_XML, i));
                    datos.add(anexo);
                }
                break;
            case PAGO_RIEGO_XML:
                LOGGER.info("Generando datos riego.");
                for (int i = pp.getCantidadRiego(); i < total; i++) {
                    anexo = getInstance(inscripcion, DatoCapturadoProductorEnum.PAGO_RIEGO_XML,
                            orden + "_" + (i + 1) + "_" + DatoCapturadoProductorEnum.PAGO_RIEGO_XML.getClave() + ".xml", orden, suffix, PREDIO);
                    anexo.setTipo("archivo");
                    anexo.setClave(orden + "-" + (i + 1) + "_" + DatoCapturadoProductorEnum.PAGO_RIEGO_XML.getClave());
                    anexo.setOrden(getOrden(pp, DatoCapturadoProductorEnum.PAGO_RIEGO_XML, i));
                    datos.add(anexo);
                }
                break;
        }
        for (DatoCapturado dc : datos) {
            registroEntidad.guarda(dc);
        }
    }

    private int getOrden(PredioProductor pp, DatoCapturadoProductorEnum tipo, int contador) {
        InscripcionProductor ip = pp.getInscripcionProductor();
        String expresion = pp.getOrden() + "-%" + tipo.getClave();
        List<DatoCapturado> datos = buscadorDatoCapturado.buscaAproximado(ip.getClass(),
                ip.getUuid(), expresion, true);
        return datos.get(0).getOrden() + contador;
    }

    private int getOrdenDato(PredioProductor pp, DatoCapturadoProductorEnum tipo, int contador) {
        InscripcionProductor ip = pp.getInscripcionProductor();
        String expresion = pp.getOrden() + "_" + tipo.getClave();
        List<DatoCapturado> datos = buscadorDatoCapturado.buscaAproximado(ip.getClass(),
                ip.getUuid(), expresion, true);
        return datos.get(0).getOrden() + contador;
    }

    private void actualizaDatosCapturadosPredio(PredioProductor pp, String tipo) {
        //Desactivamos los datos capturados.
        InscripcionProductor ip = pp.getInscripcionProductor();
        List<DatoCapturado> datos = new ArrayList<>();
        datos.addAll(buscadorDatoCapturado.buscaAproximado(InscripcionProductor.class, ip.getUuid(),
                pp.getOrden() + "_latitud%", true));
        datos.addAll(buscadorDatoCapturado.buscaAproximado(InscripcionProductor.class, ip.getUuid(),
                pp.getOrden() + "_longitud%", true));

        for (DatoCapturado d : datos) {
            d.setActual(false);
            registroEntidad.actualiza(d);

        }
        //Generamos los nuevos datos capturados.
        DatoCapturado dc;
        List<DatoCapturado> datosNuevos = new ArrayList<>();
        List<DatoCapturadoProductorEnum> datosEnum;
        DatoCapturadoProductorEnum[] ENUMS_PUNTO_MEDIO = {DatoCapturadoProductorEnum.LATITUD_PREDIO, DatoCapturadoProductorEnum.LONGITUD_PREDIO};

        DatoCapturadoProductorEnum[] ENUMS_POLIGONO = {
            DatoCapturadoProductorEnum.LATITUD1_PREDIO, DatoCapturadoProductorEnum.LONGITUD1_PREDIO,
            DatoCapturadoProductorEnum.LATITUD2_PREDIO, DatoCapturadoProductorEnum.LONGITUD2_PREDIO,
            DatoCapturadoProductorEnum.LATITUD3_PREDIO, DatoCapturadoProductorEnum.LONGITUD3_PREDIO,
            DatoCapturadoProductorEnum.LATITUD4_PREDIO, DatoCapturadoProductorEnum.LONGITUD4_PREDIO
        };

        if (tipo.equals(MEDIO_A_POLIGONO)) {
            datosEnum = Arrays.asList(ENUMS_POLIGONO);
        } else {
            datosEnum = Arrays.asList(ENUMS_PUNTO_MEDIO);
        }
        int orden = pp.getOrden();
        String suffix = ip.getPredios().size() > 1 ? " (" + (pp.getOrden()) + ")" : "";
        for (int i = 0; i < datosEnum.size(); i++) {
            DatoCapturadoProductorEnum dce = datosEnum.get(i);
            dc = getInstance(ip, dce, getValor(dce, pp), orden, suffix, PREDIO);
            dc.setTipo("texto");
            dc.setClave(orden + "_" + dce.getClave());
            dc.setOrden(getOrdenDato(pp, DatoCapturadoProductorEnum.LOCALIDAD_PREDIO, i + 1));
            datosNuevos.add(dc);
        }

        for (DatoCapturado d : datosNuevos) {
            registroEntidad.guarda(d);
        }
    }

    private String getValor(DatoCapturadoProductorEnum dce, PredioProductor pp) {
        switch (dce) {
            case LATITUD1_PREDIO:
            case LATITUD2_PREDIO:
            case LATITUD3_PREDIO:
            case LATITUD4_PREDIO:
                return String.valueOf(pp.getLatitud1());
            case LONGITUD1_PREDIO:
            case LONGITUD2_PREDIO:
            case LONGITUD3_PREDIO:
            case LONGITUD4_PREDIO:
                return String.valueOf(pp.getLongitud1());
            case LATITUD_PREDIO:
                return String.valueOf(pp.getLatitud());
            case LONGITUD_PREDIO:
                return String.valueOf(pp.getLongitud());
            default:
                throw new SegalmexRuntimeException("Error", "No existe: " + dce.getNombre());

        }

    }

    private void verificaCierreCorreccion(InscripcionProductor inscripcion) {
        Parametro parametro = buscadorParametro.buscaElementoOpcional(inscripcion.getCultivo()
                .getClave() + ":" + inscripcion.getCiclo().getClave() + ":cierre-correccion-productores");
        if (Objects.nonNull(parametro)) {
            Calendar cierre = SegalmexDateUtils
                    .parseCalendar(parametro.getValor(), "yyyy-MM-dd'T'HH:mm:ss");
            if (Calendar.getInstance().after(cierre)) {
                throw new SegalmexRuntimeException("Error al corregir al productor.",
                        "La corrección de productores cerró el " + parametro.getValor());
            }
        }
    }

    public void setValidadadores(List<ValidadorInscripcion> validadadores) {
        this.validadadores = validadadores;
    }

    private Boolean isBeneficiarioDuplicado(InscripcionProductor inscripcion) {
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setCultivo(inscripcion.getCultivo());
        parametros.setCiclo(inscripcion.getCiclo());
        parametros.setCurpBeneficiario(inscripcion.getCurpBeneficiario());
        parametros.setNoEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.CANCELADA.getClave()));
        parametros.setNoFolio(inscripcion.getFolio());
        return !buscadorInscripcionProductor.busca(parametros).isEmpty();
    }

    private void determinaTipoProductor(InscripcionProductor ip) {
        BigDecimal hectareasTotales = BigDecimal.ZERO;
        BigDecimal toneladasTotales = BigDecimal.ZERO;
        for (PredioProductor p : ip.getPrediosActivos()) {
            hectareasTotales = hectareasTotales.add(p.getSuperficie());
            toneladasTotales = toneladasTotales.add(p.getVolumen());
        }
        tipoProductorHelper.determinaTipoProductor(ip, hectareasTotales, toneladasTotales);
    }
}
