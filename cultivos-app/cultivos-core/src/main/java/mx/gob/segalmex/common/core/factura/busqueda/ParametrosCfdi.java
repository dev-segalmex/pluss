/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.busqueda;

import java.util.Objects;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;

/**
 *
 * @author ismael
 */
@Getter
@Setter
public class ParametrosCfdi {

    private String uuid;

    private String estatus;

    private String rfcEmisor;

    private String rfcReceptor;

    private String uuidFolioFiscalDigital;

    private String tipo;

    private Cultivo cultivo;

    private CicloAgricola ciclo;

    /**
     * La sucursal en la que se generó la inscripción de una factura.
     */
    private Sucursal sucursal;

    public boolean isJoinInscripcion() {
        return Objects.nonNull(sucursal) || Objects.nonNull(cultivo) || Objects.nonNull(ciclo);
    }

}
