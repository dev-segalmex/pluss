/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor;

import mx.gob.segalmex.granos.core.inscripcion.DatoCapturadoFactory;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import mx.gob.segalmex.granos.core.inscripcion.InscripcionDatoCapturado;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.productor.ContratoFirmado;
import mx.gob.segalmex.pluss.modelo.productor.CuentaBancaria;
import mx.gob.segalmex.pluss.modelo.productor.DatosProductor;
import mx.gob.segalmex.pluss.modelo.productor.EmpresaAsociada;
import mx.gob.segalmex.pluss.modelo.productor.InformacionSemilla;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;

/**
 * Clase que genera instancias de <code>DatoCapturado</code> a partir del modelo de datos de
 * <code>InscripcionProductor</code>.
 *
 * @author ismael
 */
public class ProductorDatoCapturadoFactory extends DatoCapturadoFactory {

    private static final String PRODUCTOR = "Productor";

    private static final String CUENTA_BANCARIA = "Cuenta bancaria";

    private static final String CONTRATOS = "Contratos";

    private static final String ANEXOS = "Anexos";

    private static final String FACTURAS_PERSONA_MORAL = "Facturas persona moral";

    private static final String SEMILLA_CERTIFICADA = "Semilla cerificada SNICS";

    private static final String BENEFICIARIO = "Beneficiario";

    private Map<String, GeneradorDatoCapturadoPredio> generadorPredio;

    private ProductorDatoCapturadoFactory() {
    }

    /**
     * Obtiene la lista de datos capturados de una inscripción de productor.
     *
     * @param inscripcion la inscripción de la que se obtienen los datos capturados.
     * @return la lista de datos capturados.
     */
    public List<DatoCapturado> getInstance(InscripcionProductor inscripcion) {
        InscripcionDatoCapturado idc = new InscripcionDatoCapturado();

        switch (inscripcion.getDatosProductor().getTipoPersona().getClave()) {
            case "moral":
                agregaDatosPersonaMoral(inscripcion, idc, inscripcion.getDatosProductor());
                break;
            case "fisica":
                agregaDatosPersonaFisica(inscripcion, idc, inscripcion.getDatosProductor());
                break;
        }

        int i = 0;
        for (ContratoFirmado cf : inscripcion.getContratosActivos()) {
            if (Objects.isNull(cf.getOrden())) {
                cf.setOrden(i);
            }
            agregaContratoFirmado(inscripcion, idc, cf);
        }

        for (PredioProductor p : inscripcion.getPrediosActivos()) {
            agregaPredioProductor(inscripcion, idc, p);
        }

        for (EmpresaAsociada ea : inscripcion.getEmpresasActivas()) {
            int orden = ea.getOrden();
            String suffix = inscripcion.getEmpresas().size() > 1
                    ? " (" + (orden + 1) + ")" : "";

            idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.RFC_EMPRESA_ASOCIADA,
                    ea.getRfc(), orden, suffix, FACTURAS_PERSONA_MORAL));

            DatoCapturado anexo = getInstance(inscripcion,
                    DatoCapturadoProductorEnum.ACTA_CONSTITUTIVA_SOCIEDAD_PDF,
                    orden + "_" + DatoCapturadoProductorEnum.ACTA_CONSTITUTIVA_SOCIEDAD_PDF
                            .getClave() + ".pdf", orden, suffix, FACTURAS_PERSONA_MORAL);
            anexo.setTipo("archivo");
            idc.agrega(anexo);

            anexo = getInstance(inscripcion, DatoCapturadoProductorEnum.LISTA_SOCIOS_PDF,
                    orden + "_" + DatoCapturadoProductorEnum.LISTA_SOCIOS_PDF.getClave() + ".pdf",
                    orden, suffix, FACTURAS_PERSONA_MORAL);
            anexo.setTipo("archivo");
            idc.agrega(anexo);
        }

        CuentaBancaria cuenta = inscripcion.getCuentaBancaria();
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.BANCO_PRODUCTOR,
                cuenta.getBanco(), CUENTA_BANCARIA));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.CLABE_PRODUCTOR,
                cuenta.getClabe(), CUENTA_BANCARIA));

        if (Objects.nonNull(inscripcion.getInformacionSemilla())) {
            InformacionSemilla semilla = inscripcion.getInformacionSemilla();
            idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.SNICS,
                    semilla.getSnics(), SEMILLA_CERTIFICADA));
            idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.TONELADAS_SNICS,
                    String.valueOf(semilla.getToneladas()), SEMILLA_CERTIFICADA));
            idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.FOLIOS_SNICS,
                    semilla.getCantidadFolios(), SEMILLA_CERTIFICADA));
            idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.RAZON_SOCIAL_SNICS,
                    semilla.getNombre(), SEMILLA_CERTIFICADA));
            idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.RFC_SNICS,
                    semilla.getRfc(), SEMILLA_CERTIFICADA));
            DatoCapturado anexoSnics = getInstance(inscripcion, DatoCapturadoProductorEnum.CONSTANCIA_SNICS,
                    DatoCapturadoProductorEnum.CONSTANCIA_SNICS.getClave() + ".pdf", SEMILLA_CERTIFICADA);
            anexoSnics.setTipo("archivo");
            idc.agrega(anexoSnics);
        }

        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.NOMBRE_BENEFICIARIO,
                inscripcion.getNombreBeneficiario(), BENEFICIARIO));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.APELLIDOS_BENEFICIARIO,
                inscripcion.getApellidosBeneficiario(), BENEFICIARIO));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.CURP_BENEFICIARIO,
                inscripcion.getCurpBeneficiario(), BENEFICIARIO));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.PARENTESCO_BENEFICIARIO,
                inscripcion.getParentesco(), BENEFICIARIO));

        DatoCapturado anexo = null;
        switch (inscripcion.getDatosProductor().getTipoPersona().getClave()) {
            case "moral":
                anexo = getInstance(inscripcion, DatoCapturadoProductorEnum.ACTA_CONSTITUTIVA_PDF,
                        DatoCapturadoProductorEnum.ACTA_CONSTITUTIVA_PDF.getClave() + ".pdf", ANEXOS);
                anexo.setTipo("archivo");
                idc.agrega(anexo);

                anexo = getInstance(inscripcion, DatoCapturadoProductorEnum.CURP_REPRESENTANTE_PDF,
                        DatoCapturadoProductorEnum.CURP_REPRESENTANTE_PDF.getClave() + ".pdf", ANEXOS);
                anexo.setTipo("archivo");
                idc.agrega(anexo);

                anexo = getInstance(inscripcion, DatoCapturadoProductorEnum.DOCUMENTO_IDENTIFICACION_REPRESENTANTE_PDF,
                        DatoCapturadoProductorEnum.DOCUMENTO_IDENTIFICACION_REPRESENTANTE_PDF.getClave() + ".pdf", ANEXOS);
                anexo.setTipo("archivo");
                idc.agrega(anexo);
                break;
            case "fisica":
                anexo = getInstance(inscripcion, DatoCapturadoProductorEnum.CURP_PRODUCTOR_PDF,
                        DatoCapturadoProductorEnum.CURP_PRODUCTOR_PDF.getClave() + ".pdf", ANEXOS);
                anexo.setTipo("archivo");
                idc.agrega(anexo);

                anexo = getInstance(inscripcion, DatoCapturadoProductorEnum.RFC_PRODUCTOR_PDF,
                        DatoCapturadoProductorEnum.RFC_PRODUCTOR_PDF.getClave() + ".pdf", ANEXOS);
                anexo.setTipo("archivo");
                idc.agrega(anexo);

                anexo = getInstance(inscripcion, DatoCapturadoProductorEnum.DOCUMENTO_IDENTIFICACION_PRODUCTOR_PDF,
                        DatoCapturadoProductorEnum.DOCUMENTO_IDENTIFICACION_PRODUCTOR_PDF.getClave() + ".pdf", ANEXOS);
                anexo.setTipo("archivo");
                idc.agrega(anexo);
                break;
        }

        anexo = getInstance(inscripcion, DatoCapturadoProductorEnum.DATOS_BANCARIOS_PDF,
                DatoCapturadoProductorEnum.DATOS_BANCARIOS_PDF.getClave() + ".pdf", ANEXOS);
        anexo.setTipo("archivo");
        idc.agrega(anexo);

        anexo = getInstance(inscripcion, DatoCapturadoProductorEnum.INSCRIPCION_PRODUCTOR_PDF,
                DatoCapturadoProductorEnum.INSCRIPCION_PRODUCTOR_PDF.getClave() + ".pdf", ANEXOS);
        anexo.setTipo("archivo");
        idc.agrega(anexo);

        if (!inscripcion.getEmpresasActivas().isEmpty()
                && !inscripcion.getCultivo().getClave().equals(CultivoEnum.TRIGO.getClave())) {
            anexo = getInstance(inscripcion, DatoCapturadoProductorEnum.CARTA_ACUERDO_PDF,
                    DatoCapturadoProductorEnum.CARTA_ACUERDO_PDF.getClave() + ".pdf", ANEXOS);
            anexo.setTipo("archivo");
            idc.agrega(anexo);
        }

        return idc.getDatos();
    }

    private void agregaDatosPersonaMoral(InscripcionProductor inscripcion, InscripcionDatoCapturado idc, DatosProductor dp) {
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.NOMBRE_SOCIEDAD, dp.getNombreMoral(), PRODUCTOR));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.RFC_SOCIEDAD, dp.getRfc(), PRODUCTOR));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.CURP_REPRESENTANTE, dp.getCurp(), PRODUCTOR));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.NOMBRE_REPRESENTANTE, dp.getNombre(), PRODUCTOR));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.PAPELLIDO_REPRESENANTE, dp.getPrimerApellido(), PRODUCTOR));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.SAPELLIDO_REPRESENTANTE, dp.getSegundoApellido(), PRODUCTOR));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.TIPO_IDENTIFICACION_REPRESENTANTE, dp.getTipoDocumento(), PRODUCTOR));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.NUMERO_IDENTIFICACION_REPRESENTANTE, dp.getNumeroDocumento(), PRODUCTOR));
    }

    private void agregaDatosPersonaFisica(InscripcionProductor inscripcion, InscripcionDatoCapturado idc, DatosProductor dp) {
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.RFC_PRODUCTOR, dp.getRfc(), PRODUCTOR));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.CURP_PRODUCTOR, dp.getCurp(), PRODUCTOR));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.NOMBRE_PRODUCTOR, dp.getNombre(), PRODUCTOR));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.PAPELLIDO_PRODUCTOR, dp.getPrimerApellido(), PRODUCTOR));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.SAPELLIDO_PRODUCTOR, dp.getSegundoApellido(), PRODUCTOR));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.TIPO_IDENTIFICACION_PRODUCTOR, dp.getTipoDocumento(), PRODUCTOR));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.NUMERO_IDENTIFICACION_PRODUCTOR, dp.getNumeroDocumento(), PRODUCTOR));
    }

    private void agregaPredioProductor(InscripcionProductor inscripcion, InscripcionDatoCapturado idc, PredioProductor p) {
        GeneradorDatoCapturadoPredio generador = generadorPredio.get(inscripcion.getCultivo().getClave());
        generador.generaDatosCapturadosPredio(inscripcion, idc, p);
    }

    private void agregaContratoFirmado(InscripcionProductor inscripcion, InscripcionDatoCapturado idc, ContratoFirmado cf) {
        String suffix = inscripcion.getContratos().size() > 1
                ? " (" + cf.getNumeroContrato() + ")" : "";
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.NO_CONTRATO,
                String.valueOf(cf.getNumeroContrato()), cf.getOrden(), suffix, CONTRATOS));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.CANTIDAD_CONTRATADA,
                String.valueOf(cf.getCantidadContratada()), cf.getOrden(), suffix, CONTRATOS));
        idc.agrega(getInstance(inscripcion, DatoCapturadoProductorEnum.CANTIDAD_CONTRATADA_COBERTURA,
                String.valueOf(cf.getCantidadContratadaCobertura()), cf.getOrden(), suffix, CONTRATOS));
    }

    public void setGeneradorPredio(Map<String, GeneradorDatoCapturadoPredio> generadorPredio) {
        this.generadorPredio = generadorPredio;
    }
}
