/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.xls;

import java.util.List;
import java.util.Objects;
import mx.gob.segalmex.common.core.util.XlsUtils;
import mx.gob.segalmex.pluss.modelo.productor.Domicilio;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.stereotype.Component;

/**
 *
 * @author erikcam
 */
@Component("prediosProductorXls")
public class PrediosProductorXls {

    public byte[] exporta(List<InscripcionProductor> inscripciones) {
        SXSSFWorkbook wb = new SXSSFWorkbook(100);
        CreationHelper ch = wb.getCreationHelper();
        Sheet s = wb.createSheet();

        String[] encabezados = new String[]{
            "No.",
            "Folio",
            "Nombre productor",
            "Primer apellido productor",
            "Segundo apellido productor",
            "RFC",
            "CURP",
            "Teléfono",
            "Correo electrónico",
            "Estado",
            "Municipio",
            "Localidad",
            "Estatus inscripción",
            "Folio predio",
            "Tipo cultivo",
            "Tipo de posesión",
            "Documento que acredita",
            "Régimen hídrico",
            "Volumen obtenido (V)",
            "Superficie sembrada (S)",
            "Rendimiento (R = V / S)",
            "Estado del predio",
            "Municipio del predio",
            "Localidad del predio",
            "Latitud",
            "Longitud",
            "Latitud 1",
            "Latitud 2",
            "Latitud 3",
            "Latitud 4",
            "Longitud 1",
            "Longitud 2",
            "Longitud 3",
            "Longitud 4"
        };
        XlsUtils.creaEncabezados(s, encabezados);

        CellStyle cs = wb.createCellStyle();
        cs.setDataFormat(ch.createDataFormat().getFormat("dd/MM/yyyy HH:mm"));

        int renglon = 1;
        for (InscripcionProductor i : inscripciones) {
            for (PredioProductor pre : i.getPrediosActivos()) {
                int j = 0;
                Row r = s.createRow(renglon);
                XlsUtils.createCell(r, j++, renglon);
                XlsUtils.createCell(r, j++, i.getFolio());
                XlsUtils.createCell(r, j++, i.getDatosProductor().getNombre());
                XlsUtils.createCell(r, j++, i.getDatosProductor().getPrimerApellido());
                XlsUtils.createCell(r, j++, i.getDatosProductor().getSegundoApellido());
                XlsUtils.createCell(r, j++, i.getDatosProductor().getRfc());
                XlsUtils.createCell(r, j++, i.getDatosProductor().getCurp());
                XlsUtils.createCell(r, j++, i.getNumeroTelefono());
                XlsUtils.createCell(r, j++, i.getCorreoElectronico());
                Domicilio dp = i.getDomicilio();
                XlsUtils.createCell(r, j++, dp.getEstado().getNombre());
                XlsUtils.createCell(r, j++, dp.getMunicipio().getNombre());
                XlsUtils.createCell(r, j++, dp.getLocalidad());
                XlsUtils.createCell(r, j++, i.getEstatus().getNombre());
                XlsUtils.createCell(r, j++, pre.getFolio());
                XlsUtils.createCell(r, j++, pre.getTipoCultivo().getNombre());
                XlsUtils.createCell(r, j++, pre.getTipoPosesion().getNombre());
                XlsUtils.createCell(r, j++, pre.getTipoDocumentoPosesion().getNombre());
                XlsUtils.createCell(r, j++, pre.getRegimenHidrico().getNombre());
                XlsUtils.createCell(r, j++, pre.getVolumen());
                XlsUtils.createCell(r, j++, pre.getSuperficie());
                XlsUtils.createCell(r, j++, pre.getRendimiento());
                XlsUtils.createCell(r, j++, pre.getEstado().getNombre());
                XlsUtils.createCell(r, j++, pre.getMunicipio().getNombre());
                XlsUtils.createCell(r, j++, pre.getLocalidad());
                XlsUtils.createCell(r, j++, Objects.nonNull(pre.getLatitud()) ? pre.getLatitud().toString() : "--");
                XlsUtils.createCell(r, j++, Objects.nonNull(pre.getLongitud()) ? pre.getLongitud().toString() : "--");
                XlsUtils.createCell(r, j++, Objects.nonNull(pre.getLatitud1()) ? pre.getLatitud1().toString() : "--");
                XlsUtils.createCell(r, j++, Objects.nonNull(pre.getLatitud2()) ? pre.getLatitud2().toString() : "--");
                XlsUtils.createCell(r, j++, Objects.nonNull(pre.getLatitud3()) ? pre.getLatitud3().toString() : "--");
                XlsUtils.createCell(r, j++, Objects.nonNull(pre.getLatitud4()) ? pre.getLatitud4().toString() : "--");
                XlsUtils.createCell(r, j++, Objects.nonNull(pre.getLongitud1()) ? pre.getLongitud1().toString() : "--");
                XlsUtils.createCell(r, j++, Objects.nonNull(pre.getLongitud2()) ? pre.getLongitud2().toString() : "--");
                XlsUtils.createCell(r, j++, Objects.nonNull(pre.getLongitud3()) ? pre.getLongitud3().toString() : "--");
                XlsUtils.createCell(r, j++, Objects.nonNull(pre.getLongitud4()) ? pre.getLongitud4().toString() : "--");
                renglon++;
            }

        }

        for (int i = 0; i < encabezados.length; i++) {
            s.autoSizeColumn(i);
        }

        return XlsUtils.creaArchivo(wb);
    }
}
