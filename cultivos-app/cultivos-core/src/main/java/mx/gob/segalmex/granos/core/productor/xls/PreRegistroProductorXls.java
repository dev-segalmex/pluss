/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.xls;

import java.util.List;
import mx.gob.segalmex.common.core.util.XlsUtils;
import mx.gob.segalmex.pluss.modelo.productor.PreRegistroProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioPreRegistro;
import org.apache.commons.lang3.StringUtils;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.CreationHelper;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Component("preRegistroProductorXls")
public class PreRegistroProductorXls {

    public byte[] exporta(List<PreRegistroProductor> registros) {
        SXSSFWorkbook wb = new SXSSFWorkbook(100);
        CreationHelper ch = wb.getCreationHelper();
        Sheet s = wb.createSheet();

        String[] encabezados = new String[]{
            "No.",
            "Folio",
            "Fecha de registro",
            "Nombre",
            "Apellido Paterno",
            "Apellido Materno",
            "CURP",
            "RFC",
            "Correo",
            "Teléfono",
            "Cultivo",
            "Ciclo agrícola",
            "Tipo",
            "Cobertura(s)",
            "Número de predios",
            "No. predio",
            "Cultivo",
            "Posesión cultivo",
            "Régimen",
            "Estado",
            "Municipio",
            "Volumen",
            "Superficie",
            "Nombre beneficiario",
            "Apellido beneficiario"
        };
        XlsUtils.creaEncabezados(s, encabezados);

        CellStyle cs = wb.createCellStyle();
        cs.setDataFormat(ch.createDataFormat().getFormat("dd/MM/yyyy HH:mm"));

        int renglon = 1;
        int index = 1;
        for (PreRegistroProductor i : registros) {
            int noPredio = 1;
            for (PredioPreRegistro p : i.getPredios()) {
                int j = 0;
                Row r = s.createRow(renglon);
                XlsUtils.createCell(r, j++, index);
                XlsUtils.createCell(r, j++, i.getFolio());
                XlsUtils.createCell(r, j++, i.getFechaCreacion(), cs);
                XlsUtils.createCell(r, j++, i.getNombre());
                XlsUtils.createCell(r, j++, i.getPrimerApellido());
                XlsUtils.createCell(r, j++, StringUtils.defaultIfEmpty(i.getSegundoApellido(), "--"));
                XlsUtils.createCell(r, j++, i.getCurp());
                XlsUtils.createCell(r, j++, i.getRfc());
                XlsUtils.createCell(r, j++, StringUtils.defaultIfEmpty(i.getCorreoElectronico(), "--"));
                XlsUtils.createCell(r, j++, StringUtils.defaultIfEmpty(i.getTelefono(), "--"));
                XlsUtils.createCell(r, j++, i.getCultivo().getNombre());
                XlsUtils.createCell(r, j++, i.getCiclo().getNombre());
                XlsUtils.createCell(r, j++, i.getEtiquetaTipoCultivo());
                XlsUtils.createCell(r, j++, i.getEtiquetaCoberturas());
                XlsUtils.createCell(r, j++, i.getPredios().size());
                XlsUtils.createCell(r, j++, noPredio++);
                XlsUtils.createCell(r, j++, p.getTipoCultivo().getNombre());
                XlsUtils.createCell(r, j++, p.getTipoPosesion().getNombre());
                XlsUtils.createCell(r, j++, p.getRegimenHidrico().getNombre());
                XlsUtils.createCell(r, j++, p.getEstado().getNombre());
                XlsUtils.createCell(r, j++, p.getMunicipio().getNombre());
                XlsUtils.createCell(r, j++, p.getVolumen().doubleValue());
                XlsUtils.createCell(r, j++, p.getSuperficie().doubleValue());
                XlsUtils.createCell(r, j++, i.getNombreBeneficiario());
                XlsUtils.createCell(r, j++, i.getApellidosBeneficiario());
                renglon++;
            }
            index++;
        }

        for (int i = 0; i < encabezados.length; i++) {
            s.autoSizeColumn(i);
        }

        return XlsUtils.creaArchivo(wb);
    }

}
