/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.validador;

import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;

/**
 * Validador de pruebas que considera todos los CFDIs que terminan en triple
 * cero como no válidos.
 *
 * @author ismael
 */
@Slf4j
public class ValidadorCfdiDummy implements ValidadorCfdi {

    /**
     * Valida como positivos todos los CFDIs que el uuid del timbre fiscal no
     * terminan en triple cero.
     *
     * @param inscripcion la inscripción con el cfdi a validar.
     * @return <code>true</code> cuando el uuid del timbre fiscal digital no
     * termina en triple cero.
     */
    @Override
    public boolean valida(InscripcionFactura inscripcion) {
        LOGGER.info("Validando CFDI con ValidadorCfdiDummy...");
        return !inscripcion.getCfdi().getUuidTimbreFiscalDigital().endsWith("000");
    }

}
