/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor.pdf;

import com.lowagie.text.Document;
import com.lowagie.text.DocumentException;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfPCell;
import com.lowagie.text.pdf.PdfPTable;
import com.lowagie.text.pdf.PdfWriter;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.granos.core.contrato.pdf.PdfConstants;
import mx.gob.segalmex.granos.core.contrato.pdf.PdfHelper;
import mx.gob.segalmex.granos.core.contrato.pdf.SolicitudGobMxPdfHelper;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;

/**
 *
 * @author oscar
 */
@Slf4j
public class MaizInscripcionPdfHelper extends AbstractInscripcionPdfHelper {

    @Override
    public void generaContenido(InscripcionProductor inscripcion, Document d, PdfWriter writer) throws DocumentException {
        PdfPTable contenido = generaTablaContenido();
        LOGGER.info("Creando pdf productor Maíz.");
        agregaDatosPrograma(contenido, inscripcion);
        agregaDatosVentanilla(contenido, inscripcion);
        agregaDatosProductor(contenido, inscripcion);
        agregaDatosContacto(contenido, inscripcion);
        agregaDomicilioProductor(contenido, inscripcion);
        agregaContratos(contenido, inscripcion);
        agregaDatosBancarios(contenido, inscripcion);
        agregaPredios(contenido, inscripcion);
        agregaSociedades(contenido, inscripcion);
        agregaBeneficiario(contenido, inscripcion);
        agregaTerminos(contenido, inscripcion);
        agregaFirmas(contenido, inscripcion);

        PdfContentByte contentByte = writer.getDirectContent();
        SolicitudGobMxPdfHelper.writeColumns(d, contentByte, contenido, false);
    }

    @Override
    public void agregaTerminos(PdfPTable contenido, InscripcionProductor inscripcion) {
        PdfPTable seccion = generaTablaContenido();

        seccion.addCell(generaCeldaEncabezado("Términos y condiciones"));
        PdfPCell legales = null;
        switch (inscripcion.getCiclo().getClave()) {
            case "oi-2021":
                legales = PdfHelper.creaCeldaAviso(
                        "A través del presente registro, autorizo que los predios referidos e información productiva y personal,"
                        + " sean considerados en el Programa de Precios de Garantía a Productos Alimentarios Básicos."
                        + " Me obligo a proporcionar la información y/o documentación que me sea requerida por la SADER-SEGALMEX"
                        + " y a notificar cualquier cambio que sufra la información proporcionada. A la vez, manifiesto bajo protesta de decir la verdad que,"
                        + " soy mediano productor de Maíz, que los datos contenidos en este registro son ciertos y reales,"
                        + " y que adquirí un Instrumento de Administración de Riesgos (IAR) para participar o tengo un Contrato a Precio Fijo."
                        + " Acepto la responsabilidad en la veracidad de la información proporcionada a SEGALMEX."
                        + " En caso de falsificar total o parcialmente la información o realizar SIMULACIÓN,"
                        + " me comprometo a devolver sin reserva alguna el incentivo recibido y aceptar la sanción"
                        + " administrativa que conforme a derecho proceda (CANCELACIÓN). En caso de alterar la documentación y/o firmas, mi registro será cancelado."
                        + " En situaciones de PAGO(S) EN DEMASÍA (en cumplimiento a las obligaciones aceptadas en este registro), me comprometo a reintegrar el recurso,"
                        + " a través de depósito bancario a “SEGALMEX”. Estoy de acuerdo en beneficiarme con el incentivo, para la adquisición del IAR ($100/t),"
                        + " a los medianos productores de maíz, publicado en las Reglas de Operación del Programa de Precios de Garantía a Productos Alimentarios Básicos,"
                        + " para el ejercicio fiscal 2021 y en la Mecánica Operativa para Maíz comercializado por medianos productores,"
                        + " ciclo otoño-invierno 2020-2021. Envío mi registro FIRMADO por mi puño y letra,"
                        + " que ampara el total de toneladas de maíz que cosecharé en el ciclo mencionado, y estarán reflejadas en el sistema SEGALMEX-Maíz."
                        + " Estoy consciente que podré ser incluido en un BURÓ DE INCUMPLIMIENTO, que puede limitar mi participación,"
                        + " si no cumplo con lo estipulado en el Contrato de Compra Venta (CCV) firmado. Considero que puedo recibir o no el incentivo con la información presentada,"
                        + " soportada y de acuerdo a los criterios de elegibilidad.\n\n "
                        + "Los datos personales recabados serán protegidos, incorporados y tratados en el marco de la Ley General de Protección de Documentos Personales en Posesión "
                        + "de Sujetos Obligados, con fundamento en los artículos 3, 27 y 28.",
                        PdfConstants.BACKGROUND_GOBMX_WHITE,
                        PdfConstants.BACKGROUND_GOBMX_DARKGRAY);
                legales.setHorizontalAlignment(PdfPCell.ALIGN_JUSTIFIED);
            case "oi-2022":
                legales = PdfHelper.creaCeldaAviso(
                        "A través del presente registro, autorizo que los predios referidos e información "
                        + "productiva y personal, sean considerados en el Programa de Precios de Garantía a "
                        + "Productos Alimentarios Básicos. Me obligo a proporcionar la información y "
                        + "documentación que me sea requerida por la SADER-SEGALMEX y a notificar cualquier "
                        + "cambio que sufra la información proporcionada. A la vez, manifiesto bajo protesta "
                        + "de, decir la verdad que los datos contenidos en este registro son ciertos, y que "
                        + "adquirí un Instrumento de Administración de Riesgos (IAR). Acepto la "
                        + "responsabilidad, en la veracidad de la información proporcionada a SEGALMEX. En "
                        + "caso de incumplimiento total o parcial, así como de SIMULACIÓN, me comprometo a "
                        + "devolver sin reserva alguna el incentivo recibido y a aceptar la sanción "
                        + "administrativa que conforme a derecho proceda. Soy consciente que, en caso de "
                        + "alteración de documentación y firmas, mi registro será cancelado. También me "
                        + "comprometo a que en caso de que me realicen algún pago en DEMASÍA, lo "
                        + "reintegraré completamente, a través de un depósito bancario\n\n. Estoy de acuerdo "
                        + "en beneficiarme con el incentivo exclusivamente al IAR en del ciclo agrícola "
                        + "vigente, publicado en el Diario Oficial de la federación 2022 y la mecánica "
                        + "operativa de MAIZ del ciclo correspondiente. Firmo mi registro que ampara el "
                        + "total de toneladas de maíz (grano o semilla) producidas y comercializadas del "
                        + "ciclo mencionado, y estarán reflejadas en el sistema SEGALMEX. Estoy consciente "
                        + "que entraré en un buró de incumplimiento, que limita mi participación, si no "
                        + "cumplo con lo estipulado en el Contrato de Compra Venta firmado. Los datos "
                        + "personales recabados serán protegidos, incorporados y tratados en el marco de la "
                        + "Ley General de Protección de Documentos Personales en Posesión de Sujetos "
                        + "Obligados, con fundamento en los artículos 3, 27 y 28.",
                        PdfConstants.BACKGROUND_GOBMX_WHITE,
                        PdfConstants.BACKGROUND_GOBMX_DARKGRAY);
                legales.setHorizontalAlignment(PdfPCell.ALIGN_JUSTIFIED);
                break;
            default:
                legales = PdfHelper.creaCeldaAviso(
                        "A través del presente registro, autorizo que los predios referidos e información productiva y personal,"
                        + " sean considerados en el Programa de Precios de Garantía a Productos Alimentarios Básicos. Me obligo a proporcionar la información"
                        + " y/o documentación que me sea requerida por SEGALMEX y a notificar cualquier cambio que sufra la información proporcionada."
                        + " A la vez, manifiesto bajo protesta de decir la verdad que soy mediano productor y que los datos contenidos en este registro son"
                        + " ciertos y reales, y que adquirí un Instrumento de Administración de Riesgospara participar,por lo que acepto mi responsabilidad,"
                        + " en la veracidad de la información proporcionada a SEGALMEX. En caso denotificar total o parcialmente la información, así como"
                        + " de realizar SIMULACIÓN, me comprometo a devolver sin reserva alguna el incentivo recibido y aceptar la sanción"
                        + " administrativa que conforme a derecho proceda. También me comprometo a que en caso de que me realicen algún PAGO ENDEMASÍA,"
                        + " lo reintegraré através de un depósito bancario. Estoy de acuerdo en beneficiarme con el incentivo que brinda el Programa de Precios "
                        + " de Garantía del ciclo agrícola " + inscripcion.getCiclo().getNombre() + ". Envío mi registro FIRMADO por mi puño y letra y"
                        + " que ampara el total de toneladas de Maíz que cosecharé EXCLUSIVAMENTE en el ciclo " + inscripcion.getCiclo().getNombre()
                        + ", y estarán reflejadas en el sistema SEGALMEX. El incentivo (establecido por SEGALMEX) por tonelada será directamente sobre el"
                        + " IAR al finalizar la cosecha."
                        + " Considero que puedo recibir o no el incentivo con la información presentada, soportada y de acuerdo a los criterios de elegibilidad.\n\n"
                        + " Los datos personales recabados serán protegidos, incorporados y tratados en el marco de la Ley General de Protección"
                        + " de Documentos Personales en Posesión de Sujetos Obligados, con fundamento en los artículos 3, 27 y 28.",
                        PdfConstants.BACKGROUND_GOBMX_WHITE,
                        PdfConstants.BACKGROUND_GOBMX_DARKGRAY);
                legales.setHorizontalAlignment(PdfPCell.ALIGN_JUSTIFIED);
        }

        seccion.addCell(legales);

        contenido.addCell(seccion);
        contenido.addCell(PdfHelper.generaCeldaVacia(1));
    }

}
