/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato;

import java.math.BigDecimal;
import mx.gob.segalmex.granos.core.inscripcion.ProcesadorDatoCapturado;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.folio.GeneradorFolio;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.granos.core.contrato.busqueda.BuscadorContratoProductor;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import mx.gob.segalmex.pluss.modelo.secuencias.TipoFolioEnum;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.granos.core.contrato.busqueda.BuscadorInscripcionContrato;
import mx.gob.segalmex.granos.core.contrato.busqueda.ParametrosContratoProductor;
import mx.gob.segalmex.granos.core.contrato.busqueda.ParametrosInscripcionContrato;
import mx.gob.segalmex.granos.core.historico.BuscadorHistoricoRegistro;
import mx.gob.segalmex.granos.core.historico.ParametrosHistoricoRegistro;
import mx.gob.segalmex.granos.core.historico.ProcesadorHistoricoRegistro;
import mx.gob.segalmex.granos.core.inscripcion.ActualizadorEstatusHelper;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.historico.HistoricoRegistro;
import mx.gob.segalmex.pluss.modelo.inscripcion.ContratoProductor;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContratoEstado;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.contrato.CoberturaInscripcionContrato;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ismael
 */
@Service("procesadorInscripcionContrato")
public class ProcesadorInscripcionContratoJpa implements ProcesadorInscripcionContrato {

    private static final String SEPARADOR_CONTRATO = "-";

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private GeneradorFolio generadorFolio;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private ProcesadorHistoricoRegistro procesadorHistorico;

    @Autowired
    private BuscadorHistoricoRegistro buscadorHistorico;

    @Autowired
    private ProcesadorDatoCapturado procesadorDatoCapturado;

    @Autowired
    private BuscadorInscripcionContrato buscadorContrato;

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Autowired
    private ActualizadorEstatusHelper actualizadorEstatusHelper;

    @Autowired
    private BuscadorContratoProductor buscadorContratoProductor;

    @Transactional
    @Override
    public void procesa(InscripcionContrato inscripcion) {
        verificaCierre(inscripcion);
        if (Objects.isNull(inscripcion.getTipoPrecio())) {
            throw new IllegalArgumentException("El tipo de precio no puede ser nulo.");
        }

        inscripcion.setNumeroContrato(generaNumeroContrato(inscripcion));
        validaNumeroContrato(inscripcion.getNumeroContrato(), inscripcion.getCultivo(), inscripcion.getCiclo());
        inscripcion.setUuid(UUID.randomUUID().toString());
        inscripcion.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.NUEVO));
        inscripcion.setEstatusPrevio(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.DESCONOCIDO));
        inscripcion.setRfcEmpresa(inscripcion.getSucursal().getEmpresa().getRfc());
        inscripcion.setEstadosDestinoTexto(getEstadosDestinoTexto(inscripcion.getEstadosDestino()));
        inscripcion.setFolio(generadorFolio.genera(TipoFolioEnum.SEQ_FOLIO_MAIZ, 6));
        registroEntidad.guarda(inscripcion);

        int orden = 0;
        // Almacenamos los estados
        for (InscripcionContratoEstado e : inscripcion.getEstadosDestino()) {
            registroEntidad.guarda(e);
        }

        //Almacena las coberturas agregadas (IAR)
        orden = 0;
        for (CoberturaInscripcionContrato c : inscripcion.getCoberturas()) {
            c.setOrden(orden++);
            registroEntidad.guarda(c);
        }
        //Se almacenan los contratos productor
        registraContratosProductor(inscripcion, inscripcion.getContratosProductor(), false);

        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.CREACION,
                inscripcion.getUsuarioRegistra(), false, getEtiquetaGrupo(inscripcion));
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.ASIGNACION,
                inscripcion.getUsuarioRegistra(), true, getEtiquetaGrupo(inscripcion));
    }

    private void verificaCierre(InscripcionContrato inscripcion) {
        Parametro parametro = buscadorParametro.buscaElementoOpcional(inscripcion.getCultivo()
                .getClave() + ":" + inscripcion.getCiclo().getClave() + ":contratos:cierre");
        if (Objects.nonNull(parametro)) {
            Calendar cierre = SegalmexDateUtils
                    .parseCalendar(parametro.getValor(), "yyyy-MM-dd'T'HH:mm:ss");
            if (Calendar.getInstance().after(cierre)) {
                throw new SegalmexRuntimeException("Error al registrar el contrato.",
                        "El registro de contratos cerró el " + parametro.getValor());
            }
        }
    }

    private String generaNumeroContrato(InscripcionContrato i) {
        String rfc = i.getSucursal().getEmpresa().getRfc();

        StringBuilder sb = new StringBuilder();
        sb.append(i.getEstado().getAbreviatura());
        sb.append(SEPARADOR_CONTRATO);
        sb.append(i.getCiclo().getAbreviatura());
        sb.append(SEPARADOR_CONTRATO);
        sb.append(rfc.substring(rfc.length() - 3, rfc.length()));
        sb.append(SEPARADOR_CONTRATO);
        sb.append(SegalmexDateUtils.format(i.getFechaFirma(), "ddMMyy"));
        sb.append(SEPARADOR_CONTRATO);
        sb.append(i.getTipoCultivo().getAbreviatura());
        sb.append(SEPARADOR_CONTRATO);
        sb.append(StringUtils.leftPad(i.getNumeroContrato(), 3, "0"));
        return sb.toString();
    }

    @Transactional
    @Override
    public void asigna(InscripcionContrato inscripcion, Usuario usuario, Usuario validador) {
        inscripcion.setUsuarioValidador(validador);
        inscripcion.setUsuarioAsignado(validador);
        actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, EstatusInscripcionEnum.VALIDACION);

        ParametrosHistoricoRegistro parametros = new ParametrosHistoricoRegistro();
        parametros.setEntidad(inscripcion);
        parametros.setTipo(buscadorCatalogo.buscaElemento(TipoHistoricoInscripcion.class,
                TipoHistoricoInscripcionEnum.ASIGNACION.getClave()));

        HistoricoRegistro asignacion = buscadorHistorico.busca(parametros).get(0);
        procesadorHistorico.finaliza(asignacion, usuario);
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.VALIDACION,
                usuario, true, getEtiquetaGrupo(inscripcion));
        inscripcion.setFechaValidacion(Calendar.getInstance());
        registroEntidad.actualiza(inscripcion);

        // Generamos datos capturados
        procesadorDatoCapturado.procesa(inscripcion);
    }

    @Transactional
    @Override
    public void validaPositivo(InscripcionContrato inscripcion, Usuario usuario, List<DatoCapturado> capturados) {
        procesadorDatoCapturado.validaEstatus(inscripcion, capturados, EstatusInscripcionEnum.POSITIVA);
        validaNumeroProductoresContrato(inscripcion);
        // Cancelamos los históricos de validación y revalidación
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.REVALIDACION);
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.VALIDACION);

        // Actualizamos la inscripción y los datos capturados
        actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, EstatusInscripcionEnum.POSITIVA);
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.FINALIZACION,
                usuario, false, true, getEtiquetaGrupo(inscripcion));
        inscripcion.setFechaFinalizacion(Calendar.getInstance());
        registroEntidad.actualiza(inscripcion);
    }

    @Transactional
    @Override
    public void cancela(InscripcionContrato inscripcion, Usuario usuario, EstatusInscripcion estatus) {
        switch (inscripcion.getEstatus().getClave()) {
            case "cancelada":
            case "negativo":
                throw new SegalmexRuntimeException("Error", "El registro ya estaba en estatus "
                        + inscripcion.getEstatus().getNombre());
        }

        String etiquetaGrupo = getEtiquetaGrupo(inscripcion);
        TipoHistoricoInscripcionEnum tipoHistorico = null;
        switch (EstatusInscripcionEnum.getInstance(estatus.getClave())) {
            case CANCELADA:
                tipoHistorico = TipoHistoricoInscripcionEnum.CANCELADO;
                break;
            case NEGATIVO:
                tipoHistorico = TipoHistoricoInscripcionEnum.NEGATIVO;
                break;
        }
        procesadorHistorico.crea(inscripcion, tipoHistorico, usuario, false,
                true, etiquetaGrupo);

        actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, estatus);
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.FINALIZACION,
                usuario, false, true, etiquetaGrupo);
        inscripcion.setFechaFinalizacion(Calendar.getInstance());
        registroEntidad.actualiza(inscripcion);
    }

    @Transactional
    @Override
    public void solicitaCorreccion(InscripcionContrato inscripcion, Usuario usuario, List<DatoCapturado> capturados) {
        actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, EstatusInscripcionEnum.CORRECCION);
        procesadorDatoCapturado.solicitaCorreccion(inscripcion, capturados);

        // Cancelamos los históricos de validación y revalidación
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.REVALIDACION);
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.VALIDACION);

        // Generamos el histórico de corrección
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.CORRECCION,
                usuario, true, true, getEtiquetaGrupo(inscripcion));

        // Lo reasignamos al usuario que registró
        inscripcion.setUsuarioAsignado(inscripcion.getUsuarioRegistra());
        registroEntidad.actualiza(inscripcion);
    }

    @Transactional
    @Override
    public void corrige(InscripcionContrato inscripcion, Usuario usuario, List<DatoCapturado> capturados) {
        actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, EstatusInscripcionEnum.SOLVENTADA);
        verificaCierreCorreccion(inscripcion);
        // Validamos los valores modificados
        procesadorDatoCapturado.corrige(inscripcion, capturados);

        // Cancelamos históricos de corrección
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.CORRECCION);

        // Generamos el histórico de revalidación
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.REVALIDACION,
                usuario, true, true, getEtiquetaGrupo(inscripcion));

        // Lo reasignamos al usuario que validó
        inscripcion.setUsuarioAsignado(inscripcion.getUsuarioValidador());

        // Actualizamos las entidades
        registroEntidad.actualiza(inscripcion);
    }

    @Transactional
    @Override
    public void actualiza(InscripcionContrato inscripcion, List<DatoCapturado> capturados) {
        verificaCierreCorreccion(inscripcion);
        // Validamos los valores modificados
        procesadorDatoCapturado.corrige(inscripcion, capturados);

        // Actualizamos las entidades
        registroEntidad.actualiza(inscripcion);
    }

    @Transactional
    @Override
    public void revalida(InscripcionContrato inscripcion, Usuario usuario) {
        switch (inscripcion.getEstatus().getClave()) {
            case "positiva":
            case "cancelada":
            case "correccion":
            case "negativo":
                break;
            default:
                throw new SegalmexRuntimeException("Error", "El registro no se puede revalidar ya que se "
                        + "encuentra en estado: " + inscripcion.getEstatus().getNombre());
        }
        actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, EstatusInscripcionEnum.SOLVENTADA);

        // Cancelamos históricos de corrección (por si estaba en corrección)
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.CORRECCION);

        // Generamos el histórico de revalidación
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.REVALIDACION,
                usuario, true, true, getEtiquetaGrupo(inscripcion));

        // Lo reasignamos al usuario que validó
        inscripcion.setUsuarioAsignado(inscripcion.getUsuarioValidador());
    }

    @Override
    @Transactional
    public void solicitaComplemento(InscripcionContrato inscripcion, Usuario usuario) {
        // Generamos el histórico de solicitud de complemento
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.SOLICITUD_COMPLEMENTO,
                usuario, true, false, getEtiquetaGrupo(inscripcion));
    }

    @Override
    @Transactional
    public void aceptaComplemento(InscripcionContrato inscripcion, Usuario usuario) {
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.SOLICITUD_COMPLEMENTO);
        if (inscripcion.getCoberturas().isEmpty()) {
            //si no tine cobertura creamos Histórico de documentación de cobertura
            procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.DOCUMENTACION_COBERTURA,
                    usuario, true, true, getEtiquetaGrupo(inscripcion));
        } else {
            // creamos Historico de autorización
            procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.COMPLEMENTO_AUTORIZADO,
                    usuario, false, true, getEtiquetaGrupo(inscripcion));
        }
    }

    @Override
    @Transactional
    public void rechazaComplemento(InscripcionContrato inscripcion, Usuario usuario) {
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.SOLICITUD_COMPLEMENTO);
        // creamos Historico de rechazo
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.COMPLEMENTO_RECHAZADO,
                usuario, false, true, getEtiquetaGrupo(inscripcion));
    }

    @Override
    @Transactional
    public void procesaIar(InscripcionContrato inscripcion, Usuario usuario) {
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.DOCUMENTACION_COBERTURA);
        //Almacena las coberturas agregadas (IAR)
        int orden = 0;
        for (CoberturaInscripcionContrato c : inscripcion.getCoberturas()) {
            c.setOrden(orden++);
            registroEntidad.guarda(c);
        }
        //Generamos datos capturados para los IAR
        procesadorDatoCapturado.procesaIar(inscripcion);
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.VALIDACION_COMPlEMENTO,
                usuario, true, true, getEtiquetaGrupo(inscripcion));
    }

    @Override
    @Transactional
    public void solicitaCorreccionComplemento(InscripcionContrato inscripcion, Usuario usuario, List<DatoCapturado> capturados) {

        procesadorDatoCapturado.solicitaCorreccion(inscripcion, capturados);

        // Cancelamos los históricos de validación y revalidación
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.VALIDACION_COMPlEMENTO);
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.REVALIDACION_COMPlEMENTO);

        // Generamos el histórico de corrección complemento
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.CORRECION_COMPlEMENTO,
                usuario, true, true, getEtiquetaGrupo(inscripcion));
    }

    @Override
    @Transactional
    public void validaComplementoPositivo(InscripcionContrato inscripcion, Usuario usuario, List<DatoCapturado> capturados) {
        procesadorDatoCapturado.validaEstatus(inscripcion, capturados, EstatusInscripcionEnum.POSITIVA);

        // Cancelamos los históricos de validación y quizá despues agreguemos revalidación y se tendría que cancelar
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.VALIDACION_COMPlEMENTO);
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.REVALIDACION_COMPlEMENTO);

        // creamos Historico de autorización
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.COMPLEMENTO_AUTORIZADO,
                usuario, false, true, getEtiquetaGrupo(inscripcion));
    }

    @Override
    @Transactional
    public void corrigeComplemento(InscripcionContrato inscripcion, Usuario usuario, List<DatoCapturado> capturados) {
        // Corregimos los valores modificados
        procesadorDatoCapturado.corrige(inscripcion, capturados);

        // Cancelamos históricos de corrección complemento
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.CORRECION_COMPlEMENTO);

        // Generamos el histórico de revalidación complemento
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.REVALIDACION_COMPlEMENTO,
                usuario, true, true, getEtiquetaGrupo(inscripcion));

        // Actualizamos las entidades
        registroEntidad.actualiza(inscripcion);
    }

    private boolean cancelaHistoricos(InscripcionContrato inscripcion, Usuario usuario, TipoHistoricoInscripcionEnum tipo) {
        // Cancelamos los históricos de validación y revalidación
        List<HistoricoRegistro> historicos = buscadorHistorico.busca(inscripcion,
                buscadorCatalogo.buscaElemento(TipoHistoricoInscripcion.class,
                        tipo.getClave()), true);
        for (HistoricoRegistro abierto : historicos) {
            procesadorHistorico.finaliza(abierto, usuario);
        }

        return !historicos.isEmpty();
    }

    private String getEstadosDestinoTexto(List<InscripcionContratoEstado> estados) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < estados.size(); i++) {
            sb.append(estados.get(i).getEstado().getNombre());
            sb.append(i < estados.size() - 1 ? ", " : "");
        }
        return sb.toString();
    }

    public void validaNumeroContrato(String numeroContrato, Cultivo cultivo, CicloAgricola ciclo) {
        ParametrosInscripcionContrato parametros = new ParametrosInscripcionContrato();
        parametros.setNumeroContrato(numeroContrato);
        parametros.setCultivo(cultivo);
        parametros.setCiclo(ciclo);
        if (!buscadorContrato.busca(parametros).isEmpty()) {
            throw new SegalmexRuntimeException("Error de registro.",
                    "El número de contrato ya existe.");
        }
    }

    /**
     * Se encarga de armar la etiquetaGrupo que debe llevar su Histórico.
     *
     * @param inscripcion
     * @return
     */
    private String getEtiquetaGrupo(InscripcionContrato inscripcion) {
        return inscripcion.getCultivo().getClave() + ":" + inscripcion.getCiclo().getClave();
    }

    private void verificaCierreCorreccion(InscripcionContrato inscripcion) {
        Parametro parametro = buscadorParametro.buscaElementoOpcional(inscripcion.getCultivo()
                .getClave() + ":" + inscripcion.getCiclo().getClave() + ":cierre-correccion-contratos");
        if (Objects.nonNull(parametro)) {
            Calendar cierre = SegalmexDateUtils
                    .parseCalendar(parametro.getValor(), "yyyy-MM-dd'T'HH:mm:ss");
            if (Calendar.getInstance().after(cierre)) {
                throw new SegalmexRuntimeException("Error al corregir el contrato.",
                        "La corrección de contratos cerró el " + parametro.getValor());
            }
        }
    }

    @Transactional
    @Override
    public void solicitaCorreccionContratoProductor(InscripcionContrato inscripcion, Usuario usuario) {
        actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, EstatusInscripcionEnum.CORRECCION);
        // Cancelamos los históricos de validación y revalidación
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.REVALIDACION);
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.VALIDACION);
        // Generamos el histórico de Correccion contrato productor
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.CORRECION_CONTRATO_PRODUCTOR,
                usuario, true, true, getEtiquetaGrupo(inscripcion));
        // Lo reasignamos al usuario que registró
        inscripcion.setUsuarioAsignado(inscripcion.getUsuarioRegistra());
        registroEntidad.actualiza(inscripcion);
    }

    @Transactional
    @Override
    public void corrigeContratoProductor(InscripcionContrato inscripcion, Usuario usuario, List<ContratoProductor> contratosProductor) {
        actualizadorEstatusHelper.cambiaEstatusInscripcion(inscripcion, EstatusInscripcionEnum.SOLVENTADA);
        verificaCierreCorreccion(inscripcion);
        //cancelamos el historico de CorreccionContratoProductor
        cancelaHistoricos(inscripcion, usuario, TipoHistoricoInscripcionEnum.CORRECION_CONTRATO_PRODUCTOR);
        //cancelamos los contratosPRoductorPrevios
        cancelaContratosProductor(inscripcion.getContratosProductor());
        //agregamos la nueva lista de ContratosProductor
        registraContratosProductor(inscripcion, contratosProductor, true);
        procesadorHistorico.crea(inscripcion, TipoHistoricoInscripcionEnum.REVALIDACION,
                usuario, true, true, getEtiquetaGrupo(inscripcion));
        inscripcion.setUsuarioAsignado(inscripcion.getUsuarioValidador());
        registroEntidad.actualiza(inscripcion);
    }

    private void cancelaContratosProductor(List<ContratoProductor> contratosProductor) {
        for (ContratoProductor cp : contratosProductor) {
            cp.setCorrecto(false);
            cp.setEstatusFechaBaja(SegalmexDateUtils.format(Calendar.getInstance(), "yyyy-MM-dd'T'HH:mm:ss"));
            registroEntidad.actualiza(cp);
        }
    }

    private void registraContratosProductor(InscripcionContrato inscripcion, List<ContratoProductor> contratosProductor, boolean recalcula) {
        int orden = 0;
        BigDecimal volumen = BigDecimal.ZERO;
        BigDecimal superficie = BigDecimal.ZERO;
        for (ContratoProductor cp : contratosProductor) {
            if (cp.isCorrecto()) {
                volumen = volumen.add(cp.getVolumenContrato());
                superficie = superficie.add(cp.getSuperficie());
                cp.setOrden(orden++);
                cp.setInscripcionContrato(inscripcion);
                registroEntidad.guarda(cp);
            }
        }
        if (recalcula) {
            inscripcion.setNumeroProductores(orden + 1);
            inscripcion.setVolumenTotal(volumen);
            inscripcion.setSuperficie(superficie);
        }
    }

    private void validaNumeroProductoresContrato(InscripcionContrato contrato) {
        ParametrosContratoProductor parametros = new ParametrosContratoProductor();
        parametros.setInscripcion(contrato);
        if (contrato.getNumeroProductores() != buscadorContratoProductor.busca(parametros).size()) {
            throw new SegalmexRuntimeException("Error al validar:",
                    "El número de productores no coincide con la cantidad de "
                    + "productores que respaldan el contrato.");
        }
    }
}
