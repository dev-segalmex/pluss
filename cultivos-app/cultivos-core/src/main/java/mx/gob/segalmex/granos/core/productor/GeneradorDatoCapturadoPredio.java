/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor;

import mx.gob.segalmex.granos.core.inscripcion.InscripcionDatoCapturado;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;

/**
 *
 * @author jurgen
 */
public interface GeneradorDatoCapturadoPredio {

    void generaDatosCapturadosPredio(InscripcionProductor inscripcion, InscripcionDatoCapturado idc, PredioProductor p);

}
