/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura.modelo;

import java.math.BigDecimal;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 *
 * @author ismael
 */
@Getter
@Setter
@XmlAccessorType(XmlAccessType.NONE)
public class CfdiConcepto {

    @XmlAttribute(name = "ClaveProdServ")
    private String claveProductoServicio;

    @XmlAttribute(name = "ClaveUnidad")
    private String claveUnidad;

    @XmlAttribute(name = "Cantidad")
    private BigDecimal cantidad;

    @XmlAttribute(name = "ValorUnitario")
    private BigDecimal valorUnitario;

    @XmlAttribute(name = "Unidad")
    private String unidad;

    @XmlAttribute(name = "Descripcion")
    private String descripcion;

    @XmlAttribute(name = "Importe")
    private BigDecimal importe;

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("claveProductoServicio", claveProductoServicio)
                .append("claveUnidad", claveUnidad)
                .append("cantidad", cantidad)
                .append("valorUnitario", valorUnitario)
                .append("unidad", unidad)
                .append("descripcion", descripcion)
                .append("importe", importe)
                .toString();
    }

}
