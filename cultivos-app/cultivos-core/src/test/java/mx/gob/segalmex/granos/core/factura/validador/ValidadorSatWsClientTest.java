/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.factura.validador;

import java.math.BigDecimal;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.factura.validador.ValidadorCfdiSatWsClient;
import mx.gob.segalmex.pluss.modelo.factura.Cfdi;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import org.junit.Test;
import org.springframework.ws.client.core.WebServiceTemplate;

/**
 *
 * @author ismael
 */
@Slf4j
public class ValidadorSatWsClientTest {

    @Test
    public void validaTest() {
        ValidadorCfdiSatWsClient client = new ValidadorCfdiSatWsClient();
        client.setWsTemplate(new WebServiceTemplate());
        client.setUrl("https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc");

        Cfdi cfdi = new Cfdi();
        cfdi.setRfcEmisor("CIN790627UX7");
        cfdi.setRfcReceptor("HEGI821214D79");
        cfdi.setTotal(new BigDecimal("14811.000"));
        cfdi.setUuidTimbreFiscalDigital("b8d8bf78-6ed2-4730-bde7-5836422b4dcC");
        InscripcionFactura inscripcion = new InscripcionFactura();
        inscripcion.setCfdi(cfdi);
        boolean valida = client.valida(inscripcion);
        LOGGER.debug("La factura es válida: {}", valida);
    }
}
