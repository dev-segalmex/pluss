/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.validador;

import java.util.ArrayList;
import java.util.List;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import org.junit.Test;

/**
 *
 * @author oscar
 */
public class ValidadorRfcHelperTest {


    @Test
    public void validaTestExitoA() {
        ValidadorRfcHelper.valida("uiso8901061ba", ValidadorRfcHelper.GENERICOS, "del productor");
        ValidadorRfcHelper.valida("IIZ030710FP7", ValidadorRfcHelper.GENERICOS, "del productor");
        ValidadorRfcHelper.valida("UISO8901061BA", ValidadorRfcHelper.GENERICOS, "del productor");
        ValidadorRfcHelper.valida("CIN790627UX7", ValidadorRfcHelper.GENERICOS, "del productor");
        ValidadorRfcHelper.valida("HEGI821214D79", ValidadorRfcHelper.GENERICOS, "del productor");
        ValidadorRfcHelper.valida("XAXX010101000", ValidadorRfcHelper.GENERICOS, "del productor");
        ValidadorRfcHelper.valida("XEXX010101000", ValidadorRfcHelper.GENERICOS, "del productor");
        ValidadorRfcHelper.valida("MART900123VC0", ValidadorRfcHelper.GENERICOS, "del productor");
        ValidadorRfcHelper.valida("mart900123vc0", ValidadorRfcHelper.GENERICOS, "del productor");

    }

    @Test
    public void validaTestExitoB() {
        ValidadorRfcHelper.valida("IIZ030710FP7", null, "del productor");
        ValidadorRfcHelper.valida("UISO8901061BA", null, "del productor");
        ValidadorRfcHelper.valida("CIN790627UX7", null, "del productor");
        ValidadorRfcHelper.valida("HEGI821214D79", null, "del productor");
        ValidadorRfcHelper.valida("MART900123VC0", null, "del productor");
    }

    @Test
    public void validaTestExitoC() {
        List<String> excepciones = new ArrayList<>();
        excepciones.add("UISO890106000");
        ValidadorRfcHelper.valida("UISO890106000", excepciones, "del productor");
    }

    @Test(expected = SegalmexRuntimeException.class)
    public void validaTestErrorA() {
        ValidadorRfcHelper.valida("XAXX010101000", null, "del productor");
    }

    @Test(expected = SegalmexRuntimeException.class)
    public void validaTestErrorC() {
        ValidadorRfcHelper.valida("UISO8901061B10", ValidadorRfcHelper.GENERICOS, "del productor");
    }

    @Test(expected = SegalmexRuntimeException.class)
    public void validaTestErrorD() {
        ValidadorRfcHelper.valida("MART900123VC7", ValidadorRfcHelper.GENERICOS, "del productor");
    }

    @Test(expected = NullPointerException.class)
    public void validaTestErrorE() {
        ValidadorRfcHelper.valida(null, ValidadorRfcHelper.GENERICOS, "del productor");
    }

    @Test(expected = SegalmexRuntimeException.class)
    public void validaTestErrorF() {
        ValidadorRfcHelper.valida("ABC11", ValidadorRfcHelper.GENERICOS, "del productor");
    }

}
