/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato.pdf;

import com.lowagie.text.DocumentException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.Municipio;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricolaEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.Empresa;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoEmpresa;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoPrecio;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author ismael
 */
public class InscripcionContratoPdfCreatorTest {

    @Test
    public void testCreate() throws DocumentException, IOException {
        InscripcionContrato inscripcion = new InscripcionContrato();
        inscripcion.setFechaCreacion(Calendar.getInstance());
        inscripcion.setFolio("123456");
        inscripcion.setNumeroContrato("SIN-OI2020-A00-010101-A-001");
        inscripcion.setFechaFirma(Calendar.getInstance());
        inscripcion.setNumeroProductores(10);
        inscripcion.setCultivo(new Cultivo());
        inscripcion.getCultivo().setNombre("Maíz comercial");
        inscripcion.getCultivo().setClave("maiz-comercial");
        inscripcion.setCiclo(new CicloAgricola());
        inscripcion.getCiclo().setNombre("O.I. 2021-2022");
        inscripcion.getCiclo().setClave(CicloAgricolaEnum.OI2022.getClave());
        inscripcion.setTipoCultivo(new TipoCultivo());
        inscripcion.getTipoCultivo().setNombre("Maíz amarillo");

        inscripcion.setTipoEmpresaComprador(new TipoEmpresa());
        inscripcion.getTipoEmpresaComprador().setNombre("Comercializadora");
        inscripcion.setTipoEmpresaVendedor(new TipoEmpresa());
        inscripcion.getTipoEmpresaVendedor().setNombre("Productor");

        Empresa e = new Empresa();
        e.setRfc("MIL880101T00");

        Sucursal s = new Sucursal();
        s.setNombre("Molino Milpa Alta");
        s.setEmpresa(e);
        s.setEstado(new Estado());
        s.getEstado().setNombre("Ciudad de México");
        s.setMunicipio(new Municipio());
        s.getMunicipio().setNombre("Milpa Alta");
        s.setLocalidad("Milpa Alta");
        inscripcion.setSucursal(s);

        inscripcion.setEstado(new Estado());
        inscripcion.getEstado().setNombre("Sinaloa");
        inscripcion.setPrecioFijo(BigDecimal.ONE);
        inscripcion.setPrecioFuturo(BigDecimal.ZERO);
        inscripcion.setBaseAcordada(BigDecimal.ZERO);
        inscripcion.setSuperficie(BigDecimal.TEN);
        inscripcion.setVolumenTotal(BigDecimal.TEN);
        inscripcion.setTipoPrecio(new TipoPrecio());
        inscripcion.getTipoPrecio().setNombre("Precio cerrado");
        inscripcion.setContratosProductor(new ArrayList<>());
        inscripcion.setCoberturas(new ArrayList<>());

        InscripcionContratoPdfCreator creator = new InscripcionContratoPdfCreator("dev");
        byte[] pdf = creator.create(inscripcion);

        FileOutputStream fos = new FileOutputStream("target/inscripcion-contrato.pdf");
        IOUtils.write(pdf, fos);
        fos.close();
    }
}
