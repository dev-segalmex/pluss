/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.productor;

import java.math.BigDecimal;
import java.util.ArrayList;
import org.junit.Assert;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import org.junit.Test;

/**
 *
 * @author jurgen
 */
public class EstadoProductorHelperTest {

    @Test
    public void prueba() {
        InscripcionProductor i = new InscripcionProductor();
        PredioProductor predioS = new PredioProductor();
        PredioProductor predioSa = new PredioProductor();
        PredioProductor predioT = new PredioProductor();
        //
        Estado sinaloa = new Estado();
        //
        Estado sonora = new Estado();
        //
        Estado tamaulipas = new Estado();

        sinaloa.setClave("25");
        sinaloa.setNombre("Sinaloa");
        predioS.setEstado(sinaloa);
        predioS.setSuperficie(new BigDecimal(150.0009));
        i.setPredios(new ArrayList<>());
        i.getPredios().add(predioS);

        sonora.setClave("26");
        sonora.setNombre("Sonora");
        predioSa.setEstado(sonora);
        predioSa.setSuperficie(new BigDecimal(150));
        i.getPredios().add(predioSa);

        tamaulipas.setClave("28");
        tamaulipas.setNombre("Tamaulipas");
        predioT.setEstado(tamaulipas);
        predioT.setSuperficie(new BigDecimal(160));
        i.getPredios().add(predioT);
        Estado resultado = EstadoProductorHelper.getEstado(i);
        Assert.assertNotEquals("No debe ser Sinaloa.", sinaloa, resultado);
        Assert.assertNotEquals("No debe ser Sonara.", sonora, resultado);
        Assert.assertEquals("Se esperaba Tamaulipas.", tamaulipas, resultado);
    }

    @Test
    public void prueba_2() {
        InscripcionProductor i = new InscripcionProductor();
        PredioProductor predioS = new PredioProductor();
        Estado sinaloa = new Estado();

        sinaloa.setClave("25");
        sinaloa.setNombre("Sinaloa");
        predioS.setEstado(sinaloa);
        predioS.setSuperficie(new BigDecimal(150.0009));
        i.setPredios(new ArrayList<>());
        i.getPredios().add(predioS);

        Estado resultado = EstadoProductorHelper.getEstado(i);

        Assert.assertEquals("Se esperaba Sinaloa.", sinaloa, resultado);
    }
}
