/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.UUID;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.datos.ProcesadorCargadorDatos;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.pluss.modelo.catalogos.Banco;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.Municipio;
import mx.gob.segalmex.pluss.modelo.catalogos.Pais;
import mx.gob.segalmex.pluss.modelo.catalogos.Parentesco;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoDocumento;
import mx.gob.segalmex.pluss.modelo.factura.Factura;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import mx.gob.segalmex.pluss.modelo.factura.UsoFactura;
import mx.gob.segalmex.pluss.modelo.personas.Sexo;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersona;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.GrupoIndigena;
import mx.gob.segalmex.pluss.modelo.catalogos.NivelEstudio;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoProductor;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.productor.CuentaBancaria;
import mx.gob.segalmex.pluss.modelo.productor.DatosProductor;
import mx.gob.segalmex.pluss.modelo.productor.Domicilio;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCicloGrupo;
import mx.gob.segalmex.pluss.modelo.productor.RegimenHidrico;
import mx.gob.segalmex.pluss.modelo.productor.TipoPosesion;
import mx.gob.segalmex.pluss.modelo.productor.TipoProductorEnum;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ismael
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:testCultivosApplicationContext.xml",
    "classpath:mysqlPersistenceApplicationContext.xml"
})
@Transactional
@Slf4j
public class FacturaProductorHelperTest {

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private ProcesadorCargadorDatos cargadorDatos;

    @Autowired
    private FacturaProductorHelper helper;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private RegistroEntidad registroEntidad;

    @Before
    @Transactional
    public void setUp() throws InterruptedException {
        cargadorDatos.exec();
        entityManager.clear();
        guardaInscripcionProductor("arroz", "arroz-grueso", "00000A");
        guardaInscripcionProductor("maiz-comercial", "maiz-blanco", "00000M");

    }

    @Test
    @Transactional
    public void finalizaPositivoArroz() {
        LOGGER.debug("========== Arroz ==========");

        List<InformacionEstimulo> informaciones = new ArrayList<>();
        informaciones.add(new InformacionEstimulo(new BigDecimal("120"), new BigDecimal("200"), 1));
        informaciones.add(new InformacionEstimulo(new BigDecimal("300"), new BigDecimal("100"), 2));

        ProductorCiclo pc = new ProductorCiclo();
        pc.setToneladasFacturadas(BigDecimal.ZERO);
        pc.setToneladasTotales(new BigDecimal("320"));
        pc.setGrupos(new ArrayList<>());
        ProductorCicloGrupo pcg = new ProductorCicloGrupo();
        pcg.setLimite(new BigDecimal(600));
        pcg.setToneladasTotales(new BigDecimal("320"));
        pcg.setGrupoEstimulo("arroz");
        pc.getGrupos().add(pcg);
        pc.setFolio("00000A");

        Factura f = new Factura();
        f.setToneladasDisponibles(new BigDecimal("400"));
        f.setEstatus("otro");

        InscripcionFactura inscripcion = new InscripcionFactura();
        inscripcion.setTipoFactura("productor");
        inscripcion.setFacturables(new BigDecimal("100"));
        inscripcion.setCultivo(new Cultivo());
        inscripcion.getCultivo().setClave("arroz");
        inscripcion.setTipoCultivo(new TipoCultivo());
        inscripcion.getTipoCultivo().setClave("arroz-grueso");
        inscripcion.getTipoCultivo().setGrupoEstimulo("arroz");
        inscripcion.setEstado(new Estado());
        inscripcion.getEstado().setClave("1");
        inscripcion.setFactura(f);
        inscripcion.setProductorCiclo(pc);

        List<UsoFactura> usos = helper.finalizaPositivo(inscripcion, pc, informaciones);
        for (UsoFactura u : usos) {
            LOGGER.debug("######## Toneladas: {}, Estímulo: {} ########", u.getToneladas(), u.getEstimuloTonelada());
        }
        LOGGER.debug("========== Fin factura 1 ==========");

        inscripcion = new InscripcionFactura();
        inscripcion.setTipoFactura("productor");
        inscripcion.setFacturables(new BigDecimal("100"));
        inscripcion.setCultivo(new Cultivo());
        inscripcion.getCultivo().setClave("arroz");
        inscripcion.setTipoCultivo(new TipoCultivo());
        inscripcion.getTipoCultivo().setClave("arroz-grueso");
        inscripcion.getTipoCultivo().setGrupoEstimulo("arroz");
        inscripcion.setEstado(new Estado());
        inscripcion.getEstado().setClave("1");
        inscripcion.setFactura(f);
        inscripcion.setProductorCiclo(pc);

        usos = helper.finalizaPositivo(inscripcion, pc, informaciones);
        for (UsoFactura u : usos) {
            LOGGER.debug("######## Toneladas: {}, Estímulo: {} ########", u.getToneladas(), u.getEstimuloTonelada());
        }
        LOGGER.debug("========== Fin factura 2 ==========");

        inscripcion = new InscripcionFactura();
        inscripcion.setTipoFactura("productor");
        inscripcion.setFacturables(new BigDecimal("100"));
        inscripcion.setCultivo(new Cultivo());
        inscripcion.getCultivo().setClave("arroz");
        inscripcion.setTipoCultivo(new TipoCultivo());
        inscripcion.getTipoCultivo().setClave("arroz-grueso");
        inscripcion.getTipoCultivo().setGrupoEstimulo("arroz");
        inscripcion.setEstado(new Estado());
        inscripcion.getEstado().setClave("1");
        inscripcion.setFactura(f);
        inscripcion.setProductorCiclo(pc);

        usos = helper.finalizaPositivo(inscripcion, pc, informaciones);
        for (UsoFactura u : usos) {
            LOGGER.debug("######## Toneladas: {}, Estímulo: {} ########", u.getToneladas(), u.getEstimuloTonelada());
        }
        LOGGER.debug("========== Fin factura 3 ==========");
    }

    @Test
    @Transactional
    public void finalizaPositivoMaiz() {
        LOGGER.debug("========== Maíz ==========");

        List<InformacionEstimulo> informaciones = new ArrayList<>();
        informaciones.add(new InformacionEstimulo(new BigDecimal("600"), new BigDecimal("150"), 1));

        ProductorCiclo pc = new ProductorCiclo();
        pc.setToneladasFacturadas(BigDecimal.ZERO);
        pc.setToneladasTotales(new BigDecimal("520"));
        pc.setGrupos(new ArrayList<>());
        ProductorCicloGrupo pcg = new ProductorCicloGrupo();
        pcg.setLimite(new BigDecimal(600));
        pcg.setToneladasContratadas(new BigDecimal(520));
        pcg.setToneladasCobertura(new BigDecimal(510));
        pcg.setToneladasTotales(new BigDecimal("520"));
        pcg.setGrupoEstimulo("maiz");
        pc.getGrupos().add(pcg);
        pc.setFolio("00000M");

        Factura f = new Factura();
        f.setToneladasDisponibles(new BigDecimal("400"));
        f.setEstatus("otro");

        InscripcionFactura inscripcion = new InscripcionFactura();
        inscripcion.setTipoFactura("productor");
        inscripcion.setFacturables(new BigDecimal("220"));
        inscripcion.setCultivo(new Cultivo());
        inscripcion.getCultivo().setClave("maiz-comercial");
        inscripcion.setTipoCultivo(new TipoCultivo());
        inscripcion.getTipoCultivo().setClave("maiz-blanco");
        inscripcion.getTipoCultivo().setGrupoEstimulo("maiz");
        inscripcion.setEstado(new Estado());
        inscripcion.getEstado().setClave("1");
        inscripcion.setFactura(f);
        inscripcion.setProductorCiclo(pc);

        List<UsoFactura> usos = helper.finalizaPositivo(inscripcion, pc, informaciones);
        for (UsoFactura u : usos) {
            LOGGER.debug("Toneladas: {}, Estímulo: {}", u.getToneladas(), u.getEstimuloTonelada());
        }
        LOGGER.debug("========== Fin factura 1 ==========");

        inscripcion = new InscripcionFactura();
        inscripcion.setTipoFactura("productor");
        inscripcion.setFacturables(new BigDecimal("160"));
        inscripcion.setCultivo(new Cultivo());
        inscripcion.getCultivo().setClave("maiz-comercial");
        inscripcion.setTipoCultivo(new TipoCultivo());
        inscripcion.getTipoCultivo().setClave("maiz-blanco");
        inscripcion.getTipoCultivo().setGrupoEstimulo("maiz");
        inscripcion.setEstado(new Estado());
        inscripcion.getEstado().setClave("1");
        inscripcion.setFactura(f);
        inscripcion.setProductorCiclo(pc);

        usos = helper.finalizaPositivo(inscripcion, pc, informaciones);
        for (UsoFactura u : usos) {
            LOGGER.debug("Toneladas: {}, Estímulo: {}", u.getToneladas(), u.getEstimuloTonelada());
        }
        LOGGER.debug("========== Fin factura 2 ==========");
    }

    private void guardaInscripcionProductor(String cultivo, String tipoCultivo, String folio) {
        InscripcionProductor ip = new InscripcionProductor();
        ip.setFolio(folio);
        ip.setApellidosBeneficiario("--");
        ip.setBodega(buscadorCatalogo.buscaElemento(Sucursal.class, "0000"));
        ip.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, "oi-2021"));
        ip.setGrupoIndigena(buscadorCatalogo.buscaElemento(GrupoIndigena.class, "ninguno"));
        ip.setNivelEstudio(buscadorCatalogo.buscaElemento(NivelEstudio.class, "desconocido"));
        ip.setClaveArchivos("--");
        ip.setCorreoElectronico("--");
        CuentaBancaria cb = new CuentaBancaria();
        cb.setBanco(buscadorCatalogo.buscaElemento(Banco.class, "002"));
        cb.setClabe("051234567899876543");
        cb.setNumero("--");
        ip.setCuentaBancaria(cb);
        registroEntidad.guarda(ip.getCuentaBancaria());
        ip.setCultivo(buscadorCatalogo.buscaElemento(Cultivo.class, cultivo));
        ip.setCurpBeneficiario("--");
        DatosProductor dp = new DatosProductor();
        dp.setCurp("--");
        dp.setFechaNacimiento(Calendar.getInstance());
        dp.setNombre("--");
        dp.setNumeroDocumento("1");
        dp.setPais(buscadorCatalogo.buscaElemento(Pais.class, "MX"));
        dp.setPrimerApellido("--");
        dp.setRfc("rfc");
        dp.setSegundoApellido("--");
        dp.setSexo(buscadorCatalogo.buscaElemento(Sexo.class, "masculino"));
        dp.setTipoDocumento(buscadorCatalogo.buscaElemento(TipoDocumento.class, "ine"));
        dp.setTipoPersona(buscadorCatalogo.buscaElemento(TipoPersona.class, "fisica"));
        ip.setDatosProductor(dp);
        registroEntidad.guarda(ip.getDatosProductor());
        Domicilio d = new Domicilio();
        d.setCalle("a");
        d.setCodigoPostal("00000");
        d.setEstado(buscadorCatalogo.buscaElemento(Estado.class, "1"));
        d.setLocalidad("--");
        d.setMunicipio(buscadorCatalogo.buscaElemento(Municipio.class, "1001"));
        d.setNumeroInterior("1");
        ip.setDomicilio(d);
        registroEntidad.guarda(ip.getDomicilio());
        ip.setEncargado("--");
        ip.setEstado(buscadorCatalogo.buscaElemento(Estado.class, "1"));
        ip.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, "positiva"));
        ip.setEstatusPrevio(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, "desconocido"));
        ip.setEtiquetaTipoCultivo("etiqueta");
        ip.setForzada(true);
        ip.setNombreBeneficiario("a");
        ip.setNumeroContratos(0);
        ip.setNumeroEmpresas(0);
        ip.setNumeroPredios(0);
        ip.setNumeroTelefono("5555555");
        ip.setParentesco(buscadorCatalogo.buscaElemento(Parentesco.class, "hijo"));
        ip.setPredios(getPredios(ip, tipoCultivo));
        ip.setSucursal(buscadorCatalogo.buscaElemento(Sucursal.class, "0000"));
        ip.setTipoRegistro("fisica");
        ip.setUsuarioAsignado(buscadorCatalogo.buscaElemento(Usuario.class, "usuario@segalmex.gob.mx"));
        ip.setUsuarioRegistra(ip.getUsuarioAsignado());
        ip.setUuid(UUID.randomUUID().toString());
        ip.setRfcEmpresa("rfc");
        ip.setTipoProductor(buscadorCatalogo.buscaElemento(TipoProductor.class,
                TipoProductorEnum.PEQUENO.getClave()));
        registroEntidad.guarda(ip);
    }

    private List<PredioProductor> getPredios(InscripcionProductor ip, String tipoCultivo) {
        List<PredioProductor> predios = new ArrayList<>();
        PredioProductor p = new PredioProductor();
        p.setCantidadRiego(0);
        p.setCantidadSiembra(0);
        p.setCorregido(true);
        p.setEstado(buscadorCatalogo.buscaElemento(Estado.class, "1"));
        p.setFolio("");
        p.setInscripcionProductor(ip);
        p.setLatitud(BigDecimal.TEN);
        p.setLocalidad("--");
        p.setLongitud(BigDecimal.ONE);
        p.setMunicipio(buscadorCatalogo.buscaElemento(Municipio.class, "1001"));
        p.setOrden(0);
        p.setRegimenHidrico(buscadorCatalogo.buscaElemento(RegimenHidrico.class, "riego"));
        p.setRendimiento(BigDecimal.ZERO);
        p.setRendimientoMaximo(BigDecimal.ZERO);
        p.setRendimientoSolicitado(BigDecimal.ZERO);
        p.setSolicitado("no");
        p.setSuperficie(new BigDecimal(47.75));
        p.setTipoCultivo(buscadorCatalogo.buscaElemento(TipoCultivo.class, tipoCultivo));
        p.setTipoDocumentoPosesion(buscadorCatalogo.buscaElemento(TipoDocumento.class, "escritura-publica"));
        p.setTipoPosesion(buscadorCatalogo.buscaElemento(TipoPosesion.class, "propia"));
        p.setVolumen(BigDecimal.ONE);
        p.setVolumenCorregido(false);
        p.setVolumenSolicitado(BigDecimal.ZERO);
        predios.add(p);
        return predios;
    }
}
