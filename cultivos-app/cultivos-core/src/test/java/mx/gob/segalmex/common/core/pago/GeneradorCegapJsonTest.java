/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.pago;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.pluss.modelo.catalogos.Banco;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.Municipio;
import mx.gob.segalmex.pluss.modelo.factura.UsoFactura;
import mx.gob.segalmex.pluss.modelo.pago.RequerimientoPago;
import mx.gob.segalmex.pluss.modelo.pago.RequerimientoUsoFactura;
import mx.gob.segalmex.pluss.modelo.personas.Persona;
import mx.gob.segalmex.pluss.modelo.productor.CuentaBancaria;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import mx.gob.segalmex.pluss.modelo.productor.Productor;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;
import org.json.JSONObject;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author ismael
 */
@Slf4j
public class GeneradorCegapJsonTest {

    @Test
    @Ignore
    public void getJson() {
        RequerimientoPago requerimiento = new RequerimientoPago();
        requerimiento.setFechaCreacion(Calendar.getInstance());
        requerimiento.setLayout("PAGOS_MASIVOS");
        requerimiento.setClaveUnidadOperativa("29");
        requerimiento.setNumeroCuenta("01720137001906");
        requerimiento.setClaveArea("3958");
        requerimiento.setClaveUsuario("SMARQUEZ");
        requerimiento.setPrograma("CO_MAIZ");
        requerimiento.setClaveCentroAcopio("3958");
        requerimiento.setRequerimientos(new ArrayList<>());

        InscripcionProductor i = new InscripcionProductor();
        i.setFolio("000000");
        i.setCuentaBancaria(new CuentaBancaria());
        i.getCuentaBancaria().setBanco(new Banco());
        i.getCuentaBancaria().getBanco().setNombre("BBVA");
        i.getCuentaBancaria().setClabe("012000111222333444");
        i.setPredios(new ArrayList<>());
        Map<String, InscripcionProductor> map = new HashMap<>();
        map.put(i.getFolio(), i);

        PredioProductor pp = new PredioProductor();
        pp.setEstado(new Estado());
        pp.getEstado().setNombre("Sinaloa");
        pp.getEstado().setClave("25");
        pp.setMunicipio(new Municipio());
        pp.getMunicipio().setNombre("Ahome");
        pp.setLocalidad("LOS MOCHIS");

        i.getPredios().add(pp);

        UsoFactura uso = new UsoFactura();
        uso.setFolio("000000");
        uso.setToneladas(BigDecimal.TEN);
        uso.setFechaCreacion(Calendar.getInstance());
        uso.setProductorCiclo(new ProductorCiclo());
        uso.getProductorCiclo().setFolio("000000");
        uso.setProductor(new Productor());
        uso.setEstimuloTonelada(new BigDecimal("359.69"));
        uso.setEstimuloTotal(uso.getToneladas().multiply(uso.getEstimuloTonelada()));
        Persona p = new Persona();
        p.setNombreIcao("ISMAEL ISIDRO");
        p.setApellidosIcao("HERNANDEZ GONZALEZ");
        p.setPrimerApellido("HERNÁNDEZ");
        p.setSegundoApellido("GONZÁLEZ");
        uso.getProductor().setPersona(p);
        RequerimientoUsoFactura ufr = new RequerimientoUsoFactura();
        ufr.setRequerimiento(requerimiento);
        ufr.setUsoFactura(uso);
        requerimiento.getRequerimientos().add(ufr);

        GeneradorCegapJson generador = new GeneradorCegapJson();
        JSONObject json = generador.getJson(requerimiento, map);
        LOGGER.debug("Petición JSON: {}", json.toString(4));
    }
}
