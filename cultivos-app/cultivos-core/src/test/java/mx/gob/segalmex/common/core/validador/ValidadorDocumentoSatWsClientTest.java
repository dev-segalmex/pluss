/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.validador;

import java.math.BigDecimal;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.pluss.modelo.productor.PredioDocumento;
import org.junit.Test;
import org.springframework.ws.client.core.WebServiceTemplate;

/**
 *
 * @author oscar
 */
@Slf4j
public class ValidadorDocumentoSatWsClientTest {

    @Test
    public void validaTest() {
        ValidadorDocumentoSatWsClient client = new ValidadorDocumentoSatWsClient();
        client.setWsTemplate(new WebServiceTemplate());
        client.setUrl("https://consultaqr.facturaelectronica.sat.gob.mx/ConsultaCFDIService.svc");

        PredioDocumento pd = new PredioDocumento();
        pd.setRfcEmisor("IIZ030710FP7");
        pd.setRfcReceptor("UISO8901061BA");
        pd.setTotal(new BigDecimal("2625.000"));
        pd.setUuidTimbreFiscalDigital("3A6A9AF1-0109-4FA2-A0E2-2CDC9A052449");
        boolean valido = client.valida(pd);
        LOGGER.debug("El predio documento es válido: {}", valido);
    }
}
