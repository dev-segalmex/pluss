package mx.gob.segalmex.granos.core.contrato.pdf;

import java.io.IOException;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.granos.core.contrato.ProcesadorContratoProductorXls;
import mx.gob.segalmex.pluss.modelo.inscripcion.ContratoProductor;
import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author oscar
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:testCultivosApplicationContext.xml",
    "classpath:mysqlPersistenceApplicationContext.xml"
})
@Slf4j
@Ignore
public class ProcesadorContratoProductorXlsTest {

    @Autowired
    private ProcesadorContratoProductorXls procesadorContratoProductorXls;

    @Test
    public void procesa() throws IOException {
        byte[] datosPrueba = IOUtils.toByteArray(ProcesadorContratoProductorXlsTest.class.
                getResourceAsStream("/contrato-productor-xlsx/contrato-productor-1-reg.xlsx"));
        List<ContratoProductor> elementos = procesadorContratoProductorXls.procesa(datosPrueba, null, null);
        LOGGER.info("Elementos: {} ", elementos.size());
        Assert.assertEquals("Debe tener 1 registro.", 1, elementos.size());
    }

    @Test
    public void procesaVolumenIarMayor() throws IOException {
        byte[] datosPrueba = IOUtils.toByteArray(ProcesadorContratoProductorXlsTest.class.
                getResourceAsStream("/contrato-productor-xlsx/contrato-productor-1-error-volmenes.xlsx"));
        List<ContratoProductor> elementos = procesadorContratoProductorXls.procesa(datosPrueba, null, null);
        LOGGER.info("Elementos: {} ", elementos.size());
        Assert.assertEquals("Debe tener 1 registro.", 1, elementos.size());
        Assert.assertEquals("Debe ser inválido.", false, elementos.get(0).isCorrecto());
    }

    @Test
    public void procesa1Ok2Err() throws IOException {
        byte[] datosPrueba = IOUtils.toByteArray(ProcesadorContratoProductorXlsTest.class.
                getResourceAsStream("/contrato-productor-xlsx/contrato-productor-1-reg-2-err.xlsx"));
        List<ContratoProductor> elementos = procesadorContratoProductorXls.procesa(datosPrueba, null, null);
        LOGGER.info("Elementos: {} ", elementos.size());
        Assert.assertEquals("Debe tener 3 registro.", 3, elementos.size());
        Assert.assertEquals("Debe ser inválido.", false, elementos.get(1).isCorrecto());
        Assert.assertEquals("Debe ser inválido.", false, elementos.get(2).isCorrecto());
    }

    @Test(expected = SegalmexRuntimeException.class)
    public void procesaErrorEncabezado() throws IOException {
        byte[] datosPrueba = IOUtils.toByteArray(ProcesadorContratoProductorXlsTest.class.
                getResourceAsStream("/contrato-productor-xlsx/error-encabezado.xlsx"));
        procesadorContratoProductorXls.procesa(datosPrueba, null, null);
        Assert.fail("Debió lanzar excepción SegalmexRuntimeException.");
    }

    @Test(expected = SegalmexRuntimeException.class)
    public void procesaFaltaEncabezado() throws IOException {
        byte[] datosPrueba = IOUtils.toByteArray(ProcesadorContratoProductorXlsTest.class.
                getResourceAsStream("/contrato-productor-xlsx/error-falta-encabezado.xlsx"));
        procesadorContratoProductorXls.procesa(datosPrueba, null, null);
        Assert.fail("Debió lanzar excepción SegalmexRuntimeException.");
    }

    @Test(expected = SegalmexRuntimeException.class)
    public void procesaMenosEncabezado() throws IOException {
        byte[] datosPrueba = IOUtils.toByteArray(ProcesadorContratoProductorXlsTest.class.
                getResourceAsStream("/contrato-productor-xlsx/error-menos-encabezados.xlsx"));
        procesadorContratoProductorXls.procesa(datosPrueba, null, null);
        Assert.fail("Debió lanzar excepción SegalmexRuntimeException.");
    }

    @Test
    public void procesaSoloEncabezado() throws IOException {
        byte[] datosPrueba = IOUtils.toByteArray(ProcesadorContratoProductorXlsTest.class.
                getResourceAsStream("/contrato-productor-xlsx/solo-encabezado.xlsx"));
        List<ContratoProductor> elementos = procesadorContratoProductorXls.procesa(datosPrueba, null, null);
        LOGGER.info("Elementos: {} ", elementos.size());
        Assert.assertEquals("Debe tener 0 registro.", 0, elementos.size());
    }

    @Test(expected = SegalmexRuntimeException.class)
    public void procesaEncabezadoEspacioEnMedio() throws IOException {
        byte[] datosPrueba = IOUtils.toByteArray(ProcesadorContratoProductorXlsTest.class.
                getResourceAsStream("/contrato-productor-xlsx/error-espacio-encabezados.xlsx"));
        procesadorContratoProductorXls.procesa(datosPrueba, null, null);
        Assert.fail("Debió lanzar excepción SegalmexRuntimeException.");
    }

    @Test(expected = SegalmexRuntimeException.class)
    public void procesaArchivoVacio() throws IOException {
        byte[] datosPrueba = IOUtils.toByteArray(ProcesadorContratoProductorXlsTest.class.
                getResourceAsStream("/contrato-productor-xlsx/error-vacio.xlsx"));
        procesadorContratoProductorXls.procesa(datosPrueba, null, null);
        Assert.fail("Debió lanzar excepción SegalmexRuntimeException.");
    }

    @Test
    public void procesa3RegistrosSolo2Obtenidos() throws IOException {
        //Archivo con 3 registros pero el de enmedio no trae nada, por lo que debe obtener solo 2
        byte[] datosPrueba = IOUtils.toByteArray(ProcesadorContratoProductorXlsTest.class.
                getResourceAsStream("/contrato-productor-xlsx/contrato-productor-2de3-obtenidos.xlsx"));
        List<ContratoProductor> elementos = procesadorContratoProductorXls.procesa(datosPrueba, null, null);
        LOGGER.info("Elementos: {} ", elementos.size());
        Assert.assertEquals("Debe tener 2 registro.", 2, elementos.size());
    }

    @Test
    public void procesa3RegistrosSolo2ObtenidosB() throws IOException {
        //Archivo con 3 registros pero el de enmedio no trae nada, por lo que debe obtener solo 2
        byte[] datosPrueba = IOUtils.toByteArray(ProcesadorContratoProductorXlsTest.class.
                getResourceAsStream("/contrato-productor-xlsx/contrato-productor-2de3-obtenidosb.xlsx"));
        List<ContratoProductor> elementos = procesadorContratoProductorXls.procesa(datosPrueba, null, null);
        LOGGER.info("Elementos: {} ", elementos.size());
        Assert.assertEquals("Debe tener 2 registro.", 2, elementos.size());
    }

    @Test
    public void procesa11Registros() throws IOException {
        byte[] datosPrueba = IOUtils.toByteArray(ProcesadorContratoProductorXlsTest.class.
                getResourceAsStream("/contrato-productor-xlsx/contrato-productor-11-reg.xlsx"));
        List<ContratoProductor> elementos = procesadorContratoProductorXls.procesa(datosPrueba, null, null);
        LOGGER.info("Elementos: {} ", elementos.size());
        Assert.assertEquals("Debe tener 11 registro.", 11, elementos.size());
    }

    @Test
    public void procesa2Ok1ErrApellidoPa() throws IOException {
        byte[] datosPrueba = IOUtils.toByteArray(ProcesadorContratoProductorXlsTest.class.
                getResourceAsStream("/contrato-productor-xlsx/contrato-productor-2-reg-1-erro-apellidopa.xlsx"));
        List<ContratoProductor> elementos = procesadorContratoProductorXls.procesa(datosPrueba, null, null);
        LOGGER.info("Elementos: {} ", elementos.size());
        Assert.assertEquals("Debe tener 3 registro.", 3, elementos.size());
        Assert.assertEquals("Debe ser inválido.", true, elementos.get(0).isCorrecto());
        Assert.assertEquals("Debe ser inválido.", false, elementos.get(1).isCorrecto());
        Assert.assertEquals("Debe ser inválido.", true, elementos.get(2).isCorrecto());
    }

    @Test
    public void procesa2Ok1SinApellidoMat() throws IOException {
        byte[] datosPrueba = IOUtils.toByteArray(ProcesadorContratoProductorXlsTest.class.
                getResourceAsStream("/contrato-productor-xlsx/contrato-productor-2-reg-1-sin-apellidoma.xlsx"));
        List<ContratoProductor> elementos = procesadorContratoProductorXls.procesa(datosPrueba, null, null);
        LOGGER.info("Elementos: {} ", elementos.size());
        Assert.assertEquals("Debe tener 3 registro.", 3, elementos.size());
        Assert.assertEquals("Debe ser inválido.", true, elementos.get(0).isCorrecto());
        Assert.assertEquals("Debe ser inválido.", false, elementos.get(1).isCorrecto());
        Assert.assertEquals("Debe ser inválido.", true, elementos.get(2).isCorrecto());
    }

    @Test
    public void procesa3con2ErroresEnTne() throws IOException {
        byte[] datosPrueba = IOUtils.toByteArray(ProcesadorContratoProductorXlsTest.class.
                getResourceAsStream("/contrato-productor-xlsx/contrato-productor-3-reg-2err-tne.xlsx"));
        List<ContratoProductor> elementos = procesadorContratoProductorXls.procesa(datosPrueba, null, null);
        LOGGER.info("Elementos: {} ", elementos.size());
        Assert.assertEquals("Debe tener 3 registro.", 3, elementos.size());
        Assert.assertEquals("Debe ser inválido.", false, elementos.get(0).isCorrecto());
        Assert.assertEquals("Debe ser inválido.", true, elementos.get(1).isCorrecto());
        Assert.assertEquals("Debe ser inválido.", false, elementos.get(2).isCorrecto());
    }

}
