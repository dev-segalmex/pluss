package mx.gob.segalmex.pluss.core.productor;

import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.granos.core.productor.EstadoProductorHelper;
import mx.gob.segalmex.pluss.modelo.productor.PredioBasico;
import org.junit.Assert;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class EstadoProductorHelperTest {

    @Test
    public void testGetEstado() {
        Estado e1 = new Estado();
        e1.setClave("1");
        e1.setNombre("Estado 1");

        Estado e2 = new Estado();
        e2.setClave("2");
        e2.setNombre("Estado 2");

        List<PredioBasico> predios = new ArrayList<>();
        PredioBasico p1 = new PredioBasico();
        p1.setEstado(e1);
        p1.setSuperficie(BigDecimal.TEN);
        predios.add(p1);

        PredioBasico p2 = new PredioBasico();
        p2.setEstado(e2);
        p2.setSuperficie(BigDecimal.ONE);
        predios.add(p2);

        Assert.assertEquals("El estado esperado es incorrecto", e1, EstadoProductorHelper.getEstado(predios));

        PredioBasico p3 = new PredioBasico();
        p3.setEstado(e2);
        p3.setSuperficie(BigDecimal.TEN);
        predios.add(p3);
        Assert.assertEquals("El estado esperado es incorrecto", e2, EstadoProductorHelper.getEstado(predios));
    }
}
