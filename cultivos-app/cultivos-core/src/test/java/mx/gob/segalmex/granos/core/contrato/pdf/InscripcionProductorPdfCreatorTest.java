/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.core.contrato.pdf;

import com.lowagie.text.DocumentException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import mx.gob.segalmex.pluss.modelo.catalogos.Banco;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.Municipio;
import mx.gob.segalmex.pluss.modelo.catalogos.Parentesco;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoDocumento;
import mx.gob.segalmex.pluss.modelo.personas.Sexo;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersona;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersonaEnum;
import mx.gob.segalmex.granos.core.productor.pdf.InscripcionProductorPdfCreator;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.Empresa;
import mx.gob.segalmex.pluss.modelo.catalogos.GrupoIndigena;
import mx.gob.segalmex.pluss.modelo.catalogos.NivelEstudio;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoProductor;
import mx.gob.segalmex.pluss.modelo.productor.ContratoFirmado;
import mx.gob.segalmex.pluss.modelo.productor.CuentaBancaria;
import mx.gob.segalmex.pluss.modelo.productor.DatosProductor;
import mx.gob.segalmex.pluss.modelo.productor.Domicilio;
import mx.gob.segalmex.pluss.modelo.productor.EmpresaAsociada;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import mx.gob.segalmex.pluss.modelo.productor.RegimenHidrico;
import mx.gob.segalmex.pluss.modelo.productor.TipoPosesion;
import org.apache.commons.io.IOUtils;
import org.junit.Test;

/**
 *
 * @author ismael
 */
public class InscripcionProductorPdfCreatorTest {

    @Test
    public void testCreate() throws DocumentException, IOException {
        InscripcionProductor inscripcion = new InscripcionProductor();
        inscripcion.setFolio("");
        inscripcion.setPredios(new ArrayList<>());
        inscripcion.setCultivo(new Cultivo());
        inscripcion.getCultivo().setNombre("Maíz");
        inscripcion.getCultivo().setClave("maiz-comercial");
        inscripcion.setCiclo(new CicloAgricola());
        inscripcion.getCiclo().setNombre("O.I. 2021-2022");
        inscripcion.getCiclo().setClave("oi-2022");
        inscripcion.setEtiquetaTipoCultivo("");
        inscripcion.setTipoRegistro("fisica");
        inscripcion.setGrupoIndigena(new GrupoIndigena());
        inscripcion.getGrupoIndigena().setNombre("--");
        inscripcion.setTipoProductor(new TipoProductor());
        inscripcion.getTipoProductor().setNombre("Pequeño");
        inscripcion.setNivelEstudio(new NivelEstudio());
        inscripcion.getNivelEstudio().setNombre("Primaria");

        DatosProductor dp = new DatosProductor();
        dp.setNombre("");
        dp.setPrimerApellido("");
        dp.setSegundoApellido("");
        dp.setSexo(new Sexo());
        dp.getSexo().setNombre("");
        dp.setTipoPersona(new TipoPersona());
        dp.getTipoPersona().setClave(TipoPersonaEnum.FISICA.getClave());
        dp.setTipoDocumento(new TipoDocumento());
        dp.getTipoDocumento().setNombre("");
        inscripcion.setDatosProductor(dp);

        Domicilio d = new Domicilio();
        d.setCalle("");
        d.setNumeroExterior("");
        d.setNumeroInterior("");
        d.setLocalidad("");
        d.setMunicipio(new Municipio());
        d.getMunicipio().setNombre("");
        d.setEstado(new Estado());
        d.getEstado().setNombre("");
        inscripcion.setDomicilio(d);

        inscripcion.setContratos(new ArrayList<>());
        ContratoFirmado c = new ContratoFirmado();
        inscripcion.getContratos().add(c);

        CuentaBancaria cb = new CuentaBancaria();
        cb.setBanco(new Banco());
        cb.getBanco().setNombre("");
        cb.setClabe("");
        cb.setNumero("");
        inscripcion.setCuentaBancaria(cb);

        inscripcion.setNombreBeneficiario("");
        inscripcion.setParentesco(new Parentesco());
        inscripcion.getParentesco().setNombre("");

        Empresa e = new Empresa();
        e.setRfc("");

        Sucursal s = new Sucursal();
        s.setNombre("");
        s.setEmpresa(e);
        s.setEstado(new Estado());
        s.getEstado().setNombre("");
        s.setMunicipio(new Municipio());
        s.getMunicipio().setNombre("");
        s.setLocalidad("");
        inscripcion.setSucursal(s);
        inscripcion.setEncargado("");

        inscripcion.setBodega(s);

        PredioProductor p = new PredioProductor();
        p.setFolio("");
        p.setTipoCultivo(new TipoCultivo());
        p.getTipoCultivo().setNombre("");
        p.setTipoPosesion(new TipoPosesion());
        p.getTipoPosesion().setNombre("");
        p.setTipoDocumentoPosesion(dp.getTipoDocumento());
        p.setRegimenHidrico(new RegimenHidrico());
        p.getRegimenHidrico().setNombre("");
        p.setMunicipio(d.getMunicipio());
        p.setEstado(d.getEstado());
        p.setSolicitado("solicitado");

        inscripcion.setPredios(new ArrayList<>());
        inscripcion.getPredios().add(p);

        EmpresaAsociada ea = new EmpresaAsociada();
        e.setRfc("AAA000000AAA");
        e.setNombre("Segalmex");

        inscripcion.setEmpresas(new ArrayList<>());
        inscripcion.getEmpresas().add(ea);

        InscripcionProductorPdfCreator creator = new InscripcionProductorPdfCreator("prod");
        byte[] pdf = creator.create(inscripcion);

        FileOutputStream fos = new FileOutputStream("target/inscripcion-productor-maiz.pdf");
        IOUtils.write(pdf, fos);
        fos.close();
    }

    @Test
    public void testCreateArroz() throws DocumentException, IOException {
        InscripcionProductor inscripcion = new InscripcionProductor();
        inscripcion.setFolio("");
        inscripcion.setPredios(new ArrayList<>());
        inscripcion.setCultivo(new Cultivo());
        inscripcion.getCultivo().setNombre("Arroz");
        inscripcion.getCultivo().setClave("arroz");
        inscripcion.setCiclo(new CicloAgricola());
        inscripcion.getCiclo().setNombre("O.I. 2021-2022");
        inscripcion.getCiclo().setClave("oi-2022");
        inscripcion.setEtiquetaTipoCultivo("");
        inscripcion.setTipoRegistro("fisica");
        inscripcion.setGrupoIndigena(new GrupoIndigena());
        inscripcion.getGrupoIndigena().setNombre("--");
        inscripcion.setTipoProductor(new TipoProductor());
        inscripcion.getTipoProductor().setNombre("Pequeño");
        inscripcion.setNivelEstudio(new NivelEstudio());
        inscripcion.getNivelEstudio().setNombre("Primaria");

        DatosProductor dp = new DatosProductor();
        dp.setNombre("");
        dp.setPrimerApellido("");
        dp.setSegundoApellido("");
        dp.setSexo(new Sexo());
        dp.getSexo().setNombre("");
        dp.setTipoPersona(new TipoPersona());
        dp.getTipoPersona().setClave(TipoPersonaEnum.FISICA.getClave());
        dp.setTipoDocumento(new TipoDocumento());
        dp.getTipoDocumento().setNombre("");
        inscripcion.setDatosProductor(dp);

        Domicilio d = new Domicilio();
        d.setCalle("");
        d.setNumeroExterior("");
        d.setNumeroInterior("");
        d.setLocalidad("");
        d.setMunicipio(new Municipio());
        d.getMunicipio().setNombre("");
        d.setEstado(new Estado());
        d.getEstado().setNombre("");
        inscripcion.setDomicilio(d);

        inscripcion.setContratos(new ArrayList<>());
        ContratoFirmado c = new ContratoFirmado();
        inscripcion.getContratos().add(c);

        CuentaBancaria cb = new CuentaBancaria();
        cb.setBanco(new Banco());
        cb.getBanco().setNombre("");
        cb.setClabe("");
        cb.setNumero("");
        inscripcion.setCuentaBancaria(cb);

        inscripcion.setNombreBeneficiario("");
        inscripcion.setParentesco(new Parentesco());
        inscripcion.getParentesco().setNombre("");

        Empresa e = new Empresa();
        e.setRfc("");

        Sucursal s = new Sucursal();
        s.setNombre("");
        s.setEmpresa(e);
        s.setEstado(new Estado());
        s.getEstado().setNombre("");
        s.setMunicipio(new Municipio());
        s.getMunicipio().setNombre("");
        s.setLocalidad("");
        inscripcion.setSucursal(s);
        inscripcion.setEncargado("");

        inscripcion.setBodega(s);

        PredioProductor p = new PredioProductor();
        p.setFolio("");
        p.setTipoCultivo(new TipoCultivo());
        p.getTipoCultivo().setNombre("");
        p.setTipoPosesion(new TipoPosesion());
        p.getTipoPosesion().setNombre("");
        p.setTipoDocumentoPosesion(dp.getTipoDocumento());
        p.setRegimenHidrico(new RegimenHidrico());
        p.getRegimenHidrico().setNombre("");
        p.setMunicipio(d.getMunicipio());
        p.setEstado(d.getEstado());
        p.setSolicitado("solicitado");
        p.setVolumen(BigDecimal.ONE);
        p.setVolumenSolicitado(BigDecimal.ONE);

        inscripcion.setPredios(new ArrayList<>());
        inscripcion.getPredios().add(p);

        EmpresaAsociada ea = new EmpresaAsociada();
        e.setRfc("AAA000000AAA");
        e.setNombre("Segalmex");

        inscripcion.setEmpresas(new ArrayList<>());
        inscripcion.getEmpresas().add(ea);

        InscripcionProductorPdfCreator creator = new InscripcionProductorPdfCreator("prod");
        byte[] pdf = creator.create(inscripcion);

        FileOutputStream fos = new FileOutputStream("target/inscripcion-productor-arroz.pdf");
        IOUtils.write(pdf, fos);
        fos.close();
    }

    @Test
    public void testCreateTrigo() throws DocumentException, IOException {
        InscripcionProductor inscripcion = new InscripcionProductor();
        inscripcion.setFolio("");
        inscripcion.setPredios(new ArrayList<>());
        inscripcion.setCultivo(new Cultivo());
        inscripcion.getCultivo().setNombre("Trigo");
        inscripcion.getCultivo().setClave("trigo");
        inscripcion.setCiclo(new CicloAgricola());
        inscripcion.getCiclo().setNombre("O.I. 2021-2022");
        inscripcion.getCiclo().setClave("oi-2022");
        inscripcion.setEtiquetaTipoCultivo("");
        inscripcion.setTipoRegistro("fisica");
        inscripcion.setGrupoIndigena(new GrupoIndigena());
        inscripcion.getGrupoIndigena().setNombre("--");
        inscripcion.setTipoProductor(new TipoProductor());
        inscripcion.getTipoProductor().setNombre("Pequeño");
        inscripcion.setNivelEstudio(new NivelEstudio());
        inscripcion.getNivelEstudio().setNombre("Primaria");

        DatosProductor dp = new DatosProductor();
        dp.setNombre("");
        dp.setPrimerApellido("");
        dp.setSegundoApellido("");
        dp.setSexo(new Sexo());
        dp.getSexo().setNombre("");
        dp.setTipoPersona(new TipoPersona());
        dp.getTipoPersona().setClave(TipoPersonaEnum.FISICA.getClave());
        dp.setTipoDocumento(new TipoDocumento());
        dp.getTipoDocumento().setNombre("");
        inscripcion.setDatosProductor(dp);

        Domicilio d = new Domicilio();
        d.setCalle("");
        d.setNumeroExterior("");
        d.setNumeroInterior("");
        d.setLocalidad("");
        d.setMunicipio(new Municipio());
        d.getMunicipio().setNombre("");
        d.setEstado(new Estado());
        d.getEstado().setNombre("");
        inscripcion.setDomicilio(d);

        inscripcion.setContratos(new ArrayList<>());
        ContratoFirmado c = new ContratoFirmado();
        inscripcion.getContratos().add(c);

        CuentaBancaria cb = new CuentaBancaria();
        cb.setBanco(new Banco());
        cb.getBanco().setNombre("");
        cb.setClabe("");
        cb.setNumero("");
        inscripcion.setCuentaBancaria(cb);

        inscripcion.setNombreBeneficiario("");
        inscripcion.setParentesco(new Parentesco());
        inscripcion.getParentesco().setNombre("");

        Empresa e = new Empresa();
        e.setRfc("");

        Sucursal s = new Sucursal();
        s.setNombre("");
        s.setEmpresa(e);
        s.setEstado(new Estado());
        s.getEstado().setNombre("");
        s.setMunicipio(new Municipio());
        s.getMunicipio().setNombre("");
        s.setLocalidad("");
        inscripcion.setSucursal(s);
        inscripcion.setEncargado("");

        inscripcion.setBodega(s);

        PredioProductor p = new PredioProductor();
        p.setFolio("");
        p.setTipoCultivo(new TipoCultivo());
        p.getTipoCultivo().setNombre("");
        p.setTipoPosesion(new TipoPosesion());
        p.getTipoPosesion().setNombre("");
        p.setTipoDocumentoPosesion(dp.getTipoDocumento());
        p.setRegimenHidrico(new RegimenHidrico());
        p.getRegimenHidrico().setNombre("");
        p.setMunicipio(d.getMunicipio());
        p.setEstado(d.getEstado());
        p.setSolicitado("solicitado");
        p.setVolumen(BigDecimal.ONE);
        p.setVolumenSolicitado(BigDecimal.ONE);

        inscripcion.setPredios(new ArrayList<>());
        inscripcion.getPredios().add(p);

        EmpresaAsociada ea = new EmpresaAsociada();
        e.setRfc("AAA000000AAA");
        e.setNombre("Segalmex");

        inscripcion.setEmpresas(new ArrayList<>());
        inscripcion.getEmpresas().add(ea);

        InscripcionProductorPdfCreator creator = new InscripcionProductorPdfCreator("prod");
        byte[] pdf = creator.create(inscripcion);

        FileOutputStream fos = new FileOutputStream("target/inscripcion-productor-trigo.pdf");
        IOUtils.write(pdf, fos);
        fos.close();
    }
}
