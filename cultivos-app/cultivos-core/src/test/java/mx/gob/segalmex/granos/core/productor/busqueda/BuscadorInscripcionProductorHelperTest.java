package mx.gob.segalmex.granos.core.productor.busqueda;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionResumen;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 *
 * @author oscar
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {
    "classpath:testCultivosApplicationContext.xml",
    "classpath:mysqlPersistenceApplicationContext.xml"
})
@Slf4j
public class BuscadorInscripcionProductorHelperTest {

    @Autowired
    private BuscadorInscripcionProductorHelper buscador;

    @Test
    public void buscaResumen() {
        List<InscripcionResumen> resumen = buscador.buscaResumen(new ParametrosInscripcionProductor());
        LOGGER.info("Total {}", resumen.size());
    }

}
