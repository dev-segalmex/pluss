/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.factura;

import mx.gob.segalmex.common.core.factura.modelo.CfdiComprobante;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author ismael
 */
public class CfdiParserTest {

    @Test
    public void parse() throws Exception {
        CfdiParser parser = new CfdiParser();
        CfdiComprobante comprobante = parser.parse(CfdiParser.class.getResourceAsStream("/cfdi-001.xml"));
        Assert.assertNotNull(comprobante);
    }
}
