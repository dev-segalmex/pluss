/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.maiz.registro.ventanilla.basico');
    var data = {
        registro: {}
    };
    var handlers = {};
    var utils = {};

    data.campoInput = ['#rfc-ventanilla'];
    data.camposSelect = ['#ciclo-ventanilla', '#cultivo-ventanilla'];

    $.segalmex.maiz.registro.ventanilla.basico.init = function () {
        utils.inicializaCatalogos();
        utils.inicializaValidaciones();
        $('#consultar-button').click(handlers.consulta);
        $('#regresar-button').click(handlers.regresa);
        $('#guardar-button').click(handlers.guardaRegistroBasicoVentanilla);
    };

    utils.inicializaCatalogos = function () {
        $.ajax({
            type: 'GET',
            url: '/cultivos/resources/paginas/empresa/registro/basico/',
            dataType: 'json'
        }).done(function (response) {
            $('#ciclo-ventanilla').actualizaCombo(response.ciclos);
            $('#cultivo-ventanilla').actualizaCombo(response.cultivos);
        }).fail(function () {
            alert('Error: Al descargar los recursos de la página.');
        });
    };

    utils.inicializaValidaciones = function () {
        $(".valid-field").configura();
        $('#rfc-ventanilla').configura({
            type: 'rfc'
        });
        $('.valid-field').validacion();
    };

    utils.limpiar = function () {
        $('select.valid-field').val('0').limpiaErrores();
        $('input.valid-field').val('').limpiaErrores();
        data.registro = {};
    };

    utils.construyeUrl = function () {
        var coded = $.base64.btoa(data.registro.uuid + '|' + data.registro.rfc);
        $('#url').html(data.registro.url + '/cultivos/publico/registro-ventanilla.html?token=' + coded);
    };

    handlers.consulta = function (e) {
        e.preventDefault();
        $(e.target).prop('disabled', true);
        var errores = [];
        $(data.campoInput.join(',')).valida(errores, false);
        $(data.camposSelect.join(',')).valida(errores, false);
        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            $(e.target).prop('disabled', false);
            return;
        }

        data.registro = {
            ciclo: $('#ciclo-ventanilla').val(),
            cultivo: $('#cultivo-ventanilla').val()
        };
        $.ajax({
            url: '/cultivos/resources/empresas/' + $('#rfc-ventanilla').val() + '/registro/',
            type: 'GET',
            data: data.registro,
            dataType: 'json'
        }).done(function (response) {
            data.registro = response;
            utils.construyeUrl();
            $('#div-registro').hide();
            $('#resultado').show();
            $(e.target).prop('disabled', false);
        }).fail(function () {
            alert('No se encontró ningún resultado.');
            $(e.target).prop('disabled', false);
        });
    };

    handlers.regresa = function (e) {
        $('#resultado').hide();
        $('#div-registro').show();
    };

    handlers.guardaRegistroBasicoVentanilla = function (e) {
        e.preventDefault();
        $(e.target).prop('disabled', true);
        var errores = [];
        $(data.campoInput.join(',')).valida(errores, false);
        $(data.camposSelect.join(',')).valida(errores, false);
        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            $(e.target).prop('disabled', false);
            return;
        }
        data.registro = {
            rfc: $('#rfc-ventanilla').val(),
            ciclo: {id: $('#ciclo-ventanilla').val()},
            cultivo: {id: $('#cultivo-ventanilla').val()}
        };
        $.ajax({
            url: '/cultivos/resources/empresas/' + data.registro.rfc + '/registro/',
            type: 'POST',
            data: JSON.stringify(data.registro),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('Registro de ventanilla básico ha sido exitoso.');
            $(e.target).prop('disabled', false);
            handlers.consulta(e);
            utils.limpiar();
        }).fail(function () {
            alert('Error: no se pudo realizar el registro básico de ventanilla.');
            $(e.target).prop('disabled', false);
        });
    };
})(jQuery);