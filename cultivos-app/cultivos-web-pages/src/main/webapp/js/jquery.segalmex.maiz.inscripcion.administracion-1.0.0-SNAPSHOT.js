/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($) {
    $.segalmex.namespace('segalmex.maiz.inscripcion.administracion');
    var data = {
        tipoInscripcion: 'contratos',
        urlEstatus: 'cancelacion',
        tipoCancelacion: ''
    };
    var handlers = {};
    var utils = {};

    $.segalmex.maiz.inscripcion.administracion.init = function () {
        utils.cargaCatalogos();
        utils.inicializaValidaciones();

        $('#menu-bandejas a.nav-link').click(handlers.cambiaBandeja);
        $('#datos-busqueda-form').keypress(function (event) {
            if (event.which === 13) {
                event.preventDefault();
                $('#button-buscar').click();
            }
        });

        $('#button-buscar').click(handlers.busca);
        $('#button-limpiar').click(handlers.limpia);
        $('#revalidar-inscripcion-button').click(handlers.revalidaInscripcion);
        $('#reasignar-inscripcion-button').click(handlers.reasignaInscripcion);
        $('#si-reasignar-button').click(handlers.confirmaReasignacion);
        $('#no-reasignar-button').click(handlers.desisteReasignacion);
        $('#cancelar-inscripcion-button').click(handlers.cancelaInscripcion);
        $('#cancelar-negativo-inscripcion-button').click(handlers.cancelaInscripcion);
        $('#si-cancelar-button').click(handlers.confirmaCancelacion);
        $('#no-cancelar-button').click(handlers.desisteCancelacion);
        $('#regresar-button').click(handlers.regresa);
        $('#edicion-inscripcion-button').click(handlers.permiteEdicion);
        $('#cierra-edicion-button').click(handlers.cierraEdicion);
        $('#guarda-edicion-button').click(handlers.confirmaEdicion);
        $('#button-pendiente').click(utils.marcaPendiente);
        $('#si-pendiente-button').click(handlers.confirmaPendiente);
        $('#no-pendiente-button').click(handlers.desistePendiente);
        $('#button-cancelar-deudor').click(handlers.cancelaProductorDeudor);
        setTimeout(function () {
            $('#folio').focus();
        }, 1000);
    };

    handlers.cambiaBandeja = function (e) {
        e.preventDefault();
        var id = e.target.id.substring('muestra-'.length);
        if (data.tipoInscripcion === id) {
            return;
        }
        data.tipoInscripcion = id;

        $('#menu-bandejas a.nav-link').removeClass('active');
        utils.regresa();
        $('#folio').val('');

        $(e.target).addClass('active');
        $('#inscripcion-botones button.btn').prop('disabled', false);
        $('#edicion-inscripcion-button').hide();
    };

    handlers.busca = function (e) {
        e.preventDefault();
        var errores = [];
        $('.valid-field').valida(errores, true);
        if (errores.length > 0) {
            return;
        }

        $('#resultados-busqueda').hide();
        var datos = {
            folio: $('#folio').val()
        };

        $.ajax({
            url: '/cultivos/resources/' + data.tipoInscripcion + '/inscripcion/administracion/',
            type: 'GET',
            data: datos,
            dataType: 'json'
        }).done(function (response) {
            if (response.length === 0) {
                alert('Error: No existe el folio especificado.');
                return;
            }
            utils.muestraDetalle(response[0].uuid);
        }).fail(function () {
            alert('Error: No fue posible realizar la consulta.');
        })
    };

    handlers.limpia = function (e) {
        e.preventDefault();
        $('#folio').limpiaErrores().val('');
    }

    handlers.cancelaInscripcion = function (e) {
        e.preventDefault();
        data.tipoCancelacion = e.target.id ? e.target.id : e.target.parentElement.id;
        if (utils.validaEstatusCancelacion()) {
            alert('Error: El registro ya se encuentra cancelado.');
            return;
        }
        $('#comentario-cancelacion').val('');
        $('#confirmacion-modal').modal('show');
    };

    handlers.cancelaProductorDeudor = function (e) {
        e.preventDefault();
        data.tipoCancelacion = e.target.id ? e.target.id : e.target.parentElement.id;
        if (utils.validaEstatusCancelacion()) {
            alert('Error: El registro ya se encuentra cancelado.');
            return;
        }
        $('#comentario-cancelacion').val('');
        $('#confirmacion-modal').modal('show');
    };

    handlers.confirmaCancelacion = function (e) {
        e.preventDefault();
        $('#confirmacion-modal button').prop('disabled', true);
        var errores = [];
        $('#confirmacion-modal .valid-field').valida(errores, true);
        if (errores.length > 0) {
            $('#confirmacion-modal button').prop('disabled', false);
            return;
        }
        var url = '';
        switch (data.tipoCancelacion) {
            case 'cancelar-inscripcion-button':
                url = '/cultivos/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + data.actual.uuid + '/cancelacion/';
                break;
            case 'cancelar-negativo-inscripcion-button':
                url = '/cultivos/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + data.actual.uuid + '/negativo/';
                break;
            case 'button-cancelar-deudor':
                url = '/cultivos/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + data.actual.uuid + '/negativo/deudor/';
                break;
        }
        $.ajax({
            url: url,
            data: JSON.stringify({comentarioEstatus: $('#comentario-cancelacion').val()}),
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('El registro ha sido cancelado.');
            $('#confirmacion-modal button').prop('disabled', false);
            $('#confirmacion-modal').modal('hide');
            utils.muestraDetalle(response.uuid);
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Error: No fue posible cancelar el registro.');
            }
            $('#confirmacion-modal button').prop('disabled', false);
        });
    };

    handlers.desisteCancelacion = function (e) {
        $('#comentario-cancelacion').val('');
        $('#confirmacion-modal').modal('hide');
    };

    handlers.revalidaInscripcion = function (e) {
        e.preventDefault();
        if (!confirm('¿Está seguro de enviar a revalidación el registro?')) {
            return;
        }

        $('#inscripcion-botones button.btn').prop('disabled', true);
        $.ajax({
            url: '/cultivos/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + data.actual.uuid + '/revalidacion/',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('El registro ha sido enviado a revalidación.');
            $('#inscripcion-botones button.btn').prop('disabled', false);
            utils.muestraDetalle(response.uuid);
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Error: No fue posible enviar a revalidación. Verifique que el estatus sea positivo, cancelado o en corrección.');
            }
            $('#inscripcion-botones button.btn').prop('disabled', false);
        });
    };

    handlers.reasignaInscripcion = function (e) {
        e.preventDefault();
        if (data.actual.usuarioValidador === undefined) {
            alert('Error: El registro aún no ha sido asignado.');
            return;
        }

        $('#usuario-validador').val('0');
        $('#reasignacion-modal').modal('show');
    };

    handlers.confirmaReasignacion = function (e) {
        e.preventDefault();
        $('#reasignacion-modal button').prop('disabled', true);
        var errores = [];
        $('#reasignacion-modal .valid-field').valida(errores, true);
        if (errores.length > 0) {
            $('#reasignacion-modal button').prop('disabled', false);
            return;
        }

        $.ajax({
            url: '/cultivos/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + data.actual.uuid + '/validacion/reasignacion/',
            data: JSON.stringify({id: $('#usuario-validador').val()}),
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('El validador ha sido reasignado.');
            $('#reasignacion-modal button').prop('disabled', false);
            $('#reasignacion-modal').modal('hide');
            utils.muestraDetalle(response.uuid);
        }).fail(function () {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Error: No fue posible cambiar validador.');
            }
            $('#reasignacion-modal button').prop('disabled', false);
        });
    };

    handlers.desisteReasignacion = function (e) {
        $('#usuario-validador').val('0');
        $('#reasignacion-modal').modal('hide');
    }

    handlers.regresa = function (e) {
        e.preventDefault();
        utils.regresa();
    }

    handlers.permiteEdicion = function (e) {
        e.preventDefault();
        $('#edicion-modal').modal('show');
        $('#contrato-actual').val(data.actual.numeroContratos);
        $('#predio-actual').val(data.actual.numeroPredios);
        $('#empresa-actual').val(data.actual.numeroEmpresas);
        $('#contrato-permitido').change(utils.calculaContratos);
        $('#predio-permitido').change(utils.calculaPredios);
        $('#empresa-permitido').change(utils.calculaEmpresas);
        $('#contrato-permitido').val('0');
        $('#predio-permitido').val('0');
        $('#empresa-permitido').val('0');
        $('#contrato-total').val(data.actual.numeroContratos);
        $('#predio-total').val(data.actual.numeroPredios);
        $('#empresa-total').val(data.actual.numeroEmpresas);
    };

    handlers.cierraEdicion = function (e) {
        e.preventDefault();
        $('#edicion-modal').modal('hide');
        $('#contrato-permitido').val('');
        $('#predio-permitido').val('');
        $('#empresa-permitido').val('');
        $('#contrato-total').val('');
        $('#predio-total').val('');
        $('#empresa-total').val('');
    };

    handlers.confirmaEdicion = function (e) {
        e.preventDefault();
        $('#edicion-modal button').prop('disabled', true);
        var errores = [];
        $('#edicion-modal .valid-field').valida(errores, true);
        if (errores.length > 0) {
            $('#edicion-modal button').prop('disabled', false);
            return;
        }
        var inscripcion = {
            numeroContratos: $('#contrato-total').val(),
            numeroPredios: $('#predio-total').val(),
            numeroEmpresas: $('#empresa-total').val()
        };
        $.ajax({
            url: '/cultivos/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + data.actual.uuid + '/edicion/',
            data: JSON.stringify(inscripcion),
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('Se actualizó el registro.');
            $('#edicion-modal button').prop('disabled', false);
            $('#edicion-modal').modal('hide');
            utils.muestraDetalle(response.uuid);
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.maiz.inscripcion.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Error: No fue posible actualizar el registro.');
            }
            $('#edicion-modal button').prop('disabled', false);
        });
    };

    utils.regresa = function () {
        data.actual = null;
        $('#detalle-elemento').hide();
        $('#detalle-inscripcion-elemento,#estatus-inscripcion-elemento,#historial-inscripcion-elemento,#datos-capturados-inscripcion-elemento').html('');
        $('#datos-busqueda-form').show();
        $('#folio').limpiaErrores().focus();
        $('#edicion-inscripcion-button').hide();
    }

    utils.muestraDetalle = function (uuid) {
        $.ajax({
            url: '/cultivos/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + uuid,
            data: {historial: true, expand: 3, contratos: true},
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            data.actual = response;
            $('#button-flecha,#button-pendiente,#button-cancelar-deudor').prop('disabled', true);
            // Mostramos el detalle
            switch (data.tipoInscripcion) {
                case 'contratos':
                    $('#detalle-inscripcion-elemento').html($.segalmex.maiz.inscripcion.vista.construyeInscripcion(response));
                    //agregamos la funcion de cacelar negativo los contratos y omitmos el botón de marcar como pendiente;
                    $('#button-flecha').prop('disabled', false);
                    $('#button-pendiente,#button-cancelar-deudor').hide();
                    break;
                case 'productores':
                    $.segalmex.maiz.inscripcion.productor.vista.muestraInscripcion(response, 'detalle-inscripcion-elemento');
                    $('#edicion-inscripcion-button').show();
                    $('#button-flecha,#button-pendiente,#button-cancelar-deudor').prop('disabled', false);
                    $('#button-pendiente,#button-cancelar-deudor').show();
                    break;
                case 'facturas':
                    $('#detalle-inscripcion-elemento').html($.segalmex.maiz.inscripcion.factura.vista.construyeInscripcion(response));
                    break;
            }
            $('#estatus-inscripcion-elemento').html($.segalmex.maiz.inscripcion.vista.construyeEstatus(response));
            $('#historial-inscripcion-elemento').html($.segalmex.maiz.inscripcion.vista.construyeHistorial(response.historial));

            $('#detalle-elemento').show();
            $('#datos-busqueda-form,#resultados-busqueda').hide();
        }).fail(function () {
            alert('Error: No fue posible obtener el registro.');
        });
    }

    utils.inicializaValidaciones = function () {
        $('.valid-field').configura();
        $('#folio').configura({
            pattern: /^\d{1,6}$/,
            minlength: 1,
            maxlength: 6
        }).validacion();
        $('#comentario-cancelacion,#comentario-pendiente').configura({type: 'comentario'}).configura({maxlength: 1023});
        $('#contrato-actual,#contrato-permitido,#contrato-total').configura({
            type: 'number',
            min: 0
        });
        $('.valid-field').validacion();
    };

    utils.cargaCatalogos = function () {
        $.ajax({
            url: '/cultivos/resources/paginas/bandeja-asignacion/',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            $('#usuario-validador').actualizaCombo(response.validadores);
        }).fail(function () {
            alert('Error: No se pudo obtener los catálogos de la página.');
        });
    };

    utils.calculaContratos = function () {
        var actual = $('#contrato-actual').val();
        var permitido = $('#contrato-permitido').val();
        var etiqueta = 'contrato-total';
        utils.calculaTotal(actual, permitido, etiqueta);
    };

    utils.calculaPredios = function () {
        var actual = $('#predio-actual').val();
        var permitido = $('#predio-permitido').val();
        var etiqueta = 'predio-total';
        utils.calculaTotal(actual, permitido, etiqueta);
    };

    utils.calculaEmpresas = function () {
        var actual = $('#empresa-actual').val();
        var permitido = $('#empresa-permitido').val();
        var etiqueta = 'empresa-total';
        utils.calculaTotal(actual, permitido, etiqueta);
    };

    utils.calculaTotal = function (actual, permitido, etiqueta) {
        var total = Number.parseInt(actual) + Number.parseInt(permitido);
        $('#' + etiqueta).val(total);
    };

    handlers.desistePendiente = function (e) {
        $('#comentario-pendiente').val('');
        $('#confirmacion-pendiente-modal').modal('hide');
    };


    utils.marcaPendiente = function (e) {
        e.preventDefault();
        data.tipoCancelacion = e.target.id ? e.target.id : e.target.parentElement.id;
        if (data.actual.estatus.clave === 'pendiente-revision') {
            alert('Error: El registro ya se encuentra marcado como pendiente.');
            return;
        }
        $('#comentario-pendiente').val('');
        $('#confirmacion-pendiente-modal').modal('show');
    };

    handlers.confirmaPendiente = function (e) {
        e.preventDefault();
        $('#confirmacion-pendiente-modal button').prop('disabled', true);
        var errores = [];
        $('#confirmacion-pendiente-modal .valid-field').valida(errores, true);
        if (errores.length > 0) {
            $('#confirmacion-pendiente-modal button').prop('disabled', false);
            return;
        }

        var url = '/cultivos/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + data.actual.uuid + '/pendiente/';
        $.ajax({
            url: url,
            data: JSON.stringify({comentarioEstatus: $('#comentario-pendiente').val()}),
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('El registro ha sido marcado como pendiente.');
            $('#confirmacion-pendiente-modal button').prop('disabled', false);
            $('#confirmacion-pendiente-modal').modal('hide');
            utils.muestraDetalle(response.uuid);
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Error: No fue posible macarcar como pendiente el registro.');
            }
            $('#confirmacion-pendiente-modal button').prop('disabled', false);
        });
    };

    utils.validaEstatusCancelacion = function () {
        return data.actual.estatus.clave === 'negativo-deudor' ||
                data.actual.estatus.clave === 'cancelada' ||
                data.actual.estatus.clave === 'negativo';

    };
})(jQuery);
