/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.cultivos.administracion.pagos');
    var data = {};
    var handlers = {};
    var utils = {};

    $.segalmex.cultivos.administracion.pagos.init = function (params) {
        utils.inicializaValidaciones();
        $('#button-buscar').click(handlers.busca);
        $('#button-limpiar').click(handlers.limpia);
        $('#button-regresar').click(handlers.regresaDetalle);
        $('#button-requerimiento-cancelado').click(handlers.cancelaNoPagado);
        $('#button-requerimiento-devuelto').click(handlers.devuelveNoPagado);
    };

    handlers.busca = function (e) {
        e.preventDefault();
        $('#button-buscar').html('Buscando...').prop('disabled', true);
        var errores = [];
        $('.valid-field').valida(errores, false);
        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            $('#button-buscar').html('Buscar').prop('disabled', false);
            return;
        }

        var id = $('#folio').val();
        var cegap = $('#cegap').val();
        if (id === '' && cegap === '') {
            alert('Es necesario ingresar un parámetro de búsqueda.');
            $('#button-buscar').html('Buscar').prop('disabled', false);
            return;
        }

        var datos = {
            id: id,
            cegap: cegap
        };

        for (var prop in datos) {
            if (datos[prop] === '') {
                delete datos[prop];
            }
        }

        $.ajax({
            url: '/cultivos/resources/requerimientos-pago/administracion/',
            data: datos,
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            // Mostramos el detalle del requerimiento.
            data.uuidRequerimiento = response.uuid;
            $('#detalle-elemento').html(utils.creaDetalleRequerimiento(response));
            $('#table-detalle-facturas').on('click', 'button.no-pagado', handlers.muestraModal);
            $('#div-busqueda-requerimiento').hide();
            $('#detalle-requerimiento').show();
            $('#button-buscar').html('Buscar').prop('disabled', false);
        }).fail(function (responseF) {
            if (responseF.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(responseF.responseJSON));
            } else {
                alert('Error: No fue posible obtener el requerimiento.');
            }
            $('#button-buscar').html('Buscar').prop('disabled', false);
        });

    };

    handlers.cancelaNoPagado = function (e) {
        e.preventDefault();
        if ($('#motivo').val() === '') {
            alert('Es necesario agregar el motivo.');
            return;
        }

        if (!confirm("¿Seguro que desea cancelar el requerimiento?")) {
            return;
        }
        $('#button-requerimiento-cancelado').html('Cancelando...').prop('disabled', true);

        var usoFactura = {
            uuid: data.uuidUsoFactura,
            comentarioEstatus: $('#motivo').val()
        };
        $.ajax({
            url: '/cultivos/resources/requerimientos-pago/' + data.uuidRequerimiento + '/uso-factura/no-pagado-cancelado/',
            type: 'PUT',
            data: JSON.stringify(usoFactura),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('Requerimiento cancelado correctamente.');
            utils.getRequerimiento(response.uuid);
            $('#button-requerimiento-cancelado').html('Cancelado').prop('disabled', false);
            $('#no-pagado-modal').modal('hide');
        }).fail(function (responseF) {
            if (responseF.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(responseF.responseJSON));
            } else {
                alert('Error: No fue posible cancelar como No Pagado el requerimiento.');
            }
            $('#button-requerimiento-cancelado').html('Cancelado').prop('disabled', false);
        });
    };

    handlers.devuelveNoPagado = function (e) {
        e.preventDefault();
        if ($('#motivo').val() === '') {
            alert('Es necesario agregar el motivo.');
            return;
        }

        if (!confirm("¿Seguro que desea devolver el requerimiento?")) {
            return;
        }
        $('#button-requerimiento-devuelto').html('Devolviendo...').prop('disabled', true);

        var usoFactura = {
            uuid: data.uuidUsoFactura,
            comentarioEstatus: $('#motivo').val()
        };
        $.ajax({
            url: '/cultivos/resources/requerimientos-pago/' + data.uuidRequerimiento + '/uso-factura/no-pagado-devuelto/',
            type: 'PUT',
            data: JSON.stringify(usoFactura),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('Requerimiento devuelto correctamente.');
            utils.getRequerimiento(response.uuid);
            $('#button-requerimiento-devuelto').html('Devuelto').prop('disabled', false);
            $('#no-pagado-modal').modal('hide');
        }).fail(function (responseF) {
            if (responseF.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(responseF.responseJSON));
            } else {
                alert('Error: No fue posible cancelar como No Pagado el requerimiento.');
            }
            $('#button-requerimiento-devuelto').html('Devuelto').prop('disabled', false);
        });
    };


    handlers.muestraModal = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var uuid = id.substring('no-pagado-'.length);
        data.uuidUsoFactura = uuid;
        $('#motivo').val('');
        $('#no-pagado-modal').modal('show');
    };

    handlers.regresaDetalle = function (e) {
        e.preventDefault();
        utils.regresa();
    };

    handlers.limpia = function (e){
        e.preventDefault();
        $('#folio,#cegap').val('').limpiaErrores();
    };

    utils.getRequerimiento = function (uuid) {
        $.ajax({
            url: '/cultivos/resources/requerimientos-pago/' + uuid,
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            // Mostramos el detalle
            data.uuidRequerimiento = response.uuid;
            $('#detalle-elemento').html(utils.creaDetalleRequerimiento(response));
            $('#table-detalle-facturas').on('click', 'button.no-pagado', handlers.muestraModal);
        }).fail(function () {
            alert('Error: No fue posible obtener el registro.');
        });
    };

    utils.regresa = function () {
        $('#detalle-requerimiento').hide();
        $('#detalle-elemento').html('');
        $('#folio,#cegap').val('').limpiaErrores();
        $('#div-busqueda-requerimiento').show();
    };


    utils.inicializaValidaciones = function () {
        $(".valid-field").configura();

        //No tenemos Folio, es el ID
        $('#folio').configura({
            pattern: /^\d{1,6}$/,
            minlength: 1,
            maxlength: 9,
            required: false
        });

        $('#cegap').configura({
            minlength: 1,
            maxlength: 2047,
            textTransform: null,
            required: false,
            pattern: /^[A-Za-z0-9ÑñÁÉÍÓÚáéíóúÄËÏÖÜäëïöü@\.,:\n\-\(\)" ]*$/
        });

        $('#motivo').configura({
            minlength: 1,
            maxlength: 2047,
            allowSpace: true,
            textTransform: null,
            pattern: /^[A-Za-z0-9ÑñÁÉÍÓÚáéíóúÄËÏÖÜäëïöü@\.,:\n\-\(\)" ]*$/
        }).validacion();
        $('.valid-field').validacion();
    };


    utils.creaDetalleRequerimiento = function (r) {
        var buffer = [];

        buffer.push('<div class="card mb-4 shadow-sm">');
        buffer.push('<h4 class="card-header">Detalle del Requerimiento de Pago</h4>');

        buffer.push('<div class="card-body">'); // card-body
        buffer.push('<h3 class="card-title">Folio: ');
        buffer.push(r.id);
        buffer.push('</h3>');
        buffer.push('<h4>Programa de Precios de Garantía a Productos Alimentarios Básicos</h4>');
        buffer.push('<br/>');
        //PAGO
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Programa:</strong><br/>');
        buffer.push(r.programa);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Ciclo agrícola:</strong><br/>');
        buffer.push(r.ciclo.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Layout:</strong><br/>');
        buffer.push(r.layout);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Fecha:</strong><br/>');
        buffer.push($.segalmex.date.isoToFecha(r.fechaCreacion));
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Clave Área:</strong><br/>');
        buffer.push(r.claveArea);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Clave Centro de acopio:</strong><br/>');
        buffer.push(r.claveCentroAcopio);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Estatus:</strong><br/>');
        buffer.push(r.estatus);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Usuario:</strong><br/>');
        buffer.push(r.usuario.nombre);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');
        var clase = r.reporteGenerado === '--' ? 'btn btn-outline-secondary disabled' : 'btn btn-outline-secondary';
        buffer.push('<div class="text-right">');
        buffer.push('<a role="button" class="' + clase + '"' +
                ' href="/cultivos/resources/requerimientos-pago/' + r.uuid + '/requerimiento-pago.xlsx' +
                '" target="_blank"><i class="fa fa-file-excel"></i> Descargar requerimiento de pago</a>');
        buffer.push('</div>');

        //TABLA DE USOS FACTURAS
        buffer.push('<h4>Facturas</h4>');
        buffer.push('<br/>');
        buffer.push('<div class="table-responsive">');
        buffer.push('<table id="table-detalle-facturas" class="table table-striped table-bordered table-hover">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<tr class="success">');
        buffer.push('<th>#</th>');
        buffer.push('<th>Folio</th>');
        buffer.push('<th>Fecha de registro</th>');
        buffer.push('<th>Productor</th>');
        buffer.push('<th>RFC Productor</th>');
        buffer.push('<th>Toneladas</th>');
        buffer.push('<th>Estmulo total</th>');
        buffer.push('<th>Estatus</th>');
        buffer.push('<th></th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');
        var toneladas = 0;
        var total = 0;
        if (r.requerimientos) {
            for (var i = 0; i < r.requerimientos.length; i++) {
                var f = r.requerimientos[i];
                buffer.push('<tr>');
                buffer.push('<td>');
                buffer.push(i + 1);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(f.usoFactura.folio);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push($.segalmex.date.isoToFecha(f.usoFactura.fechaCreacion));
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(f.productor.nombre);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(f.productor.rfc);
                buffer.push('</td>');
                buffer.push('<td class="text-right">');
                buffer.push(utils.format(f.usoFactura.toneladas.toFixed(3)));
                buffer.push('</td>');
                buffer.push('<td class="text-right">');
                buffer.push(f.usoFactura.estimuloTotal ? utils.format(f.usoFactura.estimuloTotal.toFixed(2)) : '--');
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(f.usoFactura.estatus.nombre);
                buffer.push('</td>');
                buffer.push('<td class="text-center">');
                buffer.push('<button id="no-pagado-' + f.usoFactura.uuid + '" class="btn btn-danger btn-sm no-pagado"');
                buffer.push(f.usoFactura.estatus.clave !== 'en-proceso' ? ' disabled="disabled">' : '>');
                buffer.push('<i class="fa fa-minus-circle"></i></button>');
                buffer.push('</td>');
                buffer.push('</tr>');
                toneladas += f.usoFactura.toneladas;
                total += f.usoFactura.estimuloTotal;
            }
            if (r.requerimientos.length === 0) {
                buffer.push('<tr><td colspan="7">No existen requerimientos de pago registrados.</td></tr>');
            }
        } else {
            buffer.push('<tr><td colspan="7">No existen requerimientos de pago registrados.</td></tr>');
        }
        buffer.push('</tbody>');
        buffer.push('<tfoot class="thead-dark">');
        buffer.push('<tr>');
        buffer.push('<th colspan="5"></th>');
        buffer.push('<th class="text-right">');
        buffer.push(utils.format(toneladas.toFixed(3)));
        buffer.push('</th>');
        buffer.push('<th class="text-right">');
        buffer.push(utils.format(total.toFixed(2)));
        buffer.push('</th>');
        buffer.push('<th colspan="2"></th>');
        buffer.push('</tr>');
        buffer.push('</tfoot>');
        buffer.push('</table>');
        buffer.push('</div>');
        buffer.push('<br/>');

        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card
        return buffer.join('\n');
    };

    utils.format = function (str) {
        return str.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    };
})(jQuery);