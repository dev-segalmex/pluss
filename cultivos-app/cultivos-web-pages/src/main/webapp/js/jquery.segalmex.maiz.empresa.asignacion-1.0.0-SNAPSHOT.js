/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.maiz.empresa.asignacion');
    var data = {};
    var handlers = {};
    var utils = {};

    $.segalmex.maiz.empresa.asignacion.init = function (params) {
        handlers.cargaRegistros();
        $('#button-asignar-Validador').click(handlers.asignaValidador);
        $('#cerrar-modal-button').click(handlers.cierraModal);
        $('#asignar-validador-button').click(handlers.guardaAsignacion);
    };

    handlers.cargaRegistros = function () {
        $.ajax({
            url: '/cultivos/resources/paginas/bandejas/asignacion/empresas/',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            $('#table-asignacion').html(utils.creaTablaRegistros(response));
            $('#div-asignacion table #todas-checkbox').click(handlers.seleccionaTodos);
            utils.cargaCatalogos();
        }).fail(function () {
            alert('Ocurrió un error al cargar los registros.');
        });
    };

    handlers.seleccionaTodos = function (e) {
        var checked = $(e.target).is(':checked');
        $('#div-asignacion table tbody input:checkbox').prop('checked', checked);
    };

    handlers.asignaValidador = function (e) {
        e.preventDefault();
        var seleccionados = $('#table-asignacion table tbody input:checked');
        if (seleccionados.length === 0) {
            alert('Error: Es necesario seleccionar al menos un registro para asignar validador.');
            e.target.disabled = false;
            return;
        }
        $('#asignacion-modal').modal('show');
    };

    handlers.cierraModal = function (e) {
        e.preventDefault();
        $('#asignacion-modal').modal('hide');
        $('#usuario-validador-modal').val('0');
    };

    handlers.guardaAsignacion = function (e) {
        e.preventDefault();
        e.target.disabled = true;
        var validador = $('#usuario-validador-modal').val();
        if (validador === '0') {
            alert('Error: Seleccione un usuario');
            e.target.disabled = false;
            return;
        }

        var seleccionados = $('#table-asignacion table tbody input:checked');
        var registros = [];
        seleccionados.each(function () {
            var uuid = this.id.substring(0, this.id.length - '-uuid-check'.length);
            registros.push(uuid);
        });

        var usuario = $('#usuario-validador-modal').val();

        for (var i = 0; i < registros.length; i++) {
            var uuid = registros[i];
            $.ajaxq('asignacionesQueue', {
                url: '/cultivos/resources/empresas/' + uuid + '/validacion/',
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json',
                data: JSON.stringify({id: usuario})
            }).done(function () {
                handlers.cargaRegistros();
                alert('Éxito: se asignó validador correctamente.');
                $('#asignacion-modal').modal('hide');
                $('#usuario-validador-modal').val('0');
                handlers.cargaRegistros();
                e.target.disabled = false;
            }).fail(function () {
                alert('Error: ocurrio un error al asignar validador.');
                e.target.disabled = false;
            });
        }
    };

    utils.creaTablaRegistros = function (registros) {
        var buffer = [];
        buffer.push('<table id="table-resultados-busqueda" class="table table-striped table-bordered table-hover" width="100%">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<tr class="success">');
        buffer.push('<th class="text-center">');
        buffer.push('<input id="todas-checkbox" type="checkbox"/>');
        buffer.push('</th>');
        buffer.push('<th>#</th>');
        buffer.push('<th>RFC</th>');
        buffer.push('<th>Nombre</th>');
        buffer.push('<th>Responsable</th>');
        buffer.push('<th>Ciclo-Cultivo</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');
        for (var i = 0; i < registros.length; i++) {
            var r = registros[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-center">');
            buffer.push('<input type="checkbox" id="' + r.uuid + '-uuid-check" class="uuid-check" value="' + r.estatus + '"/>');
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(r.rfc);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(r.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(r.nombreResponsable);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(r.ciclo.nombre + '-' + r.cultivo.nombre);
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        if (registros.length === 0) {
            buffer.push('<tr><td colspan="8">No hay registros para asignar validador.</td></tr>');
        }
        buffer.push('</tbody>');
        buffer.push('</table>');
        buffer.push('<br/>');
        return buffer.join('');
    };

    utils.cargaCatalogos = function () {
        $.ajax({
            url: '/cultivos/resources/paginas/bandeja-asignacion/ventanilla/',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            $('#usuario-validador-modal').actualizaCombo(response.validadores);
        }).fail(function () {
            alert('Error: No se pudo obtener los catálogos de la página.');
        });
    };
})(jQuery);