/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.maiz.consultas.seguimiento.productor');
    var data = {
        productor: [],
        productorCiclo: []

    };
    var handlers = {};
    var utils = {};

    $.segalmex.maiz.consultas.seguimiento.productor.init = function (params) {
        utils.inicializaValidaciones();
        $('#button-buscar').click(handlers.busca);
        $('#button-limpiar').click(utils.limpiar);
        $('#button-regresar').click(handlers.regresa);
    };

    utils.inicializaValidaciones = function () {
        $(".valid-field").configura({required: false});
        $('#curp').configura({
            type: 'curp',
            required: false
        });
        $('#rfc').configura({
            type: 'rfc-fisica',
            required: false
        });

        $('#nombre,#pApellido,#sApellido').configura({
            required: false
        });

        $('.valid-field').validacion();
    };

    utils.limpiar = function () {
        $('#curp,#rfc,#nombre,#pApellido,#sApellido').val('').limpiaErrores();
        $('#resultados-busqueda').hide();
    };

    handlers.busca = function (e) {
        e.preventDefault();
        $(e.target).html('Buscando...').prop('disabled', true);
        var errores = [];
        $('.valid-field').valida(errores, false);
        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            $(e.target).html('Buscar').prop('disabled', false);
            return;
        }

        if (utils.validaCamposVacios()) {
            $(e.target).html('Buscar').prop('disabled', false);
            alert('Ingrese al menos un parámetro para realizar la búsqueda.');
            return;
        }
        $('#cargando-resultados').show();
        $('#resultados-busqueda').hide();
        var datos = {
            curp: $('#curp').val(),
            rfcProductor: $('#rfc').val(),
            nombre: $('#nombre').val(),
            papellido: $('#pApellido').val(),
            sapellido: $('#sApellido').val(),
            seguimiento: true
        };
        for (var prop in datos) {
            if (datos[prop] === '') {
                delete datos[prop];
            }
        }

        data.params = datos;

        $.ajax({
            url: '/cultivos/resources/productores/',
            type: 'GET',
            data: datos,
            dataType: 'json'
        }).done(function (response) {
            $('#data').html(utils.construyeProductor(response));
            $(e.target).html('Buscar').prop('disabled', false);
            $('#cargando-resultados').hide();
            $('#resultados-busqueda').slideDown();
            $('#table-resultados-busqueda').on('click', 'a.link-uuid', handlers.muestraProductorCiclo);
        }).fail(function (jqXHR) {
            $('#cargando-resultados').hide();
            $(e.target).html('Buscar').prop('disabled', false);
            alert('Error: No fue posible realizar la consulta.');
        });
    };

    utils.construyeProductor = function (productores) {
        var buffer = [];
        buffer.push('<table id="table-resultados-busqueda" class="table table-striped table-bordered table-hover" width="100%">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<tr class="success">');
        buffer.push('<th>#</th>');
        buffer.push('<th>Nombre</th>');
        buffer.push('<th>CURP</th>');
        buffer.push('<th>RFC</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');

        if (productores.length === 0) {
            buffer.push('<tr><td colspan="4">No se encontraron resultados.</td></tr>');
        }

        for (var i = 0; i < productores.length; i++) {
            var p = productores[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-center">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td><a class="link-uuid" href="#" id="uuid.' + p.uuid + '">');
            buffer.push(p.nombre);
            buffer.push('</a></td>');
            buffer.push('<td>');
            buffer.push(p.curp);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(p.rfc);
            buffer.push('</td>');
            buffer.push('</tr>');
        }

        buffer.push('</tbody>');
        buffer.push('</table>');

        buffer.push('<br/>');
        return buffer.join('');
    };

    handlers.muestraProductorCiclo = function (e) {
        e.preventDefault();
        var uuid = e.target.id.split('.')[1];
        $.ajax({
            url: '/cultivos/resources/productores/' + uuid,
            type: 'GET',
            dataType: 'json'
        }).done(function (responseP) {
            if (responseP.length > 0) {
                data.productorCiclo = responseP;
            }
            $('#table-productor-ciclo').html(utils.construyeProductorCiclo(responseP));
            $('#productor-ciclo').show();
            $('#datos-busqueda-form,#resultados-busqueda').hide();
            $('#table-resultados-productor').on('click', 'a.link-id', handlers.muestraDetalle);
        }).fail(function () {
            alert('Error: No se encontró el productor.');
        });
    };


    utils.construyeProductorCiclo = function (productores) {

        var buffer = [];
        buffer.push('<table id="table-resultados-productor" class="table table-striped table-bordered table-hover" width="100%">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<tr class="success">');
        buffer.push('<th>#</th>');
        buffer.push('<th>Nombre</th>');
        buffer.push('<th>Ciclo</th>');
        buffer.push('<th>Cultivo</th>');
        buffer.push('<th>Activo</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');
        if (productores.length === 0) {
            buffer.push('<tr><td colspan="4">No se encontraron resultados.</td></tr>');
        } else {
            for (var i = 0; i < productores.length; i++) {
                var p = productores[i];
                buffer.push('<tr>');
                buffer.push('<td class="text-center">');
                buffer.push(i + 1);
                buffer.push('</td>');
                buffer.push('<td><a class="link-id" href="#" id="id.' + p.id + '">');
                buffer.push(p.productor.nombre);
                buffer.push('</a></td>');
                buffer.push('<td>');
                buffer.push(p.ciclo.nombre);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(p.cultivo.nombre);
                buffer.push('</td>');
                buffer.push('<td class="text-center">');
                buffer.push(p.activo ? '<strong class="text-success"><i class="fas fa-check-circle"></i></strong>'
                                     : '<strong class="text-danger"><i class="fas fa-times-circle"></i></strong>');
                buffer.push('</td>');
                buffer.push('</tr>');
            }
        }
        buffer.push('</tbody>');
        buffer.push('</table>');

        buffer.push('<br/>');
        return buffer.join('');
    };

    utils.getDatosMostarDetalles = function (id) {
        var datos = {};
        for (var i = 0; i < data.productorCiclo.length; i++) {
            var p = data.productorCiclo[i];
            if (parseInt(id) === parseInt(p.id)) {
                datos = {
                    cultivo: p.cultivo.id,
                    ciclo: p.ciclo.id,
                    uuidProductor: p.productor.uuid,
                };
            }
        }
        return datos;
    };

    handlers.muestraDetalle = function (e) {
        e.preventDefault();
        var id = e.target.id.split('.')[1];
        var datos = utils.getDatosMostarDetalles(id);
        var productor = utils.getProductorCiclo(id);
        $('#detalle-productor').html($.segalmex.maiz.vista.seguimiento.productor.muestraProductor(productor));
        utils.cargaInscripciones(datos);
        utils.cargaFacturas(datos);
        utils.cargaPagos(datos);
        $('#detalle-elemento').show();
        $('#productor-ciclo').hide();
        $('#button-regresar-detalle').click(handlers.regresaProductorCiclo);
    };

    utils.getProductorCiclo = function (id){
        for (var i = 0; i < data.productorCiclo.length; i++) {
            var p = data.productorCiclo[i];
            if (parseInt(id) === parseInt(p.id)) {
                return p;
            }
        }
    };

    handlers.regresa = function (e) {
        e.preventDefault();
        $('#productor-ciclo').hide();
        $('#datos-busqueda-form,#resultados-busqueda').show();
    };

    handlers.regresaProductorCiclo = function (e) {
        e.preventDefault();
        $('#detalle-elemento').hide();
        $('#productor-ciclo').show();
    };

    utils.cargaInscripciones = function (datos) {
        $.ajax({
            url: '/cultivos/resources/productores/seguimiento/inscripcion/',
            type: 'GET',
            data: datos,
            dataType: 'json'
        }).done(function (response) {
            //construimos las inscripciones relacionadas con el rfc del productor
            $('#detalle-inscripciones').html($.segalmex.maiz.vista.seguimiento.productor.creaTablaInscripciones(response));
            $('#table-resultados-busqueda-inscripciones').on('click', 'a.link-inscripcion', handlers.muestraDetalleProductor);
        }).fail(function () {
            alert('No se encontró inscripción');
        });
    };

    utils.cargaFacturas = function (datos) {
        $.ajax({
            url: '/cultivos/resources/facturas/seguimiento/inscripcion/',
            type: 'GET',
            data: datos,
            dataType: 'json'
        }).done(function (response2) {
//            Construimos las facturas registradas por el productor
            $('#detalle-facturas').html($.segalmex.maiz.vista.seguimiento.productor.creaTablaFacturas(response2));
            $('#table-resultados-busqueda-facturas').on('click', 'a.link-facturas', handlers.muestraDetalleFacturas);
        }).fail(function () {
            alert('Error: No se encontraron facturas.');
        });
    };

    utils.cargaPagos = function (datos) {
        $.ajax({
            url: '/cultivos/resources/facturas/seguimiento/usos-factura/',
            type: 'GET',
            data: datos,
            dataType: 'json'
        }).done(function (response3) {
            //Construimos los pagos
            $('#detalle-pagos').html($.segalmex.maiz.vista.seguimiento.productor.creaTablaPagos(response3));
        }).fail(function () {
            alert('Error: No se encontraron usos factura.');
        });
    };

    handlers.regresaDetalleSeguimiento = function () {
        $('#detalle-elemento').show();
        $('#detalle-inscripciones-pro,#detalle-elemento-facturas').hide();
    };

    handlers.muestraDetalleProductor = function (e) {
        e.preventDefault();
        var uuid = e.target.id.split('.')[1];
        $.ajax({
            url: '/cultivos/resources/productores/maiz/inscripcion/' + uuid,
            data: {archivos: true, datos: true, historial: true, contratos: true, comentarios: true, expand: 3},
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            // Mostramos el detalle
            data.prediosDocumentos = response.prediosDocumentos;
            $.segalmex.maiz.inscripcion.productor.vista.muestraInscripcion(response, 'detalle-inscripcion-productor');
            $('#detalle-tipo-productor').html($.segalmex.maiz.inscripcion.vista.construyeTipoProductor(response.tipoProductor));
            $('#estatus-inscripcion-productor').html($.segalmex.maiz.inscripcion.vista.construyeEstatus(response));
            $('#historial-inscripcion-productor').html($.segalmex.maiz.inscripcion.vista.construyeHistorial(response.historial));
            $('#datos-capturados-inscripcion-productor').html($.segalmex.maiz.inscripcion.vista.construyeDatosCapturados(response.datos));
            $('#comentarios-inscripcion-productor').html($.segalmex.maiz.inscripcion.vista.construyeComentarios(response.comentarios, $.segalmex.common.pagina.comun.registrado));
            $('#button-regresar-detalle-seguimineto').click(handlers.regresaDetalleSeguimiento);
            $('#detalle-elemento').hide();
            $('#detalle-inscripciones-pro').show();
            $('#predios-documento-table').on('click', 'button.mostrar-repetidos', handlers.muestraFoliosRepetidos);
        }).fail(function () {
            alert('Error: No fue posible obtener el registro.');
        });
    };

    handlers.muestraDetalleFacturas = function (e) {
        e.preventDefault();
        var uuid = e.target.id.split('.')[1];
        $.ajax({
            url: '/cultivos/resources/facturas/maiz/inscripcion/' + uuid,
            data: {archivos: true, datos: true, historial: true, comentarios: true,
                uso_factura: true, productor: true, productorCiclo: true},
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            // Mostramos el detalle

            $.segalmex.maiz.inscripcion.factura.vista.muestraInscripcion(response, 'detalle-inscripcion-factura');
            $('#verificacion-inscripcion-factura').html($.segalmex.maiz.inscripcion.factura.vista.muestraVerificaciones(response));
            $('#productor-ciclco-factura').html($.segalmex.maiz.inscripcion.factura.vista.muestraProductorCiclo(response));
            $('#estatus-inscripcion-factura').html($.segalmex.maiz.inscripcion.vista.construyeEstatus(response));
            $('#historial-inscripcion-factura').html($.segalmex.maiz.inscripcion.vista.construyeHistorial(response.historial));
            $('#datos-capturados-inscripcion-factura').html($.segalmex.maiz.inscripcion.vista.construyeDatosCapturados(response.datos));
            $('#comentarios-inscripcion-factura').html($.segalmex.maiz.inscripcion.vista.construyeComentarios(response.comentarios, $.segalmex.common.pagina.comun.registrado));
            $('#pago-inscripcion-factura').html($.segalmex.maiz.inscripcion.factura.vista.muestraPago(response));
            $('#button-regresar-detalle-seguimineto2').click(handlers.regresaDetalleSeguimiento);
            $('#detalle-elemento').hide();
            $('#detalle-elemento-facturas').show();
        }).fail(function () {
            alert('Error: No fue posible obtener el registro.');
        });
    };

    utils.validaCamposVacios = function () {
        if ($('#curp').val() === '' && $('#rfc').val() === '' && $('#nombre').val() === '' && $('#pApellido').val() === '' && $('#sApellido').val() === '') {
            return true;
        }
        return false;
    };

    handlers.muestraFoliosRepetidos = function (e) {
        e.preventDefault();
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('mostrar-repetidos-'.length), 10);
        for (var i = 0; i < data.prediosDocumentos.length; i++) {
            if (idx === i) {
                var pd = data.prediosDocumentos[i];
            }
        }
        $.ajax({
            url: '/cultivos/resources/predio-documento/duplicados/' + pd.uuidTimbreFiscalDigital,
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            var folios = response.join(',');
            $('#label-folios').html('<strong >' + folios + '</strong>');
            $('#muestra-repetidos-modal').modal('show');
        }).fail(function () {
            alert('Error: No se pudo obtener los folios duplicados.');
        });
    };
})(jQuery);