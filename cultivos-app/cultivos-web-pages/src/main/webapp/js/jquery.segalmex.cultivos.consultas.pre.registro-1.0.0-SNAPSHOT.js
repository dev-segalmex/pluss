/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.cultivos.consultas.pre.registro');
    var data = {
        cultivo: ''
    };
    var handlers = {};
    var utils = {};

    $.segalmex.cultivos.consultas.pre.registro.init = function (params) {
        data.cultivo = params.cultivo;
        utils.inicializaValidaciones();
        utils.cargaCatalogos();
    };

    utils.inicializaValidaciones = function () {
        $(".valid-field").configura();
        $('#fecha-inicio-registro,#fecha-fin-registro').configura({
            type: 'date',
            required: false
        });
        $('#fecha-inicio-registro,#fecha-fin-registro').datepicker({
            language: "es",
            autoclose: true,
            forceParse: false,
            format: 'dd/mm/yyyy',
            yearRange: "c-10:c"
        });
        $('#rfc').configura({
            type: 'rfc-fisica',
            required: false
        });
        $('#folio').configura({
            pattern: /^\d{1,6}$/,
            minlength: 1,
            maxlength: 6,
            required: false
        });
        $('#curp').configura({
            type: 'curp',
            required: false
        });

        $('#nombre,#papellido,#sapellido').configura({
            required: false
        });

        $('#ciclo').configura({
            required: true
        });
        $('.valid-field').validacion();
    };

    $.segalmex.cultivos.consultas.pre.registro.limpiar = function () {
        $('.valid-field').val('').limpiaErrores();
        $('#ciclo').val('0').change().limpiaErrores();
        $('#resultados-busqueda').hide();
        $('#cargando-resultados').hide();
    };

    $.segalmex.cultivos.consultas.pre.registro.regresaResultados = function (e) {
        e.preventDefault();
        $('#detalle-elemento').hide();
        if (!data.busquedaAgrupada) {
            $('#datos-busqueda-form,#resultados-busqueda,#div-botones').show();
        }
        if (data.busquedaAgrupada) {
            $('#resultados-busqueda-agrupados').show();
        }
    };

    utils.getFiltros = function (e) {
        var datos = {
            folio: $('#folio').val(),
            fechaInicio: $('#fecha-inicio-registro').val(),
            fechaFin: $('#fecha-fin-registro').val(),
            nombre: $('#nombre').val(),
            papellido: $('#papellido').val(),
            sapellido: $('#sapellido').val(),
            curp: $('#curp').val(),
            rfcProductor: $('#rfc').val(),
            simplifica: true,
            ciclo: $('#ciclo').val()
        };
        for (var prop in datos) {
            if (datos[prop] === '') {
                delete datos[prop];
            }
        }
        if (datos.fechaInicio) {
            datos.fechaInicio = $.segalmex.date.fechaToIso(datos.fechaInicio);
        }
        if (datos.fechaFin) {
            datos.fechaFin = $.segalmex.date.fechaToIso(datos.fechaFin);
        }
        if (datos.ciclo === '0') {
            delete datos.ciclo;
        }
        return datos;
    };

    $.segalmex.cultivos.consultas.pre.registro.exporta = function (e) {
        e.preventDefault();
        utils.configuraCiclo(true);
        if (!utils.validaFiltros(e)) {
            return;
        }
        var filtros = utils.getFiltros(e);
        if (!filtros) {
            return;
        }
        $.segalmex.openPdf('/' + data.cultivo + '/resources/productores/maiz/pre-registro/csv/resultados-pre-registro.xlsx?' + $.param(filtros));
        $('#cargando-resultados').hide();
    };

    utils.validaFiltros = function (e) {
        $('#resultados-busqueda').hide();
        var errores = [];
        $('.valid-field').valida(errores, false);
        $.segalmex.validaFechaInicialContraFinal(errores, 'fecha-inicio-registro', 'fecha-fin-registro');
        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            $('#cargando-resultados').hide();
            return false;
        }
        return true;
    };

    $.segalmex.cultivos.consultas.pre.registro.busca = function (e) {
        e.preventDefault();
        utils.configuraCiclo(true);
        if (!utils.validaFiltros(e)) {
            return;
        }

        var filtros = utils.getFiltros(e);
        if (!filtros) {
            utils.desHabilitaCtrls(false);
            return;
        }
        $('#cargando-resultados').show();
        data.params = filtros;
        $.ajax({
            url: '/' + data.cultivo + '/resources/productores/maiz/pre-registro/',
            type: 'GET',
            data: filtros,
            dataType: 'json'
        }).done(function (response) {
            $(e.target).html('Buscar').prop('disabled', false);
            $('#button-limpiar').prop('disabled', false);
            $('#cargando-resultados').hide();
            $('#data').html(utils.creaDatatable(response));
            $('#table-resultados-busqueda').on('click', 'a.link-uuid', handlers.muestraDetalle);
            $('#resultados-busqueda').slideDown();
            data.busquedaAgrupada = false;
        }).fail(function () {
            alert('Error: No fue posible realizar la consulta.');
            $(e.target).html('Buscar').prop('disabled', false);
            $('#button-limpiar').prop('disabled', false);
            $('#cargando-resultados').hide();
        });
    };

    utils.creaDatatable = function (resultados) {
        var buffer = [];
        buffer.push('<table id="table-resultados-busqueda"');
        buffer.push(' class="table table-striped" width="100%">');
        buffer.push('<thead class = "table-success">');
        buffer.push('<tr class="success">');
        buffer.push('<th>No.</th>');
        buffer.push('<th>Folio</th>');
        buffer.push('<th>Fecha de registro</th>');
        buffer.push('<th>Nombre</th>');
        buffer.push('<th>CURP</th>');
        buffer.push('<th>RFC</th>');
        buffer.push('<th>Ciclo</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');
        for (var i = 0; i < resultados.length; i++) {
            var resultado = resultados[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td><a class="link-uuid" href="#" id="uuid.' + resultado.uuid + '">');
            buffer.push(resultado.folio);
            buffer.push('</a></td>');
            buffer.push('<td>');
            buffer.push($.segalmex.date.isoToFecha(resultado.fechaCreacion));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.nombre + ' ' + resultado.primerApellido + ' ' + resultado.segundoApellido);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.curp);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.rfc);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.ciclo.nombre);
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        if (resultados.length === 0) {
            buffer.push('<tr><td colspan="8" class="text-center">Sin resultados</td></tr>');
        }
        buffer.push('</tbody>');
        buffer.push('</table>');

        buffer.push('<br/>');
        return buffer.join('');
    };

    handlers.muestraDetalle = function (e) {
        e.preventDefault();
        var uuid = e.target.id.split('.')[1];
        $.ajax({
            url: '/' + data.cultivo + '/resources/productores/maiz/pre-registro/' + uuid,
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            // Mostramos el detalle
            $('#detalle-pre-registro').html(utils.creaDetalle(response));
            $('#detalle-elemento').show();
            $('#datos-busqueda-form,#resultados-busqueda,#div-botones,#resultados-busqueda-agrupados').hide();
            $('#button-descargar').attr('href', '/' + data.cultivo + '/resources/productores/maiz/pre-registro/' + response.uuid + '/pre-registro.pdf');
        }).fail(function () {
            alert('Error: No fue posible obtener el registro.');
        });
    };

    utils.creaDetalle = function (preRegistro) {
        var buffer = [];
        buffer.push('<div class="card mb-4 shadow-sm">');
        buffer.push('<h4 class="card-header">Detalle del Pre registro</h4>');

        buffer.push('<div class="card-body">'); // card-body
        buffer.push('<h3 class="card-title">Folio: ');
        buffer.push(preRegistro.folio);
        buffer.push('</h3>');
        buffer.push('<h4>Programa de Precios de Garantía a Productos Alimentarios Básicos</h4>');

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Cultivo:</strong><br/>');
        buffer.push(preRegistro.cultivo.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Ciclo agrícola:</strong><br/>');
        buffer.push(preRegistro.ciclo.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Tipo:</strong><br/>');
        buffer.push(preRegistro.etiquetaTipoCultivo);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<h4>Datos del productor</h4>');
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Nombre:</strong><br/>');
        buffer.push(preRegistro.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Primer apellido:</strong><br/>');
        buffer.push(preRegistro.primerApellido);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Segundo apellido:</strong><br/>');
        buffer.push(preRegistro.segundoApellido);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>CURP:</strong><br/>');
        buffer.push(preRegistro.curp);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>RFC:</strong><br/>');
        buffer.push(preRegistro.rfc);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Correo electrónico:</strong><br/>');
        buffer.push(preRegistro.correoElectronico);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Teléfono:</strong><br/>');
        buffer.push(preRegistro.telefono);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');
        if (preRegistro.cultivo.clave !== 'arroz') {
            if (preRegistro.cantidadContratos > 0) {
                buffer.push('<h4>Contratos del productor</h4>');
                buffer.push('<div class="table-responsive">');
                buffer.push('<table class="table table-bordered table-striped">');
                buffer.push('<thead class="thead-dark"><tr>');
                buffer.push('<th>Cantidad</th>');
                buffer.push('<th>Tipo de precio</th>');
                buffer.push('</tr></thead>');
                buffer.push('<tbody>');
                if (preRegistro.precioAbierto > 0) {
                    buffer.push('<tr>');
                    buffer.push('<td>');
                    buffer.push(preRegistro.precioAbierto);
                    buffer.push('</td>');
                    buffer.push('<td>Precio abierto (dólares)</td>');
                    buffer.push('</tr>');
                }
                if (preRegistro.precioCerradoDolares > 0) {
                    buffer.push('<tr>');
                    buffer.push('<td>');
                    buffer.push(preRegistro.precioCerradoDolares);
                    buffer.push('</td>');
                    buffer.push('<td>Precio cerrado (dólares)</td>');
                    buffer.push('</tr>');
                }
                if (preRegistro.precioCerradoPesos > 0) {
                    buffer.push('<tr>');
                    buffer.push('<td>');
                    buffer.push(preRegistro.precioCerradoPesos);
                    buffer.push('</td>');
                    buffer.push('<td>Precio cerrado (pesos mexicanos)</td>');
                    buffer.push('</tr>');
                }
                if (preRegistro.precioReferencia > 0) {
                    buffer.push('<tr>');
                    buffer.push('<td>');
                    buffer.push(preRegistro.precioReferencia);
                    buffer.push('</td>');
                    buffer.push('<td>Precio de referencia</td>');
                    buffer.push('</tr>');
                }

                if (preRegistro.precioAbiertoPesos > 0) {
                    buffer.push('<tr>');
                    buffer.push('<td>');
                    buffer.push(preRegistro.precioAbiertoPesos);
                    buffer.push('</td>');
                    buffer.push('<td>Precio abierto (pesos mexicanos)</td>');
                    buffer.push('</tr>');
                }

                buffer.push('<tbody>');
                buffer.push('</table>');
                buffer.push('</div>');
                buffer.push('<br/>');
            }
        }

        buffer.push('<h4>Beneficiario</h4>');
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Nombre:</strong><br/>');
        buffer.push(preRegistro.nombreBeneficiario);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Apellidos:</strong><br/>');
        buffer.push(preRegistro.apellidosBeneficiario);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>CURP:</strong><br/>');
        buffer.push(preRegistro.curpBeneficiario);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Parentesco:</strong><br/>');
        buffer.push(preRegistro.parentesco.nombre);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<div class="text-right">');
        buffer.push('<a role="button" class="btn btn-outline-success"' +
                'href="#" id="button-descargar"' +
                'target="_blank"><i class="fas fa-file-pdf"></i> Descargar pre-registro</a>');
        buffer.push('</div>');

        if (preRegistro.predios) {
            buffer.push('<h4>Predios</h4>');
            buffer.push('<div class="table-responsive">');
            buffer.push('<table class="table table-bordered table-striped">');
            buffer.push('<thead class="thead-dark"><tr>');
            buffer.push('<th>#</th>');
            buffer.push('<th>Tipo de cultivo</th>');
            buffer.push('<th>Tipo de posesión</th>');
            buffer.push('<th>Régimen Hídrico</th>');
            buffer.push('<th>Volumen (Toneladas)</th>');
            buffer.push('<th>Superficie (hectáreas)</th>');
            buffer.push('<th>Rendimiento (t/ha)</th>');
            buffer.push('<th>Municipio y estado</th>');
            buffer.push('</tr></thead>');
            buffer.push('<tbody>');
            for (var i = 0; i < preRegistro.predios.length; i++) {
                var predio = preRegistro.predios[i];
                buffer.push('<tr>');
                buffer.push('<td class="text-right">');
                buffer.push(i + 1);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(predio.tipoCultivo.nombre);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(predio.tipoPosesion.nombre);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(predio.regimenHidrico.nombre);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(utils.format(predio.volumen, 3));
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(utils.format(predio.superficie, 3));
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push((utils.format((parseFloat(predio.volumen) / parseFloat(predio.superficie)), 3)));
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(predio.municipio.nombre);
                buffer.push(', ');
                buffer.push(predio.estado.nombre);
                buffer.push('</td>');
                buffer.push('</tr>');
            }
        }

        buffer.push('<tbody>');
        buffer.push('</table>');
        buffer.push('</div>');
        buffer.push('<br/>');

        //Coberturas
        if (preRegistro.cultivo.clave !== 'arroz' && preRegistro.coberturas && preRegistro.coberturas.length > 0) {
            buffer.push('<h4>Instrumento de Administración de riesgos (IAR)</h4>');
            buffer.push('<div class="table-responsive">');
            buffer.push('<table class="table table-bordered table-striped">');
            buffer.push('<thead class="thead-dark"><tr>');
            buffer.push('<th>Tipo</th>');
            buffer.push('<th>Cantidad</th>');
            buffer.push('<th>Correduría</th>');
            buffer.push('</tr></thead>');
            buffer.push('<tbody>');
            for (var i = 0; i < preRegistro.coberturas.length; i++) {
                var iar = preRegistro.coberturas[i];
                buffer.push('<tr>');
                buffer.push('<td>');
                buffer.push(iar.tipoIar.nombre);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(iar.cantidad);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(iar.entidadCobertura ? iar.entidadCobertura.nombre : '--');
                buffer.push('</td>');

                buffer.push('</tr>');
            }
        }

        buffer.push('<tbody>');
        buffer.push('</table>');
        buffer.push('</div>');
        buffer.push('<br/>');

        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card
        return buffer.join('\n');
    };

    utils.cargaCatalogos = function () {
        $.ajax({
            url: '/cultivos/resources/paginas/consultas',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            $('#button-exportar-ciclo,#button-flecha').prop('disabled', true);
            data.ciclos = response.ciclos;
            $('#ciclo').actualizaCombo(response.ciclos);
        }).fail(function () {
            alert('Error: No se pudo obtener los catálogos de la página.');
        });
    };

    utils.format = function (str, fix) {
        return parseFloat(str).toFixed(fix).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    };

    $.segalmex.cultivos.consultas.pre.registro.exportaCiclo = function (e) {
        e.preventDefault();
        var ciclo = $.segalmex.get(data.ciclos, $(e.target).val());
        if (!ciclo) {
            $('#button-exportar-ciclo').attr('href', '#');
            $('#button-exportar-ciclo,#button-flecha').prop('disabled', true);
            return;
        }
        $('#button-exportar-ciclo,#button-flecha').prop('disabled', false);
        $('#button-exportar-ciclo').attr('href', '/' + data.cultivo + '/resources/productores/maiz/pre-registro/reporte/' + ciclo.id + '.xlsx');
    };

    utils.getEtiquetaTiposPrecio = function (pre) {
        var etiqueta = [];
        if (pre.precioAbierto === true) {
            etiqueta.push('Precio abierto (dólares)');
        }
        if (pre.precioCerradoDolares === true) {
            etiqueta.push('Precio cerrado (dólares)');
        }
        if (pre.precioCerradoPesos === true) {
            etiqueta.push('Precio cerrado (pesos mexicanos)');
        }

        if (pre.precioReferencia === true) {
            etiqueta.push('Precio de referencia');
        }
        return etiqueta.join(', ');
    };

    $.segalmex.cultivos.consultas.pre.registro.buscaAgrupado = function (e) {
        e.preventDefault();
        utils.desHabilitaCtrls(true);
        utils.configuraCiclo(false);
        if (!utils.validaFiltros(e)) {
            return;
        }
        var filtros = utils.getFiltros(e);
        if (!filtros) {
            return;
        }
        $('#cargando-resultados').show();
        $.ajax({
            url: '/' + data.cultivo + '/resources/productores/maiz/pre-registro/agrupado',
            type: 'GET',
            data: filtros,
            dataType: 'json'
        }).done(function (response) {
            $('#cargando-resultados').hide();
            data.agrupaciones = response;
            for (var i = 0; i < data.agrupaciones.length; i++) {
                data.agrupaciones[i].id = i + 1;
            }
            var renglones = '';
            if (response.length === 0) {
                renglones = '<tr><td colspan="4" class="text-center">Sin resultados</td></tr>';
                $('#data').html(renglones);
            } else {
                renglones = utils.creaDatatableAgrupaciones(data.agrupaciones);
                $('#data').html(renglones);
                $('#table-resultados-busqueda-agrupaciones').on('click', 'a.link-id', handlers.buscaAgrupados);
            }
            utils.desHabilitaCtrls(false);
            data.busquedaAgrupada = true;
            $('#resultados-busqueda').slideDown();
        }).fail(function () {
            utils.desHabilitaCtrls(false);
            alert('Error: No fue posible realizar la consulta.');
            $('#cargando-resultados').hide();
        });
    };

    utils.configuraCiclo = function (requerido) {
        $('#ciclo').configura({
            required: requerido
        });
        $('#ciclo').validacion();
    };

    utils.desHabilitaCtrls = function (disabled) {
        $('#button-exportar').prop('disabled', disabled);
        $('#button-buscar').prop('disabled', disabled);
        $('#button-buscar-agrupado').prop('disabled', disabled);
        $('#button-limpiar').prop('disabled', disabled);
    };

    utils.creaDatatableAgrupaciones = function (resultados) {
        var buffer = [];
        buffer.push('<table id="table-resultados-busqueda-agrupaciones"');
        buffer.push(' class="table table-striped" width="100%">');
        buffer.push('<thead class = "table-success">');
        buffer.push('<tr class="success">');
        buffer.push('<th>Ciclo</th>');
        buffer.push('<th>Estado</th>');
        buffer.push('<th>Total</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');
        var total = 0;
        for (var a = 0; a < resultados.length; a++) {
            var resultado = resultados[a];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(resultado.ciclo.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.estado.nombre);
            buffer.push('</td>');
            buffer.push('<td class="text-right"><a class="link-id" href="#" id="id.' + resultado.id + '">');
            buffer.push(resultado.total);
            buffer.push('</a></td>');
            buffer.push('</tr>');
            total += parseInt(resultado.total);
        }
        buffer.push('<tfoot class="table-success">');
        buffer.push('<tr>');
        buffer.push('<th colspan="2">Total</th>');
        buffer.push('<th class="text-right">' + total + '</th>');
        buffer.push('</tr>');
        buffer.push('</tfoot>');
        buffer.push('</tbody>');
        buffer.push('</table>');
        buffer.push('<br/>');
        return buffer.join('');
    };

    handlers.buscaAgrupados = function (e) {
        e.preventDefault();
        var id = e.target.id.split('.')[1];
        var agrupacion = $.segalmex.get(data.agrupaciones, id);
        utils.desHabilitaCtrls(true);
        $('#resultados-busqueda-agrupados').show();
        $('#datos-busqueda-form,#resultados-busqueda').hide();
        $('#cargando-resultados').show();
        $('#data-agrupados').html('');
        var filtros = utils.getFiltros(e);
        filtros.ciclo = agrupacion.ciclo.id;
        filtros.estado = agrupacion.estado.id;

        $.ajax({
            url: '/' + data.cultivo + '/resources/productores/maiz/pre-registro/',
            type: 'GET',
            data: filtros,
            dataType: 'json'
        }).done(function (response) {
            $('#resultados-busqueda-agrupados').show();
            $('#datos-busqueda-form,#resultados-busqueda,#div-botones').hide();
            var renglones = '';
            if (response.length === 0) {
                renglones = '<tr><td colspan="5" class="text-center">Sin resultados</td></tr>';
                $('#data-agrupados').html(renglones);
            } else {
                renglones = utils.creaDatatable(response);
                $('#data-agrupados').html(renglones);
                $('#table-resultados-busqueda').on('click', 'a.link-uuid', handlers.muestraDetalle);
            }
            utils.desHabilitaCtrls(false);
            $('#cargando-resultados').hide();
        }).fail(function () {
            utils.desHabilitaCtrls(false);
            alert('Error: No fue posible realizar la consulta.');
            $('#cargando-resultados').hide();
        });
    };

    $.segalmex.cultivos.consultas.pre.registro.regresaResultadosAgrupaciones = function (e) {
        e.preventDefault();
        $('#resultados-busqueda-agrupados').hide();
        $('#datos-busqueda-form,#resultados-busqueda,#div-botones').show();
    };
})(jQuery);