/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.maiz.empresa.validacion');
    var data = {
        registro: null
    };
    var handlers = {};
    var utils = {};

    $.segalmex.maiz.empresa.validacion.init = function () {
        handlers.cargaRegistros();
        $('#button-regresar').click(handlers.regresa);
        $('#todos-correctos-button').click(handlers.marcaTodosCorrectos);
        $('#todos-incorrectos-button').click(handlers.marcaTodosIncorrectos);
        $('#inscripcion-datos-capturados').on('change', 'input[type=radio]:checked', handlers.radioEstatusChange);
        $('#button-validacion-positiva').click(handlers.validaPositivamente);
        $('#button-validacion-negativa').click(handlers.validaNegativamente);
    };

    handlers.cargaRegistros = function () {
        $.ajax({
            url: '/cultivos/resources/paginas/bandejas/validacion/empresas/',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            $('#table-validacion').html(utils.creaTablaRegistros(response));
            $('#table-resultados-busqueda').on('click', 'a.link-rfc', handlers.muestraDetalle);
        }).fail(function () {
            alert('Ocurrió un error al cargar los registros.');
        });
    };

    handlers.muestraDetalle = function (e) {
        e.preventDefault();
        var uuid = e.target.id.split('.')[1];
        $.ajax({
            url: '/cultivos/resources/empresas/' + uuid + '/ventanilla/',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            // Mostramos el detalle
            data.registro = response;
            $('#registro-ventanilla').html($.segalmex.maiz.vista.validacion.empresa.construyeRegistro(response));
            $('#sucursales-table').on('click', 'a.muestra-ubicacion', handlers.muestraUbicacion);
            if (response.datos) {
                $('#inscripcion-datos-capturados tbody').html($.segalmex.maiz.vista.validacion.empresa.creaTablaCaptura(response.datos));
                utils.configuraTablaCaptura();
            }
            $('#detalle-registro').show();
            $('#div-validacion').hide();
        }).fail(function () {
            alert('Error: No fue posible obtener el registro.');
        });
    };

    handlers.regresa = function (e) {
        e.preventDefault();
        $('#detalle-registro').hide();
        $('#div-validacion').show();
        $('#registro-ventanilla').html('');
    };

    handlers.validaPositivamente = function (e) {
        e.preventDefault();
        if (!utils.realizaValidaciones()) {
            return;
        }
        var capturados = utils.getDatosCapturados();
        $.ajax({
            url: '/cultivos/resources/empresas/' + data.registro.uuid + '/validacion/positiva/',
            data: JSON.stringify(capturados),
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('El registro ha sido validado positivamente.');
            handlers.cargaRegistros();
            $('#detalle-registro').hide();
            $('#div-validacion').show();
            $('#registro-ventanilla').html('');
        }).fail(function () {
            alert('Error: No fue posible validar positivamente.');
        });
    };

    handlers.validaNegativamente = function (e) {
        e.preventDefault();
        $(e.target).prop('disabled', true);
        if (!utils.isEstatusSeleccionado()) {
            alert('Error: Es necesario indicar el estatus de todos los campos.');
            $(e.target).prop('disabled', false);
            return;
        }

        if (utils.isAprobada()) {
            alert('Error: Para validar negativamente se requiere de al menos un campo erróneo.');
            $(e.target).prop('disabled', false);
            return;
        }

        if (!utils.validaTablaCaptura()) {
            $(e.target).prop('disabled', false);
            return;
        }
        var capturados = utils.getDatosCapturados();
        $.ajax({
            url: '/cultivos/resources/empresas/' + data.registro.uuid + '/validacion/negativa/',
            data: JSON.stringify(capturados),
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('El registro ha sido validado negativamente.');
            handlers.cargaRegistros();
            $('#detalle-registro').hide();
            $('#div-validacion').show();
            $('#registro-ventanilla').html('');
        }).fail(function () {
            alert('Error: No fue posible validar negativamente.');
            $(e.target).prop('disabled', false);
        });
    };

    utils.realizaValidaciones = function () {
        if (!utils.isEstatusSeleccionado()) {
            alert('Error: Es necesario indicar el estatus de todos los campos');
            return false;
        }
        if (!utils.isAprobada()) {
            alert('Error: Para validar positivamente se requiere el estatus correcto de todos los campos.');
            return false;
        }
        if (!utils.validaTablaCaptura()) {
            return false;
        }
        return true;
    };

    utils.isEstatusSeleccionado = function () {
        return $('#inscripcion-datos-capturados input[type=radio]:checked').length === data.registro.datos.length;
    };

    utils.isAprobada = function () {
        var datos = $('#inscripcion-datos-capturados input[type=radio]:checked');
        for (var i = 0; i < datos.length; i++) {
            if ($(datos[i]).val() === 'false') {
                return false;
            }
        }
        return true;
    };

    utils.validaTablaCaptura = function () {
        $('#inscripcion-datos-capturados tbody input.valid-field').limpiaErrores();
        var errores = [];
        $('#inscripcion-datos-capturados tbody input.valid-field').valida(errores, true);
        return errores.length === 0;
    };

    utils.getDatosCapturados = function () {
        var datos = $('#inscripcion-datos-capturados input[type=radio]:checked');
        var dcs = [];
        for (var i = 0; i < datos.length; i++) {
            var dato = datos[i];
            var correcto = $(dato).val();
            var clave = dato.id.split('.')[0];
            var d = {clave: clave, correcto: correcto};
            if (correcto === 'false') {
                d.comentario = $('#comentario-' + clave).val();
            }
            dcs.push(d);
        }
        return dcs;
    };

    utils.creaTablaRegistros = function (registros) {
        var buffer = [];
        buffer.push('<table id="table-resultados-busqueda" class="table table-striped table-bordered table-hover" width="100%">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<tr class="success">');
        buffer.push('<th>#</th>');
        buffer.push('<th>RFC</th>');
        buffer.push('<th>Nombre</th>');
        buffer.push('<th>Responsable</th>');
        buffer.push('<th>Ciclo-Cultivo</th>');
        buffer.push('<th>Estatus</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');
        for (var i = 0; i < registros.length; i++) {
            var r = registros[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td><a class="link-rfc" href="#" id="folio.' + r.uuid + '">');
            buffer.push(r.rfc);
            buffer.push('</a></td>');
            buffer.push('<td>');
            buffer.push(r.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(r.nombreResponsable);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(r.ciclo.nombre + '-' + r.cultivo.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(r.estatus ? r.estatus.nombre : '--');
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        if (registros.length === 0) {
            buffer.push('<tr><td colspan="8">No hay registros.</td></tr>');
        }
        buffer.push('</tbody>');
        buffer.push('</table>');
        buffer.push('<br/>');
        return buffer.join('');
    };

    handlers.muestraUbicacion = function (e) {
        e.preventDefault();
        var ubicacion = $(e.target).html();
        $('#maps').muestraMaps({
            ubicacion: ubicacion
        });
    };

    utils.creaCampoCaptura = function (dato) {
        var buffer = [];
        switch (dato.tipo) {
            case 'texto':
                buffer.push('<input id="');
                buffer.push(dato.clave);
                buffer.push('" type="text" class="form-control dato-valor valid-field"/>');
                break;
            case 'archivo':
                buffer.push('<input id="');
                buffer.push(dato.clave);
                buffer.push('" ');
                if (dato.correcto) {
                    buffer.push('type="text" class="form-control dato-valor" value="');
                    buffer.push(dato.valor);
                    buffer.push('" disabled="disabled" />');
                } else {
                    buffer.push('type="file" class="form-control-file" accept="application/pdf" aria-describedby="' + dato.clave + '-ayuda"/>');
                    buffer.push('<small id="' + dato.clave + '-ayuda" class="form-text text-muted">El archivo no deberá ser mayor a 24 MB (25164800 bytes).</small>');
                }
                break;
        }
        return buffer.join('');
    };

    utils.configuraTablaCaptura = function () {
        $('#inscripcion-datos-capturados tbody input.valid-field').configura({type: 'comentario'}).configura({maxlength: 1023});
        $('#inscripcion-datos-capturados tbody input.valid-field').validacion();
    };

    handlers.marcaTodosCorrectos = function (e) {
        $('#inscripcion-datos-capturados input.form-check-input').prop('checked', false);
        $('#inscripcion-datos-capturados input.comentario-dato-capturado').val('').prop('disabled', true);
        $('#inscripcion-datos-capturados input.form-check-input[value=true]').prop('checked', true);
    };

    handlers.marcaTodosIncorrectos = function (e) {
        $('#inscripcion-datos-capturados input.form-check-input').prop('checked', false);
        $('#inscripcion-datos-capturados input.comentario-dato-capturado').prop('disabled', false);
        $('#inscripcion-datos-capturados input.form-check-input[value=false]').prop('checked', true);
    };

    handlers.radioEstatusChange = function (e) {
        var idval = e.target.id.split('.');
        $('#comentario-' + idval[0]).prop('disabled', idval[1] === 'true').val('').limpiaErrores();
    };

})(jQuery);