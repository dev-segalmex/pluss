/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


(function ($) {
    $.segalmex.namespace('segalmex.cultivos.uso.factura');
    var utils = {};
    var handlers = {};
    var data = {
        usos: [],
        bancos: [],
        tiposUso: [],
        uuid: ''
    };

    $.segalmex.cultivos.uso.factura.init = () => {
        utils.inicializaValidaciones();
        utils.cargaCatalogos();
        $('#button-buscar').click(handlers.busca);
        $('#button-regresar').click(utils.regresa);
        $('#cierra-modal').click(utils.cierraModal);
        $('#button-limpiar').click(handlers.limpiar);
        $('#clabe-productor').change(utils.cambiaClabe);
        $('#estimulo-tonelada').change(utils.calculaEstimuloTotal);
        $('#button-duplicar-uso-factura').click(handlers.duplicaUso);
    };

    handlers.duplicaUso = () => {
        $('#button-duplicar-uso-factura').prop('disabled', true);
        var errores = [];
        $('#estimulo-tonelada,#clabe-productor,#tipo-uso').valida(errores, true);
        if (errores.length !== 0) {
            $('#' + errores[0].campo).focus();
            $('#button-duplicar-uso-factura').prop('disabled', false);
            return;
        }

        var uso = {
            estimuloTonelada: $('#estimulo-tonelada').val(),
            clabe: $('#clabe-productor').val(),
            tipo: {id: $('#tipo-uso').val()}
        };

        $.ajax({
            type: 'PUT',
            url: '/cultivos/resources/facturas/uso-factura/duplica/' + data.uuid,
            data: JSON.stringify(uso),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            utils.cierraModal();
            alert('Se ha generado un nuevo uso factura.');
            utils.regresa();
            $('#button-duplicar-uso-factura').prop('disabled', false);
        }).fail(function (jqXHR) {
            $('#button-duplicar-uso-factura').prop('disabled', false);
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Error: no se pudo duplicar el uso factura.');
            }
        });

    };
    handlers.busca = () => {
        $('#folio').limpiaErrores();
        $('#guardar-button').html("Buscando...").prop('disabled', true);
        var errores = [];
        $('#folio').valida(errores, true);
        if (errores.length !== 0) {
            $('#' + errores[0].campo).focus();
            $('#button-buscar').html("Buscar").prop('disabled', false);
            return;
        }

        $.ajax({
            url: '/cultivos/resources/facturas/uso-factura/' + $('#folio').val(),
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            if (response.length === 0) {
                alert('No se encontró uso factura con el folio especificado.');
                $('#button-buscar').html("Buscar").prop('disabled', false);
                return;
            }
            data.usos = response;
            $('#detalle-elemento').html(utils.muestraDetalleUsoFactura(response));
            $('#div-busqueda-uso-factura').hide();
            $('#detalle-uso-factura').show();
            $('#button-buscar').html("Buscar").prop('disabled', false);
            $('#table-usos-factura').on('click', 'button.duplicar-uso', handlers.duplicarUso);
            $('#table-usos-factura').on('click', 'button.mostrar-comentario', handlers.muestraComentario);
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert("Ocurrió un error al buscar el uso factura.");
            }
            $('#button-buscar').html("Buscar").prop('disabled', false);
        });
    };

    handlers.muestraComentario = (e) => {
        e.preventDefault();
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var comentario = id.substring('mostrar-comentario-'.length);
        $('#comentario-modal .modal-body p').html(comentario);
        $('#comentario-modal').modal('show');
    };

    handlers.limpiar = () => {
        $('#folio').val('').limpiaErrores();
    };

    handlers.duplicarUso = (e) => {
        e.preventDefault();
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var uuid = id.substring('duplicar-uso-'.length);
        var uso = {};
        for (var i = 0; i < data.usos.length; i++) {
            if (data.usos[i].uuid === uuid) {
                uso = data.usos[i];
            }
        }
        utils.llenaModalUso(uso);
        $('#duplicar-uso-modal').modal('show');
    };

    utils.llenaModalUso = (uso) => {
        $('#toneladas').val(uso.toneladas);
        $('#estimulo-tonelada').val(uso.estimuloTonelada);
        $('#estimulo-total').val(uso.estimuloTotal);
        $('#clabe-productor').val(uso.clabe);
        $('#clabe-productor').change();
        $('#tipo-uso').val(uso.tipo.id);
        data.uuid = uso.uuid;
    };

    utils.cambiaClabe = () => {
        var clabe = $('#clabe-productor').val();
        if (clabe !== '') {
            var banco = $.segalmex.get(data.bancos, clabe.substring(0, 3), 'clave');
            if (!banco) {
                alert('Error: El número de CLABE no pertenece a ningún banco.');
                $('#clabe-productor,#numero-cuenta-productor').val('');
                $('#banco-productor').val('0');
                return;
            }
            $('#clabe-productor').val(clabe);
            $('#banco-productor').val(banco.id).prop('disabled', true);
        }
    };

    utils.regresa = () => {
        $('#detalle-uso-factura').hide();
        $('#div-busqueda-uso-factura').show();
    };

    utils.inicializaValidaciones = () => {
        $(".valid-field").configura();
        $('#folio').configura({
            pattern: /^\d{1,6}$/,
            minlength: 1,
            maxlength: 6
        });
        $('#estimulo-tonelada').configura({
            type: 'number',
            min: 0
        });
        $('#clabe-productor').configura({
            pattern: /^\d{18}$/,
            minlength: 18,
            maxlength: 18
        });
        $('.valid-field').validacion();
    };

    utils.muestraDetalleUsoFactura = (usos) => {
        var buffer = ``;
        var pc = usos[0].productorCiclo;
        var tc = usos[0].tipoCultivo;
        var estado = usos[0].estado;
        var contrato = usos[0].contrato ? usos[0].contrato : '--';
        var tp = usos[0].tipoPrecio;
        //Construimos el detalle del productor
        buffer += `<div class="card mb-4 shadow-sm">
                   <h4 class="card-header">Detalle</h4>
                   <div class="card-body">
                   <h4>Programa de Precios de Garantía a Productos Alimentarios Básicos</h4>
                   <br/>
                   <div class="row">
                   <div class="col-md-12"><strong>Productor:</strong><br/>${pc.productor.nombre}</div>
                   </div>
                   <br/>
                   <div class="row">
                   <div class="col-md-4"><strong>Ciclo:</strong><br/>${pc.ciclo.nombre}</div>
                   <div class="col-md-4"><strong>Tipo de cultivo:</strong><br/>${tc.nombre}</div>
                   <div class="col-md-4"><strong>Estado:</strong><br/>${estado.nombre}</div>
                   </div>
                   <br/>
                   <div class="row">
                   <div class="col-md-4"><strong>Contrato:</strong><br/>${contrato}</div>
                   <div class="col-md-4"><strong>Tipo de precio:</strong><br/>${tp ? tp.nombre : '--'}</div>
                   </div>
                   <br/>
                   </div>
                   </div>`;

        buffer += `<div class="card mb-4 shadow-sm">
                   <h4 class="card-header">Usos</h4>
                   <div class="card-body table-responsive">
                   <table id="table-usos-factura" class="table table-striped table-bordered table-hover mb-0">
                   <thead class="thead-dark">
                   <tr class="success">
                   <th>#</th>
                   <th>Toneladas</th>
                   <th>Estímulo por tonelada</th>
                   <th>Estímulo total</th>
                   <th>Datos bancarios</th>
                   <th>Acciones</th>
                   </tr>
                   </thead>
                   <tbody>`;


        for (var i = 0; i < usos.length; i++) {
            var uso = usos[i];
            var comentario = uso.comentarioEstatus ? true : false;
            buffer += `<tr>
                       <td class="text-center">${i + 1}</td>
                       <td class="text-right">${uso.toneladas}</td>
                       <td class="text-right">${uso.estimuloTonelada}</td>
                       <td class="text-right">${uso.estimuloTotal}</td>
                       <td >${uso.banco.nombre + ' - ' + uso.clabe} </td>
                       <td class="text-center"><button id="duplicar-uso-${uso.uuid}"
                       class="btn btn-primary btn-sm duplicar-uso" title="Duplicar"><i class="fas fa-plus"></i></button>
                       ${comentario ?
                       '<button id="mostrar-comentario-' + uso.comentarioEstatus + '" \n\
                       class="btn btn-secondary btn-sm mostrar-comentario" title="Comentario"><i class="fas fa-comment-alt"></i></button>'
                       : '<button disabled="disabled" class="btn btn-secondary btn-sm mostrar-comentario" title="Comentario"><i class="fas fa-comment-alt"></i></button>'}
                       </td>`;
        }
        return buffer;
    };

    utils.cargaCatalogos = () => {
        $.ajax({
            type: 'GET',
            url: '/cultivos/resources/paginas/uso-factura/',
            dataType: 'json'
        }).done(function (response) {
            data.bancos = response.bancos;
            data.tiposUso = response.tiposUsoFactura;
            $('#banco-productor').actualizaCombo(data.bancos);
            $('#tipo-uso').actualizaCombo(data.tiposUso);
        }).fail(function () {
            alert('Error: Al descargar los recursos de la página.');
        });
    };

    utils.calculaEstimuloTotal = () => {
        var toneladas = parseFloat($('#toneladas').val());
        var estimulo = parseFloat($('#estimulo-tonelada').val());
        var estimuloTotal = toneladas * estimulo;
        $('#estimulo-total').val(!isNaN(estimuloTotal) && estimuloTotal !== Infinity ? estimuloTotal.toFixed(3) : '');
    };

    utils.cierraModal = () => {
        $('#form-duplicar-uso input.valid-field').limpiaErrores().val('');
        $('#form-duplicar-uso select.valid-field').limpiaErrores().val('0');
        $('#duplicar-uso-modal').modal('hide');
    };
})(jQuery);