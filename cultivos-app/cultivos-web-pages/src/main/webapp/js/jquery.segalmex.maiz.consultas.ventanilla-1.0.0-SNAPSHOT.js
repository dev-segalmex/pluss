/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.maiz.consultas.ventanilla');
    var data = {};
    var handlers = {};
    var utils = {};

    $.segalmex.maiz.consultas.ventanilla.init = function (params) {
        utils.inicializaValidaciones();
        utils.cargaCatalogos();
        $('#button-buscar').click(handlers.busca);
        $('#button-limpiar').click(utils.limpiar);
        $('#button-regresar-resultados').click(handlers.regresaResultados);
        $('#button-regresar').click(utils.regresa);
    };


    utils.inicializaValidaciones = function () {
        $(".valid-field").configura();
        $('#fecha-inicio-registro,#fecha-fin-registro').configura({
            type: 'date',
            required: false
        });
        $('#fecha-inicio-registro,#fecha-fin-registro').datepicker({
            language: "es",
            autoclose: true,
            forceParse: false,
            format: 'dd/mm/yyyy',
            yearRange: "c-10:c"
        });
        $('#rfc').configura({
            type: 'rfc',
            required: false
        });

        $('#estatus').configura({
            required: false
        });

        $('#ciclo').configura({
            required: true
        });

        $('.valid-field').validacion();
    };

    utils.limpiar = function () {
        $('.valid-field').val('').limpiaErrores();
        $('#resultados-busqueda').hide();
        $('#cargando-resultados').hide();
        $('#estatus').val('0');
        $('#ciclo').val('0');
    };

    handlers.regresaResultados = function (e) {
        e.preventDefault();
        $('#detalle-elemento').hide();
        $('#datos-busqueda-form,#resultados-busqueda').show();
    };

    handlers.busca = function (e) {
        e.preventDefault();
        $(e.target).html('Buscando...').prop('disabled', true);
        $('#resultados-busqueda').hide();

        var errores = [];
        $('.valid-field').valida(errores, false);
        $.segalmex.validaFechaInicialContraFinal(errores, 'fecha-inicio-registro', 'fecha-fin-registro');
        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            $(e.target).html('Buscar').prop('disabled', false);
            $('#cargando-resultados').hide();
            return;
        }
        var datos = {
            rfc: $('#rfc').val(),
            fechaInicio: $('#fecha-inicio-registro').val(),
            fechaFin: $('#fecha-fin-registro').val(),
            estatus: $('#estatus').val(),
            ciclo: $('#ciclo').val()
        };
        $('#cargando-resultados').show();
        for (var prop in datos) {
            if (datos[prop] === '') {
                delete datos[prop];
            }
        }
        if (datos.fechaInicio) {
            datos.fechaInicio = $.segalmex.date.fechaToIso(datos.fechaInicio);
        }
        if (datos.fechaFin) {
            datos.fechaFin = $.segalmex.date.fechaToIso(datos.fechaFin);
        }
        if (datos.estatus === '0') {
            delete datos.estatus;
        }
        if (datos.ciclo === '0') {
            delete datos.ciclo;
        }
        data.params = datos;
        $('#button-exportar').attr('href', '/cultivos/resources/empresas/csv/resultados.xlsx?' + $.param(data.params));
        $.ajax({
            url: '/cultivos/resources/empresas/',
            type: 'GET',
            data: datos,
            dataType: 'json'
        }).done(function (response) {
            $(e.target).html('Buscar').prop('disabled', false);
            $('#button-limpiar').prop('disabled', false);
            $('#cargando-resultados').hide();
            $('#data').html(utils.creaDatatable(response));
            $('#table-resultados-busqueda').on('click', 'a.link-uuid', handlers.muestraDetalle);
            $('#resultados-busqueda').slideDown();
        }).fail(function () {
            alert('Error: No fue posible realizar la consulta.');
            $(e.target).html('Buscar').prop('disabled', false);
            $('#button-limpiar').prop('disabled', false);
            $('#cargando-resultados').hide();
        });
    };

    utils.creaDatatable = function (resultados) {
        var buffer = [];
        buffer.push('<table id="table-resultados-busqueda"');
        buffer.push(' class="table table-striped" width="100%">');
        buffer.push('<thead class = "table-success">');
        buffer.push('<tr class="success">');
        buffer.push('<th>No.</th>');
        buffer.push('<th>RFC</th>');
        buffer.push('<th>Nombre</th>');
        buffer.push('<th>Estaus</th>');
        buffer.push('<th>Validador</th>');
        buffer.push('<th>Ciclo-Cultivo</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');
        for (var i = 0; i < resultados.length; i++) {
            var resultado = resultados[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td><a class="link-uuid" href="#" id="uuid.' + resultado.uuid + '">');
            buffer.push(resultado.rfc);
            buffer.push('</a></td>');
            buffer.push('<td>');
            buffer.push(resultado.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.estatus.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.usuarioValidador ? resultado.usuarioValidador.nombre : '--');
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.ciclo.nombre + '-' + resultado.cultivo.nombre);
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        if (resultados.length === 0) {
            buffer.push('<tr><td colspan="6" class="text-center">Sin resultados</td></tr>');
        }
        buffer.push('</tbody>');
        buffer.push('</table>');
        buffer.push('<br/>');
        return buffer.join('');
    };

    handlers.muestraDetalle = function (e) {
        e.preventDefault();
        var uuid = e.target.id.split('.')[1];
        $.ajax({
            url: '/cultivos/resources/empresas/' + uuid + '/ventanilla/',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            // Mostramos el detalle
            data.registro = response;
            $('#registro-ventanilla').html($.segalmex.maiz.vista.validacion.empresa.construyeRegistro(response));
            $('a.muestra-ubicacion').click(handlers.muestraUbicacion);
            $('#sucursales-table').on('click', 'a.muestra-ubicacion', handlers.muestraUbicacion);
            $('#detalle-ventanilla').show();
            $('#resultados-busqueda').hide();
            $('#datos-busqueda-form').hide();
        }).fail(function () {
            alert('Error: No fue posible obtener el registro.');
        });
    };

    handlers.muestraUbicacion = function (e) {
        e.preventDefault();
        var ubicacion = $(e.target).html();
        $('#maps').muestraMaps({
            ubicacion: ubicacion
        });
    };

    utils.regresa = function () {
        $('#datos-busqueda-form').show();
        $('#resultados-busqueda').show();
        $('#detalle-ventanilla').hide();
    };

    utils.cargaCatalogos = function () {
        $.ajax({
            url: '/cultivos/resources/paginas/consultas',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            $('#estatus').actualizaCombo(response.estatus, {value: 'clave'});
            $('#ciclo').actualizaCombo(response.ciclos);
        }).fail(function () {
            alert('Error: No se pudo obtener los catálogos de la página.');
        });
    };
})(jQuery);