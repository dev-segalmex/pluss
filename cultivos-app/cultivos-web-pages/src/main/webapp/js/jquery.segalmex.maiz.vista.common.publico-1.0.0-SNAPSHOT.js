(function ($) {
    $.segalmex.namespace('segalmex.maiz.inscripcion.vista');

    var utils = {};

    utils.formatoNombreUsuario = function (nombre) {
        var n = nombre.split('@')[0];
        if (n.length > 4) {
            return n.substring(0, 4) + '...';
        } else {
            return n + '...';
        }
    }

    $.segalmex.maiz.inscripcion.vista.construyeEstatus = function (inscripcion, tipoInscripcion) {
        var buffer = [];
        buffer.push('<div class="card mb-4 shadow-sm">'); // card
        if (tipoInscripcion === 'productor') {
            buffer.push('<h4 class="card-header">Estatus del registro del productor</h4>');
        } else {
            buffer.push('<h4 class="card-header">Estatus del registro de la factura</h4>');
        }
        buffer.push('<div class="card-body">'); // card-body

        buffer.push('<h3 class="card-title">' + inscripcion.estatus.nombre + '</h3>');

        if (inscripcion.comentarioEstatus) {
            buffer.push('<div class="row">'); // row
            buffer.push('<div class="col-md-12">');
            buffer.push('<strong>Comentario:</strong><br/>');
            buffer.push(inscripcion.comentarioEstatus);
            buffer.push('</div>');
            buffer.push('</div>');
        }

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-12">');
        buffer.push('<strong>Descripción:</strong><br/>');
        buffer.push(utils.agregaDescripcionEstatus(inscripcion.estatus.clave, tipoInscripcion));
        buffer.push('</div>');
        buffer.push('</div>'); // end-row

        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card

        return buffer.join('\n');
    }

    $.segalmex.maiz.inscripcion.vista.construyeHistorial = function (historial) {
        var buffer = [];
        buffer.push('<div class="card mb-4 shadow-sm">'); // card
        buffer.push('<h4 class="card-header">Historial</h4>');

        buffer.push('<div class="card-body">'); // card-body
        buffer.push('<div class="table-responsive">')
        buffer.push('<table class="table table-striped table-bordered table-hover">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<th>#</th>');
        buffer.push('<th>Tipo</th>');
        buffer.push('<th>Fecha inicio</th>');
        buffer.push('<th>Fecha fin</th>');
        buffer.push('</thead>');

        buffer.push('<tbody>');
        if (historial && historial.length > 0) {
            buffer.push($.segalmex.maiz.inscripcion.vista.construyeRenglonesHistorial(historial));
        } else {
            buffer.push('<tr><td colspan="6">Sin historial</td></tr>')
        }
        buffer.push('</tbody>');
        buffer.push('</table>');
        buffer.push('</div>')
        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card

        return buffer.join('\n');
    }

    $.segalmex.maiz.inscripcion.vista.construyeRenglonesHistorial = function (historial) {
        var buffer = [];
        for (var i = 0; i < historial.length; i++) {
            var h = historial[i];

            buffer.push('<tr>');
            buffer.push('<td class="text-right">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(h.tipo.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push($.segalmex.date.isoToFechaHora(h.fechaCreacion));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(h.fechaFinaliza ? $.segalmex.date.isoToFechaHora(h.fechaFinaliza) : '--');
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        return buffer.join('\n');
    }

    $.segalmex.maiz.inscripcion.vista.construyeDatosCapturados = function (datos) {
        if (!datos) {
            return '';
        }

        var visibles = datos;
        var publicos = [];
        for (var i = 0; i < datos.length; i++) {
            var dato = datos[i];
            if (dato.correcto !== undefined && dato.correcto === false) {
                publicos.push(dato);
            }
        }
        visibles = publicos;

        if (visibles.length === 0) {
            return '';
        }

        var buffer = [];
        buffer.push('<div class="card mb-4 shadow-sm">'); // card
        buffer.push('<h4 class="card-header">Datos capturados</h4>');

        buffer.push('<div class="card-body">'); // card-body
        buffer.push('<div class="table-responsive">');
        buffer.push('<table class="table table-striped table-bordered table-hover">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<th>#</th>');
        buffer.push('<th>Campo</th>');
        buffer.push('<th>Valor</th>');
        buffer.push('<th>Estatus</th>');
        buffer.push('<th>Comentario</th>');
        buffer.push('</thead>');

        buffer.push('<tbody>');
        buffer.push($.segalmex.maiz.inscripcion.vista.construyeRenglonesDatosCapturados(visibles));
        buffer.push('</tbody>');
        buffer.push('</table>');
        buffer.push('</div>');
        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card

        return buffer.join('\n');
    };

    $.segalmex.maiz.inscripcion.vista.construyeRenglonesDatosCapturados = function (datos) {
        var buffer = [];

        for (var i = 0; i < datos.length; i++) {
            var dato = datos[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-right">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(dato.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(dato.valorTexto ? dato.valorTexto : dato.valor);
            buffer.push('</td>');
            buffer.push('<td class="text-center">');
            buffer.push(dato.correcto
                    ? '<i class="fas fa-check-circle text-success"></i>'
                    : (dato.correcto !== undefined ? '<i class="fas fa-times-circle text-danger"></i>' : '--'));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(dato.comentario ? dato.comentario : '--');
            buffer.push('</td>');
            buffer.push('</tr>');
        }

        return buffer.join('\n');
    }


    $.segalmex.maiz.inscripcion.vista.construyeComentarios = function (comentarios, usuario) {
        if (!comentarios) {
            return '';
        }

        var buffer = [];
        buffer.push('<div class="card mb-4 shadow-sm">'); // card
        buffer.push('<h4 class="card-header">Comentarios</h4>');

        buffer.push('<div class="card-body">'); // card-body
        buffer.push($.segalmex.maiz.inscripcion.vista.construyeToastComentarios(comentarios, usuario.nombre));
        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card

        return buffer.join('\n');
    }

    $.segalmex.maiz.inscripcion.vista.construyeToastComentarios = function (comentarios, nombreUsuario) {
        var buffer = [];
        for (var i = 0; i < comentarios.length; i++) {
            var comentario = comentarios[i];
            buffer.push('<div class="mb-2 clearfix">');
            buffer.push('<div role="alert" aria-live="assertive" aria-atomic="true" class="toast show' + (comentario.usuario.nombre === nombreUsuario ? ' float-right' : '') + '" data-autohide="false">');
            buffer.push('<div class="toast-header">');
            buffer.push('<svg class="bd-placeholder-img rounded mr-2" width="20" height="20" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img"><rect width="100%" height="100%" fill="' + (comentario.usuario.nombre === nombreUsuario ? '#13322B' : '#007aff') + '"></rect></svg>');
            buffer.push('<strong class="mr-auto">' + utils.formatoNombreUsuario(comentario.usuario.nombre) + '</strong>');
            buffer.push('<small>' + $.segalmex.date.isoToFecha(comentario.fechaCreacion) + '</small>');
            buffer.push('</div>');
            buffer.push('<div class="toast-body">');
            buffer.push('<div><strong>En ' + utils.colocaAcento(comentario.tipo) + ':</strong></div><div>');
            buffer.push(comentario.contenido);
            buffer.push('</div>');
            buffer.push('</div>');
            buffer.push('</div>');
            buffer.push('</div>');
        }

        return buffer.join('\n');
    }

    utils.colocaAcento = function (tipo) {
        switch (tipo) {
            case 'asignacion':
                return 'asignación';
                break;
            case 'validacion':
                return 'validación';
                break;
            case 'documentacion':
                return 'documentación';
                break;
            case 'revalidacion':
                return 'revalidación';
                break;
                break;
            case 'correccion':
                return 'corrección';
                break;
            case 'edicion':
                return 'edición';
                break;
            case 'supervicion':
                return 'supervición';
                break;
            default:
                return tipo;

        }
        ;
    };



    utils.agregaDescripcionEstatus = function (estatus, tipoInscripcion) {
        switch (estatus) {
            case 'pendiente':
                return tipoInscripcion === 'productor'
                        ? 'La documentación se ha comenzado a subir a sistema, pero aún no se ha completado la carga'
                            + ' y el registro del productor firmado. En el momento en el que la ventanilla termine el registro,'
                            + ' el área de validación lo visualizará en estatus NUEVO.'
                        : '--';
                break;
            case 'nuevo':
                return tipoInscripcion === 'productor'
                        ? 'Existe información y documentación del registro del productor,'
                            + ' subida en el sistema, y está formado el expediente para ser asignado y revisado por algún validador.'
                        : 'El registro de la compra o datos de la factura del productor se ha completado en plataforma.';
                break;
            case 'validacion':
                return tipoInscripcion === 'productor'
                        ? 'El folio del productor ha sido asignado a algún validador para la revisión de su expediente,'
                            + ' por lo que iniciará el cotejo de datos y documentos.'
                        : 'La factura capturada ha sido asignada a un validador. Está lista para ser revisada y cotejada en formato PDF y XML.';
                break;
            case 'correcto':
            case 'positiva':
                return tipoInscripcion === 'productor'
                        ? 'El expediente del productor ha sido revisado y se observa que fue debidamente completado,'
                            + ' los documentos son correctos y legibles; por lo tanto, puede continuar con la carga de compras.'
                        : 'La compra ha sido revisada y cumple con los requerimientos, la información es correcta y consistente.'
                            + ' La factura tiene los elementos de cotejo correctos.'
                            + ' Por lo tanto, inicia el proceso de dispersión del incentivo.';
                break;
            case 'correccion':
                return tipoInscripcion === 'productor'
                        ? 'El expediente fue revisado, pero NO ha sido debidamente completada la información solicitada,'
                                +' y es necesario que se atiendan las observaciones que desde el sistema pueden revisarse;'
                                +' y corregir por parte de la ventanilla y el productor. Una vez atendidas las observaciones,'
                                +' el expediente pasará a revalidación y podrá continuar con un nuevo proceso de revisión.'
                        : 'La compra fue revisada, pero <strong>no fue debidamente completada</strong> y es necesario que se atiendan'
                                +' las observaciones realizadas por el área (el productor debe apoyarse de su ventanilla).'
                                +' Una vez atendidas las observaciones, la factura deberá enviarse nuevamente para su revisión.';
                break;
            case 'solventada':
                return tipoInscripcion === 'productor'
                        ? 'La ventanilla atendió las observaciones realizadas por validación al expediente.'
                                +' Si las observaciones fueron atendidas en su <strong>totalidad y de manera correcta</strong> pasará'
                                +' a validación positiva. </br>'
                           +'De no ser así, regresará a estatus de <strong>corrección</strong> hasta que las cumpla de forma completa '
                           +'y correcta con la información solicitada. En este momento, el expediente se encuentra formado '
                           +'en el área de validación para la revisión.'
                        : 'La ventanilla atendió las observaciones realizadas por validación a la compra.'
                                +' Si las observaciones son atendidas en su <strong>totalidad y de manera correcta</strong> pasará '
                                +'a validación positiva.'
                            +'De lo contrario, regresará a estatus de <strong>corrección</strong> hasta que se atiendan las observaciones '
                            +'de forma completa y correcta. En este caso, la compra se encuentra formada, en el área de '
                            +'validación para su revisión.';
                break;
            case 'supervision':
                return tipoInscripcion === 'productor'
                        ? 'El folio queda en espera para asignar otro tipo de estatus ya que se ha identificado alguna '
                                +'diferencia entre la información requerida y la subida al sistema. </br>'
                            +'En este caso se le solicitará al productor documentación que debe ser enviada '
                            +'a oficinas de SEGALMEX en formatos originales, para cotejo de firmas, sellos, '
                            +'identificaciones, etc. (Se anexará de forma clara el comentario al documento a enviar '
                            +'y la dirección en la cual será recibida).'
                        : '--';
                break;
            case 'no-elegible':
            case 'cancelada':
                return tipoInscripcion === 'productor'
                        ? 'El expediente presenta alguna situación como: no cumple con los criterios de elegibilidad del programa, '
                                +'duplicidad de registro, incumplimiento de los requisitos, abandono del trámite y/o solicitud '
                                +'de cancelación explicita por parte del productor.'
                        : 'La compra está cancelada, esto se debe a una de las siguientes causas: '
                                +'<ol>'
                                    +'<li>La factura del productor se encuentra duplicada y previamente ya se ha subido a sistema por alguien más. </li>'
                                    +'<li>La factura ya rebasó el máximo de toneladas permitidas de acuerdo a su superficie y rendimientos registrados por el productor.</li>'
                                    +'<li>La factura no se encuentra vigente en los registros del SAT.</li>'
                                +'</ol> ';
                break;
            case 'pendiente-revision':
                return tipoInscripcion === 'productor'
                        ? 'El expediente se encuentra en espera de revisión hasta que exista contacto directo con el '
                                +'productor registrado, para realizar el cotejo de la información referente a los datos '
                                +'del registro. El productor debe ponerse en contacto con SEGALMEX a las líneas de atención telefónica.'
                        : '--';
                break;
            default:
                return '--';
        }
        ;
    };



    $.segalmex.maiz.inscripcion.vista.generaMensajeExcepcion = function (response) {
        var texto = "";
        if (response.motivos) {
            $.each(response.motivos, function () {
                texto += "\t• " + this + "\n";
            });
        }
        var mensaje = "";
        if (texto === "") {
            mensaje = response.mensaje;
        } else {
            mensaje = response.mensaje + "\n\n" + texto;
        }
        return mensaje;
    };

})(jQuery);