/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {

    $.segalmex.namespace('segalmex.cultivos.seguimiento.productor.publico');

    var handlers = {};
    var utils = {};

    $.segalmex.cultivos.seguimiento.productor.publico.init = function (params) {
        utils.inicializaValidaciones();
        $('#button-buscar').click(handlers.busca);
        $('#button-limpiar').click(utils.limpiar);
        $('#button-regresar').click(handlers.regresa);
        $('#button-regresar-factura').click(handlers.regresaFactura);
    };

    utils.inicializaValidaciones = function () {
        $('#folio').configura({
            pattern: /^\d{1,6}$/,
            minlength: 1,
            maxlength: 6,
            required: true
        });
        $('#curp').configura({
            type: 'curp',
            required: true
        });

        $('#rfc-productor').configura({
            type: 'rfc',
            required: true
        });

        $('.valid-field').validacion();
    };

    utils.limpiar = function (e) {
        e.preventDefault();
        $('#folio,#curp,#rfc-productor').val('').limpiaErrores();
    };

    handlers.regresaFactura = function (e) {
        e.preventDefault();
        $('#detalle-elemento').show();
        $('#detalle-elemento-facturas').hide();
    };

    handlers.busca = function (e) {
        e.preventDefault();
        $(e.target).html('Buscando...').prop('disabled', true);
        var errores = [];
        $('.valid-field').valida(errores, false);
        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            $(e.target).html('Buscar').prop('disabled', false);
            return;
        }

        var datos = {
            folio: $('#folio').val(),
            curp: $('#curp').val(),
            rfcProductor: $('#rfc-productor').val()
        };
        for (var prop in datos) {
            if (datos[prop] === '') {
                delete datos[prop];
            }
        }

        $.ajaxq('publicQueue', {
            url: '/cultivos/resources/productores/publico/inscripcion/',
            type: 'GET',
            data: datos,
            dataType: 'json'
        }).done(function (response) {
            $(e.target).html('Buscar').prop('disabled', false);
            $('#button-limpiar').prop('disabled', false);
            $('#div-avisos').hide();
            handlers.muestraDetalle(response.uuid);
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert("No existe registro con los datos capturados.");
            }
            $(e.target).html('Buscar').prop('disabled', false);
            $('#button-limpiar').prop('disabled', false);
        });
    };

    handlers.muestraDetalle = function (uuid) {
        $.ajaxq('publicQueue', {
            url: '/cultivos/resources/productores/publico/inscripcion/' + uuid,
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            // Mostramos el detalle
            $.segalmex.maiz.inscripcion.productor.vista.muestraInscripcion(response, 'detalle-inscripcion-productor', true);
            $('#detalle-tipo-productor').html($.segalmex.maiz.inscripcion.vista.construyeTipoProductor(response.tipoProductor));
            $('#estatus-inscripcion-productor').html($.segalmex.maiz.inscripcion.vista.construyeEstatus(response, 'productor'));
            $('#historial-inscripcion-productor').html($.segalmex.maiz.inscripcion.vista.construyeHistorial(response.historial));
            $('#datos-capturados-inscripcion-productor').html($.segalmex.maiz.inscripcion.vista.construyeDatosCapturados(response.datos, true));
            utils.cargaFacturas(response);
            utils.cargaPagos(response);
            $('#detalle-elemento').show();
            $('#div-filtros').hide();
            $('#btn-descarga-inscripcion').html('');
        }).fail(function () {
            alert('Error: No fue posible obtener el registro.');
        });
    };

    utils.cargaPagos = function (inscripcion) {
        if (inscripcion.productor === undefined) {
            $('#detalle-pagos').html($.segalmex.maiz.vista.seguimiento.productor.creaTablaPagos([]));
            return;
        }
        var datos = {
            uuidProductor: inscripcion.productor.uuid,
            cultivo: inscripcion.cultivo.id,
            ciclo: inscripcion.ciclo.id
        };
        $.ajaxq('publicQueue', {
            url: '/cultivos/resources/facturas/publico/inscripcion/usos-factura/',
            type: 'GET',
            data: datos,
            dataType: 'json'
        }).done(function (response) {
            $('#detalle-pagos').html($.segalmex.maiz.vista.seguimiento.productor.creaTablaPagos(response));
        }).fail(function () {
            alert('Error: No se encontraron usos factura.');
        });
    };

    handlers.regresa = function (e) {
        e.preventDefault();
        $('#detalle-elemento').hide();
        $('#div-filtros').show();
        $('#div-avisos').show();
    };

    utils.cargaFacturas = function (inscripcion) {
        if (inscripcion.productor === undefined) {
            $('#facturas').html($.segalmex.maiz.vista.seguimiento.productor.creaTablaFacturas([]));
            return;
        }
        var datos = {
            uuid: inscripcion.productor.uuid,
            cultivo: inscripcion.cultivo.id,
            ciclo: inscripcion.ciclo.id
        };
        $.ajaxq('publicQueue', {
            url: '/cultivos/resources/facturas/publico/inscripcion/',
            type: 'GET',
            data: datos,
            dataType: 'json'
        }).done(function (response) {
            //Construimos las facturas registradas por el productor
            $('#facturas').html($.segalmex.maiz.vista.seguimiento.productor.creaTablaFacturas(response));
            $('#table-resultados-busqueda-facturas').on('click', 'a.link-facturas', handlers.muestraDetalleFacturas);
        }).fail(function () {
            alert('Error: No se encontraron facturas.');
        });
    };

    handlers.muestraDetalleFacturas = function (e) {
        e.preventDefault();
        var uuid = e.target.id.split('.')[1];
        $.ajaxq('publicQueue', {
            url: '/cultivos/resources/facturas/publico/inscripcion/' + uuid,
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            // Mostramos el detalle
            $.segalmex.maiz.inscripcion.factura.vista.muestraInscripcion(response, 'detalle-inscripcion-factura', true);
            $('#verificacion-inscripcion-factura').html($.segalmex.maiz.inscripcion.factura.vista.muestraVerificaciones(response));
            $('#productor-ciclco-factura').html($.segalmex.maiz.inscripcion.factura.vista.muestraProductorCiclo(response));
            $('#estatus-inscripcion-factura').html($.segalmex.maiz.inscripcion.vista.construyeEstatus(response, 'factura'));
            $('#historial-inscripcion-factura').html($.segalmex.maiz.inscripcion.vista.construyeHistorial(response.historial));
            $('#datos-capturados-inscripcion-factura').html($.segalmex.maiz.inscripcion.vista.construyeDatosCapturados(response.datos));
            //$('#comentarios-inscripcion-factura').html($.segalmex.maiz.inscripcion.vista.construyeComentarios(response.comentarios, $.segalmex.common.pagina.comun.registrado));
            $('#pago-inscripcion-factura').html($.segalmex.maiz.inscripcion.factura.vista.muestraPago(response));
            $('#button-regresar-detalle-seguimineto2').click(handlers.regresaDetalleSeguimiento);
            $('#detalle-elemento').hide();
            $('#div-archivos').html('');
            $('#detalle-elemento-facturas').show();
            $('body,html').animate({scrollTop: 0}, 1);
        }).fail(function () {
            alert('Error: No fue posible obtener el registro.');
        });
    };

    utils.construyeNombreCompleto = function (dp) {
        var buffer = [];
        if (dp.nombre) {
            buffer.push(dp.nombre);
        }
        if (dp.primerApellido) {
            buffer.push(dp.primerApellido);
        }
        if (dp.segundoApellido) {
            buffer.push(dp.segundoApellido);
        }
        return buffer.join(' ');
    };

})(jQuery);