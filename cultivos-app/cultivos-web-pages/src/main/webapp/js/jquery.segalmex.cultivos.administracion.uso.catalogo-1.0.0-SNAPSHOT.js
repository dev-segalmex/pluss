(function ($) {
    $.segalmex.namespace('segalmex.cultivos.administrar.uso.catalogo');
    var utils = {};
    var handlers = {};
    var data = {
    };

    $.segalmex.cultivos.administrar.uso.catalogo.init = function () {
        utils.cargaUsos();
        utils.inicializaValidaciones();
        utils.configuraPantalla();
    };

    utils.configuraPantalla = function () {
        $('#nuevo-button').click(utils.nuevoUso);
        $('#regresar-button').click(utils.regresar);
        $('#guardar-button').click(utils.guarda);
        $('#edicion-button').click(utils.guardaEdicion);
        $('#cierra-edicion-button').click(handlers.cierraModalEdicion);
    };


    handlers.muestraModalEdicion = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('editar-uso-'.length), 10);

        data.uso = $.segalmex.get(data.usos, idx);

        $('#edicion-clase').val(data.uso.clase);
        $('#edicion-clave').val(data.uso.clave);
        $('#edicion-uso').val(data.uso.uso);
        $('#edicion-orden').val(data.uso.orden);

        $('#edit-uso-modal').modal('show');

    };

    handlers.cierraModalEdicion = function () {
        $('#edit-uso-modal').modal('hide');
        $('#edit-uso-modal .valid-field').val('').limpiaErrores();
    };


    utils.guarda = function () {
        $('#div-form-uso .valid-field').limpiaErrores();
        var errores = [];
        $('#div-form-uso .valid-field').valida(errores, true);
        if (errores.length !== 0) {
            return;
        }

        var uso = {
            clase: $('#clase').val(),
            clave: $('#clave').val(),
            uso: $('#uso').val(),
            orden: $('#orden').val()
        };

        $.ajax({
            url: '/cultivos/resources/uso-catalogo/',
            type: 'POST',
            data: JSON.stringify(uso),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            // Guardamos el registro
            alert('Uso guardado con éxito.');
            utils.cargaUsos();
            utils.regresar();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Ocurrió un error al guardar el registro.');
            }
        });
    };

    utils.guardaEdicion = function () {
        $('#edit-uso-modal .valid-field').limpiaErrores();
        var errores = [];
        $('#edit-uso-modal .valid-field').valida(errores, true);
        if (errores.length !== 0) {
            return;
        }

        var uso = {
            id: data.uso.id,
            clase: $('#edicion-clase').val(),
            clave: $('#edicion-clave').val(),
            uso: $('#edicion-uso').val(),
            orden: $('#edicion-orden').val()
        };

        $.ajax({
            url: '/cultivos/resources/uso-catalogo/',
            type: 'PUT',
            data: JSON.stringify(uso),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            // Guardamos el registro
            alert('Uso editado con éxito.');
            utils.cargaUsos();
            utils.regresar();
            handlers.cierraModalEdicion();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Ocurrió un error al guardar el registro.');
            }
        });


    };

    utils.inicializaValidaciones = function () {
        $(".valid-field").configura();

        $('#orden,#edicion-orden').configura({
            type: 'number'
        });

        $('#clave,#edicion-clave,#uso,#edicion-uso,#clase,#edicion-clase').configura({
            minlength: 1,
            maxlength: 2047,
            allowSpace: false,
            textTransform: null,
            pattern: /^[A-Za-z0-9ÑñÁÉÍÓÚáéíóúÄËÏÖÜäëïöü@\.,:\n\-\(\)_" ]*$/
        });

        $('.valid-field').validacion();
    };

    utils.nuevoUso = function () {
        utils.limpiaUso();
        $('#table-usos,#div-nuevo').hide();
        $('#div-form-uso').show();
    };

    utils.regresar = function () {
        $('#table-usos,#div-nuevo').show();
        $('#div-form-uso').hide();
        utils.limpiaUso();
    };

    utils.limpiaUso = function () {
        $('#clave,#clase,#uso,#orden').val('').limpiaErrores();
    };

    utils.cargaUsos = function (e) {
        $.ajax({
            url: '/cultivos/resources/uso-catalogo/',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            data.usos = response;
            var table = utils.creaDataTable(response);
            $('#table-usos').html(table);
            utils.configuraTablaResultados();
            $('#usos-table').on('click', 'button.editar-uso', handlers.muestraModalEdicion);
            $('#usos-table').on('click', 'button.eliminar-uso', utils.eliminaUso);
        }).fail(function () {
            alert('Error: Al cargar los usos.');
        });
    };

    utils.eliminaUso = function (e) {
        if(!confirm("¿Seguro que desea eliminar el uso catálogo?")){
            return ;
        }
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-uso-'.length), 10);
        var uso = {
            id: idx
        };

        $.ajax({
            url: '/cultivos/resources/uso-catalogo',
            type: 'DELETE',
            data: JSON.stringify(uso),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (uso) {
            // Guardamos el registro
            alert('Uso eliminado con éxito.');
            utils.cargaUsos();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Ocurrió un error al eliminar el registro.');
            }
        });


    };

    utils.creaDataTable = function (registros) {
        var buffer = `
            <table id="usos-table" class="table table-bordered table-hover table-striped">
              <thead class="thead-dark">
                <tr>
                  <th>#</th>
                  <th>Uso</th>
                  <th>Clave</th>
                  <th>Clase</th>
                  <th>Acciones</th>
                </tr>
              </thead>
              <tbody>
        `;

        for (var i = 0; i < registros.length; i++) {
            var a = registros[i];
            buffer += `
                <tr>
                    <td>${i + 1}</td>
                    <td>${a.uso}</td>
                    <td>${a.clave}</td>
                    <td><p title="${a.clase}">${utils.getNombreSimpleClase(a.clase)}</p></td>
                <td class="text-center">
                    <button id="editar-uso- ${a.id}
                    " class="btn btn-primary btn-sm editar-uso">
                    <i class="fas fa-pen"></i></button>

                    <button id="eliminar-uso- ${a.id}
                    " class="btn btn-danger btn-sm eliminar-uso">
                    <i class="fa fa-minus-circle"></i></button>

                    </td>

                </tr>
            `;
        }
        buffer += `
            </tbody>
            </table>
        `;

        return buffer;
    };

    utils.configuraTablaResultados = function () {
        var aoColumns = [{"bSortable": true}, {"bSortable": true},
            {"bSortable": true}, {"bSortable": true}, {"bSortable": false}];
        utils.configuraTablaUsos('usos-table', aoColumns);
    };

    utils.configuraTablaUsos = function (tablaId, aoColumns) {
        var table = $('#' + tablaId).DataTable({
            "oLanguage": {
                "sEmptyTable": "No se encontró información para mostrar en la tabla",
                "sSearch": "Buscar ",
                "sZeroRecords": "No se encontraron resultados."
            },
            "bFilter": true,
            "bInfo": false,
            "bPaginate": false,
            "aoColumns": aoColumns,
            "aoColumnDefs": [{"bSortable": false, "aTargets": ["no-sort"]}]
        });
        table.on('order.dt search.dt', function () {
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).order([[1, 'asc']]).draw(false);
        $("#" + tablaId + "_filter input").addClass('input-buscar');
        $(".dataTables_empty").attr("colspan", "100%");
    };

    utils.getNombreSimpleClase = function (nombre) {
        var nombreSimple;
        switch (nombre) {
            case 'mx.gob.segalmex.common.modelo.catalogos.Estado':
                nombreSimple = 'Estados';
                break;
            case 'mx.gob.segalmex.common.modelo.catalogos.TipoDocumento':
                nombreSimple = 'Tipo documento';
                break;
            case 'mx.gob.segalmex.common.modelo.factura.UnidadCfdi':
                nombreSimple = 'Unidad CFDI';
                break;
            case 'mx.gob.segalmex.common.modelo.personas.TipoPersona':
                nombreSimple = 'Tipo persona';
                break;
            case 'mx.gob.segalmex.common.modelo.seguridad.Area':
                nombreSimple = 'Área';
                break;
            case 'mx.gob.segalmex.common.modelo.seguridad.Usuario':
                nombreSimple = 'Usuario';
                break;
            case 'mx.gob.segalmex.granos.modelo.catalogos.CicloAgricola':
                nombreSimple = 'Ciclo agrícola';
                break;
            case 'mx.gob.segalmex.granos.modelo.catalogos.Empresa':
                nombreSimple = 'Empresa';
                break;
            case 'mx.gob.segalmex.granos.modelo.catalogos.Sucursal':
                nombreSimple = 'Sucursal';
                break;
            case 'mx.gob.segalmex.granos.modelo.catalogos.TipoCultivo':
                nombreSimple = 'Tipo cultivo';
                break;
            case 'mx.gob.segalmex.granos.modelo.catalogos.TipoEmpresa':
                nombreSimple = 'Tipo empresa';
                break;
            case 'mx.gob.segalmex.granos.modelo.catalogos.TipoPrecio':
                nombreSimple = 'Tipo precio';
                break;
            case 'mx.gob.segalmex.pluss.cultivos.modelo.contrato.TipoCompradorCobertura':
                nombreSimple = 'Tipo comprador cobertura';
                break;
            default:
                nombreSimple = nombre;
        }
        return nombreSimple;
    };
})(jQuery);