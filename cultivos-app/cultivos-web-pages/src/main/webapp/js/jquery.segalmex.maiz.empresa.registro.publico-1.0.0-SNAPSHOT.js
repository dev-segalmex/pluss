/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.maiz.empresa.registro.publico');
    var data = {
        sucursales: [],
        uuid: null,
        manejoCompra: [],
        manejoVenta: [],
        anexosSucursalAcopio: [],
        anexosSucursalCertificado: [],
        anexosSucursalDomicilio: [],
        anexosSucursalPosesion: [],
        catalogos: null,
        anioBase: 2018
    };
    var handlers = {};
    var utils = {};

    var camposInputEmpresa = [
        '#volumen-actual-almacenamiento', '#superficie-almacenamiento',
        '#email-enlace-empresa', '#tipo-almacenamiento-empresa',
        '#numero-exterior-empresa', '#numero-interior-empresa',
        '#numero-almacenamiento', '#capacidad-almacenamiento',
        '#almacenadora-almacenamiento',
        '#telefono-oficina-empresa', '#calle-empresa',
        '#enlace-empresa', '#celular-enlace-empresa',
        '#nombre-empresa', '#responsable-empresa',
        '#latitud-empresa', '#longitud-empresa',
        '#localidad-empresa', '#cp-empresa'
    ];

    var camposSelectEmpresa = [
        '#tipo-empresa',
        '#estado-empresa',
        '#municipio-empresa',
        '#existe-equipo-empresa',
        '#normas-calidad-empresa',
        '#tipo-posesion-empresa',
        '#aplica-almacenadora-almacenamiento',
        '#aplica-sucursales'
    ];

    $.segalmex.maiz.empresa.registro.publico.init = function (params) {
        utils.cargaCatalogos();

        var token = $.segalmex.getParameterByName('token');
        var decoded = $.base64.atob(token).split('|');
        if (decoded.length === 2 && decoded[0].length === 36 && (decoded[1].length === 12 || decoded[1].length === 13)) {
            data.uuid = decoded[0];
            data.rfc = decoded[1];
            utils.getRegistro(data.rfc, data.uuid);
            $('#nombre-empresa').focus();
        } else {
            alert('Verifique que la URL sea correcta y vuelva a intentar.');
            return;
        }

        utils.inicializaValidaciones();
        $('#estado-sucursal').change(handlers.cambiaEstadoSucursal);
        $('#predios-table').on('click', 'button.eliminar-predio', handlers.eliminaSucursal);
        $('#compras-table').on('click', 'button.eliminar-compra', handlers.eliminaCompra);
        $('#ventas-table').on('click', 'button.eliminar-ventas', handlers.eliminaVenta);
        $('#agregar-sucursal-button').click(handlers.agregaSucursal);
        $('#guardar-button').click(handlers.guarda);
        $('#cerrar-descarga').click(handlers.cierraDescarga);
        $('#aplica-sucursales').change(handlers.cambiaAplicaSucursal);
        $('#aplica-almacenadora-almacenamiento').change(handlers.aplicaAlmacenadora);
        $('#predios-table').on('click', 'a.muestra-ubicacion', handlers.muestraUbicacion);
        $('#estado-uno-compra').change(handlers.cambiaEstadoUnoCompra);
        $('#estado-dos-compra').change(handlers.cambiaEstadoDosCompra);
        $('#estado-uno-venta').change(handlers.cambiaEstadoUnoVenta);
        $('#estado-dos-venta').change(handlers.cambiaEstadoDosVenta);
        $('#estado-empresa').change(handlers.cambiaEstadoEmpresa);

        $('#agregar-compra-button').click(utils.generaCompra);
        $('#agregar-venta-button').click(utils.generaVenta);
        $('input.form-control-file').change(handlers.verificaArchivo);
        $('#inscripcion-datos-capturados').on('click', 'button.btn-comentario', handlers.muestraComentario);
        $('#reenviar-archivos').click(handlers.reenviaArchivos);
    };

    handlers.cambiaAplicaSucursal = function (e) {
        var v = $(e.target).val();
        data.sucursales = [];
        utils.construyeSucursales();
        $('#sucursales-div').hide();
        $('#cantidad-sucursales').prop('disabled', true).val('0');
        switch (v) {
            case 'true':
                $('#sucursales-div').show();
                $('#cantidad-sucursales').prop('disabled', false);
                break;
        }
    };

    handlers.cambiaEstadoEmpresa = function (e) {
        utils.cambiaEstado(e.target, 'municipio-empresa');
    };

    handlers.cambiaEstadoSucursal = function (e) {
        utils.cambiaEstado(e.target, 'municipio-sucursal');
    };

    handlers.cambiaEstadoUnoCompra = function (e) {
        utils.cambiaEstado(e.target, 'municipio-uno-compra');
    };

    handlers.cambiaEstadoUnoVenta = function (e) {
        utils.cambiaEstado(e.target, 'municipio-uno-venta');
    };

    handlers.cambiaEstadoDosCompra = function (e) {
        utils.cambiaEstado(e.target, 'municipio-dos-compra');
    };

    handlers.cambiaEstadoDosVenta = function (e) {
        utils.cambiaEstado(e.target, 'municipio-dos-venta');
    };

    handlers.cambiaEstadoCompraTableDatos = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = id.substring(0, 2);
        utils.cambiaEstado(e.target, idx + 'municipio-compra');
    };

    handlers.cambiaEstadoVentaTableDatos = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = id.substring(0, 2);
        utils.cambiaEstado(e.target, idx + 'municipio-venta');
    };

    handlers.cambiaEstadoSucursalTableDatos = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = id.substring(0, 2);
        utils.cambiaEstado(e.target, idx + 'municipio-sucursal');
    };

    handlers.aplicaAlmacenadora = function (e) {
        var aplica = $(e.target).val();
        $('#almacenadora-almacenamiento').val('').prop('disabled', false);
        switch (aplica) {
            case 'false':
                $('#almacenadora-almacenamiento').val('--').prop('disabled', true);
                break;
        }
    };

    handlers.agregaSucursal = function (e) {
        e.preventDefault();
        $(e.target).prop('disabled', true);
        var errores = [];
        $('#sucursales-div .valid-field').valida(errores, true);
        if (errores.length !== 0) {
            $('#' + errores[0].campo).focus();
            $(e.target).prop('disabled', false);
            return;
        }

        if (utils.validaArchivos('#sucursales-div input.form-control-file:enabled')) {
            $(e.target).prop('disabled', false);
            return;
        }

        var sucursal = {
            nombre: $('#nombre-sucursal').val(),
            domicilio: {
                direccion: $('#calle-sucursal').val() + ', Ext.' + $('#numero-exterior-sucursal').val() + ', C.P. ' + $('#cp-sucursal').val(),
                calle: $('#calle-sucursal').val(),
                numeroExterior: $('#numero-exterior-sucursal').val(),
                numeroInterior: $('#numero-interior-sucursal').val(),
                localidad: $('#localidad-sucursal').val(),
                codigoPostal: $('#cp-sucursal').val(),
                estado: {id: $('#estado-sucursal').val(), nombre: $('#estado-sucursal option:selected').text()},
                municipio: {clave: $('#municipio-sucursal').val(), nombre: $('#municipio-sucursal option:selected').text()}
            },
            latitud: $('#latitud-sucursal').val(),
            longitud: $('#longitud-sucursal').val(),
            nombreEnlace: $('#enlace-sucursal').val(),
            numeroCelularEnlace: $('#celular-enlace-sucursal').val(),
            correoEnlace: $('#email-enlace-sucursal').val()
        };

        data.anexosSucursalAcopio.push(utils.generaAnexo('justificacion-grano-sucursal-pdf', data.sucursales.length));
        data.anexosSucursalCertificado.push(utils.generaAnexo('certificado-bascula-sucursal-pdf', data.sucursales.length));
        data.anexosSucursalDomicilio.push(utils.generaAnexo('comprobane-domicilio-sucursal-pdf', data.sucursales.length));
        data.anexosSucursalPosesion.push(utils.generaAnexo('documento-posesion-sucursal-pdf', data.sucursales.length));
        data.sucursales.push(sucursal);
        utils.limpiaSucursal();
        utils.construyeSucursales();
        $(e.target).prop('disabled', false);
    };

    handlers.muestraComentario = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var clave = id.substring('btn-comentario-'.length);
        $('#comentario-modal .modal-body p').html($('#comentario-' + clave).val());
        $('#comentario-modal').modal('show');
    }

    utils.generaAnexo = function (id, index) {
        var anexo = $('#' + id).clone();
        anexo.attr('id', index + '_' + id);
        return anexo;
    }

    handlers.eliminaSucursal = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-predio-'.length), 10);

        var predios = [];
        var anexosSucursalAcopio = [];
        var anexosSucursalCertificado = [];
        var anexosSucursalDomicilio = [];
        var anexosSucursalPosesion = [];

        for (var i = 0; i < data.sucursales.length; i++) {
            if (i !== idx) {
                predios.push(data.sucursales[i]);
                anexosSucursalAcopio.push(data.anexosSucursalAcopio[i]);
                anexosSucursalCertificado.push(data.anexosSucursalCertificado[i]);
                anexosSucursalDomicilio.push(data.anexosSucursalDomicilio[i]);
                anexosSucursalPosesion.push(data.anexosSucursalPosesion[i]);
            }
        }
        data.sucursales = predios;
        data.anexosSucursalAcopio = anexosSucursalAcopio;
        data.anexosSucursalCertificado = anexosSucursalCertificado;
        data.anexosSucursalDomicilio = anexosSucursalDomicilio;
        data.anexosSucursalPosesion = anexosSucursalPosesion;
        utils.construyeSucursales();
    };

    handlers.muestraUbicacion = function (e) {
        e.preventDefault();
        var ubicacion = $(e.target).html();
        $('#maps').muestraMaps({
            ubicacion: ubicacion
        });
    };

    handlers.guarda = function (e) {
        $(e.target).prop('disabled', true);
        var errores = [];
        $(camposInputEmpresa.join(',')).valida(errores, false);
        $(camposSelectEmpresa.join(',')).valida(errores, false);
        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            $(e.target).prop('disabled', false);
            return;
        }

        if (utils.validaAplicaSucursal()) {
            $(e.target).prop('disabled', false);
            return;
        }

        var manejoGrano = data.manejoCompra.length + data.manejoVenta.length;
        if (manejoGrano === 0) {
            alert('Error: es necesario agregar un actor de compra o venta.');
            $(e.target).prop('disabled', false);
            return;
        }

        $('#sucursales-div input.form-control-file').prop('disabled', true);
        var archivosError = utils.validaArchivos('input.form-control-file:enabled');
        $('#sucursales-div input.form-control-file').prop('disabled', false);
        if (archivosError) {
            $(e.target).prop('disabled', false);
            return;
        }

        var registro = utils.generaEmpresa();
        $.ajax({
            url: '/cultivos/resources/empresas/' + data.rfc + '/registro/' + data.uuid,
            data: JSON.stringify(registro),
            type: 'PUT',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            $('#condiciones-modal').modal('hide');
            $('#descarga-modal').modal('show');
            $('#anexos-table tbody').html(utils.construyeAnexos());
            $('#descarga-modal .modal-footer button.btn').prop('disabled', true);
            utils.subeArchivos();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Ocurrió un error al registrar la ventanilla.');
            }
            $(e.target).prop('disabled', false);
        });
    };

    handlers.reenviaArchivos = function (e) {
        e.preventDefault();
        $('#descarga-modal #anexos-table tbody td.text-center').html('<i class="fas fa-upload"></i>');
        $('#descarga-modal .modal-footer button.btn').prop('disabled', true);
        utils.subeArchivos();
    }

    handlers.cierraDescarga = function (e) {
        $('#guardar-button').html('Guardar').prop('disabled', false);
        $('.estatus-pdf').html('');
        $('#descarga-modal').modal('hide');
    };

    utils.inicializaValidaciones = function () {
        $('.valid-field').configura();
        $('#nombre-empresa,#almacenadora-almacenamiento,#actores-compra,#actores-venta,#nombre-sucursal,#enlace-sucursal').configura({
            type: 'name-moral'
        });
        $('#responsable-empresa,#enlace-empresa').configura({
            type: 'nombre-icao'
        }).configura({textTransform: 'upper'});
        $('#telefono-oficina-empresa,#celular-enlace-empresa,#celular-enlace-sucursal').configura({
            pattern: /^\d{10}$/,
            minlength: 10,
            maxlength: 10
        });
        $('#calle-empresa,#numero-exterior-empresa,#numero-interior-empresa,#localidad-empresa,'
                + '#calle-sucursal,#numero-exterior-sucursal,#numero-interior-sucursal,#localidad-sucursal').configura({
            type: 'name-moral'
        });
        $('#numero-interior-empresa,#numero-interior-sucursal').configura({
            required: false
        });
        $('#cp-empresa,#cp-sucursal').configura({
            pattern: /^\d{5}$/,
            minlength: 5,
            maxlength: 5
        });
        $('#email-enlace-empresa,#email-enlace-sucursal').configura({
            type: 'email'
        });
        $('#capacidad-almacenamiento,#volumen-actual,almacenamiento,#superficie-almacenamiento,'
                + '#cantidad-uno-compra,#cantidad-dos-compra,#cantidad-uno-venta,#cantidad-dos-venta,'
                + '#cantidad-sucursales,#numero-almacenamiento').configura({
            type: 'number',
            min: 0
        });
        $('#capacidad-almacenamiento,#cantidad-sucursales').configura({
            pattern: /^\d{1,5}$/
        });


        $('#latitud-empresa,#longitud-empresa,#latitud-sucursal,#longitud-sucursal').configura({
            type: 'point'
        });
        $('#latitud-sucursal,#latitud-empresa').configura({
            min: 14.5,
            max: 32.5
        });
        $('#longitud-sucursal,#longitud-empresa').configura({
            min: -117.2,
            max: -86.7
        });

        $('#extension-telefono-extra').configura({
            required: false
        });
        $('.valid-field').validacion();
    };

    utils.getRegistro = function (rfc, uuid) {
        $.ajaxq('registroQueue', {
            type: 'GET',
            url: '/cultivos/resources/empresas/' + rfc + '/registro/' + uuid,
            dataType: 'json',
            data: {expand: 3}
        }).done(function (response) {
            $('#rfc').val(response.rfc);
            $('#cultivo').val(response.cultivo.nombre);
            $('#ciclo-agricola').val(response.ciclo.nombre);
            $('#guardar-button').prop('disabled', !response.editable);
            if (response.estatus) {
                $('#registro-empresa-form').remove();
                switch (response.estatus.clave) {
                    case 'correccion':
                        utils.construyeRevalidacion(response);
                        break;
                    default:
                        utils.construyeRegistro(response);
                }
            } else {
                $('#registro-empresa-form').show();
            }
        }).fail(function () {
            alert('Error: No fue posible obtener el registro.');
        });
    };

    utils.construyeRegistro = function (registro) {
        $('#registro-ventanilla').html($.segalmex.maiz.vista.validacion.empresa.construyeRegistro(registro));
        $('a.muestra-ubicacion').click(handlers.muestraUbicacion);
        $('#sucursales-table').on('click', 'a.muestra-ubicacion', handlers.muestraUbicacion);
        $('#div-encabezado').hide();
        $('#detalle-registro').show();
    };

    utils.construyeRevalidacion = function (registro) {
        $('#registro-ventanilla').html($.segalmex.maiz.vista.validacion.empresa.construyeRegistro(registro));
        $('a.muestra-ubicacion').click(handlers.muestraUbicacion);
        $('#sucursales-table').on('click', 'a.muestra-ubicacion', handlers.muestraUbicacion);
        if (registro.datos) {
            $('#inscripcion-datos-capturados tbody').html(utils.creaTablaCaptura(registro.datos));
            utils.configuraTablaCaptura(registro.datos);
            $('#envia-correccion-button').click(handlers.enviaCorreccion);
            $('#table-dato-capturado').show();
        }
        $('#div-encabezado').hide();
        $('#detalle-registro').show();
    };

    utils.creaTablaCaptura = function (datos) {
        var buffer = [];
        var grupo = 'general';
        var indice = 1;
        for (var i = 0; i < datos.length; i++) {
            var dato = datos[i];
            if (dato.grupo === undefined) {
                dato.grupo = 'General';
            }

            if (dato.grupo !== grupo) {
                buffer.push('<tr class="table-secondary"><th colspan="8">' + dato.grupo + '</th></tr>');
                grupo = dato.grupo;
            }
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(indice++);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push('<label for="' + dato.clave + '">' + dato.nombre + '</label>');
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(utils.creaCampoCaptura(dato));
            buffer.push('</td>');
            buffer.push('<td class="text-center">');
            buffer.push(dato.correcto
                    ? '<i class="fas fa-check-circle text-success"></i>'
                    : '<i class="fas fa-times-circle text-danger"></i>');
            buffer.push('</td>');
            buffer.push('<td><div class="form-inline">');
            buffer.push('<input id="comentario-' + dato.clave
                    + '" type="text" class="form-control" readonly="readonly" value="'
                    + (dato.comentario ? dato.comentario : '') + '"/>');
            if (dato.comentario) {
                buffer.push('<button type="button" id="btn-comentario-' + dato.clave + '" class="btn-comentario btn btn-outline-secondary ml-2"><i class="fas fa-comment-alt"></i></button>');
            }
            buffer.push('</div></td>');
            buffer.push('</tr>');
        }

        return buffer.join('\n');
    };

    utils.creaCampoCaptura = function (dato) {
        var buffer = [];

        switch (dato.tipo) {
            case 'boolean':
                buffer.push('<select id="');
                buffer.push(dato.clave);
                buffer.push('" class="form-control valid-field dato-valor">');
                buffer.push('<option value="0">Seleccione</option>');
                buffer.push('<option value="true">Sí</option>');
                buffer.push('<option value="false">No</option>');
                buffer.push('</select>');
                break;
            case 'texto':
                buffer.push('<input id="');
                buffer.push(dato.clave);
                buffer.push('" value="');
                buffer.push(dato.valor);
                buffer.push('" type="text" class="form-control dato-valor valid-field"/>');
                break;
            case 'catalogo':
                buffer.push('<select id="');
                buffer.push(dato.clave);
                buffer.push('" class="form-control valid-field dato-valor">');
                buffer.push('<option value="0">Seleccione</option>');
                buffer.push('</select>');
                break;
            case 'archivo':
                buffer.push('<input id="');
                buffer.push(dato.clave);
                buffer.push('" ');
                if (dato.correcto) {
                    buffer.push('type="text" class="form-control dato-valor" value="');
                    buffer.push(dato.valor);
                    buffer.push('" disabled="disabled" />');
                } else {
                    buffer.push('type="file" class="form-control-file" accept="application/pdf" aria-describedby="' + dato.clave + '-ayuda"/>');
                    buffer.push('<small id="' + dato.clave + '-ayuda" class="form-text text-muted">El archivo no deberá ser mayor a 24 MB (25164800 bytes).</small>');
                }
                break;
        }

        return buffer.join('');
    };

    handlers.verificaTamanoArchivo = function (e) {
        $.segalmex.archivos.verificaArchivo(e, 'pdf', 24 * 1024 * 1024);
    };

    utils.configuraTablaCaptura = function (datos) {
        for (var i = 0; i < datos.length; i++) {
            var dato = datos[i];
            var valores = [];
            var tmp = dato.clave.split('_');
            var clave = tmp.length === 1 ? tmp[0] : tmp[1];
            var params = null;
            switch (clave) {
                case 'nombre-empresa':
                case 'almacenadora-almacenamiento':
                case 'actores-compra':
                case 'actores-venta':
                case 'nombre-sucursal':
                case 'enlace-sucursal':
                    params = {
                        type: 'name-moral'
                    };
                    break;
                case 'tipo-empresa':
                    valores = data.catalogos.tiposSucursal;
                    break;
                case 'responsable-empresa':
                case 'enlace-empresa':
                    params = {
                        type: 'nombre-icao',
                        textTransform: 'upper'
                    };
                    break;
                case 'telefono-oficina-empresa':
                case 'celular-enlace-empresa':
                case 'celular-enlace-sucursal':
                    params = {
                        pattern: /^\d{10}$/,
                        minlength: 10,
                        maxlength: 10
                    };
                    break;
                case 'calle-empresa':
                case 'numero-exterior-empresa':
                case 'numero-interior-empresa':
                case 'localidad-empresa':
                case 'calle-sucursal':
                case 'numero-exterior-sucursal':
                case 'numero-interior-sucursal':
                case 'localidad-sucursal':
                    params = {
                        type: 'name-moral'
                    };
                    break;
                case 'cp-empresa':
                case 'cp-sucursal':
                    params = {
                        pattern: /^\d{5}$/,
                        minlength: 5,
                        maxlength: 5
                    };
                    break;
                case 'estado-empresa':
                    valores = data.catalogos.estados;
                    $('#estado-empresa').change(handlers.cambiaEstadoEmpresa);
                    break;
                case 'estado-compra':
                    valores = data.catalogos.estados;
                    $('#' + dato.clave).change(handlers.cambiaEstadoCompraTableDatos);

                    break;
                case 'estado-venta':
                    valores = data.catalogos.estados;
                    $('#' + dato.clave).change(handlers.cambiaEstadoVentaTableDatos);
                    break;
                case 'estado-sucursal':
                    valores = data.catalogos.estadosPredio;
                    $('#' + dato.clave).change(handlers.cambiaEstadoSucursalTableDatos);
                    break
                case 'municipio-empresa':
                case 'municipio-compra':
                case 'municipio-venta':
                case 'municipio-sucursal':
                    valores = utils.filtraMunicipios(data.catalogos.municipios, dato.valor);
                    break;
                case 'email-enlace-empresa':
                case 'correo-enlace-sucursal':
                    params = {
                        type: 'email'
                    };
                    break;
                case 'existe-equipo-empresa':
                    break;
                case 'normas-calidad-empresa':
                    break;
                case 'tipo-posesion-empresa':
                    valores = data.catalogos.tiposPosesion;
                    break;
                case 'tipo-almacenamiento-empresa':
                    break;
                case 'latitud-empresa':
                case 'latitud-sucursal':
                    params = {
                        type: 'point',
                        min: 14.5,
                        max: 32.5
                    };
                    break;
                case 'longitud-empresa':
                case 'longitud-sucursal':
                    params = {
                        type: 'point',
                        min: -117.2,
                        max: -86.7
                    };
                    break;
                case 'capacidad-almacenamiento':
                case 'volumen-actual-almacenamiento':
                case 'superficie-almacenamiento':
                case 'cantidad-compra':
                case 'cantidad-venta':
                case 'numero-almacenamiento':
                    params = {
                        type: 'number',
                        min: 0
                    };
                    break;
                case 'aplica-almacenadora-almacenamiento':
                    break;
                case 'anio-compra':
                case 'anio-venta':
                    params = {
                        type: 'number',
                        min: 0
                    };
                    break;
            }
            switch (dato.tipo) {
                case 'boolean':
                    $('#' + dato.clave).prop('disabled', dato.correcto).val(dato.valor);
                    break;
                case 'catalogo':
                    $('#' + dato.clave).actualizaCombo(valores, {value: 'clave'}).prop('disabled', dato.correcto).val(dato.valor).configura(params);
                    break;
                case 'texto':
                    $('#' + dato.clave).prop('disabled', dato.correcto).val(dato.valor).configura(params);
                    break;
                case 'archivo':
                    if (dato.correcto) {
                        $('#' + dato.clave).prop('disabled', dato.correcto).val(dato.valor);
                    }
                    break;
            }
        }
        $('#inscripcion-datos-capturados tbody input.valid-field').validacion();
        $('#inscripcion-datos-capturados tbody input.form-control-file').change(handlers.verificaTamanoArchivo);
    };

    utils.filtraMunicipios = function (municipios, clave) {
        var estado = clave.length === 5 ? clave.substring(0, 2) : clave.substring(0, 1);
        var filtrados = [];
        for (var i = 0; i < municipios.length; i++) {
            var m = municipios[i];
            if (m.estado.clave === estado) {
                filtrados.push(m);
            }
        }
        return filtrados;
    };

    utils.validaTablaCaptura = function () {
        $('#inscripcion-datos-capturados tbody input.valid-field').limpiaErrores();
        var errores = [];
        $('#inscripcion-datos-capturados tbody input.valid-field').valida(errores, true);
        return errores.length === 0;
    };

    handlers.enviaCorreccion = function (e) {
        $(e.target).prop('disabled', true);
        var anexos = $('#inscripcion-datos-capturados tbody input.form-control-file:enabled').length > 0 ? true : false;
        if (!utils.validaTablaCaptura()) {
            $(e.target).prop('disabled', false);
            return;
        }
        if (utils.validaArchivos('#inscripcion-datos-capturados tbody input.form-control-file:enabled')) {
            $(e.target).prop('disabled', false);
            return;
        }
        if (anexos) {
            $('#inscripcion-datos-capturados input.form-control-file').each(function () {
                var fd = new FormData();
                var archivo = $(this)[0].files[0];
                fd.append('file', archivo);
                var tipo = this.id.substring(0, this.id.length - '-pdf'.length);
                $.ajaxq('registroQueque', {
                    url: '/cultivos/resources/empresas/' + data.rfc + '/registro/' + data.uuid + '/anexos/' + tipo + '/pdf/',
                    type: 'POST',
                    data: fd,
                    contentType: false,
                    processData: false
                }).done(function (response) {
                }).fail(function () {
                    alert('Error: no fue posible anexar el archivo: ' + tipo + '. ');
                });
            });
        }

        var capturados = utils.getDatosCapturados();

        $.ajaxq('registroQueque', {
            url: '/cultivos/resources/empresas/' + data.uuid + '/correccion/',
            data: JSON.stringify(capturados),
            type: 'PUT',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('El registro ha sido enviado a corrección.');
            utils.getRegistro(data.rfc, data.uuid);
        }).fail(function (jqXHR) {
            alert('Error: No fue posible corregir el registro.');
            $(e.target).prop('disabled', false);
        });
    };

    utils.getDatosCapturados = function () {
        var datos = $('#inscripcion-datos-capturados .dato-valor');
        var dcs = [];
        for (var i = 0; i < datos.length; i++) {
            var dato = datos[i];
            var valor = $(dato).val();
            var clave = dato.id;
            var d = {clave: clave, valor: valor};
            if ($(dato).is('select')) {
                d.valorTexto = $(dato).find('option:selected').text();
            }
            dcs.push(d);
        }
        return dcs;
    };

    utils.cargaCatalogos = function () {
        $.ajaxq('registroQueue', {
            type: 'GET',
            url: '/cultivos/resources/paginas/empresa/registro/',
            dataType: 'json'
        }).done(function (response) {
            data.catalogos = response;
            data.municipios = response.municipios;
            $('#estado-empresa,#estado-uno-compra,#estado-dos-compra,#estado-uno-venta,#estado-dos-venta').actualizaCombo(response.estados);
            $('#estado-sucursal').actualizaCombo(response.estadosPredio);
            $('#tipo-empresa,#tipo-sucursal').actualizaCombo(response.tiposSucursal);
            $('#tipo-posesion-empresa').actualizaCombo(response.tiposPosesion);
        }).fail(function () {
            alert('Error: Al descargar los recursos de la página.');
        });
    };

    utils.cambiaEstado = function (target, idMunicipio) {
        var v = $(target).val();
        var estado = v !== '0' ? $.segalmex.get(data.municipios, v) : {clave: '0'};
        var municipios = [];
        if (estado.clave !== '0') {
            for (var i = 0; i < data.municipios.length; i++) {
                var m = data.municipios[i];
                if (m.estado.id === estado.id) {
                    municipios.push(m);
                }
            }
        }
        $('#' + idMunicipio).actualizaCombo(municipios, {value: 'clave'}).val('0');
    };

    utils.limpiaSucursal = function () {
        $('#sucursales-div input').val('');
        $('#sucursales-div input.valid-field').limpiaErrores();
        $('#sucursales-div select.valid-field').val('0').limpiaErrores();
    };

    utils.construyeSucursales = function () {
        var buffer = [];
        for (var i = 0; i < data.sucursales.length; i++) {
            var s = data.sucursales[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-right">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(s.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(s.domicilio.direccion);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(s.domicilio.localidad);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(s.domicilio.municipio.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(s.domicilio.estado.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push('<a class="muestra-ubicacion" href="#">[' + s.latitud + ',' + s.longitud + ']</a>');
            buffer.push('</td>');
            if (!s.id) {
                buffer.push('<td class="text-center"><button id="eliminar-predio-');
                buffer.push(i);
                buffer.push('" class="btn btn-danger btn-sm eliminar-predio"><i class="fa fa-minus-circle"></i></button></td>');
                buffer.push('</td>');
            } else {
                buffer.push('<td></td>');
            }
            buffer.push('</tr>');
        }
        $('#predios-table tbody').html(buffer.join(''));
    };

    utils.validaArchivos = function (expresion) {
        var files = $(expresion);
        var texto = [];
        var errores = false;
        for (var i = 0; i < files.length; i++) {
            if ($(files[i]).val() === '') {
                errores = true;
                texto.push(' * Seleccione el archivo para: ' + $('label[for=' + files[i].id + ']').html() + '\n');
            }
        }
        if (errores) {
            alert('Verifique lo siguiente:\n\n' + texto.join(''));
        }
        return errores;
    };

    //subir archivos
    utils.subeArchivos = function () {
        $('#sucursales-div input.form-control-file').prop('disabled', true);
        var files = $('input.form-control-file:enabled');
        $('#sucursales-div input.form-control-file').prop('disabled', false);
        var anexos = data.anexosSucursalDomicilio.concat(data.anexosSucursalPosesion).concat(data.anexosSucursalCertificado).concat(data.anexosSucursalAcopio);
        var fds = [];

        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            var id = file.id;
            var tipo = id.substring(0, id.length - '-pdf'.length);

            var fd = new FormData();
            var archivo = $('#' + id)[0].files[0];
            fd.append('file', archivo);
            fds.push({id: id, fd: fd, tipo: tipo});
        }

        for (i = 0; i < anexos.length; i++) {
            var anexo = anexos[i];

            var id = anexo.attr('id');
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            fds.push({id: id, fd: fd, tipo: tipo});
        }

        var adjuntos = 0;
        var pendientes = [];
        utils.procesaArchivos(fds, pendientes, adjuntos);
    };


    utils.procesaArchivos = function (fds, pendientes, adjuntos) {
        for (var i = 0; i < fds.length; i++) {
            (function (dato, ultimo) {
                $.ajaxq('registroQueque', {
                    url: '/cultivos/resources/empresas/' + data.rfc + '/registro/' + data.uuid + '/anexos/' + dato.tipo + '/pdf/',
                    type: 'POST',
                    data: dato.fd,
                    contentType: false,
                    processData: false
                }).done(function () {
                    utils.cargaBarraProgreso(++adjuntos, fds.length);
                    $('#estatus-' + dato.id).html('<i class="fas fa-check-circle text-success"></i> Adjuntado');
                }).fail(function () {
                    pendientes.push(dato);
                    $('#estatus-' + dato.id).html('<i class="fas fa-times-circle text-danger"></i> No adjuntado');
                }).always(function () {
                    if (ultimo) {
                        if (pendientes.length === 0) {
                            $('#cerrar-descarga').prop('disabled', false);
                            $('#descargar-inscripcion-link').removeClass('disabled');
                            utils.getRegistro(data.rfc, data.uuid);
                        } else {
                            $('#reenviar-archivos').prop('disabled', false);
                        }
                    }
                });
            })(fds[i], i === fds.length - 1);
        }
    };

    utils.construyeAnexos = function () {
        $('#sucursales-div input.form-control-file').prop('disabled', true);
        var files = $('input.form-control-file:enabled');
        $('#sucursales-div input.form-control-file').prop('disabled', true);

        var buffer = [];
        var index = 1;
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(index++);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push($('label[for=' + file.id + ']').html());
            buffer.push('</td>');
            buffer.push('<td id="estatus-' + file.id + '" class="text-center">');
            buffer.push('<i class="fas fa-upload"></i>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }

        for (i = 0; i < data.sucursales.length; i++) {
            var anx = data.anexosSucursalDomicilio[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(index++);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push('Comprobante domicilio (PDF) [' + (i + 1) + ']');
            buffer.push('</td>');
            buffer.push('<td id="estatus-' + anx.attr('id') + '" class="text-center">');
            buffer.push('<i class="fas fa-upload"></i>');
            buffer.push('</td>');
            buffer.push('</tr>');

            anx = data.anexosSucursalPosesion[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(index++);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push('Documento de posesión (PDF) [' + (i + 1) + ']');
            buffer.push('</td>');
            buffer.push('<td id="estatus-' + anx.attr('id') + '" class="text-center">');
            buffer.push('<i class="fas fa-upload"></i>');
            buffer.push('</td>');
            buffer.push('</tr>');

            anx = data.anexosSucursalCertificado[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(index++);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push('Certificado de báscula (PDF) [' + (i + 1) + ']');
            buffer.push('</td>');
            buffer.push('<td id="estatus-' + anx.attr('id') + '" class="text-center">');
            buffer.push('<i class="fas fa-upload"></i>');
            buffer.push('</td>');
            buffer.push('</tr>');

            anx = data.anexosSucursalAcopio[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(index++);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push('Justificacion de grano acopiado (PDF) [' + (i + 1) + ']');
            buffer.push('</td>');
            buffer.push('<td id="estatus-' + anx.attr('id') + '" class="text-center">');
            buffer.push('<i class="fas fa-upload"></i>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }

        return buffer.join('\n');
    };

    utils.cargaBarraProgreso = function (actual, total) {
        if (total === 0) {
            $('.progress-bar').css('width', 100 + '%');
            $('.progress-bar').attr('aria-valuenow', 100);
            $('.progress-bar').text('Completado ' + 100 + '%');
        } else {
            var progreso = Math.trunc(actual * 100 / total);
            $('.progress-bar').css('width', progreso + '%');
            $('.progress-bar').attr('aria-valuenow', progreso);
            $('.progress-bar').text('Completado ' + progreso + '%');
        }
    };

    utils.construyeVistaAnexos = function (archivos) {
        if (!archivos || archivos.length === 0) {
            return '';
        }

        var buffer = [];
        buffer.push('<h4>Archivos anexos</h4>');
        buffer.push('<ul>');
        for (var i = 0; i < archivos.length; i++) {
            var archivo = archivos[i];
            buffer.push('<li><a href="/cultivos/resources/archivos/maiz/' + archivo.uuid + '/'
                    + archivo.nombre + '?inline=true" target="_blank">');
            buffer.push(archivo.nombreOriginal);
            buffer.push(' [' + archivo.tipo + '] ');
            buffer.push(' <i class="fas fa-file"></i></a></li>');
        }
        buffer.push('</ul><br/>');
        return buffer.join('');
    };

    utils.validaAplicaSucursal = function () {
        var aplica = $('#aplica-sucursales').val();
        var sucursales = $('#cantidad-sucursales').val();
        var mensaje;
        var valida = false;
        switch (aplica) {
            case 'true':
                if (sucursales === '0') {
                    mensaje = 'Indique el número de sucursales a registrar.';
                    valida = true;
                }
                if (sucursales < data.sucursales.length) {
                    mensaje = 'Error: El número de sucursales es menor que los registrados en la tabla.';
                    valida = true;
                }
                if (sucursales > data.sucursales.length) {
                    mensaje = 'Error: El número de sucursales es mayor que los registrados en la tabla';
                    valida = true;
                }
                break;
        }
        if (valida) {
            alert(mensaje);
        }
        return valida;
    };

    utils.generaEmpresa = function () {
        var empresa = {
            sucursales: data.sucursales,
            nombre: $('#nombre-empresa').val(),
            tipoSucursal: {id: $('#tipo-empresa').val()},
            nombreResponsable: $('#responsable-empresa').val(),
            telefonoOficina: $('#telefono-oficina-empresa').val(),
            extensionAdicional: $('#extension-telefono-extra').val(),
            domicilio: {
                calle: $('#calle-empresa').val(),
                numeroExterior: $('#numero-exterior-empresa').val(),
                numeroInterior: $('#numero-interior-empresa').val(),
                localidad: $('#localidad-empresa').val(),
                codigoPostal: $('#cp-empresa').val(),
                estado: {id: $('#estado-empresa').val()},
                municipio: {clave: $('#municipio-empresa').val()}
            },
            nombreEnlace: $('#enlace-empresa').val(),
            numeroCelularEnlace: $('#celular-enlace-empresa').val(),
            correoEnlace: $('#email-enlace-empresa').val(),

            existeEquipoAnalisis: $('#existe-equipo-empresa').val(),
            aplicaNormasCalidad: $('#normas-calidad-empresa').val(),
            tipoPosesion: {id: $('#tipo-posesion-empresa').val()},
            tipoAlmacenamiento: $('#tipo-almacenamiento-empresa').val(),
            latitud: $('#latitud-empresa').val(),
            longitud: $('#longitud-empresa').val(),
            numeroAlmacenes: $('#numero-almacenamiento').val(),
            capacidad: $('#capacidad-almacenamiento').val(),
            volumenActual: $('#volumen-actual-almacenamiento').val(),
            superficie: $('#superficie-almacenamiento').val(),
            habilitaAlmacenadora: $('#aplica-almacenadora-almacenamiento').val(),
            nombreAlmacenadora: $('#almacenadora-almacenamiento').val(),
            datosComercializacion: [].concat(data.manejoCompra).concat(data.manejoVenta)
        };
        return empresa;
    };

    utils.generaCompra = function () {
        var errores = [];
        $('#compra-form .valid-field').limpiaErrores().valida(errores, true);
        if (errores.length > 0) {
            return;
        }
        var table = 'compras-table';
        var idButton = 'compra';
        var datosUno = {
            tipo: 'compra',
            anio: data.anioBase,
            nombreActor: $('#actores-compra').val(),
            cantidad: $('#cantidad-uno-compra').val(),
            estado: {id: $('#estado-uno-compra').val(), nombre: $('#estado-uno-compra option:selected').text()},
            municipio: {clave: $('#municipio-uno-compra').val(), nombre: $('#municipio-uno-compra option:selected').text()}
        };
        data.manejoCompra.push(datosUno);
        var datosDos = {
            tipo: 'compra',
            anio: data.anioBase + 1,
            nombreActor: $('#actores-compra').val(),
            cantidad: $('#cantidad-dos-compra').val(),
            estado: {id: $('#estado-dos-compra').val(), nombre: $('#estado-dos-compra option:selected').text()},
            municipio: {clave: $('#municipio-dos-compra').val(), nombre: $('#municipio-dos-compra option:selected').text()}
        };
        data.manejoCompra.push(datosDos);
        utils.agregaManejoGrano(data.manejoCompra, table, idButton);
        $('#compra-form input.valid-field').val('');
        $('#compra-form select.valid-field').val('0').change();
        $('#compra-form .valid-field').limpiaErrores();
    };

    utils.generaVenta = function () {
        var errores = [];
        $('#venta-form .valid-field').limpiaErrores().valida(errores, true);
        if (errores.length > 0) {
            return;
        }
        var table = 'ventas-table';
        var idButton = 'ventas';
        var datosUno = {
            tipo: 'venta',
            anio: data.anioBase,
            nombreActor: $('#actores-venta').val(),
            cantidad: $('#cantidad-uno-venta').val(),
            estado: {id: $('#estado-uno-venta').val(), nombre: $('#estado-uno-venta option:selected').text()},
            municipio: {clave: $('#municipio-uno-venta').val(), nombre: $('#municipio-uno-venta option:selected').text()}
        };
        data.manejoVenta.push(datosUno);
        var datosDos = {
            tipo: 'venta',
            anio: data.anioBase + 1,
            nombreActor: $('#actores-venta').val(),
            cantidad: $('#cantidad-dos-venta').val(),
            estado: {id: $('#estado-dos-venta').val(), nombre: $('#estado-dos-venta option:selected').text()},
            municipio: {clave: $('#municipio-dos-venta').val(), nombre: $('#municipio-dos-venta option:selected').text()}
        };
        data.manejoVenta.push(datosDos);
        utils.agregaManejoGrano(data.manejoVenta, table, idButton);
        $('#venta-form input.valid-field').val('');
        $('#venta-form select.valid-field').val('0').change();
        $('#venta-form .valid-field').limpiaErrores();
    };

    utils.agregaManejoGrano = function (manejo, tabla, idButton) {
        var buffer = [];
        for (var i = 0; i < manejo.length; i++) {
            var g = manejo[i];
            buffer.push('<tr>');
            if (i % 2 === 0) {
                buffer.push('<td class="align-middle" rowspan="2">');
                buffer.push((i + 2) / 2);
                buffer.push('</td>');
                buffer.push('<td class="align-middle" rowspan="2">');
                buffer.push(g.nombreActor);
                buffer.push('</td>');
            }
            buffer.push('<td>');
            buffer.push(g.estado.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(g.municipio.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(g.anio);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(g.cantidad);
            buffer.push('</td>');
            if (i % 2 === 0) {
                buffer.push('<td rowspan="2" class="text-center align-middle"><button id="eliminar-' + idButton + '-');
                buffer.push(i);
                buffer.push('" class="btn btn-danger btn-sm eliminar-' + idButton + '"><i class="fa fa-minus-circle"></i></button></td>');
                buffer.push('</td>');
            }
            buffer.push('</tr>');
        }
        $('#' + tabla + ' tbody').html(buffer.join(''));
    };

    handlers.eliminaCompra = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-compra-'.length), 10);
        var compras = [];
        for (var i = 0; i < data.manejoCompra.length; i++) {
            if (idx === i) {
                continue;
            }
            if (idx + 1 === i) {
                continue;
            }
            compras.push(data.manejoCompra[i]);
        }
        data.manejoCompra = compras;
        var table = 'compras-table';
        var idButton = 'compra';
        utils.agregaManejoGrano(data.manejoCompra, table, idButton);
    };

    handlers.eliminaVenta = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-ventas-'.length), 10);
        var ventas = [];
        for (var i = 0; i < data.manejoVenta.length; i++) {
            if (idx === i) {
                continue;
            }
            if (idx + 1 === i) {
                continue;
            }
            ventas.push(data.manejoVenta[i]);
        }
        data.manejoVenta = ventas;
        var table = 'ventas-table';
        var idButton = 'ventas';
        utils.agregaManejoGrano(data.manejoVenta, table, idButton);
    };

    handlers.verificaArchivo = function (e) {
        $.segalmex.archivos.verificaArchivo(e, 'pdf', 24 * 1024 * 1024);
    };
})(jQuery);