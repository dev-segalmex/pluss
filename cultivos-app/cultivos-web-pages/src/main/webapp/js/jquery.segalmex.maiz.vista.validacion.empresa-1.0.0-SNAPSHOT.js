(function ($) {
    $.segalmex.namespace('segalmex.maiz.vista.validacion.empresa');

    var utils = {};
    var handlers = {};

    $.segalmex.maiz.vista.validacion.empresa.construyeRegistro = function (registro, print) {
        if (print === undefined) {
            print = false;
        }
        var buffer = '';
        buffer = `
                <div class="card mb-4 shadow-sm">
                <h4 class="card-header">Detalle del registro de ventanilla</h4>
                <div class="card-body">
                <h3 class="card-title">Folio:${ registro.id}</h3>
                <h4>Programa de Precios de Garantía a Productos Alimentarios Básicos</h4>
                <div class="row">
                <div class="col-md-4"><strong>Cultivo:</strong><br/>${registro.cultivo.nombre}</div>
                <div class="col-md-4"><strong>Ciclo agrícola:</strong><br/>${registro.ciclo.nombre}</div>
                </div>
                <br/>

                <div class="row">
                <div class="col-md-4"><strong>RFC:</strong><br/>${registro.rfc}</div>
                <div class="col-md-4"><strong>Nombre:</strong><br/>${registro.nombre}</div>
                </div>

                <div class="row">
                <div class="col-md-4"><strong>Tipo:</strong><br/>${registro.tipoSucursal.nombre}</div>
                <div class="col-md-4"><strong>Fecha de registro :</strong><br/>${$.segalmex.date.isoToFecha(registro.fechaCreacion)}</div>
                </div>

                <div class="row">
                <div class="col-md-4"><strong>Nombre del responsable:</strong><br/>${registro.nombreResponsable}</div>
                <div class="col-md-4"><strong>Teléfono oficina:</strong><br/>${registro.telefonoOficina}</div>
                <div class="col-md-4"><strong>Extensión/Teléfono adicional:</strong><br/>${registro.extensionAdicional ? registro.extensionAdicional : '--'}</div>

                </div>
                <br/>

                <h4>Domicilio Fiscal</h4>
                <div class="row">
                <div class="col-md-4"><strong>Calle:</strong><br/>${registro.domicilio.calle}</div>
                <div class="col-md-4"><strong>Número exterior:</strong><br/>${registro.domicilio.numeroExterior}</div>
                <div class="col-md-4"><strong> Número interior :</strong><br/>${registro.domicilio.numeroInterior}</div>
                </div>

                <div class="row">
                <div class="col-md-4"><strong>Localidad:</strong><br/>${registro.domicilio.localidad}</div>
                <div class="col-md-4"><strong>Código postal:</strong><br/>${registro.domicilio.codigoPostal}</div>
                </div>

                <div class="row">
                <div class="col-md-4"><strong>Estado:</strong><br/>${registro.domicilio.estado.nombre}</div>
                <div class="col-md-4"><strong>Municipio:</strong><br/>${registro.domicilio.municipio.nombre}</div>
                </div>
                <br/>

                <h4>Enlace con SEGALMEX</h4>
                <div class="row">
                <div class="col-md-4"><strong>Nombre del enlace:</strong><br/>${registro.nombreEnlace}</div>
                <div class="col-md-4"><strong>No. de celular:</strong><br/>${registro.numeroCelularEnlace}</div>
                <div class="col-md-4"><strong>Correo electrónico :</strong><br/>${registro.correoEnlace}</div>
                </div>
                <br/>

                <h4>Infraestructura de la empresa</h4>
                <div class="row">
                <div class="col-md-4"><strong>¿Existe equipo de análisis en operación?:</strong><br/>${registro.existeEquipoAnalisis ? 'Sí' : 'No'}</div>
                <div class="col-md-4"><strong>¿Aplica normas de calidad?:</strong><br/>${registro.aplicaNormasCalidad ? 'Sí' : 'No'}</div>
                </div>

                <div class="row">
                <div class="col-md-4"><strong>Tipo de posesión:</strong><br/>${registro.tipoPosesion.nombre}</div>
                <div class="col-md-4"><strong>Tipo de almacenamiento:</strong><br/>${registro.tipoAlmacenamiento}</div>
                </div>
                <br/>

                <h4>Coordenadas geográficas</h4>
                <div class="row">
                <div class="col-md-4"><strong>Latitud y longitud:</strong><br/>
                <a class="muestra-ubicacion" href="#">[${registro.latitud} , ${registro.longitud}]</a></div>
                </div>
                <br/>

                <h4>Capacidad de almacenamiento</h4>
                <div class="row">
                <div class="col-md-4"><strong>Número de almacenes:</strong><br/>${registro.numeroAlmacenes}</div>
                </div>

                <div class="row">
                <div class="col-md-4"><strong>Capacidad instalada:</strong><br/>${registro.capacidad}</div>
                <div class="col-md-4"><strong>Volumen almacenado actual:</strong><br/>${registro.volumenActual}</div>
                <div class="col-md-4"><strong>Superficie de la instalación:</strong><br/>${registro.superficie}</div>
                </div>

                <div class="row">
                <div class="col-md-4"><strong>¿Está habilitada por alguna almacenadora?:</strong><br/>${registro.habilitaAlmacenadora ? 'Sí' : 'No'}</div>
                <div class="col-md-4"><strong>Nombre de la almacenadora:</strong><br/>${registro.nombreAlmacenadora}</div>
                </div>
                <br/>

                <h4>Manejo del grano</h4>
                <h5>Compra</h5>
                <table id="grano-compra" class="table table-striped table-bordered table-hover">
                <thead class="thead-dark">
                <tr class="success">
                <th>#</th>
                <th>Nombre de actor</th>
                <th>Estado</th>
                <th>Municipio</th>
                <th>Año</th>
                <th>Cantidad</th>
                </tr>
                </thead>
                <tbody>
                `;

        if (registro.datosComercializacion) {
            buffer += utils.construyeDatosComercializacion(registro.datosComercializacion, 'compra');
        } else {
            buffer += `<tr><td colspan="8" class="text-center">Sin registros.</td></tr>`;
        }

        buffer += `
                </tbody>
                </table>

                <h5>Venta</h5>
                <table id="grano-venta" class="table table-striped table-bordered table-hover">
                <thead class="thead-dark">
                <tr class="success">
                <th>#</th>
                <th>Nombre de actor</th>
                <th>Estado</th>
                <th>Municipio</th>
                <th>Año</th>
                <th>Cantidad</th>
                </tr>
                </thead>
                <tbody>
                `;

        if (registro.datosComercializacion) {
            buffer += utils.construyeDatosComercializacion(registro.datosComercializacion, 'venta');
        } else {
            buffer += `<tr><td colspan="8" class="text-center">Sin registros.</td></tr>`;
        }

        buffer += `
                </tbody>
                </table>
                <br/>

                <h4>Bodegas o centros de acopio (Sucursales)</h4>
                <table id="sucursales-table" class="table table-striped table-bordered table-hover">
                <thead class="thead-dark">
                <tr class="success">
                <th>#</th>
                <th>Nombre</th>
                <th>Dirección</th>
                <th>Localidad</th>
                <th>Municipio</th>
                <th>Estado</th>
                <th>Ubicación</th>
                </tr>
                </thead>
                <tbody>
                `;

        if (registro.sucursales) {
            for (var i = 0; i < registro.sucursales.length; i++) {
                var s = registro.sucursales[i];
                buffer += `
                    <tr>
                    <td class="text-right">${i + 1}</td>
                    <td>${s.nombre}</td>
                    <td>${s.domicilio.calle}, No. Ext. ${s.domicilio.numeroExterior}${s.domicilio.numeroInterior ? ', No. Int. ' + s.domicilio.numeroInterior : ''}, C.P. ${s.domicilio.codigoPostal}</td>
                    <td>${s.domicilio.localidad}</td>
                    <td>${s.domicilio.municipio.nombre}</td>
                    <td>${s.domicilio.estado.nombre}</td>
                    <td><a class="muestra-ubicacion" href="#">[${s.latitud},${s.longitud}]</a></td>
                    </tr>
                    `;
            }
            buffer += `
                </tbody>
                </table>
                `;
            buffer += `
                 <h4>Enlace (Sucursales)</h4>
                <table id="sucursales-table" class="table table-striped table-bordered table-hover">
                <thead class="thead-dark">
                <tr class="success">
                <th>#</th>
                <th>Nombre Sucursal</th>
                <th>Nombre del enlace</th>
                <th>Número celular</th>
                <th>Correo electrónico</th>
                </tr>
                </thead>
                `;
            for (var i = 0; i < registro.sucursales.length; i++) {
                var s = registro.sucursales[i];
                buffer += `
                    <tr>
                    <td class="text-right">${i + 1}</td>
                    <td>${s.nombre}</td>
                    <td>${s.nombreEnlace}</td>
                    <td>${s.numeroCelularEnlace}</td>
                    <td>${s.correoEnlace}</td>
                    </tr>
                    `;
            }
        } else {
            buffer += `<tr><td colspan="8" class="text-center">Sin sucursales.</td></tr>`;
        }
        buffer += `
                </tbody>
                </table>
                `;
        if (registro.archivos) {
            buffer += utils.construyeArchivos(registro);
        }
        buffer += `<div class="text-right"><a href="/cultivos/publico/impresion-registro-ventanilla.html?uuid=${registro.uuid}" id="button-imprimir-formato" class="btn btn-outline-success" target="_blank"><i class="fa fa-file"></i>  Descargar formato</a></div>
                </div>
                </div>
                `;
        if (!print) {
            buffer += utils.construyeEstatus(registro);
        };
        return buffer;
    };

    $.segalmex.maiz.vista.validacion.empresa.creaTablaCaptura = function (datos, estatus) {
        var buffer = [];
        var grupo = 'general';
        for (var i = 0; i < datos.length; i++) {
            var dato = datos[i];
            if (dato.grupo === undefined) {
                dato.grupo = 'General';
            }

            if (dato.grupo !== grupo) {
                buffer.push('<tr class="table-secondary"><th colspan="4">' + dato.grupo + '</th></tr>');
                grupo = dato.grupo;
            }
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(dato.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            if (dato.tipo === 'boolean') {
                buffer.push('<input type="text" class="form-control" value="' + (dato.valor === 'true' ? 'Sí' : 'No') + '" readonly="readonly"/>');
            } else {
                buffer.push('<input type="text" class="form-control" value="' + (dato.valorTexto ? dato.valorTexto : dato.valor) + '" readonly="readonly"/>');
            }
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push('<div class="form-check form-check-inline">');
            buffer.push('<input class="form-check-input" type="radio" name="correcto-' + dato.clave + '" id="' + dato.clave + '.true" value="true"' + (dato.correcto ? ' checked="checked" ' : '') + '/>');
            buffer.push('<label class="form-check-label" for="' + dato.clave + '.true"><i class="fas fa-check-circle text-success"></i></label>');
            buffer.push('</div>');
            buffer.push('<div class="form-check form-check-inline">');
            buffer.push('<input class="form-check-input" type="radio" name="correcto-' + dato.clave + '" id="' + dato.clave + '.false" value="false"' + (dato.correcto === false ? ' checked="checked" ' : '') + '/>');
            buffer.push('<label class="form-check-label" for="' + dato.clave + '.false"><i class="fas fa-times-circle text-danger"></i></label>');
            buffer.push('</div>');
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push('<label for="comentario-' + dato.clave + '" class="sr-only">Comentario de ' + dato.nombre + '</label>');
            buffer.push('<input id="comentario-' + dato.clave + '" type="text" class="valid-field form-control comentario-dato-capturado" '
                    + (dato.correcto === false ? '' : 'disabled="disabled" ')
                    + 'maxlength=' + (dato.tipo === 'archivo' ? '"1023" ' : '"255" ')
                    + 'value="' + (!estatus && dato.comentario ? dato.comentario : '') + '"/>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }

        return buffer.join('\n');
    };

    utils.construyeArchivos = function (registro) {
        var buffer = '';
        if (registro.archivos || registro.archivos.length > 0) {
            buffer = `
                    <h4>Archivos anexos</h4>
                    <ul>
                    `;
            for (var i = 0; i < registro.archivos.length; i++) {
                var archivo = registro.archivos[i];
                buffer += `
                        <li><a href="/cultivos/resources/archivos/maiz/${archivo.uuid}/${archivo.nombre}?inline=true" target="_blank">
                        ${archivo.nombreOriginal} [ ${archivo.tipo} ]   <i class="fas fa-file"></i></a></li>
                        `;
            }
            buffer += `</ul><br/>`;
        }
        return buffer;
    };

    utils.construyeDatosComercializacion = function (datosComercializacion, tipo) {
        var buffer = '';
        var indice = 1;
        var total = 0;
        var rowspan = 2;
        for (var i = 0; i < datosComercializacion.length; i++) {
            var dc = datosComercializacion[i];
            if (dc.tipo === tipo) {
                buffer += '<tr>';
                if (i % 2 === 0) {
                    buffer += `<td class="align-middle" rowspan="${rowspan}">${indice++}</td>
                        <td class="align-middle" rowspan="${rowspan}">${dc.nombreActor}</td>`;
                }
                buffer += `<td>${dc.estado.nombre}</td>
                    <td>${dc.municipio.nombre}</td>
                    <td class="text-right">${dc.anio}</td>
                    <td class="text-right">${utils.format(dc.cantidad)}</td></tr>`;
                total += dc.cantidad;
            }
        }
        buffer += `<tfoot class="thead-dark"><tr><th colspan="5">Total</th><th class="text-right">${utils.format(total)}</th></tr></tfoot>`;
        return buffer;
    };

    utils.construyeEstatus = function (registro) {
        var buffer = [];
        buffer.push('<div class="card mb-4 shadow-sm">'); // card
        buffer.push('<h4 class="card-header">Estatus</h4>');
        buffer.push('<div class="card-body">'); // card-body

        buffer.push('<h3 class="card-title">' + registro.estatus.nombre + '</h3>');

        if (registro.comentarioEstatus) {
            buffer.push('<div class="row">'); // row
            buffer.push('<div class="col-md-12">');
            buffer.push('<strong>Comentario:</strong><br/>');
            buffer.push(registro.comentarioEstatus);
            buffer.push('</div>');
            buffer.push('</div>');
        }

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Usuario registra:</strong><br/>');
        buffer.push(registro.usuarioRegistra ? registro.usuarioRegistra.nombre : '--');
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Usuario validador:</strong><br/>');
        buffer.push(registro.usuarioValidador ? utils.formatoNombreUsuario(registro.usuarioValidador.nombre) : '--');
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Usuario asignado (actual):</strong><br/>');
        buffer.push(registro.usuarioAsignado ? utils.formatoNombreUsuario(registro.usuarioAsignado.nombre) : '--');
        buffer.push('</div>');
        buffer.push('</div>'); // end-row

        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card

        return buffer.join('\n');
    };

    utils.formatoNombreUsuario = function (nombre) {
        var n = nombre.split('@')[0];
        if (n.length > 4) {
            return n.substring(0, 4) + '...';
        } else {
            return n + '...';
        }
    };

    utils.format = function (str) {
        return str.toFixed(3).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    };
})(jQuery);