(function ($) {
    $.segalmex.namespace('segalmex.cultivos.administrar.alerta');
    var utils = {};
    var handlers = {};
    var data = {
        alertas: [],
        alerta: {}
    };

    $.segalmex.cultivos.administrar.alerta.init = () => {
        utils.cargaCatalogos();
        utils.configuraPantalla();
        utils.inicializaValidaciones();
        utils.cargaInformacionAlerta();
    };

    handlers.guardaInformacionAlerta = (e) => {
        e.preventDefault();
        $('#guardar-button').prop('disable', true);
        $('#div-form-alerta .valid-field').limpiaErrores();
        var errores = [];
        $('#div-form-alerta .valid-field').valida(errores, true);
        if (errores.length !== 0) {
            $('#guardar-button').prop('disable', false);
            return;
        }
        var cultivo = $('#cultivo').val();
        var ciclo = $('#ciclo').val();
        var infoAlerta = {
            cultivo: {id: cultivo !== '0' ? cultivo : null},
            ciclo: {id: ciclo !== '0' ? ciclo : null},
            tipo: $('#tipo').val(),
            valor: $('#valor').val(),
            comentario: $('#comentario').val()
        };

        $.ajax({
            url: '/cultivos/resources/informacion-alerta/',
            type: 'POST',
            data: JSON.stringify(infoAlerta),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('Nueva información alerta guardada con éxito.');
            utils.cargaInformacionAlerta();
            utils.regresaTablaInformacionAlerta();
            $('#guardar-button').prop('disable', false);
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Ocurrió un error al guardar el registro.');
            }
            $('#guardar-button').prop('disable', false);
        });
    };

    handlers.muestraComentario = (e) => {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var comentario = id.substring('btn-comentario-'.length);
        $('#comentario-modal .modal-body p').html(comentario);
        $('#comentario-modal').modal('show');
    };

    handlers.eliminaInformacionAlerta = (e) => {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-alerta-'.length), 10);
        var alerta = $.segalmex.get(data.alertas, idx);
        if (!confirm("¿Seguro que desea eliminar la información alerta: " + alerta.folio + "?")) {
            return;
        }

        $.ajax({
            url: '/cultivos/resources/informacion-alerta/',
            type: 'DELETE',
            data: JSON.stringify(alerta),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('Información alerta eliminada con éxito.');
            utils.cargaInformacionAlerta();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Ocurrió un error al eliminar el registro.');
            }
        });
    };
    utils.cargaInformacionAlerta = () => {
        $.ajax({
            type: 'GET',
            url: '/cultivos/resources/informacion-alerta/',
            dataType: 'json'
        }).done(function (response) {
            data.alertas = response;
            $('#div-informacion-alerta').html(utils.creaTablaInformacionAlerta(response));
            utils.configuraTablaResultados();
            $('#alerta-table').on('click', 'button.eliminar-alerta', handlers.eliminaInformacionAlerta);
            $('#alerta-table').on('click', 'button.btn-comentario', handlers.muestraComentario);
        }).fail(function () {
            alert('Error: Al descargar los recursos de la página.');
        });
    };

    utils.creaTablaInformacionAlerta = (resultados) => {
        var buffer = `
            <table id="alerta-table" class="table table-bordered table-hover table-striped">
            <thead class="thead-dark">
              <tr>
                <th>#</th>
                <th>Folio</th>
                <th>Cultivo</th>
                <th>Ciclo</th>
                <th>Tipo</th>
                <th>Valor</th>
                <th class="text-center">Acciones</th>
              </tr>
            </thead>
            <tbody>`;
        for (var info of resultados) {
            var contador = 1;
            buffer += `
                <tr>
                    <td>${contador++}</td>
                    <td>${info.folio}</td>
                    <td>${info.cultivo ? info.cultivo.nombre : '--'}</td>
                    <td>${info.ciclo ? info.ciclo.nombre : '--'}</td>
                    <td>${info.tipo.toUpperCase()}</td>
                    <td>${info.valor}</td>
                    <td class="text-center">
                    <button id="eliminar-alerta- ${info.id}
                    " class="btn btn-danger btn-sm eliminar-alerta">
                    <i class="fa fa-minus-circle"></i></button>
                    <button type="button" id="btn-comentario-${info.comentario}" 
                    class="btn btn-outline-secondary btn-sm btn-comentario"><i 
                    class="fas fa-comment-alt"></i></button>
                    </td>
                </tr>
            `;
        }
        buffer += `</tbody>
                   </table>`;
        return buffer;
    };

    utils.cargaCatalogos = () => {
        $.ajax({
            type: 'GET',
            url: '/cultivos/resources/paginas/informacion-alerta',
            dataType: 'json'
        }).done(function (response) {
            $('#cultivo').actualizaCombo(response.cultivos, {first: 'Todos'});
            $('#ciclo').actualizaCombo(response.ciclos, {first: 'Todos'});
        }).fail(function () {
            alert('Error: Al descargar los recursos de la página.');
        });
    };

    utils.inicializaValidaciones = () => {
        $(".valid-field").configura();
        $('#comentario').configura({
            type: 'comentario',
            maxlength: 2047
        });
        $('#cultivo,#ciclo').configura({
            required: false
        });
        $('.valid-field').validacion();
    };

    utils.configuraPantalla = () => {
        $('#nuevo-button').click(utils.nuevaInformacionAlerta);
        $('#tipo').change(utils.configuraCampoValor);
        $('#regresar-button').click(utils.regresaTablaInformacionAlerta);
        $('#guardar-button').click(handlers.guardaInformacionAlerta);
    };

    utils.nuevaInformacionAlerta = () => {
        $('#valor,#comentario').val('').limpiaErrores();
        $('#cultivo,#ciclo,#tipo').val('0').limpiaErrores();
        $('#div-informacion-alerta,#div-nuevo').hide();
        $('#div-form-alerta').show();
    };

    utils.configuraCampoValor = (e) => {
        var tipo = $('#' + e.target.id).val();
        $('#valor').val('').limpiaErrores().prop('disable', true);
        if (tipo !== '0') {
            switch (tipo) {
                case 'curp':
                    $('#valor').configura({type: 'curp'});
                    break;
                case 'rfc' :
                    $('#valor').configura({type: 'rfc-fisica'});
                    break;
            }
            $('#valor').prop('disable', false);
        }
    };

    utils.regresaTablaInformacionAlerta = () => {
        $('#div-form-alerta').hide();
        $('#div-informacion-alerta,#div-nuevo').show();
    };

    utils.configuraTablaResultados = () => {
        var aoColumns = [{"bSortable": false}, {"bSortable": true}, {"bSortable": true},
            {"bSortable": true}, {"bSortable": true}, {"bSortable": true}, {"bSortable": false}];
        utils.configuraTablaInformacionAlerta('alerta-table', aoColumns);
    };

    utils.configuraTablaInformacionAlerta = (tablaId, aoColumns) => {
        var table = $('#' + tablaId).DataTable({
            "oLanguage": {
                "sEmptyTable": "No se encontró información para mostrar en la tabla",
                "sSearch": "Buscar ",
                "sZeroRecords": "No se encontraron resultados."
            },
            "bFilter": true,
            "bInfo": false,
            "bPaginate": false,
            "aoColumns": aoColumns,
            "aoColumnDefs": [{"bSortable": false, "aTargets": ["no-sort"]}]
        });
        table.on('order.dt search.dt', function () {
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).order([[1, 'asc']]).draw(false);
        $("#" + tablaId + "_filter input").addClass('input-buscar');
        $(".dataTables_empty").attr("colspan", "100%");
    };
})(jQuery);