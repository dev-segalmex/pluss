(function ($) {
    $.segalmex.namespace('segalmex.cultivos.administrar.productor');
    var utils = {};
    var handlers = {};
    var data = {
        uuidInscripcion: '',
        prediosInscripcion: [],
        cultivoRegistro: '',
        prediosRegistro: []
    };

    $.segalmex.cultivos.administrar.productor.init = function () {
        utils.configuraPantalla();
        utils.inicializaValidaciones();
    };

    handlers.busca = function (e) {
        e.preventDefault();
        var campos = $('#folio');
        campos.limpiaErrores();
        var errores = [];
        campos.valida(errores, true);
        if (errores.length > 0) {
            return;
        }
        var datos = {
            folio: $('#folio').val(),
            seguimiento: true
        };
        $.ajax({
            url: '/cultivos/resources/productores/maiz/inscripcion/',
            type: 'GET',
            data: datos,
            dataType: 'json'
        }).done(function (response) {
            if (response.length === 0) {
                alert('No se encontró registro con el folio especificado.');
                return;
            }
            utils.muestraDetalleProductor(response[0].uuid);
        }).fail(function () {
            alert('Error: No existe registro con el folio especificado.');
        });
    };

    handlers.confirmaEliminacion = function (e) {
        e.preventDefault();
        var idxPredio = $('#indice-predio').val();
        var folioPredio = $('#folio-predio').val();
        if (idxPredio === '0') {
            alert('* Índice del predio este campo es obligatorio.');
            return;
        }
        if (folioPredio === '') {
            alert('* Folio este campo es obligatorio.');
            return;
        }
        if (utils.validaPredio(parseInt(idxPredio), folioPredio)) {
            alert('Error: el folio del predio no coincide con el índice seleccionado.');
            return;
        }

        var idx = (parseInt(idxPredio) - 1);
        $.ajax({
            url: '/cultivos/resources/productores/maiz/inscripcion/' + data.uuidInscripcion + '/predios/' + idx,
            type: 'DELETE',
            dataType: 'json'
        }).done(function (response) {
            alert('Se eliminó el predio correctamente.');
            utils.muestraDetalleProductor(data.uuidInscripcion);
            utils.cierraEliminaPredioModal();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('No fue posible eliminar el predio del registro.');
            }
        });

    };

    handlers.confirmaEliminacionContrato = function (e) {
        e.preventDefault();
        var folioContrato = $('#folio-contrato').val();
        if (folioContrato === '') {
            alert('* Folio este campo es obligatorio.');
            return;
        }
        if (data.contratoElimina.folio !== folioContrato) {
            alert('Error: el folio ingresado no coincide con el folio del contrato seleccionado.');
            return;
        }

        $.ajax({
            url: '/cultivos/resources/productores/maiz/inscripcion/' + data.uuidInscripcion + '/contrato/' + data.contratoElimina.orden,
            type: 'DELETE',
            dataType: 'json'
        }).done(function (response) {
            alert('Se eliminó el contrato correctamente.');
            utils.muestraDetalleProductor(data.uuidInscripcion);
            utils.cierraEliminaContratoModal();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('No fue posible eliminar el contrato del registro.');
            }
        });
    };

    handlers.guardaTotalXml = function (e) {
        e.preventDefault();
        var idxPredio = $('#indice-predio-xml').val();
        var folioPredio = $('#folio-predio-xml').val();
        if (idxPredio === '0') {
            alert('* Índice del predio este campo es obligatorio.');
            return;
        }
        if (folioPredio === '') {
            alert('* Folio este campo es obligatorio.');
            return;
        }

        if (utils.validaPredio(parseInt(idxPredio), folioPredio)) {
            alert('Error: el folio del predio no coincide con el índice seleccionado.');
            return;
        }

        var idx = parseInt(idxPredio) - 1;
        var cantidadSiembra = $('#siembra-total').val();
        var cantidadRiego = $('#riego-total').val();
        $.ajax({
            url: '/cultivos/resources/productores/maiz/inscripcion/' + data.uuidInscripcion + '/predios/' + idx + '/permiso-siembra/cantidad/' + cantidadSiembra,
            type: 'PUT',
            dataType: 'json'
        }).done(function (response) {
            $.ajax({
                url: '/cultivos/resources/productores/maiz/inscripcion/' + data.uuidInscripcion + '/predios/' + idx + '/pago-riego/cantidad/' + cantidadRiego,
                type: 'PUT',
                dataType: 'json'
            }).done(function (response) {
                alert('Se actualizó la cantidad de XML del registro.');
                utils.muestraDetalleProductor(data.uuidInscripcion);
            }).fail(function (jqXHR) {
                if (jqXHR.responseJSON) {
                    alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
                } else {
                    alert('No fue posible actualizar el total de XML.');
                }
            });
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('No fue posible actualizar el total de XML.');
            }

        });

        utils.cierraModificaXmlModal();
    };

    handlers.limpia = function (e) {
        e.preventDefault();
        $('#folio').val('').limpiaErrores();
    };

    utils.muestraDetalleProductor = function (uuid) {
        $.ajax({
            url: '/cultivos/resources/productores/maiz/inscripcion/' + uuid,
            data: {archivos: false, datos: false, historial: false, contratos: true, comentarios: true, expand: 3},
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            // Mostramos el detalle
            data.uuidInscripcion = response.uuid;
            data.prediosInscripcion = response.predios;
            data.cultivoRegistro = response.cultivo.clave;
            data.contratosProductor = response.contratos;
            data.prediosDocumentos = response.prediosDocumentos;
            $.segalmex.maiz.inscripcion.productor.vista.muestraInscripcion(response, 'detalle-inscripcion-productor');
            $('#estatus-inscripcion-productor').html($.segalmex.maiz.inscripcion.vista.construyeEstatus(response));
            utils.actualizaNoPredios(response.predios);
            utils.deshabilitaButtonXml(response.cultivo);
            $('#detalle-elemento').show();
            $('#busqueda-div').hide();
            $('#div-predios').html(utils.construyePredios(response.predios));
            $('#div-contratos').html(utils.construyeContratos(response.contratos ? response.contratos : []));
            $('#predios-table').on('click', 'button.eliminar-predio', handlers.eliminaPredio);
            $('#predios-table').on('click', 'button.cambiar-predio', handlers.modificaPredio);
            $('#predios-table').on('click', 'a.muestra-ubicacion', handlers.muestraUbicacion);
            $('#contratos-table').on('click', 'button.eliminar-contrato', handlers.eliminaContrato);
            $('#predios-documento-table').on('click', 'button.mostrar-repetidos', handlers.muestraFoliosRepetidos);
        }).fail(function () {
            alert('Error: No fue posible obtener el registro.');
        });
    };

    handlers.muestraTotalXml = function (e) {
        e.preventDefault();
        var idx = $(e.target).val();

        if (idx === '0') {
            $('#siembra-actual,#siembra-permitido,#siembra-total,#riego-actual,#riego-permitido,#riego-total').val('0');
            return;
        }

        for (var i = 0; i < data.prediosInscripcion.length; i++) {
            var predio = data.prediosInscripcion[i];
            if (parseInt(idx) === predio.orden) {
                var predioSeleccionado = data.prediosInscripcion[i];
            }
        }

        $('#siembra-actual').val(predioSeleccionado.cantidadSiembra);
        $('#siembra-permitido').val('0');
        $('#siembra-total').val(predioSeleccionado.cantidadSiembra);
        $('#riego-actual').val(predioSeleccionado.cantidadRiego);
        $('#riego-permitido').val('0');
        $('#riego-total').val(predioSeleccionado.cantidadRiego);
    };

    handlers.permitePositivoDeudor = function (e) {
        e.preventDefault();
        $.ajax({
            url: '/cultivos/resources/productores/maiz/inscripcion/' + data.uuidInscripcion + '/positivo/deudor/',
            type: 'PUT',
            dataType: 'json'
        }).done(function (response) {
            alert('Se actualizó el registro con éxito.');
            utils.muestraDetalleProductor(data.uuidInscripcion);
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Ocurrió un error al intentar permitir estatus positivo.');
            }
    });
    };

    utils.actualizaNoPredios = function (predios) {
        var numeroPredios = [];
        for (var i = 0; i < predios.length; i++) {
            var predio = {
                id: predios[i].orden,
                valor: predios[i].orden,
                nombre: predios[i].orden
            };
            numeroPredios.push(predio);
        }
        $('#indice-predio-xml').actualizaCombo(numeroPredios);
        $('#indice-predio').actualizaCombo(numeroPredios);
        $('#indice-predio-ubicacion').actualizaCombo(numeroPredios);
    };

    utils.inicializaValidaciones = function () {
        $(".valid-field").configura({required: false});
        $('#folio,#folio-contrato').configura({
            pattern: /^\d{1,6}$/,
            minlength: 1,
            maxlength: 6,
            required: true
        });

        $('#folio-predio,#folio-predio-xml').configura({
            type: 'name-moral',
            maxlength: 64,
            required: true
        });

        $('#siembra-actual,#siembra-permitido,#siembra-total,#riego-actual,#riego-permitido,#riego-total').configura({
            type: 'number',
            min: 0
        });
        $('.valid-field').validacion();
    };

    utils.configuraPantalla = function () {
        $('#button-buscar').click(handlers.busca);
        $('#button-limpiar').click(handlers.limpia);
        $('#button-forzar-estatus').click(utils.forzarEstatus);
        $('#button-modificar-xml').click(utils.modificaXmlModal);
        $('#button-verificar-datos-capturados').click(utils.verificarDatosCapturados);
        $('#cierra-xml-button').click(utils.cierraModificaXmlModal);
        $('#cierra-elimina-button').click(utils.cierraEliminaPredioModal);
        $('#cierra-modifica-button').click(utils.cierraModalCambioGeorreferencia);
        $('#button-regresar-resultados').click(utils.regresar);
        $('#guarda-elimina-button').click(handlers.confirmaEliminacion);
        $('#guarda-modifica-button').click(handlers.confirmaCambioGeorreferencia);
        $('#indice-predio-xml').change(handlers.muestraTotalXml);
        $('#siembra-permitido').change(utils.calculaSiembra);
        $('#riego-permitido').change(utils.calculaRiego);
        $('#guarda-xml-button').click(handlers.guardaTotalXml);
        $('#cierra-elimina-contrato-button').click(utils.cierraEliminaContratoModal);
        $('#guarda-elimina-contrato-button').click(handlers.confirmaEliminacionContrato);
        $('#button-permite-positivo-deudor').click(handlers.permitePositivoDeudor);
    };

    utils.cierraEliminaPredioModal = function () {
        $('#elimina-predio-modal').modal('hide');
        $('#folio-predio').val('').limpiaErrores();
        $('#indice-predio').val('0').limpiaErrores();
    };

    utils.cierraEliminaContratoModal = function () {
        $('#elimina-contrato-modal').modal('hide');
        $('#numero-contrato-elimina,#folio-contrato').val('').limpiaErrores();
    };


    utils.cierraModalCambioGeorreferencia = function () {
        $('#modifica-predio-modal').modal('hide');
        $('#folio-predio-ubicacion').val('').limpiaErrores();
        $('#indice-predio-ubicacion').val('0').limpiaErrores();
    };

    utils.modificaXmlModal = function () {
        $('#xml-predio-modal').modal('show');
    };

    utils.cierraModificaXmlModal = function () {
        $('#xml-predio-modal').modal('hide');
        $('#folio-predio-xml').val('').limpiaErrores();
        $('#indice-predio-xml').val('0').limpiaErrores();
        $('#siembra-actual,#siembra-permitido,#siembra-total,#riego-actual,'
                + '#riego-permitido,#riego-total').val('').limpiaErrores();
    };

    utils.forzarEstatus = function () {
        $.ajax({
            url: '/cultivos/resources/productores/maiz/inscripcion/' + data.uuidInscripcion + '/forzada/',
            type: 'PUT',
            dataType: 'json'
        }).done(function (response) {
            alert('Se actualizó el registro con éxito.');
            utils.muestraDetalleProductor(data.uuidInscripcion);
        }).fail(function () {
            alert('No fue posible cambiar el estatus del registro.');
        });
    };

    utils.verificarDatosCapturados = function () {
        $.ajax({
            url: '/cultivos/resources/productores/maiz/inscripcion/' + data.uuidInscripcion + '/datos/duplicados/',
            type: 'DELETE',
            dataType: 'json'
        }).done(function (response) {
            alert('Se actualizó el registro con éxito.');
            utils.muestraDetalleProductor(data.uuidInscripcion);
        }).fail(function () {
            alert('No fue posible verificar los datos capturados.');
        });
    };

    utils.regresar = function () {
        $('#busqueda-div').show();
        $('#detalle-elemento').hide();
    };

    utils.validaPredio = function (noPredio, folio) {
        for (var i = 0; i < data.prediosInscripcion.length; i++) {
            var predio = data.prediosInscripcion[i];
            if (predio.orden === noPredio && predio.folio === folio) {
                return false;
            }
        }
        return true;
    };

    utils.deshabilitaButtonXml = function (cultivo) {
        $('#button-modificar-xml').prop('disabled', true);
        if (cultivo.clave === 'maiz-comercial') {
            $('#button-modificar-xml').prop('disabled', false);
        }
    };

    utils.calculaSiembra = function () {
        var actual = $('#siembra-actual').val();
        var permitido = $('#siembra-permitido').val();
        var etiqueta = '#siembra-total';
        utils.calculaTotalXm(actual, permitido, etiqueta);
    };

    utils.calculaRiego = function () {
        var actual = $('#riego-actual').val();
        var permitido = $('#riego-permitido').val();
        var etiqueta = '#riego-total';
        utils.calculaTotalXm(actual, permitido, etiqueta);
    };

    utils.calculaTotalXm = function (actual, permitido, etiqueta) {
        var totalXml = parseInt(actual) + parseInt(permitido);
        $(etiqueta).val(!isNaN(totalXml) && totalXml !== Infinity ? totalXml : '');
    };

    utils.construyePredios = function (predios) {
        var buffer = [];
        buffer.push('<div class="table-responsive">');
        buffer.push('<table id="predios-table" class="table table-bordered table-hover table-striped">');
        buffer.push('<thead class="thead-dark"><tr>');
        buffer.push('<th>#</th>');
        buffer.push('<th>Folio</th>');
        buffer.push('<th>Tipo cultivo</th>');
        buffer.push('<th>Posesión</th>');
        buffer.push('<th>R. hídrico</th>');
        buffer.push('<th>Municipio, Estado</th>');
        buffer.push('<th>Localidad</th>');
        buffer.push('<th>Ubicación</th>');
        buffer.push('<th>V&nbsp;(t)</th>');
        buffer.push('<th>S&nbsp;(ha)</th>');
        buffer.push('<th>R&nbsp;(t/ha)</th>');
        buffer.push('<th>Acciones</th>');
        buffer.push('</tr></thead>');
        buffer.push('<tbody>');
        for (var i = 0; i < predios.length; i++) {
            var predio = predios[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.fechaCancelacion ? '<strike>' + predio.folio + '</strike>' : predio.folio);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.fechaCancelacion ? '<strike>' + predio.tipoCultivo.nombre + '</strike>' : predio.tipoCultivo.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.fechaCancelacion ? '<strike>' + predio.tipoPosesion.nombre + ', ' + predio.tipoDocumentoPosesion.nombre + '</strike>' : predio.tipoPosesion.nombre + ', ' + predio.tipoDocumentoPosesion.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.fechaCancelacion ? '<strike>' + predio.regimenHidrico.nombre + '</strike>' : predio.regimenHidrico.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.fechaCancelacion ? '<strike>' + predio.municipio.nombre + ', ' + predio.estado.nombre + '</strike>' : predio.municipio.nombre + ', ' + predio.estado.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.fechaCancelacion ? '<strike>' + predio.localidad + ', ' + predio.localidad + '</strike>' : predio.localidad + ', ' + predio.localidad);
            buffer.push('</td>');
            buffer.push('<td>');
            if (predio.latitud) {
                buffer.push(predio.fechaCancelacion ? '<strike>' + '<a class="muestra-ubicacion" href="#">[' + predio.latitud + ',' + predio.longitud + ']</a>' + '</strike>' : '<a class="muestra-ubicacion" href="#">[' + predio.latitud + ',' + predio.longitud + ']</a>');
            } else {
                buffer.push(predio.fechaCancelacion ? '<strike>' + '<a class="muestra-ubicacion" href="#">[' + predio.latitud1 + ',' + predio.longitud1 + ']</a>,' + '</strike>' : '<a class="muestra-ubicacion" href="#">[' + predio.latitud1 + ',' + predio.longitud1 + ']</a>,');
                buffer.push(predio.fechaCancelacion ? '<strike>' + '<a class="muestra-ubicacion" href="#">[' + predio.latitud2 + ',' + predio.longitud2 + ']</a>,' + '</strike>' : '<a class="muestra-ubicacion" href="#">[' + predio.latitud2 + ',' + predio.longitud2 + ']</a>,');
                buffer.push(predio.fechaCancelacion ? '<strike>' + '<a class="muestra-ubicacion" href="#">[' + predio.latitud3 + ',' + predio.longitud3 + ']</a>,' + '</strike>' : '<a class="muestra-ubicacion" href="#">[' + predio.latitud3 + ',' + predio.longitud3 + ']</a>,');
                buffer.push(predio.fechaCancelacion ? '<strike>' + '<a class="muestra-ubicacion" href="#">[' + predio.latitud4 + ',' + predio.longitud4 + ']</a>,' + '</strike>' : '<a class="muestra-ubicacion" href="#">[' + predio.latitud4 + ',' + predio.longitud4 + ']</a>');
            }
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            var predioVolumen = predio.volumen;
            var predioVolumenSolicitado = predio.volumenSolicitado;
            var volumen = predio.solicitado === 'solicitado' ? (utils.format(predioVolumen, 3) + utils.setRojo(utils.format(predioVolumenSolicitado, 3))) : utils.format(predioVolumen, 3);
            buffer.push(predio.fechaCancelacion ? '<strike>' + volumen + '</strike>' : volumen);
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(predio.fechaCancelacion ? '<strike>' + utils.format(predio.superficie, 3) + '</strike>' : utils.format(predio.superficie, 3));
            buffer.push('</td>');
            buffer.push(predio.fechaCancelacion
                    ? '<strike>' + predio.solicitado !== 'solicitado' && predio.rendimientoMaximo > 0 && predio.rendimiento > predio.rendimientoMaximo
                    ? '<td class="text-right bg-danger">' : '<td class="text-right">' + '</strike>'
                    : predio.solicitado !== 'solicitado' && predio.rendimientoMaximo > 0 && predio.rendimiento > predio.rendimientoMaximo
                    ? '<td class="text-right bg-danger">' : '<td class="text-right">');
            var rendimiento = predio.solicitado === 'solicitado' ? (utils.format(predio.rendimiento, 3) + utils.setRojo(utils.format(predio.rendimientoSolicitado, 3))) : (utils.format(predio.rendimiento, 3));
            buffer.push(predio.fechaCancelacion
                    ? (predio.solicitado !== 'solicitado' && predio.rendimientoMaximo > 0 && predio.rendimiento > predio.rendimientoMaximo) ? '<strike> <span class="font-weight-bold text-white">' + rendimiento + '</span> </strike>'
                    : '<strike>' + rendimiento + '</strike>'
                    : (predio.solicitado !== 'solicitado' && predio.rendimientoMaximo > 0 && predio.rendimiento > predio.rendimientoMaximo) ? '<span class="font-weight-bold text-white">' + rendimiento + '</span>' : rendimiento);
            buffer.push('</td>');
            buffer.push('<td class="text-center"><button id="eliminar-predio-');
            buffer.push(i);
            buffer.push('" class="btn btn-danger btn-sm eliminar-predio" title="Eliminar predio"><i class="fa fa-minus-circle"></i></button>');
            buffer.push('&nbsp;<button id="cambiar-predio-');
            buffer.push(i);
            buffer.push('" class="btn btn-primary btn-sm cambiar-predio" title="Cambiar tipo de georreferencia"><i class="fas fa-map-marker-alt"></i></button>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        buffer.push('</tbody>');
        buffer.push(predios.length > 0 ? utils.agregaTotales(predios) : '');
        buffer.push('</table>');
        buffer.push('</div>');
        buffer.push('<br/>');

        return buffer.join('');
    };

    utils.setRojo = function (val) {
        return '\n<p style="color:Red;"> (' + val + ')</p>';
    };

    utils.agregaTotales = function (predios) {
        var volumenTot = 0;
        var volumenSolTot = 0;
        var superficieTot = 0;
        var rendimientoTot = 0;
        var rendimientoSolTot = 0;
        for (var p in predios) {
            if (!predios[p].fechaCancelacion) {
                volumenTot += parseFloat(predios[p].volumen);
                volumenSolTot += parseFloat(predios[p].volumenSolicitado);
                superficieTot += parseFloat(predios[p].superficie);
            }
        }

        rendimientoTot = parseFloat(volumenTot / superficieTot);
        rendimientoSolTot += parseFloat(volumenSolTot / superficieTot);

        var isSolicitado = volumenTot !== volumenSolTot;
        var buffer = [];
        buffer.push('<tfoot class="thead-dark">');
        buffer.push('<tr>');
        buffer.push('<th colspan="8">Total</th>');
        buffer.push('<th class="text-right">' + utils.format(volumenTot, 3)
                + (isSolicitado ? utils.setRojo(utils.format(volumenSolTot, 3)) : '') + '</th>');
        buffer.push('<th class="text-right">' + utils.format(superficieTot, 3) + '</th>');
        buffer.push('<th class="text-right">' + utils.format(rendimientoTot, 3)
                + (isSolicitado ? utils.setRojo(utils.format(rendimientoSolTot, 3)) : '') + '</th>');
        buffer.push('<th colspan="1"></th>');
        buffer.push('</tr>');
        buffer.push('</tfoot>');
        return buffer.join('');
    };

    utils.format = function (str, fix) {
        return str.toFixed(fix).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    };

    handlers.eliminaPredio = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-predio-'.length), 10);
        $('#indice-predio').val(idx + 1).prop('disabled', true);
        $('#elimina-predio-modal').modal('show');
    };

    handlers.eliminaContrato = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-contrato-'.length), 10);
        var contrato = utils.getContrato(idx);
        $('#numero-contrato-elimina').val(contrato.numeroContrato);
        $('#elimina-contrato-modal').modal('show');
    };

    handlers.muestraUbicacion = function (e) {
        e.preventDefault();
        var ubicacion = $(e.target).html();
        $('#maps').muestraMaps({
            ubicacion: ubicacion
        });
    };

    handlers.modificaPredio = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('cambiar-predio-'.length), 10);
        $('#indice-predio-ubicacion').val(idx + 1).prop('disabled', true);
        $('#modifica-predio-modal').modal('show');
    };

    handlers.confirmaCambioGeorreferencia = function (e) {
        e.preventDefault();
        var idxPredio = $('#indice-predio-ubicacion').val();
        var folioPredio = $('#folio-predio-ubicacion').val();
        if (idxPredio === '0') {
            alert('* Índice del predio este campo es obligatorio.');
            return;
        }
        if (folioPredio === '') {
            alert('* Folio este campo es obligatorio.');
            return;
        }
        if (utils.validaPredio(parseInt(idxPredio), folioPredio)) {
            alert('Error: el folio del predio no coincide con el índice seleccionado.');
            return;
        }
        var p;
        for (var i = 0; i < data.prediosInscripcion.length; i++) {
            var predio = data.prediosInscripcion[i];
            if (predio.orden === parseInt(idxPredio) && predio.folio === folioPredio) {
                p = predio;
            }
        }
        var idx = (parseInt(idxPredio) - 1);
        var url = '/cultivos/resources/productores/maiz/inscripcion/' + data.uuidInscripcion + '/predios/' + idx + '/punto-medio/';
        if (p.latitud) {
            url = '/cultivos/resources/productores/maiz/inscripcion/' + data.uuidInscripcion + '/predios/' + idx + '/poligono/';
        }

        $.ajax({
            url: url,
            type: 'POST',
            dataType: 'json'
        }).done(function (response) {
            alert('Se actualizó el tipo de la georreferencia.');
            utils.muestraDetalleProductor(data.uuidInscripcion);
            utils.cierraModalCambioGeorreferencia();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('No fue posible cambiar la georreferencia del predio.');
            }

        });
    };

    utils.construyeContratos = function (contratos) {
        var buffer = [];
        buffer.push('<div class="table-responsive">');
        buffer.push('<table id="contratos-table" class="table table-bordered table-striped">');
        buffer.push('<thead class="thead-dark"><tr>');
        buffer.push('<th>No. contrato</th>');
        buffer.push('<th>Folio</th>');
        buffer.push('<th>Registrante</th>');
        buffer.push('<th>Cantidad contratada</th>');
        buffer.push('<th>Cantidad contratada cobertura</th>');
        buffer.push('<th>Estatus</th>');
        buffer.push('<th></th>');
        buffer.push('</tr></thead>');
        buffer.push('<tbody>');
        for (var i = 0; i < contratos.length; i++) {
            var contrato = contratos[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            if (contrato.archivo) {
                buffer.push('<a href="/cultivos/resources/archivos/' + contrato.archivo.sistema + '/' + contrato.archivo.uuid + '/' + contrato.archivo.nombre + '" target="_blank">');
                buffer.push(contrato.fechaCancelacion ? '<strike>' + contrato.numeroContrato + '</strike>' : contrato.numeroContrato);
                buffer.push('</a>');
            } else {
                buffer.push(contrato.numeroContrato);
            }
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(contrato.fechaCancelacion ? '<strike>' + contrato.folio + '</strike>' : contrato.folio);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(contrato.fechaCancelacion ? '<strike>' + contrato.empresa + '</strike>' : contrato.empresa);
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(contrato.fechaCancelacion ? '<strike>' + contrato.cantidadContratada + '</strike>' : contrato.cantidadContratada);
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(contrato.fechaCancelacion ? '<strike>' + contrato.cantidadContratadaCobertura + '</strike>' : contrato.cantidadContratadaCobertura);
            buffer.push('</td>');
            if (contrato.estatus) {
                buffer.push(contrato.estatus.clave === 'positiva'
                        ? '<td class="text-success"><i class="fas fa-check-circle"></i> '
                        : '<td class="text-danger"><i class="fas fa-exclamation-circle"></i> ');
                buffer.push(contrato.estatus.nombre);
            } else {
                buffer.push('<td>');
                buffer.push('--');
            }
            buffer.push('</td>');
            buffer.push('<td class="text-center"><button id="eliminar-contrato-');
            buffer.push(i);
            buffer.push('" class="btn btn-danger btn-sm eliminar-contrato" title="Eliminar contrato"><i class="fa fa-minus-circle"></i></button>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        buffer.push('<tbody>');
        buffer.push('</table>');
        buffer.push('</div>');
        buffer.push('<br/>');
        return buffer.join('');
    };

    utils.getContrato = function (idx) {
        for (var i = 0; i < data.contratosProductor.length; i++) {
            if (i === idx) {
                data.contratoElimina = data.contratosProductor[i];
                return data.contratoElimina;
            }
        }
    };

    handlers.muestraFoliosRepetidos = function (e) {
        e.preventDefault();
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('mostrar-repetidos-'.length), 10);
        for (var i = 0; i < data.prediosDocumentos.length; i++) {
            if (idx === i) {
                var pd = data.prediosDocumentos[i];
            }
        }
        $.ajax({
            url: '/cultivos/resources/predio-documento/duplicados/' + pd.uuidTimbreFiscalDigital,
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            var folios = response.join(',');
            $('#label-folios').html('<strong >' + folios + '</strong>');
            $('#muestra-repetidos-modal').modal('show');
        }).fail(function () {
            alert('Error: No se pudo obtener los folios duplicados.');
        });
    };
})(jQuery);