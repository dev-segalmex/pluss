/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


(function ($) {
    $.segalmex.namespace('segalmex.cultivos.archivo');
    var utils = {};
    var handlers = {};

    $.segalmex.cultivos.archivo.init = function () {
        utils.inicializaValidaciones();
        $('#button-guardar').click(handlers.guarda);
        $('#button-limpiar').click(handlers.limpiar);
        $('#archivo-pdf').change(handlers.verificaTamanoArchivo);
    };

    utils.inicializaValidaciones = function () {
        $(".valid-field").configura();

        $('#uuid').configura({
            type: 'comentario',
            required: true
        });

        $('#clave').configura({
            type: 'comentario',
            textTransform: 'lower',
            required: true
        });

        $('#tipo').configura({
            required: true
        });
        $('.valid-field').validacion();
    };


    handlers.guarda = function () {
        $('#tipo,#uuid,#clave').limpiaErrores();
        $('#guardar-button').html("Guardando...").prop('disabled', true);
        var errores = [];

        $('#tipo,#uuid,#clave').valida(errores, true);

        if (errores.length !== 0) {
            $('#' + errores[0].campo).focus();
            $('#guardar-button').html("Guardar").prop('disabled', false);
            return;
        }
        if ($('#archivo-pdf').val() === '') {
            $('#guardar-button').html("Guardar").prop('disabled', false);
            alert('Error: Es necesario agregar el archivo.');
            return;
        }

        var fd = new FormData();
        var archivo = $('#archivo-pdf')[0].files[0];
        fd.append('file', archivo);
        var tipo = $('#tipo').val();
        var url = '';
        switch (tipo) {
            case "1":
                url = '/cultivos/resources/productores/maiz/inscripcion/';
                break;
            case "2":
                url = '/cultivos/resources/contratos/maiz/inscripcion/';
                break;
            case "3":
                url = '/cultivos/resources/facturas/maiz/inscripcion/';
                break;
        }

        $.ajax({
            url: url + $('#uuid').val() + '/anexos/' + $('#clave').val() + '/pdf/',
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false
        }).done(function () {
            alert('Anexo guardado con éxito.');
            $('#clave,#archivo-pdf').val('').limpiaErrores();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert("Ocurrió un error al guardar el anexo.");
            }
        });

    };

    handlers.limpiar = function () {
        $('#uuid,#clave,#archivo-pdf').val('').limpiaErrores();
        $('#tipo').val(0).limpiaErrores();
    };

    handlers.verificaTamanoArchivo = function (e) {
        var tipo = $('#archivo-pdf').val().includes('xml') ? 'xml' : $('#archivo-pdf').val().includes('xlsx') ? 'xlsx' : 'pdf';
        $.segalmex.archivos.verificaArchivo(e, tipo, 24 * 1024 * 1024);
    };

})(jQuery);