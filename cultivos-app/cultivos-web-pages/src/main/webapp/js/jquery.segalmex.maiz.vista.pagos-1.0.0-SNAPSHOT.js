(function ($) {
    $.segalmex.namespace('segalmex.maiz.vista.pagos');

    var utils = {};
    $.segalmex.maiz.vista.pagos.muestraRequerimientoPago = function (requerimiento) {
        var buffer = [];

        buffer.push('<div class="card mb-4 shadow-sm">');
        buffer.push('<h4 class="card-header">Detalle del Requerimiento de Pago</h4>');

        buffer.push('<div class="card-body">'); // card-body
        buffer.push('<h3 class="card-title">Folio: ');
        buffer.push(requerimiento.id);
        buffer.push('</h3>');
        buffer.push('<h4>Programa de Precios de Garantía a Productos Alimentarios Básicos</h4>');
        buffer.push('<br/>');
        //PAGO
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Programa:</strong><br/>');
        buffer.push(requerimiento.programa);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Ciclo agrícola:</strong><br/>');
        buffer.push(requerimiento.ciclo.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Layout:</strong><br/>');
        buffer.push(requerimiento.layout);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Fecha:</strong><br/>');
        buffer.push($.segalmex.date.isoToFecha(requerimiento.fechaCreacion));
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Clave Área:</strong><br/>');
        buffer.push(requerimiento.claveArea);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Clave Centro de acopio:</strong><br/>');
        buffer.push(requerimiento.claveCentroAcopio);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Estatus:</strong><br/>');
        buffer.push(requerimiento.estatus);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Usuario:</strong><br/>');
        buffer.push(requerimiento.usuario.nombre);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');
        var clase = requerimiento.reporteGenerado === '--' ? 'btn btn-outline-secondary disabled' : 'btn btn-outline-secondary';
        buffer.push('<div class="text-right">');
        buffer.push('<a role="button" class="' + clase + '"' +
                ' href="/cultivos/resources/requerimientos-pago/' + requerimiento.uuid + '/requerimiento-pago.xlsx' +
                '" target="_blank"><i class="fa fa-file-excel"></i> Descargar requerimiento de pago</a>');
        buffer.push('</div>');

        //TABLA DE USOS FACTURAS
        buffer.push('<h4>Facturas</h4>');
        buffer.push('<br/>');
        buffer.push('<div class="table-responsive">');
        buffer.push('<table id="table-detalle-facturas" class="table table-striped table-bordered table-hover">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<tr class="success">');
        buffer.push('<th>#</th>');
        buffer.push('<th>Folio</th>');
        buffer.push('<th>Fecha de registro</th>');
        buffer.push('<th>Productor</th>');
        buffer.push('<th>RFC Productor</th>');
        buffer.push('<th>Método</th>');
        buffer.push('<th>Toneladas</th>');
        buffer.push('<th>Estmulo total</th>');
        buffer.push('<th>Estatus</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');
        var toneladas = 0;
        var total = 0;
        if (requerimiento.requerimientos) {
            for (var i = 0; i < requerimiento.requerimientos.length; i++) {
                var f = requerimiento.requerimientos[i];
                var factor = f.usoFactura.estatus.factor;
                buffer.push('<tr>');
                buffer.push('<td>');
                buffer.push(i + 1);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(f.usoFactura.folio);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push($.segalmex.date.isoToFecha(f.usoFactura.fechaCreacion));
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(f.productor.nombre);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(f.productor.rfc);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(f.usoFactura.tipoCultivo.abreviatura + f.usoFactura.ordenEstimulo);
                buffer.push('</td>');
                buffer.push('<td class="text-right">');
                buffer.push(utils.format(f.usoFactura.toneladas.toFixed(3)));
                buffer.push('</td>');
                buffer.push('<td class="text-right">');
                buffer.push(f.usoFactura.estimuloTotal ? utils.format(f.usoFactura.estimuloTotal.toFixed(2)) : '--');
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(f.usoFactura.estatus.nombre);
                buffer.push('</td>');
                buffer.push('</tr>');
                toneladas += utils.calculaEstimuloUsoFactura(f.usoFactura.toneladas, factor);
                total += utils.calculaEstimuloUsoFactura(f.usoFactura.estimuloTotal, factor);
            }
            if (requerimiento.requerimientos.length === 0) {
                buffer.push('<tr><td colspan="8">No existen requerimientos de pago registrados.</td></tr>');
            }
        } else {
            buffer.push('<tr><td colspan="7">No existen requerimientos de pago registrados.</td></tr>');
        }
        buffer.push('</tbody>');
        buffer.push('<tfoot class="thead-dark">');
        buffer.push('<tr>');
        buffer.push('<th colspan="6"></th>');
        buffer.push('<th class="text-right">');
        buffer.push(utils.format(toneladas.toFixed(3)));
        buffer.push('</th>');
        buffer.push('<th class="text-right">');
        buffer.push(utils.format(total.toFixed(2)));
        buffer.push('</th>');
        buffer.push('<th colspan="1"></th>');
        buffer.push('</tr>');
        buffer.push('</tfoot>');
        buffer.push('</table>');
        buffer.push('</div>');
        buffer.push('<br/>');

        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card
        return buffer.join('\n');
    };

    utils.calculaEstimuloUsoFactura = function (valor, factor) {
        return  (valor * factor);
    };

    $.segalmex.maiz.vista.pagos.creaTablaUsosFactura = function (resultados) {
        var buffer = [];
        buffer.push('<table id="table-resultados-busqueda" class="table table-striped table-bordered table-hover" width="100%">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<tr class="success">');
        buffer.push('<th>');
        buffer.push('<input id="todas-checkbox" type="checkbox"/>');
        buffer.push('</th>');
        buffer.push('<th>Folio</th>');
        buffer.push('<th>Fecha</th>');
        buffer.push('<th>Productor</th>');
        buffer.push('<th>CURP </th>');
        buffer.push('<th>RFC </th>');
        buffer.push('<th>Tipo </th>');
        buffer.push('<th>Método</th>');
        buffer.push('<th>Estado </th>');
        buffer.push('<th>Estímulo x tonelada</th>');
        buffer.push('<th>Toneladas</th>');
        buffer.push('<th>Estímulo total</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');
        var toneladas = 0;
        var total = 0;
        for (var i = 0; i < resultados.length; i++) {
            var f = resultados[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-center">');
            buffer.push('<input type="checkbox" id="' + f.uuid + '-factura-check" class="factura-check" value="' + f.estatus + '"/>');
            buffer.push('</td>');
            buffer.push('<td><a class="link-folio" href="#" id="folio.' + f.folio + '">');
            buffer.push(f.folio);
            buffer.push('</a></td>');
            buffer.push('<td>');
            buffer.push($.segalmex.date.isoToFecha(f.fechaCreacion));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(f.productor.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(f.productor.curp);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(f.productor.rfc);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(f.productorCiclo.tipoProductor.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(f.tipoCultivo.abreviatura + f.ordenEstimulo);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(f.estado ? f.estado.nombre : '--');
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(utils.format(f.estimuloTonelada.toFixed(2)));
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(utils.format(f.toneladas.toFixed(3)));
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(utils.format(f.estimuloTotal.toFixed(2)));
            buffer.push('</td>');
            buffer.push('</tr>');
            toneladas += f.toneladas;
            total += f.estimuloTotal;
        }
        if (resultados.length === 0) {
            buffer.push('<tr><td colspan="12">No hay facturas para registrar un requerimiento de pago.</td></tr>')
        }
        buffer.push('</tbody>');
        if (toneladas > 0) {
            buffer.push('<tfoot class="thead-dark">');
            buffer.push('<tr>');
            buffer.push('<th colspan="10"></th>');
            buffer.push('<th class="text-right">');
            buffer.push(utils.format(toneladas.toFixed(3)));
            buffer.push('</th>');
            buffer.push('<th class="text-right">');
            buffer.push(utils.format(total.toFixed(2)));
            buffer.push('</th>');
            buffer.push('</tr>');
            buffer.push('</tfoot>');
        }
        buffer.push('</table>');


        buffer.push('<br/>');
        return buffer.join('');
    };

    $.segalmex.maiz.vista.pagos.creaTablaRequerimientos = function (requerimientos) {
        var buffer = [];
        buffer.push('<table id="table-resultados-busqueda" class="table table-striped table-bordered table-hover" width="100%">');
        buffer.push('<thead class="thead-dark text-center">');
        buffer.push('<tr class="success">');
        buffer.push('<th rowspan="2" class="align-top">#</th>');
        buffer.push('<th rowspan="2" class="align-top">Fecha de registro</th>');
        buffer.push('<th rowspan="2" class="align-top">Ciclo</th>');
        buffer.push('<th rowspan="2" class="align-top">CEGAP</th>');
        buffer.push('<th colspan="2">Facturas</th>');
        buffer.push('<th colspan="2">Toneladas</th>');
        buffer.push('<th colspan="2">Estímulo</th>');
        buffer.push('</tr>');
        buffer.push('<tr>');
        buffer.push('<th>Total</th>');
        buffer.push('<th>No pagadas</th>');
        buffer.push('<th>Total</th>');
        buffer.push('<th>No pagadas</th>');
        buffer.push('<th>Total</th>');
        buffer.push('<th>No pagadas</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');
        if (requerimientos.length === 0) {
            buffer.push('<tr><td colspan="10">No hay requerimientos de pago.</td></tr>');
            return buffer.join('');
        }

        var toneladas = 0;
        var total = 0;
        var facturas = 0;
        var toneladasNoPago = 0;
        var totalNoPago = 0;
        var facturasNoPago = 0;
        for (var i = 0; i < requerimientos.length; i++) {
            var r = requerimientos[i];
            buffer.push('<tr>');
            buffer.push(r.fechaCreacion.length === 7 ? '<td class="text-right">' : '<td class="text-right"><a class="link-uuid" href="#" id="uuid.' + r.uuid + '">');
            buffer.push(r.id);
            buffer.push(r.fechaCreacion.length === 7 ? '</td>' : '</a></td>');
            buffer.push('<td>');
            buffer.push($.segalmex.date.isoToFecha(r.fechaCreacion));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(r.ciclo.nombre);
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(r.cegap ? r.cegap : '--');
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(utils.format(r.facturas));
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(utils.format(r.facturasNoPago));
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(utils.format(r.toneladasTotales.toFixed(3)));
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(utils.format(r.toneladasNoPago.toFixed(3)));
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(utils.format(r.montoTotal.toFixed(2)));
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(utils.format(r.montoTotalNoPago.toFixed(2)));
            buffer.push('</td>');
            buffer.push('</tr>');

            toneladas += r.toneladasTotales;
            total += r.montoTotal;
            facturas += r.facturas;

            toneladasNoPago += r.toneladasNoPago;
            totalNoPago += r.montoTotalNoPago;
            facturasNoPago += r.facturasNoPago;
        }
        buffer.push('</tbody>');
        if (toneladas > 0) {
            buffer.push('<tfoot class="thead-dark">');
            buffer.push('<tr>');
            buffer.push('<th colspan="4"></th>');
            buffer.push('<th class="text-right">');
            buffer.push(utils.format(facturas));
            buffer.push('</th>');
            buffer.push('<th class="text-right">');
            buffer.push(utils.format(facturasNoPago));
            buffer.push('</th>');
            buffer.push('<th class="text-right">');
            buffer.push(utils.format(toneladas.toFixed(3)));
            buffer.push('</th>');
            buffer.push('<th class="text-right">');
            buffer.push(utils.format(toneladasNoPago.toFixed(3)));
            buffer.push('</th>');
            buffer.push('<th class="text-right">');
            buffer.push(utils.format(total.toFixed(2)));
            buffer.push('</th>');
            buffer.push('<th class="text-right">');
            buffer.push(utils.format(totalNoPago.toFixed(2)));
            buffer.push('</th>');
            buffer.push('</tfoot>');
        }
        buffer.push('</table>');

        buffer.push('<br/>');
        return buffer.join('');
    };

    utils.format = function (str) {
        return str.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    }

})(jQuery);
