/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.maiz.administrar.empresa');
    var data = {
        claveEmpresa: '',
        nombreEmpresa: ''
    };
    var handlers = {};
    var utils = {};

    var campos = ["#nombre-empresa", "#rfc-empresa"];
    var camposSucursal = ["#nombre-sucursal", "#localidad-sucursal", "#direccion-sucursal", "#latitud-sucursal", "#longitud-sucursal"];
    var selectSucursal = ["#estado-sucursal", "#municipio-sucursal", "#tipo-sucursal"];
    var camposEdicionSucursal = ["#edicion-nombre", "#edicion-localidad", "#edicion-direccion", "#edicion-latitud", "#edicion-longitud"];
    var selectEdicionSucursal = ["#edicion-estado", "#edicion-municipio", "#edicion-tipo"];

    $.segalmex.maiz.administrar.empresa.init = function (params) {
        utils.cargaCatalogos();
        utils.cargaEmpresas();
        utils.inicializaValidaciones();
        $('#button-guardar').click(utils.gurda);
        $('#button-nuevo').click(handlers.nuevo);
        $('#button-regresar').click(handlers.regresa);
        $('#button-guardar-sucursal').click(utils.guardaSucursal);
        $('#estado-sucursal').change(handlers.cambiaEstadoDireccion);
        $('#edicion-estado').change(handlers.cambiaEstadoEdicionDireccion);
        $('#button-regresar-empresa').click(handlers.regresaSucursal);
        $('#button-regresar-detalle-sucursal').click(handlers.regresaDetalleSucursal);
        $('#button-guardar-usuario').click(utils.guardaUsuario);
        $('#button-guarda-edicion').click(handlers.guardaEdicion);
        $('#button-cancelar-edicion').click(handlers.cancelaEdicion);
    };

    handlers.nuevo = function () {
        $('#empresas-registradas').hide();
        $('#div-nueva-empresa').show();
    };

    handlers.regresa = function () {
        $('#div-nueva-empresa').hide();
        $('#empresas-registradas').show();
        $('#nombre-empresa,#rfc-empresa').val('');
        $('.valid-field').limpiaErrores();
    };

    handlers.regresaDetalle = function () {
        $('#detalle-empresa').hide();
        $('#empresas-registradas').show();
        $('#nombre-empresa,#rfc-empresa').val('');
        $('.valid-field').limpiaErrores();
    };

    handlers.regresaSucursal = function () {
        $('#nueva-sucursal').hide();
        $('#detalle-empresa').show();
        utils.limpiar();
    };

    handlers.regresaDetalleEmpresa = function (e) {
        $('#detalle-sucursal').hide();
        $('#detalle-empresa').show();
    };

    handlers.regresaDetalleSucursal = function () {
        $('#nuevo-usuario').hide();
        $('#detalle-sucursal').show();
    };

    utils.cargaEmpresas = function () {
        $.ajax({
            url: '/cultivos/resources/empresas/administracion-empresa/',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            $('#table-empresas').html(utils.creaTablaEmpresa(response));
            $('#table-resultados-busqueda').on('click', 'a.link-uuid', handlers.muestraDetalle);
            utils.configuraTablaResultados();
        }).fail(function () {
            alert('Error: No fue posible cargar los datos de empresas.');
        });
    };

    utils.creaTablaEmpresa = function (empresas) {
        utils.ordena(empresas);
        var buffer = '';
        buffer = `
            <table id="table-resultados-busqueda" class="table table-striped table-bordered table-hover" width="100%">
            <thead class="thead-dark">
            <tr class="success">
            <th>#</th>
            <th>Folio</th>
            <th>RFC</th>
            <th>Nombre</th>
            </tr>
            </thead>
            <tbody>`;

        if (empresas.length === 0) {
            buffer += `<tr><td colspan="4">No hay empresas registradas.</td></tr>`;
        } else {

            for (var i = 0; i < empresas.length; i++) {
                var e = empresas[i];
                buffer += `
                <tr>
                <td>${i + 1}</td>
                <td><a class="link-uuid" href="#" id="id.${e.clave}">${e.clave}</a></td>
                <td>${e.rfc}</td>
                <td>${e.nombre}</td>
                </tr>`;
            }
        }
        buffer += `</table>
                   <br/>`;
        return buffer;
    };

    utils.ordena = function (arreglo) {
        if (arreglo.length > 0) {
            arreglo.sort(function (elementoA, elementoB) {
                var palabraA = elementoA.clave;
                var palabraB = elementoB.clave;
                if (palabraA < palabraB) {
                    return -1;
                }
                if (palabraA > palabraB) {
                    return 1;
                }
                return 0;
            });
        }
    };

    utils.inicializaValidaciones = function () {
        $(".valid-field").configura();
        $('#nombre-empresa,#nombre-sucursal,#edicion-nombre').configura({
            type: 'name-moral'
        }).configura({
            maxlength: 255
        });
        $('#rfc-empresa').configura({
            type: 'rfc'
        });

        $('#localidad-sucursal,#direccion-sucursal,#edicion-localidad,#edicion-direccion').configura({
            type: 'name-moral'
        });

        $('#latitud-sucursal,#edicion-latitud').configura({
            type: 'point',
            min: 14.5,
            max: 32.72
        });
        $('#longitud-sucursal,#edicion-longitud').configura({
            type: 'point',
            min: -117.2,
            max: -86.7
        });
        $('.valid-field').validacion();
    };

    utils.gurda = function () {
        $([campos].join(',')).limpiaErrores();
        var errores = [];
        $([campos].join(',')).valida(errores, false);

        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            $('#guardar-button').html("Guardar").prop('disabled', false);
            return;
        }

        var inscripcion = {
            nombre: $('#nombre-empresa').val(),
            rfc: $('#rfc-empresa').val()
        };
        $.ajax({
            url: '/cultivos/resources/empresas/administracion-empresa/',
            type: 'POST',
            data: JSON.stringify(inscripcion),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('Correcto: Se registró la empresa correctamente.');
            utils.cargaEmpresas();
            $('#div-nueva-empresa').hide();
            $('#empresas-registradas').show();
            $('#nombre-empresa,#rfc-empresa').val('');
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Error: No se pudo registrar la empresa.');
            }
        });
    };

    handlers.muestraDetalle = function (e) {
        e.preventDefault();
        var clave = e.target.id.split('.')[1];
        data.empresa = {clave: clave};
        utils.muestraEmpresaActualizada(clave);
    };

    handlers.muestraDetalleSucursal = function (e) {
        e.preventDefault();
        var clave = e.target.id.split('.')[1];
        $.ajax({
            url: '/cultivos/resources/empresas/administracion-empresa/sucursal/' + clave,
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            data.claveSucursal = response.clave;
            data.sucursal = response;
            $('#detalle-empresa').hide();
            $('#detalle-sucursal').show();
            $('#datos-sucursal').html(utils.creaDetalleSucursal(response));
            $('a.muestra-ubicacion').click(handlers.muestraUbicacion);
            $('#button-regresar-detalle-empresa').click(handlers.regresaDetalleEmpresa);
            $('#button-agregar-usuario').click(utils.nuevoUsuario);
            $('#button-editar-sucursal').click(handlers.editaSucursal);
        }).fail(function () {
            alert('Error: No fue posible obtener la información de la sucursal.');
        });

    };

    utils.creaDetalleSucursal = function (sucursal) {
        var buffer = '';
        buffer = `
            <div class="card mb-4 shadow-sm">
            <h4 class="card-header">Detalle de la sucursal</h4>
            <div class="card-body">
            <h3 class="card-title">Folio: ${sucursal.clave}</h3>
            <h4>Programa de Precios de Garantía a Productos Alimentarios Básicos</h4>
            <br/>

            <div class="row">
            <div class="col-md-4"><strong>Nombre:</strong><br/>${sucursal.nombre}</div>
            <div class="col-md-4"><strong>Tipo:</strong><br/>${sucursal.tipo.nombre}</div>
            </div>
            <div class="row">
            <div class="col-md-4"><strong>Estado:</strong><br/>${sucursal.estado.nombre}</div>
            <div class="col-md-4"><strong>Municipio:</strong><br/>${sucursal.municipio.nombre}</div>
            <div class="col-md-4"><strong>Localidad:</strong><br/>${sucursal.localidad}</div>
            </div>
            <br/>

            <div class="row">
            <div class="col-md-8"><strong>Dirección:</strong><br/>${sucursal.direccion}</div>
            </div>
            <br/>

            <h4>Georreferencia</h4>
            <div class="row">
            <div class="col-md-4"><strong>Latitud y longitud:</strong><br/>
            <a class="muestra-ubicacion" href="#">[${sucursal.latitud} , ${sucursal.longitud}]</a></div>
            </div>
            <br/>

            <h4>Usuarios</h4>
            <br/>
            <table id="table-detalle-usuarios" class="table table-striped table-bordered table-hover">
            <thead class="thead-dark">
            <tr class="success">
            <th>#</th>
            <th>Nombre</th>
            <th>Apellidos</th>
            <th>Sexo</th>
            <th>Usuario</th>
            <th>Puesto</th>
            </tr>
            </thead>
            <tbody>`;
        if (sucursal.usuarios) {

            for (var i = 0; i < sucursal.usuarios.length; i++) {
                var u = sucursal.usuarios[i];
                buffer += `
                <tr>
                <td>${i + 1}</td>
                <td>${u.persona.nombre}</td>
                <td>${u.persona.apellidos}</td>
                <td>${u.persona.sexo.nombre}</td>
                <td>${u.clave}</td>
                <td>${u.puesto}</td>
                </tr>`;
            }
        } else {

            buffer += `<tr><td colspan="6">No existen usuarios registrados.</td></tr>`;
        }


        buffer += `
            </div>
            </div>`;

        return buffer;
    };


    utils.creaDetalle = function (empresa) {
        var buffer = [];

        buffer.push('<div class="card mb-4 shadow-sm">');
        buffer.push('<h4 class="card-header">Detalle de la empresa</h4>');

        buffer.push('<div class="card-body">'); // card-body
        buffer.push('<h3 class="card-title">Folio: ');
        buffer.push(empresa.clave);
        buffer.push('</h3>');
        buffer.push('<h4>Programa de Precios de Garantía a Productos Alimentarios Básicos</h4>');
        buffer.push('<br/>');
        //Informacion de la empresa
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Nombre:</strong><br/>');
        buffer.push(empresa.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>RFC:</strong><br/>');
        buffer.push(empresa.rfc);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        //Tabla de sucursales
        buffer.push('<h4>Sucursales</h4>');
        buffer.push('<br/>');
        buffer.push('<table id="table-detalle-sucursales" class="table table-striped table-bordered table-hover">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<tr class="success">');
        buffer.push('<th>#</th>');
        buffer.push('<th>Folio</th>');
        buffer.push('<th>Nombre</th>');
        buffer.push('<th>Tipo</th>');
        buffer.push('<th>Estado</th>');
        buffer.push('<th>Municipio</th>');
        buffer.push('<th>Localidad</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');

        if (empresa.sucursales) {
            utils.ordena(empresa.sucursales);
            for (var i = 0; i < empresa.sucursales.length; i++) {
                var s = empresa.sucursales[i];
                buffer.push('<tr>');
                buffer.push('<td>');
                buffer.push(i + 1);
                buffer.push('</td>');
                buffer.push('<td><a class="link-uuid" href="#" id="clave.' + s.clave + '">');
                buffer.push(s.clave);
                buffer.push('</a></td>');
                buffer.push('<td>');
                buffer.push(s.nombre);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(s.tipo.nombre);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(s.estado.nombre);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(s.municipio.nombre);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(s.localidad);
                buffer.push('</td>');
                buffer.push('</tr>');
            }
        } else {
            buffer.push('<tr><td colspan="7">No existen sucursales registradas.</td></tr>');
        }

        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card
        return buffer.join('\n');
    };

    utils.nuevaSucursal = function () {
        $('#detalle-empresa').hide();
        $('#nombre-sucursal').val(data.nombreEmpresa).change();
        $('#nueva-sucursal').show();
    };

    utils.nuevoUsuario = function () {
        $('#detalle-sucursal').hide();
        $('#nuevo-usuario').show();
    };

    utils.cargaCatalogos = function () {
        $.ajax({
            type: 'GET',
            url: '/cultivos/resources/paginas/empresa/registro/',
            dataType: 'json'
        }).done(function (response) {
            $('#estado-sucursal,#edicion-estado').actualizaCombo(response.estados);
            $('#tipo-sucursal,#edicion-tipo').actualizaCombo(response.tiposSucursal);
            $('#usuarios').actualizaCombo(response.usuarios);
            data.municipios = response.municipios;
        }).fail(function () {
            alert('Error: Al descargar los recursos de la página.');
        });
    };

    handlers.cambiaEstadoDireccion = function (e) {
        utils.cambiaEstado(e.target, 'municipio-sucursal');
    };

    handlers.cambiaEstadoEdicionDireccion = function (e) {
        utils.cambiaEstado(e.target, 'edicion-municipio');
    };

    utils.cambiaEstado = function (target, idMunicipio) {
        var v = $(target).val();
        var estado = v !== '0' ? $.segalmex.get(data.municipios, v) : {clave: '0'};
        var municipios = [];
        if (estado.clave !== '0') {
            for (var i = 0; i < data.municipios.length; i++) {
                var m = data.municipios[i];
                if (m.estado.id === estado.id) {
                    municipios.push(m);
                }
            }
        }
        $('#' + idMunicipio).actualizaCombo(municipios).val('0');
    };

    utils.guardaSucursal = function () {
        $([camposSucursal].join(',')).limpiaErrores();
        $([selectSucursal].join(',')).limpiaErrores();
        var errores = [];

        $([camposSucursal].join(',')).valida(errores, false);
        $([selectSucursal].join(',')).valida(errores, false);

        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            return;
        }

        var sucursal = utils.generaDatos();

        $.ajax({
            url: '/cultivos/resources/empresas/administracion-empresa/sucursal/',
            type: 'POST',
            data: JSON.stringify(sucursal),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('Correcto: Se registró la sucursal correctamente.');
            utils.limpiar();
            $('#nueva-sucursal').hide();
            utils.muestraEmpresaActualizada(data.claveEmpresa);
        }).fail(function (responseF) {
            alert('Error: no se pudo registrar la sucursal.');
        });
    };

    utils.generaDatos = function () {
        var sucursal = {
            empresa: data.empresa,
            estado: {id: $("#estado-sucursal").val()},
            municipio: {id: $("#municipio-sucursal").val()},
            tipo: {id: $("#tipo-sucursal").val()},
            nombre: $("#nombre-sucursal").val(),
            localidad: $("#localidad-sucursal").val(),
            direccion: $("#direccion-sucursal").val(),
            latitud: $("#latitud-sucursal").val(),
            longitud: $("#longitud-sucursal").val()
        };
        return sucursal;
    };

    utils.limpiar = function () {
        $('#estado-sucursal,#municipio-sucursal,#tipo-sucursal').val('0').change();
        $([selectEdicionSucursal].join(',')).val('0').change();
        $([camposEdicionSucursal].join(',')).val('');
        $('#nombre-sucursal,#localidad-sucursal,#direccion-sucursal,#latitud-sucursal,#longitud-sucursal').val('');
        $('.valid-field').limpiaErrores();
    };

    utils.guardaUsuario = function () {
        var usuario = $('#usuarios').val();
        if (usuario === '0') {
            alert('Error: es necesario seleccionar un usuario.');
            return;
        }
        var uso = utils.generaUso();

        $.ajax({
            url: '/cultivos/resources/empresas/administracion-empresa/uso-catalogo/',
            type: 'POST',
            data: JSON.stringify(uso),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('Correcto: Se registró el usuario correctamente.');
            utils.cargaEmpresas();
            utils.cargaCatalogos();
            utils.limpiar();
            $('#nuevo-usuario').hide();
            $('#empresas-registradas').show();
            $('#usuarios').val('0');
        }).fail(function (responseF) {
            alert('Error: no se pudo asignar el usuario.');
        });
    };

    utils.generaUso = function () {
        var uso = {
            usuario: {id: $('#usuarios').val()},
            sucursal: {clave: data.claveSucursal}
        };
        return uso;
    };

    utils.configuraTablaResultados = function () {
        var aoColumns = [{"bSortable": false}, {"bSortable": true}, {"bSortable": true}, {"bSortable": true}];
        utils.configuraTablaFactura('table-resultados-busqueda', aoColumns);
    };

    utils.configuraTablaFactura = function (tablaId, aoColumns) {
        var table = $('#' + tablaId).DataTable({
            "oLanguage": {
                "sEmptyTable": "No se encontró información para mostrar en la tabla",
                "sSearch": "Buscar ",
                "sZeroRecords": "No se encontraron resultados."
            },
            "bFilter": true,
            "bInfo": false,
            "bPaginate": false,
            "aoColumns": aoColumns,
            "aoColumnDefs": [{"bSortable": false, "aTargets": ["no-sort"]}]
        });
        table.on('order.dt search.dt', function () {
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).order([[1, 'asc']]).draw(false);
        $("#" + tablaId + "_filter input").addClass('input-buscar');
        $(".dataTables_empty").attr("colspan", "100%");
    };

    utils.muestraEmpresaActualizada = function (clave) {
        $.ajax({
            url: '/cultivos/resources/empresas/administracion-empresa/' + clave,
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            // Mostramos el detalle de la empresa
            data.claveEmpresa = response.clave;
            data.nombreEmpresa = response.nombre;
            $('#datos-empresa').html(utils.creaDetalle(response));
            $('#button-regresar-detalle').click(handlers.regresaDetalle);
            $('#button-agregar-sucursal').click(utils.nuevaSucursal);
            $('#empresas-registradas').hide();
            $('#detalle-empresa').show();
            $('#table-detalle-sucursales').on('click', 'a.link-uuid', handlers.muestraDetalleSucursal);
        }).fail(function () {
            alert('Error: No fue posible obtener la información de la empresa.');
        });
    };

    handlers.muestraUbicacion = function (e) {
        e.preventDefault();
        var ubicacion = $(e.target).html();
        $('#maps').muestraMaps({
            ubicacion: ubicacion
        });
    };

    handlers.editaSucursal = function (e) {
        e.preventDefault();
        $('#detalle-sucursal').hide();
        $('#edicion-nombre').val(data.sucursal.nombre).change();
        $('#edicion-tipo').val(data.sucursal.tipo.id);
        $('#edicion-estado').val(data.sucursal.estado.id);
        $('#edicion-estado').change();
        $('#edicion-municipio').val(data.sucursal.municipio.id);
        $('#edicion-localidad').val(data.sucursal.localidad).change();
        $('#edicion-direccion').val(data.sucursal.direccion).change();
        $('#edicion-latitud').val(data.sucursal.latitud);
        $('#edicion-longitud').val(data.sucursal.longitud);
        $([camposEdicionSucursal].join(',')).limpiaErrores();
        $([selectEdicionSucursal].join(',')).limpiaErrores();
        $('#div-edicion-sucursal').show();
    };

    handlers.guardaEdicion = function (e) {
        e.preventDefault();
        $([camposEdicionSucursal].join(',')).limpiaErrores();
        $([selectEdicionSucursal].join(',')).limpiaErrores();
        var errores = [];

        $([camposEdicionSucursal].join(',')).valida(errores, false);
        $([selectEdicionSucursal].join(',')).valida(errores, false);

        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            return;
        }
        var editada = utils.getEditada();
        $.ajax({
            url: '/cultivos/resources/empresas/administracion-empresa/sucursal/' + data.sucursal.clave,
            type: 'PUT',
            data: JSON.stringify(editada),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('Correcto: Se editó la sucursal correctamente.');
            utils.limpiar();
            $('#div-edicion-sucursal').hide();
            utils.muestraEmpresaActualizada(data.claveEmpresa);
        }).fail(function (response) {
            alert('Error: no se pudo editar la sucursal.');
        });

    };

    handlers.cancelaEdicion = function (e) {
        e.preventDefault();
        $('#div-edicion-sucursal').hide();
        $('#detalle-sucursal').show();
    };

    utils.getEditada = function () {
        var sucursal = {
            empresa: data.empresa,
            estado: {id: $("#edicion-estado").val()},
            municipio: {id: $("#edicion-municipio").val()},
            tipo: {id: $("#edicion-tipo").val()},
            nombre: $("#edicion-nombre").val(),
            localidad: $("#edicion-localidad").val(),
            direccion: $("#edicion-direccion").val(),
            latitud: $("#edicion-latitud").val(),
            longitud: $("#edicion-longitud").val()
        };
        return sucursal;
    };
})(jQuery);