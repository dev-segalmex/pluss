/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.cultivos.excepcion.rfc');

    var data = {
        excepcionesRfc: []
    };
    var utils = {};
    var handlers = {};

    $.segalmex.cultivos.excepcion.rfc.init = function () {
        utils.inicializaValidaciones();
        $('#button-mostrar').click(utils.verExcepciones);
        $('#button-ocultar').click(utils.ocultarExcepciones);
        $('#button-guardar').click(handlers.guarda);
        $('#constancia-pdf').change(handlers.verificaTamanoArchivo);
    };

    handlers.eliminaRfc = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.split('-')[2]);
        var eliminar = $.segalmex.get(data.excepcionesRfc, idx);
        if (!confirm('Se va a eliminar la excepción para el RFC: ' + eliminar.rfc + '\n¿Desea continuar?')) {
            return;
        }
        $('#table-rfc').html(utils.construyeTablaRfcs(data.excepcionesRfc));
        $('#table-resultados').on('click', 'button.eliminar-rfc', handlers.eliminaRfc);

        $.ajax({
            url: '/cultivos/resources/rfc-excepcion/' + eliminar.rfc,
            type: 'DELETE',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            utils.cargaRfcs();
            alert('La excepción del RFC se eliminó con éxito.');
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Error: No se pudo eliminar el registro.');
            }
        });
    };

    utils.inicializaValidaciones = function () {
        $(".valid-field").configura();
        $('#rfc').configura({
            type: 'rfc'
        });
        $('.valid-field').validacion();
    };

    utils.cargaRfcs = function () {
        $.ajax({
            url: '/cultivos/resources/rfc-excepcion',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            data.excepcionesRfc = response;
            $('#table-rfc').html(utils.construyeTablaRfcs(data.excepcionesRfc));
            $('#table-resultados').on('click', 'button.eliminar-rfc', handlers.eliminaRfc);
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert("Ocurrió un error al cargar los registros.");
            }
        });
    };

    utils.construyeTablaRfcs = function (excepcionesRfc) {
        var buffer = '';
        buffer = `
            <table id="table-resultados" class="table table-striped table-bordered table-hover" width="100%">
            <thead class="thead-dark">
            <tr class="success">
            <th>#</th>
            <th>RFC</th>
            <th class="text-center"><i class="fa fa-minus-circle"></i></th>
            </tr>
            </thead>
            <tbody>`;

        if (excepcionesRfc.length === 0) {
            buffer += `<tr><td colspan="3">No hay RFC registrados.</td></tr>`;
        } else {
            for (var i = 0; i < excepcionesRfc.length; i++) {
                var e = excepcionesRfc[i];
                buffer += `
                <tr>
                <td>${i + 1}</td>
                <td>${e.rfc}</td>
                <td class="text-center"><button id="eliminar-rfc-${e.id}"
                    class="btn btn-danger btn-sm eliminar-rfc"><i class="fa fa-minus-circle"></i></button></td>
                </tr>`;
            }
        }

        buffer += `</table><br/>`;
        return buffer;
    };

    utils.verExcepciones = function (e) {
        e.preventDefault();
        utils.cargaRfcs();
        utils.muestraExcepciones(true);
    };

    utils.ocultarExcepciones = function (e) {
        e.preventDefault();
        utils.muestraExcepciones(false);
    };

    utils.muestraExcepciones = function (mostrar) {
        if (mostrar) {
            $('#div-excepciones-rfc').show();
        } else {
            $('#div-excepciones-rfc').hide();
        }
    };

    handlers.guarda = function () {
        $('#rfc').limpiaErrores();
        $('#guardar-button').html("Guardando...").prop('disabled', true);
        var errores = [];

        $('#rfc').valida(errores, true);

        if (errores.length !== 0) {
            $('#' + errores[0].campo).focus();
            $('#guardar-button').html("Guardar").prop('disabled', false);
            return;
        }
        if ($('#constancia-pdf').val() === '') {
            $('#guardar-button').html("Guardar").prop('disabled', false);
            alert('Error: Es necesario agregar la constancia.');
            return;
        }

        var fd = new FormData();
        var constancia = $('#constancia-pdf')[0].files[0];
        fd.append('file', constancia);

        var rfc = $('#rfc').val();

        $.ajax({
            url: '/cultivos/resources/rfc-excepcion/?rfc=' + rfc,
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false
        }).done(function (response) {
            $('#guardar-button').html("Guardar").prop('disabled', false);
            $('#rfc,#constancia-pdf').val('').limpiaErrores();
            alert('La excepción del RFC se guardó con éxito.');
            utils.cargaRfcs();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Error: No se pudo guardar el registro.');
            }
        });
    };

    handlers.verificaTamanoArchivo = function (e) {
        $.segalmex.archivos.verificaArchivo(e, 'pdf', 24 * 1024 * 1024);
    };
})(jQuery);