(function ($) {
    $.segalmex.namespace('segalmex.cultivos.administrar.estimulo');
    var utils = {};
    var handlers = {};
    var data = {
        estimulos: [],
        estimulosNuevos: [],
        tipoBusqueda: 'maiz-comercial'
    };

    $.segalmex.cultivos.administrar.estimulo.init = function () {
        utils.cargaCatalogos();
        utils.inicializaValidaciones();
    };

    utils.cargaCatalogos = function () {
        $.ajax({
            url: '/cultivos/resources/paginas/estimulo',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            data.tiposCultivo = response.tipos;
            data.cultivos = response.cultivos;
            data.ciclos = response.ciclos;
            data.estados = response.estados;
            $('#ciclo, #edicion-ciclo').actualizaCombo(response.ciclos);
            $('#cultivo, #edicion-cultivo').actualizaCombo(response.cultivos);
            $('#estado-estimulo,#edicion-estado').actualizaCombo(response.estados);
            utils.configuraPantalla();
            utils.cargaEstimulos();
        }).fail(function () {
            alert('Error: Al cargar los catálogos para estímulos.');
        });
    };

    utils.actualizaTipoCultivo = function (idTipoCultivo) {
        var tipos = [];
        var clave = data.tipoBusqueda === 'maiz-comercial' ? 'maiz' : data.tipoBusqueda;
        for (var i = 0; i < data.tiposCultivo.length; i++) {
            if (data.tiposCultivo[i].clave.includes(clave)) {
                tipos.push(data.tiposCultivo[i]);
            }
        }
        $('#' + idTipoCultivo).actualizaCombo(tipos);
    };

    handlers.actualizaTipoCultivoNuevo = function () {
        utils.actualizaTipoCultivo('tipo-cultivo');
    };

    handlers.actualizaTipoCultivoEdicion = function () {
        utils.actualizaTipoCultivo('edicion-tipo-cultivo');
    };

    utils.configuraPantalla = function () {
        $('#nuevo-button').click(utils.nuevoEstimulo);
        $('#regresar-button').click(utils.regresar);
        $('#guardar-button').click(utils.guarda);
        $('#edicion-button').click(utils.guardaEdicion);
        $('#cierra-edicion-button').click(handlers.cierraModalEdicion);
        $('#menu-bandejas a.nav-link').click(handlers.cambiaBandeja);
        $('input:radio[name=tipo-registro]').change(utils.cambiarTipoReigstro);
        $('input:radio[name=tipo-registro-edicion]').change(utils.cambiarTipoReigstro);
        handlers.actualizaTipoCultivoNuevo();
        $('#ciclo').change(utils.filtraRequerimientos);
        $('#tipo-estimulo').change(handlers.cambiaTipo);
        $('#edicion-tipo').change(handlers.cambiaTipo);
    };

    handlers.cambiaTipo = function (e) {
        e.preventDefault();
        var id = e.target.id;
        $('#contrato,#edicion-contrato').prop('disabled', true).val('').limpiaErrores();
        var tipo = $('#' + id).val();
        if (tipo !== '0' && tipo === 'contrato') {
            $('#contrato,#edicion-contrato').prop('disabled', false);
        }
    };

    handlers.cambiaBandeja = function (e) {
        e.preventDefault();
        var id = e.target.id.substring('muestra-'.length);
        if (data.tipoBusqueda === id) {
            return;
        }
        data.tipoBusqueda = id;
        $('#menu-bandejas a.nav-link').removeClass('active');
        $(e.target).addClass('active');
        handlers.actualizaTipoCultivoNuevo();
        utils.filtraRequerimientos();
    };

    utils.cambiarTipoReigstro = function (e) {
        e.preventDefault();
        $('#tipo-cultivo,#grupo,#edicion-tipo-cultivo,#edicion-grupo').prop('disabled', true);
        $('#grupo,#tipo-cultivo,#edicion-tipo-cultivo,#edicion-grupo').val('0');
        var name = e.target.name;
        var tipo = $('input:radio[name=' + name + ']:checked').val();
        switch (tipo) {
            case 'grupo-estimulo':
                switch (data.tipoBusqueda) {
                    case 'maiz-comercial':
                    case 'arroz':
                        $('#grupo,#edicion-grupo').actualizaCombo([{id: '1', nombre: 'Seleccionado'}]).val('1');
                        $('#grupo,#edicion-grupo').prop('disabled', true);
                        break;
                    case 'trigo':
                        $('#grupo,#edicion-grupo').actualizaCombo([{id: 'trigo', nombre: 'trigo'}, {id: 'trigo-cristalino', nombre: 'trigo-cristalino'}]);
                        $('#grupo,#edicion-grupo').prop('disabled', false);
                        break;
                }
                break;
            case 'tipo-cultivo':
                $('#tipo-cultivo,#edicion-tipo-cultivo').prop('disabled', false);
                break;
        }
    };

    utils.filtraRequerimientos = function () {
        var estimulos = [];
        estimulos = utils.getEstimulosFiltrados();
        var table = utils.creaDataTable(estimulos, 'estimulo-table');
        $('#table-estimulos').html(table);
        utils.configuraTablaResultados();
        $('#estimulo-table').on('click', 'button.editar-estimulo', handlers.muestraModalEdicion);
        $('#estimulo-table').on('click', 'button.eliminar-estimulo', utils.eliminaEstimulo);
    };


    utils.getEstimulosFiltrados = function () {
        var estimulos = [];
        var cultivo = data.tipoBusqueda === 'maiz-comercial' ? 'maiz' : data.tipoBusqueda;
        var ciclo = $.segalmex.get(data.ciclos, $('#ciclo').val());
        for (var i = 0; i < data.estimulos.length; i++) {
            var e = data.estimulos[i];
            if (e.aplicaEstimulo.includes(cultivo)) {
                if (ciclo) {
                    if (e.ciclo.clave === ciclo.clave) {
                        estimulos.push(e);
                    }
                } else {
                    estimulos.push(e);
                }
            }
        }
        return estimulos;
    };

    handlers.muestraModalEdicion = function (e) {
        e.preventDefault();
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('editar-estimulo-'.length), 10);
        handlers.actualizaTipoCultivoEdicion();
        $('input:radio[name=tipo-registro-edicion]').attr('checked', false);
        data.estimulo = $.segalmex.get(data.estimulos, idx);

        if (data.estimulo.aplicaEstimulo.includes('grupo-estimulo')) {
            $('#tipo-registro-edicion-1').prop('checked', true);
            $('input:radio[name=tipo-registro-edicion]').change();
            if (data.tipoBusqueda === 'trigo') {
                var grupo = data.estimulo.aplicaEstimulo.substring('grupo-estimulo:'.length, data.estimulo.aplicaEstimulo.length);
                $('#grupo,#edicion-grupo').val(grupo);
            }
        } else {
            $('#tipo-registro-edicion-2').prop('checked', true);
            var tipoCultivo = $.segalmex.get(data.tiposCultivo, data.estimulo.aplicaEstimulo.substring('tipo-cultivo:'.length, data.estimulo.aplicaEstimulo.length), 'clave');
            $('input:radio[name=tipo-registro-edicion]').change();
            $('#edicion-tipo-cultivo').val(tipoCultivo.id);
        }

        $('#edicion-ciclo').val(data.estimulo.ciclo.id);
        $('#edicion-fecha-inicio').val($.segalmex.date.isoToFecha(data.estimulo.fechaInicio));
        $('#edicion-fecha-fin').val($.segalmex.date.isoToFecha(data.estimulo.fechaFin));
        $('#edicion-cantidad').val(data.estimulo.cantidad);
        $('#edicion-orden').val(data.estimulo.orden);
        $('#edicion-estado').val(data.estimulo.estado.id);
        $('#edicion-tipo').val(data.estimulo.tipo);
        $('#edicion-contrato').val(data.estimulo.tipo === 'contrato' ? data.estimulo.contrato : '').prop('disabled', data.estimulo.tipo === 'contrato' ? false : true);
        $('#edicion-tipo-productor').val(data.estimulo.tipoProductor);
        $('#edit-estimulo-modal').modal('show');

    };

    handlers.cierraModalEdicion = function () {
        $('#edit-estimulo-modal').modal('hide');
        $('#edicion-ciclo, #edicion-cultivo, #edicion-estado').val('0').limpiaErrores();
        $('#edicion-fecha-inicio, #edicion-fecha-fin, #edicion-cantidad, #edicion-orden').val('').limpiaErrores();
        $('#edicion-tipo-cultivo').val('0').limpiaErrores();
    };


    utils.guarda = function () {
        $('#div-form-estimulos .valid-field').limpiaErrores();
        var errores = [];
        $('#div-form-estimulos .valid-field').valida(errores, false);
        $.segalmex.validaFechaInicialContraFinal(errores, 'fecha-inicio', 'fecha-fin');
        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            return;
        }
        var aplicaEstimulo = '';
        switch ($('input:radio[name=tipo-registro]:checked').val()) {
            case 'grupo-estimulo':
                var grupo = '';
                switch (data.tipoBusqueda) {
                    case 'maiz-comercial':
                        grupo = 'maiz';
                        break;
                    case 'trigo':
                        grupo = $('#grupo').val();
                        break;
                    case 'arroz':
                        grupo = 'arroz';
                        break;
                }
                aplicaEstimulo = $('input:radio[name=tipo-registro]:checked').val() + ':' + grupo;
                break;
            case 'tipo-cultivo':
                var tipo = $.segalmex.get(data.tiposCultivo, $('#tipo-cultivo').val());
                aplicaEstimulo = $('input:radio[name=tipo-registro]:checked').val() + ':' + tipo.clave;
                break;
        }

        var estado = $.segalmex.get(data.estados, $('#estado-estimulo').val());
        var ciclo = $.segalmex.get(data.ciclos, $('#ciclo').val());
        var estimulo = {
            ciclo: ciclo,
            fechaInicio: $.segalmex.date.fechaToIso($('#fecha-inicio').val()),
            fechaFin: $.segalmex.date.fechaToIso($('#fecha-fin').val()),
            cantidad: $('#cantidad').val(),
            orden: $('#orden').val(),
            estado: estado,
            aplicaEstimulo: aplicaEstimulo,
            tipo: $('#tipo-estimulo').val(),
            contrato: $('#contrato').val(),
            tipoProductor: $('#tipo-productor').val()

        };

        $.ajax({
            url: '/cultivos/resources/estimulo/',
            type: 'POST',
            data: JSON.stringify(estimulo),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            // Guardamos el registro
            alert('Estímulo guardado con éxito.');
            data.estimulosNuevos.push(response);
            utils.limpiaEstimulo();
            if (data.tipoBusqueda === 'arroz') {
                $('#tipo-estimulo').prop('disabled', true).val('factura');
            }
            $('#div-table-nuevos').show();
            var table = utils.creaDataTable(data.estimulosNuevos, 'estimulo-table-nuevos');
            $('#table-estimulos-nuevos').html(table);
            $('#estimulo-table-nuevos').on('click', 'button.editar-estimulo', handlers.muestraModalEdicion);
            $('#estimulo-table-nuevos').on('click', 'button.eliminar-estimulo', utils.eliminaEstimulo);
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Ocurrió un error al guardar el registro.');
            }
        });

    };

    utils.guardaEdicion = function () {
        $('#edit-estimulo-modal .valid-field').limpiaErrores();
        var errores = [];
        $('#edit-estimulo-modal .valid-field').valida(errores, false);
        $.segalmex.validaFechaInicialContraFinal(errores, 'edicion-fecha-inicio', 'edicion-fecha-fin');
        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            return;
        }

        var aplicaEstimulo = '';
        switch ($('input:radio[name=tipo-registro-edicion]:checked').val()) {
            case 'grupo-estimulo':
                var grupo = '';
                switch (data.tipoBusqueda) {
                    case 'maiz-comercial':
                        grupo = 'maiz';
                        break;
                    case 'trigo':
                        grupo = $('#edicion-grupo').val();
                        break;
                    case 'arroz':
                        grupo = 'arroz';
                        break;
                }
                aplicaEstimulo = $('input:radio[name=tipo-registro-edicion]:checked').val() + ':' + grupo;
                break;
            case 'tipo-cultivo':
                var tipo = $.segalmex.get(data.tiposCultivo, $('#edicion-tipo-cultivo').val());
                aplicaEstimulo = $('input:radio[name=tipo-registro-edicion]:checked').val() + ':' + tipo.clave;
                break;
        }

        var estimulo = {
            id: data.estimulo.id,
            ciclo: {id: $('#edicion-ciclo').val()},
            fechaInicio: $.segalmex.date.fechaToIso($('#edicion-fecha-inicio').val()),
            fechaFin: $.segalmex.date.fechaToIso($('#edicion-fecha-fin').val()),
            cantidad: $('#edicion-cantidad').val(),
            orden: $('#edicion-orden').val(),
            estado: {id: $('#edicion-estado').val()},
            aplicaEstimulo: aplicaEstimulo,
            tipo: $('#edicion-tipo').val(),
            contrato: $('#edicion-contrato').val(),
            tipoProductor: $('#edicion-tipo-productor').val()
        };

        $.ajax({
            url: '/cultivos/resources/estimulo',
            type: 'PUT',
            data: JSON.stringify(estimulo),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            // Guardamos el registro
            alert('Estímulo editado con éxito.');
            utils.cargaEstimulos();
            utils.regresar();
            handlers.cierraModalEdicion();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Ocurrió un error al guardar el registro.');
            }
        });


    };

    utils.eliminaEstimulo = function (e) {
        if (!confirm("¿Seguro que desea eliminar el estímulo?")) {
            return;
        }
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-estimulo-'.length), 10);
        var estimulo = {
            id: idx
        };

        $.ajax({
            url: '/cultivos/resources/estimulo',
            type: 'DELETE',
            data: JSON.stringify(estimulo),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            // Guardamos el registro
            alert('Estímulo eliminado con éxito.');
            utils.cargaEstimulos();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Ocurrió un error al eliminar el registro.');
            }
        });


    };

    utils.inicializaValidaciones = function () {
        $(".valid-field").configura();

        $('#cantidad, #orden, #edicion-cantidad, #edicion-orden').configura({
            type: 'number'
        });

        $('#fecha-inicio, #fecha-fin, #edicion-fecha-inicio, #edicion-fecha-fin').configura({
            type: 'date',
            before: true
        });

        $('#fecha-inicio, #fecha-fin, #edicion-fecha-inicio, #edicion-fecha-fin').datepicker({
            language: "es",
            autoclose: true,
            forceParse: false,
            format: 'dd/mm/yyyy',
            yearRange: "c-10:c"
        });

        $('#contrato,#edicion-contrato').configura({
            pattern: /^[A-Z]{3}-[A-Z]{2}[0-9]{2}-[A-Z0-9]{3}-\d{6}-(P|C|S|D|E|A|B)-\d{3}$/,
            minlength: 25,
            maxlength: 25
        });

        $('.valid-field').validacion();
    };

    utils.nuevoEstimulo = function () {
        if ($('#ciclo').val() === '0') {
            alert('Es necesario seleccionar el ciclo.');
            return;
        }
        utils.limpiaEstimulo();
        $('input:radio[name=tipo-registro]').change();
        $('#muestra-arroz,#muestra-maiz-comercial,#muestra-trigo').addClass('disabled');
        $('#table-estimulos,#div-nuevo').hide();
        $('#div-form-estimulos').show();
        $('#tipo-estimulo').val('0').limpiaErrores().prop('disabled', false);
        if (data.tipoBusqueda === 'arroz') {
            $('#tipo-estimulo').prop('disabled', true).val('factura');
            $('#tipo-estimulo').val('factura');
        }
    };

    utils.regresar = function () {
        $('#table-estimulos,#div-nuevo').show();
        $('#div-form-estimulos').hide();
        $('#table-estimulos-nuevos').html('');
        $('#div-table-nuevos').hide();
        $('#muestra-arroz,#muestra-maiz-comercial,#muestra-trigo').removeClass('disabled');
        utils.limpiaEstimulo();
        if (data.estimulosNuevos.length > 0) {
            utils.cargaEstimulos();
            data.estimulosNuevos = [];
        }
    };

    utils.limpiaEstimulo = function () {
        $('#tipo-cultivo,#estado-estimulo,#tipo-estimulo,#tipo-productor').val('0').limpiaErrores();
        if (data.tipoBusqueda === 'trigo') {
            $('#grupo').val('0').limpiaErrores();
        }
        $('#fecha-inicio, #fecha-fin, #cantidad, #orden').val('').limpiaErrores();
        $('#tipo-cultivo').limpiaErrores();
        $('#contrato').prop('disabled', true).val('').limpiaErrores();
    };

    utils.cargaEstimulos = function (e) {
        $.ajax({
            url: '/cultivos/resources/estimulo/',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            data.estimulos = response;
            utils.filtraRequerimientos();
        }).fail(function () {
            alert('Error: Al cargar los estímulos.');
        });
    };

    utils.creaDataTable = function (registros, id) {
        var buffer = `
            <table id="${id}" class="table table-bordered table-hover table-striped">
              <thead class="thead-dark">
                <tr>
                  <th>#</th>
                  <th>Ciclo</th>
                  <th>Grupo estímulo</th>
                  <th>Inicio-Fin</th>
                  <th>Estado</th>
                  <th>Cantidad</th>
                  <th>Orden</th>
                  <th>Tipo</th>
                  <th>Productor</th>`;
        if (id === 'estimulo-table') {
            buffer += `<th class="text-center">Acciones</th>`;
        }
        buffer += `</tr>
              </thead>
              <tbody>
        `;

        for (var i = 0; i < registros.length; i++) {
            var a = registros[i];
            buffer += `
                <tr>
                    <td>${i + 1}</td>
                    <td>${a.ciclo.nombre}</td>
                    <td>${a.aplicaEstimulo}</td>
                    <td>${$.segalmex.date.isoToFechaHora(a.fechaInicio)}
                    <br/>${$.segalmex.date.isoToFechaHora(a.fechaFin)}
                    </td>
                    <td>${a.estado.nombre}</td>
                    <td>${a.cantidad}</td>
                    <td>${a.orden}</td>
                    <td>${a.tipo}</td>
                    <td>${a.tipoProductor}</td>`;
            if (id === 'estimulo-table') {
                buffer +=
                        `
                        <td class="text-center">
                            <button id="editar-estimulo-${a.id}
                            " class="btn btn-primary btn-sm editar-estimulo">
                            <i class="fas fa-pen"></i></button>
                        <button id="eliminar-estimulo-${a.id}
                        " class="btn btn-danger btn-sm eliminar-estimulo">
                        <i class="fa fa-minus-circle"></i></button>

                        </td>`;
            }

            buffer += ` </tr>
            `;
        }
        buffer += `
            </tbody>
            </table>
        `;

        return buffer;
    };

    utils.configuraTablaResultados = function () {
        var aoColumns = [{"bSortable": false}, {"bSortable": true}, {"bSortable": true},
            {"bSortable": true}, {"bSortable": true}, {"bSortable": true}, {"bSortable": true}, {"bSortable": true}, {"bSortable": true}, {"bSortable": true}];
        utils.configuraTablaEstimulos('estimulo-table', aoColumns);
    };

    utils.configuraTablaEstimulos = function (tablaId, aoColumns) {
        var table = $('#' + tablaId).DataTable({
            "oLanguage": {
                "sEmptyTable": "No se encontró información para mostrar en la tabla",
                "sSearch": "Buscar ",
                "sZeroRecords": "No se encontraron resultados."
            },
            "bFilter": true,
            "bInfo": false,
            "bPaginate": false,
            "aoColumns": aoColumns,
            "aoColumnDefs": [{"bSortable": false, "aTargets": ["no-sort"]}]
        });
        table.on('order.dt search.dt', function () {
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).order([[1, 'asc']]).draw(false);
        $("#" + tablaId + "_filter input").addClass('input-buscar');
        $(".dataTables_empty").attr("colspan", "100%");
    };


})(jQuery);