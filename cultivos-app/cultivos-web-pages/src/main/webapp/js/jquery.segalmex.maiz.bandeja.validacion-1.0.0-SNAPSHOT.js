/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


(function ($) {
    $.segalmex.namespace('segalmex.maiz.inscripcion.validacion');

    var data = {
        tipoInscripcion: 'contratos',
        inscripcion: null,
        entradas: [],
        cultivos: [],
        ciclos: [],
        cultivoSeleccionado: '',
        solicitado: false
    };
    var handlers = {};
    var utils = {};

    $.segalmex.maiz.inscripcion.validacion.init = function () {
        utils.cargaCatalogos();
    };

    utils.setCultivo = function () {
        var cultivo = $.segalmex.getParameterByName('cultivo');
        switch (cultivo) {
            case 'maiz-comercial':
            case 'trigo':
                $('#button-' + cultivo).click();
                break;
            case 'arroz':
                $('#button-' + cultivo).click();
                $('#muestra-productores').click();
                break;
            default:
                alert('No hay cultivo seleccionado.');
        }
    };

    $.segalmex.maiz.inscripcion.validacion.cargaDatos = function (fn) {
        var datos = {
            cultivo: data.cultivoSeleccionado
        };
        if (data.cultivoSeleccionado === '') {
            delete datos.cultivo;
        }
        $.ajax({
            url: '/cultivos/resources/paginas/bandejas/validacion/' + data.tipoInscripcion,
            data: datos,
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            data.entradas = response;
            fn(response);
        }).fail(function () {
            alert('Error: No se pudo obtener la lista de inscripciones de contrato.');
        });
        return data.entradas;
    };

    $.segalmex.maiz.inscripcion.validacion.limpiar = function () {
        data.inscripcion = null;
        $('#detalle-elemento').html('');
        $('#detalles-entrada,#botones-entrada,#table-dato-capturado,#div-datos-capturados-agrupados').hide();
        $('#inscripcion-datos-capturados tbody').html('<tr><td colspan="100%"></td></tr>');
        $('#inscripcion-datos-capturados-correctos tbody').html('<tr><td colspan="100%"></td></tr>');
        $('#inscripcion-datos-capturados-incorrectos tbody').html('<tr><td colspan="100%"></td></tr>');
    };

    $.segalmex.maiz.inscripcion.validacion.mostrar = function (e) {
        e.preventDefault();
        // $('#button-cancelacion,#supervision-inscripcion-button').hide();
        var folio = e.target.id.substring('link-id-'.length);
        data.inscripcion = $.segalmex.get(data.entradas, folio, 'folio');
        var actual = $.segalmex.common.bandejas.actual();

        // Obtenemos la inscripcion
        var tipoInscripcion = data.tipoInscripcion;
        $.ajax({
            url: '/cultivos/resources/' + tipoInscripcion + '/maiz/inscripcion/' + data.inscripcion.uuid,
            data: {datos: true, contratos: true, archivos: true, comentarios: true, productorCiclo: true, expand: 3},
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            data.inscripcion = response;
            // Mostramos el detalle
            var cultivoClave = response.cultivo.clave;
            utils.construyeInscripcion(response, tipoInscripcion);
            utils.configuraBotones(tipoInscripcion, actual, cultivoClave);
            utils.muestraDatosCapturados(response.datos, actual);
            $('.detalle-elemento-form').hide();
            switch (actual) {
                case 'validacion':
                case 'validacion-complemento':
                    utils.configuraTablaCaptura('#inscripcion-datos-capturados');
                    $('#table-dato-capturado').show();
                    break;
                case 'supervision':
                case 'revalidacion':
                case 'espera':
                    utils.configuraTablaCaptura('#inscripcion-datos-capturados-correctos');
                    utils.configuraTablaCaptura('#inscripcion-datos-capturados-incorrectos');
                    $('#div-datos-capturados-agrupados').show();
                    $('#btn-datos-correctos').html('Mostrar <span class="fas fa-check-circle text-secondary" aria-hidden="true">');
                    $('#div-tabla-incorrectos').hide();
                    break;
                case 'revalidacion-complemento':
                    utils.configuraTablaCaptura('#inscripcion-datos-capturados-correctos');
                    utils.configuraTablaCaptura('#inscripcion-datos-capturados-incorrectos');
                    $('#div-datos-capturados-agrupados').show();
                    $('#btn-datos-correctos').html('Mostrar <span class="fas fa-check-circle text-secondary" aria-hidden="true">');
                    $('#div-tabla-incorrectos').hide();
                    break;
            }
            if (tipoInscripcion === 'contratos' && response.contratosProductor.length > 0) {
                $('#button-corregir-productores').show();
            }
            $('#lista-entradas').hide();
            $('#detalle-elemento-' + actual).show();
            $('#detalles-entrada,#botones-entrada').show();
        }).fail(function () {
            alert('Error: No fue posible obtener el registro.');
        });
    };

    utils.muestraDatosCapturados = function (datos, actual) {
        switch (actual) {
            case "revalidacion":
            case "espera":
            case 'supervision':
                //Filtramos datos por correctos e incorrectos
                var datosCorrectos = utils.filtradatosCapturados(datos, true);
                var datosIncorrectos = utils.filtradatosCapturados(datos, false);
                $('#inscripcion-datos-capturados-correctos tbody').html(utils.creaTablaCaptura(datosCorrectos, ['revalidacion', 'espera', 'supervision'].indexOf(actual) !== -1));
                $('#inscripcion-datos-capturados-incorrectos tbody').html(utils.creaTablaCaptura(datosIncorrectos, ['revalidacion', 'espera', 'supervision'].indexOf(actual) !== -1));
                break;
            case 'validacion-complemento':
                utils.filtraDatosCobertura(datos);
                $('#inscripcion-datos-capturados tbody').html(utils.creaTablaCaptura(utils.filtraDatosIncorrectos(datos), ['revalidacion', 'espera', 'supervision'].indexOf(actual) !== -1));
                break;
            case 'revalidacion-complemento':
                // filtramos los datos de IAR y los generales.
                utils.filtraDatosCobertura(datos);
                var datosCorrectos = utils.filtradatosCapturados(data.datosCapturadosIar, true);
                var datosIncorrectos = utils.filtradatosCapturados(data.datosCapturadosIar, false);
                $('#inscripcion-datos-capturados-correctos tbody').html(utils.creaTablaCaptura(datosCorrectos, ['revalidacion', 'espera', 'supervision', 'revalidacion-complemento'].indexOf(actual) !== -1));
                $('#inscripcion-datos-capturados-incorrectos tbody').html(utils.creaTablaCaptura(datosIncorrectos, ['revalidacion', 'espera', 'supervision', 'revalidacion-complemento'].indexOf(actual) !== -1));
                break;
            default :
                $('#inscripcion-datos-capturados tbody').html(utils.creaTablaCaptura(datos, ['revalidacion', 'espera', 'supervision'].indexOf(actual) !== -1));
        }
    };

    utils.filtraDatosIncorrectos = function (datos) {
        var incorrectos = [];
        for (var i = 0; i < datos.length; i++) {
            var d = datos[i];
            if (!d.correcto) {
                incorrectos.push(d);
            }
        }
        return incorrectos;
    };

    utils.filtraDatosCobertura = function (datos) {
        data.datosCapturadosGeneral = [];
        data.datosCapturadosIar = [];
        for (var i = 0; i < datos.length; i++) {
            var d = datos[i];
            if (d.grupo === undefined || d.grupo === 'general') {
                data.datosCapturadosGeneral.push(d);
            } else if (d.grupo === 'Instrumentos de administración de riesgos') {
                data.datosCapturadosIar.push(d);
            }
        }
    };

    utils.filtradatosCapturados = function (datos, correcto) {
        var datosFiltrados = [];
        for (var i = 0; i < datos.length; i++) {
            var d = datos[i];
            if (d.correcto && correcto) {
                datosFiltrados.push(d);
            } else if (!d.correcto && !correcto) {
                datosFiltrados.push(d);
            }
        }
        return datosFiltrados;
    };

    handlers.muestraDatosCorrectos = function (e) {
        e.preventDefault();
        if ($('#div-tabla-incorrectos').is(":visible")) {
            $('#inscripcion-datos-capturados-correctos tbody input.valid-field').limpiaErrores();
            var errores = [];
            $('#inscripcion-datos-capturados-correctos tbody input.valid-field').valida(errores, true);
            if (errores.length !== 0) {
                return;
            }

            $('#btn-datos-correctos').html('Mostrar <span class="fas fa-check-circle text-secondary" aria-hidden="true">');
            $('#div-tabla-incorrectos').hide();
        } else {
            $('#btn-datos-correctos').html('Ocultar <span class="fas fa-check-circle text-secondary" aria-hidden="true">');
            $('#div-tabla-incorrectos').show();
        }
    };

    $.segalmex.maiz.inscripcion.validacion.construyeTabla = function (lista) {
        return $.segalmex.common.bandejas.vista.construyeTabla({
            id: 'table-inscripciones',
            lista: lista,
            link: true,
            btnActualizar: true,
            tipoInscripcion: data.tipoInscripcion,
            filtro: true
        });
    };

    $.segalmex.maiz.inscripcion.validacion.configuraTabla = function () {
        $.segalmex.common.bandejas.vista.configuraTablaInscripcion('table-inscripciones', data.tipoInscripcion);
        $('#cultivo').actualizaCombo(data.cultivos, {value: 'clave'});
        $('#ciclo').actualizaCombo(data.ciclos, {value: 'clave'});
    };

    handlers.cambiaBandeja = function (e) {
        e.preventDefault();
        var id = e.target.id.substring('muestra-'.length);
        if (data.tipoInscripcion === id) {
            return;
        }
        data.tipoInscripcion = id;
        data.folio = null;

        $('#menu-bandejas a.nav-link').removeClass('active');
        $(e.target).addClass('active');
        $.segalmex.common.bandejas.init({
            configuracion: utils.configuracionBandejas(),
            namespace: $.segalmex.maiz.inscripcion.validacion,
            tipoInscripcion: data.tipoInscripcion,
            cultivo: data.cultivoSeleccionado
        });
    };

    utils.validaPrediosSolicitados = function (predios) {
        for (var i = 0; i < predios.length; i++) {
            var p = predios[i];
            if (p.solicitado === 'solicitado') {
                data.solicitado = true;
                $('#button-flecha').prop('disabled', false);
                return;
            }
        }
        data.solicitado = false;
        $('#button-flecha').prop('disabled', true);

    };

    handlers.validaPositivamente = function (e) {
        e.preventDefault();
        var capturados;
        if (!utils.realizaValidaciones()) {
            return;
        }

        switch (data.tipoInscripcion) {
            case 'facturas':
                if (!utils.realizaValidacionesFactura(data.inscripcion)) {
                    return;
                }
                break;
        }

        if (data.tipoInscripcion === 'productores') {
            if (e.target.id === 'button-validacion-positiva-2' && data.solicitado) {
                alert('Error: Se requiere indicar si la validación positiva es con o sin rendimiento solicitado.');
                return;
            }

            var isSolicitado = null;
            if (e.target.id === 'button-validacion-positiva-con') {
                isSolicitado = true;
            } else if (e.target.id === 'button-validacion-positiva-sin') {
                isSolicitado = false;
            }
            capturados = {
                datosCapturados: utils.getDatosCapturados(),
                solicitado: isSolicitado
            };
        } else {
            capturados = utils.getDatosCapturados();
        }

        if (data.tipoInscripcion === 'facturas') {
            capturados = {
                datosCapturados: utils.getDatosCapturados()
            };
        }

        $.ajax({
            url: '/cultivos/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + data.inscripcion.uuid + '/validacion/positiva/',
            data: JSON.stringify(capturados),
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('El registro ha sido validado positivamente.');
            $.segalmex.common.bandejas.actualizar();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Error: No fue posible validar positivamente.');
            }
        });
    };

    handlers.validaPositivaFacturaProductor = function (e) {
        e.preventDefault();
        if (!utils.realizaValidaciones()) {
            return;
        }

        if (!utils.realizaValidacionesFactura(data.inscripcion)) {
            return;
        }

        datos = {
            datosCapturados: utils.getDatosCapturados()
        };

        utils.enviaValidacionPositiva(datos);

    };

    handlers.validaPositivaFacturaFechaPago = function (e) {
        e.preventDefault();
        if (!utils.realizaValidaciones()) {
            return;
        }

        if (!utils.realizaValidacionesFactura(data.inscripcion)) {
            return;
        }

        datos = {
            datosCapturados: utils.getDatosCapturados(),
            fechaPago: true
        };
        utils.enviaValidacionPositiva(datos);
    };

    handlers.validaNegativamente = function (e) {
        utils.enviaValidacionNegativa(e, 'validacion/negativa');
    };

    handlers.nuevoComentario = function (e) {
        $('#comentario-nuevo-modal').modal('show');
        $('#contenido-comentario').val('');
    }

    handlers.agregaComentario = function (e) {
        $('#contenido-comentario').limpiaErrores();
        var errores = [];
        $('#contenido-comentario').valida(errores, true);
        if (errores.length > 0) {
            return;
        }

        var comentario = {
            tipo: $.segalmex.common.bandejas.actual(),
            contenido: $('#contenido-comentario').val()
        }

        $.ajax({
            url: '/cultivos/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + data.inscripcion.uuid + '/comentario/',
            data: JSON.stringify(comentario),
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            $('#comentario-nuevo-modal').modal('hide');
        }).fail(function () {
            alert('Error: No fue posible agregar el comentario.');
            $('#botones-entrada button').prop('disabled', false);
        });
    }

    handlers.cancela = function (e) {
        $('#comentario-cancelacion-factura').val('');
        $('#confirmacion-cancelacion-modal').modal('show');
    }

    handlers.confirmaCancelacionFactura = function (e) {
        e.preventDefault();
        $('#confirmacion-cancelacion-modal button').prop('disabled', true);
        var errores = [];
        $('#confirmacion-cancelacion-modal .valid-field').valida(errores, true);
        if (errores.length > 0) {
            $('#confirmacion-cancelacion-modal button').prop('disabled', false);
            return;
        }

        $.ajax({
            url: '/cultivos/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + data.inscripcion.uuid + '/cancelacion/',
            data: JSON.stringify({comentarioEstatus: $('#comentario-cancelacion-factura').val()}),
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('El registro ha sido cancelado.');
            $('#confirmacion-cancelacion-modal').modal('hide');
            $('#confirmacion-cancelacion-modal button').prop('disabled', false);
            $.segalmex.common.bandejas.actualizar();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Error: No fue posible validar negativamente.');
            }

            $('#confirmacion-cancelacion-modal button').prop('disabled', false);
        });
    };

    handlers.regresar = function (e) {
        e.preventDefault();

        $.segalmex.maiz.inscripcion.validacion.limpiar();
        $('#lista-entradas').show();
    };

    handlers.marcaTodosCorrectos = function (e) {
        e.preventDefault();
        utils.marcaTodosCorrectos('#inscripcion-datos-capturados');
    };

    handlers.marcaTodosIncorrectos = function (e) {
        e.preventDefault();
        utils.marcaTodosIncorrectos('#inscripcion-datos-capturados');
    };

    handlers.marcaTodosCorrectosTablaIncorrectos = function (e) {
        e.preventDefault();
        utils.marcaTodosCorrectos('#inscripcion-datos-capturados-incorrectos');
    };

    handlers.marcaTodosIncorrectosTablaIncorrectos = function (e) {
        e.preventDefault();
        utils.marcaTodosIncorrectos('#inscripcion-datos-capturados-incorrectos');
    };

    handlers.marcaTodosCorrectosTablaCorrectos = function (e) {
        e.preventDefault();
        utils.marcaTodosCorrectos('#inscripcion-datos-capturados-correctos');
    };

    handlers.marcaTodosIncorrectosTablaCorrectos = function (e) {
        e.preventDefault();
        utils.marcaTodosIncorrectos('#inscripcion-datos-capturados-correctos');
    };

    utils.marcaTodosCorrectos = function (id) {
        $(id + ' input.form-check-input').prop('checked', false);
        $(id + ' input.comentario-dato-capturado').val('').prop('disabled', true);
        $(id + ' input.form-check-input[value=true]').prop('checked', true);
    };

    utils.marcaTodosIncorrectos = function (id) {
        $(id + ' input.form-check-input').prop('checked', false);
        $(id + ' input.comentario-dato-capturado').prop('disabled', false);
        $(id + ' input.form-check-input[value=false]').prop('checked', true);
    };

    handlers.radioEstatusChange = function (e) {
        var idval = e.target.id.split('.');
        $('#comentario-' + idval[0]).prop('disabled', idval[1] === 'true').val('').limpiaErrores();
    };

    handlers.noElegible = function (e) {
        e.preventDefault();
        $('#comentario-cancelacion').val('');
        $('#confirmacion-modal').modal('show');
    };

    handlers.desisteCancelacion = function (e) {
        $('#comentario-cancelacion').val('');
        $('#confirmacion-modal').modal('hide');
    };

    handlers.confirmaNoElegible = function (e) {
        e.preventDefault();
        if ($('#comentario-cancelacion').val() === '') {
            alert('Comentario es campo obligatorio');
            return;
        }
        $.ajax({
            url: '/cultivos/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + data.inscripcion.uuid + '/no-elegible/',
            data: JSON.stringify({comentarioEstatus: $('#comentario-cancelacion').val()}),
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('El registro ha sido marcado como no elegible.');
            $('#confirmacion-modal').modal('hide');
            $('#confirmacion-modal button').prop('disabled', false);
            $.segalmex.common.bandejas.actualizar();
        }).fail(function () {
            alert('Error: No fue posible cancelar el registro.');
            $('#confirmacion-modal button').prop('disabled', false);
        });
    };

    handlers.enviaSupervision = function (e) {
        utils.enviaValidacionNegativa(e, 'supervision');
    };

    utils.enviaValidacionNegativa = function (e, tipo) {
        e.preventDefault();
        $(e.target).prop('disabled', true);
        if (!utils.isEstatusSeleccionado()) {
            alert('Error: Es necesario indicar el estatus de todos los campos.');
            $(e.target).prop('disabled', false);
            return;
        }

        if (utils.isAprobada()) {
            switch (tipo) {
                case 'validacion/negativa':
                    alert('Error: Para validar negativamente se requiere de al menos un campo erróneo.');
                    break;
                case 'supervision':
                    alert('Error: Para enviar a supervisión se requiere de al menos un campo erróneo.');
                    break;
            }
            $(e.target).prop('disabled', false);
            return;
        }

        if (!utils.validaTablaCaptura()) {
            $(e.target).prop('disabled', false);
            return;
        }
        var capturados = utils.getDatosCapturados();
        $.ajax({
            url: '/cultivos/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + data.inscripcion.uuid + '/' + tipo + '/',
            data: JSON.stringify(capturados),
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            switch (tipo) {
                case 'validacion/negativa':
                    alert('El registro ha sido validado negativamente.');
                    break;
                case 'supervision':
                    alert('El registro ha sido enviado a supervisión.');
                    break;
            }
            $.segalmex.common.bandejas.actualizar();
            $(e.target).prop('disabled', false);
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                switch (tipo) {
                    case 'validacion/negativa':
                        alert('Error: No fue posible validar negativamente.');
                        break;
                    case 'supervision':
                        alert('Error: No fue posible enviar el registro a supervisión.');
                        break;
                }
            }
            $(e.target).prop('disabled', false);
        });
    };

    utils.realizaValidaciones = function () {
        if (!utils.isEstatusSeleccionado()) {
            alert('Error: Es necesario indicar el estatus de todos los campos');
            return false;
        }

        if (!utils.isAprobada()) {
            alert('Error: Para validar positivamente se requiere el estatus correcto de todos los campos.');
            return false;
        }

        if (!utils.validaTablaCaptura()) {
            return false;
        }

        return true;
    }

    utils.realizaValidacionesFactura = function (inscripcion) {
        switch (inscripcion.tipoFactura) {
            case 'productor':
                if (!utils.isToneladasFacturablesPositiva(inscripcion)) {
                    alert('Error: Es necesario que las toneladas facturables no sean 0.');
                    return false;
                }
                break;
            case 'global':
            case 'global-empresa':
                break;
            default:
                throw 'Error de tipo de factura.';
        }
        return true;
    }

    utils.isToneladasFacturablesPositiva = function (inscripcion) {
        var pc = inscripcion.productorCiclo;
        var disponibles = inscripcion.toneladasDisponiblesFactura !== undefined ? inscripcion.toneladasDisponiblesFactura : inscripcion.cfdi.totalToneladas;
        var facturables = Math.min(pc.toneladasTotales - pc.toneladasFacturadas, inscripcion.cantidad, disponibles);
        return facturables > 0;
    }


    utils.isAprobada = function () {
        var datos = [];
        var datosCapturados = $('#inscripcion-datos-capturados input[type=radio]:checked');
        var datosCorrectos = $('#inscripcion-datos-capturados-incorrectos input[type=radio]:checked');
        var datosIncorrectos = $('#inscripcion-datos-capturados-correctos input[type=radio]:checked');

        for (var i = 0; i < datosCapturados.length; i++) {
            var d = datosCapturados[i];
            datos.push(d);
        }

        for (var i = 0; i < datosCorrectos.length; i++) {
            var d = datosCorrectos[i];
            datos.push(d);
        }

        for (var i = 0; i < datosIncorrectos.length; i++) {
            var d = datosIncorrectos[i];
            datos.push(d);
        }

        for (var i = 0; i < datos.length; i++) {
            if ($(datos[i]).val() === 'false') {
                return false;
            }
        }
        return true;
    };

    utils.getDatosCapturados = function () {
        var datos = [];
        var datosCapturados = $('#inscripcion-datos-capturados input[type=radio]:checked');
        var datosIncorrectos = $('#inscripcion-datos-capturados-incorrectos input[type=radio]:checked');
        var datosCorrectos = $('#inscripcion-datos-capturados-correctos input[type=radio]:checked');

        for (var i = 0; i < datosCapturados.length; i++) {
            var d = datosCapturados[i];
            datos.push(d);
        }

        for (var i = 0; i < datosCorrectos.length; i++) {
            var d = datosCorrectos[i];
            datos.push(d);
        }

        for (var i = 0; i < datosIncorrectos.length; i++) {
            var d = datosIncorrectos[i];
            datos.push(d);
        }
        var dcs = [];
        for (var i = 0; i < datos.length; i++) {
            var dato = datos[i];
            var correcto = $(dato).val();
            var clave = dato.id.split('.')[0];
            var d = {clave: clave, correcto: correcto};
            if (correcto === 'false') {
                d.comentario = $('#comentario-' + clave).val();
            }
            dcs.push(d);
        }
        return dcs;
    };

    utils.getTamanioDatosCapturados = function () {
        var datosCapturados = $('#inscripcion-datos-capturados input[type=radio]:checked');
        var datosCorrectos = $('#inscripcion-datos-capturados-incorrectos input[type=radio]:checked');
        var datosIncorrectos = $('#inscripcion-datos-capturados-correctos input[type=radio]:checked');
        return (datosCapturados.length + datosCorrectos.length + datosIncorrectos.length);
    };

    utils.isEstatusSeleccionado = function () {
        return  utils.getTamanioDatosCapturados() === data.inscripcion.datos.length;
    };

    utils.configuracionBandejas = function () {
        switch (data.tipoInscripcion) {
            case 'contratos':
                return {
                    idBandeja: 'entradas-bandeja',
                    idLista: 'lista-entradas',
                    defaultBandeja: 'validacion',
                    entradas: [
                        {nombre: 'Validación', titulo: 'Validación de contratos', prefijoId: 'validacion', descripcion: 'Lista de registros de contratos para validación.'},
                        {nombre: 'Revalidación', titulo: 'Revalidación de contratos', prefijoId: 'revalidacion', descripcion: 'Lista de registros de contratos para revalidación .'},
                        {nombre: 'Complementos', titulo: 'Complemento de contratos', prefijoId: 'solicitud-complemento', descripcion: 'Lista de registros de contratos para complementos.'},
                        {nombre: 'Validación complemento', titulo: 'Validación de complemento', prefijoId: 'validacion-complemento', descripcion: 'Lista de registros de contratos para validar complementos.'},
                        {nombre: 'Revalidación complemento', titulo: 'Revalidación de complemento', prefijoId: 'revalidacion-complemento', descripcion: 'Lista de registros de contratos para revalidar complementos.'}
                    ]

                };
            case 'productores':
                return {
                    idBandeja: 'entradas-bandeja',
                    idLista: 'lista-entradas',
                    defaultBandeja: 'validacion',
                    entradas: [
                        {nombre: 'Validación', titulo: 'Validación de productores', prefijoId: 'validacion', descripcion: 'Lista de registros de productores para validación.'},
                        {nombre: 'Revalidación', titulo: 'Revalidación de productores', prefijoId: 'revalidacion', descripcion: 'Lista de registros de productores para revalidación.'},
                        {nombre: 'Espera', titulo: 'Revalidación en espera de productores', prefijoId: 'espera', descripcion: 'Lista de registros de productores para espera de revalidación.'},
                        {nombre: 'Supervisión', titulo: 'Supervisión de productores', prefijoId: 'supervision', descripcion: 'Lista de registros de productores para supervisión .'}
                    ]
                };
            case 'facturas':
                return {
                    idBandeja: 'entradas-bandeja',
                    idLista: 'lista-entradas',
                    defaultBandeja: 'validacion',
                    entradas: [
                        {nombre: 'Validación', titulo: 'Validación de facturas', prefijoId: 'validacion', descripcion: 'Lista de registros de facturas para validación.'},
                        {nombre: 'Revalidación', titulo: 'Revalidación de facturas', prefijoId: 'revalidacion', descripcion: 'Lista de registros de facturas para revalidación .'}
                    ]
                };
        }
    };

    utils.creaTablaCaptura = function (datos, estatus) {
        var buffer = [];
        var grupo = 'general';
        for (var i = 0; i < datos.length; i++) {
            var dato = datos[i];
            if (dato.grupo === undefined) {
                dato.grupo = 'General';
            }

            if (dato.grupo !== grupo) {
                buffer.push('<tr class="table-secondary"><th colspan="4">' + dato.grupo + '</th></tr>');
                grupo = dato.grupo;
            }
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(dato.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push('<input type="text" class="form-control" value="' + (dato.valorTexto ? dato.valorTexto : dato.valor) + '" readonly="readonly"/>');
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push('<div class="form-check form-check-inline">');
            buffer.push('<input class="form-check-input" type="radio" name="correcto-' + dato.clave + '" id="' + dato.clave + '.true" value="true"' + (estatus && dato.correcto ? ' checked="checked" ' : '') + '/>');
            buffer.push('<label class="form-check-label" for="' + dato.clave + '.true"><i class="fas fa-check-circle text-success"></i></label>');
            buffer.push('</div>');
            buffer.push('<div class="form-check form-check-inline">');
            buffer.push('<input class="form-check-input" type="radio" name="correcto-' + dato.clave + '" id="' + dato.clave + '.false" value="false"' + (estatus && !dato.correcto ? ' checked="checked" ' : '') + '/>');
            buffer.push('<label class="form-check-label" for="' + dato.clave + '.false"><i class="fas fa-times-circle text-danger"></i></label>');
            buffer.push('</div>');
            buffer.push('</td>');
            buffer.push('<td><div class="form-inline">');
            buffer.push('<label for="comentario-' + dato.clave + '" class="sr-only">Comentario de ' + dato.nombre + '</label>');
            buffer.push('<input id="comentario-' + dato.clave + '" type="text" class="valid-field form-control comentario-dato-capturado" '
                    + (estatus && !dato.correcto ? '' : 'disabled="disabled" ')
                    + 'maxlength=' + (dato.tipo === 'archivo' ? '"1023" ' : '"255" ')
                    + 'value="' + (estatus && dato.comentario ? dato.comentario : '') + '"/>');
            if (dato.comentario) {
                buffer.push('<button type="button" id="btn-comentario-' + dato.clave + '" class="btn-comentario btn btn-outline-secondary ml-2"><i class="fas fa-comment-alt"></i></button>');
            }
            buffer.push('</div></td>');
            buffer.push('</tr>');
        }

        return buffer.join('\n');
    }

    utils.configuraTablaCaptura = function (idTabla) {
        $(idTabla + ' tbody input.valid-field').configura({type: 'comentario'}).configura({maxlength: 1023});
        $(idTabla + ' tbody input.valid-field').validacion();
    }

    utils.validaTablaCaptura = function () {
        $('#inscripcion-datos-capturados tbody input.valid-field').limpiaErrores();
        $('#inscripcion-datos-capturados-incorrectos tbody input.valid-field').limpiaErrores();
        $('#inscripcion-datos-capturados-correctos tbody input.valid-field').limpiaErrores();

        var errores = [];
        $('#inscripcion-datos-capturados tbody input.valid-field').valida(errores, false);
        $('#inscripcion-datos-capturados-incorrectos tbody input.valid-field').valida(errores, false);
        $('#inscripcion-datos-capturados-correctos tbody input.valid-field').valida(errores, true);

        return errores.length === 0;
    };

    utils.construyeInscripcion = function (inscripcion, tipoInscripcion) {
        switch (tipoInscripcion) {
            case 'contratos':
                $('#detalle-elemento').html($.segalmex.maiz.inscripcion.vista.construyeInscripcion(inscripcion));
                $('#button-ver-productores').click($.segalmex.common.bandejas.muestraDetalleContratoProductor);
                $('#detalle-div').html('');
                $('#detalle-div').html($.segalmex.maiz.inscripcion.vista.generaTablaDetalleContratosProductor(inscripcion));
                $.segalmex.common.bandejas.configuraTablaContratosProductor();
                break;
            case 'productores':
                $.segalmex.maiz.inscripcion.productor.vista.muestraInscripcion(inscripcion, 'detalle-elemento');
                utils.validaPrediosSolicitados(inscripcion.predios);
                data.prediosDocumentos = inscripcion.prediosDocumentos;
                $('#predios-documento-table').on('click', 'button.mostrar-repetidos', handlers.muestraFoliosRepetidos);
                break;
            case 'facturas':
                var detalle = $.segalmex.maiz.inscripcion.factura.vista.construyeInscripcion(inscripcion);
                detalle += $.segalmex.maiz.inscripcion.factura.vista.muestraVerificaciones(inscripcion);
                detalle += $.segalmex.maiz.inscripcion.factura.vista.muestraProductorCiclo(inscripcion);
                detalle += $.segalmex.maiz.inscripcion.factura.vista.muestraCantidadFacturable(inscripcion);
                $('#detalle-elemento').html(detalle);
                break;
            default:
                alert('Registro desconocido');
        }
        $('#detalle-elemento-comentarios').html($.segalmex.maiz.inscripcion.vista.construyeComentarios(inscripcion.comentarios, $.segalmex.common.pagina.comun.registrado));
    };

    utils.configuraBotones = function (tipoInscripcion, bandeja, cultivo) {
        $('#botones-entrada button,#btn-validar-group').show();
        $('#button-corregir-productores').hide();
        switch (tipoInscripcion) {
            case 'contratos':
                $('#no-elegible-inscripcion-button').hide();
                $('#supervision-inscripcion-button').hide();
                $('#btn-validar-group').hide();
                $('#button-cancelacion,#btn-validar-positivo-facturas,#button-complementos,#button-validacion-complemento').hide();
                if (cultivo === 'trigo') {
                    switch (bandeja) {
                        case 'solicitud-complemento':
                            $('#button-validacion-positiva,#button-validacion-negativa').hide();
                            $('#button-complementos').show();
                            break;
                        case 'validacion-complemento':
                        case 'revalidacion-complemento':
                            $('#button-validacion-positiva,#button-validacion-negativa').hide();
                            $('#button-validacion-complemento').show();
                            break;
                        default:
                            $('#button-validacion-positiva,#button-validacion-negativa').show();
                    }
                }
                break;
            case 'productores':
                $('#button-cancelacion,#button-complementos,#button-validacion-complemento').hide();
                switch (bandeja) {
                    case 'supervision':
                        $('#supervision-inscripcion-button').hide();
                        break;
                }
                $('#button-cancelacion,#button-validacion-positiva,#btn-validar-positivo-facturas').hide();
                break;
            case 'facturas':
                $('#no-elegible-inscripcion-button').hide();
                $('#supervision-inscripcion-button').hide();
                $('#btn-validar-group,#btn-validar-positivo-facturas,#button-complementos,#button-validacion-complemento').hide();
                if (cultivo === 'trigo' && data.inscripcion.tipoFactura === 'productor') {
                    $('#button-validacion-positiva').hide();
                    $('#btn-validar-positivo-facturas').show();
                }
                break;
            default:
                alert('Registro desconocido');
        }
    };

    utils.cargaCatalogos = function () {
        $.ajax({
            url: '/cultivos/resources/paginas/bandeja-asignacion/',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            data.cultivos = response.cultivos;
            data.ciclos = response.ciclos;
            utils.configuraPantalla();
        }).fail(function () {
            alert('Error: No se pudo obtener los catálogos de la página.');
        });
    };

    utils.configuraPantalla = function () {
        $.segalmex.common.bandejas.init({
            configuracion: utils.configuracionBandejas(),
            namespace: $.segalmex.maiz.inscripcion.validacion,
            tipoInscripcion: data.tipoInscripcion
        });
        $('#menu-bandejas a.nav-link').click(handlers.cambiaBandeja);
        $('#button-regresar').click(handlers.regresar);
        $('#button-cancelacion').click(handlers.cancela);
        $('#si-cancelar-factura-button').click(handlers.confirmaCancelacionFactura);
        $('#button-validacion-negativa').click(handlers.validaNegativamente);
        $('#button-validacion-positiva').click(handlers.validaPositivamente);
        $('#button-validacion-positiva-2').click(handlers.validaPositivamente);
        $('#button-validacion-positiva-con').click(handlers.validaPositivamente);
        $('#button-validacion-positiva-sin').click(handlers.validaPositivamente);
        //Botones para validar positivamente las facturas
        $('#button-validacion-positiva-productor').click(handlers.validaPositivaFacturaProductor);
        $('#button-validacion-positiva-fecha-compra').click(handlers.validaPositivaFacturaFechaPago);
        $('#nuevo-comentario-button').click(handlers.nuevoComentario);
        $('#agregar-comentario-button').click(handlers.agregaComentario);
        $('#todos-correctos-button').click(handlers.marcaTodosCorrectos);
        $('#todos-incorrectos-button').click(handlers.marcaTodosIncorrectos);
        //botones para las tablas de datos Correctos/Incorrectos
        $('#todos-correctos-button-table-incorrectos').click(handlers.marcaTodosCorrectosTablaIncorrectos);
        $('#todos-incorrectos-button-table-incorrectos').click(handlers.marcaTodosIncorrectosTablaIncorrectos);
        $('#todos-correctos-button-table-correctos').click(handlers.marcaTodosCorrectosTablaCorrectos);
        $('#todos-incorrectos-button-table-correctos').click(handlers.marcaTodosIncorrectosTablaCorrectos);
        $('#button-acepta-complemento').click(handlers.aceptaComplemento);
        $('#button-rechaza-complemento').click(handlers.rechazaComplemento);
        $('#btn-datos-correctos').click(handlers.muestraDatosCorrectos);
        $('#inscripcion-datos-capturados-incorrectos').on('change', 'input[type=radio]:checked', handlers.radioEstatusChange);
        $('#inscripcion-datos-capturados-incorrectos').on('click', 'button.btn-comentario', handlers.muestraComentario);
        $('#inscripcion-datos-capturados-correctos').on('change', 'input[type=radio]:checked', handlers.radioEstatusChange);
        $('#inscripcion-datos-capturados-correctos').on('click', 'button.btn-comentario', handlers.muestraComentario);
        $('#button-complemento-negativa').click(handlers.validaNegativamenteComplemento);
        $('#button-complemento-positiva').click(handlers.validaPositivoComplemento);
        $('#button-corregir-productores').click(handlers.solicitaCorreccionContratoProductor);

        $('#inscripcion-datos-capturados').on('change', 'input[type=radio]:checked', handlers.radioEstatusChange);
        $('#inscripcion-datos-capturados').on('click', 'button.btn-comentario', handlers.muestraComentario);
        $('.cultivos').click(handlers.cambiaCultivo);
        $('#contenido-comentario,#comentario-cancelacion-factura,#comentario-cancelacion').configura({
            minlength: 1,
            maxlength: 2047,
            allowSpace: true,
            textTransform: null,
            pattern: /^[A-Za-z0-9ÑñÁÉÍÓÚáéíóúÄËÏÖÜäëïöü@\.,:\n\-\(\)" ]*$/
        }).validacion();
        $('#no-elegible-inscripcion-button').click(handlers.noElegible);
        $('#si-cancelar-button').click(handlers.confirmaNoElegible);
        $('#no-cancelar-button').click(handlers.desisteCancelacion);
        $('#supervision-inscripcion-button').click(handlers.enviaSupervision);
        setTimeout(function () {
            utils.setCultivo();
        }, 100);
    };

    handlers.muestraComentario = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var clave = id.substring('btn-comentario-'.length);
        if ($('#comentario-' + clave).val() !== '') {
            $('#comentario-modal .modal-body p').html($('#comentario-' + clave).val());
            $('#comentario-modal').modal('show');
        }
    };

    handlers.cambiaCultivo = function (e) {
        $('.cultivos').removeClass("active");
        $('#' + $(this).attr('id')).addClass("active");
        var cultivo = $(this).attr('id').substring(7);
        data.cultivoSeleccionado = cultivo;
        $.segalmex.common.bandejas.filtraRegstros(cultivo, '0', '0');
        $('#cultivo-label').val(cultivo === 'maiz-comercial' ? 'maíz' : cultivo).
                css('text-transform', 'capitalize');
    };

    utils.enviaValidacionPositiva = function (datos) {
        $.ajax({
            url: '/cultivos/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + data.inscripcion.uuid + '/validacion/positiva/',
            data: JSON.stringify(datos),
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('El registro ha sido validado positivamente.');
            $.segalmex.common.bandejas.actualizar();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Error: No fue posible validar positivamente.');
            }
        });
    };

    handlers.muestraFoliosRepetidos = function (e) {
        e.preventDefault();
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('mostrar-repetidos-'.length), 10);
        for (var i = 0; i < data.prediosDocumentos.length; i++) {
            if (idx === i) {
                var pd = data.prediosDocumentos[i];
            }
        }
        $.ajax({
            url: '/cultivos/resources/predio-documento/duplicados/' + pd.uuidTimbreFiscalDigital,
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            var folios = response.join(',');
            $('#label-folios').html('<strong >' + folios + '</strong>');
            $('#muestra-repetidos-modal').modal('show');
        }).fail(function () {
            alert('Error: No se pudo obtener los folios duplicados.');
        });
    };

    handlers.aceptaComplemento = function (e) {
        e.preventDefault();
        $('#button-acepta-complemento').prop('disabled', true);
        $.ajax({
            url: '/cultivos/resources/contratos/maiz/inscripcion/' + data.inscripcion.uuid + '/complemento/aceptado/',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('Se ha aceptado el complemento.');
            $('#button-acepta-complemento').prop('disabled', false);
            $.segalmex.common.bandejas.actualizar();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert("Ocurrió un error al aceptar el complemento.");
            }
            $('#button-acepta-complemento').prop('disabled', false);
        });
    };

    handlers.rechazaComplemento = function (e) {
        e.preventDefault();
        $('#button-rechaza-complemento').prop('disabled', true);
        $.ajax({
            url: '/cultivos/resources/contratos/maiz/inscripcion/' + data.inscripcion.uuid + '/complemento/rechazado/',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('Se ha rechazado el complemento.');
            $('#button-rechaza-complemento').prop('disabled', false);
            $.segalmex.common.bandejas.actualizar();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert("Ocurrió un error al rechazar el complemento.");
            }
            $('#button-rechaza-complemento').prop('disabled', false);
        });
    };

    handlers.solicitaCorreccionContratoProductor = function (e) {
        e.preventDefault();
        $('#button-corregir-productores').prop('disabled', true);
        if (!confirm("El número de productores del contrato no será modificado. ¿Desea continuar?")) {
            alert('Es necesario corregir primero el número de productores y después enviar a corrección de productores.');
            $('#button-corregir-productores').prop('disabled', false);
            return;
        }
        $.ajax({
            url: '/cultivos/resources/contratos/maiz/inscripcion/' + data.inscripcion.uuid + '/correccion/contrato-productor/',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('El registro ha sido enviado a corrección de contratos de productor.');
            $('#button-corregir-productores').prop('disabled', false);
            $.segalmex.common.bandejas.actualizar();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert("Ocurrió un error cerregir el registro.");
            }
            $('#button-corregir-productores').prop('disabled', false);
        });
    };

    handlers.validaNegativamenteComplemento = function (e) {
        e.preventDefault();
        $('#button-complemento-negativa').prop('disabled', true);
        var tamanioDatos = utils.getTamanioDatosCapturados();
        if (tamanioDatos !== data.datosCapturadosIar.length) {
            alert('Error: Es necesario indicar el estatus de todos los campos.');
            $('#button-complemento-negativa').prop('disabled', false);
            return;
        }

        if (utils.isAprobada()) {
            alert('Error: Para validar negativamente se requiere de al menos un campo erróneo.');
            $('#button-complemento-negativa').prop('disabled', false);
            return;
        }

        if (!utils.validaTablaCaptura()) {
            $('#button-complemento-negativa').prop('disabled', false);
            return;
        }

        var capturados = utils.getDatosCapturados();
        var datos = capturados.concat(data.datosCapturadosGeneral);

        $.ajax({
            url: '/cultivos/resources/contratos/maiz/inscripcion/' + data.inscripcion.uuid + '/complemento/validacion/negativa/',
            type: 'POST',
            data: JSON.stringify(datos),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('El complemento se ha validado negativamente.');
            $('#button-complemento-negativa').prop('disabled', false);
            $.segalmex.common.bandejas.actualizar();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert("Ocurrió un error al validar negativamente el complemento.");
            }
            $('#button-complemento-negativa').prop('disabled', false);
        });
    };

    handlers.validaPositivoComplemento = function (e) {
        e.preventDefault();
        $('#button-complemento-positiva').prop('disabled', true);

        var tamanioDatos = utils.getTamanioDatosCapturados();
        if (tamanioDatos !== data.datosCapturadosIar.length) {
            alert('Error: Es necesario indicar el estatus de todos los campos.');
            $('#button-complemento-positiva').prop('disabled', false);
            return;
        }

        if (!utils.isAprobada()) {
            alert('Error: Para validar positivamente se requiere el estatus correcto de todos los campos.');
            $('#button-complemento-positiva').prop('disabled', false);
            return false;
        }

        if (!utils.validaTablaCaptura()) {
            $('#button-complemento-positiva').prop('disabled', false);
            return false;
        }

        var capturados = utils.getDatosCapturados();
        var datosCorrectos = utils.filtradatosCapturados(data.inscripcion.datos, true);
        var datos = capturados.concat(datosCorrectos);

        $.ajax({
            url: '/cultivos/resources/contratos/maiz/inscripcion/' + data.inscripcion.uuid + '/complemento/validacion/positiva/',
            type: 'POST',
            data: JSON.stringify(datos),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('El complemento se ha validado positivamente.');
            $('#button-complemento-positiva').prop('disabled', false);
            $.segalmex.common.bandejas.actualizar();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert("Ocurrió un error al validar positivamente el complemento.");
            }
            $('#button-complemento-positiva').prop('disabled', false);
        });
    };
})(jQuery);