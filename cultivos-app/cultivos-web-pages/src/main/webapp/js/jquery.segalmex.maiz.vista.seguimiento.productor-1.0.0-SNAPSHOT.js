(function ($) {

    var utils = {};

    $.segalmex.namespace('segalmex.maiz.vista.seguimiento.productor');

    $.segalmex.maiz.vista.seguimiento.productor.muestraProductor = function (productor) {
        var buffer = [];

        buffer.push('<div class="card mb-4 shadow-sm">');
        buffer.push('<h4 class="card-header">Detalle del productor</h4>');

        buffer.push('<div class="card-body">'); // card-body
        buffer.push('<h4>Programa de Precios de Garantía a Productos Alimentarios Básicos</h4>');
        buffer.push('<div class="row mb-3">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Cultivo:</strong><br/>');
        buffer.push(productor.cultivo.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Ciclo agrícola:</strong><br/>');
        buffer.push(productor.ciclo.nombre);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<h4>Datos del productor</h4>');
        buffer.push('<div class="row mb-3">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Nombre:</strong><br/>');
        buffer.push(productor.productor.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>RFC:</strong><br/>');
        buffer.push(productor.productor.clave);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>CURP:</strong><br/>');
        buffer.push(productor.productor.curp);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row

        buffer.push('<div class="row  mb-3">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Fecha de Nacimiento:</strong><br/>');
        buffer.push($.segalmex.date.isoToFecha(productor.productor.persona.fechaNacimiento));
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Nacionalidad:</strong><br/>');
        buffer.push(productor.productor.persona.nacionalidad.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Tipo productor:</strong><br/>');
        buffer.push(productor.tipoProductor.nombre);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<h4>Toneladas</h4>');
        buffer.push('<div class="table-responsive">');
        buffer.push('<table  class="table table-striped table-bordered table-hover">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<tr>');
        buffer.push('<th>Grupo</th>');
        buffer.push('<th>Toneladas</th>');
        buffer.push('<th>Cantidad</th>');
        buffer.push('<th>Autorizadas</th>');
        buffer.push('<th>Comprobadas</th>');
        buffer.push('<th>Restantes</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');

        var pcg = productor.grupos;
        for (var i = 0; i < pcg.length; i++) {
            var p = pcg[i];
            var autorizadas = utils.getAutorizadas(p);
            buffer.push('<tr>');
            buffer.push('<td rowspan = 4 class="text-center"><br/><br/><br/>');
            buffer.push(utils.getNombreGrupo(p.grupoEstimulo));
            buffer.push('</td>');
            buffer.push('<td>Producidas</td>');
            buffer.push('<td class="text-right">');
            buffer.push(p.toneladasTotales.toFixed(3));
            buffer.push('</td>');
            buffer.push('<td rowspan = 4 class="text-right"><br/><br/><br/>');//aurotizadas
            buffer.push(autorizadas.toFixed(3));
            buffer.push('</td>');
            buffer.push('<td rowspan = 4 class="text-right"><br/><br/><br/>');//comprobadas
            buffer.push(p.toneladasFacturadas.toFixed(3));
            buffer.push('</td>');
            buffer.push('<td rowspan = 4 class="text-right"><br/><br/><br/>');
            buffer.push((autorizadas - p.toneladasFacturadas).toFixed(3));//restantes
            buffer.push('</td>');
            buffer.push('</tr>');
            buffer.push('<td>Contratadas</td>');
            buffer.push('<td class="text-right">');
            buffer.push(productor.cultivo.clave !== 'arroz' ? p.toneladasContratadas.toFixed(3) : '--');
            buffer.push('</td>');
            buffer.push('<tr>');
            buffer.push('<td>Cobertura</td>');
            buffer.push('<td class="text-right">');
            buffer.push(productor.cultivo.clave !== 'arroz' ? p.toneladasCobertura.toFixed(3) : '--');
            buffer.push('</td>');
            buffer.push('</tr>');
            buffer.push('<tr>');
            buffer.push('<td>Límite</td>');
            buffer.push('<td class="text-right">');
            buffer.push(p.limite.toFixed(3));
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        buffer.push('</tbody>');
        buffer.push('</table>');
        buffer.push('</div>');

        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card
        return buffer.join('\n');
    };

    $.segalmex.maiz.vista.seguimiento.productor.creaTablaInscripciones = function (inscripciones) {
        var buffer = [];
        buffer.push('<div class="card mb-4 shadow-sm">');
        buffer.push('<h4 class="card-header">Registros</h4>');

        buffer.push('<div class="card-body table-responsive">'); // card-body

        buffer.push('<table id="table-resultados-busqueda-inscripciones" class="table table-striped table-bordered table-hover mb-0">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<tr class="success">');
        buffer.push('<th>#</th>');
        buffer.push('<th>Folio</th>');
        buffer.push('<th>Fecha</th>');
        buffer.push('<th>Nombre Productor</th>');
        buffer.push('<th>CURP</th>');
        buffer.push('<th>RFC</th>');
        buffer.push('<th>Ciclo</th>');
        buffer.push('<th>Cultivo</th>');
        buffer.push('<th>Estatus</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');

        for (var i = 0; i < inscripciones.length; i++) {
            var f = inscripciones[i];
            var dp = f.datosProductor;
            buffer.push('<tr>');
            buffer.push('<td class="text-center">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td> <a class="link-inscripcion" href="#" id="uuid.' + f.uuid + '">');
            buffer.push(f.folio);
            buffer.push('</a></td>');
            buffer.push('<td>');
            buffer.push($.segalmex.date.isoToFecha(f.fechaCreacion));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push($.segalmex.maiz.vista.seguimiento.productor.construyeNombreCompleto(dp));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(dp.curp);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(dp.rfc);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(f.ciclo.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(f.cultivo.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(f.estatus.nombre);
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        if (inscripciones.length === 0) {
            buffer.push('<tr><td colspan="9">No hay inscripciones.</td></tr>');
        }
        buffer.push('</table>');
        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card
        return buffer.join('\n');
    };

    $.segalmex.maiz.vista.seguimiento.productor.creaTablaFacturas = function (facturas) {
        var buffer = [];
        buffer.push('<div class="card mb-4 shadow-sm">');
        buffer.push('<h4 class="card-header">Facturas</h4>');
        buffer.push('<div class="card-body table-responsive">'); // card-body

        buffer.push('<table id="table-resultados-busqueda-facturas" class="table table-striped table-bordered table-hover mb-0">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<tr class="success">');
        buffer.push('<th>No.</th>');
        buffer.push('<th>Folio</th>');
        buffer.push('<th>Fecha</th>');
        buffer.push('<th>Receptor</th>');
        buffer.push('<th>RFC Receptor</th>');
        buffer.push('<th>Toneladas</th>');
        buffer.push('<th>Ciclo</th>');
        buffer.push('<th>Cultivo</th>');
        buffer.push('<th>Estatus</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');

        for (var i = 0; i < facturas.length; i++) {
            var f = facturas[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-center">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td><a class="link-facturas" href="#" id="uuid.' + f.uuid + '">');
            buffer.push(f.folio);
            buffer.push('</a></td>');
            buffer.push('<td>');
            buffer.push($.segalmex.date.isoToFecha(f.fechaCreacion));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(f.cfdi.nombreReceptor);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(f.cfdi.rfcReceptor);
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(f.factura ? f.factura.toneladasTotales : '--');
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(f.ciclo.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(f.cultivo.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(f.estatus.nombre);
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        if (facturas.length === 0) {
            buffer.push('<tr><td colspan="9">No hay facturas registradas.</td></tr>');
        }
        buffer.push('</table>');
        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card
        return buffer.join('\n');
    };

    $.segalmex.maiz.vista.seguimiento.productor.creaTablaPagos = function (pagos) {
        var buffer = [];
        buffer.push('<div class="card mb-4 shadow-sm">');
        buffer.push('<h4 class="card-header">Pagos</h4>');
        buffer.push('<div class="card-body table-responsive">'); // card-body

        buffer.push('<table id="table-resultados-busqueda" class="table table-striped table-bordered table-hover mb-0">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<tr class="success">');
        buffer.push('<th>No.</th>');
        buffer.push('<th>Folio</th>');
        buffer.push('<th>Fecha</th>');
        buffer.push('<th>Estatus</th>');
        buffer.push('<th>Tipo</th>');
        buffer.push('<th>Estímulo x Tonelada</th>');
        buffer.push('<th>Toneladas</th>');
        buffer.push('<th>Estímulo Total</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');

        for (var i = 0; i < pagos.length; i++) {
            var p = pagos[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-center">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(p.folio);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push($.segalmex.date.isoToFecha(p.fechaCreacion));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(p.estatus.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(p.tipo.nombre);
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(p.estimuloTonelada);
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(p.toneladas);
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(p.estimuloTotal);
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        if (pagos.length === 0) {
            buffer.push('<tr><td colspan="9">No hay pagos.</td></tr>');
        }
        buffer.push('<tbody>');
        if (pagos.length > 0) {
            buffer.push(utils.agregaTotales(pagos));
        }
        buffer.push('</table>');
        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card
        return buffer.join('\n');
    };

    $.segalmex.maiz.vista.seguimiento.productor.construyeNombreCompleto = function (dp) {
        var buffer = [];
        if (dp.nombre) {
            buffer.push(dp.nombre);
        }
        if (dp.primerApellido) {
            buffer.push(dp.primerApellido);
        }
        if (dp.segundoApellido) {
            buffer.push(dp.segundoApellido);
        }
        return buffer.join(' ');
    };

    utils.getAutorizadas = function (grupo) {
        var numeros = [];
        numeros.push(utils.validaNumero(grupo.toneladasTotales));
        numeros.push(utils.validaNumero(grupo.toneladasContratadas));
        numeros.push(utils.validaNumero(grupo.toneladasCobertura));
        numeros.push(utils.validaNumero(grupo.limite));
        numeros.sort(function (a, b) {
            return a - b;
        });
        return numeros[0];
    };

    utils.validaNumero = function (numero) {
        if (numero !== undefined && numero > 0) {
            return numero;
        }
    };

    utils.getNombreGrupo = function (grupo) {
        switch (grupo) {
            case 'trigo':
                return 'Trigo';
                break;
            case 'trigo-cristalino':
                return 'Trigo cristalino';
                break;
            case 'arroz':
                return 'Arroz';
                break;
            case 'maiz':
                return 'Maíz';
            default:
                return grupo;
                break;
        }
    };

    utils.agregaTotales = function (pagos) {
        var toneladas = 0;
        var estimuloTotal = 0;
        for (var p in pagos) {
            toneladas += parseFloat((pagos[p].toneladas * pagos[p].estatus.factor));
            estimuloTotal += parseFloat((pagos[p].estimuloTotal * pagos[p].estatus.factor));
        }

        var buffer = [];
        buffer.push('<tfoot class="thead-dark">');
        buffer.push('<tr>');
        buffer.push('<th colspan="6">Total</th>');
        buffer.push('<th class="text-right">' + utils.format(toneladas, 3) + '</th>');
        buffer.push('<th class="text-right">' + utils.format(estimuloTotal, 3) + '</th>');
        buffer.push('</tr>');
        buffer.push('</tfoot>');
        return buffer.join('');
    };

    utils.format = function (str, fix) {
        return str.toFixed(fix).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    };
})(jQuery);