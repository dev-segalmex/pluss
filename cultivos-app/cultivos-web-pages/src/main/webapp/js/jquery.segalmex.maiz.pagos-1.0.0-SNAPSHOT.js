/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.maiz.pagos');
    var data = {
        tipoBusqueda: 'maiz-comercial',
        requerimientos: [],
        requerimientosMes: []
    };
    var handlers = {};
    var utils = {};

    $.segalmex.maiz.pagos.init = function (params) {
        utils.inicializaCatalogos();
        $('#regresar-button').click(handlers.regresaResultados);
        $('#button-nuevo').click(handlers.nuevo);
        $('#button-regresar-requerimiento').click(utils.regresarRequermientos);
        $('#button-guardar').click(handlers.guardaRequerimiento);
        $('#button-guardar-pendientes').click(utils.mustraModalCometario);
        $('#confirmar-pendiente-button').click(handlers.guardaPendientes);
        $('#button-guardar-orden-pago').click(handlers.guardaRequerimiento);
        $('#button-mes').click(handlers.muestraPorMes);
        $('#button-dia').click(handlers.muestraPorDia);
        $('#menu-bandejas a.nav-link').click(handlers.cambiaBandeja);
        $('#ciclo').change(utils.filtraRequerimientos);
        $('#button-regresar-detalle').click(utils.regresaDetalle);
        $('#button-aprobar-requerimiento').click(handlers.apruebaRequerimiento);
        $('#button-cancelar-requerimiento').click(handlers.cancelaRequerimiento);
        $('#button-pagar-requerimiento').click(handlers.actualizaPagado);
        utils.inicializaValidaciones();
    };

    utils.inicializaValidaciones = function () {
        $(".valid-field").configura();

        $('#comentario').configura({
            type: 'comentario'
        }).configura({maxlength: 1023});

        $('.valid-field').validacion();
    };

    handlers.apruebaRequerimiento = function (e) {
        e.preventDefault();
        if (!confirm("¿Seguro que desea aprobar el requerimiento?")) {
            return;
        }
        $('#button-aprobar-requerimiento').prop('disabled', true).html('Aprobando...');
        $.ajax({
            url: '/cultivos/resources/requerimientos-pago/' + data.uuidRequerimiento,
            type: 'PUT',
            dataType: 'json'
        }).done(function (response) {
            alert('Se aprobó el requerimiento de pago.');
            data.uuidRequerimiento = response.uuid;
            $('#muestra-arroz,#muestra-maiz-comercial,#muestra-trigo').addClass('disabled');
            $('#detalle-elemento').html($.segalmex.maiz.vista.pagos.muestraRequerimientoPago(response));
            $('#button-aprobar-requerimiento').html('Aprobar');
            utils.habilitaAcciones(response.estatus);
        }).fail(function (responseF) {
            if (responseF.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(responseF.responseJSON));
            } else {
                alert('Error: No fue posible aprobar el requerimiento.');
            }
            $('#button-aprobar-requerimiento').html('Aprobar');
        });
    };


    handlers.actualizaPagado = function (e) {
        e.preventDefault();
        if (!confirm("¿Seguro que desea marcar como pagado el requerimiento?")) {
            return;
        }
        $('#button-pagar-requerimiento').prop('disabled', true).html('Marcando pago...');
        $.ajax({
            url: '/cultivos/resources/requerimientos-pago/pagar/' + data.uuidRequerimiento,
            type: 'PUT',
            dataType: 'json'
        }).done(function (response) {
            alert('Se marcó el requerimiento como pagado');
            data.uuidRequerimiento = response.uuid;
            $('#muestra-arroz,#muestra-maiz-comercial,#muestra-trigo').addClass('disabled');
            $('#detalle-elemento').html($.segalmex.maiz.vista.pagos.muestraRequerimientoPago(response));
            $('#button-pagar-requerimiento').html('Marcar como pagado');
            utils.habilitaAcciones(response.estatus);
        }).fail(function (responseF) {
            if (responseF.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(responseF.responseJSON));
            } else {
                alert('Error: No fue posible marcar el requerimiento como pagado.');
            }
            $('#button-pagar-requerimiento').html('Marcar como pagado');
        });
    };

    handlers.cancelaRequerimiento = function (e) {
        e.preventDefault();
        if (!confirm("¿Seguro que desea cancelar el requerimiento?")) {
            return;
        }
        $('#button-cancelar-requerimiento').prop('disabled', true).html('Cancelando...');
        $.ajax({
            url: '/cultivos/resources/requerimientos-pago/' + data.uuidRequerimiento,
            type: 'DELETE',
            dataType: 'json'
        }).done(function (response) {
            alert('Se canceló el requerimiento de pago.');
            //Volvemos a cargar los requerimientos de pago.
            $('#button-cancelar-requerimiento').html('Cancelar');
            utils.cargaRequerimientos();
            utils.regresaDetalle();
        }).fail(function (responseF) {
            if (responseF.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(responseF.responseJSON));
            } else {
                alert('Error: No fue posible cancelar el requerimiento.');
            }
            $('#button-cancelar-requerimiento').html('Cancelar');
        });
    };

    handlers.cambiaBandeja = function (e) {
        e.preventDefault();
        $('#div-consultas #' + 'div-' + data.tipoBusqueda).hide();
        var id = e.target.id.substring('muestra-'.length);
        if (data.tipoBusqueda === id) {
            $('#div-consultas #' + 'div-' + data.tipoBusqueda).show();
            return;
        }
        data.tipoBusqueda = id;
        $('#div-consultas #' + 'div-' + data.tipoBusqueda).show();
        $('#menu-bandejas a.nav-link').removeClass('active');
        $(e.target).addClass('active');
        utils.filtraRequerimientos();
    };

    handlers.seleccionaTodos = function (e) {
        var checked = $(e.target).is(':checked');
        $('#div-usos-factura table tbody input:checkbox').prop('checked', checked);

    };

    handlers.muestraDetalle = function (e) {
        e.preventDefault();
        var uuid = e.target.id.split('.')[1];
        $('#cargando-resultados').show();
        $('#div-requerimientos').hide();

        $.ajax({
            url: '/cultivos/resources/requerimientos-pago/' + uuid,
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            // Mostramos el detalle
            data.uuidRequerimiento = response.uuid;
            $('#muestra-arroz,#muestra-maiz-comercial,#muestra-trigo').addClass('disabled');
            $('#cargando-resultados').hide();
            $('#div-requerimientos').hide();
            $('#detalle-elemento').html($.segalmex.maiz.vista.pagos.muestraRequerimientoPago(response));
            $('#div-detalle-pago').show();
            utils.habilitaAcciones(response.estatus);
        }).fail(function () {
            alert('Error: No fue posible obtener el registro.');
        });
    };

    utils.habilitaAcciones = function (estatusRequerimineto) {
        $('#button-cancelar-requerimiento,#button-aprobar-requerimiento,#button-pagar-requerimiento').prop('disabled', true);
        switch (estatusRequerimineto) {
            case 'nuevo':
                $('#button-cancelar-requerimiento,#button-aprobar-requerimiento').prop('disabled', false);
            case 'aprobado':
                $('#button-cancelar-requerimiento').prop('disabled', false);
                break;
            case 'enviado':
                $('#button-pagar-requerimiento').prop('disabled', false);
                break;
        }
    };

    handlers.regresaResultados = function (e) {
        e.preventDefault();
        $('#detalle').html('');
        $('#datos-busqueda-form,#detalle-elemento').show();
    };

    handlers.muestraPorDia = function (e) {
        var requerimientos = [];
        requerimientos = utils.getRequerimientosFiltrados();
        $('#table-requerimientos-pago').html($.segalmex.maiz.vista.pagos.creaTablaRequerimientos(requerimientos));
        $('#table-resultados-busqueda').on('click', 'a.link-uuid', handlers.muestraDetalle);
    };

    handlers.muestraPorMes = function (e) {
        $('#table-requerimientos-pago').html($.segalmex.maiz.vista.pagos.creaTablaRequerimientos(data.requerimientosMes));
    };

    utils.inicializaCatalogos = function () {
        $.ajaxq('catalogosQueue', {
            url: '/cultivos/resources/paginas/requerimientos-pago',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            data.ciclos = response.ciclos;
            data.cultivos = response.cultivos;
            $('#ciclo').actualizaCombo(data.ciclos);
            utils.cargaRequerimientos();
        }).fail(function () {
            alert('Error: No fue posible cargar los catálogos de pagos.');
        });
    };


    utils.cargaRequerimientos = function () {
        $.ajaxq('catalogosQueue', {
            url: '/cultivos/resources/requerimientos-pago/',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            data.requerimientos = response;
            utils.filtraRequerimientos();
        }).fail(function () {
            alert('Error: No fue posible cargar los datos de pagos.');
        });
    };

    utils.filtraRequerimientos = function () {
        var req = [];
        req = utils.getRequerimientosFiltrados();
        utils.generaRequerimientosPorMes();
        $('#table-requerimientos-pago').html($.segalmex.maiz.vista.pagos.creaTablaRequerimientos(req));
        // utils.configuraTablaRequeriminetos();
        $('#table-resultados-busqueda').on('click', 'a.link-uuid', handlers.muestraDetalle);
    };

    handlers.nuevo = function () {
        //Para poder generar un nuevo requerimiento de pago es necesario indicar el ciclo agrícola.
        if ($('#ciclo').val() === '0') {
            alert('Es necesario seleccionar el ciclo.');
            return;
        }
        utils.mostrarUsosFactura();
        $('#cargando-resultados').show();
    };

    utils.mostrarUsosFactura = function () {
        var cultivo = $.segalmex.get(data.cultivos, data.tipoBusqueda, 'clave');
        var parametros = {
            ciclo: $('#ciclo').val(),
            cultivo: cultivo.id
        };
        $.ajax({
            url: '/cultivos/resources/facturas/usos-factura/',
            type: 'GET',
            data: parametros,
            dataType: 'json'
        }).done(function (response) {
            $('#muestra-arroz,#muestra-maiz-comercial,#muestra-trigo').addClass('disabled');
            $('#div-requerimientos').hide();
            $('#cargando-resultados').hide();
            $('#table-usos-factura').html($.segalmex.maiz.vista.pagos.creaTablaUsosFactura(response));
            $('#div-usos-factura').show();
            $('#div-usos-factura table #todas-checkbox').click(handlers.seleccionaTodos);
            $('#div-usos-factura table').on('click', 'a.link-folio', utils.buscaUsoFactura);
        }).fail(function () {
            $('#cargando-resultados').hide();
            alert('Error: No fue posible realizar la consulta.');
        });

    };

    utils.regresarRequermientos = function () {
        $('#muestra-arroz,#muestra-maiz-comercial,#muestra-trigo').removeClass('disabled');
        $('#table-usos-factura').html('');
        $('#div-usos-factura').hide();
        $('#div-requerimientos').show();
    };

    utils.regresaDetalle = function () {
        $('#muestra-arroz,#muestra-maiz-comercial,#muestra-trigo').removeClass('disabled');
        $('#div-detalle-pago').hide();
        $('#div-requerimientos').show();
        $('#detalle-elemento').html('');
    };

    handlers.guardaRequerimiento = function (e) {
        e.preventDefault();
        e.target.disabled = true;

        var seleccionados = $('#table-usos-factura table tbody input:checked');
        if (seleccionados.length === 0) {
            alert('Error: Es necesario seleccionar al menos una factura para enviar a pago.');
            e.target.disabled = false;
            return;
        }
        var estatus = [];
        seleccionados.each(function () {
            var e = this.value;
            estatus.push(e);
        });

        var usosFacturas = [];
        seleccionados.each(function () {
            var uuid = this.id.substring(0, this.id.length - '-factura-check'.length);
            usosFacturas.push(uuid);
        });
        data.usosFacturasUuid = usosFacturas;
        var cultivo = $.segalmex.get(data.cultivos, data.tipoBusqueda, 'clave');
        var parametros = {
            usosFacturasUuid: usosFacturas,
            ciclo: {id: $('#ciclo').val()},
            cultivo: {id: cultivo.id},
            layout: 'PAGOS_MASIVOS'
        };

        if (e.target.id === 'button-guardar-orden-pago') {
            parametros.layout = 'ORDENES_PAGO';
        }

        $.ajax({
            url: '/cultivos/resources/requerimientos-pago/',
            type: 'POST',
            data: JSON.stringify(parametros),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('Se enviaron a pago los uso factura seleccionados.');
            e.target.disabled = false;
            utils.inicializaCatalogos();
            utils.regresarRequermientos();

        }).fail(function (responseF) {
            if (responseF.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(responseF.responseJSON));
            } else {
                alert('Error: no se pudo enviar a pagos.');
            }
            e.target.disabled = false;
        });

    };


    utils.mustraModalCometario = function (e) {
        var seleccionados = $('#table-usos-factura table tbody input:checked');
        if (seleccionados.length === 0) {
            alert('Error: Es necesario seleccionar al menos una factura para enviar a pendiente.');
            e.target.disabled = false;
            return;
        }
        $('#capturar-comentario-modal').modal('show');
        $('#comentario').val('');
    };

    handlers.guardaPendientes = function (e) {
        e.preventDefault();
        e.target.disabled = true;

        $('#comentario').limpiaErrores();
        var errores = [];
        $('#comentario').valida(errores, true);
        if (errores.length > 0) {
            return;
        }

        var seleccionados = $('#table-usos-factura table tbody input:checked');

        var usosFacturas = [];
        seleccionados.each(function () {
            var uuid = this.id.substring(0, this.id.length - '-factura-check'.length);
            usosFacturas.push(uuid);
        });
        data.usosFacturasUuid = usosFacturas;
        var parametros = {
            usosFacturasUuid: usosFacturas,
            comentario: $('#comentario').val()
        };
        $.ajax({
            url: '/cultivos/resources/uso-factura/estatus/pendiente/',
            type: 'PUT',
            data: JSON.stringify(parametros),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('Se enviaron a pendiete los uso factura seleccionados.');
            e.target.disabled = false;
            $('#capturar-comentario-modal').modal('hide');
            handlers.nuevo();
        }).fail(function (responseF) {
            if (responseF.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(responseF.responseJSON));
            } else {
                alert('Error: no se pudo enviar a pendiente.');
            }
            e.target.disabled = false;
        });

    };


    utils.buscaUsoFactura = function (e) {
        e.preventDefault();
        var cultivo = $.segalmex.get(data.cultivos, data.tipoBusqueda, 'clave');
        var datos = {
            folio: e.target.id.split('.')[1],
            cultivo: cultivo.id
        };
        $.ajax({
            url: '/cultivos/resources/facturas/maiz/inscripcion/',
            type: 'GET',
            data: datos,
            dataType: 'json'
        }).done(function (response) {
            utils.muestraDetalleUso(response[0].uuid);
        }).fail(function () {
            alert('Error: No fue posible obtener el detalle de la factura.');
        });
    };

    utils.muestraDetalleUso = function (uuid) {

        $.ajax({
            url: '/cultivos/resources/facturas/maiz/inscripcion/' + uuid,
            data: {archivos: true, datos: true, historial: true, comentarios: true},
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            // Mostramos el detalle

            $.segalmex.maiz.inscripcion.factura.vista.muestraInscripcion(response, 'detalle-factura');
            $('#verificacion-factura').html($.segalmex.maiz.inscripcion.factura.vista.muestraVerificaciones(response));
            $('#estatus-factura').html($.segalmex.maiz.inscripcion.vista.construyeEstatus(response));
            $('#historial-factura').html($.segalmex.maiz.inscripcion.vista.construyeHistorial(response.historial));
            $('#datos-capturados-factura').html($.segalmex.maiz.inscripcion.vista.construyeDatosCapturados(response.datos));
            $('#comentarios-factura').html($.segalmex.maiz.inscripcion.vista.construyeComentarios(response.comentarios, $.segalmex.common.pagina.comun.registrado));

            $('#detalle-elemento-factura').show();
            $('#div-usos-factura').hide();
            $('#button-regresar-detalle-factura').click(utils.regresaTablaFacturas);
        }).fail(function () {
            alert('Error: No fue posible obtener la factura.');
        });
    };

    utils.regresaTablaFacturas = function () {
        $('#div-usos-factura').show();
        $('#detalle-elemento-factura').hide();
    };

    utils.generaRequerimientosPorMes = function () {
        data.requerimientosMes = [];
        var requerimientosMes = {};
        var id = 1;
        var requerimientosFiltrado = [];
        requerimientosFiltrado = utils.getRequerimientosFiltrados();
        for (var i = 0; i < requerimientosFiltrado.length; i++) {
            var actual = requerimientosFiltrado[i];
            var mes = ($.segalmex.date.isoToFecha(actual.fechaCreacion)).substring(3, 10);
            var mesCiclo = mes + actual.ciclo.clave;
            var auxiliar = requerimientosMes['mes' + mesCiclo];
            if (auxiliar) {
                auxiliar.montoTotal = auxiliar.montoTotal + actual.montoTotal;
                auxiliar.montoTotalNoPago = auxiliar.montoTotalNoPago + actual.montoTotalNoPago;
                auxiliar.toneladasNoPago = auxiliar.toneladasNoPago + actual.toneladasNoPago;
                auxiliar.toneladasTotales = auxiliar.toneladasTotales + actual.toneladasTotales;
                auxiliar.facturas = auxiliar.facturas + actual.facturas;
                auxiliar.facturasNoPago = auxiliar.facturasNoPago + actual.facturasNoPago;
                auxiliar.cegap++;
            } else {
                var requerimiento = {
                    id: id++,
                    fechaCreacion: mes,
                    montoTotal: actual.montoTotal,
                    cegap: 1,
                    montoTotalNoPago: actual.montoTotalNoPago,
                    toneladasNoPago: actual.toneladasNoPago,
                    toneladasTotales: actual.toneladasTotales,
                    facturas: actual.facturas,
                    facturasNoPago: actual.facturasNoPago,
                    ciclo: actual.ciclo
                };
                requerimientosMes['mes' + mesCiclo] = requerimiento;
                data.requerimientosMes.push(requerimiento);
            }
        }
    };

    utils.getRequerimientosFiltrados = function () {
        var requerimientos = [];
        var cultivo = $.segalmex.get(data.cultivos, data.tipoBusqueda, 'clave');
        var ciclo = $.segalmex.get(data.ciclos, $('#ciclo').val());
        for (var i = 0; i < data.requerimientos.length; i++) {
            var r = data.requerimientos[i];
            if (r.cultivo.clave === cultivo.clave) {
                if (ciclo) {
                    if (r.ciclo.clave === ciclo.clave) {
                        requerimientos.push(r);
                    }
                } else {
                    requerimientos.push(r);
                }
            }
        }
        return requerimientos;
    };

})(jQuery);
