(function ($) {
    $.segalmex.namespace('segalmex.cultivos.administrar.parametro');
    var utils = {};
    var handlers = {};
    var data = {
    };

    $.segalmex.cultivos.administrar.parametro.init = function () {
        utils.cargaParametros();
        utils.inicializaValidaciones();
        utils.configuraPantalla();
    };

    utils.configuraPantalla = function () {
        $('#nuevo-button').click(utils.nuevoParametro);
        $('#regresar-button').click(utils.regresar);
        $('#guardar-button').click(utils.guarda);
        $('#edicion-button').click(utils.guardaEdicion);
        $('#cierra-edicion-button').click(handlers.cierraModalEdicion);
    };


    handlers.muestraModalEdicion = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('editar-param-'.length), 10);

        data.param = $.segalmex.get(data.parametros, idx);

        $('#edicion-clave').val(data.param.clave).attr('disabled', true);
        $('#edicion-grupo').val(data.param.grupo);
        $('#edicion-valor').val(data.param.valor);
        $('#edicion-orden').val(data.param.orden);
        $('#edicion-desc').val(data.param.descripcion);

        $('#edit-param-modal').modal('show');

    };

    handlers.cierraModalEdicion = function () {
        $('#edit-param-modal').modal('hide');
        $('#edit-param-modal .valid-field').val('').limpiaErrores();
    };


    utils.guarda = function () {
        $('#div-form-param .valid-field').limpiaErrores();
        var errores = [];
        $('#div-form-param .valid-field').valida(errores, true);
        if (errores.length !== 0) {
            return;
        }

        var parametro = {
            clave: $('#clave').val(),
            grupo: $('#grupo').val(),
            valor: $('#valor').val(),
            orden: $('#orden').val(),
            descripcion: $('#desc').val()
        };

        $.ajax({
            url: '/cultivos/resources/parametro/',
            type: 'POST',
            data: JSON.stringify(parametro),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            // Guardamos el registro
            alert('Parámetro guardado con éxito.');
            utils.cargaParametros();
            utils.regresar();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Ocurrió un error al guardar el registro.');
            }
        });
    };

    utils.guardaEdicion = function () {
        $('#edit-param-modal .valid-field').limpiaErrores();
        var errores = [];
        $('#edit-param-modal .valid-field').valida(errores, true);
        if (errores.length !== 0) {
            return;
        }

        var parametro = {
            clave: $('#edicion-clave').val(),
            grupo: $('#edicion-grupo').val(),
            valor: $('#edicion-valor').val(),
            orden: $('#edicion-orden').val(),
            descripcion: $('#edicion-desc').val()
        };

        $.ajax({
            url: '/cultivos/resources/parametro/edicion/',
            type: 'PUT',
            data: JSON.stringify(parametro),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            // Guardamos el registro
            alert('Parámetro editado con éxito.');
            utils.cargaParametros();
            utils.regresar();
            handlers.cierraModalEdicion();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Ocurrió un error al guardar el registro.');
            }
        });


    };

    handlers.eliminaParametro = function (e) {
        if(!confirm("¿Seguro que desea eliminar el parámetro?")){
            return ;
        }
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-parametro-'.length), 10);

        var parametro = data.param = $.segalmex.get(data.parametros, idx);

        $.ajax({
            url: '/cultivos/resources/parametro/',
            type: 'DELETE',
            data: JSON.stringify(parametro),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            // Guardamos el registro
            alert('Parámetro eliminado con éxito.');
            utils.cargaParametros();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Ocurrió un error al eliminar el registro.');
            }
        });
    };

    utils.inicializaValidaciones = function () {
        $(".valid-field").configura();

        $('#orden,#edicion-orden').configura({
            type: 'number'
        });

        $('#valor,#edicion-valor,#grupo,#edicion-grupo,#clave,#edicion-clave').configura({
            minlength: 1,
            maxlength: 2047,
            allowSpace: false,
            textTransform: null,
            pattern: /^[A-Za-z0-9ÑñÁÉÍÓÚáéíóúÄËÏÖÜäëïöü@\.,:\n\-\(\)" ]*$/
        });

        $('#desc,#edicion-desc').configura({
            type: 'comentario',
            maxlength: 2047
        });

        $('.valid-field').validacion();
    };

    utils.nuevoParametro = function () {
        utils.limpiaParametro();
        $('#table-parametros,#div-nuevo').hide();
        $('#div-form-param').show();
    };

    utils.regresar = function () {
        $('#table-parametros,#div-nuevo').show();
        $('#div-form-param').hide();
        utils.limpiaParametro();
    };

    utils.limpiaParametro = function () {
        $('#clave,#grupo,#valor,#orden,#desc').val('').limpiaErrores();
    };

    utils.cargaParametros = function (e) {
        $.ajax({
            url: '/cultivos/resources/parametro/',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            data.parametros = response;
            var table = utils.creaDataTable(response);
            $('#table-parametros').html(table);
            utils.configuraTablaResultados();
            $('#parametros-table').on('click', 'button.editar-param', handlers.muestraModalEdicion);
            $('#parametros-table').on('click', 'button.eliminar-parametro', handlers.eliminaParametro);
        }).fail(function () {
            alert('Error: Al cargar los parámetros.');
        });
    };

    utils.creaDataTable = function (registros) {
        var buffer = `
            <table id="parametros-table" class="table table-bordered table-hover table-striped">
              <thead class="thead-dark">
                <tr>
                  <th>#</th>
                  <th>Clave</th>
                  <th>Grupo</th>
                  <th>Valor</th>
                  <th>Descripción</th>
                  <th class="text-center">Acciones</th>
                </tr>
              </thead>
              <tbody>
        `;

        for (var i = 0; i < registros.length; i++) {
            var a = registros[i];
            buffer += `
                <tr>
                    <td>${i + 1}</td>
                    <td>${a.clave}</td>
                    <td>${a.grupo}</td>
                    <td>${a.valor}</td>
                    <td>${a.descripcion}</td>
                <td class="text-center">
                <button id="editar-param- ${a.id}
                " class="btn btn-primary btn-sm editar-param">
                <i class="fas fa-pen"></i></button>

                <button id="eliminar-parametro- ${a.id}
                " class="btn btn-danger btn-sm eliminar-parametro">
                    <i class="fa fa-minus-circle"></i></button>
                </td>
                </tr>
            `;
        }
        buffer += `
            </tbody>
            </table>
        `;

        return buffer;
    };

    utils.configuraTablaResultados = function () {
        var aoColumns = [{"bSortable": false}, {"bSortable": true}, {"bSortable": true},
            {"bSortable": true}, {"bSortable": true}, {"bSortable": false}];
        utils.configuraTablaParametros('parametros-table', aoColumns);
    };

    utils.configuraTablaParametros = function (tablaId, aoColumns) {
        var table = $('#' + tablaId).DataTable({
            "oLanguage": {
                "sEmptyTable": "No se encontró información para mostrar en la tabla",
                "sSearch": "Buscar ",
                "sZeroRecords": "No se encontraron resultados."
            },
            "bFilter": true,
            "bInfo": false,
            "bPaginate": false,
            "aoColumns": aoColumns,
            "aoColumnDefs": [{"bSortable": false, "aTargets": ["no-sort"]}]
        });
        table.on('order.dt search.dt', function () {
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).order([[1, 'asc']]).draw(false);
        $("#" + tablaId + "_filter input").addClass('input-buscar');
        $(".dataTables_empty").attr("colspan", "100%");
    };


})(jQuery);