/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


(function ($) {
    $.segalmex.namespace('segalmex.maiz.inscripcion.asignacion');

    var data = {
        tipoInscripcion: 'contratos',
        entradas: [],
        inscripcion: null,
        cultivos: [],
        ciclos: [],
        tiposRegistro: [],
        cultivoSeleccionado: ''
    };
    var handlers = {};
    var utils = {};

    $.segalmex.maiz.inscripcion.asignacion.init = function () {
        utils.cargaCatalogos();
        $.segalmex.common.bandejas.init({
            configuracion: utils.configuracionBandejas(),
            namespace: $.segalmex.maiz.inscripcion.asignacion,
            tipoInscripcion: data.tipoInscripcion
        });
        $('#menu-bandejas a.nav-link').click(handlers.cambiaBandeja);
        $('#button-regresar').click(handlers.regresar);
        $('#nuevo-comentario-button').click(handlers.nuevoComentario);
        $('#agregar-comentario-button').click(handlers.agregaComentario);
        $('#button-asignar').click(handlers.asignaValidador);
        $('#asignar-multiple-button').click(handlers.multipleAsignacionValidador);
        $('.cultivos').click(handlers.cambiaCultivo);
        $('#contenido-comentario').configura({
            minlength: 1,
            maxlength: 2047,
            allowSpace: true,
            textTransform: null,
            pattern: /^[A-Za-zÑñÁÉÍÓÚáéíóúÄËÏÖÜäëïöü\.,:\n\- ]*$/
        }).validacion();
        setTimeout(function () {
            utils.setCultivo();
        }, 100);
    };

    utils.setCultivo = function () {
        var cultivo = $.segalmex.getParameterByName('cultivo');
        switch (cultivo) {
            case 'maiz-comercial':
            case 'trigo':
                $('#button-' + cultivo).click();
                break;
            case 'arroz':
                $('#button-' + cultivo).click();
                $('#muestra-productores').click();
                break;
            default:
                alert('No hay cultivo seleccionado.');
        }
    };

    handlers.cambiaCultivo = function () {
        $('.cultivos').removeClass("active");
        $('#' + $(this).attr('id')).addClass("active");
        var cultivo = $(this).attr('id').substring(7);
        data.cultivoSeleccionado = cultivo;
        $.segalmex.common.bandejas.filtraRegstros(cultivo, '0', '0');
        $('#cultivo-label').val(cultivo === 'maiz-comercial' ? 'maíz' : cultivo).
                css('text-transform', 'capitalize');
    };

    $.segalmex.maiz.inscripcion.asignacion.cargaDatos = function (fn) {
        var datos = {
            cultivo: data.cultivoSeleccionado
        };
        if (data.cultivoSeleccionado === '') {
            delete datos.cultivo;
        }
        $.ajax({
            url: '/cultivos/resources/paginas/bandejas/asignacion/' + data.tipoInscripcion,
            data: datos,
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            data.entradas = response;
            fn(response);
        }).fail(function () {
            alert('Error: No se pudo obtener la lista de registros de contrato.');
        });
        return data.entradas;
    };

    $.segalmex.maiz.inscripcion.asignacion.limpiar = function () {
        data.folio = null;
        $('#detalle-elemento').html('');
        $('#detalles-entrada,#botones-entrada').hide();
        $('#usuario-validador').val('0');
    };

    $.segalmex.maiz.inscripcion.asignacion.mostrar = function (e) {
        e.preventDefault();
        var folio = e.target.id.substring('link-id-'.length);
        data.inscripcion = $.segalmex.get(data.entradas, folio, 'folio');
        var actual = $.segalmex.common.bandejas.actual();

        var tipoInscripcion = data.tipoInscripcion;
        $.ajax({
            url: '/cultivos/resources/' + tipoInscripcion + '/maiz/inscripcion/' + data.inscripcion.uuid,
            data: {comentarios: true, expand: 3, contratos: true},
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            // Mostramos el detalle
            utils.construyeInscripcion(response, tipoInscripcion);
            $('#lista-entradas').hide();
            $('.detalle-elemento-form').hide();

            $('#detalle-elemento-' + actual).show();
            $('#detalles-entrada,#botones-entrada').show();
        }).fail(function () {
            alert('Error: No fue posible obtener el registro.');
        });
    }

    $.segalmex.maiz.inscripcion.asignacion.construyeTabla = function (lista) {
        return $.segalmex.common.bandejas.vista.construyeTabla({
            id: 'table-inscripciones',
            lista: lista,
            link: true,
            btnActualizar: true,
            btnAsignar: true,
            tipoInscripcion: data.tipoInscripcion,
            filtro: true,
            tipoRegistro: true
        });
    };

    $.segalmex.maiz.inscripcion.asignacion.configuraTabla = function () {
        $.segalmex.common.bandejas.vista.configuraTablaInscripcion('table-inscripciones', data.tipoInscripcion);
        $('#ciclo').actualizaCombo(data.ciclos, {value: 'clave'});
        $('#tipo-registro').actualizaCombo(data.tiposRegistro, {value: 'clave'}).prop('disabled', true);
        $('#tipo-registro').change(utils.habilitaAsociado);
        $('#rfc-sociedad').configura({
            type: 'rfc-moral',
            required: false
        }).validacion();
    };

    utils.habilitaAsociado = function () {
        $('#rfc-sociedad').val('').limpiaErrores().prop('disabled', true);
        var tipoRegistro = $('#tipo-registro').val();
        if (tipoRegistro === 'asociado') {
            $('#rfc-sociedad').prop('disabled', false);
        }
    };

    handlers.cambiaBandeja = function (e) {
        e.preventDefault();
        var id = e.target.id.substring('muestra-'.length);
        if (data.tipoInscripcion === id) {
            return;
        }
        data.tipoInscripcion = id;
        data.folio = null;

        $('#menu-bandejas a.nav-link').removeClass('active');
        $(e.target).addClass('active');
        $.segalmex.common.bandejas.init({
            configuracion: utils.configuracionBandejas(),
            namespace: $.segalmex.maiz.inscripcion.asignacion,
            tipoInscripcion: data.tipoInscripcion,
            cultivo: data.cultivoSeleccionado
        });
        utils.habilitaAsociado();
    };

    handlers.asignaValidador = function (e) {
        e.preventDefault();
        $('#button-asignar').prop('disabled', true).html('Asignando...');
        var usuario = $('#usuario-validador').val();
        if (usuario === '0') {
            alert('Error: Seleccione un usuario validador.');
            $('#button-asignar').prop('disabled', false).html('Asignar validador');
            return;
        }

        $.ajax({
            url: '/cultivos/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + data.inscripcion.uuid + '/validacion/',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify({id: usuario})
        }).done(function (response) {
            alert('Validador asignado con éxito.');
            $('#button-asignar').prop('disabled', false).html('Asignar validador');
            $.segalmex.common.bandejas.actualizar();
            $('.detalle-elemento-form').hide();
            $('#detalle-elemento-comentarios').html('');
            $('#lista-entradas').show();
        }).fail(function () {
            alert('Error: No fue posible asignar el validador.');
            $('#button-asignar').prop('disabled', false).html('Asignar validador');
        });
    }

    handlers.multipleAsignacionValidador = function (e) {
        e.preventDefault();
        $('#asignacion-multiple-modal .modal-footer button').prop('disabled', true);
        var usuario = $('#usuario-validador-modal').val();
        if (usuario === '0') {
            alert('Error: Seleccione un usuario validador.');
            $('#asignacion-multiple-modal .modal-footer button').prop('disabled', false);
            return;
        }
        var cantidad = parseInt($('#cantidad-asignados').val(), 10);
        if (cantidad <= 0) {
            alert('Error: Seleccione la cantidad de asignaciones.');
            $('#asignacion-multiple-modal .modal-footer button').prop('disabled', false);
            return;
        }
        var rfcSociedad = $('#rfc-sociedad').val();
        var tipoRegistro = $('#tipo-registro').val();
        if (tipoRegistro === 'asociado' && rfcSociedad === '') {
            alert('Error: ingrese el RFC de la sociedad.');
            $('#asignacion-multiple-modal .modal-footer button').prop('disabled', false);
            return;
        }


        var errores = 0;
        var exitos = 0;

        var datos = {
            cultivo: data.cultivoSeleccionado,
            ciclo: $('#ciclo').val(),
            tipoRegistro: $('#tipo-registro').val(),
            rfcSociedad: rfcSociedad
        };

        if (datos.cultivo === '0') {
            delete datos.cultivo;
        }
        if (datos.ciclo === '0') {
            delete datos.ciclo;
        }
        if (datos.tipoRegistro === '0') {
            delete datos.tipoRegistro;
        }

        if (rfcSociedad === '') {
            delete datos.rfcSociedad;
        }

        $('#progress-bar-ayuda').html('Actualizando lista de inscripciones...');
        $.ajax({
            url: '/cultivos/resources/paginas/bandejas/asignacion/' + data.tipoInscripcion + '/',
            data: datos,
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            var limite = Math.min(response.length, cantidad);
            if (limite === 0) {
                $('#progress-bar-ayuda').html('Sin inscripciones para asignar.');
                $('#asignacion-multiple-modal .modal-footer button').prop('disabled', false);
                $('#asignar-multiple-button').prop('disabled', true);
                $.segalmex.common.bandejas.actualizar();
            }
            for (var i = 0; i < limite; i++) {
                var inscripcion = response[i];
                $.ajaxq('asignacionesQueue', {
                    url: '/cultivos/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + inscripcion.uuid + '/validacion/',
                    type: 'POST',
                    dataType: 'json',
                    contentType: 'application/json',
                    data: JSON.stringify({id: usuario}),
                }).done(function () {
                    exitos++;
                    utils.cargaBarraProgreso(exitos, limite);
                    $('#progress-bar-ayuda').html('Asignando validadores...');
                }).fail(function () {
                    errores++;
                }).always(function () {
                    if (exitos + errores === limite) {
                        $('#asignacion-multiple-modal .modal-footer button').prop('disabled', false);
                        $('#progress-bar-ayuda').html('Asignación de validadores finalizada.');
                        $('#asignar-multiple-button').prop('disabled', true);
                        $.segalmex.common.bandejas.actualizar();
                    }
                });
            }
        }).fail(function () {
            alert('Error: No se pudo obtener la lista de inscripciones.');
            $('#asignacion-multiple-modal .modal-footer button').prop('disabled', false);
        });

    }

    handlers.nuevoComentario = function (e) {
        $('#comentario-nuevo-modal').modal('show');
        $('#contenido-comentario').val('');
    }

    handlers.agregaComentario = function (e) {
        $('#contenido-comentario').limpiaErrores();
        var errores = [];
        $('#contenido-comentario').valida(errores, true);
        if (errores.length > 0) {
            return;
        }

        var comentario = {
            tipo: $.segalmex.common.bandejas.actual(),
            contenido: $('#contenido-comentario').val()
        }

        $.ajax({
            url: '/cultivos/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + data.inscripcion.uuid + '/comentario/',
            data: JSON.stringify(comentario),
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            $('#comentario-nuevo-modal').modal('hide');
        }).fail(function () {
            alert('Error: No fue posible agregar el comentario.');
            $('#botones-entrada button').prop('disabled', false);
        });
    }

    handlers.regresar = function (e) {
        e.preventDefault();

        $.segalmex.maiz.inscripcion.asignacion.limpiar();
        $('#lista-entradas').show();
    };

    utils.cargaCatalogos = function () {
        $.ajax({
            url: '/cultivos/resources/paginas/bandeja-asignacion/',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            $('#usuario-validador,#usuario-validador-modal').actualizaCombo(response.validadores);
            data.cultivos = response.cultivos;
            data.ciclos = response.ciclos;
            data.tiposRegistro = response.tiposRegistro;
        }).fail(function () {
            alert('Error: No se pudo obtener los catálogos de la página.');
        });
    };

    utils.configuracionBandejas = function () {
        switch (data.tipoInscripcion) {
            case 'contratos':
                return {
                    idBandeja: 'entradas-bandeja',
                    idLista: 'lista-entradas',
                    defaultBandeja: 'asignacion',
                    entradas: [
                        {nombre: 'Asignación', titulo: 'Asignación de contratos', prefijoId: 'asignacion', descripcion: 'Lista de registros de contratos para asignación de validador.'}
                    ]
                };
            case 'productores':
                return {
                    idBandeja: 'entradas-bandeja',
                    idLista: 'lista-entradas',
                    defaultBandeja: 'asignacion',
                    entradas: [
                        {nombre: 'Asignación', titulo: 'Asignación de productores', prefijoId: 'asignacion', descripcion: 'Lista de registros de productores para asignación de validador.'}
                    ]
                };
            case 'facturas':
                return {
                    idBandeja: 'entradas-bandeja',
                    idLista: 'lista-entradas',
                    defaultBandeja: 'asignacion',
                    entradas: [
                        {nombre: 'Asignación', titulo: 'Asignación de facturas', prefijoId: 'asignacion', descripcion: 'Lista de registros de facturas para asignación de validador.'}
                    ]
                };
        }
    };

    utils.construyeInscripcion = function (inscripcion, tipoInscripcion) {
        switch (tipoInscripcion) {
            case 'contratos':
                $('#detalle-elemento').html($.segalmex.maiz.inscripcion.vista.construyeInscripcion(inscripcion));
                $('#button-ver-productores').click($.segalmex.common.bandejas.muestraDetalleContratoProductor);
                $('#detalle-div').html('');
                $('#detalle-div').html($.segalmex.maiz.inscripcion.vista.generaTablaDetalleContratosProductor(inscripcion));
                $.segalmex.common.bandejas.configuraTablaContratosProductor();
                break;
            case 'productores':
                $.segalmex.maiz.inscripcion.productor.vista.muestraInscripcion(inscripcion, 'detalle-elemento');
                break;
            case 'facturas':
                $('#detalle-elemento').html($.segalmex.maiz.inscripcion.factura.vista.construyeInscripcion(inscripcion));
                break;
            default:
                alert('Registro desconocido');
        }
        $('#detalle-elemento-comentarios').html($.segalmex.maiz.inscripcion.vista.construyeComentarios(inscripcion.comentarios, $.segalmex.common.pagina.comun.registrado));
    };

    utils.cargaBarraProgreso = function (actual, total) {
        if (total === 0) {
            $('.progress-bar').css('width', 100 + '%');
            $('.progress-bar').attr('aria-valuenow', 100);
            $('.progress-bar').text(100 + '%');
        } else {
            var progreso = Math.trunc(actual * 100 / total);
            $('.progress-bar').css('width', progreso + '%');
            $('.progress-bar').attr('aria-valuenow', progreso);
            $('.progress-bar').text(progreso + '%');
        }
    };
})(jQuery);