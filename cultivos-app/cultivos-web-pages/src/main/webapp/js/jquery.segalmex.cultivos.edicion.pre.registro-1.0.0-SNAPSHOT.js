/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 * Common para las pantallas de Edición de Pre registro de productores.
 */

(function ($) {
    $.segalmex.namespace('segalmex.cultivos.edicion.pre.registro');
    var data = {
        predios: [],
        uuid: null,
        tiposPrecio: [],
        coberturaRequerida: false,
        cultivo: '',
        tipos: [],
        tiposIar: [],
        iars: [],
        claveBC: '2',
        claveSonora: '26',
        tiposCultivo: [],
        tiposIarRequerdidos: []
    };
    var handlers = {};
    var utils = {};

    $.segalmex.cultivos.edicion.pre.registro.init = function (params) {
        data.cultivo = params.cultivo;
        utils.inicializaValidaciones();
        utils.cargaCatalogos();
    };

    //Buscamos el pre-registro de acuerdo a los parametros ingresados
    $.segalmex.cultivos.edicion.pre.registro.busca = function (e) {
        e.preventDefault();
        $(e.target).prop('disabled', true);
        if ($('#folio-busqueda').val() === '' && $('#curp-busqueda').val() === '') {
            alert('Ingrese el número de folio o CURP para la búsqueda.');
            $('#button-buscar').prop('disabled', false);
            return;
        }

        var errores = [];
        $('.valid-field').valida(errores, true);
        if (errores.length > 0) {
            $('#button-buscar').prop('disabled', false);
            return;
        }

        var datos = {
            folio: $('#folio-busqueda').val(),
            curp: $('#curp-busqueda').val(),
            ciclo: $('#ciclo').val(),
            simplifica: false
        };

        for (var prop in datos) {
            if (datos[prop] === '') {
                delete datos[prop];
            }
        }

        $.ajax({
            url: '/' + data.cultivo + '/resources/productores/maiz/pre-registro/',
            type: 'GET',
            data: datos,
            dataType: 'json'
        }).done(function (response) {
            $('#button-buscar').prop('disabled', false);
            if (response.length === 0) {
                alert('No se encontró ningún resultado.');
                return;
            }
            utils.cargaCatalogosCultivo(response[0].ciclo.clave);
            data.preRegistro = response[0];
            $('#datos-busqueda').hide();
            $('#form-pre-registro').show();
        }).fail(function () {
            alert('Error: No se encontró resultado.');
            $('#button-buscar').prop('disabled', false);
        });
    };

    $.segalmex.cultivos.edicion.pre.registro.habilitaTipoPrecio = function () {
        var contratos = $('#total-contratos').val();
        $('#tipo-precio,#agregar-contrato-button,#cantidad-contratos').prop('disabled', true);
        if (contratos === 0 || contratos === undefined) {
            data.tipos = [];
            utils.construyeTablaTipoPrecios();
        }
        if (contratos > 0) {
            $('#tipo-precio,#agregar-contrato-button,#cantidad-contratos').prop('disabled', false);
        }
    };

    $.segalmex.cultivos.edicion.pre.registro.validaSiCobertura = function () {
        $('#actualizar-button').prop('disabled', false);
        if ($('#cobertura').val() === 'false') {
            alert('Es necesario adquirir un Instrumento de Administración de riesgos (IAR).');
            $('#actualizar-button').prop('disabled', true);
        }
    };

    $.segalmex.cultivos.edicion.pre.registro.limpiar = function () {
        $('#ciclo').val('0').limpiaErrores();
        $('#folio-busqueda,#curp-busqueda').val('').limpiaErrores();
    };

    $.segalmex.cultivos.edicion.pre.registro.actualiza = function (e) {
        e.preventDefault();
        $('#actualizar-button').prop('disabled', true);

        $('#predios-div .valid-field').prop('disabled', true);
        var errores = [];
        $('.valid-field').valida(errores, true);
        $('#predios-div .valid-field').prop('disabled', false);
        if (errores.length > 0) {
            $('#actualizar-button').prop('disabled', false);
            return;
        }
        //valida si el telefono y la confirmacion coinciden
        var telefono = $('#telefono').val();
        var telefonoConfirmacion = $('#confirmacion-telefono').val();
        if (telefono !== telefonoConfirmacion) {
            alert('Error: El número de teléfono y la confirmación no coinciden.');
            $('#actualizar-button').prop('disabled', false);
            return;
        }

        if (data.predios.length === 0) {
            alert('Error: Es necesario agregar al menos un predio.');
            $('#actualizar-button').prop('disabled', false);
            return;
        }
        //validacion del numero de predios
        var predios = parseInt($('#cantidad-predios').val(), 10);
        if (predios !== data.predios.length) {
            alert('Error: La cantidad de predios es diferente a los predios agregados.');
            $('#actualizar-button').prop('disabled', false);
            return;
        }

        if (data.cultivo !== 'arroz') {
            var numContratos = parseInt($('#total-contratos').val());
            var numIar = parseInt($('#total-iar').val());
            var cantidadTp = 0;
            var cantidadIar = 0;

            for (var i = 0; i < data.tipos.length; i++) {
                cantidadTp += data.tipos[i].cantidad;
            }

            for (var i = 0; i < data.iars.length; i++) {
                cantidadIar += data.iars[i].cantidad;
            }

            if (numIar === 0) {
                alert('El Total de IARs debe ser mayor que 0.');
                $('#actualizar-button').prop('disabled', false);
                return;
            }

            if (numContratos !== cantidadTp) {
                alert('Error: El total de contratos no coincide con la cantidad de precios agregados.');
                $('#actualizar-button').prop('disabled', false);
                return;
            }
            if (numIar !== cantidadIar) {
                alert('Error: El total de IARs no coincide con la cantidad de los IAR agregados.');
                $('#actualizar-button').prop('disabled', false);
                return;
            }
        }

        if ($('#curp').val() === $('#curp-beneficiario').val()) {
            alert('Error: La CURP del productor no puede ser igual al del beneficiario..');
            $('#actualizar-button').prop('disabled', false);
            $('#rendimiento').prop('disabled', true);
            return;
        }

        $('#actualizar-button').prop('disabled', false);
        utils.actualizaPreRegistro();
    };

    $.segalmex.cultivos.edicion.pre.registro.regresar = function () {
        $('#ciclo,#contratos-firados,#tipo,#tipo-posesion-predio,#regimen-hidrico-predio,#estado-predio,#munocipio-predio,#parentesco-beneficiario').val('0').limpiaErrores();
        $('input.valid-field').val('').limpiaErrores();
        $('#predios-table tbody').html('');
        data.predios = [];
        data.tipos = [];
        data.iars = [];
        $('#folio-busqueda').val('').limpiaErrores();
        $('#curp-busqueda').val('').limpiaErrores();
        $('#form-pre-registro').hide();
        $('#datos-busqueda').show();
        $('#ciclo').prop('disabled', false);
        data.tipos = [];
        data.coberturaRequerida = false;
    };

    utils.construyePreRegistro = function () {
        data.uuid = data.preRegistro.uuid;
        $('#curp').val(data.preRegistro.curp);
        $('#rfc').val(data.preRegistro.rfc);
        $('#nombre').val(data.preRegistro.nombre);
        $('#papellido').val(data.preRegistro.primerApellido);
        $('#sapellido').val(data.preRegistro.segundoApellido);
        $('#telefono').val(data.preRegistro.telefono);
        $('#confirmacion-telefono').val(data.preRegistro.telefono);
        $('#email').val(data.preRegistro.correoElectronico);
        $('#cantidad-predios').val(data.preRegistro.predios.length);
        data.predios = data.preRegistro.predios;
        utils.construyePredios();
        $('#ciclo').val(data.preRegistro.ciclo.id).prop('disabled', true);
        $('#nombre-beneficiario').val(data.preRegistro.nombreBeneficiario);
        $('#apellidos-beneficiario').val(data.preRegistro.apellidosBeneficiario);
        $('#curp-beneficiario').val(data.preRegistro.curpBeneficiario);
        if (data.preRegistro.parentesco.clave !== 'no-disponible') {
            $('#parentesco-beneficiario').val(data.preRegistro.parentesco.id);
        }

        if (data.preRegistro.cultivo.clave !== 'arroz') {
            $('#total-contratos').val(data.preRegistro.cantidadContratos);
            $('#tipo-precio,#cantidad-contratos').prop('disabled', data.preRegistro.cantidadContratos > 0 ? false : true);
            utils.defineTiposPrecio(data.preRegistro);
            utils.construyeTablaTipoPrecios();
            $('#cobertura').val('true');
            $('#total-iar').val(utils.calcultaTotalIar(data.preRegistro.coberturas));
            utils.defineIars(data.preRegistro.coberturas);
            $('#especifique-cobertura').prop('disabled', true);
        }
    };

    utils.defineTiposPrecio = function (pre) {
        if (pre.precioAbierto > 0) {
            utils.agregaTipos('precio-abierto', pre.precioAbierto);
        }
        if (pre.precioCerradoDolares > 0) {
            utils.agregaTipos('precio-cerrado', pre.precioCerradoDolares);
        }
        if (pre.precioCerradoPesos > 0) {
            utils.agregaTipos('precio-cerrado-pesos', pre.precioCerradoPesos);
        }
        if (pre.precioReferencia > 0) {
            utils.agregaTipos('precio-referencia', pre.precioReferencia);
        }
        if (pre.precioAbiertoPesos > 0) {
            utils.agregaTipos('precio-abierto-pesos', pre.precioAbiertoPesos);
        }
    };

    utils.agregaTipos = function (tipo, cantidad) {
        var tp = $.segalmex.get(data.tiposPrecio, tipo, 'clave');
        data.tipos.push({
            cantidad: cantidad,
            id: tp.id,
            clave: tp.clave,
            nombre: tp.nombre
        });
    };

    utils.construyeTablaTipoPrecios = function () {
        var buffer = [];
        for (var i = 0; i < data.tipos.length; i++) {
            var p = data.tipos[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(p.cantidad);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(p.nombre);
            buffer.push('</td>');
            buffer.push('<td class="text-center"><button id="eliminar-precio-');
            buffer.push(i);
            buffer.push('" class="btn btn-danger btn-sm eliminar-precio"><i class="fa fa-minus-circle"></i></button></td>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        $('#precios-table tbody').html(buffer.join(''));
    };

    utils.inicializaValidaciones = function () {
        $(".valid-field").configura();

        $('#curp,#reimpresion-curp,#curp-beneficiario').configura({
            type: 'curp'
        });

        $('#curp-busqueda').configura({
            type: 'curp',
            required: false
        });

        $('#nombre,#papellido,#sapellido,#nombre-beneficiario,#apellidos-beneficiario').configura({
            type: 'nombre-icao'
        }).configura({textTransform: 'upper'});
        ;
        $('#sapellido,#tipo-precio,#cantidad-contratos,#cantidad-iar,#tipo-iar').configura({
            required: false
        });
        $('#volumen-predio,#superficie-predio,#cantidad-predios,#cantidad-contratos,#total-contratos,#total-iar,#cantidad-iar').configura({
            type: 'number',
            min: 0
        });
        $('#rfc,#reimpresion-rfc').configura({
            type: 'rfc-fisica'
        });
        $('#telefono,#confirmacion-telefono').configura({
            pattern: /^\d{10}$/,
            minlength: 10,
            maxlength: 10,
            allowSpace: false
        });
        $('#email').configura({
            type: 'email'
        });

        $('#folio-busqueda').configura({
            pattern: /^\d{1,6}$/,
            minlength: 1,
            maxlength: 6,
            required: false
        });
        $('.valid-field').validacion();
    };

    utils.cargaCatalogosCultivo = function (ciclo) {
        var datos = {
            cultivo: data.cultivo === 'maiz' ? 'maiz-comercial' : data.cultivo,
            ciclo: ciclo
        };
        $.ajax({
            type: 'GET',
            url: '/cultivos/resources/paginas/productor/pre-registro',
            data: datos,
            dataType: 'json'
        }).done(function (response) {
            data.tiposPrecio = response.tiposPrecio;
            data.coberturas = response.coberturas;
            data.estados = response.estados;
            data.municipios = response.municipios;
            data.tiposIar = response.tiposIar;
            data.tiposCultivo = response.tipos;
            data.tiposIarRequerdidos = response.tiposRequierenIar;
            $('#estado-predio').actualizaCombo(data.estados);
            $('#tipo').actualizaCombo(response.tipos);
            $('#tipo-posesion-predio').actualizaCombo(response.tiposPosesion);
            $('#regimen-hidrico-predio').actualizaCombo(response.regimenes);
            $('#parentesco-beneficiario').actualizaCombo(response.parentescos);
            $('#tipo-precio').actualizaCombo(response.tiposPrecio);
            $('#tipo-iar').actualizaCombo(response.tiposIar, {value: 'clave'});
            $('#especifique-cobertura').actualizaCombo(response.coberturas);
            utils.construyePreRegistro();
        }).fail(function () {
            alert('Error: Al descargar los recursos de la página.');
        });
    };

    $.segalmex.cultivos.edicion.pre.registro.agregaPredio = function (e) {
        //validacion de campo
        $('#predios-div .valid-field').limpiaErrores();
        var errores = [];
        $('#predios-div .valid-field').valida(errores, true);
        if (errores.length > 0) {
            $('#' + errores[0].campo).focus();
            return;
        }
        var predio = {
            tipoCultivo: {id: $('#tipo').val(), nombre: $("#tipo option:selected").text()},
            tipoPosesion: {id: $('#tipo-posesion-predio').val(), nombre: $("#tipo-posesion-predio option:selected").text()},
            regimenHidrico: {id: $('#regimen-hidrico-predio').val(), nombre: $("#regimen-hidrico-predio option:selected").text()},
            superficie: $("#superficie-predio").val(),
            volumen: $("#volumen-predio").val(),
            estado: {id: $("#estado-predio").val(), nombre: $("#estado-predio option:selected").text()},
            municipio: {id: $('#municipio-predio').val(), nombre: $("#municipio-predio option:selected").text()}
        };
        data.predios.push(predio);
        utils.limpiaPredio();
        utils.construyePredios();
    };

    $.segalmex.cultivos.edicion.pre.registro.eliminaPredio = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-predio-'.length), 10);
        var predios = [];
        for (var i = 0; i < data.predios.length; i++) {
            if (i !== idx) {
                predios.push(data.predios[i]);
            }
        }
        data.predios = predios;
        utils.construyePredios();
    };

    $.segalmex.cultivos.edicion.pre.registro.cambiaEstadoPredio = function (e) {
        utils.cambiaEstado(e.target, 'municipio-predio');
    };

    utils.cargaCatalogos = function () {
        $.ajax({
            type: 'GET',
            url: '/cultivos/resources/paginas/productor/pre-registro/catalogo/',
            dataType: 'json'
        }).done(function (response) {
            $('#ciclo').actualizaCombo(response.ciclos);
        }).fail(function () {
            alert('Error: Al descargar los recursos de la página.');
        });
    };

    utils.actualizaPreRegistro = function () {
        var preRegistro = utils.generaDatos();
        $.ajax({
            url: '/' + data.cultivo + '/resources/productores/maiz/pre-registro/' + data.uuid,
            type: 'PUT',
            data: JSON.stringify(preRegistro),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('El pre registro fue actualizado correctamente.');
            $.segalmex.cultivos.edicion.pre.registro.regresar();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('ERROR: no se pudo actualizar el pre registro.');
            }
        });
    };

    utils.generaDatos = function () {
        var preRegistro = {
            nombre: $('#nombre').val(),
            primerApellido: $('#papellido').val(),
            segundoApellido: $('#sapellido').val(),
            curp: $('#curp').val(),
            rfc: $('#rfc').val(),
            telefono: $('#telefono').val(),
            correoElectronico: $('#email').val(),
            cantidadContratos: $('#total-contratos').val(),
            predios: data.predios,
            precioAbierto: 0,
            precioCerradoDolares: 0,
            precioCerradoPesos: 0,
            precioReferencia: 0,
            precioAbiertoPesos: 0,
            nombreBeneficiario: $('#nombre-beneficiario').val(),
            apellidosBeneficiario: $('#apellidos-beneficiario').val(),
            curpBeneficiario: $('#curp-beneficiario').val(),
            parentesco: {id: $('#parentesco-beneficiario').val()}
        };

        if (data.cultivo !== 'arroz') {
            preRegistro.coberturas = data.iars;

        }
        for (var i = 0; i < data.tipos.length; i++) {
            var tp = data.tipos[i];
            switch (tp.clave) {
                case 'precio-abierto':
                    preRegistro.precioAbierto = tp.cantidad;
                    break;
                case 'precio-abierto-pesos':
                    preRegistro.precioAbiertoPesos = tp.cantidad;
                    break;
                case 'precio-cerrado':
                    preRegistro.precioCerradoDolares = tp.cantidad;
                    break;
                case 'precio-cerrado-pesos':
                    preRegistro.precioCerradoPesos = tp.cantidad;
                    break;
                case 'precio-referencia':
                    preRegistro.precioReferencia = tp.cantidad;
                    break;
            }
        }
        return preRegistro;
    };

    utils.cambiaEstado = function (target, idMunicipio) {
        var v = $(target).val();
        var estado = v !== '0' ? $.segalmex.get(data.estados, v) : {clave: '0'};
        var municipios = [];
        if (estado.clave !== '0') {
            for (var i = 0; i < data.municipios.length; i++) {
                var m = data.municipios[i];
                if (m.estado.id === estado.id) {
                    municipios.push(m);
                }
            }
        }
        $('#' + idMunicipio).actualizaCombo(municipios).val('0');
    };

    utils.construyePredios = function () {
        var buffer = [];
        for (var i = 0; i < data.predios.length; i++) {
            var predio = data.predios[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-right">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.tipoCultivo.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.tipoPosesion.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.regimenHidrico.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.volumen);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.superficie);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.municipio.nombre);
            buffer.push(', ');
            buffer.push(predio.estado.nombre);
            buffer.push('</td>');
            buffer.push('<td class="text-center"><button id="eliminar-predio-');
            buffer.push(i);
            buffer.push('" class="btn btn-danger btn-sm eliminar-predio"><i class="fa fa-minus-circle"></i></button></td>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        $('#predios-table tbody').html(buffer.join(''));
    };

    utils.limpiaPredio = function () {
        $('#predios-div input.valid-field').val('');
        $('#predios-div select.valid-field').val('0').change();
        $('#predios-div .valid-field').limpiaErrores();
    };

    $.segalmex.cultivos.edicion.pre.registro.agregaTipoPrecio = function () {
        var tipo = parseInt($('#tipo-precio').val());
        var cantidad = parseInt($('#cantidad-contratos').val());
        if (tipo === 0) {
            alert('Seleccione un tipo de precio.');
            return;
        }
        if (cantidad < 1) {
            alert('La cantidad debe ser mayor o igual a 1.');
            return;
        }

        var errores = [];
        $('#cantidad-contratos').configura({required: true});
        $('#cantidad-contratos').valida(errores, true);
        if (errores.length > 0) {
            return;
        }
        $('#cantidad-contratos').configura({required: false});
        var tc = {};
        for (var i = 0; i < data.tiposPrecio.length; i++) {
            if (tipo === data.tiposPrecio[i].id) {
                tc = {
                    cantidad: cantidad,
                    id: data.tiposPrecio[i].id,
                    clave: data.tiposPrecio[i].clave,
                    nombre: data.tiposPrecio[i].nombre
                };
            }
        }
        utils.calculaTiposPrecio(tc);
        $('#tipo-precio').val('0').limpiaErrores();
        $('#cantidad-contratos').val('').limpiaErrores();
        utils.construyeTablaTipoPrecios();
    };

    $.segalmex.cultivos.edicion.pre.registro.eliminaTipoPrecio = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-precio-'.length), 10);
        var precios = [];
        for (var i = 0; i < data.tipos.length; i++) {
            if (i !== idx) {
                precios.push(data.tipos[i]);
            }
        }
        data.tipos = precios;
        utils.construyeTablaTipoPrecios();
    };

    $.segalmex.cultivos.edicion.pre.registro.validaTipoCobertura = function (e) {
        var v = $(e.target).val();
        $('#especifique-cobertura').prop('disabled', true).limpiaErrores().val('0');
        if (utils.validaTipoIarRequerido(v)) {
            $('#especifique-cobertura').prop('disabled', false);
        }
    };

    $.segalmex.cultivos.edicion.pre.registro.validaCurpRfc = function () {
        var curp = $('#curp').val();
        var rfc = $('#rfc').val();

        if (curp.length >= 10 && rfc.length >= 10
                && curp.substring(0, 10) !== rfc.substring(0, 10)) {
            alert('La CURP y el RFC no coinciden en los primeros 10 caracteres.');
        }
    };

    $.segalmex.cultivos.edicion.pre.registro.filtraEstados = function () {
        if (data.cultivo === 'trigo' && data.preRegistro.ciclo.clave === 'oi-2022' && $('#tipo').val() !== '0') {
            $('#estado-predio').val('0').change().limpiaErrores();
            var v = parseInt($('#tipo').val());
            var tc;
            var estados = [];
            for (var i = 0; i < data.tiposCultivo.length; i++) {
                if (v === data.tiposCultivo[i].id) {
                    tc = data.tiposCultivo[i];
                }
            }
            switch (tc.clave) {
                case 'trigo-cristalino':
                    for (var i = 0; i < data.estados.length; i++) {
                        if (data.estados[i].clave === data.claveBC || data.estados[i].clave === data.claveSonora) {
                            estados.push(data.estados[i]);
                        }
                    }
                    $('#estado-predio').actualizaCombo(estados);
                    break;
                default :
                    $('#estado-predio').actualizaCombo(data.estados);
            }
        }
    };

    utils.calculaTiposPrecio = function (tc) {
        if (data.tipos.length > 0) {
            var existe = false;
            for (var i = 0; i < data.tipos.length; i++) {
                if (data.tipos[i].clave === tc.clave) {
                    data.tipos[i].cantidad += tc.cantidad;
                    existe = true;
                    return;
                }
            }
            if (!existe) {
                data.tipos.push(tc);
                return;
            }
        } else {
            data.tipos.push(tc);
        }
    };

    $.segalmex.cultivos.edicion.pre.registro.agregaIar = function () {
        var tipo = $('#tipo-iar').val();
        var cantidad = parseInt($('#cantidad-iar').val());

        var errores = [];
        $('#cantidad-iar').configura({required: true});
        $('#cantidad-iar').valida(errores, true);
        if (errores.length > 0) {
            return;
        }
        $('#cantidad-iar').configura({required: false});
        if (cantidad < 1) {
            alert('La cantidad debe ser mayor o igual a 1.');
            return;
        }

        if (tipo === '0') {
            alert('Seleccione el tipo de IAR.');
            return;
        }

        if (utils.validaTipoIarRequerido(tipo)) {
            if ($('#especifique-cobertura').val() === '0') {
                alert('Seleccione una correduría.');
                return;
            }
        }

        var iar = {
            cantidad: cantidad
        };
        for (var i = 0; i < data.coberturas.length; i++) {
            if (parseInt($('#especifique-cobertura').val()) === data.coberturas[i].id) {
                iar.entidadCobertura = data.coberturas[i];
            }
        }

        for (var i = 0; i < data.tiposIar.length; i++) {
            if (tipo === data.tiposIar[i].clave) {
                iar.tipoIar = data.tiposIar[i];
            }
        }

        if (data.iars.length > 0) {
            for (var i = 0; i < data.iars.length; i++) {
                if (data.iars[i].entidadCobertura && iar.entidadCobertura
                        && data.iars[i].entidadCobertura.clave === iar.entidadCobertura.clave
                        && data.iars[i].tipoIar.clave === iar.tipoIar.clave) {
                    alert('Ya se agregó una IAR de tipo: ' + iar.tipoIar.nombre + ', con la correduría: ' + iar.entidadCobertura.nombre);
                    return;
                }
                if (data.iars[i].tipoIar.clave === iar.tipoIar.clave && !iar.entidadCobertura) {
                    alert('Ya se agregó una IAR de tipo: ' + iar.tipoIar.nombre + '.');
                    return;
                }
            }
        }
        data.iars.push(iar);
        $('#tipo-iar').val('0').limpiaErrores();
        $('#tipo-iar').change();
        $('#cantidad-iar').val('').limpiaErrores();
        utils.construyeTablaIars();
    };

    $.segalmex.cultivos.edicion.pre.registro.eliminaIar = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-iar-'.length), 10);
        var iars = [];
        for (var i = 0; i < data.iars.length; i++) {
            if (i !== idx) {
                iars.push(data.iars[i]);
            }
        }
        data.iars = iars;
        utils.construyeTablaIars();
    };

    utils.calcultaTotalIar = function (coberturas) {
        var total = 0;
        for (var i = 0; i < coberturas.length; i++) {
            total += coberturas[i].cantidad;
        }
        return total;
    };

    utils.construyeTablaIars = function () {
        var buffer = [];
        for (var i = 0; i < data.iars.length; i++) {
            var iar = data.iars[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(iar.tipoIar.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(iar.cantidad);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(iar.entidadCobertura ? iar.entidadCobertura.nombre : '--');
            buffer.push('</td>');
            buffer.push('<td class="text-center"><button id="eliminar-iar-');
            buffer.push(i);
            buffer.push('" class="btn btn-danger btn-sm eliminar-iar"><i class="fa fa-minus-circle"></i></button></td>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        $('#iars-table tbody').html(buffer.join(''));
    };

    utils.defineIars = function (coberturas) {
        for (var i = 0; i < coberturas.length; i++) {
            data.iars.push({
                cantidad: coberturas[i].cantidad,
                entidadCobertura: coberturas[i].entidadCobertura,
                tipoIar: coberturas[i].tipoIar
            });
        }
        utils.construyeTablaIars();
    };

    utils.validaTipoIarRequerido = function (tipo) {
        for (var i = 0; i < data.tiposIarRequerdidos.length; i++) {
            if (data.tiposIarRequerdidos[i].valor === tipo) {
                return true;
            }
        }
        return false;
    };
})(jQuery);
