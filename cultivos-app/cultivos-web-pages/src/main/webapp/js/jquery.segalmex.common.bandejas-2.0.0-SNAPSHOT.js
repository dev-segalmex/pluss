(function ($) {
    $.segalmex.namespace('segalmex.common.bandejas');

    var data = {
        configuracion: [],
        suffix: '-tab',
        contexto: null,
        idDetalleElemento: 'detalle-elemento',
        idBotonesEntrada: 'botones-entrada',
        idDivLista: 'div-lista', // El id en donde se muestra la tabla con los elementos de la bandeja
        filtradas: [],
        bandejaActual: '',
        tipoInscripcion: '',
        cultivoSeleccionado: ''
    };
    var util = {};
    var handlers = {};

    /**
     * Inicializa las bandejas con base en los parámetros especificados.
     *
     * @param {type} params
     * @returns {undefined}
     */
    $.segalmex.common.bandejas.init = function (params) {
        data.contexto = params.namespace;
        data.configuracion = params.configuracion;
        data.tipoInscripcion = params.tipoInscripcion;
        data.cultivoSeleccionado = params.cultivo;
        $('#' + data.idDivLista).html('<div class="text-center alert alert-secondary"><div class="spinner-border" role="status"><span class="sr-only">Cargando...</span></div><div>Inicializando bandeja...</div></div>');

        data.contexto.cargaDatos(function (response) {
            util.filtra(response, data.configuracion.entradas);
            $("#detalles-entrada").hide();
            util.creaTabs(data.configuracion.idBandeja, data.configuracion.entradas);
            util.creaSeccionDetalleLista(data.configuracion.idLista, data.configuracion.entradas);
            util.inicializaBandeja(data.configuracion.defaultBandeja);
            $('#estatus-tabs').delegate('a', 'click', util.muestraBandeja);
            $('#tipo-registro').prop('disabled', data.tipoInscripcion === 'productores' ? false : true);
        });
    }

    $.segalmex.common.bandejas.actualizar = function () {
        // Ocultamos los detalle de la entrada seleccionada y los botones
        $('#detalles-entrada').hide();
        $('#' + data.idBotonesEntrada).hide();
        $('#' + data.idDivLista).html('<div class="text-center alert alert-secondary"><div class="spinner-border" role="status"><span class="sr-only">Cargando...</span></div><div>Inicializando bandeja...</div></div>');
        $('#lista-entradas').show();

        // Cargamos nuevamente los datos que generan las bandejas y filtramos por tipo
        data.contexto.cargaDatos(function (response) {
            var datos = response;
            util.filtra(datos, data.configuracion.entradas);

            // Mostramos la bandeja actual con los datos actualizados y los contadores
            util.muestraTabla(data.bandejaActual);
            util.actualizaContadores();
            $('#tipo-registro').prop('disabled', data.tipoInscripcion === 'productores' ? false : true);
        });
    };

    $.segalmex.common.bandejas.actual = function () {
        return data.bandejaActual;
    }

    util.filtra = function (datos, entradas) {
        data.filtradas = [];
        $.each(entradas, function () {
            var bandeja = this.prefijoId;
            data.filtradas[bandeja] = $.grep(datos, function (dato) {
                return dato.tipo === bandeja;
            });
        });
    };

    util.creaTabs = function (idSeccion, entradas) {
        var buffer = [];

        buffer.push('<div id="estatus-tabs" class="list-group">');

        $.each(entradas, function () {
            buffer.push('<a href="#" class="estatus-tab list-group-item list-group-item-action d-flex justify-content-between align-items-center" ');
            buffer.push('id="');
            buffer.push(this.prefijoId);
            buffer.push('-tab" > ');
            buffer.push(this.nombre);
            buffer.push(' &nbsp; <span id="');
            buffer.push(this.prefijoId);
            buffer.push('-badge" class="badge badge-pill badge-secondary">');
            buffer.push(data.filtradas[this.prefijoId] &&
                    data.filtradas[this.prefijoId].length > 0 ?
                    data.filtradas[this.prefijoId].length : "0");
            buffer.push('</span>');
            buffer.push('</a>');
        });

        buffer.push('</div>');

        $("#" + idSeccion).html(buffer.join(''));
    };

    /**
     * Crea el detalle de una bandeja seleccionada sobre el id de sección que se le especifique
     * @param {type} idSeccion la sección donde se va a pintar el detalle
     * @param {type} entradas el json de entrada para crear el detalle
     * @returns {undefined}
     */
    util.creaSeccionDetalleLista = function (idLista, entradas) {
        var buffer = [];

        $.each(entradas, function () {
            buffer.push('<div id="');
            buffer.push(this.prefijoId);
            buffer.push('-detalle-entrada" class="detalle-bandeja" style="display:none;">');
            buffer.push('<h2>');
            buffer.push(this.titulo ? this.titulo : this.nombre);
            buffer.push('</h2>');
            buffer.push('<p>');
            buffer.push(this.descripcion);
            buffer.push('</p>');
            buffer.push('<hr/></div>');
        });
        ``
        buffer.push('<div id="' + data.idDivLista + '">');
        buffer.push('</div>');

        $("#" + idLista).html(buffer.join(''));
    };

    /**
     * Inicializa la bandeja que se le indica a traves de su id de bandeja
     * @param {type} idBandeja
     */
    util.inicializaBandeja = function (idBandeja) {
        $(".estatus-tab").removeClass("list-group-item-success");
        $("#" + idBandeja + "-tab").addClass("list-group-item-success");
        data.bandejaActual = idBandeja;
        util.regresaLista();
        util.muestraTabla(idBandeja);
        $("#" + idBandeja + "-detalle-entrada").show();
        $('#lista-entradas').show();
    };

    util.regresaLista = function () {
        $(".detalle-bandeja").hide();
        $("#" + data.bandejaActual + "-detalle-entrada").show();
        $("#" + data.bandejaActual + "-tab").focus();
    };

    util.muestraTabla = function (idBandeja) {
        $("#" + data.idDivLista).html('');
        $('#' + data.idDetalleElemento).html('');
        $('#' + data.idBotonesEntrada).hide();
        if (!data.filtradas[idBandeja]) {
            alert('No se encuentran datos para  la bandeja seleccionada:' + idBandeja);
        }
        $("#" + data.idDivLista).html(data.contexto.construyeTabla(data.filtradas[idBandeja]));
        data.contexto.configuraTabla();
        $('.detalle-entrada').click(data.contexto.mostrar);
        $('#actualizar-lista').click($.segalmex.common.bandejas.actualizar);
        $('#asignar-modal').click(function (e) {
            if (data.cultivoSeleccionado === undefined || data.cultivoSeleccionado === '') {
                alert('Error: es necesario seleccionar un cultivo.');
                return;
            }
            $('#asignacion-multiple-modal .modal-footer button').prop('disabled', false);
            $('#usuario-validador-modal,#cantidad-asignados').val('0');
            $('.progress-bar').css('width', '0%');
            $('.progress-bar').attr('aria-valuenow', 0);
            $('#progress-bar-ayuda').html('Seleccione usuario y cantidad.');
            $('#asignacion-multiple-modal').modal('show');
        });
        $('#filtrar').click($.segalmex.common.bandejas.filtraPorCiclo);
    };

    util.actualizaContadores = function () {
        $.each(data.configuracion.entradas, function () {
            var filtradas = data.filtradas[this.prefijoId];
            var cantidad = filtradas && filtradas.length > 0 ? filtradas.length : 0;
            $("#" + this.prefijoId + "-badge").html(cantidad);
        });
    };

    util.muestraBandeja = function (event) {
        data.contexto.limpiar();
        event.preventDefault();

        var id = event.target.id;

        if (event.target.localName === 'span') {
            id = event.target.parentElement.id;
        }

        var prefijo = id.substring(0, id.length - data.suffix.length);
        $(".detalle-bandeja").hide();
        util.inicializaBandeja(prefijo);
        util.actualizaContadores();
    };


    $.segalmex.common.bandejas.filtraPorCiclo = function (e) {
        e.preventDefault();
        var ciclo = $('#ciclo').val();
        var tipoRegistro = $('#tipo-registro').val();
        var rfcSociedad = $('#rfc-sociedad').val();
        if (ciclo === '0') {
            alert('Indique el ciclo.');
            return;
        }
        var errores = [];

        $('#rfc-sociedad').valida(errores, true);
        if (errores.length !== 0) {
            $('#' + errores[0].campo).focus();
            return;
        }
        datos = {
            ciclo: ciclo,
            tipoRegistro: tipoRegistro,
            rfcSociedad: rfcSociedad
        };

        if (tipoRegistro === '0') {
            delete datos.tipoRegistro;
        }

        $.segalmex.common.bandejas.filtraRegstros(data.cultivoSeleccionado, datos.ciclo, datos.tipoRegistro, datos.rfcSociedad);
    };

    $.segalmex.common.bandejas.filtraRegstros = function (cultivo, ciclo, tipoRegistro, rfcSociedad) {
        data.cultivoSeleccionado = cultivo;
        datos = {
            cultivo: data.cultivoSeleccionado,
            ciclo: ciclo,
            tipoRegistro: tipoRegistro,
            rfcSociedad: rfcSociedad
        };

        if (cultivo === '') {
            delete datos.cultivo;
        }
        if (ciclo === '0') {
            delete datos.ciclo;
        }
        if (tipoRegistro === '0' || tipoRegistro === undefined) {
            delete datos.tipoRegistro;
        }
        if (rfcSociedad === '') {
            delete datos.rfcSociedad;
        }
        $.ajax({
            url: '/cultivos/resources/paginas/bandejas/' + data.configuracion.defaultBandeja + '/' + data.tipoInscripcion,
            data: datos,
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            $('#detalles-entrada').hide();
            $('#' + data.idBotonesEntrada).hide();
            $('#' + data.idDivLista).html('<div class="text-center alert alert-secondary"><div class="spinner-border" role="status"><span class="sr-only">Cargando...</span></div><div>Inicializando bandeja...</div></div>');
            $('#lista-entradas').show();
            var datos = response;
            util.filtra(datos, data.configuracion.entradas);
            util.muestraTabla(data.bandejaActual);
            util.actualizaContadores();
            $('#cultivo').val(cultivo);
            $('#ciclo').val(ciclo);
            if (tipoRegistro !== undefined) {
                $('#tipo-registro').val(tipoRegistro).prop('disabled', data.tipoInscripcion === 'productores' ? false : true);
                $('#rfc-sociedad').val(rfcSociedad).prop('disabled', tipoRegistro === 'asociado' ? false : true);
            } else {
                $('#tipo-registro').prop('disabled', data.tipoInscripcion === 'productores' ? false : true);
            }
            data.contexto.cargaDatos(function () {});
        }).fail(function () {
            alert('Error: hubo un error al filtrar los registros.');
        });
    };

    $.segalmex.common.bandejas.muestraDetalleContratoProductor = function (e) {
        e.preventDefault();
        $('#detalle-contrato-productor-modal').modal('show').focus();
    };

    $.segalmex.common.bandejas.configuraTablaContratosProductor = function () {
        var aoColumns = [{"bSortable": false}, {"bSortable": true},
            {"bSortable": true}, {"bSortable": true}, {"bSortable": false},
            {"bSortable": false}, {"bSortable": false}];
        $.segalmex.common.bandejas.configuraTablaContratoProductor('detalle-productores-table', aoColumns);
    };

    $.segalmex.common.bandejas.configuraTablaContratoProductor = function (tablaId, aoColumns) {
        var table = $('#' + tablaId).DataTable({
            "oLanguage": {
                "sEmptyTable": "No se encontró información para mostrar en la tabla",
                "sSearch": "Buscar ",
                "sZeroRecords": "No se encontraron resultados."
            },
            "bFilter": true,
            "bInfo": false,
            "bPaginate": false,
            "aoColumns": aoColumns,
            "aoColumnDefs": [{"bSortable": false, "aTargets": ["no-sort"]}]
        });
        table.on('order.dt search.dt', function () {
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).order([[1, 'asc']]).draw(false);
        $("#" + tablaId + "_filter input").addClass('input-buscar');
        $(".dataTables_empty").attr("colspan", "100%");
    };
})(jQuery);