/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.maiz.impresion.ventanilla');
    var utils = {};

    $.segalmex.maiz.impresion.ventanilla.init = function () {
        var uuid = $.segalmex.getParameterByName('uuid');
        utils.getRegistro(uuid);
    };

    utils.getRegistro = function (uuid) {
        $.ajax({
            url: '/cultivos/resources/empresas/' + uuid + '/ventanilla/',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            // Mostramos el detalle
            $('#registro-ventanilla').html($.segalmex.maiz.vista.validacion.empresa.construyeRegistro(response, true));
            $('#detalle-ventanilla').show();
        }).fail(function () {
            alert('Error: No fue posible obtener el registro.');
        });
    };

})(jQuery);