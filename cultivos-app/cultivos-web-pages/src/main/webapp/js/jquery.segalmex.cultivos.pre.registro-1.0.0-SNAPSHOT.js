/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.cultivos.pre.registro');
    var data = {
        predios: [],
        cicloActivo: {},
        cultivo: '',
        tipos: [],
        tiposIar: [],
        iars: [],
        claveBC: '2',
        claveSonora: '26',
        tiposIarRequerdidos: []
    };
    var handlers = {};
    var utils = {};

    $.segalmex.cultivos.pre.registro.init = function (params) {
        data.cultivo = params.cultivo;
        utils.inicializaValidaciones();
        utils.cargaCatalogos(params.cultivo);
    };

    utils.inicializaValidaciones = function () {
        $(".valid-field").configura();

        $('#curp,#reimpresion-curp,#curp-beneficiario').configura({
            type: 'curp'
        });

        $('#nombre,#papellido,#sapellido,#nombre-beneficiario,#apellidos-beneficiario').configura({
            type: 'nombre-icao'
        }).configura({textTransform: 'upper'});
        ;
        $('#sapellido,#tipo-precio,#cantidad-contratos,#cantidad-iar,#tipo-iar').configura({
            required: false
        });
        $('#volumen-predio,#superficie-predio,#cantidad-predios,#total-contratos,#cantidad-contratos,#total-iar,#cantidad-iar').configura({
            type: 'number',
            min: 0
        });
        $('#rfc,#reimpresion-rfc').configura({
            type: 'rfc-fisica'
        });
        $('#telefono,#confirmacion-telefono').configura({
            pattern: /^\d{10}$/,
            minlength: 10,
            maxlength: 10,
            allowSpace: false
        });
        $('#email').configura({
            type: 'email'
        });
        $('#reimpresion-ciclo').configura({
            required: true
        });

        $('.valid-field').validacion();
    };

    $.segalmex.cultivos.pre.registro.guarda = function () {
        //valida si el telefono y la confirmacion coinciden.
        var telefono = $('#telefono').val();
        var telefonoConfirmacion = $('#confirmacion-telefono').val();
        if (telefono !== telefonoConfirmacion) {
            alert('Error: El número de teléfono y la confirmación no coinciden.');
            $('#guardar-button').html("Guardar").prop('disabled', false);
            $('#rendimiento').prop('disabled', true);
            return;
        }

        if (data.predios.length === 0) {
            alert('Error: Es necesario agregar al menos un predio.');
            $('#guardar-button').html("Guardar").prop('disabled', false);
            $('#rendimiento').prop('disabled', true);
            return;
        }
        //validacion del numero de predios
        var predios = parseInt($('#cantidad-predios').val(), 10);
        if (predios !== data.predios.length) {
            alert('Error: La cantidad de predios es diferente a los predios agregados.');
            $('#guardar-button').html("Guardar").prop('disabled', false);
            $('#rendimiento').prop('disabled', true);
            return;
        }

        if (data.cultivo !== 'arroz') {
            var numContratos = parseInt($('#total-contratos').val());
            var numIar = parseInt($('#total-iar').val());
            var cantidadTp = 0;
            var cantidadIar = 0;

            for (var i = 0; i < data.tipos.length; i++) {
                cantidadTp += data.tipos[i].cantidad;
            }

            for (var i = 0; i < data.iars.length; i++) {
                cantidadIar += data.iars[i].cantidad;
            }

            if (numIar < 1) {
                alert('Es necesario agregar al menos un IAR.');
                $('#guardar-button').html("Guardar").prop('disabled', false);
                $('#rendimiento').prop('disabled', true);
                return;
            }

            if (numContratos !== cantidadTp) {
                alert('Error: El total de contratos no coincide con la cantidad de precios agregados.');
                $('#guardar-button').html("Guardar").prop('disabled', false);
                $('#rendimiento').prop('disabled', true);
                return;
            }
            if (numIar !== cantidadIar) {
                alert('Error: El total de IARs no coincide con la cantidad de los IAR agregados.');
                $('#guardar-button').html("Guardar").prop('disabled', false);
                $('#rendimiento').prop('disabled', true);
                return;
            }
        }

        if ($('#curp').val() === $('#curp-beneficiario').val()) {
            alert('Error: La CURP del productor no puede ser igual al del beneficiario.');
            $('#guardar-button').html("Guardar").prop('disabled', false);
            $('#rendimiento').prop('disabled', true);
            return;
        }

        $('#div-terminos').html(utils.getTerminosCondiciones());
        $('#rendimiento').prop('disabled', true);
        $('#condiciones-modal').modal('show');
    };

    $.segalmex.cultivos.pre.registro.aceptoCondiciones = function (e) {
        e.preventDefault();
        $('#si-acepto-condiciones,#no-acepto-condiciones').prop('disabled', true);
        utils.guardaPreInscripcion();
    };

    $.segalmex.cultivos.pre.registro.noAceptoCondiciones = function (e) {
        $('#condiciones-modal').modal('hide');
        $('#guardar-button').html('Guardar').prop('disabled', false);
    };

    utils.guardaPreInscripcion = function () {
        $('#si-acepto-condiciones,#no-acepto-condiciones').prop('disabled', true);
        var preRegistro = utils.generaDatos();
        var cultivo = data.cultivo === 'maiz-comercial' ? 'maiz' : data.cultivo;

        $.ajax({
            url: '/' + cultivo + '/resources/productores/maiz/pre-registro/',
            type: 'POST',
            data: JSON.stringify(preRegistro),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('El pre registro se ha guardado con el folio: ' + response.folio);
            $('#condiciones-modal').modal('hide');
            $('#si-acepto-condiciones,#no-acepto-condiciones').prop('disabled', false);
            $('#guardar-button').html('Guardar').prop('disabled', false);
            $('#abrir-descarga').attr('href', '/' + cultivo + '/resources/productores/maiz/pre-registro/' + response.uuid + '/pre-registro.pdf');
            $('#descarga-modal').modal('show');
            $.segalmex.cultivos.pre.registro.limpiar();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('ERROR: no se pudo guardar el pre registro');
            }
            $('#si-acepto-condiciones,#no-acepto-condiciones').prop('disabled', false);
            $('#guardar-button').html('Guardar').prop('disabled', false);
        });
    };

    $.segalmex.cultivos.pre.registro.reimprime = function (e) {
        e.preventDefault();
        $('#reimpresion-curp,#reimpresion-rfc').val('').limpiaErrores();
        $('#reimpresion-ciclo').val('0').limpiaErrores();
        $('#reimpresion-link').attr('href', '#').addClass('disabled');
        $('#reimprimir-modal').modal('show');
        setTimeout(function () {
            $('#reimpresion-curp').focus();
        }, 500);
    };

    $.segalmex.cultivos.pre.registro.confirmaReimpresion = function (e) {
        e.preventDefault();
        var cultivo = data.cultivo === 'maiz-comercial' ? 'maiz' : data.cultivo;
        $('#reimprimir-modal .valid-field').limpiaErrores();
        var errores = [];
        $('#reimprimir-modal .valid-field').valida(errores, true);
        if (errores.length > 0) {
            return;
        }

        var curp = $('#reimpresion-curp').val();
        var rfc = $('#reimpresion-rfc').val();
        var ciclo = $('#reimpresion-ciclo').val();
        $('#reimpresion-link').attr('href', '/' + cultivo + '/resources/productores/maiz/pre-registro/'
                + curp + '/reimpresion/' + rfc + "/" + ciclo + '/pre-registro.pdf').removeClass('disabled');

    };

    $.segalmex.cultivos.pre.registro.limpiar = function () {
        $('select.valid-field').val('0').limpiaErrores();
        $('input.valid-field').val('').limpiaErrores();
        $('#cultivo').val(utils.getNombreCultivo());
        $('#ciclo-agricola').val(data.cicloActivo.nombre);
        $('#predios-table tbody').html('');
        $('#rendimiento').prop('disabled', true);
        if (data.cultivo !== 'arroz') {
            $('#cantidad-contratos').val('').limpiaErrores();
        }
        data.predios = [];
        data.tipos = [];
        data.iars = [];
        $('#precios-table tbody').html('');
        $('#iars-table tbody').html('');
        $('#tipo-precio,#agregar-contrato-button').prop('disabled', true);
        $('#especifique-cobertura').val('0').limpiaErrores();
    };

    $.segalmex.cultivos.pre.registro.habilitaTipoPrecio = function () {
        var contratos = $('#total-contratos').val();
        $('#tipo-precio,#agregar-contrato-button,#cantidad-contratos').prop('disabled', true);
        if (contratos === 0 || contratos === undefined) {
            data.tipos = [];
            utils.construyeTablaTipoPrecios();
        }
        if (contratos > 0) {
            $('#tipo-precio,#agregar-contrato-button,#cantidad-contratos').prop('disabled', false);
        }
    };

    $.segalmex.cultivos.pre.registro.agregaIar = function () {
        var tipo = $('#tipo-iar').val();
        var cantidad = parseInt($('#cantidad-iar').val());

        var errores = [];
        $('#cantidad-iar').configura({required: true});
        $('#cantidad-iar').valida(errores, true);
        if (errores.length > 0) {
            return;
        }
        $('#cantidad-iar').configura({required: false});
        if (cantidad < 1) {
            alert('La cantidad debe ser mayor o igual a 1.');
            return;
        }

        if (tipo === '0') {
            alert('Seleccione el tipo de IAR.');
            return;
        }

        if (utils.validaTipoIarRequerido(tipo)) {
            if ($('#especifique-cobertura').val() === '0') {
                alert('Seleccione una correduría.');
                return;
            }
        }

        var iar = {
            cantidad: cantidad
        };
        for (var i = 0; i < data.coberturas.length; i++) {
            if (parseInt($('#especifique-cobertura').val()) === data.coberturas[i].id) {
                iar.entidadCobertura = data.coberturas[i];
            }
        }

        for (var i = 0; i < data.tiposIar.length; i++) {
            if (tipo === data.tiposIar[i].clave) {
                iar.tipoIar = data.tiposIar[i];
            }
        }

        if (data.iars.length > 0) {
            for (var i = 0; i < data.iars.length; i++) {
                if (data.iars[i].entidadCobertura && iar.entidadCobertura
                        && data.iars[i].entidadCobertura.clave === iar.entidadCobertura.clave
                        && data.iars[i].tipoIar.clave === iar.tipoIar.clave) {
                    alert('Ya se agregó una IAR de tipo: ' + iar.tipoIar.nombre + ', con la correduría: ' + iar.entidadCobertura.nombre);
                    return;
                }
                if (data.iars[i].tipoIar.clave === iar.tipoIar.clave && !iar.entidadCobertura) {
                    alert('Ya se agregó una IAR de tipo: ' + iar.tipoIar.nombre + '.');
                    return;
                }
            }
        }
        data.iars.push(iar);
        $('#tipo-iar').val('0').limpiaErrores();
        $('#tipo-iar').change();
        $('#cantidad-iar').val('').limpiaErrores();
        utils.construyeTablaIars();
    };

    utils.construyeTablaIars = function () {
        var buffer = [];
        for (var i = 0; i < data.iars.length; i++) {
            var iar = data.iars[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(iar.tipoIar.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(iar.cantidad);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(iar.entidadCobertura ? iar.entidadCobertura.nombre : '--');
            buffer.push('</td>');
            buffer.push('<td class="text-center"><button id="eliminar-iar-');
            buffer.push(i);
            buffer.push('" class="btn btn-danger btn-sm eliminar-iar"><i class="fa fa-minus-circle"></i></button></td>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        $('#iars-table tbody').html(buffer.join(''));
    };

    $.segalmex.cultivos.pre.registro.eliminaIar = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-iar-'.length), 10);
        var iars = [];
        for (var i = 0; i < data.iars.length; i++) {
            if (i !== idx) {
                iars.push(data.iars[i]);
            }
        }
        data.iars = iars;
        utils.construyeTablaIars();
    };

    $.segalmex.cultivos.pre.registro.agregaTipoPrecio = function () {
        var tipo = parseInt($('#tipo-precio').val());
        var cantidad = parseInt($('#cantidad-contratos').val());
        if (tipo === 0) {
            alert('Seleccione un tipo de precio.');
            return;
        }
        if (cantidad < 1) {
            alert('La cantidad debe ser mayor o igual a 1.');
            return;
        }

        var errores = [];
        $('#cantidad-contratos').configura({required: true});
        $('#cantidad-contratos').valida(errores, true);
        if (errores.length > 0) {
            return;
        }
        $('#cantidad-contratos').configura({required: false});
        var tc = {};
        for (var i = 0; i < data.tiposPrecio.length; i++) {
            if (tipo === data.tiposPrecio[i].id) {
                tc = {
                    cantidad: cantidad,
                    id: data.tiposPrecio[i].id,
                    clave: data.tiposPrecio[i].clave,
                    nombre: data.tiposPrecio[i].nombre
                };
            }
        }
        utils.calculaTiposPrecio(tc);
        $('#tipo-precio').val('0').limpiaErrores();
        $('#cantidad-contratos').val('').limpiaErrores();
        utils.construyeTablaTipoPrecios();
    };

    $.segalmex.cultivos.pre.registro.eliminaTipoPrecio = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-precio-'.length), 10);
        var precios = [];
        for (var i = 0; i < data.tipos.length; i++) {
            if (i !== idx) {
                precios.push(data.tipos[i]);
            }
        }
        data.tipos = precios;
        utils.construyeTablaTipoPrecios();
    };

    utils.construyeTablaTipoPrecios = function () {
        var buffer = [];
        for (var i = 0; i < data.tipos.length; i++) {
            var p = data.tipos[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(p.cantidad);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(p.nombre);
            buffer.push('</td>');
            buffer.push('<td class="text-center"><button id="eliminar-precio-');
            buffer.push(i);
            buffer.push('" class="btn btn-danger btn-sm eliminar-precio"><i class="fa fa-minus-circle"></i></button></td>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        $('#precios-table tbody').html(buffer.join(''));
    };

    utils.cargaCatalogos = function (cultivo) {
        var datos = {
            cultivo: cultivo
        };
        $.ajax({
            type: 'GET',
            url: '/cultivos/resources/paginas/productor/pre-registro',
            data: datos,
            dataType: 'json'
        }).done(function (response) {
            data.municipios = response.municipios;
            data.estados = response.estados;
            data.tiposPrecio = response.tiposPrecio;
            data.coberturas = response.coberturas;
            data.cicloActivo = response.cicloActivo;
            data.tiposCultivo = response.tipos;
            data.tiposIar = response.tiposIar;
            data.tiposIarRequerdidos = response.tiposRequierenIar;
            $('#ciclo-agricola').val(response.cicloActivo.nombre);
            $('#reimpresion-ciclo').actualizaCombo(response.ciclos);
            $('#estado-predio').actualizaCombo(response.estados);
            $('#tipo-cultivo').actualizaCombo(response.tipos);
            $('#tipo-posesion-predio').actualizaCombo(response.tiposPosesion);
            $('#regimen-hidrico-predio').actualizaCombo(response.regimenes);
            if (data.cultivo !== 'arroz') {
                $('#especifique-cobertura').actualizaCombo(response.coberturas);
                $('#tipo-iar').actualizaCombo(response.tiposIar, {value: 'clave'});
                $('#especifique-cobertura').prop('disabled', true);
                $('#tipo-precio').actualizaCombo(response.tiposPrecio);
                $('#tipo-precio').prop('disabled', true);
                $('#agregar-contrato-button').prop('disabled', true);
            }
            $('#parentesco-beneficiario').actualizaCombo(response.parentescos);
            if (response.registroCerrado) {
                alert("El pre registro de productor cerró el " + response.registroCerrado.valor);
            }
        }).fail(function () {
            alert('Error: Al descargar los recursos de la página.');
        });
    };

    utils.cuentaCoberturas = function () {
        var coberturas = $('#especifique-cobertura').val();
        return coberturas.length + ' coberturas seleccionadas';
    };

    utils.generaDatos = function () {
        var preRegistro = {
            nombre: $('#nombre').val(),
            primerApellido: $('#papellido').val(),
            segundoApellido: $('#sapellido').val(),
            curp: $('#curp').val(),
            rfc: $('#rfc').val(),
            telefono: $('#telefono').val(),
            correoElectronico: $('#email').val(),
            cantidadContratos: $('#total-contratos').val() === '-1' ? 0 : $('#total-contratos').val(),
            predios: data.predios,
            precioAbierto: 0,
            precioCerradoDolares: 0,
            precioCerradoPesos: 0,
            precioReferencia: 0,
            precioAbiertoPesos: 0,
            nombreBeneficiario: $('#nombre-beneficiario').val(),
            apellidosBeneficiario: $('#apellidos-beneficiario').val(),
            curpBeneficiario: $('#curp-beneficiario').val(),
            parentesco: {id: $('#parentesco-beneficiario').val()}
        };

        if (data.cultivo !== 'arroz') {
            if ($('#especifique-cobertura').val().length > 0) {
                preRegistro.coberturas = data.iars;
            }
        }
        for (var i = 0; i < data.tipos.length; i++) {
            var tp = data.tipos[i];
            switch (tp.clave) {
                case 'precio-abierto':
                    preRegistro.precioAbierto = tp.cantidad;
                    break;
                case 'precio-abierto-pesos':
                    preRegistro.precioAbiertoPesos = tp.cantidad;
                    break;
                case 'precio-cerrado':
                    preRegistro.precioCerradoDolares = tp.cantidad;
                    break;
                case 'precio-cerrado-pesos':
                    preRegistro.precioCerradoPesos = tp.cantidad;
                    break;
                case 'precio-referencia':
                    preRegistro.precioReferencia = tp.cantidad;
                    break;
            }
        }
        return preRegistro;
    };

    $.segalmex.cultivos.pre.registro.cambiaEstadoPredio = function (e) {
        utils.cambiaEstado(e.target, 'municipio-predio');
    };

    utils.cambiaEstado = function (target, idMunicipio) {
        var v = $(target).val();
        var estado = v !== '0' ? $.segalmex.get(data.estados, v) : {clave: '0'};
        var municipios = [];
        if (estado.clave !== '0') {
            for (var i = 0; i < data.municipios.length; i++) {
                var m = data.municipios[i];
                if (m.estado.id === estado.id) {
                    municipios.push(m);
                }
            }
        }
        $('#' + idMunicipio).actualizaCombo(municipios).val('0');
    };

    $.segalmex.cultivos.pre.registro.agregaPredio = function (e) {
        //validacion de campo
        $('#predios-div .valid-field').limpiaErrores();
        var errores = [];
        $('#predios-div .valid-field').valida(errores, true);
        if (errores.length > 0) {
            $('#' + errores[0].campo).focus();
            return;
        }

        var predio = {
            tipoCultivo: {id: $('#tipo-cultivo').val(), nombre: $("#tipo-cultivo option:selected").text()},
            tipoPosesion: {id: $('#tipo-posesion-predio').val(), nombre: $("#tipo-posesion-predio option:selected").text()},
            regimenHidrico: {id: $('#regimen-hidrico-predio').val(), nombre: $("#regimen-hidrico-predio option:selected").text()},
            superficie: $("#superficie-predio").val(),
            volumen: $("#volumen-predio").val(),
            rendimiento: $("#rendimiento").val(),
            estado: {id: $("#estado-predio").val(), nombre: $("#estado-predio option:selected").text()},
            municipio: {id: $('#municipio-predio').val(), nombre: $("#municipio-predio option:selected").text()}
        };
        data.predios.push(predio);
        utils.limpiaPredio();
        utils.construyePredios();
    };

    utils.construyePredios = function () {
        var buffer = [];
        for (var i = 0; i < data.predios.length; i++) {
            var predio = data.predios[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-right">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.tipoCultivo.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.tipoPosesion.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.regimenHidrico.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.volumen);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.superficie);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.rendimiento);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.municipio.nombre);
            buffer.push(', ');
            buffer.push(predio.estado.nombre);
            buffer.push('</td>');
            buffer.push('<td class="text-center"><button id="eliminar-predio-');
            buffer.push(i);
            buffer.push('" class="btn btn-danger btn-sm eliminar-predio"><i class="fa fa-minus-circle"></i></button></td>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        $('#predios-table tbody').html(buffer.join(''));
    };

    utils.limpiaPredio = function () {
        $('#predios-div input.valid-field').val('');
        $('#predios-div select.valid-field').val('0').change();
        $('#predios-div .valid-field').limpiaErrores();
    };

    $.segalmex.cultivos.pre.registro.eliminaPredio = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-predio-'.length), 10);
        var predios = [];
        for (var i = 0; i < data.predios.length; i++) {
            if (i !== idx) {
                predios.push(data.predios[i]);
            }
        }
        data.predios = predios;
        utils.construyePredios();
    };

    $.segalmex.cultivos.pre.registro.cerrarDescarga = function (e) {
        $('#descarga-modal').modal('hide');
    };

    $.segalmex.cultivos.pre.registro.validaSiCobertura = function () {
        $('#guardar-button').prop('disabled', false);
        if ($('#cobertura').val() === 'false') {
            alert('Es necesario adquirir un Instrumento de Administración de riesgos (IAR).');
            $('#guardar-button').prop('disabled', true);
        }
    };

    $.segalmex.cultivos.pre.registro.validaTipoCobertura = function (e) {
        var v = $(e.target).val();
        $('#especifique-cobertura').prop('disabled', true).limpiaErrores().val('0');
        if (utils.validaTipoIarRequerido(v)) {
            $('#especifique-cobertura').prop('disabled', false);
        }
    };

    $.segalmex.cultivos.pre.registro.eligeContrato = function (e) {
        var v = $(e.target).val();
        $('#tipo-precio').prop('disabled', true).limpiaErrores().val(0).change();
        if (v > 0) {
            $('#tipo-precio').prop('disabled', false);
        }
        $('#tipo-precio').selectpicker('refresh');
    };

    $.segalmex.cultivos.pre.registro.eligeTipoPrecio = function (e) {
        $('#div-cobertura select').val('0').change().limpiaErrores();
        $('#cobertura,#especifique-cobertura').configura({
            required: false
        });
        var tipos = $('#tipo-precio').val();
        if (tipos.length === 0) {
            return;
        }
        data.coberturaRequerida = false;
        $.each(tipos, function () {
            var v = $.segalmex.get(data.tiposPrecio, this);
            switch (v.clave) {
                case 'precio-cerrado':
                case 'precio-cerrado-pesos':
                    break;
                default:
                    $('#cobertura,#especifique-cobertura').configura({
                        required: true
                    });
                    data.coberturaRequerida = true;
            }
        });
    };

    $.segalmex.cultivos.pre.registro.calculaRendimiento = function (e) {
        e.preventDefault();
        var volumen = $('#volumen-predio').val();
        var superficie = $('#superficie-predio').val();
        var rendimiento = volumen / superficie;
        $('#rendimiento').val(!isNaN(rendimiento) && rendimiento !== Infinity ? rendimiento.toFixed(3) : '');
    };


    utils.getNombreCultivo = function () {
        switch (data.cultivo) {
            case 'maiz-comercial':
                return 'Maíz comercial';
            case 'arroz':
                return 'Arroz';
            case 'trigo':
                return 'Trigo';
            default :
                return data.cultivo;
        }
    };

    $.segalmex.cultivos.pre.registro.cerrarDescarga = function () {
        $('#descarga-modal').modal('hide');
    };

    $.segalmex.cultivos.pre.registro.validaCurpRfc = function () {
        var curp = $('#curp').val();
        var rfc = $('#rfc').val();

        if (curp.length >= 10 && rfc.length >= 10
                && curp.substring(0, 10) !== rfc.substring(0, 10)) {
            alert('Aviso: La CURP y el RFC no coinciden en los primeros 10 caracteres, si es correcto, puede continuar el pre registro.');
        }
    };

    $.segalmex.cultivos.pre.registro.filtraEstados = function () {
        if (data.cultivo === 'trigo' && $('#tipo-cultivo').val() !== '0') {
            $('#estado-predio').val('0').change().limpiaErrores();
            var v = parseInt($('#tipo-cultivo').val());
            var tc;
            var estados = [];
            for (var i = 0; i < data.tiposCultivo.length; i++) {
                if (v === data.tiposCultivo[i].id) {
                    tc = data.tiposCultivo[i];
                }
            }
            switch (tc.clave) {
                case 'trigo-cristalino':
                    for (var i = 0; i < data.estados.length; i++) {
                        if (data.estados[i].clave === data.claveBC || data.estados[i].clave === data.claveSonora) {
                            estados.push(data.estados[i]);
                        }
                    }
                    $('#estado-predio').actualizaCombo(estados);
                    break;
                default :
                    $('#estado-predio').actualizaCombo(data.estados);
            }
        }
    };

    utils.getTerminosCondiciones = function () {
        switch (data.cicloActivo.clave) {
            case 'oi-2022':
                switch (data.cultivo) {
                    case 'maiz-comercial':
                        return `<p>Con el presente pre registro acepto que, SEGALMEX no está obligado a brindarme el incentivo,
                                pues es solo un requisito.</br>
                                Ante el <strong>Programa Precios de Garantía O.I. 2021-2022</strong> deberé cumplir con los criterios de elegibilidad,
                                los requisitos y los pasos de la mecánica operativa, incluyendo la adquisición de un instrumento de administración de riesgo,
                                como una cobertura, de forma obligatoria.</br>
                                Finalmente, la información proporcionada la he verificado y confirmado, ya que en caso de que exista algún error en la CURP,
                                RFC, nombre, tipo de cultivo, superficie o volumen no podrá ser modificada en el registro.</br>
                                Los datos personales recabados serán protegidos, incorporados y tratados en el marco de la Ley General de Protección
                                de Documentos Personales en Posesión de Sujetos Obligados, con fundamento en los artículos 3,27 y 28.</br>
                                <small>Este programa es público, ajeno a cualquier partido político. Queda prohibido el uso para fines distintos a
                                los establecidos en el programa.</small></p>`;
                    case 'arroz':
                        return `<p>Con el presente pre registro acepto que, SEGALMEX no está obligado a brindarme el incentivo,
                                pues es solo un requisito.</br>
                                Ante el <strong>Programa Precios de Garantía O.I. 2021-2022</strong> deberé cumplir con los criterios de elegibilidad,
                                los requisitos y los pasos de la mecánica operativa.</br>
                                Finalmente, la información proporcionada la he verificado y confirmado, ya que en caso de que exista algún error
                                en la CURP, RFC, nombre, tipo de cultivo, superficie o volumen no podrá ser modificada en el registro.</br>
                                Los datos personales recabados serán protegidos, incorporados y tratados en el marco de la Ley General de
                                Protección de Documentos Personales en Posesión de Sujetos Obligados, con fundamento en los artículos 3,27 y 28.</br>
                                <small>Este programa es público, ajeno a cualquier partido político. Queda prohibido el uso para fines distintos a
                                los establecidos en el programa.</small></p>`;
                    case 'trigo':
                        return `<p>Con el presente pre registro acepto que, SEGALMEX no está obligado a brindarme el incentivo, 
                                pues es solo un requisito.</br>
                                Ante el <strong>Programa Precios de Garantía O.I. 2021-2022</strong> deberé cumplir con los criterios de elegibilidad,
                                los requisitos y los pasos de la mecánica operativa, incluyendo la adquisición de un instrumento de administración de riesgo,
                                como una cobertura, de forma obligatoria.</br>
                                Finalmente, la información proporcionada la he verificado y confirmado, ya que en caso de que exista algún error en la CURP,
                                RFC, nombre, tipo de cultivo, superficie o volumen no podrá ser modificada en el registro.</br>
                                Los datos personales recabados serán protegidos, incorporados y tratados en el marco de la Ley General de Protección
                                de Documentos Personales en Posesión de Sujetos Obligados, con fundamento en los artículos 3,27 y 28.</br>
                                <small>Este programa es público, ajeno a cualquier partido político. Queda prohibido el uso para fines distintos a
                                los establecidos en el programa.</small></p>`;
                }
                break;
            default :
            switch (data.cultivo) {
                case "arroz":
                    return `<p>Con el presente pre registro aceptó que, SEGALMEX no está obligado a brindarme el incentivo,
                            pues es solo un proceso. Para beneficiarse con el <strong>Programa Precios de Garantía ${data.cicloActivo.nombre}</strong>,
                            deberé cumplir con los criterios de elegibilidad, los requisitos y los pasos de la mecánica operativa.</p>
                            <p>Los datos personales recabados serán protegidos, incorporados y tratados en el marco de la
                            Ley General de Protección de Documentos Personales en Posesión de Sujetos Obligados,
                            con fundamento en los artículos 3, 27 y 28.</p>`;
                    break;
                default :
                    return `<p>Con el presente pre registro aceptó que, SEGALMEX no está obligado a brindarme el incentivo,
                            pues es solo un proceso. Para beneficiarse con el <strong>Programa Precios de Garantía ${data.cicloActivo.nombre}</strong>,
                            deberé cumplir con los criterios de elegibilidad, los requisitos y los pasos de la mecánica operativa,
                            incluyendo la adquisición de un instrumento de administración de riesgo, como una cobertura, de forma obligatoria.</p>
                            <p>Los datos personales recabados serán protegidos, incorporados y tratados en el marco de la
                            Ley General de Protección de Documentos Personales en Posesión de Sujetos Obligados,
                            con fundamento en los artículos 3, 27 y 28.</p>`;
            }
        }
    };

    utils.calculaTiposPrecio = function (tc) {
        if (data.tipos.length > 0) {
            var existe = false;
            for (var i = 0; i < data.tipos.length; i++) {
                if (data.tipos[i].clave === tc.clave) {
                    data.tipos[i].cantidad += tc.cantidad;
                    existe = true;
                    return;
                }
            }
            if (!existe) {
                data.tipos.push(tc);
                return;
            }
        } else {
            data.tipos.push(tc);
        }
    };

    utils.validaTipoIarRequerido = function (tipo) {
        for (var i = 0; i < data.tiposIarRequerdidos.length; i++) {
            if (data.tiposIarRequerdidos[i].valor === tipo) {
                return true;
            }
        }
        return false;
    };
})(jQuery);