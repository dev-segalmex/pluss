/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.cultivos.consulta.estatus');

    var data = {
        excepciones: [],
        tipoBusqueda: 'Maíz-comercial',
        productores: [],
        productoresFiltrados: [],
        facturas: [],
        facturasFiltradas: []
    };
    var utils = {};
    var handlers = {};

    $.segalmex.cultivos.consulta.estatus.init = () => {
        utils.cargaCatalogos();
        $('#ciclo').change(utils.filtraRegistros);
        $('#menu-bandejas a.nav-link').click(handlers.cambiaBandeja);
    };

    handlers.cambiaBandeja = (e) => {
        e.preventDefault();
        $('#div-' + data.tipoBusqueda).hide();
        var id = e.target.id.substring('muestra-'.length);
        if (data.tipoBusqueda === id) {
            $('#div-' + data.tipoBusqueda).show();
            return;
        }
        data.tipoBusqueda = id;
        $('#div-' + data.tipoBusqueda).show();
        $('#menu-bandejas a.nav-link').removeClass('active');
        $(e.target).addClass('active');
        utils.filtraRegistros();
    };

    utils.cargaCatalogos = () => {
        $.ajax({
            url: '/cultivos/resources/paginas/consultas',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            data.ciclos = response.ciclos;
            data.cultivos = response.cultivos;
            data.estatus = response.estatus;
            $('#ciclo').actualizaCombo(response.ciclos);
            utils.cargaInscripcionesProductor();
        }).fail(function () {
            alert('Error: No se pudo obtener los catálogos de la página.');
        });

    };

    utils.cargaInscripcionesProductor = () => {
        $.ajax({
            url: '/cultivos/resources/productores/inscripcion/resumen',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            data.productores = response;
            data.productoresFiltrados = utils.getRegistrosFiltrados(data.productores);
            utils.cargaInscripcionesFacturas();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert("Ocurrió un error al cargar los registros de productores.");
            }
        });
    };

    utils.cargaInscripcionesFacturas = () => {
        $.ajax({
            url: '/cultivos/resources/facturas/inscripcion/resumen',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            data.facturas = response;
            data.facturasFiltradas = utils.getRegistrosFiltrados(data.facturas);
            utils.creaTablaResultados();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert("Ocurrió un error al cargar los registros de facturas.");
            }
        });
    };

    utils.creaTablaResultados = () => {
        $('#div-table-resultados').html(utils.construyeTablaResultados());
    };

    utils.construyeTablaResultados = () => {
        var totalProductor = 0;
        var totalFacturaProductor = 0;
        var totalFacturaGlobal = 0;
        var buffer = '';
        buffer = `
            <table id="table-resultados" class="table table-striped table-bordered table-hover" width="100%">
            <thead class="thead-dark">
            <tr class="success">
            <th rowspan="2" class="align-top text-center">Estatus</th>
            <th rowspan="2" class="align-top text-center">Productores</th>
            <th colspan="2" class="align-top text-center">Facturas</th>
            </tr>
            <tr>
            <th class="align-top text-center">Productor</th>
            <th class="align-top text-center">Global</th>
            </tr>
            </thead>
            <tbody>`;

        for (var i = 0; i < data.estatus.length; i++) {
            var e = data.estatus[i];
            totalProductor += utils.getCantidad(e.nombre, 'productor');
            totalFacturaProductor += utils.getCantidad(e.nombre, 'factura', 'productor');
            totalFacturaGlobal += utils.getCantidad(e.nombre, 'factura', 'global');
            buffer += `
                <tr>
                <td>${e.nombre}</td>
                <td class="text-right">${utils.getCantidad(e.nombre, 'productor')}</td>
                <td class="text-right">${utils.getCantidad(e.nombre, 'factura', 'productor')}</td>
                <td class="text-right">${utils.getCantidad(e.nombre, 'factura', 'global')}</td>
                </tr>`;
        }
        ;

        buffer += `</tbody>`;
        buffer += `<tfoot class="thead-dark">
                   <tr>
                   <th>Total</th>
                   <th class="text-right">${totalProductor}</th>
                   <th class="text-right">${totalFacturaProductor}</th>
                   <th class="text-right">${totalFacturaGlobal}</th>
                   </tr>
                   </tfoot>`;
        buffer += `</table><br/>`;
        return buffer;
    };

    utils.getCantidad = (estatus, tipo, tipoFactura) => {
        var contador = 0;
        switch (tipo) {
            case 'productor':
                for (var i = 0; i < data.productoresFiltrados.length; i++) {
                    if (data.productoresFiltrados[i].estatus === estatus) {
                        contador += data.productoresFiltrados[i].total;
                    }
                }
                return contador;
            case 'factura':
                for (var i = 0; i < data.facturasFiltradas.length; i++) {
                    if (data.facturasFiltradas[i].estatus === estatus && data.facturasFiltradas[i].tipo === tipoFactura) {
                        contador += data.facturasFiltradas[i].total;
                    }
                }
                return contador;
            default :
                return contador;
        }
    };

    utils.filtraRegistros = () => {
        data.productoresFiltrados = utils.getRegistrosFiltrados(data.productores);
        data.facturasFiltradas = utils.getRegistrosFiltrados(data.facturas);
        $('#div-table-resultados').html(utils.construyeTablaResultados());
    };

    utils.getRegistrosFiltrados = (registros) => {
        var filtrados = [];
        data.tipoBusqueda = data.tipoBusqueda === 'Maíz-comercial' ? 'Maíz comercial' : data.tipoBusqueda;
        var cultivo = $.segalmex.get(data.cultivos, data.tipoBusqueda, 'nombre');
        var ciclo = $.segalmex.get(data.ciclos, $('#ciclo').val());
        for (var i = 0; i < registros.length; i++) {
            var ef = registros[i];
            if (ef.cultivo === cultivo.nombre) {
                if (ciclo) {
                    if (ef.ciclo === ciclo.nombre) {
                        filtrados.push(ef);
                    }
                } else {
                    filtrados.push(ef);
                }
            }
        }
        return filtrados;
    };
})(jQuery);