(function ($) {
    $.segalmex.namespace('segalmex.maiz.inscripcion.factura.vista');

    var utils = {};

    $.segalmex.maiz.inscripcion.factura.vista.muestraInscripcion = function (inscripcion, idDetalle, ispublico) {
        $('#' + idDetalle).html($.segalmex.maiz.inscripcion.factura.vista.construyeInscripcion(inscripcion, ispublico));
    };

    $.segalmex.maiz.inscripcion.factura.vista.construyeInscripcion = function (inscripcion, ispublico) {
        var buffer = [];
        var cfdi = inscripcion.cfdi;
        buffer.push('<div class="card mb-4 shadow-sm">');
        buffer.push('<h4 class="card-header">Detalle del Registro de Factura</h4>');

        buffer.push('<div class="card-body">'); // card-body
        buffer.push('<h3 class="card-title">Folio: ');
        buffer.push(inscripcion.folio);
        buffer.push('</h3>');
        buffer.push('<h4>Programa de Precios de Garantía a Productos Alimentarios Básicos</h4>');

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Cultivo:</strong><br/>');
        buffer.push(inscripcion.cultivo.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Ciclo agrícola:</strong><br/>');
        buffer.push(inscripcion.ciclo.nombre);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<h4>Datos de la empresa</h4>');
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-8">');
        buffer.push('<strong>Nombre:</strong><br/> ');
        buffer.push(inscripcion.sucursal.empresa.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>RFC:</strong><br/> ');
        buffer.push(inscripcion.sucursal.empresa.rfc);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Entidad:</strong><br/> ');
        buffer.push(inscripcion.sucursal.estado.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Municipio:</strong><br/> ');
        buffer.push(inscripcion.sucursal.municipio.nombre);
        buffer.push('</div>');
        if (!ispublico) {
            buffer.push('<div class="col-md-4">');
            buffer.push('<strong>Localidad:</strong><br/> ');
            buffer.push(inscripcion.sucursal.localidad);
            buffer.push('</div>');
        }
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<h4>Datos del CFDI</h4>');

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Fecha de emisión:</strong><br/>');
        buffer.push($.segalmex.date.isoToFecha(cfdi.fecha));
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Moneda:</strong><br/>');
        buffer.push(cfdi.moneda);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Subtotal (conceptos válidos):</strong><br/>');
        buffer.push(utils.format(cfdi.subtotal, 3));
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Total:</strong><br/>');
        buffer.push(utils.format(cfdi.total, 3));
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Fecha timbrado:</strong><br/>');
        buffer.push($.segalmex.date.isoToFecha(cfdi.fechaTimbrado));
        buffer.push('</div>');
        buffer.push('<div class="col-md-8">');
        buffer.push('<strong>UUID timbre fiscal digital:</strong><br/>');
        buffer.push(cfdi.uuidTimbreFiscalDigital);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<h5>Emisor</h5>');
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>RFC:</strong><br/>');
        buffer.push(cfdi.rfcEmisor);
        buffer.push('</div>');
        buffer.push('<div class="col-md-8">');
        buffer.push('<strong>Nombre:</strong><br/>');
        buffer.push(cfdi.nombreEmisor);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<h5>Receptor</h5>');
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>RFC:</strong><br/>');
        buffer.push(cfdi.rfcReceptor);
        buffer.push('</div>');
        buffer.push('<div class="col-md-8">');
        buffer.push('<strong>Nombre:</strong><br/>');
        buffer.push(cfdi.nombreReceptor);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<h5>Información adicional</h5>');
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Tipo:</strong><br/>');
        buffer.push(inscripcion.tipoCultivo ? inscripcion.tipoCultivo.nombre : '--');
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Estado:</strong><br/>');
        buffer.push(inscripcion.estado ? inscripcion.estado.nombre : '--');
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Toneladas totales:</strong><br/>');
        buffer.push(inscripcion.cantidad ? inscripcion.cantidad : '--');
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Toneladas con estímulo:</strong><br/>');
        buffer.push(inscripcion.facturables !== undefined ? inscripcion.facturables : '--');
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<h5>Precio por tonelada</h5>');
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Precio por tonelada:</strong><br/>');
        buffer.push(inscripcion.precioTonelada ? utils.format(inscripcion.precioTonelada, 3) : '--');
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Ajuste:</strong><br/>');
        buffer.push(inscripcion.precioAjuste ? utils.format(inscripcion.precioAjuste, 3) : '--');
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Precio por tonelada real:</strong><br/>');
        buffer.push(inscripcion.precioToneladaReal ? utils.format(inscripcion.precioToneladaReal, 3) : '--');
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        if (inscripcion.cultivo.clave === 'arroz' && parseFloat(inscripcion.cantidadComprobantePago) > 0.0) {
            buffer.push('<h5>Comprobante de pago del productor</h5>');
            buffer.push('<div class="row">'); // row
            buffer.push('<div class="col-md-4">');
            buffer.push('<strong>Importe del comprobante de pago:</strong><br/>');
            buffer.push('$ ' + utils.format(inscripcion.cantidadComprobantePago, 3));
            buffer.push('</div>');
            buffer.push('</div>'); // end-row
            buffer.push('<br/>');
        }

        buffer.push('<h5>Conceptos</h5>');
        buffer.push('<div class="table-responsive">');
        buffer.push('<table id = "conceptos-table" class="table table-striped table-bordered table-hover">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<tr>');
        buffer.push('<th>#</th>');
        buffer.push('<th>Cantidad</th>');
        buffer.push('<th>Unidad</th>');
        buffer.push('<th>Descripción</th>');
        buffer.push('<th>Valor unitario</th>');
        buffer.push('<th>Importe</th>');
        buffer.push('<th>Clave de servicio</th>');
        buffer.push('<th>Estatus</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');
        for (var i = 0; i < cfdi.conceptos.length; i++) {
            var c = cfdi.conceptos[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-center">');
            buffer.push(i + 1)
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(utils.format(c.cantidad, 3));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(c.unidad ? c.unidad : c.claveUnidad);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(c.descripcion);
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(utils.format(c.valorUnitario, 3));
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(utils.format(c.importe, 3));
            buffer.push('</td>');
            buffer.push('<td class="text-center">');
            buffer.push(c.claveProductoServicio);
            buffer.push('</td>');
            buffer.push('<td class="text-center">');
            buffer.push(c.estatus === 'positivo'
                    ? '<i class="fas fa-check-circle text-success"></i>'
                    : '<i class="fas fa-exclamation-circle text-warning"></i>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        buffer.push('</tbody>');
        buffer.push('</table>');
        buffer.push('</div>');
        if (inscripcion.comprobantes && inscripcion.comprobantes.length > 0) {
            buffer.push('<h5>Comprobantes de recepción</h5>');
            buffer.push('<div class="table-responsive">');
            buffer.push('<table id = "comprobantes-table" class="table table-striped table-bordered table-hover">');
            buffer.push('<thead class="thead-dark">');
            buffer.push('<tr>');
            buffer.push('<th>#</th>');
            buffer.push('<th>Descripción</th>');
            buffer.push('<th>Volumen (t)</th>');
            buffer.push(inscripcion.cultivo.clave === 'arroz' ? '<th>Fecha</th>' : '');
            buffer.push('</tr>');
            buffer.push('</thead>');
            buffer.push('<tbody>');
            var volTot = 0;
            for (var i = 0; i < inscripcion.comprobantes.length; i++) {
                var com = inscripcion.comprobantes[i];
                buffer.push('<tr>');
                buffer.push('<td class="text-center">');
                buffer.push(i + 1);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(com.descripcion);
                buffer.push('</td>');
                buffer.push('<td class="text-right">');
                buffer.push(utils.format(parseFloat(com.volumen), 3));
                buffer.push('</td>');
                if (inscripcion.cultivo.clave === 'arroz') {
                    buffer.push('<td>');
                    buffer.push(com.fechaComprobante ? $.segalmex.date.isoToFecha(com.fechaComprobante) : '--');
                    buffer.push('</td>');
                }
                buffer.push('</tr>');
                volTot += parseFloat(com.volumen);
            }
            buffer.push('</tbody>');
            buffer.push('<tfoot class="thead-dark">');
            buffer.push('<tr>');
            buffer.push('<th colspan="2">Total</th>');
            buffer.push('<th class="text-right">' + utils.format(parseFloat(volTot), 3) + '</th>');
            buffer.push(inscripcion.cultivo.clave === 'arroz' ? '<th colspan="1"></th>' : '');
            buffer.push('</tr>');
            buffer.push('</tfoot>');
            buffer.push('</table>');
            buffer.push('</div>');

        }

        switch (inscripcion.tipoFactura) {
            case 'productor':
                buffer.push('<h4>Contrato</h4>');
                buffer.push('<div class="row">'); // row
                buffer.push('<div class="col-md-4">');
                buffer.push('<strong>Número:</strong><br/>');
                if (inscripcion.contrato) {
                    buffer.push(inscripcion.contrato);
                } else {
                    buffer.push('Sin número de contrato');
                }
                buffer.push('</div>');
                buffer.push('<div class="col-md-4">');
                buffer.push('<strong>Estatus:</strong><br/>');
                if (inscripcion.estatusInscripcionContrato) {
                    buffer.push(inscripcion.estatusInscripcionContrato.clave === 'positiva'
                            ? ' <strong class="text-success"><i class="fas fa-check-circle"></i> ' + inscripcion.estatusInscripcionContrato.nombre + '</strong>'
                            : ' <strong class="text-danger"><i class="fas fa-times-circle"></i> ' + inscripcion.estatusInscripcionContrato.nombre + '</strong>');
                } else {
                    buffer.push('--');
                }
                buffer.push('</div>');
                buffer.push('</div>'); // end-row
                buffer.push('<br/>');
                break;
            case 'global':
                buffer.push(utils.muestraFacturasRelacionadas(inscripcion));
                break;
        }

        buffer.push('<h4>Fecha de pago</h4>');
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push(inscripcion.fechaPago ? $.segalmex.date.isoToFecha(inscripcion.fechaPago) : '--');
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<div id="div-archivos">');
        buffer.push('<h4>Archivos </h4>');
        buffer.push('<ul>');
        buffer.push('<li><a href="/cultivos/resources/archivos/' + cfdi.archivo.sistema + '/' + cfdi.archivo.uuid + '/'
                + cfdi.archivo.nombre + '" target="_blank">');
        buffer.push(cfdi.archivo.nombreOriginal);
        buffer.push(' <i class="fas fa-file"></i></a></li>');
        //factura pdf
        if (inscripcion.archivos) {
            for (var a = 0; a < inscripcion.archivos.length; a++) {
                var archivo = inscripcion.archivos[a];
                buffer.push('<li><a href="/cultivos/resources/archivos/' + archivo.sistema + '/' + archivo.uuid + '/'
                        + archivo.nombre + '" target="_blank">');
                buffer.push(archivo.nombreOriginal);
                buffer.push(' <i class="fas fa-file"></i></a></li>');
            }
        }
        buffer.push('</ul>');
        buffer.push('</div>');

        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card
        return buffer.join('\n');
    }

    $.segalmex.maiz.inscripcion.factura.vista.muestraVerificaciones = function (inscripcion) {
        var buffer = [];
        buffer.push('<div class="card mb-4 shadow-sm">');
        buffer.push('<h4 class="card-header">Verificaciones</h4>');
        buffer.push('<div class="card-body">'); // card-body

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>SAT:</strong><br/>');
        buffer.push(inscripcion.valida === undefined ? 'Pendiente' : (inscripcion.valida
                ? '<strong class="text-success"><i class="fas fa-check-circle"></i> Válida</strong>'
                : '<strong class="text-danger"><i class="fas fa-times-circle"></i> No válida</strong>'));
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>No duplicada:</strong><br/>');
        buffer.push(inscripcion.duplicada === undefined ? 'Pendiente' : (!inscripcion.duplicada
                ? '<strong class="text-success"><i class="fas fa-check-circle"></i> Válida</strong>'
                : '<strong class="text-danger"><i class="fas fa-times-circle"></i> No válida</strong>'));
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card
        return buffer.join('\n');
    }

    $.segalmex.maiz.inscripcion.factura.vista.muestraProductorCiclo = function (inscripcion) {
        if (!inscripcion.productorCiclo) {
            return '';
        }

        var buffer = [];
        var pc = inscripcion.productorCiclo;
        buffer.push('<div class="card mb-4 shadow-sm">');
        buffer.push('<h4 class="card-header">Productor</h4>');
        buffer.push('<div class="card-body">'); // card-body
        var productor = pc.productor;
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>RFC:</strong><br/>');
        buffer.push(productor.rfc);
        buffer.push('</div>');
        buffer.push('<div class="col-md-8">');
        buffer.push('<strong>Nombre:</strong><br/>');
        buffer.push(productor.nombre);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<h4>Hectáreas</h4>');
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Total:</strong><br/>');
        buffer.push(pc.hectareasTotales.toFixed(5));
        buffer.push(' ha</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<h4>Toneladas</h4>');
        buffer.push('<div class="table-responsive">');
        buffer.push('<table  class="table table-striped table-bordered table-hover">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<tr>');
        buffer.push('<th>Grupo</th>');
        buffer.push('<th>Toneladas</th>');
        buffer.push('<th>Cantidad</th>');
        buffer.push('<th>Autorizadas</th>');
        buffer.push('<th>Comprobadas</th>');
        buffer.push('<th>Restantes</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');


        var pcg = inscripcion.productorCiclo.grupos;
        for (var i = 0; i < pcg.length; i++) {
            var p = pcg[i];
            var autorizadas = utils.getAutorizadas(p);
            buffer.push('<tr>');
            buffer.push('<td rowspan = 4 class="text-center"><br/><br/><br/>');
            buffer.push(utils.getNombreGrupo(p.grupoEstimulo));
            buffer.push('</td>');
            buffer.push('<td>Producidas</td>');
            buffer.push('<td class="text-right">');
            buffer.push(p.toneladasTotales.toFixed(3));
            buffer.push('</td>');
            buffer.push('<td rowspan = 4 class="text-right"><br/><br/><br/>');//aurotizadas
            buffer.push(autorizadas.toFixed(3));
            buffer.push('</td>');
            buffer.push('<td rowspan = 4 class="text-right"><br/><br/><br/>');//comprobadas
            buffer.push(p.toneladasFacturadas.toFixed(3));
            buffer.push('</td>');
            buffer.push('<td rowspan = 4 class="text-right"><br/><br/><br/>');
            buffer.push((autorizadas - p.toneladasFacturadas).toFixed(3));//restantes
            buffer.push('</td>');
            buffer.push('</tr>');
            buffer.push('<td>Contratadas</td>');
            buffer.push('<td class="text-right">');
            buffer.push(inscripcion.cultivo.clave !== 'arroz' ? p.toneladasContratadas.toFixed(3) : '--');
            buffer.push('</td>');
            buffer.push('<tr>');
            buffer.push('<td>Cobertura</td>');
            buffer.push('<td class="text-right">');
            buffer.push(inscripcion.cultivo.clave !== 'arroz' ? p.toneladasCobertura.toFixed(3) : '--');
            buffer.push('</td>');
            buffer.push('</tr>');
            buffer.push('<tr>');
            buffer.push('<td>Límite</td>');
            buffer.push('<td class="text-right">');
            buffer.push(p.limite.toFixed(3));
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        buffer.push('</tbody>');
        buffer.push('</table>');
        buffer.push('</div>');


        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card
        return buffer.join('\n');
    }

    utils.getAutorizadas = function (grupo) {
        var numeros = [];
        numeros.push(utils.validaNumero(grupo.toneladasTotales));
        numeros.push(utils.validaNumero(grupo.toneladasContratadas));
        numeros.push(utils.validaNumero(grupo.toneladasCobertura));
        numeros.push(utils.validaNumero(grupo.limite));
        numeros.sort(function (a, b) {
            return a - b
        });
        return numeros[0];

    };

    utils.validaNumero = function (numero) {
        if (numero !== undefined && numero > 0) {
            return numero;
        }
    };

    utils.getNombreGrupo = function (grupo) {
        switch (grupo) {
            case 'trigo':
                return 'Trigo';
                break;
            case 'trigo-cristalino':
                return 'Trigo cristalino';
                break;
            case 'arroz':
                return 'Arroz';
                break;
            case 'maiz':
                return 'Maíz';
            default:
                return grupo;
                break;
        }
    };

    $.segalmex.maiz.inscripcion.factura.vista.muestraCantidadFacturable = function (inscripcion) {
        if (!inscripcion.productorCiclo) {
            return '';
        }

        var buffer = [];
        var pc = inscripcion.productorCiclo;
        var disponibles = inscripcion.toneladasDisponiblesFactura !== undefined ? inscripcion.toneladasDisponiblesFactura : inscripcion.cfdi.totalToneladas;
        var facturables = Math.min(pc.toneladasTotales - pc.toneladasFacturadas, inscripcion.cantidad, disponibles);
        buffer.push('<div class="card mb-4 shadow-sm">');
        buffer.push('<h4 class="card-header">Toneladas</h4>');
        buffer.push('<div class="card-body">'); // card-body
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<h2 class="text-body">Solicitadas:<br/>' + inscripcion.cantidad + ' t</h2>')
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<h2 class="text-body">Disponibles:<br/>' + disponibles + ' t</h2>')
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<h2 class="' + (facturables > 0 ? 'text-success">' : 'text-danger">') + 'Facturables:<br/> ');
        buffer.push(facturables);
        buffer.push(' t</h2>');
        buffer.push('</div>');
        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card
        return buffer.join('\n');
    }

    utils.muestraFacturasRelacionadas = function (inscripcion) {
        if (!inscripcion.relacionadas) {
            return;
        }

        var buffer = [];
        buffer.push('<h4>Facturas de productor</h4>');
        buffer.push('<div class="table-responsive">');
        buffer.push('<table id = "conceptos-table" class="table table-striped table-bordered table-hover">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<tr>');
        buffer.push('<th>#</th>');
        buffer.push('<th>RFC Emisor</th>');
        buffer.push('<th>Nombre emisor</th>');
        buffer.push('<th>UUID</th>');
        buffer.push('<th>Fecha de emisión</th>');
        buffer.push('<th>Toneladas</th>');
        buffer.push('<th>Total</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');
        for (var i = 0; i < inscripcion.relacionadas.length; i++) {
            var factura = inscripcion.relacionadas[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(factura.rfcEmisor);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(factura.nombreEmisor);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(factura.uuidTimbreFiscalDigital);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push($.segalmex.date.isoToFechaHora(factura.fecha));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(factura.toneladasTotales);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(factura.total);
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        buffer.push('</tbody>');
        buffer.push('</table>');
        buffer.push('</div>');

        return buffer.join('\n');
    }

    $.segalmex.maiz.inscripcion.factura.vista.muestraPago = function (inscripcion) {
        if (!inscripcion.usos) {
            return '';
        }
        var buffer = [];
        buffer.push('<div class="card mb-4 shadow-sm">');
        buffer.push('<h4 class="card-header">Pago</h4>');
        buffer.push('<div class="card-body">'); // card-body

        buffer.push('<div class="table-responsive">');
        buffer.push('<table id = "conceptos-table" class="table table-striped table-bordered table-hover">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<tr>');
        buffer.push('<th>#</th>');
        buffer.push('<th>Fecha</th>');
        buffer.push('<th>Estímulo x tonelada</th>');
        buffer.push('<th>Toneladas</th>');
        buffer.push('<th> Estímulo Total</th>');
        buffer.push('<th>Estatus</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');
        for (var i = 0; i < inscripcion.usos.length; i++) {
            var u = inscripcion.usos[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push($.segalmex.date.isoToFechaHora(u.fechaCreacion));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(u.estimuloTonelada);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(u.toneladas);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(u.estimuloTotal);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(u.estatus.nombre);
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        buffer.push('</tbody>');
        buffer.push('</table>');
        buffer.push('</div>');
        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card
        return buffer.join('\n');
    };

    utils.format = function (str, fix) {
        return str.toFixed(fix).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    };


})(jQuery);
