/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.cultivos.reasignacion');
    var data = {};
    var handlers = {};
    var utils = {};

    $.segalmex.cultivos.reasignacion.init = function (params) {
        utils.cargaCatalogos();
        utils.inicializaValidaciones();
        $('#button-buscar').click(handlers.busca);
        $('#button-limpiar').click(handlers.limpia);
        $('#button-reasignar').click(handlers.reasignaRegistros);
        $('#button-regresar-resultados').click(handlers.regresaResultados);
        $('#si-reasignar-button').click(handlers.confirmaReasignacionMultiple);
        $('#no-reasignar-button').click(handlers.cancelaReasignacionMultiple);
    };

    handlers.busca = function (e) {
        e.preventDefault();
        $('#button-buscar').html('Buscando...').prop('disabled', true);
        var errores = [];
        $('#datos-busqueda-form .valid-field').valida(errores, true);
        if (errores.length > 0) {
            $('#button-buscar').html('Buscar').prop('disabled', false);
            return;
        }
        var datos = utils.getFiltros();

        $('#cargando-resultados').show();
        $.ajaxq('busquedasQueue', {
            url: '/cultivos/resources/productores/maiz/inscripcion/',
            type: 'GET',
            data: datos,
            dataType: 'json'
        }).done(function (response) {
            $('#cargando-resultados').hide();
            if (response.length === 0) {
                $('#data').html('<tr><td colspan="8" class="text-center">Sin resultados</td></tr>');
                $('#div-botones').hide();
            } else {
                $('#data').html(utils.creaDatatable(response));
                utils.configuraTablaResultados();
                $('#table-resultados-busqueda').on('click', 'a.link-uuid', handlers.muestraDetalle);
                $('#data table #todas-checkbox').click(handlers.seleccionaTodos);
                $('#div-botones').show();
            }
            $('#resultados-busqueda').slideDown();
            $('#button-buscar').html('Buscar').prop('disabled', false);
        }).fail(function () {
            alert('Error: No fue posible realizar la consulta.');
            $('#cargando-resultados').hide();
            $('#button-buscar').html('Buscar').prop('disabled', false);
        });
    };

    handlers.seleccionaTodos = function (e) {
        var checked = $(e.target).is(':checked');
        $('#data table tbody input:checkbox').prop('checked', checked);
    };

    handlers.reasignaRegistros = function (e) {
        e.preventDefault();
        $('#button-reasignar').prop('disabled', true);
        var seleccionados = $('#data table tbody input:checked');
        if (seleccionados.length === 0) {
            alert('Error: Es necesario seleccionar al menos un registro para reasignarlo.');
            $('#button-reasignar').prop('disabled', false);
            return;
        }
        data.uuidProductores = [];
        seleccionados.each(function () {
            var uuid = this.id.substring(0, this.id.length - '-productor-check'.length);
            data.uuidProductores.push(uuid);
        });
        $('#usuario-validador').val('0').limpiaErrores();
        $('#reasignacion-modal').modal('show');
    };

    handlers.cancelaReasignacionMultiple = function (e) {
        e.preventDefault();
        $('#usuario-validador').val('0').limpiaErrores();
        $('#reasignacion-modal').modal('hide');
        $('#button-reasignar').prop('disabled', false);
    };

    handlers.confirmaReasignacionMultiple = function (e) {
        e.preventDefault();
        $('#reasignacion-modal button').prop('disabled', true);
        var errores = [];
        $('#reasignacion-modal .valid-field').valida(errores, true);
        if (errores.length > 0) {
            $('#reasignacion-modal button').prop('disabled', false);
            return;
        }
        //Recorremos la lista de uuid's de los productores seleccionados.
        for (var i = 0; i < data.uuidProductores.length; i++) {
            $('#si-reasignar-button').html('Reasignando...');
            var uuid = data.uuidProductores[i];
            $.ajaxq('asignacionesQueue', {
                url: '/cultivos/resources/productores/maiz/inscripcion/' + uuid + '/validacion/reasignacion/',
                data: JSON.stringify({id: $('#usuario-validador').val()}),
                type: 'POST',
                dataType: 'json',
                contentType: 'application/json'
            }).done(function (response) {

            }).fail(function (jqXHR) {
                if (jqXHR.responseJSON) {
                    alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
                } else {
                    alert('Error: No fue posible reasignar el registro.');
                }
            });
        }
        $('#si-reasignar-button').html('Reasignar');
        alert('El validador ha sido reasignado correctamente.');
        $('#reasignacion-modal button').prop('disabled', false);
        $('#reasignacion-modal').modal('hide');
        $('#button-reasignar').prop('disabled', false);
        utils.limpiar();
    };

    utils.getFiltros = function () {
        var datos = {
            cultivo: $('#cultivo').val(),
            ciclo: $('#ciclo').val(),
            tipoRegistro: $('#tipo-registro').val(),
            estatus: $('#estatus').val(),
            validador: $('#validador').val(),
            seguimiento: true
        };
        if (datos.tipoRegistro === '0') {
            delete datos.tipoRegistro;
        }

        if (datos.estatus === '0') {
            delete datos.estatus;
        }
        if (datos.validador === '0') {
            delete datos.validador;
        }
        return datos;
    };

    handlers.muestraDetalle = function (e) {
        e.preventDefault();
        var uuid = e.target.id.split('.')[1];
        $.ajax({
            url: '/cultivos/resources/productores/maiz/inscripcion/' + uuid,
            data: {archivos: true, datos: true, historial: true, contratos: true, comentarios: true, expand: 3},
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            // Mostramos el detalle
            $.segalmex.maiz.inscripcion.productor.vista.muestraInscripcion(response, 'detalle-inscripcion-productor');
            $('#estatus-inscripcion-productor').html($.segalmex.maiz.inscripcion.vista.construyeEstatus(response));
            $('#historial-inscripcion-productor').html($.segalmex.maiz.inscripcion.vista.construyeHistorial(response.historial));
            $('#comentarios-inscripcion-productor').html($.segalmex.maiz.inscripcion.vista.construyeComentarios(response.comentarios, $.segalmex.common.pagina.comun.registrado));
            $('#detalle-elemento').show();
            $('#datos-busqueda-form,#resultados-busqueda,#resultados-busqueda-agrupados').hide();
        }).fail(function () {
            alert('Error: No fue posible obtener el registro.');
        });
    };

    handlers.limpia = function (e) {
        e.preventDefault();
        utils.limpiar();
    };

    handlers.regresaResultados = function (e) {
        e.preventDefault();
        if (!data.busquedaAgrupada) {
            $('#detalle-elemento').hide();
            $('#datos-busqueda-form,#resultados-busqueda').show();
        }
        if (data.busquedaAgrupada) {
            $('#detalle-elemento').hide();
            $('#resultados-busqueda-agrupados').show();
        }
    };

    utils.construyeNombreCompleto = function (dp) {
        var buffer = [];
        if (dp.nombre) {
            buffer.push(dp.nombre);
        }
        if (dp.primerApellido) {
            buffer.push(dp.primerApellido);
        }
        if (dp.segundoApellido) {
            buffer.push(dp.segundoApellido);
        }
        return buffer.join(' ');
    };

    //Si se agregan inputs realizamos la validación de cada campo.
    utils.inicializaValidaciones = function () {
        $(".valid-field").configura();
        $('#tipo-registro,#estatus').configura({
            required: false
        });
        $('.valid-field').validacion();
    };

    utils.limpiar = function () {
        $('#cultivo,#ciclo,#tipo-registro,#estatus,#validador').val('0').limpiaErrores();
        $('#resultados-busqueda').hide();
        $('#cargando-resultados').hide();
        $('#table-resultados-busqueda tbody').html('<tr><td colspan="6" class="text-center">Sin resultados</td></tr>');
    };

    utils.cargaCatalogos = function () {
        $.ajax({
            url: '/cultivos/resources/paginas/consultas',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            $('#ciclo').actualizaCombo(response.ciclos);
            $('#cultivo').actualizaCombo(response.cultivos);
            $('#validador,#usuario-validador').actualizaCombo(response.validadores);
            $('#estatus').actualizaCombo(response.estatus, {value: 'clave'});
            $('#tipo-registro').actualizaCombo(response.tiposRegistro, {value: 'clave'});
        }).fail(function () {
            alert('Error: No se pudo obtener los catálogos de la página.');
        });
    };

    utils.creaDatatable = function (resultados) {
        var buffer = [];
        buffer.push('<table id="table-resultados-busqueda"');
        buffer.push(' class="table table-striped" width="100%">');
        buffer.push('<thead class = "table-success">');
        buffer.push('<tr class="success">');
        buffer.push('<th><input id="todas-checkbox" type="checkbox"/></th>');
        buffer.push('<th>Folio</th>');
        buffer.push('<th>Fecha de Registro</th>');
        buffer.push('<th>Nombre productor</th>');
        buffer.push('<th>CURP</th>');
        buffer.push('<th>RFC</th>');
        buffer.push('<th>Tipo</th>');
        buffer.push('<th>Estatus</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');
        for (var i = 0; i < resultados.length; i++) {
            var resultado = resultados[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-center">');
            buffer.push('<input type="checkbox" id="' + resultado.uuid + '-productor-check" class="productor-check"/>');
            buffer.push('</td>');
            buffer.push('<td><a class="link-uuid" href="#" id="uuid.' + resultado.uuid + '">');
            buffer.push(resultado.folio);
            buffer.push('</a></td>');
            buffer.push('<td>');
            buffer.push($.segalmex.date.isoToFecha(resultado.fechaCreacion));
            buffer.push('</td>');
            buffer.push('<td>');
            var dp = resultado.datosProductor;
            buffer.push(dp.tipoPersona.clave === 'fisica' ? utils.construyeNombreCompleto(dp) : dp.nombreMoral);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(dp.curp);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(dp.rfc);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(utils.getTipoRegistro(resultado.tipoRegistro));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.estatus.nombre);
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        buffer.push('</tbody>');
        buffer.push('</table>');

        buffer.push('<br/>');
        return buffer.join('');
    };

    utils.configuraTablaResultados = function () {
        var aoColumns = [{"bSortable": false}, {"bSortable": true}, {"bSortable": false},
            {"bSortable": true}, {"bSortable": true}, {"bSortable": true}, {"bSortable": true}, {"bSortable": true}];
        utils.configuraTabla('table-resultados-busqueda', aoColumns);
    };

    utils.configuraTabla = function (tablaId, aoColumns) {
        var table = $('#' + tablaId).DataTable({
            "oLanguage": {
                "sEmptyTable": "No se encontró información para mostrar en la tabla",
                "sSearch": "Buscar ",
                "sZeroRecords": "No se encontraron resultados."
            },
            "bFilter": true,
            "bInfo": false,
            "bPaginate": false,
            "aoColumns": aoColumns,
            "aoColumnDefs": [{"bSortable": false, "aTargets": ["no-sort"]}]
        });
        table.on('order.dt search.dt', function () {
            table.column(0, {search: 'applied', order: 'applied'}).nodes();
        }).order([[1, 'asc']]).draw(false);
        $("#" + tablaId + "_filter input").addClass('input-buscar');
        $(".dataTables_empty").attr("colspan", "100%");
    };

    utils.getTipoRegistro = function (tipoRegistro) {
        switch (tipoRegistro) {
            case 'fisica':
                return 'Física';
            case 'moral':
                return 'Moral';
            case 'asociado':
                return 'Asociado';
            default :
                return tipoRegistro;
        }
    };
})(jQuery);