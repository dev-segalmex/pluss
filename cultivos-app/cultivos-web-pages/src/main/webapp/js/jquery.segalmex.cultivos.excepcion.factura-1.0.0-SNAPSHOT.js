/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.cultivos.excepcion.factura');

    var data = {
        excepciones: [],
        tipoBusqueda: 'maiz-comercial'
    };
    var utils = {};
    var handlers = {};

    $.segalmex.cultivos.excepcion.factura.init = function () {
        utils.cargaCatalogos();
        utils.inicializaValidaciones();
        $('#button-nuevo').click(utils.nuevo);
        $('#button-regresar').click(utils.regresa);
        $('#button-guardar').click(handlers.guarda);
        $('#fcdi-xml').change(handlers.verificaTamanoArchivo);
        $('#ciclo').change(utils.filtraExcepciones);
        $('#menu-bandejas a.nav-link').click(handlers.cambiaBandeja);
    };

    handlers.cambiaBandeja = function (e) {
        e.preventDefault();
        $('#div-' + data.tipoBusqueda).hide();
        var id = e.target.id.substring('muestra-'.length);
        if (data.tipoBusqueda === id) {
            $('#div-' + data.tipoBusqueda).show();
            return;
        }
        data.tipoBusqueda = id;
        $('#div-' + data.tipoBusqueda).show();
        $('#menu-bandejas a.nav-link').removeClass('active');
        $(e.target).addClass('active');
        utils.filtraExcepciones();
    };

    handlers.eliminaFactura = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var uuid = id.split('.')[1];
        if (!confirm('Se borrará la excepción de factura seleccionada. \n¿Desea continuar?')) {
            return;
        }
        $.ajax({
            url: '/cultivos/resources/facturas/restringidas/' + uuid,
            type: 'DELETE',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('La excepción de la factura se eliminó con éxito.');
            utils.cargaFacturas();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Error: No se pudo eliminar el registro.');
            }
        });
    };


    utils.cargaCatalogos = function () {
        $.ajax({
            url: '/cultivos/resources/paginas/inscripcion-factura/execpcion-factura',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            data.ciclos = response.ciclos;
            data.cultivos = response.cultivos;
            $('#ciclo').actualizaCombo(response.ciclos);
            utils.cargaFacturas();
        }).fail(function () {
            alert('Error: No se pudo obtener los catálogos de la página.');
        });

    };

    utils.inicializaValidaciones = function () {
        $(".valid-field").configura();
        $('#ciclo').configura({
            required: true
        });
        $('.valid-field').validacion();
    };

    utils.cargaFacturas = function () {
        $.ajax({
            url: '/cultivos/resources/facturas/restringidas',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            data.excepciones = response;
            $('#table-factura').html(utils.construyeTablaFacturas(utils.getExcepcionesFiltradas()));
            $('#table-resultados').on('click', 'button.eliminar-factura', handlers.eliminaFactura);
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert("Ocurrió un error al cargar los registros.");
            }
        });
    };

    utils.construyeTablaFacturas = function (excepciones) {
        var buffer = '';
        buffer = `
            <table id="table-resultados" class="table table-striped table-bordered table-hover" width="100%">
            <thead class="thead-dark">
            <tr class="success">
            <th>#</th>
            <th>Ciclo</th>
            <th>Cultivo</th>
            <th>UUID</th>
            <th class="text-center"><i class="fa fa-minus-circle"></i></th>
            </tr>
            </thead>
            <tbody>`;

        if (excepciones.length === 0) {
            buffer += `<tr><td colspan="5">No hay facturas registradas.</td></tr>`;
        } else {
            for (var i = 0; i < excepciones.length; i++) {
                var e = excepciones[i];
                buffer += `
                <tr>
                <td>${i + 1}</td>
                <td>${e.ciclo.nombre}</td>
                <td>${e.cultivo.nombre}</td>
                <td>${e.uuidTimbreFiscalDigital}</td>
                <td class="text-center"><button id="eliminar.${e.uuidTimbreFiscalDigital}"
                    class="btn btn-danger btn-sm eliminar-factura"><i class="fa fa-minus-circle"></i></button></td>
                </tr>`;
            }
        }
        ;

        buffer += `</table><br/>`;
        return buffer;
    };

    utils.nuevo = function (e) {
        e.preventDefault();
        $('#ciclo').limpiaErrores();
        var errores = [];
        $('#ciclo').valida(errores, true);

        if (errores.length !== 0) {
            $('#' + errores[0].campo).focus();
            return;
        }
        var cultivo = $.segalmex.get(data.cultivos, data.tipoBusqueda, 'clave');
        var ciclo = $.segalmex.get(data.ciclos, $('#ciclo').val());
        $('#info-cultivo').val(cultivo.nombre);
        $('#info-ciclo').val(ciclo.nombre);
        $('#div-excepciones-facturas').hide();
        $('#div-nueva-factura-excepcion').show();
    };

    utils.regresa = function () {
        utils.cargaFacturas();
        $('#div-nueva-factura-excepcion').hide();
        $('#div-excepciones-facturas').show();
        $('#fcdi-xml').val('').limpiaErrores();
        $('#ciclo').change().limpiaErrores();
    };

    handlers.guarda = function () {
        $('#button-guardar').html("Guardando...").prop('disabled', true);

        if ($('#fcdi-xml').val() === '') {
            $('#button-guardar').html("Guardar").prop('disabled', false);
            alert('Error: Es necesario agregar el CFDI (XML).');
            return;
        }

        var fd = new FormData();
        var constancia = $('#fcdi-xml')[0].files[0];
        fd.append('file', constancia);

        var cultivo = $.segalmex.get(data.cultivos, data.tipoBusqueda, 'clave');
        var ciclo = $.segalmex.get(data.ciclos, $('#ciclo').val());
        $.ajax({
            url: '/cultivos/resources/facturas/restringidas/?cultivo=' + cultivo.id + '&ciclo=' + ciclo.id,
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false
        }).done(function (response) {
            $('#button-guardar').html("Guardar").prop('disabled', false);
            alert('La excepción de la factura se guardó con éxito.');
            $('#fcdi-xml').val('').limpiaErrores();
        }).fail(function (jqXHR) {
            $('#button-guardar').html("Guardar").prop('disabled', false);
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Error: No se pudo guardar el registro.');
            }
        });
    };

    handlers.verificaTamanoArchivo = function (e) {
        $.segalmex.archivos.verificaArchivo(e, 'xml', 24 * 1024 * 1024);
    };

    utils.filtraExcepciones = function () {
        var excepciones = utils.getExcepcionesFiltradas();
        $('#table-factura').html(utils.construyeTablaFacturas(excepciones));
        $('#table-resultados').on('click', 'button.eliminar-factura', handlers.eliminaFactura);
    };

    utils.getExcepcionesFiltradas = function () {
        var excepciones = [];
        var cultivo = $.segalmex.get(data.cultivos, data.tipoBusqueda, 'clave');
        var ciclo = $.segalmex.get(data.ciclos, $('#ciclo').val());
        for (var i = 0; i < data.excepciones.length; i++) {
            var ef = data.excepciones[i];
            if (ef.cultivo.clave === cultivo.clave) {
                if (ciclo) {
                    if (ef.ciclo.clave === ciclo.clave) {
                        excepciones.push(ef);
                    }
                } else {
                    excepciones.push(ef);
                }
            }
        }
        return excepciones;
    };
})(jQuery);