/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.datos;

import mx.gob.segalmex.common.core.datos.ProcesadorCargadorDatos;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author oscar
 */
public class CargadorCultivosExec {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext(new String[]{
            "datosCultivosApplicationContext.xml",
            "execDatosCultivosApplicationContext.xml",
            "mysqlPersistenceApplicationContext.xml"
        });
        ProcesadorCargadorDatos cargador = context
                .getBean("procesadorCargadorDatosDefault", ProcesadorCargadorDatos.class);
        cargador.exec();
    }
}
