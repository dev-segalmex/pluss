package mx.gob.segalmex.pluss.cultivos;

import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;
import mx.gob.segalmex.pluss.modelo.productor.TipoProductorEnum;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.Set;
import java.util.stream.Collectors;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoProductor;

/**
 * @author oscar
 */
@Component
@Slf4j
public class ActualizadorProductorPequenioHelper {

    @Autowired
    private BuscadorInscripcionProductor buscadorInscripcionProductor;

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private BuscadorProductor buscadorProductor;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    public Set<String> getCurps(String cultivo) throws IOException {
        log.info("Obteniendo curps para {}...", cultivo);
        Reader r = new InputStreamReader(ActualizadorProductorPequenioHelper.class
                .getResourceAsStream("/" + cultivo + "-pequenios.csv"), "UTF-8");
        Set<String> curps = new BufferedReader(r).lines()
                .filter(StringUtils::isNotBlank)
                .filter(l -> !StringUtils.startsWith(l, "#"))
                .collect(Collectors.toSet());
        log.info("{} Curps únicos", curps.size());
        return curps;
    }

    @Transactional
    public void actualizaProductor(String curp, CicloAgricola ci, Cultivo cu, String tipoProductor) {
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setCurp(curp);
        parametros.setCiclo(ci);
        parametros.setCultivo(cu);
        parametros.setTipoProductor(tipoProductor);
        try {
            InscripcionProductor ip = buscadorInscripcionProductor.buscaElemento(parametros);
            ip.setTipoProductor(buscadorCatalogo.buscaElemento(TipoProductor.class,
                    TipoProductorEnum.PEQUENO.getClave()));
            actualizaProductorCiclo(ip.getFolio());
            registroEntidad.actualiza(ip);
            log.info("productor {} actualizado", curp);
        } catch (EmptyResultDataAccessException ouch) {
            log.info("No existe IP con curp {}", curp);
        }

    }

    private void actualizaProductorCiclo(String folio) {
        try {
            ProductorCiclo pc = buscadorProductor.buscaElemento(folio);
            pc.setTipoProductor(buscadorCatalogo.buscaElemento(TipoProductor.class,
                    TipoProductorEnum.PEQUENO.getClave()));
            registroEntidad.actualiza(pc);
        } catch (EmptyResultDataAccessException ouch) {
            log.info("No existe PC con folio {}", folio);
        }
    }
}
