package mx.gob.segalmex.pluss.cultivos;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricolaEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;
import mx.gob.segalmex.pluss.modelo.productor.TipoProductorEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author oscar
 */
@SpringBootApplication
@Slf4j
public class ActualizadorProductorPequenioApplication implements ApplicationRunner {

    @Autowired
    private ActualizadorProductorPequenioHelper helper;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorProductor buscadorProductor;

    public static void main(String[] args) {
        SpringApplication.run(ActualizadorProductorPequenioApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments arguments) throws Exception {
        String opcion = arguments.getOptionValues("opcion").get(0);
        switch(opcion) {
            case "pequeno-no-empadronado":
                runPequenoNoEmpadronado(arguments);
                break;
            case "verifica-productores":
                verificaProductores(arguments);
                break;
            default:
                throw new IllegalArgumentException("Opción inválida");
        }
    }

    private void runPequenoNoEmpadronado(ApplicationArguments arguments) throws Exception {
        log.info("Ejecutando ActualizadorProductorPequenioApplication...");
        Cultivo c = buscadorCatalogo.buscaElemento(Cultivo.class, arguments.getOptionValues("cultivo").get(0));
        CicloAgricola ca = buscadorCatalogo.buscaElemento(CicloAgricola.class, CicloAgricolaEnum.PV2021.getClave());
        helper.getCurps(c.getClave()).forEach(curp -> helper.actualizaProductor(curp, ca, c, TipoProductorEnum.PEQUENO_NO_EMPADRONADO.getClave()));
    }

    private void verificaProductores(ApplicationArguments arguments) {
        log.info("Ejecutando ActualizadorProductorPequenioApplication...");
        Cultivo cultivo = buscadorCatalogo.buscaElemento(Cultivo.class, arguments.getOptionValues("cultivo").get(0));
        CicloAgricola ciclo = buscadorCatalogo.buscaElemento(CicloAgricola.class, CicloAgricolaEnum.PV2021.getClave());

        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setCultivo(cultivo);
        parametros.setCiclo(ciclo);
        parametros.setTipoProductor(TipoProductorEnum.PEQUENO.getClave());

        buscadorProductor.buscaProductorCiclo(parametros).forEach(p -> {
            List<ProductorCiclo> todos = buscadorProductor.busca(p.getProductor());
            todos.sort(Comparator.comparing(pc -> pc.getCiclo().getOrden()));
            Collections.reverse(todos);

            ProductorCiclo anterior = todos.size() > 1 ? todos.get(1) : null;
            if (Objects.nonNull(anterior)
                    && anterior.getTipoProductor().getClave().equals(TipoProductorEnum.MEDIANO.getClave())) {
                log.debug("Ultimo: {} - {}, Previo: {} - {}",
                        p.getProductor().getRfc(),
                        p.getFolio(),
                        anterior.getProductor().getRfc(),
                        anterior.getFolio());
            }
        });
    }
}
