package mx.gob.segalmex.pluss.cultivos;

import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.archivos.ArchivoUtils;
import mx.gob.segalmex.common.core.archivos.BuscadorArchivoRelacionado;
import mx.gob.segalmex.common.core.archivos.ManejadorArchivo;
import mx.gob.segalmex.common.core.archivos.RegistroArchivoRelacionado;
import mx.gob.segalmex.common.core.archivos.SubDirectorioHelper;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorInscripcionFactura;
import mx.gob.segalmex.common.core.factura.busqueda.ParametrosInscripcionFactura;
import mx.gob.segalmex.common.core.util.CultivoClaveUtil;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import mx.gob.segalmex.pluss.modelo.archivos.ArchivoRelacionado;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * @author ismael
 */
@SpringBootApplication
@Slf4j
public class FacturaUtilsApplication implements ApplicationRunner {

    private static final String ARG_OPCION = "opcion";

    private static final String ARG_UBICACION = "ubicacion";

    private static final String ARG_FOLIOS = "folios";

    private static final String TIPO = "cfdi";

    private static final String CLASE = InscripcionFactura.class.getSimpleName();

    private static final String XML_EXTENSION = ".xml";

    private static final String PDF_EXTENSION = ".pdf";

    private static final String NOT_FOUND_EXTENSION = ".not-found";

    @Autowired
    private BuscadorInscripcionFactura buscadorInscripcionFactura;

    @Autowired
    private BuscadorArchivoRelacionado buscadorArchivoRelacionado;

    @Autowired
    private ManejadorArchivo manejadorArchivo;

    @Autowired
    private RegistroArchivoRelacionado registroArchivoRelacionado;

    public static void main(String[] args) {
        SpringApplication.run(FacturaUtilsApplication.class, args);
    }

    @Override
    public void run(ApplicationArguments arguments) throws Exception {
        log.info("Ejecutando FacturaExtractorApplication...");
        Optional<String> opcion = arguments.getOptionValues(ARG_OPCION).isEmpty() ?
                Optional.empty() : Optional.of(arguments.getOptionValues(ARG_OPCION).get(0));

        String ubicacion = arguments.getOptionValues(ARG_UBICACION).get(0);
        String folios = arguments.getOptionValues(ARG_FOLIOS).get(0);
        switch (opcion.orElseThrow(RuntimeException::new)) {
            case "verificar":
                log.info("Ejecutando verificación de facturas faltantes...");
                ejecuta(folios, ubicacion, false);
                break;
            case "descargar":
                log.info("Ejecutando descarga de facturas...");
                ejecuta(folios, ubicacion, true);
                break;
            case "subir":
                log.info("Ejecutando subida de facturas..");
                uploadPdf(folios, ubicacion);
                break;
            default:
                throw new IllegalArgumentException("Opción desconocida: " + opcion.get());
        }
    }

    private void ejecuta(String folios, String ubicacion, boolean descargar) {
        getFolios(folios).forEach(folio -> {
            log.info("Buscando archivos de inscripcion-factura: {}", folio);
            getInscripcion(folio).forEach(factura -> {
                if (descargar) {
                    getCfdi(factura, ubicacion);
                }
                getPdf(factura, ubicacion, descargar);
            });
        });
    }

    private Stream<String> getFolios(String folios) {
        try {
            return Files.lines(Paths.get(folios));
        } catch (IOException ouch) {
            log.error("No existe el archivo: {}", folios);
            log.error("Error", ouch);
            throw new RuntimeException(folios);
        }
    }

    private void getCfdi(InscripcionFactura factura, String ubicacion) {
        String folio = factura.getFolio();
        Archivo cfdi = factura.getCfdi().getArchivo();
        try {
            Path path = Paths.get(ubicacion, folio + XML_EXTENSION);
            Files.write(path, manejadorArchivo.obten(cfdi));
        } catch (IOException ouch) {
            log.error("Error al obtener el cfdi: {}", ouch);
        }
    }

    private void getPdf(InscripcionFactura factura, String ubicacion, boolean descargar) {
        String folio = factura.getFolio();
        try {
            List<ArchivoRelacionado> archivos = buscadorArchivoRelacionado.busca(CLASE, factura.getUuid(), TIPO);
            if (archivos.isEmpty()) {
                Files.createFile(Paths.get(ubicacion, folio + NOT_FOUND_EXTENSION));
            } else {
                if (descargar) {
                    Archivo pdf = archivos.get(0).getArchivo();
                    Path path = Paths.get(ubicacion, folio + PDF_EXTENSION);
                    Files.write(path, manejadorArchivo.obten(pdf));
                }
            }
        } catch (IOException ouch) {
            log.error("Error al obtener el pdf: {}", ouch);
        }
    }

    private List<InscripcionFactura> getInscripcion(String folio) {
        ParametrosInscripcionFactura parametros = new ParametrosInscripcionFactura();
        parametros.setFolio(folio);
        return buscadorInscripcionFactura.busca(parametros);
    }

    private void uploadPdf(String folios, String ubicacion) {
        getFolios(folios).forEach(folio -> {
            log.info("Buscando inscripcion-factura: {}", folio);
            getInscripcion(folio).forEach(factura -> {
                Path path = Paths.get(ubicacion, folio + PDF_EXTENSION);
                log.info("Subiendo archivo: {}", path);
                try {
                    uploadPdf(factura, new FileInputStream(path.toFile()));
                } catch (IOException ouch) {
                    log.error("No fue posible subir el archivo: {}", path);
                }
            });
        });
    }

    private void uploadPdf(InscripcionFactura inscripcion, InputStream is) {
        try {
            String subidirectorio = SubDirectorioHelper.getSubdirectorio(inscripcion.getCiclo().getClave(),
                    "cfdi", inscripcion.getFolio());
            Archivo archivo = ArchivoUtils.getInstance(inscripcion.getFolio() + PDF_EXTENSION,
                    CultivoClaveUtil.getClaveCorta(inscripcion.getCultivo()), subidirectorio, false);

            ArchivoRelacionado ar = new ArchivoRelacionado();
            ar.setArchivo(archivo);
            ar.setUsuarioRegistra(inscripcion.getUsuarioRegistra());
            ar.setClase(inscripcion.getClass().getSimpleName());
            ar.setReferencia(inscripcion.getUuid());
            ar.setTipo("cfdi");

            registroArchivoRelacionado.registra(archivo, ar, is);
        } catch (IOException ouch) {
            throw new RuntimeException(ouch);
        }
    }

}
