package mx.gob.segalmex.pluss.cultivos;

import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.archivos.BuscadorArchivoRelacionado;
import mx.gob.segalmex.common.core.archivos.ManejadorArchivo;
import mx.gob.segalmex.common.core.archivos.ManejadorArchivoJpa;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorInscripcionFactura;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.FilterType;

/**
 * @author oscar
 */
@ComponentScan(
        basePackages = {
                "mx.gob.segalmex.common.core.archivos",
                "mx.gob.segalmex.common.core.factura.busqueda",
                "mx.gob.segalmex.common.core.persistencia",
                "mx.gob.segalmex.common.core.catalogos.busqueda"
        },
        includeFilters = {
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = BuscadorArchivoRelacionado.class),
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = BuscadorInscripcionFactura.class),
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = RegistroEntidad.class)
        },
        excludeFilters = {
                @ComponentScan.Filter(type = FilterType.ASSIGNABLE_TYPE, value = ManejadorArchivo.class),
        })
@EntityScan("mx.gob.segalmex")
@Configuration
@Slf4j
public class FacturaUtilsConfiguration {

    @Value("${pluss.cultivos.facturas.base}")
    private String base;

    @Bean(name = "manejadorArchivo")
    public ManejadorArchivo getManejadorArchivo() {
        ManejadorArchivoJpa ma = new ManejadorArchivoJpa();
        log.info("Ruta base: {}", base);
        ma.setBase(base);
        return ma;
    }

}
