/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.productor;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.archivos.ArchivoConverter;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.pluss.modelo.productor.ContratoFirmado;
import mx.gob.segalmex.granos.web.modelo.catalogos.TipoPrecioConverter;
import mx.gob.segalmex.granos.web.modelo.contrato.EstatusInscripcionConverter;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "contrato-firmado")
public class ContratoFirmadoConverter extends AbstractEntidadConverter {

    private ContratoFirmado entity;

    public ContratoFirmadoConverter() {
        entity = new ContratoFirmado();
    }

    public ContratoFirmadoConverter(ContratoFirmado entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad ContratoFirmado no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public ContratoFirmado getEntity() {
        return entity;
    }

    public String getNumeroContrato() {
        return entity.getNumeroContrato();
    }

    public void setNumeroContrato(String numeroContrato) {
        entity.setNumeroContrato(numeroContrato);
    }

    public BigDecimal getCantidadContratada() {
        return entity.getCantidadContratada();
    }

    public void setCantidadContratada(BigDecimal cantidadContratada) {
        entity.setCantidadContratada(cantidadContratada);
    }

    public BigDecimal getCantidadContratadaCobertura() {
        return entity.getCantidadContratadaCobertura();
    }

    public void setCantidadContratadaCobertura(BigDecimal cantidadContratadaCobertura) {
        entity.setCantidadContratadaCobertura(cantidadContratadaCobertura);
    }

    public String getFolio() {
        return entity.getFolio();
    }

    public void setFolio(String folio) {
        entity.setFolio(folio);
    }

    public String getEmpresa() {
        return entity.getEmpresa();
    }

    public void setEmpresa(String empresa) {
        entity.setEmpresa(empresa);
    }

    @XmlElement
    public ArchivoConverter getArchivo() {
        return Objects.nonNull(entity.getArchivo())
                ? new ArchivoConverter(entity.getArchivo(), expandLevel - 1) : null;
    }

    @XmlElement
    public EstatusInscripcionConverter getEstatus() {
        return Objects.nonNull(entity.getEstatus())
                ? new EstatusInscripcionConverter(entity.getEstatus(), expandLevel - 1) : null;
    }

    public Calendar getFechaCancelacion() {
        return entity.getFechaCancelacion();
    }

    public void setFechaCanccelacion(Calendar fechaCancelacion) {
        entity.setFechaCancelacion(fechaCancelacion);
    }

    @XmlElement
    public int getOrden() {
        return entity.getOrden();
    }

    public TipoPrecioConverter getTipoPrecio() {
        return expandLevel > 0 && Objects.nonNull(entity.getTipoPrecio())
                ? new TipoPrecioConverter(entity.getTipoPrecio(), expandLevel - 1) : null;
    }

    public void setTipoPrecio(TipoPrecioConverter tipoPrecio) {
        entity.setTipoPrecio(tipoPrecio.getEntity());
    }
}
