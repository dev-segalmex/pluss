/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.inscripcion;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.EnumConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CicloAgricolaConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CultivoConverter;
import mx.gob.segalmex.common.web.modelo.seguridad.UsuarioConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;

/**
 *
 * @author ismael
 */
public class CatalogosBandejasConverter {

    private List<Usuario> validadores = new ArrayList<>();

    private List<Cultivo> cultivos = new ArrayList<>();

    private List<CicloAgricola> ciclos = new ArrayList<>();

    private List<EnumConverter> tiposRegistro = new ArrayList<>();

    @XmlElement
    public List<UsuarioConverter> getValidadores() {
        return CollectionConverter.convert(UsuarioConverter.class, validadores, 1);
    }

    public void setValidadores(List<Usuario> validadores) {
        this.validadores = validadores;
    }

    @XmlElement
    public List<CultivoConverter> getCultivos() {
        return CollectionConverter.convert(CultivoConverter.class, cultivos, 1);
    }

    public void setCultivos(List<Cultivo> cultivos) {
        this.cultivos = cultivos;
    }

    @XmlElement
    public List<CicloAgricolaConverter> getCiclos() {
        return CollectionConverter.convert(CicloAgricolaConverter.class, ciclos, 1);
    }

    public void setCiclos(List<CicloAgricola> ciclos) {
        this.ciclos = ciclos;
    }

    @XmlElement
    public List<EnumConverter> getTiposRegistro() {
        return tiposRegistro;
    }

    public void setTiposRegistro(List<EnumConverter> tiposRegistro) {
        this.tiposRegistro = tiposRegistro;
    }
}
