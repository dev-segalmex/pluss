/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.contrato;

import mx.gob.segalmex.granos.web.modelo.historico.HistoricoRegistroConverter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.archivos.ArchivoConverter;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CicloAgricolaConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CultivoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.EstadoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.TipoCultivoConverter;
import mx.gob.segalmex.granos.web.modelo.inscripcion.ComentarioInscripcionConverter;
import mx.gob.segalmex.common.web.modelo.seguridad.UsuarioConverter;
import mx.gob.segalmex.pluss.modelo.historico.HistoricoRegistro;
import mx.gob.segalmex.pluss.modelo.inscripcion.ComentarioInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.ContratoProductor;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContratoEstado;
import mx.gob.segalmex.granos.web.modelo.catalogos.SucursalConverter;
import mx.gob.segalmex.granos.web.modelo.catalogos.TipoEmpresaConverter;
import mx.gob.segalmex.granos.web.modelo.catalogos.TipoPrecioConverter;
import mx.gob.segalmex.pluss.modelo.contrato.CoberturaInscripcionContrato;
import mx.gob.segalmex.pluss.cultivos.modelo.contrato.CoberturaInscripcionContratoConverter;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "inscripcion-contrato")
public class InscripcionContratoConverter extends AbstractEntidadConverter {

    private final InscripcionContrato entity;

    private boolean reenvio;

    private List<Archivo> archivos = new ArrayList<>();

    private List<DatoCapturado> datos = new ArrayList<>();

    private List<HistoricoRegistro> historial = new ArrayList<>();

    private List<ComentarioInscripcion> comentarios = new ArrayList<>();

    /**
     * El tipo de histórico en los casos en que se muestra la inscripción en una
     * bandeja.
     */
    private String tipo;

    private String nombreEmpresa;

    public InscripcionContratoConverter() {
        entity = new InscripcionContrato();
    }

    public InscripcionContratoConverter(InscripcionContrato entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad InscripcionContrato no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public InscripcionContrato getEntity() {
        return entity;
    }

    public String getFolio() {
        return getEntity().getFolio();
    }

    public void setFolio(String folio) {
        getEntity().setFolio(folio);
    }

    public String getUuid() {
        return getEntity().getUuid();
    }

    public void setUuid(String uuid) {
        getEntity().setUuid(uuid);
    }

    @XmlElement
    public UsuarioConverter getUsuarioRegistra() {
        return expandLevel > 0 && Objects.nonNull(entity.getUsuarioRegistra())
                ? new UsuarioConverter(getEntity().getUsuarioRegistra(), expandLevel - 1) : null;
    }

    @XmlElement
    public UsuarioConverter getUsuarioValidador() {
        return expandLevel > 0 && Objects.nonNull(entity.getUsuarioValidador())
                ? new UsuarioConverter(entity.getUsuarioValidador(), expandLevel - 1) : null;
    }

    @XmlElement
    public UsuarioConverter getUsuarioAsignado() {
        return expandLevel > 0 && Objects.nonNull(entity.getUsuarioAsignado())
                ? new UsuarioConverter(entity.getUsuarioAsignado(), expandLevel - 1) : null;
    }

    public CultivoConverter getCultivo() {
        return expandLevel > 0 && Objects.nonNull(getEntity().getCultivo())
                ? new CultivoConverter(getEntity().getCultivo(), expandLevel - 1) : null;
    }

    public void setCultivo(CultivoConverter cultivo) {
        getEntity().setCultivo(cultivo.getEntity());
    }

    public CicloAgricolaConverter getCiclo() {
        return expandLevel > 0 && Objects.nonNull(getEntity().getCiclo())
                ? new CicloAgricolaConverter(getEntity().getCiclo(), expandLevel - 1) : null;
    }

    public void setCiclo(CicloAgricolaConverter ciclo) {
        getEntity().setCiclo(ciclo.getEntity());
    }

    public TipoCultivoConverter getTipoCultivo() {
        return expandLevel > 0 && Objects.nonNull(getEntity().getTipoCultivo())
                ? new TipoCultivoConverter(getEntity().getTipoCultivo(), expandLevel - 1) : null;
    }

    public void setTipoCultivo(TipoCultivoConverter tipoCultivo) {
        getEntity().setTipoCultivo(tipoCultivo.getEntity());
    }

    @XmlElement
    public SucursalConverter getSucursal() {
        return expandLevel > 0 && Objects.nonNull(entity.getSucursal())
                ? new SucursalConverter(entity.getSucursal(), expandLevel - 1) : null;
    }

    public Boolean getCobertura() {
        return getEntity().getCobertura();
    }

    public void setCobertura(Boolean cobertura) {
        getEntity().setCobertura(cobertura);
    }

    public Integer getNumeroProductores() {
        return entity.getNumeroProductores();
    }

    public void setNumeroProductores(Integer numeroProductores) {
        entity.setNumeroProductores(numeroProductores);
    }

    public String getNumeroContrato() {
        return getEntity().getNumeroContrato();
    }

    public void setNumeroContrato(String numeroContrato) {
        getEntity().setNumeroContrato(numeroContrato);
    }

    public Calendar getFechaFirma() {
        return getEntity().getFechaFirma();
    }

    public void setFechaFirma(Calendar fechaFirma) {
        getEntity().setFechaFirma(fechaFirma);
    }

    public List<InscripcionContratoEstadoConverter> getEstadosDestino() {
        return expandLevel > 0 && Objects.nonNull(entity.getEstadosDestino())
                ? CollectionConverter.convert(InscripcionContratoEstadoConverter.class, entity.getEstadosDestino(), expandLevel - 1) : null;
    }

    public void setEstadosDestino(List<InscripcionContratoEstadoConverter> destinos) {
        if (Objects.nonNull(destinos)) {
            entity.setEstadosDestino(CollectionConverter.unconvert(InscripcionContratoEstado.class, destinos));
        }
    }

    @XmlElement
    public String getEstadosDestinoTexto() {
        return expandLevel > 0 ? entity.getEstadosDestinoTexto() : null;
    }

    public TipoEmpresaConverter getTipoEmpresaComprador() {
        return expandLevel > 0 && Objects.nonNull(getEntity().getTipoEmpresaComprador())
                ? new TipoEmpresaConverter(getEntity().getTipoEmpresaComprador(), expandLevel - 1) : null;
    }

    public void setTipoEmpresaComprador(TipoEmpresaConverter tipoEmpresaComprador) {
        getEntity().setTipoEmpresaComprador(tipoEmpresaComprador.getEntity());
    }

    public String getNombreComprador() {
        return getEntity().getNombreComprador();
    }

    public void setNombreComprador(String nombreComprador) {
        getEntity().setNombreComprador(nombreComprador);
    }

    public String getRfcComprador() {
        return getEntity().getRfcComprador();
    }

    public void setRfcComprador(String rfcComprador) {
        getEntity().setRfcComprador(rfcComprador);
    }

    public TipoEmpresaConverter getTipoEmpresaVendedor() {
        return expandLevel > 0 && Objects.nonNull(getEntity().getTipoEmpresaVendedor())
                ? new TipoEmpresaConverter(getEntity().getTipoEmpresaVendedor(), expandLevel - 1) : null;
    }

    public void setTipoEmpresaVendedor(TipoEmpresaConverter tipoEmpresaVendedor) {
        getEntity().setTipoEmpresaVendedor(tipoEmpresaVendedor.getEntity());
    }

    public String getNombreVendedor() {
        return getEntity().getNombreVendedor();
    }

    public void setNombreVendedor(String nombreVendedor) {
        getEntity().setNombreVendedor(nombreVendedor);
    }

    public String getRfcVendedor() {
        return getEntity().getRfcVendedor();
    }

    public void setRfcVendedor(String rfcVendedor) {
        getEntity().setRfcVendedor(rfcVendedor);
    }

    public boolean isReenvio() {
        return reenvio;
    }

    public void setReenvio(boolean reenvio) {
        this.reenvio = reenvio;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @XmlElement
    public List<ArchivoConverter> getArchivos() {
        if (Objects.isNull(archivos) || archivos.isEmpty()) {
            return null;
        }
        return CollectionConverter.convert(ArchivoConverter.class, archivos, expandLevel - 1);
    }

    public void setArchivos(List<Archivo> archivos) {
        this.archivos = archivos;
    }

    @XmlElement
    public List<DatoCapturadoConverter> getDatos() {
        if (Objects.isNull(datos) || datos.isEmpty()) {
            return null;
        }
        return CollectionConverter.convert(DatoCapturadoConverter.class, datos, expandLevel - 1);
    }

    public void setDatos(List<DatoCapturado> datos) {
        this.datos = datos;
    }

    @XmlElement
    public List<HistoricoRegistroConverter> getHistorial() {
        if (Objects.isNull(historial) || historial.isEmpty()) {
            return null;
        }
        return CollectionConverter.convert(HistoricoRegistroConverter.class, historial, expandLevel - 1);
    }

    public void setHistorial(List<HistoricoRegistro> historial) {
        this.historial = historial;
    }

    @XmlElement
    public EstatusInscripcionConverter getEstatus() {
        return Objects.nonNull(entity.getEstatus()) && expandLevel > 0
                ? new EstatusInscripcionConverter(entity.getEstatus(), expandLevel - 1) : null;
    }

    @XmlElement
    public ArchivoConverter getContrato() {
        return expandLevel > 0 && Objects.nonNull(entity.getContrato())
                ? new ArchivoConverter(entity.getContrato(), expandLevel - 1) : null;
    }

    @XmlElement
    public BigDecimal getVolumenTotal() {
        return entity.getVolumenTotal();
    }

    public void setVolumenTotal(BigDecimal volumentTotal) {
        entity.setVolumenTotal(volumentTotal);
    }

    @XmlElement
    public EstadoConverter getEstado() {
        return expandLevel > 0 && Objects.nonNull(entity.getEstado())
                ? new EstadoConverter(entity.getEstado(), expandLevel - 1) : null;
    }

    public void setEstado(EstadoConverter estado) {
        getEntity().setEstado(estado.getEntity());
    }

    public String getComentarioEstatus() {
        return entity.getComentarioEstatus();
    }

    public void setComentarioEstatus(String comentarioEstatus) {
        entity.setComentarioEstatus(comentarioEstatus);
    }

    public String getNombreEmpresa() {
        return nombreEmpresa;
    }

    public void setNombreEmpresa(String nombreEmpresa) {
        this.nombreEmpresa = nombreEmpresa;
    }

    @XmlElement
    public List<ComentarioInscripcionConverter> getComentarios() {
        if (Objects.isNull(comentarios) || comentarios.isEmpty()) {
            return null;
        }
        return CollectionConverter.convert(ComentarioInscripcionConverter.class, comentarios, expandLevel - 1);
    }

    public void setComentarios(List<ComentarioInscripcion> comentarios) {
        this.comentarios = comentarios;
    }

    @XmlElement
    public List<CoberturaInscripcionContratoConverter> getCoberturas() {
        if (Objects.isNull(getEntity().getCoberturas()) || getEntity().getCoberturas().isEmpty()) {
            return null;
        }
        return CollectionConverter.convert(CoberturaInscripcionContratoConverter.class, getEntity().getCoberturas(), expandLevel - 1);
    }

    public void setCoberturas(List<CoberturaInscripcionContratoConverter> coberturas) {
        if (Objects.nonNull(coberturas)) {
            getEntity().setCoberturas(CollectionConverter.unconvert(
                    CoberturaInscripcionContrato.class, coberturas));
        }
    }

    @XmlElement
    public String getFolioSnics() {
        return entity.getFolioSnics();
    }

    public void setFolioSnics(String folioSnics) {
        entity.setFolioSnics(folioSnics);
    }

    public String getClaveArchivos() {
        return entity.getClaveArchivos();
    }

    public void setClaveArchivos(String claveArchivos) {
        entity.setClaveArchivos(claveArchivos);
    }

    @XmlElement
    public BigDecimal getSuperficie() {
        return getEntity().getSuperficie();
    }

    public void setSuperficie(BigDecimal superficie) {
        getEntity().setSuperficie(superficie);
    }

    @XmlElement
    public TipoPrecioConverter getTipoPrecio() {
        return expandLevel > 0 && Objects.nonNull(entity.getTipoPrecio())
                ? new TipoPrecioConverter(entity.getTipoPrecio(), expandLevel - 1) : null;
    }

    public void setTipoPrecio(TipoPrecioConverter tipoPrecio) {
        entity.setTipoPrecio(tipoPrecio.getEntity());
    }

    @XmlElement
    public BigDecimal getPrecioFuturo() {
        return getEntity().getPrecioFuturo();
    }

    public void setPrecioFuturo(BigDecimal precioFuturo) {
        getEntity().setPrecioFuturo(precioFuturo);
    }

    @XmlElement
    public BigDecimal getBaseAcordada() {
        return getEntity().getBaseAcordada();
    }

    public void setBaseAcordada(BigDecimal baseAcordada) {
        getEntity().setBaseAcordada(baseAcordada);
    }

    @XmlElement
    public BigDecimal getPrecioFijo() {
        return getEntity().getPrecioFijo();
    }

    public void setPrecioFijo(BigDecimal precioFijo) {
        getEntity().setPrecioFijo(precioFijo);
    }

    public List<ContratoProductorConverter> getContratosProductor() {
        return expandLevel > 0 && Objects.nonNull(entity.getContratosProductor())
                ? CollectionConverter.convert(ContratoProductorConverter.class, entity.getContratosProductor(),
                        expandLevel - 1) : null;
    }

    public void setContratosProductor(List<ContratoProductorConverter> contratosProductor) {
        if (Objects.nonNull(contratosProductor)) {
            entity.setContratosProductor(CollectionConverter.unconvert(ContratoProductor.class, contratosProductor));
        }
    }
}
