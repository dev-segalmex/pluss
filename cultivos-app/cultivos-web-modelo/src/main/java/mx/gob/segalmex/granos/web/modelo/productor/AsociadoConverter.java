/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.productor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.productor.Asociado;

/**
 *
 * @author ismael
 */
public class AsociadoConverter extends AbstractEntidadConverter {

    private final Asociado entity;

    public AsociadoConverter() {
        entity = new Asociado();
        expandLevel = 1;
    }

    public AsociadoConverter(Asociado entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Asociado no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public Asociado getEntity() {
        return entity;
    }

    @XmlElement
    public String getNombre() {
        return entity.getNombre();
    }

    @XmlElement
    public String getRfc() {
        return entity.getRfc();
    }

    @XmlElement
    public String getCurp() {
        return entity.getCurp();
    }

    @XmlElement
    public String getLabel() {
        List<String> elementos = new ArrayList<>();
        if (Objects.nonNull(entity.getRfc())) {
            elementos.add(entity.getRfc());
        }
        if (Objects.nonNull(entity.getCurp())) {
            elementos.add(entity.getCurp());
        }
        if (Objects.nonNull(entity.getNombre())) {
            elementos.add(entity.getNombre());
        }
        return String.join(" - ", elementos) + " ("+entity.getRelacion()+")";
    }

    @XmlElement
    public Cultivo getCultivo() {
        return entity.getCultivo();
    }

    @XmlElement
    public CicloAgricola getCiclo() {
        return entity.getCiclo();
    }

}
