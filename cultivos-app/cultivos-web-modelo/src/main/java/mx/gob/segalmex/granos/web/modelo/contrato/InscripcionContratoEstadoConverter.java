/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.contrato;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.EstadoConverter;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContratoEstado;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "inscripcion-contrato-estado")
public class InscripcionContratoEstadoConverter extends AbstractEntidadConverter {

    private final InscripcionContratoEstado entity;

    public InscripcionContratoEstadoConverter() {
        entity = new InscripcionContratoEstado();
    }

    public InscripcionContratoEstadoConverter(InscripcionContratoEstado entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad InscripcionContratoEstado no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public InscripcionContratoEstado getEntity() {
        return entity;
    }

    public EstadoConverter getEstado() {
        return expandLevel > 0 && Objects.nonNull(getEntity().getEstado())
                ? new EstadoConverter(entity.getEstado(), expandLevel - 1) : null;
    }

    public void setEstado(EstadoConverter estado) {
        getEntity().setEstado(estado.getEntity());
    }

}
