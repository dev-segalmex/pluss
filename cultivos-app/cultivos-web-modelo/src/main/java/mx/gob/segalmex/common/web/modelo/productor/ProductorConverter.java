/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.productor;

import java.util.Objects;
import mx.gob.segalmex.pluss.modelo.personas.Persona;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;
import mx.gob.segalmex.pluss.modelo.productor.Productor;

/**
 *
 * @author ismael
 */
public class ProductorConverter extends AbstractCatalogoConverter {

    private final Productor entity;

    public ProductorConverter() {
        this.entity = new Productor();
        this.expandLevel = 1;
    }

    public ProductorConverter(Productor entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Productor no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public Productor getEntity() {
        return this.entity;
    }

    public String getRfc() {
        return entity.getRfc();
    }

    public void setRfc(String rfc) {
        entity.setRfc(rfc);
    }

    public Persona getPersona() {
        return entity.getPersona();
    }

    public void setPersona(Persona persona) {
        entity.setPersona(persona);
    }

    public String getCurp() {
        return entity.getCurp();
    }

    public void setCurp(String curp) {
        entity.setCurp(curp);
    }

    public String getUuid() {
        return entity.getUuid();
    }

    public void setUuid(String uuid) {
        entity.setUuid(uuid);
    }
}
