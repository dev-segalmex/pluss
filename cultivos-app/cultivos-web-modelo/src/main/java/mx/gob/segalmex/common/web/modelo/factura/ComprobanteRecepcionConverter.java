/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.factura;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Objects;
import mx.gob.segalmex.pluss.modelo.factura.ComprobanteRecepcion;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;

/**
 *
 * @author oscar
 */
public class ComprobanteRecepcionConverter extends AbstractEntidadConverter {

    /**
     * La entidad que respalda el converter.
     */
    private final ComprobanteRecepcion entity;

    public ComprobanteRecepcionConverter() {
        entity = new ComprobanteRecepcion();
        expandLevel = 1;
    }

    public ComprobanteRecepcionConverter(ComprobanteRecepcion entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad ComprobanteRecepcion no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public ComprobanteRecepcion getEntity() {
        return entity;
    }

    public String getFolio() {
        return entity.getFolio();
    }

    public void setFolio(String folio) {
        entity.setFolio(folio);
    }

    public String getDescripcion() {
        return entity.getDescripcion();
    }

    public void setDescripcion(String descripcion) {
        entity.setDescripcion(descripcion);
    }

    public BigDecimal getVolumen() {
        return entity.getVolumen();
    }

    public void setVolumen(BigDecimal volumen) {
        entity.setVolumen(volumen);
    }

    public String getEstatus() {
        return entity.getEstatus();
    }

    public void setEstatus(String estatus) {
        entity.setEstatus(estatus);
    }

    public int getOrden() {
        return entity.getOrden();
    }

    public void setOrden(int orden) {
        entity.setOrden(orden);
    }
    
    public Calendar getFechaComprobante() {
        return entity.getFechaComprobante();
    }

    public void setFechaComprobante(Calendar fechaComprobante) {
        entity.setFechaComprobante(fechaComprobante);
    }
}
