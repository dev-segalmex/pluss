/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.contrato;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcion;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "tipo-historico-inscripcion")
public class TipoHistoricoInscripcionConverter extends AbstractCatalogoConverter {

    private final TipoHistoricoInscripcion entity;

    public TipoHistoricoInscripcionConverter() {
        this.entity = new TipoHistoricoInscripcion();
        this.expandLevel = 1;
    }

    public TipoHistoricoInscripcionConverter(TipoHistoricoInscripcion entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad TipoHistoricoInscripcion no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public TipoHistoricoInscripcion getEntity() {
        return this.entity;
    }
}
