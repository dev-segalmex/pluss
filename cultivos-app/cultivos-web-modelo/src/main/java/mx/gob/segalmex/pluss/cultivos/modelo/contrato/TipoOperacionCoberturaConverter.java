/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.modelo.contrato;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;
import mx.gob.segalmex.pluss.modelo.contrato.TipoOperacionCobertura;

/**
 *
 * @author cuecho
 */
@XmlRootElement(name = "tipo-operacion-cobertura")
public class TipoOperacionCoberturaConverter extends AbstractCatalogoConverter {

    private final TipoOperacionCobertura entity;

    public TipoOperacionCoberturaConverter() {
        this.entity = new TipoOperacionCobertura();
        this.expandLevel = 1;
    }

    public TipoOperacionCoberturaConverter(TipoOperacionCobertura entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad TipoOperacionCobertura no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public TipoOperacionCobertura getEntity() {
        return this.entity;
    }

}
