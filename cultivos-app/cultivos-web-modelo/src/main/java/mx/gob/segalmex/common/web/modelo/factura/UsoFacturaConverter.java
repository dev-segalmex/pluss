/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.factura;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import mx.gob.segalmex.pluss.modelo.factura.UsoFactura;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.BancoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.EstadoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.TipoCultivoConverter;
import mx.gob.segalmex.common.web.modelo.productor.ProductorCicloConverter;
import mx.gob.segalmex.common.web.modelo.productor.ProductorConverter;
import mx.gob.segalmex.granos.web.modelo.catalogos.TipoPrecioConverter;
import mx.gob.segalmex.pluss.modelo.factura.TipoUsoFactura;

/**
 *
 * @author jurgen
 */
public class UsoFacturaConverter extends AbstractEntidadConverter {

    private final UsoFactura entity;

    public UsoFacturaConverter() {
        entity = new UsoFactura();
        expandLevel = 1;
    }

    public UsoFacturaConverter(UsoFactura entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad UsoFactura no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public UsoFactura getEntity() {
        return entity;
    }

    @XmlElement
    public String getUuid() {
        return entity.getUuid();
    }

    public void setUuid(String uuid) {
        entity.setUuid(uuid);
    }

    @XmlElement
    public EstatusUsoFacturaConverter getEstatus() {
        return Objects.nonNull(entity.getEstatus())
                ? new EstatusUsoFacturaConverter(entity.getEstatus(), expandLevel - 1) : null;
    }

    @XmlElement
    public String getFolio() {
        return entity.getFolio();
    }

    @XmlElement
    public TipoCultivoConverter getTipoCultivo() {
        return Objects.nonNull(entity.getTipoCultivo())
                ? new TipoCultivoConverter(entity.getTipoCultivo(), expandLevel - 1) : null;
    }

    @XmlElement
    public BigDecimal getToneladas() {
        return entity.getToneladas();
    }

    @XmlElement
    public BigDecimal getEstimuloTonelada() {
        return entity.getEstimuloTonelada();
    }

    public void setEstimuloTonelada(BigDecimal estimuloTonelada) {
        entity.setEstimuloTonelada(estimuloTonelada);
    }

    @XmlElement
    public BigDecimal getEstimuloTotal() {
        return entity.getEstimuloTotal();
    }

    @XmlElement
    public FacturaConverter getFactura() {
        return Objects.nonNull(entity.getFactura()) ? new FacturaConverter(entity.getFactura(), expandLevel - 1) : null;
    }

    public ProductorConverter getProductor() {
        return expandLevel > 0 && Objects.nonNull(entity.getProductor())
                ? new ProductorConverter(entity.getProductor(), expandLevel - 1) : null;
    }

    public void setProductor(ProductorConverter productor) {
        entity.setProductor(productor.getEntity());
    }

    @XmlElement
    public ProductorCicloConverter getProductorCiclo() {
        return Objects.nonNull(entity.getProductorCiclo())
                ? new ProductorCicloConverter(entity.getProductorCiclo(), expandLevel - 1) : null;
    }

    public void setProductorCiclo(ProductorCicloConverter productorCiclo) {
        entity.setProductorCiclo(productorCiclo.getEntity());
    }

    public TipoUsoFacturaConverter getTipo() {
        return Objects.nonNull(entity.getTipo())
                ? new TipoUsoFacturaConverter(entity.getTipo(), expandLevel - 1) : null;
    }

    public void setTipo(TipoUsoFacturaConverter tipo) {
        entity.setTipo(tipo.getEntity());
    }

    public EstadoConverter getEstado() {
        return Objects.nonNull(entity.getEstado())
                ? new EstadoConverter(entity.getEstado(), expandLevel - 1) : null;
    }

    public void setEstado(EstadoConverter estado) {
        entity.setEstado(estado.getEntity());
    }

    public String getComentarioEstatus() {
        return entity.getComentarioEstatus();
    }

    public void setComentarioEstatus(String comentarioEstatus) {
        entity.setComentarioEstatus(comentarioEstatus);
    }

    public int getOrdenEstimulo() {
        return entity.getOrdenEstimulo();
    }

    public void setOrdenEstimulo(int ordenEstimulo) {
        entity.setOrdenEstimulo(ordenEstimulo);
    }

    public BancoConverter getBanco() {
        return expandLevel > 0 && Objects.nonNull(entity.getBanco())
                ? new BancoConverter(entity.getBanco(), expandLevel - 1) : null;
    }

    public String getClabe() {
        return entity.getClabe();
    }

    public void setClabe(String clabe) {
        entity.setClabe(clabe);
    }

    public BigDecimal getSuperficie() {
        return entity.getSuperficiePredio();
    }

    public void setSuperficie(BigDecimal superficie) {
        entity.setSuperficiePredio(superficie);
    }

    public String getContrato() {
        return entity.getContrato();
    }

    @XmlElement
    public TipoPrecioConverter getTipoPrecio() {
        return expandLevel > 0 && Objects.nonNull(entity.getTipoPrecio())
                ? new TipoPrecioConverter(entity.getTipoPrecio(), expandLevel - 1) : null;
    }

    @XmlElement
    public Calendar getFechaEstimulo() {
        return entity.getFechaEstimulo();
    }

}
