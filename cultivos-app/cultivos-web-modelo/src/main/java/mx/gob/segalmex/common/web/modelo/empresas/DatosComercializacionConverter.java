/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.empresas;

import java.math.BigDecimal;
import java.util.Objects;
import mx.gob.segalmex.pluss.modelo.empresas.DatosComercializacion;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.EstadoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.MunicipioConverter;

/**
 *
 * @author jurgen
 */
public class DatosComercializacionConverter extends AbstractEntidadConverter {

    private final DatosComercializacion entity;

    public DatosComercializacionConverter() {
        entity = new DatosComercializacion();
        expandLevel = 1;
    }

    public DatosComercializacionConverter(DatosComercializacion entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad DatosComercializacion no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public DatosComercializacion getEntity() {
        return entity;
    }

    public int getAnio() {
        return entity.getAnio();
    }

    public void setAnio(int anio) {
        entity.setAnio(anio);
    }

    public BigDecimal getCantidad() {
        return entity.getCantidad();
    }

    public void setCantidad(BigDecimal cantidad) {
        entity.setCantidad(cantidad);
    }

    public String getTipo() {
        return entity.getTipo();
    }

    public void setTipo(String tipo) {
        entity.setTipo(tipo);
    }

    public String getNombreActor() {
        return entity.getNombreActor();
    }

    public void setNombreActor(String nombreActor) {
        entity.setNombreActor(nombreActor);
    }

    public EstadoConverter getEstado() {
        return Objects.nonNull(entity.getEstado()) && expandLevel > 0
                ? new EstadoConverter(entity.getEstado(), expandLevel - 1) : null;
    }

    public void setEstado(EstadoConverter estado) {
        entity.setEstado(estado.getEntity());
    }

    public MunicipioConverter getMunicipio() {
        return Objects.nonNull(entity.getMunicipio()) && expandLevel > 0
                ? new MunicipioConverter(entity.getMunicipio(), expandLevel - 1) : null;
    }

    public void setMunicipio(MunicipioConverter municipio) {
        entity.setMunicipio(municipio.getEntity());
    }
}
