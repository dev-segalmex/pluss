package mx.gob.segalmex.granos.web.modelo.catalogos;

import java.util.Objects;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.GrupoIndigena;

/**
 *
 * @author oscar
 */
public class GrupoIndigenaConverter extends AbstractCatalogoConverter {

    private final GrupoIndigena entity;

    public GrupoIndigenaConverter() {
        this.entity = new GrupoIndigena();
        this.expandLevel = 1;
    }

    public GrupoIndigenaConverter(GrupoIndigena entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad GrupoIndigena no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public GrupoIndigena getEntity() {
        return this.entity;
    }
}

