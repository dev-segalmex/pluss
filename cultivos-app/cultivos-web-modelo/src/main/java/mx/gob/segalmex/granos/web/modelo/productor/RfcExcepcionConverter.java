/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.productor;

import java.util.Calendar;
import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.archivos.ArchivoConverter;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.seguridad.UsuarioConverter;
import mx.gob.segalmex.pluss.modelo.productor.RfcExcepcion;

/**
 *
 * @author oscar
 */
@XmlRootElement(name = "rfc-excepcion")
public class RfcExcepcionConverter extends AbstractEntidadConverter {

    private final RfcExcepcion entity;

    public RfcExcepcionConverter() {
        entity = new RfcExcepcion();
    }

    public RfcExcepcionConverter(RfcExcepcion entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad RfcExcepcion no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public RfcExcepcion getEntity() {
        return entity;
    }

    public String getRfc() {
        return entity.getRfc();
    }

    public void setRfc(String rfc) {
        entity.setRfc(rfc);
    }

    public UsuarioConverter getUsuarioRegistra() {
        return expandLevel > 0 && Objects.nonNull(entity.getUsuarioRegistra())
                ? new UsuarioConverter(getEntity().getUsuarioRegistra(), expandLevel - 1) : null;
    }

    public void setUsuarioRegistra(UsuarioConverter usuarioRegistra) {
        entity.setUsuarioRegistra(usuarioRegistra.getEntity());
    }

    public boolean isActivo() {
        return entity.isActivo();
    }

    public void setActivo(boolean activo) {
        entity.setActivo(activo);
    }

    public ArchivoConverter getConstancia() {
        return expandLevel > 0 && Objects.nonNull(entity.getConstancia())
                ? new ArchivoConverter(getEntity().getConstancia(), expandLevel - 1) : null;
    }

    public UsuarioConverter getUsuarioBaja() {
        return expandLevel > 0 && Objects.nonNull(entity.getUsuarioBaja())
                ? new UsuarioConverter(getEntity().getUsuarioBaja(), expandLevel - 1) : null;
    }

    public Calendar getFechaBaja() {
        return entity.getFechaBaja();
    }

}
