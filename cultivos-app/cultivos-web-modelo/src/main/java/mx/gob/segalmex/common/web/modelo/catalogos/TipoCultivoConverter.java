/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.catalogos;

import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;

/**
 *
 * @author cuecho
 */
@XmlRootElement(name = "tipo-cultivo")
public class TipoCultivoConverter extends AbstractCatalogoConverter {

    /**
     * Entidad a representar en este converter.
     */
    private final TipoCultivo entity;

    public TipoCultivoConverter() {
        entity = new TipoCultivo();
        expandLevel = 1;
    }

    public TipoCultivoConverter(TipoCultivo entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad TipoCultivo no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    /**
     * Recupera la entidad representada.
     *
     * @return
     */
    @Override
    public TipoCultivo getEntity() {
        return this.entity;
    }

    @XmlElement
    public String getAbreviatura() {
        return entity.getAbreviatura();
    }
}
