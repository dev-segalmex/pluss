/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.factura;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import mx.gob.segalmex.pluss.modelo.factura.ComprobanteRecepcion;
import mx.gob.segalmex.pluss.modelo.factura.Factura;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import mx.gob.segalmex.pluss.modelo.factura.UsoFactura;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.archivos.ArchivoConverter;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CicloAgricolaConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CultivoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.EstadoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.TipoCultivoConverter;
import mx.gob.segalmex.common.web.modelo.productor.ProductorCicloConverter;
import mx.gob.segalmex.granos.web.modelo.inscripcion.ComentarioInscripcionConverter;
import mx.gob.segalmex.common.web.modelo.productor.ProductorConverter;
import mx.gob.segalmex.common.web.modelo.seguridad.UsuarioConverter;
import mx.gob.segalmex.pluss.modelo.historico.HistoricoRegistro;
import mx.gob.segalmex.pluss.modelo.inscripcion.ComentarioInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.productor.Asociado;
import mx.gob.segalmex.pluss.modelo.productor.Productor;
import mx.gob.segalmex.granos.web.modelo.catalogos.RegionConverter;
import mx.gob.segalmex.granos.web.modelo.catalogos.SucursalConverter;
import mx.gob.segalmex.granos.web.modelo.contrato.DatoCapturadoConverter;
import mx.gob.segalmex.granos.web.modelo.contrato.EstatusInscripcionConverter;
import mx.gob.segalmex.granos.web.modelo.historico.HistoricoRegistroConverter;
import mx.gob.segalmex.granos.web.modelo.productor.AsociadoConverter;

/**
 *
 * @author ismael
 */
public class InscripcionFacturaConverter extends AbstractEntidadConverter {

    /**
     * La entidad que respalda el converter.
     */
    private final InscripcionFactura entity;

    /**
     * El tipo de factura si es emitida o recibida.
     */
    private String tipo;

    /**
     * Los datos capturados de esta factura.
     */
    private List<DatoCapturado> datos = new ArrayList<>();

    /**
     * El asociado de esta factura.
     */
    private Asociado asociado;

    /**
     * El historial de esta factura.
     */
    private List<HistoricoRegistro> historial = new ArrayList<>();

    /**
     * Los comentarios de esta factura.
     */
    private List<ComentarioInscripcion> comentarios = new ArrayList<>();

    /**
     * Los archivos de esta factura.
     */
    private List<Archivo> archivos = new ArrayList<>();

    /**
     * Las facturas de productor en el caso de que sea una factura global.
     */
    private List<String> facturasUuid = new ArrayList<>();

    /**
     * Las facturas relacionadas con la factura global.
     */
    private List<Factura> relacionadas = new ArrayList<>();

    /**
     * El estatus de inscripción del contrato.
     */
    private EstatusInscripcion estatusInscripcionContrato;

    /**
     * El uso factura relacionado con la isncripcion.
     */
    private List<UsoFactura> usos;

    /**
     * Productor relacionado con la inscripcion.
     */
    private Productor prodcutor;

    public InscripcionFacturaConverter() {
        entity = new InscripcionFactura();
        expandLevel = 1;
    }

    public InscripcionFacturaConverter(InscripcionFactura entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Cfdi no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public InscripcionFactura getEntity() {
        return entity;
    }

    public String getFolio() {
        return entity.getFolio();
    }

    public void setFolio(String folio) {
        entity.setFolio(folio);
    }

    public String getUuid() {
        return entity.getUuid();
    }

    public void setUuid(String uuid) {
        entity.setUuid(uuid);
    }

    @XmlElement
    public EstatusInscripcionConverter getEstatus() {
        return Objects.nonNull(entity.getEstatus()) && expandLevel > 0
                ? new EstatusInscripcionConverter(entity.getEstatus(), expandLevel - 1) : null;
    }

    @XmlElement
    public UsuarioConverter getUsuarioRegistra() {
        return expandLevel > 0 && Objects.nonNull(entity.getUsuarioRegistra())
                ? new UsuarioConverter(getEntity().getUsuarioRegistra(), expandLevel - 1) : null;
    }

    @XmlElement
    public UsuarioConverter getUsuarioValidador() {
        return expandLevel > 0 && Objects.nonNull(entity.getUsuarioValidador())
                ? new UsuarioConverter(entity.getUsuarioValidador(), expandLevel - 1) : null;
    }

    @XmlElement
    public UsuarioConverter getUsuarioAsignado() {
        return expandLevel > 0 && Objects.nonNull(entity.getUsuarioAsignado())
                ? new UsuarioConverter(entity.getUsuarioAsignado(), expandLevel - 1) : null;
    }

    public CultivoConverter getCultivo() {
        return expandLevel > 0 && Objects.nonNull(getEntity().getCultivo())
                ? new CultivoConverter(getEntity().getCultivo(), expandLevel - 1) : null;
    }

    public void setCultivo(CultivoConverter cultivo) {
        getEntity().setCultivo(cultivo.getEntity());
    }

    public CicloAgricolaConverter getCiclo() {
        return expandLevel > 0 && Objects.nonNull(getEntity().getCiclo())
                ? new CicloAgricolaConverter(getEntity().getCiclo(), expandLevel - 1) : null;
    }

    public void setCiclo(CicloAgricolaConverter ciclo) {
        getEntity().setCiclo(ciclo.getEntity());
    }

    public BigDecimal getCantidad() {
        return entity.getCantidad();
    }

    public void setCantidad(BigDecimal cantidad) {
        entity.setCantidad(cantidad);
    }

    public int getNumeroComprobantes() {
        return entity.getNumeroComprobantes();
    }

    public void setNumeroComprobantes(int numeroComprobantes) {
        entity.setNumeroComprobantes(numeroComprobantes);
    }

    public BigDecimal getCantidadComprobada() {
        return entity.getCantidadComprobada();
    }

    public void setCantidadComprobada(BigDecimal cantidadComprobada) {
        entity.setCantidadComprobada(cantidadComprobada);
    }

    public CfdiConverter getCfdi() {
        return expandLevel > 0 && Objects.nonNull(entity.getCfdi())
                ? new CfdiConverter(entity.getCfdi(), expandLevel - 1) : null;
    }

    public void setCfdi(CfdiConverter cfdi) {
        entity.setCfdi(cfdi.getEntity());
    }

    @XmlElement
    public BigDecimal getToneladasDisponiblesFactura() {
        return Objects.nonNull(entity.getFactura()) ? entity.getFactura().getToneladasDisponibles() : null;
    }

    @XmlElement
    public boolean isRegistrada() {
        return Objects.nonNull(entity.getFactura());
    }

    @XmlElement
    public FacturaConverter getFactura() {
        return Objects.nonNull(entity.getFactura()) ? new FacturaConverter(entity.getFactura(), expandLevel - 1) : null;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @XmlElement
    public SucursalConverter getSucursal() {
        return expandLevel > 0 && Objects.nonNull(entity.getSucursal())
                ? new SucursalConverter(entity.getSucursal(), expandLevel - 1) : null;
    }

    @XmlElement
    public List<DatoCapturadoConverter> getDatos() {
        if (Objects.isNull(datos) || datos.isEmpty()) {
            return null;
        }
        return CollectionConverter.convert(DatoCapturadoConverter.class, datos, expandLevel - 1);
    }

    public void setDatos(List<DatoCapturado> datos) {
        this.datos = datos;
    }

    public AsociadoConverter getAsociado() {
        return Objects.nonNull(asociado) ? new AsociadoConverter(asociado, expandLevel - 1) : null;
    }

    public void setAsociado(AsociadoConverter asociado) {
        this.asociado = asociado.getEntity();
    }

    @XmlElement
    public List<HistoricoRegistroConverter> getHistorial() {
        if (Objects.isNull(historial) || historial.isEmpty()) {
            return null;
        }
        return CollectionConverter.convert(HistoricoRegistroConverter.class, historial, expandLevel - 1);
    }

    public void setHistorial(List<HistoricoRegistro> historial) {
        this.historial = historial;
    }

    @XmlElement
    public Boolean getValida() {
        return entity.getValida();
    }

    @XmlElement
    public Boolean getDuplicada() {
        return entity.getDuplicada();
    }

    public String getContrato() {
        return entity.getContrato();
    }

    public void setContrato(String contrato) {
        entity.setContrato(contrato);
    }

    @XmlElement
    public List<ComentarioInscripcionConverter> getComentarios() {
        if (Objects.isNull(comentarios) || comentarios.isEmpty()) {
            return null;
        }
        return CollectionConverter.convert(ComentarioInscripcionConverter.class, comentarios, expandLevel - 1);
    }

    public void setComentarios(List<ComentarioInscripcion> comentarios) {
        this.comentarios = comentarios;
    }

    @XmlElement
    public ProductorCicloConverter getProductorCiclo() {
        return expandLevel > 0 && Objects.nonNull(entity.getProductorCiclo())
                ? new ProductorCicloConverter(entity.getProductorCiclo(), expandLevel - 1) : null;
    }

    public void setProductorCiclo(ProductorCicloConverter productorCiclo) {
        entity.setProductorCiclo(productorCiclo.getEntity());
    }

    @XmlElement
    public List<ArchivoConverter> getArchivos() {
        if (Objects.isNull(archivos) || archivos.isEmpty()) {
            return null;
        }
        return CollectionConverter.convert(ArchivoConverter.class, archivos, expandLevel - 1);
    }

    public void setArchivos(List<Archivo> archivos) {
        this.archivos = archivos;
    }

    public List<String> getFacturasUuid() {
        return facturasUuid;
    }

    public void setFacturasUuid(List<String> facturasUuid) {
        this.facturasUuid = facturasUuid;
    }

    @XmlElement
    public List<FacturaConverter> getRelacionadas() {
        if (Objects.isNull(relacionadas) || relacionadas.isEmpty()) {
            return null;
        }
        return CollectionConverter.convert(FacturaConverter.class, relacionadas, expandLevel - 1);
    }

    public void setRelacionadas(List<Factura> relacionadas) {
        this.relacionadas = relacionadas;
    }

    @XmlElement
    public String getTipoFactura() {
        return entity.getTipoFactura();
    }

    @XmlElement
    public EstatusInscripcionConverter getEstatusInscripcionContrato() {
        return Objects.nonNull(estatusInscripcionContrato)
                ? new EstatusInscripcionConverter(estatusInscripcionContrato, expandLevel - 1)
                : null;
    }

    public void setEstatusInscripcionContrato(EstatusInscripcion estatusInscripcionContrato) {
        this.estatusInscripcionContrato = estatusInscripcionContrato;
    }

    public String getComentarioEstatus() {
        return entity.getComentarioEstatus();
    }

    public void setComentarioEstatus(String comentarioEstatus) {
        entity.setComentarioEstatus(comentarioEstatus);
    }

    @XmlElement
    public List<UsoFacturaConverter> getUsos() {
        if (Objects.isNull(usos) || usos.isEmpty()) {
            return null;
        }
        return CollectionConverter.convert(UsoFacturaConverter.class, usos, expandLevel - 1);
    }

    public void setUsos(List<UsoFactura> usos) {
        this.usos = usos;
    }

    @XmlElement
    public ProductorConverter getDatosProductor() {
        return Objects.nonNull(prodcutor)
                ? new ProductorConverter(prodcutor, expandLevel - 1) : null;
    }

    public void setDatosProductor(Productor productor) {
        this.prodcutor = productor;
    }

    public TipoCultivoConverter getTipoCultivo() {
        return expandLevel > 0 && Objects.nonNull(entity.getTipoCultivo())
                ? new TipoCultivoConverter(getEntity().getTipoCultivo(), expandLevel - 1) : null;
    }

    public void setTipoCultivo(TipoCultivoConverter tipoCultivo) {
        entity.setTipoCultivo(tipoCultivo.getEntity());
    }

    public EstadoConverter getEstado() {
        return expandLevel > 0 && Objects.nonNull(entity.getEstado())
                ? new EstadoConverter(getEntity().getEstado(), expandLevel - 1) : null;
    }

    public void setEstado(EstadoConverter estado) {
        entity.setEstado(estado.getEntity());
    }

    @XmlElement
    public Calendar getFechaPago() {
        return entity.getFechaPago();
    }

    public void setFechaPago(Calendar fechaPago) {
        entity.setFechaPago(fechaPago);
    }

    public String getClaveArchivos() {
        return entity.getClaveArchivos();
    }

    public void setClaveArchivos(String claveArchivos) {
        entity.setClaveArchivos(claveArchivos);
    }

    @XmlElement
    public List<ComprobanteRecepcionConverter> getComprobantes() {
        return expandLevel > 0 && Objects.nonNull(entity.getComprobantes())
                ? CollectionConverter.convert(ComprobanteRecepcionConverter.class,
                        entity.getComprobantes(), expandLevel - 1) : null;
    }

    public void setComprobantes(List<ComprobanteRecepcionConverter> comprobantes) {
        if (Objects.nonNull(comprobantes)) {
            entity.setComprobantes(CollectionConverter.unconvert(
                    ComprobanteRecepcion.class, comprobantes));
        }
    }

    public BigDecimal getCantidadComprobantePago() {
        return entity.getCantidadComprobantePago();
    }

    public void setCantidadComprobantePago(BigDecimal cantidadComprobantePago) {
        entity.setCantidadComprobantePago(cantidadComprobantePago);
    }

    @XmlElement
    public BigDecimal getPrecioTonelada() {
        return entity.getPrecioTonelada();
    }

    @XmlElement
    public BigDecimal getPrecioToneladaReal() {
        return entity.getPrecioToneladaReal();
    }

    public void setPrecioToneladaReal(BigDecimal precioToneladaReal) {
        entity.setPrecioToneladaReal(precioToneladaReal);
    }

    @XmlElement
    public BigDecimal getPrecioAjuste() {
        return entity.getPrecioAjuste();
    }

    public void setPrecioAjuste(BigDecimal precioAjuste) {
        entity.setPrecioAjuste(precioAjuste);
    }

    @XmlElement
    public BigDecimal getFacturables() {
        return entity.getFacturables();
    }

    @XmlElement
    public RegionConverter getRegion() {
        return expandLevel > 0 && Objects.nonNull(entity.getRegion())
                ? new RegionConverter(getEntity().getRegion(), expandLevel - 1) : null;
    }

    public void setRegion(RegionConverter region) {
        entity.setRegion(region.getEntity());
    }

}
