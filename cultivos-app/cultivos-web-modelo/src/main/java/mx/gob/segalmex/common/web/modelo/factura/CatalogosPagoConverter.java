/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.factura;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CicloAgricolaConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CultivoConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;

/**
 *
 * @author oscar
 */
public class CatalogosPagoConverter {

    private List<Cultivo> cultivos = new ArrayList<>();

    private List<CicloAgricola> ciclos = new ArrayList<>();

    @XmlElement
    public List<CultivoConverter> getCultivos() {
        return CollectionConverter.convert(CultivoConverter.class, cultivos, 1);
    }

    public void setCultivos(List<Cultivo> cultivos) {
        this.cultivos = cultivos;
    }

    @XmlElement
    public List<CicloAgricolaConverter> getCiclos() {
        return CollectionConverter.convert(CicloAgricolaConverter.class, ciclos, 1);
    }

    public void setCiclos(List<CicloAgricola> ciclos) {
        this.ciclos = ciclos;
    }

}
