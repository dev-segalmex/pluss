/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.productor;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoIar;
import mx.gob.segalmex.pluss.modelo.productor.CoberturaSeleccionada;
import mx.gob.segalmex.granos.web.modelo.catalogos.TipoIarConverter;

/**
 *
 * @author oscar
 */
@XmlRootElement(name = "cobertura-seleccionada")
public class CoberturaSeleccionadaConverter extends AbstractEntidadConverter {

    private final CoberturaSeleccionada entity;

    public CoberturaSeleccionadaConverter() {
        entity = new CoberturaSeleccionada();
    }

    public CoberturaSeleccionadaConverter(CoberturaSeleccionada entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad CoberturaSeleccionada no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public CoberturaSeleccionada getEntity() {
        return entity;
    }

    public EntidadCoberturaConverter getEntidadCobertura() {
        return Objects.nonNull(entity.getEntidadCobertura())
                ? new EntidadCoberturaConverter(entity.getEntidadCobertura(), expandLevel - 1) : null;
    }

    public void setEntidadCobertura(EntidadCoberturaConverter entidadCobertura) {
        entity.setEntidadCobertura(entidadCobertura.getEntity());
    }

    public TipoIarConverter getTipoIar() {
        return Objects.nonNull(entity.getTipoIar()) && expandLevel > 0
                ? new TipoIarConverter(entity.getTipoIar(), expandLevel - 1) : null;
    }

    public void setTipoIar(TipoIar tipoIar) {
        entity.setTipoIar(tipoIar);
    }

    public Integer getCantidad() {
        return entity.getCantidad();
    }

    public void setCantidad(Integer cantidad) {
        entity.setCantidad(cantidad);
    }

}
