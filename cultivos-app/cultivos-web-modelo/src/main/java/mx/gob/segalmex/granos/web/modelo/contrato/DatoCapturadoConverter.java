/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.contrato;

import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "dato-capturado")
public class DatoCapturadoConverter extends AbstractEntidadConverter {

    private final DatoCapturado entity;

    public DatoCapturadoConverter() {
        this.entity = new DatoCapturado();
    }

    public DatoCapturadoConverter(DatoCapturado entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad DatoCapturado no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public DatoCapturado getEntity() {
        return entity;
    }

    @XmlElement
    public String getNombre() {
        return entity.getNombre();
    }

    public String getClave() {
        return entity.getClave();
    }

    public void setClave(String clave) {
        entity.setClave(clave);
    }

    public String getValor() {
        return entity.getValor();
    }

    public void setValor(String valor) {
        entity.setValor(valor);
    }

    public Boolean getCorrecto() {
        return entity.getCorrecto();
    }

    public void setCorrecto(Boolean correcto) {
        entity.setCorrecto(correcto);
    }

    public String getComentario() {
        return entity.getComentario();
    }

    public void setComentario(String comentario) {
        entity.setComentario(comentario);
    }

    public String getValorTexto() {
        return entity.getValorTexto();
    }

    public void setValorTexto(String valorTexto) {
        entity.setValorTexto(valorTexto);
    }

    @XmlElement
    public String getGrupo() {
        return entity.getGrupo();
    }

    @XmlElement
    public String getTipo() {
        return entity.getTipo();
    }

}
