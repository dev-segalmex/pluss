/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.productor;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.EstadoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.LocalidadConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.MunicipioConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.Localidad;
import mx.gob.segalmex.pluss.modelo.productor.Domicilio;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "domicilio")
public class DomicilioConverter extends AbstractEntidadConverter {

    private Domicilio entity;

    public DomicilioConverter() {
        entity = new Domicilio();
    }

    public DomicilioConverter(Domicilio entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Domicilio no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public Domicilio getEntity() {
        return entity;
    }

    public String getCalle() {
        return entity.getCalle();
    }

    public void setCalle(String calle) {
        entity.setCalle(calle);
    }

    public String getNumeroInterior() {
        return entity.getNumeroInterior();
    }

    public void setNumeroInterior(String numeroInterior) {
        entity.setNumeroInterior(numeroInterior);
    }

    public String getNumeroExterior() {
        return entity.getNumeroExterior();
    }

    public void setNumeroExterior(String numeroExterior) {
        entity.setNumeroExterior(numeroExterior);
    }

    public String getClaveLocalidad() {
        return entity.getClaveLocalidad();
    }

    public void setClaveLocalidad(String claveLocalidad) {
        entity.setClaveLocalidad(claveLocalidad);
    }

    public String getLocalidad() {
        return entity.getLocalidad();
    }

    public void setLocalidad(String localidad) {
        entity.setLocalidad(localidad);
    }

    public LocalidadConverter getCatalogoLocalidad() {
        return Objects.nonNull(entity.getCatalogoLocalidad()) && expandLevel > 0
                ? new LocalidadConverter(entity.getCatalogoLocalidad(), expandLevel - 1) : null;
    }

    public void setCatalogoLocalidad(LocalidadConverter catalogoLocalidad) {
        entity.setCatalogoLocalidad(catalogoLocalidad.getEntity());
    }

    public EstadoConverter getEstado() {
        return Objects.nonNull(entity.getEstado()) && expandLevel > 0
                ? new EstadoConverter(entity.getEstado(), expandLevel - 1) : null;
    }

    public void setEstado(EstadoConverter estado) {
        entity.setEstado(estado.getEntity());
    }

    public MunicipioConverter getMunicipio() {
        return Objects.nonNull(entity.getMunicipio()) && expandLevel > 0
                ? new MunicipioConverter(entity.getMunicipio(), expandLevel - 1) : null;
    }

    public void setMunicipio(MunicipioConverter municipio) {
        entity.setMunicipio(municipio.getEntity());
    }

    public String getCodigoPostal() {
        return entity.getCodigoPostal();
    }

    public void setCodigoPostal(String codigoPostal) {
        entity.setCodigoPostal(codigoPostal);
    }

}
