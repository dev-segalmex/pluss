/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.catalogos;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.Region;
import mx.gob.segalmex.common.web.modelo.catalogos.CicloAgricolaConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.EstadoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.TipoCultivoConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;

/**
 *
 * @author erikcamacho
 */
@XmlRootElement(name = "region")
public class RegionConverter extends AbstractCatalogoConverter {

    /**
     * La entidad de donde se obtendrá la información.
     */
    private final Region entity;

    public RegionConverter() {
        entity = new Region();
        expandLevel = 1;
    }

    public RegionConverter(Region entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Región no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public Region getEntity() {
        return this.entity;
    }

    public CicloAgricolaConverter getCiclo() {
        return expandLevel > 0 && Objects.nonNull(getEntity().getCiclo())
                ? new CicloAgricolaConverter(getEntity().getCiclo(), expandLevel - 1) : null;
    }

    public void setCiclo(CicloAgricola ciclo) {
        entity.setCiclo(ciclo);
    }

    public TipoCultivoConverter getTipoCultivo() {
        return expandLevel > 0 && Objects.nonNull(getEntity().getTipoCultivo())
                ? new TipoCultivoConverter(getEntity().getTipoCultivo(), expandLevel - 1) : null;
    }

    public void setTipoCultivo(TipoCultivo tipoCultivo) {
        entity.setTipoCultivo(tipoCultivo);
    }

    public EstadoConverter getEstado() {
        return expandLevel > 0 && Objects.nonNull(getEntity().getEstado())
                ? new EstadoConverter(getEntity().getEstado(), expandLevel - 1) : null;
    }

    public void setEstado(Estado estado) {
        entity.setEstado(estado);
    }

    public EstadoConverter getEstadoEstimulo() {
        return expandLevel > 0 && Objects.nonNull(getEntity().getEstadoEstimulo())
                ? new EstadoConverter(getEntity().getEstadoEstimulo(), expandLevel - 1) : null;
    }

    public void setEstadoEstimulo(Estado estadoEstimulo) {
        entity.setEstadoEstimulo(estadoEstimulo);
    }

}
