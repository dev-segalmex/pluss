/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.empresas;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import mx.gob.segalmex.pluss.modelo.empresas.DatosComercializacion;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroEmpresa;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroSucursal;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.archivos.ArchivoConverter;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CicloAgricolaConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CultivoConverter;
import mx.gob.segalmex.common.web.modelo.seguridad.UsuarioConverter;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.granos.web.modelo.catalogos.TipoSucursalConverter;
import mx.gob.segalmex.granos.web.modelo.contrato.DatoCapturadoConverter;
import mx.gob.segalmex.granos.web.modelo.contrato.EstatusInscripcionConverter;
import mx.gob.segalmex.granos.web.modelo.productor.DomicilioConverter;
import mx.gob.segalmex.granos.web.modelo.productor.TipoPosesionConverter;

/**
 *
 * @author ismael
 */
public class RegistroEmpresaConverter extends AbstractEntidadConverter {

    /**
     * La entidad que respalda el converter.
     */
    private final RegistroEmpresa entity;

    private List<Archivo> archivos = new ArrayList<>();

    private String url;

    private List<DatoCapturado> datos = new ArrayList<>();

    public RegistroEmpresaConverter() {
        entity = new RegistroEmpresa();
        expandLevel = 1;
    }

    public RegistroEmpresaConverter(RegistroEmpresa entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad RegistroEmpresa no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public RegistroEmpresa getEntity() {
        return entity;
    }

    @XmlElement
    public String getRfc() {
        return entity.getRfc();
    }

    @XmlElement
    public String getUuid() {
        return entity.getUuid();
    }

    public String getNombre() {
        return entity.getNombre();
    }

    public void setNombre(String nomnbre) {
        entity.setNombre(nomnbre);
    }

    @XmlElement
    public Boolean getEditable() {
        return entity.isEditable();
    }

    public List<RegistroSucursalConverter> getSucursales() {
        if (Objects.isNull(entity.getSucursales()) || entity.getSucursales().isEmpty()) {
            return null;
        }
        return CollectionConverter.convert(RegistroSucursalConverter.class, entity.getSucursales(), expandLevel - 1);
    }

    public void setSucursales(List<RegistroSucursalConverter> sucursales) {
        entity.setSucursales(CollectionConverter.unconvert(RegistroSucursal.class, sucursales));
    }

    public List<DatosComercializacionConverter> getDatosComercializacion() {
        if (Objects.isNull(entity.getDatosComercializacion()) || entity.getDatosComercializacion().isEmpty()) {
            return null;
        }
        Collections.sort(entity.getDatosComercializacion(), (DatosComercializacion o1, DatosComercializacion o2) -> o1.getOrden() - o2.getOrden());
        return CollectionConverter.convert(DatosComercializacionConverter.class, entity.getDatosComercializacion(), expandLevel - 1);
    }

    public void setDatosComercializacion(List<DatosComercializacionConverter> datosComercializacion) {
        entity.setDatosComercializacion(CollectionConverter.unconvert(DatosComercializacion.class, datosComercializacion));
    }

    @XmlElement
    public List<ArchivoConverter> getArchivos() {
        if (Objects.isNull(archivos) || archivos.isEmpty()) {
            return null;
        }
        return CollectionConverter.convert(ArchivoConverter.class, archivos, expandLevel - 1);
    }

    public void setArchivos(List<Archivo> archivos) {
        this.archivos = archivos;
    }

    public String getNombreResponsable() {
        return entity.getNombreResponsable();
    }

    public void setNombreResponsable(String nomnbreResponsable) {
        entity.setNombreResponsable(nomnbreResponsable);
    }

    public String getTelefonoOficina() {
        return entity.getTelefonoOficina();
    }

    public void setTelefonoOficina(String telefonoOficina) {
        entity.setTelefonoOficina(telefonoOficina);
    }

    public DomicilioConverter getDomicilio() {
        return expandLevel > 0 && Objects.nonNull(entity.getDomicilio())
                ? new DomicilioConverter(entity.getDomicilio(), expandLevel - 1) : null;
    }

    public void setDomicilio(DomicilioConverter domicilio) {
        entity.setDomicilio(domicilio.getEntity());
    }

    public String getNombreEnlace() {
        return entity.getNombreEnlace();
    }

    public void setNombreEnlace(String nombreEnlace) {
        entity.setNombreEnlace(nombreEnlace);
    }

    public String getNumeroCelularEnlace() {
        return entity.getNumeroCelularEnlace();
    }

    public void setNumeroCelularEnlace(String numeroCelularEnlace) {
        entity.setNumeroCelularEnlace(numeroCelularEnlace);
    }

    public String getCorreoEnlace() {
        return entity.getCorreoEnlace();
    }

    public void setCorreoEnlace(String correoEnlace) {
        entity.setCorreoEnlace(correoEnlace);
    }

    public Boolean getExisteEquipoAnalisis() {
        return entity.isExisteEquipoAnalisis();
    }

    public void setExisteEquipoAnalisis(Boolean existeEquipoAnalisis) {
        entity.setExisteEquipoAnalisis(existeEquipoAnalisis);
    }

    public Boolean getAplicaNormasCalidad() {
        return entity.isAplicaNormasCalidad();
    }

    public void setAplicaNormasCalidad(Boolean aplicaNormasCalidad) {
        entity.setAplicaNormasCalidad(aplicaNormasCalidad);
    }

    public TipoPosesionConverter getTipoPosesion() {
        return expandLevel > 0 && Objects.nonNull(entity.getTipoPosesion())
                ? new TipoPosesionConverter(entity.getTipoPosesion(), expandLevel - 1) : null;
    }

    public void setTipoPosesion(TipoPosesionConverter tipoPosesion) {
        entity.setTipoPosesion(tipoPosesion.getEntity());
    }

    public String getTipoAlmacenamiento() {
        return entity.getTipoAlmacenamiento();
    }

    public void setTipoAlmacenamiento(String tipoAlmacenamiento) {
        entity.setTipoAlmacenamiento(tipoAlmacenamiento);
    }

    public BigDecimal getLatitud() {
        return entity.getLatitud();
    }

    public void setLatitud(BigDecimal latitud) {
        entity.setLatitud(latitud);
    }

    public BigDecimal getLongitud() {
        return entity.getLongitud();
    }

    public void setLongitud(BigDecimal longitud) {
        entity.setLongitud(longitud);
    }

    public int getNumeroAlmacenes() {
        return entity.getNumeroAlmacenes();
    }

    public void setNumeroAlmacenes(int numeroAlmacenes) {
        entity.setNumeroAlmacenes(numeroAlmacenes);
    }

    public BigDecimal getCapacidad() {
        return entity.getCapacidad();
    }

    public void setCapacidad(BigDecimal capacidad) {
        entity.setCapacidad(capacidad);
    }

    public BigDecimal getVolumenActual() {
        return entity.getVolumenActual();
    }

    public void setVolumenActual(BigDecimal volumenActual) {
        entity.setVolumenActual(volumenActual);
    }

    public BigDecimal getSuperficie() {
        return entity.getSuperficie();
    }

    public void setSuperficie(BigDecimal superficie) {
        entity.setSuperficie(superficie);
    }

    public Boolean getHabilitaAlmacenadora() {
        return entity.isHabilitaAlmacenadora();
    }

    public void setHabilitaAlmacenadora(Boolean habilitaAlmacenadora) {
        entity.setHabilitaAlmacenadora(habilitaAlmacenadora);
    }

    public String getNombreAlmacenadora() {
        return entity.getNombreAlmacenadora();
    }

    public void setNombreAlmacenadora(String nombreAlmacenadora) {
        entity.setNombreAlmacenadora(nombreAlmacenadora);
    }

    public TipoSucursalConverter getTipoSucursal() {
        return Objects.nonNull(entity.getTipoSucursal()) && expandLevel > 0
                ? new TipoSucursalConverter(entity.getTipoSucursal(), expandLevel - 1) : null;
    }

    public void setTipoSucursal(TipoSucursalConverter tipoSucursal) {
        entity.setTipoSucursal(tipoSucursal.getEntity());
    }

    public CicloAgricolaConverter getCiclo() {
        return expandLevel > 0 && Objects.nonNull(entity.getCiclo())
                ? new CicloAgricolaConverter(entity.getCiclo(), expandLevel - 1) : null;
    }

    public void setCiclo(CicloAgricolaConverter ciclo) {
        entity.setCiclo(ciclo.getEntity());
    }

    public CultivoConverter getCultivo() {
        return expandLevel > 0 && Objects.nonNull(entity.getCultivo())
                ? new CultivoConverter(entity.getCultivo(), expandLevel - 1) : null;
    }

    public void setCultivo(CultivoConverter cultivo) {
        entity.setCultivo(cultivo.getEntity());
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public EstatusInscripcionConverter getEstatus() {
        return expandLevel > 0 && Objects.nonNull(entity.getEstatus())
                ? new EstatusInscripcionConverter(entity.getEstatus(), expandLevel - 1) : null;
    }

    public void setEstatus(EstatusInscripcionConverter estatus) {
        entity.setEstatus(estatus.getEntity());
    }

    @XmlElement
    public List<DatoCapturadoConverter> getDatos() {
        if (Objects.isNull(datos) || datos.isEmpty()) {
            return null;
        }
        return CollectionConverter.convert(DatoCapturadoConverter.class, datos, expandLevel - 1);
    }

    public void setDatos(List<DatoCapturado> datos) {
        this.datos = datos;
    }

    public UsuarioConverter getUsuarioValidador() {
        return expandLevel > 0 && Objects.nonNull(entity.getUsuarioValidador())
                ? new UsuarioConverter(entity.getUsuarioValidador(), expandLevel - 1) : null;
    }

    public void setUsuarioValidador(UsuarioConverter usuario) {
        entity.setUsuarioValidador(usuario.getEntity());
    }

    public String getExtensionAdicional() {
        return entity.getExtensionAdicional();
    }

    public void setExtensionAdicional(String extensionAdicional) {
        entity.setExtensionAdicional(extensionAdicional);
    }
}
