/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.productor;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.pluss.modelo.productor.SocioDatosProductor;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "socio-datos-productor")
public class SocioDatosProductorConverter  extends AbstractEntidadConverter {

    private final SocioDatosProductor entity;

    public SocioDatosProductorConverter() {
        entity = new SocioDatosProductor();
    }

    public SocioDatosProductorConverter(SocioDatosProductor entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad SocioDatosProductor no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public SocioDatosProductor getEntity() {
        return entity;
    }

    public String getCurp() {
        return entity.getCurp();
    }

    public void setCurp(String curp) {
        entity.setCurp(curp);
    }
}
