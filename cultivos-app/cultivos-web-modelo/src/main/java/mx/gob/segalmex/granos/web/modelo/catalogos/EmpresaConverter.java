/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.catalogos;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.Empresa;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "empresa")
public class EmpresaConverter extends AbstractCatalogoConverter {

    private final Empresa entity;

    public EmpresaConverter() {
        this.entity = new Empresa();
        this.expandLevel = 1;
    }

    public EmpresaConverter(Empresa entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Empresa no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public Empresa getEntity() {
        return this.entity;
    }

    public String getRfc() {
        return entity.getRfc();
    }

    public void setRfc(String rfc) {
        entity.setRfc(rfc);
    }
}
