/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.catalogos;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoEmpresa;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "tipo-empresa")
public class TipoEmpresaConverter extends AbstractCatalogoConverter {

    private final TipoEmpresa entity;

    public TipoEmpresaConverter() {
        this.entity = new TipoEmpresa();
        this.expandLevel = 1;
    }

    public TipoEmpresaConverter(TipoEmpresa entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad TipoPersona no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public TipoEmpresa getEntity() {
        return this.entity;
    }
}
