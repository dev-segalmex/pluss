/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.factura;

import java.util.Objects;
import mx.gob.segalmex.pluss.modelo.pago.RequerimientoUsoFactura;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.productor.ProductorConverter;

/**
 *
 * @author jurgen
 */
public class RequerimientoUsoFacturaConverter extends AbstractEntidadConverter {

    private final RequerimientoUsoFactura entity;

    public RequerimientoUsoFacturaConverter() {
        entity = new RequerimientoUsoFactura();
        expandLevel = 1;
    }

    public RequerimientoUsoFacturaConverter(RequerimientoUsoFactura entity, int expandLevel) {
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public RequerimientoUsoFactura getEntity() {
        return entity;
    }

    public UsoFacturaConverter getUsoFactura() {
        return Objects.nonNull(entity.getUsoFactura()) ? new UsoFacturaConverter(entity.getUsoFactura(), expandLevel - 1) : null;
    }

    public void setUsoFactura(UsoFacturaConverter usoFactura) {
        entity.setUsoFactura(usoFactura.getEntity());
    }

    public ProductorConverter getProductor() {
        return Objects.nonNull(entity.getUsoFactura().getProductor()) ? new ProductorConverter(entity.getUsoFactura().getProductor(), expandLevel - 1) : null;
    }

    public void setProductor(ProductorConverter productor) {
        entity.getUsoFactura().setProductor(productor.getEntity());
    }

}
