/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.modelo.contrato;

import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.granos.web.modelo.contrato.DatoCapturadoConverter;

/**
 *
 * @author erikcam
 */
@XmlRootElement(name = "validacion")
public class ValidacionConverter {

    private List<DatoCapturadoConverter> datosCapturados;

    private Boolean solicitado;

    private Boolean fechaPago;

    @XmlElement
    public List<DatoCapturadoConverter> getDatosCapturados() {
        return datosCapturados;
    }

    public void setDatosCapturados(List<DatoCapturadoConverter> datosCapturados) {
        this.datosCapturados = datosCapturados;
    }

    @XmlElement
    public Boolean getSolicitado() {
        return solicitado;
    }

    public void setSolicitado(Boolean solicitado) {
        this.solicitado = solicitado;
    }

    @XmlElement
    public Boolean getFechaPago() {
        return fechaPago;
    }

    public void setFechaPago(Boolean fechaPago) {
        this.fechaPago = fechaPago;
    }
}
