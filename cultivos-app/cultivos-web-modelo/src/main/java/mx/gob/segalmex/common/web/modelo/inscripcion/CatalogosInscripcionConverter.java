/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.inscripcion;

import mx.gob.segalmex.common.web.modelo.catalogos.CultivoConverter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.pluss.modelo.catalogos.Banco;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.Municipio;
import mx.gob.segalmex.pluss.modelo.catalogos.Pais;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import mx.gob.segalmex.pluss.modelo.catalogos.Parentesco;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoDocumento;
import mx.gob.segalmex.pluss.modelo.personas.Sexo;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersona;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.BancoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CicloAgricolaConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.EstadoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.MunicipioConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.PaisConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.ParametroConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.ParentescoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.SexoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.TipoCultivoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.TipoDocumentoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.TipoPersonaConverter;
import mx.gob.segalmex.common.web.modelo.factura.TipoUsoFacturaConverter;
import mx.gob.segalmex.common.web.modelo.productor.EntidadCoberturaConverter;
import mx.gob.segalmex.common.web.modelo.seguridad.UsuarioConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.GrupoIndigena;
import mx.gob.segalmex.pluss.modelo.catalogos.NivelEstudio;
import mx.gob.segalmex.pluss.modelo.catalogos.Region;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoEmpresa;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoIar;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoPrecio;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoSucursal;
import mx.gob.segalmex.pluss.modelo.productor.EntidadCobertura;
import mx.gob.segalmex.pluss.modelo.productor.RegimenHidrico;
import mx.gob.segalmex.pluss.modelo.productor.TipoPosesion;
import mx.gob.segalmex.granos.web.modelo.catalogos.GrupoIndigenaConverter;
import mx.gob.segalmex.granos.web.modelo.catalogos.NivelEstudioConverter;
import mx.gob.segalmex.granos.web.modelo.catalogos.RegionConverter;
import mx.gob.segalmex.granos.web.modelo.catalogos.SucursalConverter;
import mx.gob.segalmex.granos.web.modelo.catalogos.TipoEmpresaConverter;
import mx.gob.segalmex.granos.web.modelo.catalogos.TipoIarConverter;
import mx.gob.segalmex.granos.web.modelo.catalogos.TipoPrecioConverter;
import mx.gob.segalmex.granos.web.modelo.catalogos.TipoSucursalConverter;
import mx.gob.segalmex.granos.web.modelo.contrato.InscripcionContratoConverter;
import mx.gob.segalmex.granos.web.modelo.productor.RegimenHidricoConverter;
import mx.gob.segalmex.granos.web.modelo.productor.TipoPosesionConverter;
import mx.gob.segalmex.pluss.modelo.contrato.TipoCompradorCobertura;
import mx.gob.segalmex.pluss.cultivos.modelo.contrato.TipoCompradorCoberturaConverter;
import mx.gob.segalmex.pluss.modelo.contrato.TipoOperacionCobertura;
import mx.gob.segalmex.pluss.cultivos.modelo.contrato.TipoOperacionCoberturaConverter;
import mx.gob.segalmex.pluss.modelo.factura.TipoUsoFactura;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "catalogos")
public class CatalogosInscripcionConverter {

    private List<Cultivo> cultivos = new ArrayList<>();

    private List<CicloAgricola> ciclos = new ArrayList<>();

    private List<TipoCultivo> tipos = new ArrayList<>();

    private List<Estado> estados = new ArrayList<>();

    private List<Municipio> municipios = new ArrayList<>();

    private List<Estado> estadosPredio = new ArrayList<>();

    private List<TipoEmpresa> tiposEmpresaComprador = new ArrayList<>();

    private List<TipoEmpresa> tiposEmpresaVendedor = new ArrayList<>();

    private List<TipoPrecio> tiposPrecio = new ArrayList<>();

    private List<TipoPersona> tiposPersona = new ArrayList<>();

    private List<Pais> paises = new ArrayList<>();

    private List<RegimenHidrico> regimenes = new ArrayList<>();

    private List<Banco> bancos = new ArrayList<>();

    private List<TipoPosesion> tiposPosesion = new ArrayList<>();

    private List<TipoDocumento> tiposDocumentoIdentificacion = new ArrayList<>();

    private List<TipoDocumento> tiposDocumentoPropia = new ArrayList<>();

    private List<TipoDocumento> tiposDocumentoRentada = new ArrayList<>();

    private List<Sexo> sexos = new ArrayList<>();

    private List<Parentesco> parentescos = new ArrayList<>();

    private Sucursal sucursal = null;

    private List<EntidadCobertura> coberturas = new ArrayList<>();

    private List<TipoSucursal> tiposSucursal = new ArrayList<>();

    private CicloAgricola cicloSeleccionado = null;

    private List<TipoCompradorCobertura> tiposComprador = new ArrayList<>();

    private List<TipoOperacionCobertura> tiposOperacion = new ArrayList<>();

    private List<Parametro> bases = new ArrayList<>();

    private List<Parametro> precios = new ArrayList<>();

    private List<Parametro> rendimientos = new ArrayList<>();

    private Parametro volumenPredio = new Parametro();

    private Parametro superficiePredio = new Parametro();

    private Parametro cantidadContratada = new Parametro();

    private Parametro superficieTemporal = new Parametro();

    private List<Usuario> usuarios = new ArrayList<>();

    private List<Sucursal> sucursales = new ArrayList<>();

    private List<Estado> estadosCultivo = new ArrayList<>();

    private List<GrupoIndigena> gruposIndigenas = new ArrayList<>();

    private List<NivelEstudio> nivelEstudios = new ArrayList<>();

    private boolean aplicaContrato = false;

    private boolean aplicaPersonaMoral = false;

    private boolean contratoRequerido = false;

    private boolean predioSolicitado = false;

    private boolean archivosPredio = false;

    private List<Parametro> archivosRiego = new ArrayList<>();

    private List<Parametro> archivosSiembra = new ArrayList<>();

    private List<InscripcionContratoConverter> contratos = new ArrayList<>();

    private Parametro registroCerrado;

    private CicloAgricola cicloActivo = new CicloAgricola();

    private boolean aplicaAjusteFactura = false;

    private boolean aplicaReciboEtiqueta = false;

    private List<Region> regiones = new ArrayList<>();

    private Parametro fechaComprobantes;

    private List<TipoIar> tiposIAr;

    private List<Parametro> tiposRequierenIar = new ArrayList<>();

    private List<TipoUsoFactura> tiposUsoFactura = new ArrayList<>();

    @XmlElement
    public List<CultivoConverter> getCultivos() {
        return Objects.nonNull(cultivos) && !cultivos.isEmpty()
                ? CollectionConverter.convert(CultivoConverter.class, this.cultivos, 1) : null;
    }

    public void setCultivos(List<Cultivo> cultivos) {
        this.cultivos = cultivos;
    }

    @XmlElement
    public List<CicloAgricolaConverter> getCiclos() {
        return Objects.nonNull(ciclos) && !ciclos.isEmpty()
                ? CollectionConverter.convert(CicloAgricolaConverter.class, this.ciclos, 1) : null;
    }

    public void setCiclos(List<CicloAgricola> ciclos) {
        this.ciclos = ciclos;
    }

    @XmlElement
    public List<TipoCultivoConverter> getTipos() {
        return Objects.nonNull(tipos) && !tipos.isEmpty()
                ? CollectionConverter.convert(TipoCultivoConverter.class, this.tipos, 1) : null;
    }

    public void setTipos(List<TipoCultivo> tipos) {
        this.tipos = tipos;
    }

    @XmlElement
    public List<EstadoConverter> getEstados() {
        return Objects.nonNull(estados) && !estados.isEmpty()
                ? CollectionConverter.convert(EstadoConverter.class, this.estados, 1) : null;
    }

    public void setEstados(List<Estado> estados) {
        this.estados = estados;
    }

    @XmlElement
    public List<MunicipioConverter> getMunicipios() {
        return Objects.nonNull(municipios) && !municipios.isEmpty()
                ? CollectionConverter.convert(MunicipioConverter.class, this.municipios, 1) : null;
    }

    public void setMunicipios(List<Municipio> municipios) {
        this.municipios = municipios;
    }

    @XmlElement
    public SucursalConverter getSucursal() {
        return Objects.nonNull(sucursal) ? new SucursalConverter(sucursal, 2) : null;
    }

    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

    @XmlElement
    public List<EstadoConverter> getEstadosPredio() {
        return Objects.nonNull(estadosPredio) && !estadosPredio.isEmpty()
                ? CollectionConverter.convert(EstadoConverter.class, estadosPredio, 1) : null;
    }

    public void setEstadosPredio(List<Estado> estadosPredio) {
        this.estadosPredio = estadosPredio;
    }

    @XmlElement
    public List<TipoEmpresaConverter> getTiposEmpresaComprador() {
        return Objects.nonNull(tiposEmpresaComprador) && !tiposEmpresaComprador.isEmpty()
                ? CollectionConverter.convert(TipoEmpresaConverter.class, tiposEmpresaComprador, 1)
                : null;
    }

    public void setTiposEmpresaComprador(List<TipoEmpresa> tiposEmpresaComprador) {
        this.tiposEmpresaComprador = tiposEmpresaComprador;
    }

    @XmlElement
    public List<TipoEmpresaConverter> getTiposEmpresaVendedor() {
        return Objects.nonNull(tiposEmpresaVendedor) && !tiposEmpresaVendedor.isEmpty()
                ? CollectionConverter.convert(TipoEmpresaConverter.class, tiposEmpresaVendedor, 1)
                : null;
    }

    public void setTiposEmpresaVendedor(List<TipoEmpresa> tiposEmpresaVendedor) {
        this.tiposEmpresaVendedor = tiposEmpresaVendedor;
    }

    @XmlElement
    public List<TipoPrecioConverter> getTiposPrecio() {
        return Objects.nonNull(tiposPrecio) && !tiposPrecio.isEmpty()
                ? CollectionConverter.convert(TipoPrecioConverter.class, tiposPrecio, 1) : null;
    }

    public void setTiposPrecio(List<TipoPrecio> tiposPrecio) {
        this.tiposPrecio = tiposPrecio;
    }

    public List<TipoPersonaConverter> getTiposPersona() {
        return Objects.nonNull(tiposPersona) && !tiposPersona.isEmpty()
                ? CollectionConverter.convert(TipoPersonaConverter.class, tiposPersona, 1) : null;
    }

    public void setTiposPersona(List<TipoPersona> tiposPersona) {
        this.tiposPersona = tiposPersona;
    }

    @XmlElement
    public List<PaisConverter> getPaises() {
        return Objects.nonNull(paises) && !paises.isEmpty()
                ? CollectionConverter.convert(PaisConverter.class, paises, 1) : null;
    }

    public void setPaises(List<Pais> paises) {
        this.paises = paises;
    }

    @XmlElement
    public List<RegimenHidricoConverter> getRegimenes() {
        return Objects.nonNull(regimenes) && !regimenes.isEmpty()
                ? CollectionConverter.convert(RegimenHidricoConverter.class, regimenes, 1) : null;
    }

    public void setRegimenes(List<RegimenHidrico> regimenes) {
        this.regimenes = regimenes;
    }

    @XmlElement
    public List<BancoConverter> getBancos() {
        return Objects.nonNull(bancos) && !bancos.isEmpty()
                ? CollectionConverter.convert(BancoConverter.class, bancos, 1) : null;
    }

    public void setBancos(List<Banco> bancos) {
        this.bancos = bancos;
    }

    @XmlElement
    public List<TipoPosesionConverter> getTiposPosesion() {
        return Objects.nonNull(tiposPosesion) && !tiposPosesion.isEmpty()
                ? CollectionConverter.convert(TipoPosesionConverter.class, tiposPosesion, 1) : null;
    }

    public void setTiposPosesion(List<TipoPosesion> tiposPosesion) {
        this.tiposPosesion = tiposPosesion;
    }

    @XmlElement
    public List<TipoDocumentoConverter> getTiposDocumentoIdentificacion() {
        return Objects.nonNull(tiposDocumentoIdentificacion)
                && !tiposDocumentoIdentificacion.isEmpty() ? CollectionConverter
                .convert(TipoDocumentoConverter.class, tiposDocumentoIdentificacion, 1)
                : null;
    }

    public void setTiposDocumentoIdentificacion(List<TipoDocumento> tiposDocumentoIdentificacion) {
        this.tiposDocumentoIdentificacion = tiposDocumentoIdentificacion;
    }

    @XmlElement
    public List<TipoDocumentoConverter> getTiposDocumentoPropia() {
        return Objects.nonNull(tiposDocumentoPropia) && !tiposDocumentoPropia.isEmpty()
                ? CollectionConverter.convert(TipoDocumentoConverter.class, tiposDocumentoPropia, 1) : null;
    }

    public void setTiposDocumentoPropia(List<TipoDocumento> tiposDocumentoPropia) {
        this.tiposDocumentoPropia = tiposDocumentoPropia;
    }

    @XmlElement
    public List<TipoDocumentoConverter> getTiposDocumentoRentada() {
        return Objects.nonNull(tiposDocumentoRentada) && !tiposDocumentoRentada.isEmpty()
                ? CollectionConverter.convert(TipoDocumentoConverter.class, tiposDocumentoRentada, 1) : null;
    }

    public void setTiposDocumentoRentada(List<TipoDocumento> tiposDocumentoRentada) {
        this.tiposDocumentoRentada = tiposDocumentoRentada;
    }

    @XmlElement
    public List<SexoConverter> getSexos() {
        return Objects.nonNull(sexos) && !sexos.isEmpty()
                ? CollectionConverter.convert(SexoConverter.class, sexos, 1) : null;
    }

    public void setSexos(List<Sexo> sexos) {
        this.sexos = sexos;
    }

    @XmlElement
    public List<ParentescoConverter> getParentescos() {
        return Objects.nonNull(parentescos) && !parentescos.isEmpty()
                ? CollectionConverter.convert(ParentescoConverter.class, parentescos, 1) : null;
    }

    public void setParentescos(List<Parentesco> parentescos) {
        this.parentescos = parentescos;
    }

    @XmlElement
    public List<EntidadCoberturaConverter> getCoberturas() {
        return Objects.nonNull(coberturas) && !coberturas.isEmpty()
                ? CollectionConverter.convert(EntidadCoberturaConverter.class, coberturas, 1) : null;
    }

    public void setCoberturas(List<EntidadCobertura> coberturas) {
        this.coberturas = coberturas;
    }

    @XmlElement
    public List<TipoSucursalConverter> getTiposSucursal() {
        return Objects.nonNull(tiposSucursal) && !tiposSucursal.isEmpty()
                ? CollectionConverter.convert(TipoSucursalConverter.class, tiposSucursal, 1) : null;
    }

    public void setTiposSucursal(List<TipoSucursal> tiposSucursal) {
        this.tiposSucursal = tiposSucursal;
    }

    @XmlElement
    public CicloAgricolaConverter getCicloSeleccionado() {
        return Objects.nonNull(cicloSeleccionado) ? new CicloAgricolaConverter(cicloSeleccionado, 1) : null;
    }

    public void setCicloSeleccionado(CicloAgricola cicloSeleccionado) {
        this.cicloSeleccionado = cicloSeleccionado;
    }

    @XmlElement
    public List<TipoCompradorCoberturaConverter> getTiposComprador() {
        return Objects.nonNull(tiposComprador) && !tiposComprador.isEmpty()
                ? CollectionConverter.convert(TipoCompradorCoberturaConverter.class, tiposComprador, 1) : null;
    }

    public void setTiposComprador(List<TipoCompradorCobertura> tiposComprador) {
        this.tiposComprador = tiposComprador;
    }

    @XmlElement
    public List<TipoOperacionCoberturaConverter> getTiposOperacion() {
        return Objects.nonNull(tiposOperacion) && !tiposOperacion.isEmpty()
                ? CollectionConverter.convert(TipoOperacionCoberturaConverter.class, tiposOperacion, 1) : null;
    }

    public void setTiposOperacion(List<TipoOperacionCobertura> tiposOperacion) {
        this.tiposOperacion = tiposOperacion;
    }

    @XmlElement
    public List<ParametroConverter> getBases() {
        return Objects.nonNull(bases) && !bases.isEmpty()
                ? CollectionConverter.convert(ParametroConverter.class, bases, 1) : null;
    }

    public void setBases(List<Parametro> bases) {
        this.bases = bases;
    }

    @XmlElement
    public List<ParametroConverter> getPrecios() {
        return Objects.nonNull(precios) && !precios.isEmpty()
                ? CollectionConverter.convert(ParametroConverter.class, precios, 1) : null;
    }

    public void setPrecios(List<Parametro> precios) {
        this.precios = precios;
    }

    @XmlElement
    public List<ParametroConverter> getRendimientos() {
        return Objects.nonNull(rendimientos) && !rendimientos.isEmpty()
                ? CollectionConverter.convert(ParametroConverter.class, rendimientos, 1) : null;
    }

    public void setRendimientos(List<Parametro> rendimientos) {
        this.rendimientos = rendimientos;
    }

    @XmlElement
    public ParametroConverter getVolumenPredio() {
        return Objects.nonNull(volumenPredio) ? new ParametroConverter(volumenPredio, 1) : null;
    }

    public void setVolumenPredio(Parametro volumenPredio) {
        this.volumenPredio = volumenPredio;
    }

    @XmlElement
    public ParametroConverter getSuperficiePredio() {
        return Objects.nonNull(superficiePredio) ? new ParametroConverter(superficiePredio, 1) : null;
    }

    public void setSuperficiePredio(Parametro superficiePredio) {
        this.superficiePredio = superficiePredio;
    }

    @XmlElement
    public ParametroConverter getCantidadContratada() {
        return Objects.nonNull(cantidadContratada) ? new ParametroConverter(cantidadContratada, 1) : null;
    }

    public void setCantidadContratada(Parametro cantidadContratada) {
        this.cantidadContratada = cantidadContratada;
    }

    public ParametroConverter getSuperficieTemporal() {
        return Objects.nonNull(superficieTemporal) ? new ParametroConverter(superficieTemporal, 1) : null;
    }

    public void setSuperficieTemporal(Parametro superficieTemporal) {
        this.superficieTemporal = superficieTemporal;
    }

    @XmlElement
    public List<UsuarioConverter> getUsuarios() {
        return Objects.nonNull(usuarios) && !usuarios.isEmpty()
                ? CollectionConverter.convert(UsuarioConverter.class, usuarios, 1) : null;
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    @XmlElement
    public List<SucursalConverter> getSucursales() {
        if (Objects.nonNull(sucursales) && !sucursales.isEmpty()) {
            List<SucursalConverter> converters = CollectionConverter.convert(SucursalConverter.class, sucursales, 1);
            Collections.sort(converters, (SucursalConverter s1, SucursalConverter s2) -> s1.getEtiqueta().compareTo(s2.getEtiqueta()));
            return converters;
        }
        return null;
    }

    public void setSucursales(List<Sucursal> sucursales) {
        this.sucursales = sucursales;
    }

    @XmlElement
    public List<EstadoConverter> getEstadosCultivo() {
        return Objects.nonNull(estadosCultivo) && !estadosCultivo.isEmpty()
                ? CollectionConverter.convert(EstadoConverter.class, this.estadosCultivo, 1) : null;
    }

    public void setEstadosCultivo(List<Estado> estadosCultivo) {
        this.estadosCultivo = estadosCultivo;
    }

    @XmlElement
    public List<GrupoIndigenaConverter> getGruposIndigenas() {
        return Objects.nonNull(gruposIndigenas) && !gruposIndigenas.isEmpty()
                ? CollectionConverter.convert(GrupoIndigenaConverter.class, this.gruposIndigenas, 1) : null;
    }

    public void setGruposIndigenas(List<GrupoIndigena> gruposIndigenas) {
        this.gruposIndigenas = gruposIndigenas;
    }

    @XmlElement
    public List<TipoIarConverter> getTiposIar() {
        return Objects.nonNull(tiposIAr) && !tiposIAr.isEmpty()
                ? CollectionConverter.convert(TipoIarConverter.class, this.tiposIAr, 1) : null;
    }

    public void setTiposIar(List<TipoIar> tiposIar) {
        this.tiposIAr = tiposIar;
    }

    @XmlElement
    public List<NivelEstudioConverter> getNivelEstudios() {
        return Objects.nonNull(nivelEstudios) && !nivelEstudios.isEmpty()
                ? CollectionConverter.convert(NivelEstudioConverter.class, this.nivelEstudios, 1) : null;
    }

    public void setNivelEstudios(List<NivelEstudio> nivelEstudios) {
        this.nivelEstudios = nivelEstudios;
    }

    @XmlElement
    public boolean isAplicaContrato() {
        return aplicaContrato;
    }

    public void setAplicaContrato(boolean aplicaContrato) {
        this.aplicaContrato = aplicaContrato;
    }

    @XmlElement
    public boolean isAplicaPersonaMoral() {
        return aplicaPersonaMoral;
    }

    public void setAplicaPersonaMoral(boolean aplicaPersonaMoral) {
        this.aplicaPersonaMoral = aplicaPersonaMoral;
    }

    @XmlElement
    public boolean isContratoRequerido() {
        return contratoRequerido;
    }

    public void setContratoRequerido(boolean contratoRequerido) {
        this.contratoRequerido = contratoRequerido;
    }

    @XmlElement
    public boolean isPredioSolicitado() {
        return predioSolicitado;
    }

    public void setPredioSolicitado(boolean predioSolicitado) {
        this.predioSolicitado = predioSolicitado;
    }

    @XmlElement
    public List<InscripcionContratoConverter> getContratos() {
        return contratos;
    }

    public void setContratos(List<InscripcionContratoConverter> contratos) {
        this.contratos = contratos;
    }

    @XmlElement
    public boolean isArchivosPredio() {
        return archivosPredio;
    }

    public void setArchivosPredio(boolean archivosPredio) {
        this.archivosPredio = archivosPredio;
    }

    public List<ParametroConverter> getArchivosRiego() {
        return Objects.nonNull(archivosRiego)
                ? CollectionConverter.convert(ParametroConverter.class, archivosRiego, 1) : null;
    }

    public void setArchivosRiego(List<Parametro> archivosRiego) {
        this.archivosRiego = archivosRiego;
    }

    public List<ParametroConverter> getArchivosSiembra() {
        return Objects.nonNull(archivosSiembra)
                ? CollectionConverter.convert(ParametroConverter.class, archivosSiembra, 1) : null;
    }

    public void setArchivosSiembra(List<Parametro> archivosSiembra) {
        this.archivosSiembra = archivosSiembra;
    }

    @XmlElement
    public CicloAgricolaConverter getCicloActivo() {
        return Objects.nonNull(cicloActivo) ? new CicloAgricolaConverter(cicloActivo, 1) : null;
    }

    public void setCicloActivo(CicloAgricola cicloActivo) {
        this.cicloActivo = cicloActivo;
    }

    @XmlElement
    public ParametroConverter getRegistroCerrado() {
        return Objects.nonNull(registroCerrado) ? new ParametroConverter(registroCerrado, 1) : null;
    }

    public void setRegistroCerrado(Parametro registroCerrado) {
        this.registroCerrado = registroCerrado;
    }

    @XmlElement
    public boolean isAplicaAjusteFactura() {
        return aplicaAjusteFactura;
    }

    public void setAplicaAjusteFactura(boolean aplicaAjusteFactura) {
        this.aplicaAjusteFactura = aplicaAjusteFactura;
    }

    @XmlElement
    public boolean isAplicaReciboEtiqueta() {
        return aplicaReciboEtiqueta;
    }

    public void setAplicaReciboEtiqueta(boolean aplicaReciboEtiqueta) {
        this.aplicaReciboEtiqueta = aplicaReciboEtiqueta;
    }

    @XmlElement
    public List<RegionConverter> getRegiones() {
        return Objects.nonNull(regiones)
                ? CollectionConverter.convert(RegionConverter.class, regiones, 1) : null;
    }

    public void setRegiones(List<Region> regiones) {
        this.regiones = regiones;
    }

    @XmlElement
    public ParametroConverter getFechaComprobantes() {
        return Objects.nonNull(fechaComprobantes) ? new ParametroConverter(fechaComprobantes, 1) : null;
    }

    public void setFechaComprobantes(Parametro fechaComprobantes) {
        this.fechaComprobantes = fechaComprobantes;
    }

    public List<ParametroConverter> getTiposRequierenIar() {
        return Objects.nonNull(tiposRequierenIar)
                ? CollectionConverter.convert(ParametroConverter.class, tiposRequierenIar, 1) : null;
    }

    public void setTiposRequierenIar(List<Parametro> tiposRequierenIar) {
        this.tiposRequierenIar = tiposRequierenIar;
    }

    @XmlElement
    public List<TipoUsoFacturaConverter> getTiposUsoFactura() {
        return Objects.nonNull(tiposUsoFactura) && !tiposUsoFactura.isEmpty()
                ? CollectionConverter.convert(TipoUsoFacturaConverter.class, this.tiposUsoFactura, 1) : null;
    }

    public void setTiposUsoFactura(List<TipoUsoFactura> tiposUsoFactura) {
        this.tiposUsoFactura = tiposUsoFactura;
    }
}
