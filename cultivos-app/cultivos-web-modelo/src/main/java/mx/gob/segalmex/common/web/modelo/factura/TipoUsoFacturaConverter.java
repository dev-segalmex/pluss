package mx.gob.segalmex.common.web.modelo.factura;

import java.math.BigDecimal;
import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;
import mx.gob.segalmex.pluss.modelo.factura.TipoUsoFactura;

/**
 *
 * @author oscar
 */
@XmlRootElement(name = "tipo-uso-factura")
public class TipoUsoFacturaConverter extends AbstractCatalogoConverter {


 private final TipoUsoFactura entity;

    public TipoUsoFacturaConverter() {
        this.entity = new TipoUsoFactura();
        this.expandLevel = 1;
    }

    public TipoUsoFacturaConverter(TipoUsoFactura entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad TipoUsoFactura no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public TipoUsoFactura getEntity() {
        return this.entity;
    }

    @XmlElement
    public BigDecimal getFactor() {
        return entity.getFactor();
    }

    public void setUuid(BigDecimal factor) {
        entity.setFactor(factor);
    }

}
