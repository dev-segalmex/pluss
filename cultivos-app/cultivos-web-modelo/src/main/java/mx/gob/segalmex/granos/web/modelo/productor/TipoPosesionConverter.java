/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.productor;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;
import mx.gob.segalmex.pluss.modelo.productor.TipoPosesion;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "tipo-posesion")
public class TipoPosesionConverter extends AbstractCatalogoConverter {

    private final TipoPosesion entity;

    public TipoPosesionConverter() {
        entity = new TipoPosesion();
        expandLevel = 1;
    }

    public TipoPosesionConverter(TipoPosesion entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad TipoPosesion no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public TipoPosesion getEntity() {
        return this.entity;
    }

}
