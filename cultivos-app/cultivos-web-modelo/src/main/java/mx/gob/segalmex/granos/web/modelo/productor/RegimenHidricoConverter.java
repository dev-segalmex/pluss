/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.productor;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;
import mx.gob.segalmex.pluss.modelo.productor.RegimenHidrico;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "regimen-hidrico")
public class RegimenHidricoConverter extends AbstractCatalogoConverter {

    private final RegimenHidrico entity;

    public RegimenHidricoConverter() {
        entity = new RegimenHidrico();
        expandLevel = 1;
    }

    public RegimenHidricoConverter(RegimenHidrico entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad RegimenHidrico no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public RegimenHidrico getEntity() {
        return this.entity;
    }

}
