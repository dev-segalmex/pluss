/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.productor;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import mx.gob.segalmex.common.web.modelo.catalogos.CicloAgricolaConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.EstadoConverter;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionAgrupada;
import mx.gob.segalmex.granos.web.modelo.contrato.EstatusInscripcionConverter;

/**
 *
 * @author oscar
 */
@XmlRootElement(name = "inscripcionAgrupada")
public class InscripcionAgrupadaConverter {

    private final InscripcionAgrupada entity;

    private final int expandLevel;

    public InscripcionAgrupadaConverter(InscripcionAgrupada entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad InscripcionAgrupada no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    public InscripcionAgrupadaConverter() {
        entity = new InscripcionAgrupada();
        expandLevel = 2;
    }

    @XmlTransient
    public InscripcionAgrupada getEntity() {
        return entity;
    }

    public int getExpandLevel() {
        return expandLevel;
    }

    public CicloAgricolaConverter getCiclo() {
        return getExpandLevel() > 0 && getEntity().getCiclo() != null
                ? new CicloAgricolaConverter(getEntity().getCiclo(), getExpandLevel() - 1) : null;
    }

    public void setCiclo(CicloAgricolaConverter ciclo) {
        getEntity().setCiclo(ciclo.getEntity());
    }

    public EstatusInscripcionConverter getEstatus() {
        return getExpandLevel() > 0 && getEntity().getEstatus() != null
                ? new EstatusInscripcionConverter(getEntity().getEstatus(), getExpandLevel() - 1) : null;
    }

    public void setEstatus(EstatusInscripcionConverter estatus) {
        getEntity().setEstatus(estatus.getEntity());
    }

    public EstadoConverter getEstado() {
        return getExpandLevel() > 0 && getEntity().getEstado() != null
                ? new EstadoConverter(getEntity().getEstado(), getExpandLevel() - 1) : null;
    }

    public void setEstado(EstadoConverter estado) {
        getEntity().setEstado(estado.getEntity());
    }

    public Integer getTotal() {
        return getEntity().getTotal();
    }

    public void setTotal(Integer total) {
        getEntity().setTotal(total);
    }

    public static List<InscripcionAgrupadaConverter> convert(Collection<InscripcionAgrupada> registros, int expandLevel) {
        List<InscripcionAgrupadaConverter> converted = new LinkedList<>();

        registros.stream().forEach((registro) -> {
            converted.add(new InscripcionAgrupadaConverter(registro, expandLevel));
        });

        return converted;
    }
}
