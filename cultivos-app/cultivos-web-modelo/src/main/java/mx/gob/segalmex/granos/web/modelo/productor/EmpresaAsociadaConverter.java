/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.productor;

import java.util.Calendar;
import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.pluss.modelo.productor.EmpresaAsociada;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "empresa-asociada")
public class EmpresaAsociadaConverter extends AbstractEntidadConverter {

    private final EmpresaAsociada entity;

    public EmpresaAsociadaConverter() {
        entity = new EmpresaAsociada();
    }

    public EmpresaAsociadaConverter(EmpresaAsociada entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad EmpresaAsociada no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public EmpresaAsociada getEntity() {
        return entity;
    }

    public String getRfc() {
        return entity.getRfc();
    }

    public void setRfc(String rfc) {
        entity.setRfc(rfc);
    }

    public String getNombre() {
        return entity.getNombre();
    }

    public void setNombre(String nombre) {
        entity.setNombre(nombre);
    }

    public Calendar getFechaCancelacion(){
        return entity.getFechaCancelacion();
    }

    public void setFechaCanccelacion(Calendar fechaCancelacion){
        entity.setFechaCancelacion(fechaCancelacion);
    }

    @XmlElement
    public int getOrden(){
        return entity.getOrden();
    }
}
