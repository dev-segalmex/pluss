/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.productor;

import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CicloAgricolaConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CultivoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.ParentescoConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoPrecio;
import mx.gob.segalmex.pluss.modelo.productor.CoberturaSeleccionada;
import mx.gob.segalmex.pluss.modelo.productor.PreRegistroProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioPreRegistro;
import mx.gob.segalmex.granos.web.modelo.catalogos.TipoPrecioConverter;
import mx.gob.segalmex.granos.web.modelo.productor.PredioPreRegistroConverter;

/**
 *
 * @author jurgen
 */
public class PreRegistroProductorConverter extends AbstractEntidadConverter {

    private final PreRegistroProductor entity;

    public PreRegistroProductorConverter() {
        this.entity = new PreRegistroProductor();
        this.expandLevel = 1;
    }

    public PreRegistroProductorConverter(PreRegistroProductor entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad pre registro productor no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public PreRegistroProductor getEntity() {
        return this.entity;
    }

    public String getUuid() {
        return entity.getUuid();
    }

    public void setUuid(String uuid) {
        entity.setUuid(uuid);
    }

    public String getFolio() {
        return entity.getFolio();
    }

    public void setFolio(String folio) {
        entity.setFolio(folio);
    }

    public String getCurp() {
        return entity.getCurp();
    }

    public void setCurp(String curp) {
        entity.setCurp(curp);
    }

    public String getNombre() {
        return entity.getNombre();
    }

    public void setNombre(String nombre) {
        entity.setNombre(nombre);
    }

    public String getPrimerApellido() {
        return entity.getPrimerApellido();
    }

    public void setPrimerApellido(String primerApellido) {
        entity.setPrimerApellido(primerApellido);
    }

    public String getSegundoApellido() {
        return entity.getSegundoApellido();
    }

    public void setSegundoApellido(String segundoApellido) {
        entity.setSegundoApellido(segundoApellido);
    }

    public String getRfc() {
        return entity.getRfc();
    }

    public void setRfc(String rfc) {
        entity.setRfc(rfc);
    }

    public CicloAgricolaConverter getCiclo() {
        return Objects.nonNull(entity.getCiclo()) && expandLevel > 0
                ? new CicloAgricolaConverter(entity.getCiclo(), expandLevel - 1) : null;
    }

    public CultivoConverter getCultivo() {
        return Objects.nonNull(entity.getCultivo()) && expandLevel > 0
                ? new CultivoConverter(entity.getCultivo(), expandLevel - 1) : null;
    }

    public List<PredioPreRegistroConverter> getPredios() {
        return expandLevel > 0 && Objects.nonNull(entity.getPredios())
                ? CollectionConverter.convert(PredioPreRegistroConverter.class, entity.getPredios(),
                        expandLevel - 1) : null;
    }

    public void setPredios(List<PredioPreRegistroConverter> predios) {
        if (Objects.nonNull(predios)) {
            entity.setPredios(CollectionConverter.unconvert(PredioPreRegistro.class, predios));
        }
    }

    public String getTelefono() {
        return entity.getTelefono();
    }

    public void setTelefono(String telefono) {
        entity.setTelefono(telefono);
    }

    public String getCorreoElectronico() {
        return entity.getCorreoElectronico();
    }

    public void setCorreoElectronico(String correoElectronico) {
        entity.setCorreoElectronico(correoElectronico);
    }

    public Boolean getFirmoContrato() {
        return entity.getFirmoContrato();
    }

    public void setFirmoContrato(Boolean firmoContrato) {
        entity.setFirmoContrato(firmoContrato);
    }

    public String getEtiquetaTipoCultivo() {
        return entity.getEtiquetaTipoCultivo();
    }

    public void setEtiquetaTipoCultivo(String etiquetaTipoCultivo) {
        entity.setEtiquetaTipoCultivo(etiquetaTipoCultivo);
    }

    public int getPrecioAbierto() {
        return entity.getPrecioAbierto();
    }

    public void setPrecioAbierto(int precioAbierto) {
        entity.setPrecioAbierto(precioAbierto);
    }

    public int getPrecioCerradoDolares() {
        return entity.getPrecioCerradoDolares();
    }

    public void setPrecioCerradoDolares(int cerradoDolares) {
        entity.setPrecioCerradoDolares(cerradoDolares);
    }

    public int getPrecioCerradoPesos() {
        return entity.getPrecioCerradoPesos();
    }

    public void setPrecioCerradoPesos(int cerradoPesos) {
        entity.setPrecioCerradoPesos(cerradoPesos);
    }

    public int getPrecioReferencia() {
        return entity.getPrecioReferencia();
    }

    public void setPrecioReferencia(int precioReferencia) {
        entity.setPrecioReferencia(precioReferencia);
    }

    public int getPrecioAbiertoPesos() {
        return entity.getPrecioAbiertoPesos();
    }

    public void setPrecioAbiertoPesos(int precioAbiertoPesos) {
        entity.setPrecioAbiertoPesos(precioAbiertoPesos);
    }

    public int getCantidadContratos() {
        return entity.getCantidadContratos();
    }

    public void setCantidadContratos(int cantidadContratos) {
        entity.setCantidadContratos(cantidadContratos);
    }

    @XmlElement
    public List<TipoPrecioConverter> getTiposPrecio() {
        return expandLevel > 0 && Objects.nonNull(entity.getTiposPrecio())
                ? CollectionConverter.convert(TipoPrecioConverter.class, entity.getTiposPrecio(),
                        expandLevel - 1) : null;
    }

    public void setTiposPrecio(List<TipoPrecioConverter> tiposPrecio) {
        if (Objects.nonNull(tiposPrecio)) {
            entity.setTiposPrecio(CollectionConverter.unconvert(TipoPrecio.class, tiposPrecio));
        }
    }

    public List<CoberturaSeleccionadaConverter> getCoberturas() {
        if (Objects.isNull(entity.getCoberturas()) || entity.getCoberturas().isEmpty()) {
            return null;
        }
        return CollectionConverter.convert(CoberturaSeleccionadaConverter.class,
                entity.getCoberturas(), expandLevel - 1);
    }

    public void setCoberturas(List<CoberturaSeleccionadaConverter> coberturas) {
        if (Objects.nonNull(coberturas)) {
            entity.setCoberturas(CollectionConverter.unconvert(CoberturaSeleccionada.class, coberturas));
        }
    }

    public String getNombreBeneficiario() {
        return entity.getNombreBeneficiario();
    }

    public void setNombreBeneficiario(String nombreBeneficiario) {
        entity.setNombreBeneficiario(nombreBeneficiario);
    }

    public String getApellidosBeneficiario() {
        return entity.getApellidosBeneficiario();
    }

    public void setApellidosBeneficiario(String apellidosBeneficiario) {
        entity.setApellidosBeneficiario(apellidosBeneficiario);
    }

    public String getCurpBeneficiario() {
        return entity.getCurpBeneficiario();
    }

    public void setCurpBeneficiario(String curpBeneficiario) {
        entity.setCurpBeneficiario(curpBeneficiario);
    }

    public ParentescoConverter getParentesco() {
        return Objects.nonNull(entity.getParentesco()) && expandLevel > 0
                ? new ParentescoConverter(entity.getParentesco(), expandLevel - 1) : null;
    }

    public void setParentesco(ParentescoConverter parentesco) {
        entity.setParentesco(parentesco.getEntity());
    }

}
