package mx.gob.segalmex.granos.web.modelo.catalogos;

import java.util.Objects;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.NivelEstudio;

/**
 *
 * @author oscar
 */
public class NivelEstudioConverter extends AbstractCatalogoConverter {

    private final NivelEstudio entity;

    public NivelEstudioConverter() {
        this.entity = new NivelEstudio();
        this.expandLevel = 1;
    }

    public NivelEstudioConverter(NivelEstudio entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad NivelEstudio no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public NivelEstudio getEntity() {
        return this.entity;
    }

}
