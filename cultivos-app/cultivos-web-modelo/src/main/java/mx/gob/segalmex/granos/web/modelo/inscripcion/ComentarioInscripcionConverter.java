/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.inscripcion;

import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.seguridad.UsuarioConverter;
import mx.gob.segalmex.pluss.modelo.inscripcion.ComentarioInscripcion;

/**
 *
 * @author ismael
 */
public class ComentarioInscripcionConverter extends AbstractEntidadConverter {

    private final ComentarioInscripcion entity;

    public ComentarioInscripcionConverter() {
        entity = new ComentarioInscripcion();
        expandLevel = 1;
    }

    public ComentarioInscripcionConverter(ComentarioInscripcion entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad ComentarioInscripcion no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public ComentarioInscripcion getEntity() {
        return entity;
    }

    public String getTipo() {
        return entity.getTipo();
    }

    public  void setTipo(String tipo) {
        entity.setTipo(tipo);
    }

    @XmlElement
    public UsuarioConverter getUsuario() {
        return Objects.nonNull(entity.getUsuario()) && expandLevel > 0
                ? new UsuarioConverter(entity.getUsuario(), expandLevel - 1) : null;
    }

    public String getContenido() {
        return entity.getContenido();
    }

    public void setContenido(String contenido) {
        entity.setContenido(contenido);
    }
}
