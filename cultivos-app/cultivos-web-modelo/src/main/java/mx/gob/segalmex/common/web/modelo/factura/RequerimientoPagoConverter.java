/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.factura;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import mx.gob.segalmex.pluss.modelo.pago.RequerimientoPago;
import mx.gob.segalmex.pluss.modelo.pago.RequerimientoUsoFactura;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CicloAgricolaConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CultivoConverter;

/**
 *
 * @author jurgen
 */
public class RequerimientoPagoConverter extends AbstractEntidadConverter {

    private final RequerimientoPago entity;

    private List<String> usosFacturasUuid = new ArrayList<>();

    public RequerimientoPagoConverter() {
        entity = new RequerimientoPago();
        expandLevel = 1;
    }

    public RequerimientoPagoConverter(RequerimientoPago entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad RequerimientoPago no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public RequerimientoPago getEntity() {
        return entity;
    }

    @XmlElement
    public String getUuid() {
        return entity.getUuid();
    }

    @XmlElement
    public Usuario getUsuario() {
        return entity.getUsuario();
    }

    @XmlElement
    public String getEstatus() {
        return entity.getEstatus();
    }

    @XmlElement
    public String getCegap() {
        return entity.getCegap();
    }

    public void setCegap(String cegap) {
        entity.setCegap(cegap);
    }

    public List<RequerimientoUsoFacturaConverter> getRequerimientos() {
        if (Objects.isNull(entity.getRequerimientos()) || entity.getRequerimientos().isEmpty()) {
            return null;
        }
        return CollectionConverter.convert(RequerimientoUsoFacturaConverter.class, entity.getRequerimientos(), expandLevel - 1);
    }

    public void setRequerimientos(List<RequerimientoUsoFacturaConverter> requerimientos) {
        this.entity.setRequerimientos(CollectionConverter.unconvert(RequerimientoUsoFactura.class, requerimientos));
    }

    @XmlElement
    public String getLayout() {
        return entity.getLayout();
    }

    public void setLayout(String layout) {
        entity.setLayout(layout);
    }

    @XmlElement
    public String getClaveUnidadOperativa() {
        return entity.getClaveUnidadOperativa();
    }

    @XmlElement
    public String getNumeroCuenta() {
        return entity.getNumeroCuenta();
    }

    @XmlElement
    public String getClaveArea() {
        return entity.getClaveArea();
    }

    @XmlElement
    public String getClaveUsuario() {
        return entity.getClaveUsuario();
    }

    @XmlElement
    public String getPrograma() {
        return entity.getPrograma();
    }

    @XmlElement
    public String getClaveCentroAcopio() {
        return entity.getClaveCentroAcopio();
    }

    @XmlElement
    public String getMonitoreable() {
        return entity.getMonitoreable();
    }

    public List<String> getUsosFacturasUuid() {
        return usosFacturasUuid;
    }

    public void setUsosFacturasUuid(List<String> usosFacturasUuid) {
        this.usosFacturasUuid = usosFacturasUuid;
    }

    @XmlElement
    public Integer getFacturas() {
        return entity.getFacturas();
    }

    @XmlElement
    public BigDecimal getMontoTotal() {
        return entity.getMontoTotal();
    }

    @XmlElement
    public BigDecimal getToneladasTotales() {
        return entity.getToneladasTotales();
    }

    @XmlElement
    public Integer getFacturasNoPago() {
        return entity.getFacturasNoPago();
    }

    @XmlElement
    public BigDecimal getMontoTotalNoPago() {
        return entity.getMontoTotalNoPago();
    }

    @XmlElement
    public BigDecimal getToneladasNoPago() {
        return entity.getToneladasNoPago();
    }

    @XmlElement
    public CicloAgricolaConverter getCiclo() {
        return expandLevel > 0 && Objects.nonNull(getEntity().getCiclo())
                ? new CicloAgricolaConverter(getEntity().getCiclo(), expandLevel - 1) : null;
    }

    public void setCiclo(CicloAgricolaConverter ciclo) {
        entity.setCiclo(ciclo.getEntity());
    }

    @XmlElement
    public CultivoConverter getCultivo() {
        return expandLevel > 0 && Objects.nonNull(getEntity().getCultivo())
                ? new CultivoConverter(getEntity().getCultivo(), expandLevel - 1) : null;
    }

    public void setCultivo(CultivoConverter cultivo) {
        entity.setCultivo(cultivo.getEntity());
    }

    @XmlElement
    public String getReporteGenerado() {
        return entity.getReporteGenerado();
    }

    public void setReporteGenerado(String reporteGenerado) {
        entity.setReporteGenerado(reporteGenerado);
    }
}
