/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.catalogos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.EstadoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.MunicipioConverter;
import mx.gob.segalmex.common.web.modelo.seguridad.UsuarioConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "sucursal")
public class SucursalConverter extends AbstractCatalogoConverter {

    private final Sucursal entity;

    List<Usuario> usuarios = new ArrayList<>();

    public SucursalConverter() {
        this.entity = new Sucursal();
        this.expandLevel = 1;
    }

    public SucursalConverter(Sucursal entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Sucursal no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public Sucursal getEntity() {
        return this.entity;
    }

    public EmpresaConverter getEmpresa() {
        return expandLevel > 0 && Objects.nonNull(entity.getEmpresa())
                ? new EmpresaConverter(entity.getEmpresa(), expandLevel - 1) : null;
    }

    public void setEmpresa(EmpresaConverter empresa) {
        entity.setEmpresa(empresa.getEntity());
    }

    public EstadoConverter getEstado() {
        return expandLevel > 0 && getEntity().getEstado() != null
                ? new EstadoConverter(getEntity().getEstado(), expandLevel - 1) : null;
    }

    public void setEstado(EstadoConverter estado) {
        entity.setEstado(estado.getEntity());
    }

    public MunicipioConverter getMunicipio() {
        return expandLevel > 0 && getEntity().getMunicipio() != null
                ? new MunicipioConverter(getEntity().getMunicipio(), expandLevel - 1) : null;
    }

    public void setMunicipio(MunicipioConverter municipio) {
        entity.setMunicipio(municipio.getEntity());
    }

    public String getLocalidad() {
        return entity.getLocalidad();
    }

    public void setLocalidad(String localidad) {
        entity.setLocalidad(localidad);
    }

    public TipoSucursalConverter getTipo() {
        return expandLevel > 0 && getEntity().getTipo() != null
                ? new TipoSucursalConverter(getEntity().getTipo(), expandLevel - 1) : null;
    }

    public void setTipo(TipoSucursalConverter tipo) {
        entity.setTipo(tipo.getEntity());
    }

    @XmlElement
    public List<UsuarioConverter> getUsuarios() {
        if (Objects.isNull(usuarios) || usuarios.isEmpty()) {
            return null;
        }
        return CollectionConverter.convert(UsuarioConverter.class, usuarios, expandLevel - 1);

    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    @XmlElement
    public String getEtiqueta(){
        return getEntity().getEmpresa().getRfc() + " - " + getEntity().getEmpresa().getNombre();
    }
    
    public BigDecimal getLatitud() {
        return entity.getLatitud();
    }

    public void setLatitud(BigDecimal latitud) {
        entity.setLatitud(latitud);
    }
    
    public BigDecimal getLongitud() {
        return entity.getLongitud();
    }

    public void setLongitud(BigDecimal longitud) {
        entity.setLongitud(longitud);
    }
    
    public String getDireccion() {
        return entity.getDireccion();
    }

    public void setDireccion(String direccion) {
        entity.setDireccion(direccion);
    }
}
