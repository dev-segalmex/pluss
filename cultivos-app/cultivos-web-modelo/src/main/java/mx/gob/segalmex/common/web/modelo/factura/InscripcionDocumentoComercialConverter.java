/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.factura;

import java.math.BigDecimal;
import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionDocumentoComercial;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CicloAgricolaConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CultivoConverter;
import mx.gob.segalmex.common.web.modelo.seguridad.UsuarioConverter;
import mx.gob.segalmex.granos.web.modelo.catalogos.SucursalConverter;
import mx.gob.segalmex.granos.web.modelo.contrato.EstatusInscripcionConverter;

/**
 * Entidad para convertir entidades {@link InscripcionDocumentoComercial} en su
 * representación para ser enviada/recibida al/del cliente.
 *
 * @author oscar
 */
public class InscripcionDocumentoComercialConverter extends AbstractEntidadConverter {

    /**
     * La entidad que respalda el converter.
     */
    private final InscripcionDocumentoComercial entity;

    public InscripcionDocumentoComercialConverter() {
        entity = new InscripcionDocumentoComercial();
        expandLevel = 1;
    }

    public InscripcionDocumentoComercialConverter(InscripcionDocumentoComercial entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad InscripcionDocumentoComercial no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public InscripcionDocumentoComercial getEntity() {
        return entity;
    }

    @XmlElement
    public EstatusInscripcionConverter getEstatus() {
        return Objects.nonNull(entity.getEstatus()) && expandLevel > 0
                ? new EstatusInscripcionConverter(entity.getEstatus(), expandLevel - 1) : null;
    }

    public String getFolio() {
        return entity.getFolio();
    }

    public void setFolio(String folio) {
        entity.setFolio(folio);
    }

    public String getUuid() {
        return entity.getUuid();
    }

    public void setUuid(String uuid) {
        entity.setUuid(uuid);
    }

    @XmlElement
    public UsuarioConverter getUsuarioRegistra() {
        return expandLevel > 0 && Objects.nonNull(entity.getUsuarioRegistra())
                ? new UsuarioConverter(getEntity().getUsuarioRegistra(), expandLevel - 1) : null;
    }

    @XmlElement
    public UsuarioConverter getUsuarioAsignado() {
        return expandLevel > 0 && Objects.nonNull(entity.getUsuarioAsignado())
                ? new UsuarioConverter(entity.getUsuarioAsignado(), expandLevel - 1) : null;
    }

    @XmlElement
    public UsuarioConverter getUsuarioValidador() {
        return expandLevel > 0 && Objects.nonNull(entity.getUsuarioValidador())
                ? new UsuarioConverter(entity.getUsuarioValidador(), expandLevel - 1) : null;
    }

    public CultivoConverter getCultivo() {
        return expandLevel > 0 && Objects.nonNull(getEntity().getCultivo())
                ? new CultivoConverter(getEntity().getCultivo(), expandLevel - 1) : null;
    }

    public void setCultivo(CultivoConverter cultivo) {
        getEntity().setCultivo(cultivo.getEntity());
    }

    public CicloAgricolaConverter getCiclo() {
        return expandLevel > 0 && Objects.nonNull(getEntity().getCiclo())
                ? new CicloAgricolaConverter(getEntity().getCiclo(), expandLevel - 1) : null;
    }

    public void setCiclo(CicloAgricolaConverter ciclo) {
        getEntity().setCiclo(ciclo.getEntity());
    }

    @XmlElement
    public SucursalConverter getSucursal() {
        return expandLevel > 0 && Objects.nonNull(entity.getSucursal())
                ? new SucursalConverter(entity.getSucursal(), expandLevel - 1) : null;
    }

    public String getComentarioEstatus() {
        return entity.getComentarioEstatus();
    }

    public void setComentarioEstatus(String comentarioEstatus) {
        entity.setComentarioEstatus(comentarioEstatus);
    }

    public BigDecimal getCantidad() {
        return entity.getCantidad();
    }

    public void setCantidad(BigDecimal cantidad) {
        entity.setCantidad(cantidad);
    }

    public String getTipo() {
        return entity.getTipo();
    }

    public void setTipo(String tipo) {
        entity.setTipo(tipo);
    }
    
    public String getClaveArchivos() {
        return entity.getClaveArchivos();
    }
    
    public void setClaveArchivos(String claveArchivos) {
        entity.setClaveArchivos(claveArchivos);
    }

}
