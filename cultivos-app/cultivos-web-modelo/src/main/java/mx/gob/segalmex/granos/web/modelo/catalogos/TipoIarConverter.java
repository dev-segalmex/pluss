/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.catalogos;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoIar;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;

/**
 *
 * @author jurgen
 */
@XmlRootElement(name = "tipo-iar")
public class TipoIarConverter extends AbstractCatalogoConverter {

    private final TipoIar entity;

    public TipoIarConverter() {
        this.entity = new TipoIar();
        this.expandLevel = 1;
    }

    public TipoIarConverter(TipoIar entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad TipoPrecio no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public TipoIar getEntity() {
        return this.entity;
    }
}
