/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.factura;

import java.math.BigDecimal;
import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import mx.gob.segalmex.pluss.modelo.factura.ConceptoCfdi;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;

/**
 *
 * @author ismael
 */
public class ConceptoCfdiConverter extends AbstractEntidadConverter {

    private final ConceptoCfdi entity;

    public ConceptoCfdiConverter() {
        entity = new ConceptoCfdi();
        expandLevel = 1;
    }

    public ConceptoCfdiConverter(ConceptoCfdi entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad ConceptoCfdi no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public ConceptoCfdi getEntity() {
        return this.entity;
    }

    @XmlElement
    public BigDecimal getCantidad() {
        return entity.getCantidad();
    }

    @XmlElement
    public String getUnidad() {
        return entity.getUnidad();
    }

    @XmlElement
    public String getClaveUnidad() {
        return entity.getClaveUnidad();
    }

    @XmlElement
    public String getDescripcion() {
        return entity.getDescripcion();
    }

    @XmlElement
    public BigDecimal getValorUnitario() {
        return entity.getValorUnitario();
    }

    @XmlElement
    public BigDecimal getImporte() {
        return entity.getImporte();
    }

    @XmlElement
    public String getEstatus() {
        return entity.getEstatus();
    }

    public void setEstatus(String estatus) {
        entity.setEstatus(estatus);
    }

    @XmlElement
    public String getClaveProductoServicio() {
        return entity.getClaveProductoServicio();
    }

    public void setClaveProductoServicio(String claveProductoServicio) {
        entity.setClaveProductoServicio(claveProductoServicio);
    }

    public int getOrden() {
        return entity.getOrden();
    }

    public void setOrden(int orden) {
        entity.setOrden(orden);
    }
}
