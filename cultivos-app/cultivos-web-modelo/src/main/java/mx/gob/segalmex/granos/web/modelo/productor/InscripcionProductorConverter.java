/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.productor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.archivos.ArchivoConverter;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CicloAgricolaConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CultivoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.EstadoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.ParentescoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.TipoCultivoConverter;
import mx.gob.segalmex.common.web.modelo.productor.ProductorConverter;
import mx.gob.segalmex.granos.web.modelo.inscripcion.ComentarioInscripcionConverter;
import mx.gob.segalmex.common.web.modelo.seguridad.UsuarioConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.historico.HistoricoRegistro;
import mx.gob.segalmex.pluss.modelo.inscripcion.ComentarioInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.productor.ContratoFirmado;
import mx.gob.segalmex.pluss.modelo.productor.EmpresaAsociada;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionMolino;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioDocumento;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import mx.gob.segalmex.pluss.modelo.productor.Productor;
import mx.gob.segalmex.granos.web.modelo.catalogos.GrupoIndigenaConverter;
import mx.gob.segalmex.granos.web.modelo.catalogos.NivelEstudioConverter;
import mx.gob.segalmex.granos.web.modelo.catalogos.SucursalConverter;
import mx.gob.segalmex.granos.web.modelo.catalogos.TipoProductorConverter;
import mx.gob.segalmex.granos.web.modelo.contrato.DatoCapturadoConverter;
import mx.gob.segalmex.granos.web.modelo.contrato.EstatusInscripcionConverter;
import mx.gob.segalmex.granos.web.modelo.historico.HistoricoRegistroConverter;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "inscripcion-productor")
public class InscripcionProductorConverter extends AbstractEntidadConverter {

    private final InscripcionProductor entity;

    private String tipo;

    private List<Archivo> archivos = new ArrayList<>();

    private List<Archivo> archivosCargados = new ArrayList<>();

    private List<Archivo> archivosOpcionales = new ArrayList<>();

    private List<DatoCapturado> datos = new ArrayList<>();

    private List<HistoricoRegistro> historial = new ArrayList<>();

    private List<ComentarioInscripcion> comentarios = new ArrayList<>();

    private boolean forzada = false;

    private List<String> rfcEmpresas = new ArrayList<>();

    private Productor productor;

    private List<PredioDocumento> prediosDocumentos = new ArrayList<>();

    public InscripcionProductorConverter() {
        entity = new InscripcionProductor();
    }

    public InscripcionProductorConverter(InscripcionProductor entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad InscripcionProductor no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public InscripcionProductor getEntity() {
        return entity;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    public String getEtiquetaTipoCultivo() {
        return entity.getEtiquetaTipoCultivo();
    }

    public void setEtiquetaTipoCultivo(String etiquetaTipoCultivo) {
        entity.setEtiquetaTipoCultivo(etiquetaTipoCultivo);
    }

    public String getFolio() {
        return entity.getFolio();
    }

    public void setFolio(String folio) {
        entity.setFolio(folio);
    }

    public String getUuid() {
        return entity.getUuid();
    }

    public void setUuid(String uuid) {
        entity.setUuid(uuid);
    }

    @XmlElement
    public EstatusInscripcionConverter getEstatus() {
        return Objects.nonNull(entity.getEstatus()) && expandLevel > 0
                ? new EstatusInscripcionConverter(entity.getEstatus(), expandLevel - 1) : null;
    }

    @XmlElement
    public UsuarioConverter getUsuarioRegistra() {
        return expandLevel > 0 && Objects.nonNull(entity.getUsuarioRegistra())
                ? new UsuarioConverter(getEntity().getUsuarioRegistra(), expandLevel - 1) : null;
    }

    @XmlElement
    public UsuarioConverter getUsuarioValidador() {
        return expandLevel > 0 && Objects.nonNull(entity.getUsuarioValidador())
                ? new UsuarioConverter(entity.getUsuarioValidador(), expandLevel - 1) : null;
    }

    @XmlElement
    public UsuarioConverter getUsuarioAsignado() {
        return expandLevel > 0 && Objects.nonNull(entity.getUsuarioAsignado())
                ? new UsuarioConverter(entity.getUsuarioAsignado(), expandLevel - 1) : null;
    }

    public CultivoConverter getCultivo() {
        return expandLevel > 0 && Objects.nonNull(getEntity().getCultivo())
                ? new CultivoConverter(getEntity().getCultivo(), expandLevel - 1) : null;
    }

    public void setCultivo(CultivoConverter cultivo) {
        getEntity().setCultivo(cultivo.getEntity());
    }

    public CicloAgricolaConverter getCiclo() {
        return expandLevel > 0 && Objects.nonNull(getEntity().getCiclo())
                ? new CicloAgricolaConverter(getEntity().getCiclo(), expandLevel - 1) : null;
    }

    public void setCiclo(CicloAgricolaConverter ciclo) {
        getEntity().setCiclo(ciclo.getEntity());
    }

    @XmlElement
    public SucursalConverter getSucursal() {
        return expandLevel > 0 && Objects.nonNull(entity.getSucursal())
                ? new SucursalConverter(entity.getSucursal(), expandLevel - 1) : null;
    }

    public void setSucursal(SucursalConverter sucursal) {
        getEntity().setSucursal(sucursal.getEntity());
    }

    @XmlElement
    public SucursalConverter getBodega() {
        return expandLevel > 0 && Objects.nonNull(entity.getBodega())
                ? new SucursalConverter(entity.getBodega(), expandLevel - 1) : null;
    }

    public void setBodega(SucursalConverter bodega) {
        entity.setBodega(bodega.getEntity());
    }

    public DatosProductorConverter getDatosProductor() {
        return expandLevel > 0 && Objects.nonNull(entity.getDatosProductor())
                ? new DatosProductorConverter(entity.getDatosProductor(), expandLevel - 1) : null;
    }

    public void setDatosProductor(DatosProductorConverter datosProductor) {
        entity.setDatosProductor(datosProductor.getEntity());
    }

    public DomicilioConverter getDomicilio() {
        return expandLevel > 0 && Objects.nonNull(entity.getDomicilio())
                ? new DomicilioConverter(entity.getDomicilio(), expandLevel - 1) : null;
    }

    public void setDomicilio(DomicilioConverter domicilio) {
        entity.setDomicilio(domicilio.getEntity());
    }

    public String getNumeroTelefono() {
        return entity.getNumeroTelefono();
    }

    public void setNumeroTelefono(String numeroTelefono) {
        entity.setNumeroTelefono(numeroTelefono);
    }

    public String getCorreoElectronico() {
        return entity.getCorreoElectronico();
    }

    public void setCorreoElectronico(String correoElectronico) {
        entity.setCorreoElectronico(correoElectronico);
    }

    public List<ContratoFirmadoConverter> getContratos() {
        return expandLevel > 0 && Objects.nonNull(entity.getContratos())
                ? CollectionConverter.convert(ContratoFirmadoConverter.class,
                        entity.getContratos(), expandLevel - 1) : null;
    }

    public void setContratos(List<ContratoFirmadoConverter> contratos) {
        if (Objects.nonNull(contratos)) {
            entity.setContratos(CollectionConverter.unconvert(
                    ContratoFirmado.class, contratos));
        }
    }

    public List<PredioProductorConverter> getPredios() {
        return expandLevel > 0 && Objects.nonNull(entity.getPredios())
                ? CollectionConverter.convert(PredioProductorConverter.class,
                        entity.getPredios(), expandLevel - 1) : null;
    }

    public void setPredios(List<PredioProductorConverter> predios) {
        if (Objects.nonNull(predios)) {
            entity.setPredios(CollectionConverter.unconvert(
                    PredioProductor.class, predios));
        }
    }

    public List<EmpresaAsociadaConverter> getEmpresas() {
        return expandLevel > 0 && Objects.nonNull(entity.getEmpresas())
                ? CollectionConverter.convert(EmpresaAsociadaConverter.class,
                        entity.getEmpresas(), expandLevel - 1) : null;
    }

    public void setEmpresas(List<EmpresaAsociadaConverter> empresas) {
        if (Objects.nonNull(empresas)) {
            entity.setEmpresas(CollectionConverter.unconvert(EmpresaAsociada.class, empresas));
        }
    }

    public CuentaBancariaConverter getCuentaBancaria() {
        return expandLevel > 0 && Objects.nonNull(entity.getCuentaBancaria())
                ? new CuentaBancariaConverter(entity.getCuentaBancaria(), expandLevel - 1) : null;
    }

    public void setCuentaBancaria(CuentaBancariaConverter cuentaBancaria) {
        entity.setCuentaBancaria(cuentaBancaria.getEntity());
    }

    public String getNombreBeneficiario() {
        return entity.getNombreBeneficiario();
    }

    public void setNombreBeneficiario(String nombreBeneficiario) {
        entity.setNombreBeneficiario(nombreBeneficiario);
    }

    public String getApellidosBeneficiario() {
        return entity.getApellidosBeneficiario();
    }

    public void setApellidosBeneficiario(String apellidosBeneficiario) {
        entity.setApellidosBeneficiario(apellidosBeneficiario);
    }

    public String getCurpBeneficiario() {
        return entity.getCurpBeneficiario();
    }

    public void setCurpBeneficiario(String curpBeneficiario) {
        entity.setCurpBeneficiario(curpBeneficiario);
    }

    public ParentescoConverter getParentesco() {
        return expandLevel > 0 && Objects.nonNull(entity.getParentesco())
                ? new ParentescoConverter(entity.getParentesco(), expandLevel - 1) : null;
    }

    public void setParentesco(ParentescoConverter parentesco) {
        entity.setParentesco(parentesco.getEntity());
    }

    public Boolean getBeneficiarioDuplicado() {
        return entity.getBeneficiarioDuplicado();
    }

    public String getEncargado() {
        return entity.getEncargado();
    }

    public void setEncargado(String encargado) {
        entity.setEncargado(encargado);
    }

    @XmlElement
    public List<ArchivoConverter> getArchivos() {
        if (Objects.isNull(archivos) || archivos.isEmpty()) {
            return null;
        }
        return CollectionConverter.convert(ArchivoConverter.class, archivos, expandLevel - 1);
    }

    public void setArchivos(List<Archivo> archivos) {
        this.archivos = archivos;
    }

    @XmlElement
    public List<ArchivoConverter> getArchivosCargados() {
        if (Objects.isNull(archivosCargados) || archivosCargados.isEmpty()) {
            return null;
        }
        return CollectionConverter.convert(ArchivoConverter.class, archivosCargados, expandLevel - 1);
    }

    public void setArchivosCargados(List<Archivo> archivosCargados) {
        this.archivosCargados = archivosCargados;
    }

    @XmlElement
    public List<ArchivoConverter> getArchivosOpcionales() {
        if (Objects.isNull(archivosOpcionales)) {
            return null;
        }
        return CollectionConverter.convert(ArchivoConverter.class, archivosOpcionales, expandLevel - 1);
    }

    public void setArchivosOpcionales(List<Archivo> archivosOpcionales) {
        this.archivosOpcionales = archivosOpcionales;
    }

    @XmlElement
    public List<DatoCapturadoConverter> getDatos() {
        if (Objects.isNull(datos) || datos.isEmpty()) {
            return null;
        }
        return CollectionConverter.convert(DatoCapturadoConverter.class, datos, expandLevel - 1);
    }

    public void setDatos(List<DatoCapturado> datos) {
        this.datos = datos;
    }

    @XmlElement
    public List<HistoricoRegistroConverter> getHistorial() {
        if (Objects.isNull(historial) || historial.isEmpty()) {
            return null;
        }
        return CollectionConverter.convert(HistoricoRegistroConverter.class, historial, expandLevel - 1);
    }

    public void setHistorial(List<HistoricoRegistro> historial) {
        this.historial = historial;
    }

    public String getComentarioEstatus() {
        return entity.getComentarioEstatus();
    }

    public void setComentarioEstatus(String comentarioEstatus) {
        entity.setComentarioEstatus(comentarioEstatus);
    }

    @XmlElement
    public List<ComentarioInscripcionConverter> getComentarios() {
        if (Objects.isNull(comentarios) || comentarios.isEmpty()) {
            return null;
        }
        return CollectionConverter.convert(ComentarioInscripcionConverter.class, comentarios, expandLevel - 1);
    }

    public void setComentarios(List<ComentarioInscripcion> comentarios) {
        this.comentarios = comentarios;
    }

    public boolean isForzada() {
        return forzada;
    }

    public void setForzada(boolean forzada) {
        this.forzada = forzada;
    }

    public List<String> getRfcEmpresas() {
        return Objects.isNull(rfcEmpresas) || rfcEmpresas.isEmpty() ? null : rfcEmpresas;
    }

    public void setRfcEmpresas(List<String> rfcEmpresas) {
        this.rfcEmpresas = rfcEmpresas;
    }

    public int getNumeroContratos() {
        return entity.getNumeroContratos();
    }

    public void setNumeroContratos(int numeroContratos) {
        entity.setNumeroContratos(numeroContratos);
    }

    public int getNumeroPredios() {
        return entity.getNumeroPredios();
    }

    public void setNumeroPredios(int numeroPredios) {
        entity.setNumeroPredios(numeroPredios);
    }

    public int getNumeroEmpresas() {
        return entity.getNumeroEmpresas();
    }

    public void setNumeroEmpresas(int numeroEmpresas) {
        entity.setNumeroEmpresas(numeroEmpresas);
    }

    public List<TipoCultivoConverter> getTiposCultivo() {
        return expandLevel > 0 && Objects.nonNull(entity.getTiposCultivo())
                ? CollectionConverter.convert(TipoCultivoConverter.class,
                        entity.getTiposCultivo(), expandLevel - 1) : null;
    }

    public void setTiposCultivo(List<TipoCultivoConverter> tiposCultivo) {
        if (Objects.nonNull(tiposCultivo)) {
            entity.setTiposCultivo(CollectionConverter.unconvert(
                    TipoCultivo.class, tiposCultivo));
        }
    }

    public InformacionSemillaConverter getInformacionSemilla() {
        return expandLevel > 0 && Objects.nonNull(entity.getInformacionSemilla())
                ? new InformacionSemillaConverter(entity.getInformacionSemilla(),
                        expandLevel - 1) : null;
    }

    public void setInformacionSemilla(InformacionSemillaConverter informacionSemilla) {
        entity.setInformacionSemilla(informacionSemilla.getEntity());
    }

    public String getTipoRegistro() {
        return entity.getTipoRegistro();
    }

    public void setTipoRegistro(String tipoRegistro) {
        entity.setTipoRegistro(tipoRegistro);
    }

    public String getClaveArchivos() {
        return entity.getClaveArchivos();
    }

    public void setClaveArchivos(String claveArchivos) {
        entity.setClaveArchivos(claveArchivos);
    }

    public List<InscripcionMolinoConverter> getMolinos() {
        if (Objects.isNull(entity.getMolinos()) || entity.getMolinos().isEmpty()) {
            return null;
        }
        return CollectionConverter.convert(InscripcionMolinoConverter.class,
                entity.getMolinos(), expandLevel - 1);
    }

    public void setMolinos(List<InscripcionMolinoConverter> molinos) {
        entity.setMolinos(CollectionConverter.unconvert(
                InscripcionMolino.class, molinos));
    }

    public ProductorConverter getProductor() {
        return expandLevel > 0 && Objects.nonNull(productor)
                ? new ProductorConverter(productor,
                        expandLevel - 1) : null;
    }

    public void setProductor(Productor productor) {
        this.productor = productor;
    }

    public List<PredioDocumentoConverter> getPrediosDocumentos() {
        return CollectionConverter.convert(PredioDocumentoConverter.class,
                prediosDocumentos, expandLevel - 1);
    }

    public void setPrediosDocumentos(List<PredioDocumento> prediosDocumentos) {
        this.prediosDocumentos = prediosDocumentos;
    }

    public EstadoConverter getEstado() {
        return expandLevel > 0 && Objects.nonNull(entity.getEstado())
                ? new EstadoConverter(entity.getEstado(), expandLevel - 1) : null;
    }

    public GrupoIndigenaConverter getGrupoIndigena() {
        return expandLevel > 0 && Objects.nonNull(entity.getGrupoIndigena())
                ? new GrupoIndigenaConverter(entity.getGrupoIndigena(),
                        expandLevel - 1) : null;
    }

    public void setGrupoIndigena(GrupoIndigenaConverter grupoIndigena) {
        entity.setGrupoIndigena(grupoIndigena.getEntity());
    }

    public TipoProductorConverter getTipoProductor() {
        return expandLevel > 0 && Objects.nonNull(entity.getTipoProductor())
                ? new TipoProductorConverter(entity.getTipoProductor(),
                    expandLevel -1) : null;
    }

    public void setTipoProductor(TipoProductorConverter tipoProductor) {
        entity.setTipoProductor(tipoProductor.getEntity());
    }

    public NivelEstudioConverter getNivelEstudio() {
        return expandLevel > 0 && Objects.nonNull(entity.getNivelEstudio())
                ? new NivelEstudioConverter(entity.getNivelEstudio(),
                        expandLevel - 1) : null;
    }

    public void setNivelEstudio(NivelEstudioConverter nivelEstudio) {
        entity.setNivelEstudio(nivelEstudio.getEntity());
    }
}
