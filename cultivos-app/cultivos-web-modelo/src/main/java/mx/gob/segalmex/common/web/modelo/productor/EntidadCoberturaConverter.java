/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.productor;

import java.util.Objects;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;
import mx.gob.segalmex.pluss.modelo.productor.EntidadCobertura;

/**
 *
 * @author jurgen
 */
public class EntidadCoberturaConverter extends AbstractCatalogoConverter{

    private final EntidadCobertura entity;

    public EntidadCoberturaConverter() {
        entity = new EntidadCobertura();
        expandLevel = 1;
    }

    public EntidadCoberturaConverter(EntidadCobertura entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Cobertura no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    /**
     * Recupera la entidad representada.
     *
     * @return
     */
    @Override
    public EntidadCobertura getEntity() {
        return this.entity;
    }

}
