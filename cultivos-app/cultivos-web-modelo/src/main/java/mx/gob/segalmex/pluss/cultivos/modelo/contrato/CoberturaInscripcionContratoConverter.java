/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.modelo.contrato;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.productor.EntidadCoberturaConverter;
import mx.gob.segalmex.pluss.modelo.contrato.CoberturaInscripcionContrato;

/**
 *
 * @author oscar
 */
@XmlRootElement(name = "cobertura-inscripcion")
public class CoberturaInscripcionContratoConverter extends AbstractEntidadConverter {

    private final CoberturaInscripcionContrato entity;

    public CoberturaInscripcionContratoConverter() {
        entity = new CoberturaInscripcionContrato();
    }

    public CoberturaInscripcionContratoConverter(CoberturaInscripcionContrato entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad CoberturaInscripcionContrato no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public CoberturaInscripcionContrato getEntity() {
        return entity;
    }

    public String getNumero() {
        return getEntity().getNumero();
    }

    public void setNumero(String numero) {
        getEntity().setNumero(numero);
    }

    public EntidadCoberturaConverter getEntidadCobertura() {
        return expandLevel > 0 && Objects.nonNull(entity.getEntidadCobertura())
                ? new EntidadCoberturaConverter(getEntity().getEntidadCobertura(), expandLevel - 1) : null;
    }

    public void setEntidadCobertura(EntidadCoberturaConverter entidadCobertura) {
        getEntity().setEntidadCobertura(entidadCobertura.getEntity());
    }

    public TipoCompradorCoberturaConverter getTipoComprador() {
        return expandLevel > 0 && Objects.nonNull(entity.getTipoComprador())
                ? new TipoCompradorCoberturaConverter(getEntity().getTipoComprador(), expandLevel - 1) : null;
    }

    public void setTipoComprador(TipoCompradorCoberturaConverter tipoComprador) {
        getEntity().setTipoComprador(tipoComprador.getEntity());
    }

    public String getNombreComprador() {
        return getEntity().getNombreComprador();
    }

    public void setNombreComprador(String nombreComprador) {
        getEntity().setNombreComprador(nombreComprador);
    }

    public Calendar getFechaCompra() {
        return getEntity().getFechaCompra();
    }

    public void setFechaCompra(Calendar fechaCompra) {
        getEntity().setFechaCompra(fechaCompra);
    }

    public Calendar getFechaVencimiento() {
        return getEntity().getFechaVencimiento();
    }

    public void setFechaVencimiento(Calendar fechaVencimiento) {
        getEntity().setFechaVencimiento(fechaVencimiento);
    }

    public BigDecimal getNumeroContratos() {
        return getEntity().getNumeroContratos();
    }

    public void setNumeroContratos(BigDecimal numeroContratos) {
        getEntity().setNumeroContratos(numeroContratos);
    }

    public BigDecimal getNumeroContratosToneladas() {
        return getEntity().getNumeroContratosToneladas();
    }

    public void setNumeroContratosToneladas(BigDecimal numeroContratosToneladas) {
        getEntity().setNumeroContratosToneladas(numeroContratosToneladas);
    }

    public BigDecimal getPrecioEjercicioDolares() {
        return getEntity().getPrecioEjercicioDolares();
    }

    public void setPrecioEjercicioDolares(BigDecimal precioEjercicioDolares) {
        getEntity().setPrecioEjercicioDolares(precioEjercicioDolares);
    }

    public BigDecimal getPrecioEjercicioPesos() {
        return getEntity().getPrecioEjercicioPesos();
    }

    public void setPrecioEjercicioPesos(BigDecimal precioEjercicioPesos) {
        getEntity().setPrecioEjercicioPesos(precioEjercicioPesos);
    }

    public BigDecimal getPrimaDolares() {
        return getEntity().getPrimaDolares();
    }

    public void setPrimaDolares(BigDecimal primaDolares) {
        getEntity().setPrimaDolares(primaDolares);
    }

    public BigDecimal getPrimaPesos() {
        return getEntity().getPrimaPesos();
    }

    public void setPrimaPesos(BigDecimal primaPesos) {
        getEntity().setPrimaPesos(primaPesos);
    }

    public BigDecimal getComisionesDolares() {
        return getEntity().getComisionesDolares();
    }

    public void setComisionesDolares(BigDecimal comisionesDolares) {
        getEntity().setComisionesDolares(comisionesDolares);
    }

    public BigDecimal getComisionesPesos() {
        return getEntity().getComisionesPesos();
    }

    public void setComisionesPesos(BigDecimal comisionesPesos) {
        getEntity().setComisionesPesos(comisionesPesos);
    }

    public TipoOperacionCoberturaConverter getTipoOperacionCobertura() {
        return expandLevel > 0 && Objects.nonNull(entity.getTipoOperacionCobertura())
                ? new TipoOperacionCoberturaConverter(getEntity().getTipoOperacionCobertura(), expandLevel - 1) : null;
    }

    public void setTipoOperacionCobertura(TipoOperacionCoberturaConverter tipoOperacionCobertura) {
        getEntity().setTipoOperacionCobertura(tipoOperacionCobertura.getEntity());
    }

}
