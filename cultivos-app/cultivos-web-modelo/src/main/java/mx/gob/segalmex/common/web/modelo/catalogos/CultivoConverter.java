/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.catalogos;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;

/**
 *
 * @author cuecho
 */
@XmlRootElement(name = "cultivo")
public class CultivoConverter extends AbstractCatalogoConverter {

    /**
     * Entidad a representar en este converter.
     */
    private final Cultivo entity;

    public CultivoConverter() {
        entity = new Cultivo();
        expandLevel = 1;
    }

    public CultivoConverter(Cultivo entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Cultivo no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    /**
     * Recupera la entidad representada.
     *
     * @return
     */
    @Override
    public Cultivo getEntity() {
        return this.entity;
    }

}
