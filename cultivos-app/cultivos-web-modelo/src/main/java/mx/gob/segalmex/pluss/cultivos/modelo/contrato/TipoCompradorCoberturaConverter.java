/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.modelo.contrato;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;
import mx.gob.segalmex.pluss.modelo.contrato.TipoCompradorCobertura;

/**
 *
 * @author oscar
 */
@XmlRootElement(name = "tipo-comprador-cobertura")
public class TipoCompradorCoberturaConverter extends AbstractCatalogoConverter {

    private final TipoCompradorCobertura entity;

    public TipoCompradorCoberturaConverter() {
        this.entity = new TipoCompradorCobertura();
        this.expandLevel = 1;
    }

    public TipoCompradorCoberturaConverter(TipoCompradorCobertura entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad TipoCompradorCobertura no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public TipoCompradorCobertura getEntity() {
        return this.entity;
    }

}
