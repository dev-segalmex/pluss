/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.catalogos;

import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;

/**
 *
 * @author cuecho
 */
@XmlRootElement(name = "ciclo-agricola")
public class CicloAgricolaConverter extends AbstractCatalogoConverter {

    /**
     * Entidad a representar en este converter.
     */
    private final CicloAgricola entity;

    public CicloAgricolaConverter() {
        entity = new CicloAgricola();
        expandLevel = 1;
    }

    public CicloAgricolaConverter(CicloAgricola entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad CicloAgricola no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    /**
     * Recupera la entidad representada.
     *
     * @return
     */
    @Override
    public CicloAgricola getEntity() {
        return this.entity;
    }

    @XmlElement
    public String getAbreviatura() {
        return entity.getAbreviatura();
    }
}
