/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.factura;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author oscar
 */
@XmlRootElement(name = "administracion-uso-factura")
public class AdministracionUsoFacturaConverter {

    private List<String> usosFacturasUuid = new ArrayList<>();

    private String comentario;

    @XmlElement
    public List<String> getUsosFacturasUuid() {
        return usosFacturasUuid;
    }

    public void setUsosFacturasUuid(List<String> usosFacturasUuid) {
        this.usosFacturasUuid = usosFacturasUuid;
    }

    @XmlElement
    public String getComentario() {
        return comentario;
    }

    public void setComentario(String comentario) {
        this.comentario = comentario;
    }

}
