/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.contrato;

import java.math.BigDecimal;
import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.EstadoConverter;
import mx.gob.segalmex.pluss.modelo.inscripcion.Predio;
import mx.gob.segalmex.granos.web.modelo.catalogos.TipoPrecioConverter;

/**
 *
 * @author cuecho
 */
@XmlRootElement(name = "predio")
public class PredioConverter extends AbstractEntidadConverter {

    private final Predio entity;

    public PredioConverter() {
        entity = new Predio();
    }

    public PredioConverter(Predio entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Predio no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public Predio getEntity() {
        return entity;
    }

    public EstadoConverter getEstado() {
        return expandLevel > 0 && Objects.nonNull(getEntity().getEstado())
                ? new EstadoConverter(getEntity().getEstado(), expandLevel - 1) : null;
    }

    public void setEstado(EstadoConverter estado) {
        getEntity().setEstado(estado.getEntity());
    }

    public BigDecimal getSuperficie() {
        return getEntity().getSuperficie();
    }

    public void setSuperficie(BigDecimal superficie) {
        getEntity().setSuperficie(superficie);
    }

    public BigDecimal getVolumen() {
        return getEntity().getVolumen();
    }

    public void setVolumen(BigDecimal volumen) {
        getEntity().setVolumen(volumen);
    }

    public TipoPrecioConverter getTipoPrecio() {
        return expandLevel > 0 && Objects.nonNull(entity.getTipoPrecio())
                ? new TipoPrecioConverter(entity.getTipoPrecio(), expandLevel - 1) : null;
    }

    public void setTipoPrecio(TipoPrecioConverter tipoPrecio) {
        entity.setTipoPrecio(tipoPrecio.getEntity());
    }

    public BigDecimal getPrecioFuturo() {
        return getEntity().getPrecioFuturo();
    }

    public void setPrecioFuturo(BigDecimal precioFuturo) {
        getEntity().setPrecioFuturo(precioFuturo);
    }

    public BigDecimal getBaseAcordada() {
        return getEntity().getBaseAcordada();
    }

    public void setBaseAcordada(BigDecimal baseAcordada) {
        getEntity().setBaseAcordada(baseAcordada);
    }

    public BigDecimal getPrecioFijo() {
        return getEntity().getPrecioFijo();
    }

    public void setPrecioFijo(BigDecimal precioFijo) {
        getEntity().setPrecioFijo(precioFijo);
    }

}
