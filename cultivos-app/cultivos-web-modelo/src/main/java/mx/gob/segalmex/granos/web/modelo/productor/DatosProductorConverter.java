/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.productor;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.SexoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.TipoDocumentoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.TipoPersonaConverter;
import mx.gob.segalmex.pluss.modelo.productor.DatosProductor;
import mx.gob.segalmex.pluss.modelo.productor.SocioDatosProductor;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "datos-productor")
public class DatosProductorConverter extends AbstractEntidadConverter {

    private final DatosProductor entity;

    public DatosProductorConverter() {
        entity = new DatosProductor();
    }

    public DatosProductorConverter(DatosProductor entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad DatosProductor no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public DatosProductor getEntity() {
        return entity;
    }

    public TipoPersonaConverter getTipoPersona() {
        return expandLevel > 0 && Objects.nonNull(entity.getTipoPersona())
                ? new TipoPersonaConverter(entity.getTipoPersona(), expandLevel - 1) : null;
    }

    public void setTipoPersona(TipoPersonaConverter tipoPersona) {
        entity.setTipoPersona(tipoPersona.getEntity());
    }

    public String getNombreMoral() {
        return entity.getNombreMoral();
    }

    public void setNombreMoral(String nombreMoral) {
        entity.setNombreMoral(nombreMoral);
    }

    public String getCurp() {
        return entity.getCurp();
    }

    public void setCurp(String curp) {
        entity.setCurp(curp);
    }

    public String getRfc() {
        return entity.getRfc();
    }

    public void setRfc(String rfc) {
        entity.setRfc(rfc);
    }

    public String getNombre() {
        return entity.getNombre();
    }

    public void setNombre(String nombre) {
        entity.setNombre(nombre);
    }

    public String getPrimerApellido() {
        return entity.getPrimerApellido();
    }

    public void setPrimerApellido(String primerApellido) {
        entity.setPrimerApellido(primerApellido);
    }

    public String getSegundoApellido() {
        return entity.getSegundoApellido();
    }

    public void setSegundoApellido(String segundoApellido) {
        entity.setSegundoApellido(segundoApellido);
    }

    public Calendar getFechaNacimiento() {
        return entity.getFechaNacimiento();
    }

    public void setFechaNacimiento(Calendar fechaNacimiento) {
        entity.setFechaNacimiento(fechaNacimiento);
    }

    public SexoConverter getSexo() {
        return expandLevel > 0 && Objects.nonNull(entity.getSexo())
                ? new SexoConverter(entity.getSexo(), expandLevel - 1) : null;
    }

    public void setSexo(SexoConverter sexo) {
        entity.setSexo(sexo.getEntity());
    }

    public TipoDocumentoConverter getTipoDocumento() {
        return expandLevel > 0 && Objects.nonNull(entity.getTipoDocumento())
                ? new TipoDocumentoConverter(entity.getTipoDocumento(), expandLevel - 1) : null;
    }

    public void setTipoDocumento(TipoDocumentoConverter tipoDocumento) {
        entity.setTipoDocumento(tipoDocumento.getEntity());
    }

    public String getNumeroDocumento() {
        return entity.getNumeroDocumento();
    }

    public void setNumeroDocumento(String numeroDocumento) {
        entity.setNumeroDocumento(numeroDocumento);
    }

    public List<SocioDatosProductorConverter> getSocios() {
        return expandLevel > 0 && Objects.nonNull(entity.getSocios())
                ? CollectionConverter.convert(SocioDatosProductorConverter.class,
                        entity.getSocios(), expandLevel - 1) : null;
    }

    public void setSocios(List<SocioDatosProductorConverter> socios) {
        if (Objects.nonNull(socios)) {
            entity.setSocios(CollectionConverter.unconvert(SocioDatosProductor.class, socios));
        }
    }

}
