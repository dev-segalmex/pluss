/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.productor;

import java.math.BigDecimal;
import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.pluss.modelo.productor.PredioDocumento;

/**
 *
 * @author oscar
 */
@XmlRootElement(name = "predio-documento")
public class PredioDocumentoConverter extends AbstractEntidadConverter {

    private final PredioDocumento entity;

    public PredioDocumentoConverter() {
        entity = new PredioDocumento();
    }

    public PredioDocumentoConverter(PredioDocumento entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad PredioDocumento no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public PredioDocumento getEntity() {
        return entity;
    }

    public String getNumero() {
        return entity.getNumero();
    }

    public void setNumero(String numero) {
        entity.setNumero(numero);
    }

    public String getTipo() {
        return entity.getTipo();
    }

    public void setTipo(String tipo) {
        entity.setTipo(tipo);
    }

    public String getRfcEmisor() {
        return entity.getRfcEmisor();
    }

    public String getRfcReceptor() {
        return entity.getRfcReceptor();
    }

    public String getUuidTimbreFiscalDigital() {
        return entity.getUuidTimbreFiscalDigital();
    }

    public BigDecimal getTotal() {
        return entity.getTotal();
    }

    public Boolean getValidacionSat() {
        return entity.getValidacionSat();
    }

    public Boolean getDuplicado() {
        return entity.getDuplicado();
    }

    public String getMonitoreable() {
        return entity.getMonitoreable();
    }

    public InscripcionProductorConverter getInscripcionProductor() {
        return Objects.nonNull(entity.getInscripcionProductor()) && expandLevel > 0
                ? new InscripcionProductorConverter(entity.getInscripcionProductor(), expandLevel - 1) : null;
    }

    public void setInscripcionProductor(InscripcionProductorConverter inscripcionProductor) {
        entity.setInscripcionProductor(inscripcionProductor.getEntity());
    }

    public boolean isActivo() {
        return entity.isActivo();
    }

}
