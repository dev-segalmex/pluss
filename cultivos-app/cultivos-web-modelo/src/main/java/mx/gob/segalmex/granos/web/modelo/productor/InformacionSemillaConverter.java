/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.productor;

import java.math.BigDecimal;
import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.pluss.modelo.productor.InformacionSemilla;

/**
 * Clase para representar e intercambiar con el cliente ejemplares de
 * {@link  InformacionSemilla}
 *
 * @author oscar
 */
@XmlRootElement(name = "informacion-semilla")
public class InformacionSemillaConverter extends AbstractEntidadConverter {

    private final InformacionSemilla entity;

    public InformacionSemillaConverter() {
        entity = new InformacionSemilla();
    }

    public InformacionSemillaConverter(InformacionSemilla entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad InformacionSemilla no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public InformacionSemilla getEntity() {
        return entity;
    }

    public String getSnics() {
        return getEntity().getSnics();
    }

    public void setSnics(String snics) {
        getEntity().setSnics(snics);
    }

    public BigDecimal getToneladas() {
        return getEntity().getToneladas();
    }

    public void setToneladas(BigDecimal toneladas) {
        getEntity().setToneladas(toneladas);
    }

    public String getCantidadFolios() {
        return getEntity().getCantidadFolios();
    }

    public void setCantidadFolios(String cantidadFolios) {
        getEntity().setCantidadFolios(cantidadFolios);
    }

    public String getNombre() {
        return getEntity().getNombre();
    }

    public void setNombre(String nombre) {
        getEntity().setNombre(nombre);
    }

    public String getRfc() {
        return getEntity().getRfc();
    }

    public void setRfc(String rfc) {
        getEntity().setRfc(rfc);
    }
}
