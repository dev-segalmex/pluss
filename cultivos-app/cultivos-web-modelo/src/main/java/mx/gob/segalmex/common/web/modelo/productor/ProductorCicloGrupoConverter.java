/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.productor;

import java.math.BigDecimal;
import java.util.Objects;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCicloGrupo;

/**
 *
 * @author oscar
 */
public class ProductorCicloGrupoConverter extends AbstractEntidadConverter {

    private final ProductorCicloGrupo entity;

    public ProductorCicloGrupoConverter() {
        this.entity = new ProductorCicloGrupo();
        this.expandLevel = 1;
    }

    public ProductorCicloGrupoConverter(ProductorCicloGrupo entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad ProductorCicloGrupo no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public ProductorCicloGrupo getEntity() {
        return this.entity;
    }

    public String getGrupoEstimulo() {
        return entity.getGrupoEstimulo();
    }

    public BigDecimal getToneladasTotales() {
        return entity.getToneladasTotales();
    }

    public BigDecimal getToneladasFacturadas() {
        return entity.getToneladasFacturadas();
    }

    public BigDecimal getToneladasContratadas() {
        return entity.getToneladasContratadas();
    }

    public BigDecimal getToneladasCobertura() {
        return entity.getToneladasCobertura();
    }

    public BigDecimal getLimite() {
        return entity.getLimite();
    }

}
