/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.productor;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CicloAgricolaConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CultivoConverter;
import mx.gob.segalmex.pluss.modelo.productor.InformacionAlerta;

/**
 *
 * @author jurgen
 */
@XmlRootElement(name = "inscripcion-productor")
public class InformacionAlertaConverter extends AbstractEntidadConverter {

    private final InformacionAlerta entity;

    public InformacionAlertaConverter() {
        entity = new InformacionAlerta();
    }

    public InformacionAlertaConverter(InformacionAlerta entity, int expanLevel) {
        Objects.requireNonNull(entity, "La entidad InformacionAlerta no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public InformacionAlerta getEntity() {
        return entity;
    }

    public CultivoConverter getCultivo() {
        return expandLevel > 0 && Objects.nonNull(getEntity().getCultivo())
                ? new CultivoConverter(getEntity().getCultivo(), expandLevel - 1) : null;
    }

    public void setCultivo(CultivoConverter cultivo) {
        getEntity().setCultivo(cultivo.getEntity());
    }

    public CicloAgricolaConverter getCiclo() {
        return expandLevel > 0 && Objects.nonNull(getEntity().getCiclo())
                ? new CicloAgricolaConverter(getEntity().getCiclo(), expandLevel - 1) : null;
    }

    public void setCiclo(CicloAgricolaConverter ciclo) {
        getEntity().setCiclo(ciclo.getEntity());
    }

    public String getFolio() {
        return entity.getFolio();
    }

    public void setFolio(String folio) {
        entity.setFolio(folio);
    }

    public String getTipo() {
        return entity.getTipo();
    }

    public void setTipo(String tipo) {
        entity.setTipo(tipo);
    }

    public String getValor() {
        return entity.getValor();
    }

    public void setValor(String valor) {
        entity.setValor(valor);
    }

    public String getComentario() {
        return entity.getComentario();
    }

    public void setComentario(String comentario) {
        entity.setComentario(comentario);
    }
}
