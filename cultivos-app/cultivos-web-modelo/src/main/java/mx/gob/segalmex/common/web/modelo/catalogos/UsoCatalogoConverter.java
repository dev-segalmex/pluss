/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.catalogos;

import java.util.Objects;
import mx.gob.segalmex.pluss.modelo.catalogos.UsoCatalogo;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.seguridad.UsuarioConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;
import mx.gob.segalmex.granos.web.modelo.catalogos.SucursalConverter;

/**
 *
 * @author jurgen
 */
public class UsoCatalogoConverter extends AbstractEntidadConverter{

    /**
     * La entidad que respalda el converter.
     */
    private final UsoCatalogo entity;

    private Sucursal sucursal;

    private Usuario usuario;

    public UsoCatalogoConverter() {
        entity = new UsoCatalogo();
        expandLevel = 1;
    }

    public UsoCatalogoConverter(UsoCatalogo entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Parametro no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public UsoCatalogo getEntity() {
        return entity;
    }

    public String getClase(){
        return entity.getClase();
    }

    public void setClase(String clase) {
        entity.setClase(clase);
    }

    public String getClave(){
        return entity.getClave();
    }

    public void setClave(String clave) {
        entity.setClave(clave);
    }

    public String getUso(){
        return entity.getUso();
    }

    public void setUso(String uso) {
        entity.setUso(uso);
    }

    public SucursalConverter getSucursal() {
        return expandLevel > 0 && Objects.nonNull(sucursal)
                ? new SucursalConverter(sucursal, expandLevel - 1) : null;
    }

    public void setSucursal(Sucursal sucursal) {
        this.sucursal = sucursal;
    }

    public UsuarioConverter getUsuario() {
        return expandLevel > 0 && Objects.nonNull(usuario)
                ? new UsuarioConverter(usuario, expandLevel - 1) : null;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public int getOrden(){
        return entity.getOrden();
    }

    public void setOrden(int orden) {
        entity.setOrden(orden);
    }
}
