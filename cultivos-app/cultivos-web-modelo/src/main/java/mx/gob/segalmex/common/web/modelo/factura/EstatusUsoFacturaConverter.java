/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.factura;

import java.math.BigDecimal;
import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.pluss.modelo.factura.EstatusUsoFactura;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "estatus-uso-factura")
public class EstatusUsoFacturaConverter extends AbstractCatalogoConverter {

    private final EstatusUsoFactura entity;

    public EstatusUsoFacturaConverter() {
        this.entity = new EstatusUsoFactura();
        this.expandLevel = 1;
    }

    public EstatusUsoFacturaConverter(EstatusUsoFactura entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad EstatusUsoFactura no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public EstatusUsoFactura getEntity() {
        return this.entity;
    }

    @XmlElement
    public BigDecimal getFactor() {
        return entity.getFactor();
    }

    public void setUuid(BigDecimal factor) {
        entity.setFactor(factor);
    }
}
