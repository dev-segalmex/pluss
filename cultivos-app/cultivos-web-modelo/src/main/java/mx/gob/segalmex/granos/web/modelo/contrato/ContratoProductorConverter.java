/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.contrato;

import java.math.BigDecimal;
import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.pluss.modelo.inscripcion.ContratoProductor;

/**
 *
 * @author jurgen
 */
@XmlRootElement(name = "contrato-productor")
public class ContratoProductorConverter extends AbstractEntidadConverter {

    private final ContratoProductor entity;

    private String numeroContrato;

    private String folioContrato;

    private String nombreEmpresaContrato;

    public ContratoProductorConverter() {
        entity = new ContratoProductor();
    }

    public ContratoProductorConverter(ContratoProductor entity, int expanLevel) {
        Objects.requireNonNull(entity, "La entidad ContratoProductor no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public ContratoProductor getEntity() {
        return entity;
    }

    public String getNombre() {
        return entity.getNombre();
    }

    public void setNombre(String nombre) {
        entity.setNombre(nombre);
    }

    public String getPrimerApellido() {
        return entity.getPrimerApellido();
    }

    public void setPrimerApellido(String primerApellido) {
        entity.setPrimerApellido(primerApellido);
    }

    public String getSegundoApellido() {
        return entity.getSegundoApellido();
    }

    public void setSegundoApellido(String segundoApellido) {
        entity.setSegundoApellido(segundoApellido);
    }

    public String getCurp() {
        return entity.getCurp();
    }

    public void setCurp(String curp) {
        entity.setCurp(curp);
    }

    public String getRfc() {
        return entity.getRfc();
    }

    public void setRfc(String rfc) {
        entity.setRfc(rfc);
    }

    public BigDecimal getSuperficie() {
        return entity.getSuperficie();
    }

    public void setSuperficie(BigDecimal superficie) {
        entity.setSuperficie(superficie);
    }

    public BigDecimal getVolumenContrato() {
        return entity.getVolumenContrato();
    }

    public void setVolumenContrato(BigDecimal volumenContrato) {
        entity.setVolumenContrato(volumenContrato);
    }

    public BigDecimal getVolumenIar() {
        return entity.getVolumenIar();
    }

    public void setVolumenIar(BigDecimal volumenIar) {
        entity.setVolumenIar(volumenIar);
    }

    public String getEstatusFechaBaja() {
        return entity.getEstatusFechaBaja();
    }

    public void setEstatusFechaBaja(String fechaBaja) {
        entity.setEstatusFechaBaja(fechaBaja);
    }

    public boolean isCorrecto() {
        return entity.isCorrecto();
    }

    public void setCorrecto(boolean correcto) {
        entity.setCorrecto(correcto);
    }

    public String getError() {
        return entity.getError();
    }

    public void setError(String error) {
        entity.setError(error);
    }

    public int getOrden() {
        return entity.getOrden();
    }

    public void setOrden(int orden) {
        entity.setOrden(orden);
    }

    public String getNumeroContrato() {
        return numeroContrato;
    }

    public void setNumeroContrato(String numeroContrato) {
        this.numeroContrato = numeroContrato;
    }

    public String getFolioContrato() {
        return folioContrato;
    }

    public void setFolioContrato(String folioContrato) {
        this.folioContrato = folioContrato;
    }

    public String getNombreEmpresaContrato() {
        return nombreEmpresaContrato;
    }

    public void setNombreEmpresaContrato(String nombreEmpresaContrato) {
        this.nombreEmpresaContrato = nombreEmpresaContrato;
    }
}
