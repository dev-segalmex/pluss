/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.factura;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.pluss.modelo.factura.FacturaRestringida;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CicloAgricolaConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CultivoConverter;
import mx.gob.segalmex.common.web.modelo.seguridad.UsuarioConverter;

/**
 *
 * @author jurgen
 */
@XmlRootElement(name = "facturaRestringida")
public class FacturaRestringidaConverter extends AbstractEntidadConverter {

    /**
     * La entidad que respalda el converter.
     */
    private final FacturaRestringida entity;

    public FacturaRestringidaConverter() {
        entity = new FacturaRestringida();
        expandLevel = 1;
    }

    public FacturaRestringidaConverter(FacturaRestringida entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad FacturaRestringida no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public FacturaRestringida getEntity() {
        return entity;
    }

    public String getUuidTimbreFiscalDigital() {
        return entity.getUuidTimbreFiscalDigital();
    }

    public void setUuidTimbreFiscalDigital(String uuid) {
        entity.setUuidTimbreFiscalDigital(uuid);
    }

    public UsuarioConverter getUsuarioRegistra() {
        return expandLevel > 0 && Objects.nonNull(entity.getUsuarioRegistra())
                ? new UsuarioConverter(getEntity().getUsuarioRegistra(), expandLevel - 1) : null;
    }

    public void setUsuarioRegistra(UsuarioConverter usuarioRegistra) {
        entity.setUsuarioRegistra(usuarioRegistra.getEntity());
    }

    public boolean isActivo() {
        return entity.isActivo();
    }

    public void setActivo(boolean activo) {
        entity.setActivo(activo);
    }

    public CultivoConverter getCultivo() {
        return expandLevel > 0 && Objects.nonNull(entity.getCultivo())
                ? new CultivoConverter(getEntity().getCultivo(), expandLevel - 1) : null;
    }

    public void setCultivo(CultivoConverter cultivo) {
        entity.setCultivo(cultivo.getEntity());
    }

    public CicloAgricolaConverter getciclo() {
        return expandLevel > 0 && Objects.nonNull(entity.getCiclo())
                ? new CicloAgricolaConverter(getEntity().getCiclo(), expandLevel - 1) : null;
    }

    public void setCiclo(CicloAgricolaConverter ciclo) {
        entity.setCiclo(ciclo.getEntity());
    }
}
