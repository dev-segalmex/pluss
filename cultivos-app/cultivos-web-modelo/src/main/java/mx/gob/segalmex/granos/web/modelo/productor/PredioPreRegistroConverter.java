/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.productor;

import java.math.BigDecimal;
import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.EstadoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.MunicipioConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.TipoCultivoConverter;
import mx.gob.segalmex.pluss.modelo.productor.PredioPreRegistro;

/**
 *
 * @author jurgen
 */
@XmlRootElement(name = "predio-pre-registro")
public class PredioPreRegistroConverter extends AbstractEntidadConverter{

    private final PredioPreRegistro entity;

    public PredioPreRegistroConverter(){
        this.entity = new PredioPreRegistro();
    }

    public PredioPreRegistroConverter(PredioPreRegistro entity, int expandLevel){
        Objects.requireNonNull(entity, "La entidad PredioPreRegistro no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public PredioPreRegistro getEntity(){
        return entity;
    }

    public TipoCultivoConverter getTipoCultivo() {
        return expandLevel > 0 && Objects.nonNull(entity.getTipoCultivo())
                ? new TipoCultivoConverter(entity.getTipoCultivo(), expandLevel - 1) : null;
    }

    public void setTipoCultivo(TipoCultivoConverter tipoCultivo) {
        entity.setTipoCultivo(tipoCultivo.getEntity());
    }

    public TipoPosesionConverter getTipoPosesion() {
        return expandLevel > 0 && Objects.nonNull(entity.getTipoPosesion())
                ? new TipoPosesionConverter(entity.getTipoPosesion(), expandLevel - 1) : null;
    }

    public void setTipoPosesion(TipoPosesionConverter tipoPosesion) {
        entity.setTipoPosesion(tipoPosesion.getEntity());
    }

    public RegimenHidricoConverter getRegimenHidrico() {
        return expandLevel > 0 && Objects.nonNull(entity.getRegimenHidrico())
                ? new RegimenHidricoConverter(entity.getRegimenHidrico(), expandLevel - 1) : null;
    }

    public void setRegimenHidrico(RegimenHidricoConverter regimenHidrico) {
        entity.setRegimenHidrico(regimenHidrico.getEntity());
    }

    public EstadoConverter getEstado() {
        return Objects.nonNull(entity.getEstado()) && expandLevel > 0
                ? new EstadoConverter(entity.getEstado(), expandLevel - 1) : null;
    }

    public void setEstado(EstadoConverter estado) {
        entity.setEstado(estado.getEntity());
    }

    public MunicipioConverter getMunicipio() {
        return Objects.nonNull(entity.getMunicipio()) && expandLevel > 0
                ? new MunicipioConverter(entity.getMunicipio(), expandLevel - 1) : null;
    }

    public void setMunicipio(MunicipioConverter municipio) {
        entity.setMunicipio(municipio.getEntity());
    }

    public BigDecimal getVolumen(){
        return entity.getVolumen();
    }

    public void setVolumen(BigDecimal volumen){
        entity.setVolumen(volumen);
    }

    public BigDecimal getSuperficie(){
        return entity.getSuperficie();
    }

    public void setSuperficie(BigDecimal superficie){
        entity.setSuperficie(superficie);
    }

    public int getOrden (){
        return entity.getOrden();
    }

    public void setOrden(int orden){
        entity.setOrden(orden);
    }
}
