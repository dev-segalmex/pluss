/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.catalogos;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoSucursal;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "tipo-sucursal")
public class TipoSucursalConverter extends AbstractCatalogoConverter {

    private final TipoSucursal entity;

    public TipoSucursalConverter() {
        this.entity = new TipoSucursal();
        this.expandLevel = 1;
    }

    public TipoSucursalConverter(TipoSucursal entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad TipoSucursal no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public TipoSucursal getEntity() {
        return this.entity;
    }
}
