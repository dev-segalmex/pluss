/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.catalogos;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoPrecio;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "tipo-precio")
public class TipoPrecioConverter extends AbstractCatalogoConverter {

    private final TipoPrecio entity;

    public TipoPrecioConverter() {
        this.entity = new TipoPrecio();
        this.expandLevel = 1;
    }

    public TipoPrecioConverter(TipoPrecio entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad TipoPrecio no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public TipoPrecio getEntity() {
        return this.entity;
    }
}
