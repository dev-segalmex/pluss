/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.pago;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Objects;
import mx.gob.segalmex.pluss.modelo.pago.Estimulo;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CicloAgricolaConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.EstadoConverter;

/**
 *
 * @author oscar
 */
public class EstimuloConverter extends AbstractEntidadConverter {

    /**
     * La entidad que respalda el converter.
     */
    private final Estimulo entity;

    public EstimuloConverter() {
        entity = new Estimulo();
        expandLevel = 1;
    }

    public EstimuloConverter(Estimulo entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Estimulo no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public Estimulo getEntity() {
        return entity;
    }

    public CicloAgricolaConverter getCiclo() {
        return Objects.nonNull(entity.getCiclo()) && expandLevel > 0
                ? new CicloAgricolaConverter(entity.getCiclo(), expandLevel - 1) : null;
    }

    public void setCiclo(CicloAgricolaConverter ciclo) {
        entity.setCiclo(ciclo.getEntity());
    }

    public EstadoConverter getEstado() {
        return Objects.nonNull(entity.getEstado()) && expandLevel > 0
                ? new EstadoConverter(entity.getEstado(), expandLevel - 1) : null;
    }

    public void setEstado(EstadoConverter estado) {
        entity.setEstado(estado.getEntity());
    }

    public Calendar getFechaInicio() {
        return entity.getFechaInicio();
    }

    public void setFechaInicio(Calendar fechaInicio) {
        entity.setFechaInicio(fechaInicio);
    }

    public Calendar getFechaFin() {
        return entity.getFechaFin();
    }

    public void setFechaFin(Calendar fechaFin) {
        entity.setFechaFin(fechaFin);
    }

    public BigDecimal getCantidad() {
        return entity.getCantidad();
    }

    public void setCantidad(BigDecimal cantidad) {
        entity.setCantidad(cantidad);
    }

    public int getOrden() {
        return entity.getOrden();
    }

    public void setOrden(int orden) {
        entity.setOrden(orden);
    }

    public String getAplicaEstimulo() {
        return entity.getAplicaEstimulo();
    }

    public void setAplicaEstimulo(String aplicaEstimulo) {
        entity.setAplicaEstimulo(aplicaEstimulo);
    }

    public String getTipo() {
        return entity.getTipo();
    }

    public void setTipo(String tipo) {
        entity.setTipo(tipo);
    }

    public String getContrato() {
        return entity.getContrato();
    }

    public void setContrato(String contrato) {
        entity.setContrato(contrato);
    }

    public String getTipoProductor() {
        return entity.getTipoProductor();
    }

    public void setTipoProductor(String tipoProductor) {
        entity.setTipoProductor(tipoProductor);
    }

}
