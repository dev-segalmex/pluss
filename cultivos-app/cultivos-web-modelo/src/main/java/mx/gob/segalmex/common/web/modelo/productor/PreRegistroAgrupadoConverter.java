package mx.gob.segalmex.common.web.modelo.productor;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import mx.gob.segalmex.common.web.modelo.catalogos.CicloAgricolaConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.EstadoConverter;
import mx.gob.segalmex.pluss.modelo.productor.PreRegistroAgrupado;

/**
 *
 * @author oscar
 */
@XmlRootElement(name = "inscripcionAgrupada")
public class PreRegistroAgrupadoConverter {

    private final PreRegistroAgrupado entity;

    private final int expandLevel;

    public PreRegistroAgrupadoConverter(PreRegistroAgrupado entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad PreRegistroAgrupado no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    public PreRegistroAgrupadoConverter() {
        entity = new PreRegistroAgrupado();
        expandLevel = 2;
    }

    @XmlTransient
    public PreRegistroAgrupado getEntity() {
        return entity;
    }

    public int getExpandLevel() {
        return expandLevel;
    }

    public CicloAgricolaConverter getCiclo() {
        return getExpandLevel() > 0 && getEntity().getCiclo() != null
                ? new CicloAgricolaConverter(getEntity().getCiclo(), getExpandLevel() - 1) : null;
    }

    public void setCiclo(CicloAgricolaConverter ciclo) {
        getEntity().setCiclo(ciclo.getEntity());
    }

    public EstadoConverter getEstado() {
        return getExpandLevel() > 0 && getEntity().getEstado() != null
                ? new EstadoConverter(getEntity().getEstado(), getExpandLevel() - 1) : null;
    }

    public void setEstado(EstadoConverter estado) {
        getEntity().setEstado(estado.getEntity());
    }

    public Integer getTotal() {
        return getEntity().getTotal();
    }

    public void setTotal(Integer total) {
        getEntity().setTotal(total);
    }

    public static List<PreRegistroAgrupadoConverter> convert(Collection<PreRegistroAgrupado> registros, int expandLevel) {
        List<PreRegistroAgrupadoConverter> converted = new LinkedList<>();

        registros.stream().forEach((registro) -> {
            converted.add(new PreRegistroAgrupadoConverter(registro, expandLevel));
        });

        return converted;
    }

}
