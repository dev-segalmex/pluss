/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.factura;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import mx.gob.segalmex.pluss.modelo.factura.Factura;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;

/**
 *
 * @author ismael
 */
public class FacturaConverter extends AbstractEntidadConverter {

    private final Factura entity;

    public FacturaConverter() {
        entity = new Factura();
        expandLevel = 1;
    }

    public FacturaConverter(Factura entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Factura no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public Factura getEntity() {
        return entity;
    }

    @XmlElement
    public String getUuid() {
        return entity.getUuid();
    }

    @XmlElement
    public String getTipo() {
        return entity.getTipo();
    }

    @XmlElement
    public String getEstatus() {
        return entity.getEstatus();
    }

    @XmlElement
    public String getRfcEmisor() {
        return entity.getRfcEmisor();
    }

    @XmlElement
    public String getNombreEmisor() {
        return entity.getNombreEmisor();
    }

    @XmlElement
    public String getRfcReceptor() {
        return entity.getRfcReceptor();
    }

    @XmlElement
    public String getNombreReceptor() {
        return entity.getNombreReceptor();
    }

    @XmlElement
    public String getUuidTimbreFiscalDigital() {
        return entity.getUuidTimbreFiscalDigital();
    }

    @XmlElement
    public BigDecimal getTotal() {
        return entity.getTotal();
    }

    @XmlElement
    public BigDecimal getToneladasTotales() {
        return entity.getToneladasTotales();
    }

    @XmlElement
    public Calendar getFecha() {
        return entity.getFecha();
    }

    @XmlElement
    public Calendar getFechaTimbrado() {
        return entity.getFechaTimbrado();
    }
}
