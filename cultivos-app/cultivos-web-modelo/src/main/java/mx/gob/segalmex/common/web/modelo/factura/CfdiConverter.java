/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.factura;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import mx.gob.segalmex.pluss.modelo.factura.Cfdi;
import mx.gob.segalmex.pluss.modelo.factura.Factura;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.archivos.ArchivoConverter;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.seguridad.UsuarioConverter;
import mx.gob.segalmex.pluss.modelo.productor.Asociado;
import mx.gob.segalmex.granos.web.modelo.productor.AsociadoConverter;

/**
 *
 * @author ismael
 */
public class CfdiConverter extends AbstractEntidadConverter {

    private final Cfdi entity;

    private String tipo;

    private String productor;

    private List<Asociado> asociados;

    /**
     * Las facturas de productores que se pueden asociar a este CFDI.
     */
    private List<Factura> facturas;

    /**
     * El precio tonelada por Factura. total/totalToneladas.
     */
    private BigDecimal precioTonelada;

    public CfdiConverter() {
        entity = new Cfdi();
        expandLevel = 1;
    }

    public CfdiConverter(Cfdi entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Cfdi no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public Cfdi getEntity() {
        return entity;
    }

    @XmlElement
    public String getUuid() {
        return entity.getUuid();
    }

    public void setUuid(String uuid) {
        entity.setUuid(uuid);
    }

    @XmlElement
    public String getEstatus() {
        return entity.getEstatus();
    }

    @XmlElement
    public String getRfcEmisor() {
        return entity.getRfcEmisor();
    }

    @XmlElement
    public String getNombreEmisor() {
        return entity.getNombreEmisor();
    }

    @XmlElement
    public String getRfcReceptor() {
        return entity.getRfcReceptor();
    }

    @XmlElement
    public String getNombreReceptor() {
        return entity.getNombreReceptor();
    }

    @XmlElement
    public String getUuidTimbreFiscalDigital() {
        return entity.getUuidTimbreFiscalDigital();
    }

    @XmlElement
    public BigDecimal getTotal() {
        return entity.getTotal();
    }

    @XmlElement
    public String getMoneda() {
        return entity.getMoneda();
    }

    @XmlElement
    public List<ConceptoCfdiConverter> getConceptos() {
        return expandLevel > 0 && Objects.nonNull(entity.getConceptos())
                && !entity.getConceptos().isEmpty()
                ? CollectionConverter.convert(ConceptoCfdiConverter.class,
                        entity.getConceptos(), expandLevel - 1) : null;
    }

    @XmlElement
    public ArchivoConverter getArchivo() {
        return expandLevel > 0 && Objects.nonNull(entity.getArchivo())
                ? new ArchivoConverter(entity.getArchivo(), expandLevel - 1) : null;
    }

    @XmlElement
    public UsuarioConverter getUsuario() {
        return expandLevel > 0 && Objects.nonNull(entity.getUsuario())
                ? new UsuarioConverter(entity.getUsuario(), expandLevel - 1) : null;
    }

    @XmlElement
    public Calendar getFechaTimbrado() {
        return entity.getFechaTimbrado();
    }

    @XmlElement
    public Calendar getFecha() {
        return entity.getFecha();
    }

    @XmlElement
    public String getMetodoPago() {
        return entity.getMetodoPago();
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getProductor() {
        return productor;
    }

    public void setProductor(String productor) {
        this.productor = productor;
    }

    @XmlElement
    public List<AsociadoConverter> getAsociados() {
        return Objects.nonNull(asociados) && !asociados.isEmpty()
                ? CollectionConverter.convert(AsociadoConverter.class, asociados, expandLevel - 1)
                : null;
    }

    public void setAsociados(List<Asociado> asociados) {
        this.asociados = asociados;
    }

    @XmlElement
    public BigDecimal getTotalToneladas() {
        return entity.getTotalToneladas();
    }

    @XmlElement
    public BigDecimal getSubtotal() {
        return entity.getSubtotal();
    }

    @XmlElement
    public List<FacturaConverter> getFacturas() {
        return Objects.nonNull(facturas) && !facturas.isEmpty()
                ? CollectionConverter.convert(FacturaConverter.class, facturas, expandLevel - 1)
                : null;
    }

    public void setFacturas(List<Factura> facturas) {
        this.facturas = facturas;
    }

    @XmlElement
    public BigDecimal getPrecioTonelada() {
        return precioTonelada;
    }

    public void setPrecioTonelada(BigDecimal precioTonelada) {
        this.precioTonelada = precioTonelada;
    }

}
