/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.productor;

import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionResumen;

/**
 *
 * @author jurgen
 */
@XmlRootElement(name = "inscripcionResumen")
public class InscripcionResumenConverter {
    
    private final InscripcionResumen entity;

    private final int expandLevel;

    public InscripcionResumenConverter(InscripcionResumen entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad InscripcionResumenno puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    public InscripcionResumenConverter() {
        entity = new InscripcionResumen();
        expandLevel = 2;
    }

    @XmlTransient
    public InscripcionResumen getEntity() {
        return entity;
    }
    
    public String getCultivo() {
        return getEntity().getCultivo();
    }

    public void setCultivo(String cultivo) {
        getEntity().setCultivo(cultivo);
    }
    
    public String getCiclo() {
        return getEntity().getCiclo();
    }

    public void setCiclo(String ciclo) {
        getEntity().setCiclo(ciclo);
    }
    
    public String getEstatus() {
        return getEntity().getEstatus();
    }

    public void setEstatus(String estatus) {
        getEntity().setEstatus(estatus);
    }
    
    public String getTipo() {
        return getEntity().getTipo();
    }

    public void setTipo(String tipo) {
        getEntity().setTipo(tipo);
    }
    
    public Integer getTotal() {
        return getEntity().getTotal();
    }

    public void setTotal(Integer total) {
        getEntity().setTotal(total);
    }
    
    public static List<InscripcionResumenConverter> convert(Collection<InscripcionResumen> registros, int expandLevel) {
        List<InscripcionResumenConverter> converted = new LinkedList<>();

        registros.stream().forEach((registro) -> {
            converted.add(new InscripcionResumenConverter(registro, expandLevel));
        });

        return converted;
    }
}
