/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.productor;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.BancoConverter;
import mx.gob.segalmex.pluss.modelo.productor.CuentaBancaria;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "cuenta-bancaria")
public class CuentaBancariaConverter extends AbstractEntidadConverter {

    private CuentaBancaria entity;

    public CuentaBancariaConverter() {
        entity = new CuentaBancaria();
    }

    public CuentaBancariaConverter(CuentaBancaria entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad CuentaBancaria no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public CuentaBancaria getEntity() {
        return entity;
    }

    public BancoConverter getBanco() {
        return expandLevel > 0 && Objects.nonNull(entity.getBanco())
                ? new BancoConverter(entity.getBanco(), expandLevel - 1) : null;
    }

    public void setBanco(BancoConverter banco) {
        entity.setBanco(banco.getEntity());
    }

    public String getNumero() {
        return entity.getNumero();
    }

    public void setNumero(String numero) {
        entity.setNumero(numero);
    }

    public String getClabe() {
        return entity.getClabe();
    }

    public void setClabe(String clabe) {
        entity.setClabe(clabe);
    }

}
