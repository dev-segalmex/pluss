/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.productor;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CicloAgricolaConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.CultivoConverter;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;
import mx.gob.segalmex.granos.web.modelo.catalogos.TipoProductorConverter;

/**
 *
 * @author ismael
 */
public class ProductorCicloConverter extends AbstractEntidadConverter {

    private final ProductorCiclo entity;

    public ProductorCicloConverter() {
        this.entity = new ProductorCiclo();
        this.expandLevel = 1;
    }

    public ProductorCicloConverter(ProductorCiclo entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad ProductorCiclo no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public ProductorCiclo getEntity() {
        return this.entity;
    }

    @XmlElement
    public ProductorConverter getProductor() {
        return Objects.nonNull(entity.getProductor())
                ? new ProductorConverter(entity.getProductor(), expandLevel - 1) : null;
    }

    public void setProductor(ProductorConverter productor) {
        entity.setProductor(productor.getEntity());
    }

    @XmlElement
    public CultivoConverter getCultivo() {
        return expandLevel > 0 && Objects.nonNull(getEntity().getCultivo())
                ? new CultivoConverter(getEntity().getCultivo(), expandLevel - 1) : null;
    }

    @XmlElement
    public boolean isActivo() {
        return entity.isActivo();
    }

    @XmlElement
    public int getInscripciones() {
        return entity.getInscripciones();
    }

    @XmlElement
    public int getPredios() {
        return entity.getPredios();
    }

    @XmlElement
    public CicloAgricolaConverter getCiclo() {
        return expandLevel > 0 && Objects.nonNull(getEntity().getCiclo())
                ? new CicloAgricolaConverter(getEntity().getCiclo(), expandLevel - 1) : null;
    }

    @XmlElement
    public BigDecimal getToneladasFacturadas() {
        return entity.getToneladasFacturadas();
    }

    @XmlElement
    public BigDecimal getHectareasTotales() {
        return entity.getHectareasTotales();
    }

    @XmlElement
    public BigDecimal getToneladasTotales() {
        return entity.getToneladasTotales();
    }

    @XmlElement
    public String getFolio() {
        return entity.getFolio();
    }

    public TipoProductorConverter getTipoProductor() {
        return expandLevel > 0 && Objects.nonNull(entity.getTipoProductor())
                ? new TipoProductorConverter(entity.getTipoProductor(),
                    expandLevel -1) : null;
    }

    public void setTipoProductor(TipoProductorConverter tipoProductor) {
        entity.setTipoProductor(tipoProductor.getEntity());
    }

    @XmlElement
    public List<ProductorCicloGrupoConverter> getGrupos() {
        if (Objects.isNull(entity.getGrupos()) || entity.getGrupos().isEmpty()) {
            return null;
        }
        return CollectionConverter.convert(ProductorCicloGrupoConverter.class,
                entity.getGrupos(), expandLevel - 1);
    }

}
