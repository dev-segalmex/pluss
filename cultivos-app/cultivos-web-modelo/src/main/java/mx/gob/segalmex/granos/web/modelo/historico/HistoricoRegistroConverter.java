/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.historico;

import java.util.Calendar;
import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.seguridad.UsuarioConverter;
import mx.gob.segalmex.pluss.modelo.historico.HistoricoRegistro;
import mx.gob.segalmex.granos.web.modelo.contrato.TipoHistoricoInscripcionConverter;

/**
 *
 * @author ismael
 */
public class HistoricoRegistroConverter extends AbstractEntidadConverter {

    private final HistoricoRegistro entity;

    public HistoricoRegistroConverter() {
        entity = new HistoricoRegistro();
    }

    public HistoricoRegistroConverter(HistoricoRegistro entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad HistoricoInscripcion no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public HistoricoRegistro getEntity() {
        return entity;
    }

    @XmlTransient
    @Override
    public Integer getId() {
        return super.getId();
    }

    @XmlElement
    public TipoHistoricoInscripcionConverter getTipo() {
        return Objects.nonNull(entity.getTipo()) && expandLevel > 0
                ? new TipoHistoricoInscripcionConverter(entity.getTipo(), expandLevel - 1) : null;
    }

    @XmlElement
    public UsuarioConverter getUsuarioRegistra() {
        return Objects.nonNull(entity.getUsuarioRegistra()) && expandLevel > 0
                ? new UsuarioConverter(entity.getUsuarioRegistra(), expandLevel - 1) : null;
    }

    @XmlElement
    public UsuarioConverter getUsuarioFinaliza() {
        return Objects.nonNull(entity.getUsuarioFinaliza()) && expandLevel > 0
                ? new UsuarioConverter(entity.getUsuarioFinaliza(), expandLevel - 1) : null;
    }

    @XmlElement
    public Calendar getFechaFinaliza() {
        return entity.getFechaFinaliza();
    }

    @XmlTransient
    @Override
    public Calendar getFechaActualizacion() {
        return super.getFechaActualizacion();
    }

}
