/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.productor;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionMolino;
import mx.gob.segalmex.granos.web.modelo.catalogos.SucursalConverter;

/**
 *
 * @author oscar
 */
@XmlRootElement(name = "inscripcion-molino")
public class InscripcionMolinoConverter extends AbstractEntidadConverter {

    private final InscripcionMolino entity;

    public InscripcionMolinoConverter() {
        entity = new InscripcionMolino();
    }

    public InscripcionMolinoConverter(InscripcionMolino entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad InscripcionMolino no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public InscripcionMolino getEntity() {
        return entity;
    }

    public SucursalConverter getMolino() {
        return Objects.nonNull(entity.getMolino())
                ? new SucursalConverter(entity.getMolino(), expandLevel - 1) : null;
    }

    public void setMolino(SucursalConverter molino) {
        getEntity().setMolino(molino.getEntity());
    }

    public int getOrden() {
        return entity.getOrden();
    }

    public void setOrden(int orden) {
        getEntity().setOrden(orden);
    }

}
