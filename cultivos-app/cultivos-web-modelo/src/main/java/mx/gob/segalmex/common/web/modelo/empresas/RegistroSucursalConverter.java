/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.empresas;

import java.math.BigDecimal;
import java.util.Objects;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroSucursal;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.granos.web.modelo.productor.DomicilioConverter;

/**
 *
 * @author jurgen
 */
public class RegistroSucursalConverter extends AbstractEntidadConverter {

    private final RegistroSucursal entity;

    public RegistroSucursalConverter() {
        entity = new RegistroSucursal();
        expandLevel = 1;
    }

    public RegistroSucursalConverter(RegistroSucursal entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad RegistroSucursal no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public RegistroSucursal getEntity() {
        return entity;
    }

    public String getNombre() {
        return entity.getNombre();
    }

    public void setNombre(String nombre) {
        entity.setNombre(nombre);
    }

    public DomicilioConverter getDomicilio() {
        return expandLevel > 0 && Objects.nonNull(entity.getDomicilio())
                ? new DomicilioConverter(entity.getDomicilio(), expandLevel - 1) : null;
    }

    public void setDomicilio(DomicilioConverter domicilio) {
        entity.setDomicilio(domicilio.getEntity());
    }

    public BigDecimal getLatitud() {
        return entity.getLatitud();
    }

    public void setLatitud(BigDecimal latitud) {
        entity.setLatitud(latitud);
    }

    public BigDecimal getLongitud() {
        return entity.getLongitud();
    }

    public void setLongitud(BigDecimal longitud) {
        entity.setLongitud(longitud);
    }

    public String getNombreEnlace() {
        return entity.getNombreEnlace();
    }

    public void setNombreEnlace(String nombreEnlace) {
        entity.setNombreEnlace(nombreEnlace);
    }

    public String getNumeroCelularEnlace() {
        return entity.getNumeroCelularEnlace();
    }

    public void setNumeroCelularEnlace(String numeroCelularEnlace) {
        entity.setNumeroCelularEnlace(numeroCelularEnlace);
    }

    public String getCorreoEnlace() {
        return entity.getCorreoEnlace();
    }

    public void setCorreoEnlace(String correoEnlace) {
        entity.setCorreoEnlace(correoEnlace);
    }

}
