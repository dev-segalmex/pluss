/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.productor;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.EstadoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.MunicipioConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.TipoCultivoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.TipoDocumentoConverter;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "predio-productor")
public class PredioProductorConverter extends AbstractEntidadConverter {

    private final PredioProductor entity;
    
    public PredioProductorConverter() {
        entity = new PredioProductor();
    }

    public PredioProductorConverter(PredioProductor entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad PredioProductor no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public PredioProductor getEntity() {
        return entity;
    }

    public String getFolio() {
        return entity.getFolio();
    }

    public void setFolio(String folio) {
        entity.setFolio(folio);
    }

    public TipoCultivoConverter getTipoCultivo() {
        return expandLevel > 0 && Objects.nonNull(entity.getTipoCultivo())
                ? new TipoCultivoConverter(entity.getTipoCultivo(), expandLevel - 1) : null;
    }

    public void setTipoCultivo(TipoCultivoConverter tipoCultivo) {
        entity.setTipoCultivo(tipoCultivo.getEntity());
    }

    public TipoPosesionConverter getTipoPosesion() {
        return expandLevel > 0 && Objects.nonNull(entity.getTipoPosesion())
                ? new TipoPosesionConverter(entity.getTipoPosesion(), expandLevel - 1) : null;
    }

    public void setTipoPosesion(TipoPosesionConverter tipoPosesion) {
        entity.setTipoPosesion(tipoPosesion.getEntity());
    }

    public TipoDocumentoConverter getTipoDocumentoPosesion() {
        return expandLevel > 0 && Objects.nonNull(entity.getTipoDocumentoPosesion())
                ? new TipoDocumentoConverter(entity.getTipoDocumentoPosesion(), expandLevel - 1) : null;
    }

    public void setTipoDocumentoPosesion(TipoDocumentoConverter tipoDocumentoPosesion) {
        entity.setTipoDocumentoPosesion(tipoDocumentoPosesion.getEntity());
    }

    public RegimenHidricoConverter getRegimenHidrico() {
        return expandLevel > 0 && Objects.nonNull(entity.getRegimenHidrico())
                ? new RegimenHidricoConverter(entity.getRegimenHidrico(), expandLevel - 1) : null;
    }

    public void setRegimenHidrico(RegimenHidricoConverter regimenHidrico) {
        entity.setRegimenHidrico(regimenHidrico.getEntity());
    }

    public BigDecimal getSuperficie() {
        return entity.getSuperficie();
    }

    public void setSuperficie(BigDecimal superficie) {
        entity.setSuperficie(superficie);
    }

    public BigDecimal getVolumen() {
        return entity.getVolumen();
    }

    public void setVolumen(BigDecimal volumen) {
        entity.setVolumen(volumen);
    }

    public BigDecimal getRendimiento() {
        return entity.getRendimiento();
    }

    public void setRendimiento(BigDecimal rendimiento) {
        entity.setRendimiento(rendimiento);
    }

    public EstadoConverter getEstado() {
        return Objects.nonNull(entity.getEstado()) && expandLevel > 0
                ? new EstadoConverter(entity.getEstado(), expandLevel - 1) : null;
    }

    public void setEstado(EstadoConverter estado) {
        entity.setEstado(estado.getEntity());
    }

    public MunicipioConverter getMunicipio() {
        return Objects.nonNull(entity.getMunicipio()) && expandLevel > 0
                ? new MunicipioConverter(entity.getMunicipio(), expandLevel - 1) : null;
    }

    public void setMunicipio(MunicipioConverter municipio) {
        entity.setMunicipio(municipio.getEntity());
    }

    public String getLocalidad() {
        return entity.getLocalidad();
    }

    public void setLocalidad(String localidad) {
        entity.setLocalidad(localidad);
    }

    public BigDecimal getLatitud() {
        return entity.getLatitud();
    }

    public void setLatitud(BigDecimal latitud) {
        entity.setLatitud(latitud);
    }

    public BigDecimal getLongitud() {
        return entity.getLongitud();
    }

    public void setLongitud(BigDecimal longitud) {
        entity.setLongitud(longitud);
    }

    public BigDecimal getLatitud1() {
        return entity.getLatitud1();
    }

    public void setLatitud1(BigDecimal latitud1) {
        entity.setLatitud1(latitud1);
    }

    public BigDecimal getLongitud1() {
        return entity.getLongitud1();
    }

    public void setLongitud1(BigDecimal longitud1) {
        entity.setLongitud1(longitud1);
    }

    public BigDecimal getLatitud2() {
        return entity.getLatitud2();
    }

    public void setLatitud2(BigDecimal latitud2) {
        entity.setLatitud2(latitud2);
    }

    public BigDecimal getLongitud2() {
        return entity.getLongitud2();
    }

    public void setLongitud2(BigDecimal longitud2) {
        entity.setLongitud2(longitud2);
    }

    public BigDecimal getLatitud3() {
        return entity.getLatitud3();
    }

    public void setLatitud3(BigDecimal latitud3) {
        entity.setLatitud3(latitud3);
    }

    public BigDecimal getLongitud3() {
        return entity.getLongitud3();
    }

    public void setLongitud3(BigDecimal longitud3) {
        entity.setLongitud3(longitud3);
    }

    public BigDecimal getLatitud4() {
        return entity.getLatitud4();
    }

    public void setLatitud4(BigDecimal latitud4) {
        entity.setLatitud4(latitud4);
    }

    public BigDecimal getLongitud4() {
        return entity.getLongitud4();
    }

    public void setLongitud4(BigDecimal longitud4) {
        entity.setLongitud4(longitud4);
    }

    @XmlElement
    public Calendar getFechaCancelacion() {
        return entity.getFechaCancelacion();
    }

    @XmlElement
    public int getOrden() {
        return entity.getOrden();
    }

    public BigDecimal getVolumenSolicitado() {
        return entity.getVolumenSolicitado();
    }

    public void setVolumenSolicitado(BigDecimal volumenSolicitado) {
        entity.setVolumenSolicitado(volumenSolicitado);
    }

    public BigDecimal getRendimientoSolicitado() {
        return entity.getRendimientoSolicitado();
    }

    public void setRendimientoSolicitado(BigDecimal rendimientoSolicitado) {
        entity.setRendimientoSolicitado(rendimientoSolicitado);
    }

    public String getSolicitado() {
        return entity.getSolicitado();
    }

    public void setSolicitado(String solicitado) {
        entity.setSolicitado(solicitado);
    }

    public BigDecimal getRendimientoMaximo() {
        return entity.getRendimientoMaximo();
    }

    public void setRendimientoMaximo(BigDecimal rendimientoMaximo) {
        entity.setRendimientoMaximo(rendimientoMaximo);
    }

    public int getCantidadSiembra() {
        return entity.getCantidadSiembra();
    }

    public void setCantidadSiembra(int cantidadSiembra) {
        entity.setCantidadSiembra(cantidadSiembra);
    }

    public int getCantidadRiego() {
        return entity.getCantidadRiego();
    }

    public void setCantidadRiego(int cantidadRiego) {
        entity.setCantidadRiego(cantidadRiego);
    }
    
    
}
