/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.contrato;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "estatus-inscripcion")
public class EstatusInscripcionConverter extends AbstractCatalogoConverter {

    private final EstatusInscripcion entity;

    public EstatusInscripcionConverter() {
        this.entity = new EstatusInscripcion();
        this.expandLevel = 1;
    }

    public EstatusInscripcionConverter(EstatusInscripcion entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad EstatusInscripcion no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public EstatusInscripcion getEntity() {
        return this.entity;
    }
}
