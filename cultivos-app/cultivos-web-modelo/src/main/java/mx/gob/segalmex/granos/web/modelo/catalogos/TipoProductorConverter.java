/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.web.modelo.catalogos;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoProductor;

/**
 *
 * @author jurgen
 */
@XmlRootElement(name = "tipo-productor")
public class TipoProductorConverter extends AbstractCatalogoConverter{

    private final TipoProductor entity;

    public TipoProductorConverter(){
        this.entity = new TipoProductor();
        this.expandLevel = 1;
    }

    public TipoProductorConverter(TipoProductor entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad TipoProductor no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public TipoProductor getEntity() {
        return this.entity;
    }
}
