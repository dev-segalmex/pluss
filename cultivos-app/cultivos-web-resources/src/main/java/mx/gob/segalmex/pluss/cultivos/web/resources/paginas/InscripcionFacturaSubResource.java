/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.paginas;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorUsoCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.ParametrosCatalogo;
import mx.gob.segalmex.common.core.empresas.busqueda.BuscadorSucursal;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.util.CultivoClaveUtil;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.common.web.modelo.inscripcion.CatalogosInscripcionConverter;
import mx.gob.segalmex.granos.core.inscripcion.BuscadorCicloAgricolaActivo;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import mx.gob.segalmex.pluss.cultivos.web.resources.productor.CultivoHelper;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import mx.gob.segalmex.pluss.modelo.catalogos.Region;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author cuecho
 */
@Slf4j
@Component
public class InscripcionFacturaSubResource {

    private static final String APLICA_CONTRATO = ":aplica-contratos";

    private static final String APLICA_AJUSTE_FACTURA = ":aplica-ajuste-factura";

    private static final String APLICA_RECIBO_ETIQUETA = ":aplica-recibo-etiqueta";

    private static final String FECHA_COMPRBANTE = ":fecha-comprobante";

    /**
     * El valor del campo para indicar que es un valor global.
     */
    private static final String GLOBAL = "--";

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private UsuarioSesionContextoHolder contextoSeguridad;

    @Autowired
    private BuscadorSucursal buscadorSucursal;

    @Autowired
    private BuscadorUsoCatalogo buscadorUso;

    @Autowired
    private CultivoHelper cultivoHelper;

    @Autowired
    private BuscadorCicloAgricolaActivo buscadorCicloAgricola;

    @Autowired
    private BuscadorParametro buscadorParametro;

    @GET
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public CatalogosInscripcionConverter getCatalogos() {
        LOGGER.info("carga de catalogo de para la inscripción contrato.");
        Cultivo c = cultivoHelper.getCultivo();
        Usuario u = contextoSeguridad.getUsuario();
        CatalogosInscripcionConverter converter = new CatalogosInscripcionConverter();
        try {
            converter.setCiclos(buscadorCatalogo.busca(CicloAgricola.class));
            converter.setCultivos(buscadorCatalogo.busca(Cultivo.class));
            converter.setEstados(buscadorCatalogo.busca(Estado.class));
            converter.setSucursal(getSucursal());
            List<CicloAgricola> ciclosActivos = buscadorCicloAgricola.busca(c, GLOBAL, u);
            CicloAgricola seleccionado = buscadorCicloAgricola.getCicloSeleccionado(c, ciclosActivos, u, true);
            List<TipoCultivo> tiposCultivo = buscadorUso.busca(TipoCultivo.class,
                    "cultivo:" + CultivoClaveUtil.getClaveCorta(c));
            converter.setTipos(tiposCultivo);
            if (Objects.nonNull(seleccionado)) {
                converter.setAplicaContrato(aplicaParametro(c, seleccionado, APLICA_CONTRATO));
                converter.setEstadosCultivo(buscadorUso.busca(Estado.class,
                        "estado:" + c.getClave() + ":" + seleccionado.getClave()));
                converter.setCicloSeleccionado(seleccionado);
                converter.setAplicaAjusteFactura(aplicaParametro(c, seleccionado, APLICA_AJUSTE_FACTURA));
                converter.setAplicaReciboEtiqueta(aplicaParametro(c, seleccionado, APLICA_RECIBO_ETIQUETA));
                converter.setRegistroCerrado(getRegistroCerrado(c.getClave()
                        + ":" + seleccionado.getClave() + ":facturas:cierre"));
                converter.setRegiones(getRegiones(seleccionado, tiposCultivo));
                converter.setFechaComprobantes(buscadorParametro.buscaElementoOpcional(c.getClave() + ":"
                        + seleccionado.getClave() + FECHA_COMPRBANTE));
            }
        } catch (SegalmexRuntimeException ouch) {
            LOGGER.error("El usuario {} no tiene ciclo seleccionado.", u.getClave());
        }
        return converter;
    }

    @Path("/empresa")
    @GET
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public CatalogosInscripcionConverter getCatalogosEmpresa() {
        LOGGER.info("Carga de catálogo para la inscripción factura ");
        CatalogosInscripcionConverter converter = new CatalogosInscripcionConverter();
        Cultivo c = cultivoHelper.getCultivo();
        Usuario u = contextoSeguridad.getUsuario();
        List<CicloAgricola> ciclosActivos = buscadorCicloAgricola.busca(c, GLOBAL, u);
        CicloAgricola seleccionado = buscadorCicloAgricola.getCicloSeleccionado(c, ciclosActivos, u, true);
        if (Objects.nonNull(seleccionado)) {
            converter.setCicloSeleccionado(seleccionado);
        }
        converter.setCiclos(buscadorCatalogo.busca(CicloAgricola.class));
        return converter;
    }

    @Path("/execpcion-factura")
    @GET
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public CatalogosInscripcionConverter getCatalogosExcepcion() {
        LOGGER.info("Carga de catálogo para excepciones de facturas");
        CatalogosInscripcionConverter converter = new CatalogosInscripcionConverter();
        converter.setCiclos(buscadorCatalogo.busca(CicloAgricola.class));
        converter.setCultivos(buscadorCatalogo.busca(Cultivo.class));
        return converter;
    }

    private Sucursal getSucursal() {
        List<Sucursal> sucursales = buscadorSucursal.busca(contextoSeguridad.getUsuario());
        return sucursales.isEmpty() ? null : sucursales.get(0);
    }

    private boolean aplicaParametro(Cultivo c, CicloAgricola seleccionado, String clave) {
        Parametro p = buscadorParametro.buscaElementoOpcional(c.getClave() + ":" + seleccionado.getClave() + clave);
        return Objects.nonNull(p) && Boolean.valueOf(p.getValor());
    }

    private Parametro getRegistroCerrado(String clave) {
        Parametro parametro = buscadorParametro.buscaElementoOpcional(clave);
        if (Objects.nonNull(parametro)) {
            Calendar cierre = SegalmexDateUtils
                    .parseCalendar(parametro.getValor(), "yyyy-MM-dd'T'HH:mm:ss");
            if (Calendar.getInstance().after(cierre)) {
                return parametro;
            }
        }
        return null;
    }

    private List<Region> getRegiones(CicloAgricola seleccionado, List<TipoCultivo> tiposCultivo) {
        ParametrosCatalogo<Region> parametros = new ParametrosCatalogo<>();
        parametros.setClase(Region.class);
        parametros.setSoloActivos(Boolean.TRUE);
        parametros.getExtraParams().put("ciclo", seleccionado);
        List<Region> regiones = new ArrayList<>();
        for (TipoCultivo tp : tiposCultivo) {
            parametros.getExtraParams().put("tipoCultivo", tp);
            regiones.addAll(buscadorCatalogo.busca(parametros));
        }
        return regiones;
    }

}
