package mx.gob.segalmex.pluss.cultivos.web.resources.paginas;

import javax.ws.rs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Path("/paginas")
@Component
public class PaginaResource {

    @Autowired
    private InscripcionContratoSubResource inscripcionContratoSubResource;

    @Autowired
    private BandejasSubResource bandejaInscripcionSubResource;

    @Autowired
    private BandejaAsignacionSubResource bandejaAsignacionSubResource;

    @Autowired
    private InscripcionProductorSubResource inscripcionProductorSubResource;

    @Autowired
    private ConsultaSubResource consultaSubResource;

    @Autowired
    private InscripcionFacturaSubResource inscripcionFacturaSubResource;

    @Autowired
    private PreRegistroProductorSubResource preRegistroProductorSubResource;

    @Autowired
    private RegistroEmpresaPaginaSubresource empresaRegistroSubReource;

    @Autowired
    private InscripcionProductorEdicionSubResourse inscripcionProductorEdicionSubResourse;

    @Autowired
    private RequerimientoPagoSubResources requerimientoPagoSubResources;

    @Autowired
    private EstimuloSubResourse estimuloSubResourse;

    @Autowired
    private InformacionAlertaSubResource informacionAlertaSubResource;

    @Autowired
    private UsoFacturaSubresource usoFacturaSubresource;

    @Path("/inscripcion")
    public InscripcionContratoSubResource getInscripcionContratoSubResource() {
        return inscripcionContratoSubResource;
    }

    @Path("/inscripcion-productor")
    public InscripcionProductorSubResource getInscripcionProductorSubResource() {
        return inscripcionProductorSubResource;
    }

    @Path("/inscripcion-factura")
    public InscripcionFacturaSubResource getInscripcionFacturaSubResource() {
        return inscripcionFacturaSubResource;
    }

    @Path("/bandejas")
    public BandejasSubResource getBandejaInscripcionSubResource() {
        return bandejaInscripcionSubResource;
    }

    @Path("/bandeja-asignacion")
    public BandejaAsignacionSubResource getBandejaAsignacionSubResource() {
        return bandejaAsignacionSubResource;
    }

    @Path("/consultas")
    public ConsultaSubResource getConsultaSubResource() {
        return consultaSubResource;
    }

    @Path("/productor/pre-registro")
    public PreRegistroProductorSubResource getPreRegistroProductorSubResource() {
        return preRegistroProductorSubResource;
    }

    @Path("/empresa/registro")
    public RegistroEmpresaPaginaSubresource getEmpresaRegistroSubReource() {
        return empresaRegistroSubReource;
    }

    @Path("/inscripcion-productor/edicion")
    public InscripcionProductorEdicionSubResourse getInscripcionProductorEdicionSubResourse() {
        return inscripcionProductorEdicionSubResourse;
    }

    @Path("/requerimientos-pago")
    public RequerimientoPagoSubResources getRequerimientoPagoSubResources() {
        return requerimientoPagoSubResources;
    }

    @Path("/estimulo")
    public EstimuloSubResourse getEstimuloSubResourse() {
        return estimuloSubResourse;
    }

    @Path("/informacion-alerta")
    public InformacionAlertaSubResource getInformacionAlerta() {
        return informacionAlertaSubResource;
    }

    @Path("/uso-factura")
    public UsoFacturaSubresource getUsoFacturaSubresource() {
        return usoFacturaSubresource;
    }
}
