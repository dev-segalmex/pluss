/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.params;

import org.apache.commons.lang3.StringUtils;

/**
 * Entidad para representar un folio, que consiste de una cadena numérica cuando éste es envíado
 * como un parámetros de una petición GET. El folio es de una longitud de 6 caractéres y está
 * rellenado con ceros al principio.
 *
 * @author ismael
 */
public class FolioParam extends AbstractParam<String> {

    /**
     * Constructor de la clase
     *
     * @param param
     */
    public FolioParam(String param) {
        super(param);
    }

    /**
     *
     * {@inheritDoc}
     *
     */
    @Override
    protected String parse(String param) {
        param = StringUtils.trimToNull(param);

        if (param == null) {
            return null;
        }

        if (!StringUtils.isNumeric(param)) {
            throw new IllegalArgumentException("El parámetro: " + param + " no es un número.");
        }

        return StringUtils.leftPad(param, 6, "0");
    }

}
