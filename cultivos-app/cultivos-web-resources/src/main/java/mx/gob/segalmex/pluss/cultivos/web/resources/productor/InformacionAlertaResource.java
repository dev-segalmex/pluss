/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.productor;

import java.util.List;
import java.util.Objects;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.granos.core.productor.ProcesadorInformacionAlerta;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInformacionAlerta;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInformacionAlerta;
import mx.gob.segalmex.granos.web.modelo.productor.InformacionAlertaConverter;
import mx.gob.segalmex.pluss.modelo.productor.InformacionAlerta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Slf4j
@Path("/informacion-alerta/")
@Component
public class InformacionAlertaResource {

    @Autowired
    private BuscadorInformacionAlerta buscadorInformacionAlerta;

    @Autowired
    private ProcesadorInformacionAlerta procesadorInformacionAlerta;

    @Autowired
    private InformacionAlertaResourceHelper resourceHelper;

    @GET
    @Produces("application/json")
    public List<InformacionAlertaConverter> busca() {
        ParametrosInformacionAlerta parametros = new ParametrosInformacionAlerta();
        List<InformacionAlerta> resultados = buscadorInformacionAlerta.busca(parametros);
        return CollectionConverter.convert(InformacionAlertaConverter.class,
                resultados, 1);
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InformacionAlertaConverter registra(InformacionAlertaConverter converter) {
        InformacionAlerta alerta = converter.getEntity();
        resourceHelper.inicializaCatalogos(alerta);
        procesadorInformacionAlerta.procesa(alerta);
        return new InformacionAlertaConverter(alerta, 1);
    }

    @DELETE
    @Consumes("application/json")
    @Produces("application/json")
    public InformacionAlertaConverter elimina(InformacionAlertaConverter converter) {
        if (Objects.isNull(converter.getFolio())) {
            throw new SegalmexRuntimeException("Error al eliminar.", "El Folio no puede ser nulo.");
        }
        InformacionAlerta alerta = buscadorInformacionAlerta.buscaElemento(converter.getFolio());
        procesadorInformacionAlerta.elimina(alerta);
        return new InformacionAlertaConverter(alerta, 1);
    }
}
