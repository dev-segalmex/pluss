/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.paginas;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorUsoCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.ParametrosUsoCatalogo;
import mx.gob.segalmex.common.web.modelo.inscripcion.CatalogosInscripcionConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.Municipio;
import mx.gob.segalmex.pluss.modelo.catalogos.PrefijoEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoSucursal;
import mx.gob.segalmex.pluss.modelo.catalogos.UsoCatalogo;
import mx.gob.segalmex.pluss.modelo.productor.TipoPosesion;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Slf4j
@Component
public class RegistroEmpresaPaginaSubresource {

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorUsoCatalogo buscadorUso;

    @GET
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public CatalogosInscripcionConverter getCatalogos() {
        CatalogosInscripcionConverter converter = new CatalogosInscripcionConverter();
        converter.setEstados(buscadorCatalogo.busca(Estado.class));
        converter.setMunicipios(buscadorCatalogo.busca(Municipio.class));
        converter.setTiposSucursal(buscadorCatalogo.busca(TipoSucursal.class));
        converter.setTiposPosesion(buscadorCatalogo.busca(TipoPosesion.class));
        converter.setUsuarios(getUsuariosDisponibles());

        converter.setEstadosPredio(buscadorUso.busca(Estado.class,
                "estado:maiz-comercial:pv-2020"));
        return converter;
    }

    @GET
    @Path("/basico/")
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public CatalogosInscripcionConverter getCatalogosRegistroEmpresaBasica() {
        CatalogosInscripcionConverter converter = new CatalogosInscripcionConverter();
        converter.setCiclos(buscadorCatalogo.busca(CicloAgricola.class));
        converter.setCultivos(buscadorCatalogo.busca(Cultivo.class));
        return converter;
    }

    private List<Usuario> getUsuariosDisponibles() {
        ParametrosUsoCatalogo params = new ParametrosUsoCatalogo();
        params.setClase(Sucursal.class);
        params.setUso(PrefijoEnum.SUCURSAL_USUARIO.getClave());
        params.setUsoAproximado(true);
        List<UsoCatalogo> usos = buscadorUso.buscaUsos(params);
        List<Usuario> usuarios = new ArrayList<>();
        for (UsoCatalogo uc : usos) {
            String nombre = uc.getUso().split(":")[1];
            try {
                Usuario u = buscadorCatalogo.buscaElemento(Usuario.class, nombre);
                usuarios.add(u);
            } catch (EmptyResultDataAccessException ouch) {
                LOGGER.error("Error al buscar el usuario: {}", nombre);
            }
        }

        List<Usuario> todos = buscadorCatalogo.busca(Usuario.class);
        todos.removeAll(usuarios);

        return todos;
    }
}
