/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.paginas;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.empresas.busqueda.BuscadorRegistroEmpresa;
import mx.gob.segalmex.common.core.empresas.busqueda.ParametrosRegistroEmpresa;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.empresas.RegistroEmpresaConverter;
import mx.gob.segalmex.common.web.modelo.factura.InscripcionFacturaConverter;
import mx.gob.segalmex.granos.core.historico.BuscadorHistoricoRegistro;
import mx.gob.segalmex.granos.core.historico.ParametrosHistoricoRegistro;
import mx.gob.segalmex.granos.web.modelo.contrato.InscripcionContratoConverter;
import mx.gob.segalmex.granos.web.modelo.productor.InscripcionProductorConverter;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import mx.gob.segalmex.pluss.cultivos.web.resources.productor.CultivoHelper;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.Empresa;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroEmpresa;
import mx.gob.segalmex.pluss.modelo.factura.Cfdi;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import mx.gob.segalmex.pluss.modelo.historico.HistoricoRegistro;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.Inscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersona;
import mx.gob.segalmex.pluss.modelo.productor.DatosProductor;
import mx.gob.segalmex.pluss.modelo.productor.EmpresaAsociada;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component
public class BandejasSubResource {

    @Autowired
    private UsuarioSesionContextoHolder contextoSeguridad;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorHistoricoRegistro buscadorHistoricoRegistro;

    @Autowired
    private BuscadorRegistroEmpresa buscadorRegistroEmpresa;

    @Autowired
    private CultivoHelper cultivoHelper;

    /**
     * Obtiene los trámites que tienen estatus <code>nuevo</code>.
     *
     * @param cultivo
     * @param ciclo
     * @return
     */
    @GET
    @Path("/asignacion/contratos/")
    @Produces({"application/json;charset=utf-8"})
    public List<InscripcionContratoConverter> getBandejaAsignacion(@QueryParam("cultivo") String cultivo,
            @QueryParam("ciclo") String ciclo) {
        List<InscripcionContratoConverter> pendientes = new ArrayList<>();
        pendientes.addAll(buscaContratos(TipoHistoricoInscripcionEnum.ASIGNACION,
                EstatusInscripcionEnum.NUEVO, false, cultivo, ciclo));
        return pendientes;
    }

    @GET
    @Path("/asignacion/productores/")
    @Produces({"application/json;charset=utf-8"})
    public List<InscripcionProductorConverter> getBandejaAsignacionProductores(@QueryParam("max") @DefaultValue("1000") int max,
            @QueryParam("cultivo") String cultivo, @QueryParam("ciclo") String ciclo,
            @QueryParam("tipoRegistro") String tipoRegistro, @QueryParam("rfcSociedad") String rfcSociedad) {
        List<InscripcionProductorConverter> pendientes = new ArrayList<>();
        pendientes.addAll(buscaProductores(TipoHistoricoInscripcionEnum.ASIGNACION,
                EstatusInscripcionEnum.NUEVO, false, max, cultivo, ciclo, tipoRegistro, rfcSociedad));
        return pendientes;
    }

    @GET
    @Path("/asignacion/facturas/")
    @Produces({"application/json;charset=utf-8"})
    public List<InscripcionFacturaConverter> getBandejaAsignacionFacturas(@QueryParam("cultivo") String cultivo,
            @QueryParam("ciclo") String ciclo) {
        List<InscripcionFacturaConverter> pendientes = new ArrayList<>();
        pendientes.addAll(buscaFacturas(TipoHistoricoInscripcionEnum.ASIGNACION,
                EstatusInscripcionEnum.NUEVO, false, cultivo, ciclo));
        return pendientes;
    }

    @Path("/asignacion/empresas/")
    @GET
    @Produces("application/json")
    public List<RegistroEmpresaConverter> getBandejaAsignacionEmpresas() {
        ParametrosRegistroEmpresa parametros = new ParametrosRegistroEmpresa();
        parametros.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.NUEVO));
        List<RegistroEmpresa> registros = buscadorRegistroEmpresa.busca(parametros);
        return CollectionConverter.convert(RegistroEmpresaConverter.class,
                simplifica(registros), 2);
    }

    @GET
    @Path("/validacion/contratos/")
    @Produces({"application/json;charset=utf-8"})
    public List<InscripcionContratoConverter> getBandejaValidacion(@QueryParam("cultivo") String cultivo,
            @QueryParam("ciclo") String ciclo) {
        List<InscripcionContratoConverter> pendientes = new ArrayList<>();
        pendientes.addAll(buscaContratos(TipoHistoricoInscripcionEnum.VALIDACION,
                EstatusInscripcionEnum.VALIDACION, true, cultivo, ciclo));
        pendientes.addAll(buscaContratos(TipoHistoricoInscripcionEnum.REVALIDACION,
                EstatusInscripcionEnum.SOLVENTADA, true, cultivo, ciclo));
        pendientes.addAll(buscaContratos(TipoHistoricoInscripcionEnum.SOLICITUD_COMPLEMENTO,
                EstatusInscripcionEnum.POSITIVA, true, cultivo, ciclo));
        pendientes.addAll(buscaContratos(TipoHistoricoInscripcionEnum. VALIDACION_COMPlEMENTO,
                EstatusInscripcionEnum.POSITIVA, true, cultivo, ciclo));
        pendientes.addAll(buscaContratos(TipoHistoricoInscripcionEnum. REVALIDACION_COMPlEMENTO,
                EstatusInscripcionEnum.POSITIVA, true, cultivo, ciclo));
        return pendientes;
    }

    @GET
    @Path("/validacion/productores/")
    @Produces({"application/json;charset=utf-8"})
    public List<InscripcionProductorConverter> getBandejaValidacionProductores(@QueryParam("cultivo") String cultivo,
            @QueryParam("ciclo") String ciclo) {
        List<InscripcionProductorConverter> pendientes = new ArrayList<>();
        pendientes.addAll(buscaProductores(TipoHistoricoInscripcionEnum.VALIDACION,
                EstatusInscripcionEnum.VALIDACION, true, 0, cultivo, ciclo, null, null));
        pendientes.addAll(buscaProductores(TipoHistoricoInscripcionEnum.REVALIDACION,
                EstatusInscripcionEnum.SOLVENTADA, true, 0, cultivo, ciclo, null, null));
        pendientes.addAll(buscaProductores(TipoHistoricoInscripcionEnum.SUPERVISION,
                EstatusInscripcionEnum.SUPERVISION, true, 0, cultivo, ciclo, null, null));
        pendientes.addAll(buscaProductores(TipoHistoricoInscripcionEnum.ESPERA,
                EstatusInscripcionEnum.SOLVENTADA, true, 0, cultivo, ciclo, null, null));
        return pendientes;
    }

    @GET
    @Path("/validacion/facturas/")
    @Produces({"application/json;charset=utf-8"})
    public List<InscripcionFacturaConverter> getBandejaValidacionFacturas(@QueryParam("cultivo") String cultivo,
            @QueryParam("ciclo") String ciclo) {
        List<InscripcionFacturaConverter> pendientes = new ArrayList<>();
        pendientes.addAll(buscaFacturas(TipoHistoricoInscripcionEnum.VALIDACION,
                EstatusInscripcionEnum.VALIDACION, true, cultivo, ciclo));
        pendientes.addAll(buscaFacturas(TipoHistoricoInscripcionEnum.REVALIDACION,
                EstatusInscripcionEnum.SOLVENTADA, true, cultivo, ciclo));
        return pendientes;
    }

    @Path("/validacion/empresas/")
    @GET
    @Produces("application/json")
    public List<RegistroEmpresaConverter> getBandejaValidacionEmpresas() {
        ParametrosRegistroEmpresa parametros = new ParametrosRegistroEmpresa();
        parametros.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.VALIDACION));
        parametros.setUsuarioValidador(contextoSeguridad.getUsuario());
        List<RegistroEmpresa> registros = buscadorRegistroEmpresa.busca(parametros);
        parametros.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.SOLVENTADA));
        registros.addAll(buscadorRegistroEmpresa.busca(parametros));
        return CollectionConverter.convert(RegistroEmpresaConverter.class,
                simplifica(registros), 2);
    }

    @GET
    @Path("/correccion/contratos/")
    @Produces({"application/json;charset=utf-8"})
    public List<InscripcionContratoConverter> getBandejaCorreccion() {
        List<InscripcionContratoConverter> pendientes = new ArrayList<>();
        pendientes.addAll(buscaContratos(TipoHistoricoInscripcionEnum.CORRECCION,
                EstatusInscripcionEnum.CORRECCION, true, null, null));
        pendientes.addAll(buscaContratos(TipoHistoricoInscripcionEnum.DOCUMENTACION_COBERTURA,
                EstatusInscripcionEnum.POSITIVA, true, null, null));
        pendientes.addAll(buscaContratos(TipoHistoricoInscripcionEnum.CORRECION_COMPlEMENTO,
                EstatusInscripcionEnum.POSITIVA, true, null, null));
        pendientes.addAll(buscaContratos(TipoHistoricoInscripcionEnum.CORRECION_CONTRATO_PRODUCTOR,
                EstatusInscripcionEnum.CORRECCION, true, null, null));
        return pendientes;
    }

    @GET
    @Path("/correccion/productores/")
    @Produces({"application/json;charset=utf-8"})
    public List<InscripcionProductorConverter> getBandejaCorreccionProductores() {
        List<InscripcionProductorConverter> pendientes = new ArrayList<>();
        pendientes.addAll(buscaProductores(TipoHistoricoInscripcionEnum.DOCUMENTACION,
                EstatusInscripcionEnum.PENDIENTE, true, 0, null, null, null, null));
        pendientes.addAll(buscaProductores(TipoHistoricoInscripcionEnum.CORRECCION,
                EstatusInscripcionEnum.CORRECCION, true, 0, null, null, null, null));
        pendientes.addAll(buscaProductores(TipoHistoricoInscripcionEnum.EDICION,
                EstatusInscripcionEnum.PENDIENTE, true, 0, null, null, null, null));
        return pendientes;
    }

    @GET
    @Path("/correccion/facturas/")
    @Produces({"application/json;charset=utf-8"})
    public List<InscripcionFacturaConverter> getBandejaCorreccionFacturas() {
        List<InscripcionFacturaConverter> pendientes = new ArrayList<>();
        pendientes.addAll(buscaFacturas(TipoHistoricoInscripcionEnum.CORRECCION,
                EstatusInscripcionEnum.CORRECCION, true, null, null));
        return pendientes;
    }

    private List<InscripcionContratoConverter> buscaContratos(TipoHistoricoInscripcionEnum tipo,
            EstatusInscripcionEnum estatus, boolean usuarioAsignado, String cultivo, String ciclo) {
        ParametrosHistoricoRegistro params = new ParametrosHistoricoRegistro();
        params.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, estatus.getClave()));
        params.setTipo(buscadorCatalogo.buscaElemento(TipoHistoricoInscripcion.class, tipo.getClave()));
        params.setMonitoreable(Boolean.TRUE);
        params.setUsuarioAsignado(usuarioAsignado ? contextoSeguridad.getUsuario() : null);
        params.setClase(InscripcionContrato.class);
        params.setCultivo(Objects.nonNull(cultivo)
                ? buscadorCatalogo.buscaElemento(Cultivo.class, cultivo) : null);
        params.setCiclo(Objects.nonNull(ciclo)
                ? buscadorCatalogo.buscaElemento(CicloAgricola.class, ciclo) : null);
        switch (tipo) {
            case CORRECCION:
                params.setCultivo(cultivoHelper.getCultivo());
        }

        List<HistoricoRegistro> historicos = buscadorHistoricoRegistro.busca(params);
        return convierteContratos(historicos);
    }

    private List<InscripcionProductorConverter> buscaProductores(TipoHistoricoInscripcionEnum tipo,
            EstatusInscripcionEnum estatus, boolean usuarioAsignado, int maximo, String cultivo, String ciclo,
            String tipoRegistro, String rfcSociedad) {
        ParametrosHistoricoRegistro params = new ParametrosHistoricoRegistro();
        params.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, estatus.getClave()));
        params.setTipo(buscadorCatalogo.buscaElemento(TipoHistoricoInscripcion.class, tipo.getClave()));
        params.setMonitoreable(Boolean.TRUE);
        params.setUsuarioAsignado(usuarioAsignado ? contextoSeguridad.getUsuario() : null);
        params.setMaximo(maximo);
        params.setClase(InscripcionProductor.class);
        params.setCultivo(Objects.nonNull(cultivo)
                ? buscadorCatalogo.buscaElemento(Cultivo.class, cultivo) : null);
        params.setCiclo(Objects.nonNull(ciclo)
                ? buscadorCatalogo.buscaElemento(CicloAgricola.class, ciclo) : null);
        params.setTipoRegistro(tipoRegistro);
        switch (tipo) {
            case DOCUMENTACION:
            case CORRECCION:
            case EDICION:
                params.setCultivo(cultivoHelper.getCultivo());
        }

        List<HistoricoRegistro> historicos = buscadorHistoricoRegistro.busca(params);
        List<HistoricoRegistro> filtrados = getFiltradosSociedad(historicos, rfcSociedad);
        return convierteProductores(filtrados);
    }

    private List<InscripcionFacturaConverter> buscaFacturas(TipoHistoricoInscripcionEnum tipo,
            EstatusInscripcionEnum estatus, boolean usuarioAsignado, String cultivo, String ciclo) {
        ParametrosHistoricoRegistro params = new ParametrosHistoricoRegistro();
        params.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, estatus.getClave()));
        params.setTipo(buscadorCatalogo.buscaElemento(TipoHistoricoInscripcion.class, tipo.getClave()));
        params.setMonitoreable(Boolean.TRUE);
        params.setUsuarioAsignado(usuarioAsignado ? contextoSeguridad.getUsuario() : null);
        params.setClase(InscripcionFactura.class);
        params.setCultivo(Objects.nonNull(cultivo)
                ? buscadorCatalogo.buscaElemento(Cultivo.class, cultivo) : null);
        params.setCiclo(Objects.nonNull(ciclo)
                ? buscadorCatalogo.buscaElemento(CicloAgricola.class, ciclo) : null);
        switch (tipo) {
            case CORRECCION:
                params.setCultivo(cultivoHelper.getCultivo());
        }

        List<HistoricoRegistro> historicos = buscadorHistoricoRegistro.busca(params);
        return convierteFacturas(historicos);
    }

    private List<InscripcionContratoConverter> convierteContratos(List<HistoricoRegistro> historicos) {
        List<InscripcionContratoConverter> converters = new ArrayList<>();
        Map<String, String> folios = new HashMap<>();
        for (HistoricoRegistro h : historicos) {
            InscripcionContrato i = simplifica((InscripcionContrato) h.getEntidad());
            i.setFechaCreacion(h.getFechaCreacion());
            if (!existe(i, folios)) {
                InscripcionContratoConverter c = new InscripcionContratoConverter(i, 2);
                c.setTipo(h.getTipo().getClave());
                converters.add(c);
            }
        }
        return converters;
    }

    private List<InscripcionProductorConverter> convierteProductores(List<HistoricoRegistro> historicos) {
        List<InscripcionProductorConverter> converters = new ArrayList<>();
        Map<String, String> folios = new HashMap<>();
        for (HistoricoRegistro h : historicos) {
            InscripcionProductor i = simplifica((InscripcionProductor) h.getEntidad());
            i.setFechaCreacion(h.getFechaCreacion());
            if (!existe(i, folios)) {
                InscripcionProductorConverter c = new InscripcionProductorConverter(i, 2);
                List<String> asociadas = new ArrayList<>();
                for (EmpresaAsociada ea : ((InscripcionProductor) h.getEntidad()).getEmpresasActivas()) {
                    asociadas.add(ea.getRfc());
                }
                c.setRfcEmpresas(asociadas);
                c.setTipo(h.getTipo().getClave());
                converters.add(c);
            }
        }
        return converters;
    }

    private List<InscripcionFacturaConverter> convierteFacturas(List<HistoricoRegistro> historicos) {
        List<InscripcionFacturaConverter> converters = new ArrayList<>();
        Map<String, String> folios = new HashMap<>();
        for (HistoricoRegistro h : historicos) {
            InscripcionFactura i = simplifica((InscripcionFactura) h.getEntidad());
            i.setFechaCreacion(h.getFechaCreacion());
            if (!existe(i, folios)) {
                InscripcionFacturaConverter c = new InscripcionFacturaConverter(i, 1);
                c.setTipo(h.getTipo().getClave());
                converters.add(c);
            }
        }
        return converters;
    }

    private boolean existe(Inscripcion i, Map<String, String> folios) {
        boolean existe = Objects.nonNull(folios.get(i.getFolio()));
        if (!existe) {
            folios.put(i.getFolio(), i.getFolio());
        }
        return existe;
    }

    private InscripcionContrato simplifica(InscripcionContrato inscripcion) {
        InscripcionContrato simplificado = new InscripcionContrato();
        simplificado.setUuid(inscripcion.getUuid());
        simplificado.setFolio(inscripcion.getFolio());
        simplificado.setSucursal(new Sucursal());
        simplificado.getSucursal().setEmpresa(new Empresa());
        simplificado.getSucursal().getEmpresa().setNombre(inscripcion.getSucursal().getEmpresa().getNombre());
        simplificado.setFechaCreacion(inscripcion.getFechaCreacion());
        simplificado.setCiclo(new CicloAgricola());
        simplificado.getCiclo().setClave(inscripcion.getCiclo().getNombre());
        simplificado.setCultivo(new Cultivo());
        simplificado.getCultivo().setNombre(inscripcion.getCultivo().getNombre());
        return simplificado;
    }

    private InscripcionProductor simplifica(InscripcionProductor inscripcion) {
        InscripcionProductor simplificado = new InscripcionProductor();
        simplificado.setUuid(inscripcion.getUuid());
        simplificado.setFolio(inscripcion.getFolio());
        simplificado.setFechaCreacion(inscripcion.getFechaCreacion());
        simplificado.setDatosProductor(new DatosProductor());
        simplificado.getDatosProductor().setNombre(inscripcion.getDatosProductor().getNombre());
        simplificado.getDatosProductor().setPrimerApellido(inscripcion.getDatosProductor().getPrimerApellido());
        simplificado.getDatosProductor().setSegundoApellido(inscripcion.getDatosProductor().getSegundoApellido());
        simplificado.getDatosProductor().setNombreMoral(inscripcion.getDatosProductor().getNombreMoral());
        simplificado.getDatosProductor().setTipoPersona(new TipoPersona());
        simplificado.getDatosProductor().getTipoPersona().setNombre(inscripcion.getDatosProductor().getTipoPersona().getNombre());
        simplificado.setCiclo(new CicloAgricola());
        simplificado.getCiclo().setClave(inscripcion.getCiclo().getNombre());
        simplificado.setCultivo(new Cultivo());
        simplificado.getCultivo().setNombre(inscripcion.getCultivo().getNombre());
        return simplificado;
    }

    private InscripcionFactura simplifica(InscripcionFactura inscripcion) {
        InscripcionFactura simplificado = new InscripcionFactura();
        simplificado.setUuid(inscripcion.getUuid());
        simplificado.setFolio(inscripcion.getFolio());
        simplificado.setFechaCreacion(inscripcion.getFechaCreacion());
        simplificado.setCfdi(new Cfdi());
        simplificado.getCfdi().setNombreEmisor(inscripcion.getCfdi().getNombreEmisor());
        simplificado.getCfdi().setRfcEmisor(inscripcion.getCfdi().getRfcEmisor());
        simplificado.getCfdi().setNombreReceptor(inscripcion.getCfdi().getNombreReceptor());
        simplificado.getCfdi().setRfcReceptor(inscripcion.getCfdi().getRfcReceptor());
        simplificado.setCiclo(new CicloAgricola());
        simplificado.getCiclo().setClave(inscripcion.getCiclo().getNombre());
        simplificado.setCultivo(new Cultivo());
        simplificado.getCultivo().setNombre(inscripcion.getCultivo().getNombre());
        return simplificado;
    }

    public List<RegistroEmpresa> simplifica(List<RegistroEmpresa> registros) {
        List<RegistroEmpresa> simplificados = new ArrayList<>();
        for (RegistroEmpresa item : registros) {
            RegistroEmpresa r = new RegistroEmpresa();
            r.setId(item.getId());
            r.setRfc(item.getRfc());
            r.setUuid(item.getUuid());
            r.setNombre(item.getNombre());
            r.setNombreResponsable(item.getNombreResponsable());
            r.setCiclo(item.getCiclo());
            r.setCultivo(item.getCultivo());
            r.setEstatus(item.getEstatus());
            simplificados.add(r);
        }
        return simplificados;
    }

    /**
     * Dada la clave de cultivo y ciclo regresa la etiquetaGrupo adecuada.
     *
     * @param cultivo
     * @param ciclo
     * @return
     */
    private String getEtiquetaGrupo(String cultivo, String ciclo) {
        if (Objects.nonNull(StringUtils.trimToNull(cultivo))
                && Objects.nonNull(StringUtils.trimToNull(ciclo))) {
            return cultivo + ":" + ciclo;
        }
        return null;
    }

    private List<HistoricoRegistro> getFiltradosSociedad(List<HistoricoRegistro> historicos, String rfcSociedad) {
        if (Objects.isNull(rfcSociedad)) {
            return historicos;
        }
        List<HistoricoRegistro> filtrados = new ArrayList<>();
        for (HistoricoRegistro h : historicos) {
            InscripcionProductor i = ((InscripcionProductor) h.getEntidad());
            for (EmpresaAsociada e : i.getEmpresasActivas()) {
                if (e.getRfc().equals(rfcSociedad)) {
                    filtrados.add(h);
                }
            }
        }
        return filtrados;
    }
}
