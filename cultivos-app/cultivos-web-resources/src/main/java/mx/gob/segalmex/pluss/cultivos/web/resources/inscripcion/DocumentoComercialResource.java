/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.inscripcion;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.archivos.ArchivoUtils;
import mx.gob.segalmex.common.core.archivos.RegistroArchivoRelacionado;
import mx.gob.segalmex.common.core.archivos.SubDirectorioHelper;
import mx.gob.segalmex.common.core.empresas.busqueda.BuscadorSucursal;
import mx.gob.segalmex.common.core.factura.ProcesadorInscripcionDocumentoComercial;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorInscripcionDocumentoComercial;
import mx.gob.segalmex.common.core.factura.busqueda.ParametrosInscripcionDocumentoComercial;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.RespuestaConverter;
import mx.gob.segalmex.common.web.modelo.factura.InscripcionDocumentoComercialConverter;
import mx.gob.segalmex.common.web.resources.archivos.ArchivoFormUtils;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import mx.gob.segalmex.pluss.cultivos.web.resources.productor.CultivoHelper;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import mx.gob.segalmex.pluss.modelo.archivos.ArchivoRelacionado;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionDocumentoComercial;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Recursos para el manejo de {@link InscripcionDocumentoComercial}.
 *
 * @author oscar
 */
@Slf4j
@Path("/documento-comercial")
@Component
public class DocumentoComercialResource {

    @Autowired
    private UsuarioSesionContextoHolder contextoSeguridad;

    @Autowired
    private RegistroArchivoRelacionado registroArchivoRelacionado;

    @Autowired
    private BuscadorInscripcionDocumentoComercial buscadorDocumentoComercial;

    @Autowired
    private ProcesadorInscripcionDocumentoComercial procesadorDocumentoComercial;

    @Autowired
    private BuscadorSucursal buscadorSucursal;

    @Autowired
    private CultivoHelper cultivoHelper;

    @Path("/maiz/inscripcion/")
    @POST
    @Consumes({"application/json;charset=utf-8"})
    @Produces({"application/json;charset=utf-8"})
    public InscripcionDocumentoComercialConverter registra(InscripcionDocumentoComercialConverter converter) {
        LOGGER.debug("Inscribiendo un documento comercial...");
        InscripcionDocumentoComercial documento = converter.getEntity();
        if (Objects.nonNull(converter.getUuid())) {
            return converter;
        }
        documento.setSucursal(buscadorSucursal.buscaElemento(contextoSeguridad.getUsuario()));
        documento.setBodega(documento.getSucursal());
        documento.setCultivo(cultivoHelper.getCultivo());
        procesadorDocumentoComercial.procesa(documento, contextoSeguridad.getUsuario());
        return new InscripcionDocumentoComercialConverter(documento, 1);
    }

    @Path("/maiz/inscripcion/{uuid}/anexos/{archivo}/pdf/")
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces({"application/json;charset=utf-8"})
    public RespuestaConverter registraAnexos(@PathParam("uuid") String uuid,
            @PathParam("archivo") String tipoArchivo, MultipartFormDataInput input) throws IOException {
        LOGGER.debug("Agregando un anexo al documento comercial...");

        InscripcionDocumentoComercial documento = buscadorDocumentoComercial.buscaElemento(uuid);

        InputStream is = ArchivoFormUtils.getInputStrem(input);
        String nombre = ArchivoFormUtils.getFilename(input, "pdf");
        String subidirectorio = SubDirectorioHelper.getSubdirectorio(documento.getCiclo().getClave(),
                "documento-comercial", documento.getFolio());
        Archivo archivo = ArchivoUtils.getInstance(nombre, "maiz", subidirectorio, false);

        ArchivoRelacionado ar = new ArchivoRelacionado();
        ar.setArchivo(archivo);
        ar.setUsuarioRegistra(contextoSeguridad.getUsuario());
        ar.setClase(documento.getClass().getSimpleName());
        ar.setReferencia(documento.getUuid());
        ar.setTipo(tipoArchivo);

        registroArchivoRelacionado.registra(archivo, ar, is);

        RespuestaConverter respuesta = new RespuestaConverter();
        respuesta.setMensaje("Archivo almacenado correctamente: " + tipoArchivo);
        return respuesta;
    }

    @Path("/maiz/recibidos/")
    @GET
    @Produces("application/json")
    public List<InscripcionDocumentoComercialConverter> getDocumentosRecibidos() {
        ParametrosInscripcionDocumentoComercial parametros = new ParametrosInscripcionDocumentoComercial();
        parametros.setEmpresa(buscadorSucursal.buscaElemento(contextoSeguridad.getUsuario()).getEmpresa());
        parametros.setCultivo(cultivoHelper.getCultivo());
        List<InscripcionDocumentoComercial> resultados = buscadorDocumentoComercial.busca(parametros);
        return CollectionConverter.convert(InscripcionDocumentoComercialConverter.class,
                simplificaEntidades(resultados), 2);
    }

    /**
     * Método para simplificar la información de las entindades que se envian al
     * cliente.
     *
     * @param resultados lista a simplificar.
     * @return una lista con las entidades simpliicadas.
     */
    private List<InscripcionDocumentoComercial> simplificaEntidades(List<InscripcionDocumentoComercial> resultados) {
        List<InscripcionDocumentoComercial> simplificados = new ArrayList<>();
        resultados.stream().map((item) -> {
            InscripcionDocumentoComercial simplificado = new InscripcionDocumentoComercial();
            simplificado.setFechaCreacion(item.getFechaCreacion());
            simplificado.setFolio(item.getFolio());
            simplificado.setTipo(item.getTipo());
            simplificado.setCantidad(item.getCantidad());
            return simplificado;
        }).forEachOrdered((simplificado) -> {
            simplificados.add(simplificado);
        });
        return simplificados;
    }
}
