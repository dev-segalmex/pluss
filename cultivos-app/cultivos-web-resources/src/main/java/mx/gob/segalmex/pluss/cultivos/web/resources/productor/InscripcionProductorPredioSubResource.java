/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.productor;

import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.granos.core.productor.ProcesadorInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.granos.web.modelo.productor.InscripcionProductorConverter;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Slf4j
@Component
public class InscripcionProductorPredioSubResource {

    @Autowired
    private BuscadorInscripcionProductor buscadorInscripcionProductor;

    @Autowired
    private ProcesadorInscripcionProductor procesadorInscripcionProductor;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private UsuarioSesionContextoHolder contextoSeguridad;

    @Path("/{index}")
    @DELETE
    public InscripcionProductorConverter eliminaPredio(@PathParam("uuid") String uuid, @PathParam("index") Integer index) {
        InscripcionProductor inscripcion = buscadorInscripcionProductor.buscaElemento(uuid);
        procesadorInscripcionProductor.eliminaPredio(inscripcion, index, contextoSeguridad.getUsuario());
        return new InscripcionProductorConverter(inscripcion, 1);
    }

    @Path("/{index}/permiso-siembra/cantidad/{total}")
    @PUT
    public InscripcionProductorConverter permisoSiembraTotal(@PathParam("uuid") String uuid,
            @PathParam("index") Integer index, @PathParam("total") Integer total) {
        try {
            LOGGER.info("Actualizando cantidades permiso siembra.");
            ParametrosInscripcionProductor params = new ParametrosInscripcionProductor();
            params.setUuid(uuid);
            params.getIncluirEstatus().add(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.VALIDACION));
            params.getIncluirEstatus().add(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.SOLVENTADA));
            params.setCultivo(buscadorCatalogo.buscaElemento(Cultivo.class, CultivoEnum.MAIZ_COMERCIAL));
            InscripcionProductor inscripcion = buscadorInscripcionProductor.buscaElemento(params);
            procesadorInscripcionProductor.actualizaTotalSiembra(inscripcion, index,
                    total, contextoSeguridad.getUsuario());
            return new InscripcionProductorConverter(inscripcion, 1);
        } catch (EmptyResultDataAccessException ouch) {
            throw new SegalmexRuntimeException("Error", "El registro está en un "
                    + "estatus incorrecto para realizar esta acción.");
        }
    }

    @Path("/{index}/pago-riego/cantidad/{total}")
    @PUT
    public InscripcionProductorConverter pagoRiegoTotal(@PathParam("uuid") String uuid,
            @PathParam("index") Integer index, @PathParam("total") Integer total) {
        try {

            LOGGER.info("Actualizando cantidades pago riego.");
            ParametrosInscripcionProductor params = new ParametrosInscripcionProductor();
            params.setUuid(uuid);
            params.getIncluirEstatus().add(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.VALIDACION));
            params.getIncluirEstatus().add(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.SOLVENTADA));
            params.setCultivo(buscadorCatalogo.buscaElemento(Cultivo.class, CultivoEnum.MAIZ_COMERCIAL));
            InscripcionProductor inscripcion = buscadorInscripcionProductor.buscaElemento(params);
            procesadorInscripcionProductor.actualizaTotalRiego(inscripcion, index,
                    total, contextoSeguridad.getUsuario());
            return new InscripcionProductorConverter(inscripcion, 1);
        } catch (EmptyResultDataAccessException ouch) {
            throw new SegalmexRuntimeException("Error", "El registro está en un "
                    + "estatus incorrecto para realizar esta acción.");
        }
    }

    @Path("/{index}/punto-medio/")
    @POST
    public InscripcionProductorConverter predioPuntoMedio(@PathParam("uuid") String uuid, @PathParam("index") Integer index) {
        try {
            ParametrosInscripcionProductor params = new ParametrosInscripcionProductor();
            params.setUuid(uuid);
            params.getIncluirEstatus().add(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.VALIDACION));
            params.getIncluirEstatus().add(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.SOLVENTADA));
            InscripcionProductor inscripcion = buscadorInscripcionProductor.buscaElemento(params);
            procesadorInscripcionProductor.cambiaPuntoMedio(inscripcion, index, contextoSeguridad.getUsuario());
            return new InscripcionProductorConverter(inscripcion, 1);
        } catch (EmptyResultDataAccessException ouch) {
            throw new SegalmexRuntimeException("Error", "El registro está en un "
                    + "estatus incorrecto para realizar esta acción.");
        }

    }

    @Path("/{index}/poligono/")
    @POST
    public InscripcionProductorConverter predioPoligono(@PathParam("uuid") String uuid, @PathParam("index") Integer index) {
        try {
            ParametrosInscripcionProductor params = new ParametrosInscripcionProductor();
            params.setUuid(uuid);
            params.getIncluirEstatus().add(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.VALIDACION));
            params.getIncluirEstatus().add(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.SOLVENTADA));
            InscripcionProductor inscripcion = buscadorInscripcionProductor.buscaElemento(params);
            procesadorInscripcionProductor.cambiaPoligono(inscripcion, index, contextoSeguridad.getUsuario());
            return new InscripcionProductorConverter(inscripcion, 1);
        } catch (EmptyResultDataAccessException ouch) {
            throw new SegalmexRuntimeException("Error", "El registro está en un "
                    + "estatus incorrecto para realizar esta acción.");
        }
    }
}
