/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.inscripcion;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorUsoFactura;
import mx.gob.segalmex.common.core.factura.busqueda.ParametrosInscripcionFactura;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.factura.EstatusFacturaEnum;
import mx.gob.segalmex.pluss.modelo.factura.UsoFactura;
import mx.gob.segalmex.pluss.modelo.pago.LayoutEnum;
import mx.gob.segalmex.pluss.modelo.pago.RequerimientoPago;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Component
public class RequerimientoPagoResourceHelper {

    @Autowired
    private BuscadorUsoFactura buscador;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    public List<UsoFactura> getUsosFactura(List<String> uuidUsos) {
        List<UsoFactura> usosFactura = new ArrayList<>();
        for (String uuid : uuidUsos) {
            usosFactura.add(buscador.buscaElementoUuid(uuid));
        }
        return usosFactura;
    }

    public List<RequerimientoPago> simplificaPropiedades(List<RequerimientoPago> requerimientos) {

        List<RequerimientoPago> simplificados = new ArrayList<>();
        for (RequerimientoPago r : requerimientos) {
            RequerimientoPago simplificado = new RequerimientoPago();
            simplificado.setUuid(r.getUuid());
            simplificado.setId(r.getId());
            simplificado.setFechaCreacion(r.getFechaCreacion());
            simplificado.setCegap(r.getCegap());
            simplificado.setFacturas(r.getFacturas());
            simplificado.setMontoTotal(r.getMontoTotal());
            simplificado.setToneladasTotales(r.getToneladasTotales());
            simplificado.setFacturasNoPago(r.getFacturasNoPago());
            simplificado.setMontoTotalNoPago(r.getMontoTotalNoPago());
            simplificado.setToneladasNoPago(r.getToneladasNoPago());
            simplificado.setPrograma(r.getPrograma());
            simplificado.setCultivo(r.getCultivo());
            simplificado.setCiclo(new CicloAgricola());
            simplificado.getCiclo().setNombre(r.getCiclo().getNombre());
            simplificado.getCiclo().setId(r.getCiclo().getId());
            simplificado.getCiclo().setClave(r.getCiclo().getClave());
            simplificados.add(simplificado);
        }
        return simplificados;
    }

    public ParametrosInscripcionFactura getInstance(InscripcionParam param) {
        ParametrosInscripcionFactura parametros = new ParametrosInscripcionFactura();

        parametros.setFechaInicio(Objects.nonNull(param.getFechaInicio())
                ? param.getFechaInicio().getValue() : null);
        if (Objects.nonNull(param.getFechaFin())) {
            parametros.setFechaFin(param.getFechaFin().getValue());
            parametros.getFechaFin().add(Calendar.DATE, 1);
        }
        parametros.setFolio(Objects.nonNull(param.getFolio()) ? param.getFolio().getValue() : null);
        parametros.setEstatusFactura(EstatusFacturaEnum.ASIGNADA.getClave());

        return parametros;
    }

    public void inicializaCatalogos(RequerimientoPago rp){
        rp.setCultivo(buscadorCatalogo.buscaElemento(Cultivo.class, rp.getCultivo().getId()));
        rp.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, rp.getCiclo().getId()));
        rp.setLayout(LayoutEnum.getInstance(rp.getLayout()).getClave());
    }
}
