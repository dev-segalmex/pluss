/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.productor;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorUsoCatalogo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.cultivos.web.resources.inscripcion.InscripcionParam;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.Municipio;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoIar;
import mx.gob.segalmex.pluss.modelo.catalogos.UsoCatalogo;
import mx.gob.segalmex.pluss.modelo.productor.CoberturaSeleccionada;
import mx.gob.segalmex.pluss.modelo.productor.EntidadCobertura;
import mx.gob.segalmex.pluss.modelo.productor.PreRegistroProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioPreRegistro;
import mx.gob.segalmex.pluss.modelo.productor.RegimenHidrico;
import mx.gob.segalmex.pluss.modelo.productor.TipoPosesion;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Component
public class ProductorPreRegistroResourceHelper {

    private static final String TIPO_IAR = "ninguno";

    private static final String COBERTURA = "no-aplica";

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private CultivoHelper cultivoHelper;

    @Autowired
    private BuscadorUsoCatalogo buscadorUso;

    public void inicializaCatalogos(PreRegistroProductor pre) {
        if (pre.getPredios().isEmpty()) {
            throw new IllegalArgumentException("La lista de predios esta vacia.");
        }
        pre.setCultivo(buscadorCatalogo.buscaElemento(Cultivo.class, pre.getCultivo().getClave()));
        switch (pre.getCultivo().getClave()) {
            case "maiz-comercial":
            case "trigo":
                inicializaCatalogosPre(pre);
                break;
            case "arroz":
                inicializaCatalogosArroz(pre);
                break;
            default:
                throw new SegalmexRuntimeException("Error de Cultivo.",
                        "No existe el cultivo especificado");
        }
    }

    public void inicializaCatalogosPre(PreRegistroProductor pre) {
        List<UsoCatalogo> cicloActivo = buscadorUso.buscaUsos(
                CicloAgricola.class, pre.getCultivo().getClave() + ":pre-registro:ciclo-activo");
        pre.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, cicloActivo.get(0).getClave()));
        for (PredioPreRegistro p : pre.getPredios()) {
            p.setEstado(buscadorCatalogo.buscaElemento(Estado.class, p.getEstado().getId()));
            p.setMunicipio(buscadorCatalogo.buscaElemento(Municipio.class, p.getMunicipio().getId()));
            p.setRegimenHidrico(buscadorCatalogo.buscaElemento(RegimenHidrico.class, p.getRegimenHidrico().getId()));
            p.setTipoCultivo(buscadorCatalogo.buscaElemento(TipoCultivo.class, p.getTipoCultivo().getId()));
            p.setTipoPosesion(buscadorCatalogo.buscaElemento(TipoPosesion.class, p.getTipoPosesion().getId()));
        }

        if (Objects.nonNull(pre.getCoberturas())) {
            for (CoberturaSeleccionada cs : pre.getCoberturas()) {
                cs.setEntidadCobertura(Objects.nonNull(cs.getEntidadCobertura())
                        ? buscadorCatalogo.buscaElemento(EntidadCobertura.class, cs.getEntidadCobertura().getId())
                        : buscadorCatalogo.buscaElemento(EntidadCobertura.class, COBERTURA));
                cs.setTipoIar(Objects.nonNull(cs.getTipoIar())
                        ? buscadorCatalogo.buscaElemento(TipoIar.class, cs.getTipoIar().getClave())
                        : buscadorCatalogo.buscaElemento(TipoIar.class, TIPO_IAR));
            }
        }
        pre.setCorreoElectronico(StringUtils.stripToNull(pre.getCorreoElectronico()));
        pre.setTelefono(StringUtils.stripToNull(pre.getTelefono()));
        Objects.requireNonNull(pre.getCorreoElectronico(), "El correo electrónico es requerido.");
        Objects.requireNonNull(pre.getTelefono(), "El teléfono es requerido.");
    }

    public void inicializaCatalogosArroz(PreRegistroProductor pre) {
        List<UsoCatalogo> cicloActivo = buscadorUso.buscaUsos(
                CicloAgricola.class, CultivoEnum.ARROZ.getClave() + ":pre-registro:ciclo-activo");
        pre.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, cicloActivo.get(0).getClave()));

        for (PredioPreRegistro p : pre.getPredios()) {
            p.setEstado(buscadorCatalogo.buscaElemento(Estado.class, p.getEstado().getId()));
            p.setMunicipio(buscadorCatalogo.buscaElemento(Municipio.class, p.getMunicipio().getId()));
            p.setRegimenHidrico(buscadorCatalogo.buscaElemento(RegimenHidrico.class, p.getRegimenHidrico().getId()));
            p.setTipoCultivo(buscadorCatalogo.buscaElemento(TipoCultivo.class, p.getTipoCultivo().getId()));
            p.setTipoPosesion(buscadorCatalogo.buscaElemento(TipoPosesion.class, p.getTipoPosesion().getId()));
        }
        pre.setCorreoElectronico(StringUtils.stripToNull(pre.getCorreoElectronico()));
        pre.setTelefono(StringUtils.stripToNull(pre.getTelefono()));
        Objects.requireNonNull(pre.getCorreoElectronico(), "El correo electrónico es requerido.");
        Objects.requireNonNull(pre.getTelefono(), "El teléfono es requerido.");
    }

    public ParametrosInscripcionProductor getInstance(InscripcionParam param) {
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setFolio(Objects.nonNull(param.getFolio()) ? param.getFolio().getValue() : null);
        parametros.setFechaInicio(Objects.nonNull(param.getFechaInicio())
                ? param.getFechaInicio().getValue() : null);
        if (Objects.nonNull(param.getFechaFin())) {
            parametros.setFechaFin(param.getFechaFin().getValue());
            parametros.getFechaFin().add(Calendar.DATE, 1);
        }
        parametros.setCurp(param.getCurp());
        parametros.setRfcProductor(param.getRfcProductor());
        parametros.setNombre(param.getNombre());
        parametros.setPapellido(param.getPapellido());
        parametros.setSapellido(param.getSapellido());
        parametros.setSimplifica(param.getSimplifica());
        parametros.setCultivo(Objects.nonNull(param.getCultivo())
                ? buscadorCatalogo.buscaElemento(Cultivo.class, param.getCultivo())
                : cultivoHelper.getCultivo());
        parametros.setCiclo(Objects.nonNull(param.getCiclo())
                ? buscadorCatalogo.buscaElemento(CicloAgricola.class, param.getCiclo()) : null);
        parametros.setEstado(Objects.nonNull(param.getEstado())
                ? buscadorCatalogo.buscaElemento(Estado.class, param.getEstado()) : null);
        return parametros;
    }

    public List<PreRegistroProductor> simplificaPropieades(List<PreRegistroProductor> registros) {
        List<PreRegistroProductor> simplificado = new ArrayList<>();
        for (PreRegistroProductor p : registros) {
            PreRegistroProductor item = new PreRegistroProductor();
            item.setUuid(p.getUuid());
            item.setFechaCreacion(p.getFechaCreacion());
            item.setFolio(p.getFolio());
            item.setCurp(p.getCurp());
            item.setRfc(p.getRfc());
            item.setNombre(p.getNombre());
            item.setPrimerApellido(p.getPrimerApellido());
            item.setSegundoApellido(p.getSegundoApellido());
            item.setCiclo(p.getCiclo());
            simplificado.add(item);
        }
        return simplificado;
    }

    public PreRegistroProductor simplificaPreRegistro(PreRegistroProductor preRegistro) {
        PreRegistroProductor simplificado = new PreRegistroProductor();
        simplificado.setCurp(preRegistro.getCurp());
        simplificado.setRfc(preRegistro.getRfc());
        simplificado.setNombre(preRegistro.getNombre());
        simplificado.setPrimerApellido(preRegistro.getPrimerApellido());
        simplificado.setSegundoApellido(preRegistro.getSegundoApellido());
        simplificado.setApellidosBeneficiario(preRegistro.getApellidosBeneficiario());
        simplificado.setNombreBeneficiario(preRegistro.getNombreBeneficiario());
        simplificado.setCurpBeneficiario(preRegistro.getCurpBeneficiario());
        simplificado.setParentesco(preRegistro.getParentesco());
        simplificado.setTelefono(preRegistro.getTelefono());
        simplificado.setCorreoElectronico(preRegistro.getCorreoElectronico());
        return simplificado;
    }
}
