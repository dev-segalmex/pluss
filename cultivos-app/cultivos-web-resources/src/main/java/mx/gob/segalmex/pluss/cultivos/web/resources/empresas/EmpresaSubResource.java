/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.empresas;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorUsoCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.ParametrosCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.ParametrosUsoCatalogo;
import mx.gob.segalmex.common.core.empresas.ProcesadorEmpresa;
import mx.gob.segalmex.common.core.empresas.busqueda.BuscadorSucursal;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.seguridad.busqueda.BuscadorUsuario;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.UsoCatalogoConverter;
import mx.gob.segalmex.common.web.modelo.empresas.EmpresaConverter;
import mx.gob.segalmex.granos.web.modelo.catalogos.SucursalConverter;
import mx.gob.segalmex.pluss.cultivos.web.resources.inscripcion.InscripcionParam;
import mx.gob.segalmex.pluss.modelo.catalogos.Empresa;
import mx.gob.segalmex.pluss.modelo.catalogos.PrefijoEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;
import mx.gob.segalmex.pluss.modelo.catalogos.UsoCatalogo;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Slf4j
@Component
public class EmpresaSubResource {

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private ProcesadorEmpresa procesadorEmpresa;

    @Autowired
    private BuscadorSucursal buscadorSucursal;

    @Autowired
    private BuscadorUsuario buscadorUsuario;

    @Autowired
    private BuscadorUsoCatalogo buscadorUso;

    @Autowired
    private EmpresaResourceHelper empresaResourceHelper;

    @GET
    @Produces("application/json")
    public List<EmpresaConverter> index(@BeanParam InscripcionParam params) {
        List<Empresa> resultados = buscadorCatalogo.busca(Empresa.class);
        return CollectionConverter.convert(EmpresaConverter.class,
                resultados, 2);
    }

    @Path("/{clave}")
    @GET
    @Produces("application/json")
    public EmpresaConverter getDetalle(@PathParam("clave") String clave, @DefaultValue("2") @QueryParam("expand") int expandLevel) {
        Empresa empresa = buscadorCatalogo.buscaElemento(Empresa.class, clave);
        EmpresaConverter converter = new EmpresaConverter(empresa, expandLevel);
        converter.setSucursales(buscadorSucursal.getSucursales(empresa));
        return converter;
    }

    @Path("/rfc/{rfc}")
    @GET
    @Produces("application/json")
    public EmpresaConverter getEmpresa(@PathParam("rfc") String rfc, @DefaultValue("2") @QueryParam("expand") int expandLevel) {
        ParametrosCatalogo<Empresa> parametros = new ParametrosCatalogo<>();
        parametros.setClase(Empresa.class);
        parametros.getExtraParams().put("rfc", rfc);
        Empresa empresa = buscadorCatalogo.buscaElemento(parametros);
        EmpresaConverter converter = new EmpresaConverter(empresa, expandLevel);
        return converter;
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public EmpresaConverter registra(EmpresaConverter converter) throws IOException {
        Empresa empresa = converter.getEntity();
        try {
            ParametrosCatalogo<Empresa> parametros = new ParametrosCatalogo<>();
            parametros.setClase(Empresa.class);
            parametros.getExtraParams().put("rfc", empresa.getRfc());
            buscadorCatalogo.buscaElemento(parametros);
            throw new SegalmexRuntimeException("Error al registrar:", "Ya existe una empresa con el mismo RFC.");
        } catch (EmptyResultDataAccessException e) {
            if (Objects.isNull(empresa.getId())) {
                procesadorEmpresa.procesa(empresa);
                return new EmpresaConverter(empresa, 1);
            }
        }
        return converter;
    }

    @Path("/sucursal/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public SucursalConverter registra(SucursalConverter converter) throws IOException {
        Empresa empresa = buscadorCatalogo.buscaElemento(Empresa.class, converter.getEmpresa().getClave());
        Sucursal sucursal = converter.getEntity();
        sucursal.setEmpresa(empresa);
        empresaResourceHelper.cargaCatalogosSucursal(sucursal);
        procesadorEmpresa.procesa(sucursal);
        return new SucursalConverter(sucursal, 1);
    }

    @Path("/sucursal/{clave}")
    @GET
    @Produces("application/json")
    public SucursalConverter getUsuariosSucursal(@PathParam("clave") String clave, @DefaultValue("2") @QueryParam("expand") int expandLevel) {
        Sucursal sucursal = buscadorCatalogo.buscaElemento(Sucursal.class, clave);
        SucursalConverter converter = new SucursalConverter(sucursal, 3);
        ParametrosUsoCatalogo params = new ParametrosUsoCatalogo();
        params.setClase(Sucursal.class);
        params.setClave(clave);
        params.setUso(PrefijoEnum.SUCURSAL_USUARIO.getClave());
        params.setUsoAproximado(true);
        List<UsoCatalogo> uso = buscadorUso.buscaUsos(params);
        List<Usuario> usuarios = new ArrayList<>();
        for (UsoCatalogo a : uso) {
            String parametroClave = a.getUso().substring(17);
            usuarios.add(buscadorCatalogo.buscaElemento(Usuario.class, parametroClave));
        }
        converter.setUsuarios(usuarios);
        return converter;
    }

    @Path("/uso-catalogo/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public UsoCatalogoConverter registraUsuario(UsoCatalogoConverter converter) throws IOException {
        Usuario usuario = buscadorCatalogo.buscaElemento(Usuario.class, converter.getUsuario().getId());
        Sucursal sucursal = buscadorCatalogo.buscaElemento(Sucursal.class, converter.getSucursal().getClave());
        UsoCatalogo uso = new UsoCatalogo();
        uso.setUso(PrefijoEnum.SUCURSAL_USUARIO.getClave() + usuario.getClave());
        uso.setClase(Sucursal.class.getName());
        uso.setClave(sucursal.getClave());
        procesadorEmpresa.procesa(uso);
        return new UsoCatalogoConverter(uso, 1);
    }

    @PUT
    @Path("/sucursal/{clave}")
    @Consumes("application/json")
    @Produces("application/json")
    public SucursalConverter actualiza(@PathParam("clave") String clave, SucursalConverter converter) throws IOException {
        Sucursal actual = buscadorCatalogo.buscaElemento(Sucursal.class, clave);
        Sucursal nueva = converter.getEntity();
        empresaResourceHelper.cargaCatalogosSucursal(nueva);
        procesadorEmpresa.actualiza(actual, nueva);
        return new SucursalConverter(actual, 1);
    }
}
