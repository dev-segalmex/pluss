/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.productor;

import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.productor.InformacionAlerta;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Slf4j
@Component
public class InformacionAlertaResourceHelper {

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    public void inicializaCatalogos(InformacionAlerta infoAlerta) {
        infoAlerta.setCultivo(Objects.nonNull(infoAlerta.getCultivo().getId())
                ? buscadorCatalogo.buscaElemento(Cultivo.class, infoAlerta.getCultivo().getId()) : null);
        infoAlerta.setCiclo(Objects.nonNull(infoAlerta.getCiclo().getId())
                ? buscadorCatalogo.buscaElemento(CicloAgricola.class, infoAlerta.getCiclo().getId()) : null);
    }
}
