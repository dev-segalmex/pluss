/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.inscripcion;

import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.factura.ProcesadorInscripcionFactura;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorInscripcionFactura;
import mx.gob.segalmex.common.core.factura.busqueda.ParametrosInscripcionFactura;
import mx.gob.segalmex.common.web.modelo.factura.InscripcionFacturaConverter;
import mx.gob.segalmex.pluss.cultivos.web.resources.productor.CultivoHelper;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Component
@Slf4j
public class ConceptoSubResource {

    @Autowired
    private BuscadorInscripcionFactura buscadorInscripcion;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private ProcesadorInscripcionFactura procesadorFactura;

    @Autowired
    private CultivoHelper cultivoHelper;

    @Path("/{index}")
    @DELETE
    public InscripcionFacturaConverter ignoraConcepto(@PathParam("uuid") String uuid, @PathParam("index") Integer index) {
        LOGGER.info("Ignorando concepto de factura...");
        ParametrosInscripcionFactura parametros = new ParametrosInscripcionFactura();
        parametros.setUuid(uuid);
        parametros.getEstatusIncluir().add(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.VALIDACION));
        parametros.getEstatusIncluir().add(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.SOLVENTADA));
        parametros.setTipoFactura("productor");
        parametros.setCultivo(cultivoHelper.getCultivo());
        try {
            InscripcionFactura inscripcion = buscadorInscripcion.buscaElemento(parametros);
            procesadorFactura.ignoraConcepto(inscripcion, index);
            return new InscripcionFacturaConverter(inscripcion, 1);
        } catch (EmptyResultDataAccessException ouch) {
            LOGGER.error("No se encontró la InscripcionFactura.");
            throw new SegalmexRuntimeException("Error", "La inscripción factura está en un "
                    + "estatus incorrecto para realizar esta acción.");
        }
    }

}
