/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.productor;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.archivos.ArchivoUtils;
import mx.gob.segalmex.common.core.archivos.BuscadorArchivoRelacionado;
import mx.gob.segalmex.common.core.archivos.ManejadorArchivo;
import mx.gob.segalmex.common.core.archivos.RegistroArchivoRelacionado;
import mx.gob.segalmex.common.core.archivos.SubDirectorioHelper;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.common.core.util.AmbienteUtils;
import mx.gob.segalmex.common.core.util.CultivoClaveUtil;
import mx.gob.segalmex.common.core.validador.ValidadorCurpHelper;
import mx.gob.segalmex.common.core.validador.ValidadorInscripcion;
import mx.gob.segalmex.common.core.validador.ValidadorProductorDeudor;
import mx.gob.segalmex.common.core.validador.ValidadorRfcHelper;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.RespuestaConverter;
import mx.gob.segalmex.common.web.modelo.productor.ProductorCicloConverter;
import mx.gob.segalmex.common.web.modelo.productor.ProductorConverter;
import mx.gob.segalmex.common.web.modelo.seguridad.UsuarioConverter;
import mx.gob.segalmex.common.web.resources.archivos.ArchivoFormUtils;
import mx.gob.segalmex.granos.core.contrato.busqueda.BuscadorDatoCapturado;
import mx.gob.segalmex.granos.core.contrato.busqueda.BuscadorInscripcionContrato;
import mx.gob.segalmex.granos.core.historico.BuscadorHistoricoRegistro;
import mx.gob.segalmex.granos.core.inscripcion.BuscadorComentarioInscripcion;
import mx.gob.segalmex.granos.core.inscripcion.ProcesadorComentarioInscripcion;
import mx.gob.segalmex.granos.core.inscripcion.ProcesadorDatoCapturado;
import mx.gob.segalmex.granos.core.productor.ProcesadorInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.ProcesadorProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInscripcionProductorHelper;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorPredioDocumento;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.pdf.InscripcionProductorPdfCreator;
import mx.gob.segalmex.granos.core.productor.xls.InscripcionProductorXls;
import mx.gob.segalmex.granos.core.rfc.busqueda.BuscadorRfcExcepcion;
import mx.gob.segalmex.granos.web.modelo.contrato.DatoCapturadoConverter;
import mx.gob.segalmex.granos.web.modelo.inscripcion.ComentarioInscripcionConverter;
import mx.gob.segalmex.granos.web.modelo.productor.InscripcionAgrupadaConverter;
import mx.gob.segalmex.granos.web.modelo.productor.InscripcionProductorConverter;
import mx.gob.segalmex.granos.web.modelo.productor.InscripcionResumenConverter;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import mx.gob.segalmex.pluss.cultivos.modelo.contrato.ValidacionConverter;
import mx.gob.segalmex.pluss.cultivos.web.resources.inscripcion.InscripcionParam;
import mx.gob.segalmex.pluss.cultivos.web.resources.params.FolioParam;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import mx.gob.segalmex.pluss.modelo.archivos.ArchivoRelacionado;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.ReferenciaReporteEnum;
import mx.gob.segalmex.pluss.modelo.historico.HistoricoRegistro;
import mx.gob.segalmex.pluss.modelo.inscripcion.ComentarioInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionAgrupada;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionResumen;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersonaEnum;
import mx.gob.segalmex.pluss.modelo.productor.ContratoFirmado;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.Productor;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Slf4j
@Path("/productores/")
@Component
public class ProductorResource {

    @Autowired
    private ProductorResourceHelper helper;

    @Autowired
    private UsuarioSesionContextoHolder contextoSeguridad;

    @Autowired
    private AmbienteUtils ambienteUtils;

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private BuscadorInscripcionProductor buscadorInscripcionProductor;

    @Autowired
    private BuscadorArchivoRelacionado buscadorArchivoRelacionado;

    @Autowired
    private BuscadorDatoCapturado buscadorDatoCapturado;

    @Autowired
    private BuscadorHistoricoRegistro buscadorHistorico;

    @Autowired
    private RegistroArchivoRelacionado registroArchivoRelacionado;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    private ProcesadorInscripcionProductor procesadorInscripcion;

    @Autowired
    private BuscadorInscripcionContrato buscadorInscripcionContrato;

    @Autowired
    private BuscadorComentarioInscripcion buscadorComentarioInscripcion;

    @Autowired
    private ProcesadorComentarioInscripcion procesadorComentarioInscripcion;

    @Autowired
    private InscripcionProductorXls inscripcionProductorXls;

    @Autowired
    private ProcesadorDatoCapturado procesadorDatoCapturado;

    @Autowired
    private ProductorPreRegistroResource productorPreRegistroResource;

    @Autowired
    private InscripcionProductorPredioSubResource inscripcionProductorPredioSubResource;

    @Autowired
    private InscripcionProductorContratoSubResource inscripcionProductorContratoSubResource;

    @Autowired
    private CultivoHelper cultivoHelper;

    @Autowired
    private BuscadorProductor buscadorProductor;

    private List<ValidadorInscripcion> validadadores;

    @Autowired
    private BuscadorPredioDocumento buscadorPredioDocumento;

    @Autowired
    private BuscadorRfcExcepcion buscadorRfcExcepcion;

    @Autowired
    private BuscadorInscripcionProductorHelper buscadorInscripcionProductorHelper;

    @Autowired
    private ManejadorArchivo manejadorArchivo;

    @Autowired
    private ProcesadorProductor procesadorProductor;

    @Autowired
    private ValidadorProductorDeudor validadorProductorDeudor;

    @Path("/maiz/pre-registro/")
    public ProductorPreRegistroResource getProductorPreRegistroResource() {
        return productorPreRegistroResource;
    }

    @Path("/maiz/inscripcion/{uuid}/predios/")
    public InscripcionProductorPredioSubResource getInscripcionProductorPredioSubResource() {
        return inscripcionProductorPredioSubResource;
    }

    @Path("/maiz/inscripcion/{uuid}/contrato/")
    public InscripcionProductorContratoSubResource getInscripcionProductorContratoSubResource() {
        return inscripcionProductorContratoSubResource;
    }

    @Path("/maiz/inscripcion/")
    @GET
    @Produces("application/json")
    public List<InscripcionProductorConverter> busca(@BeanParam InscripcionParam params) {
        ParametrosInscripcionProductor parametros = helper.getInstance(params);
        List<InscripcionProductor> resultados = buscadorInscripcionProductor.busca(parametros);

        return CollectionConverter.convert(InscripcionProductorConverter.class,
                helper.simplificaPropieades(resultados), 2);
    }

    @Path("/seguimiento/inscripcion/")
    @GET
    @Produces("application/json")
    public List<InscripcionProductorConverter> seguimiento(@BeanParam InscripcionParam params) {
        ParametrosInscripcionProductor pip = new ParametrosInscripcionProductor();
        pip.setUuidProductor(params.getUuidProductor());
        pip.setCultivo(buscadorCatalogo.buscaElemento(Cultivo.class, params.getCultivo()));
        pip.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, params.getCiclo()));

        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setFolio(buscadorProductor.buscaElementoPc(pip).getFolio());
        List<InscripcionProductor> resultados = buscadorInscripcionProductor.busca(parametros);

        return CollectionConverter.convert(InscripcionProductorConverter.class,
                helper.simplificaPropieades(resultados), 2);
    }

    @GET
    @Path("/maiz/inscripcion/agrupada")
    @Produces("application/json")
    public List<InscripcionAgrupadaConverter> buscaAgrupada(@BeanParam InscripcionParam params) {
        ParametrosInscripcionProductor parametros = helper.getInstance(params);
        List<InscripcionAgrupada> resultados = buscadorInscripcionProductorHelper.busca(parametros);

        return InscripcionAgrupadaConverter.convert(resultados, 2);
    }

    @Path("/publico/inscripcion/")
    @GET
    @Produces("application/json")
    public InscripcionProductorConverter busca(@QueryParam("folio") FolioParam folio,
            @QueryParam("curp") String curp, @QueryParam("rfcProductor") String rfcProductor) {
        if (Objects.isNull(curp) || Objects.isNull(rfcProductor) || Objects.isNull(folio)) {
            LOGGER.warn("No se puede buscar sin parámetros.");
            throw new IllegalArgumentException("No se puede buscar sin parámetros.");
        }
        ValidadorCurpHelper.valida(curp, "del productor");

        List<String> excepciones = buscadorRfcExcepcion.getExcepciones(rfcProductor, true);
        ValidadorRfcHelper.valida(rfcProductor, excepciones, "del productor");

        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setCurp(curp);
        parametros.setRfcProductor(rfcProductor);
        parametros.setFolio(Objects.nonNull(folio) ? folio.getValue() : null);
        try {
            InscripcionProductor inscripcion = buscadorInscripcionProductor.buscaElemento(parametros);
            return new InscripcionProductorConverter(helper.simplificaPropieades(inscripcion), 2);
        } catch (EmptyResultDataAccessException ouch) {
            LOGGER.error("No existe inscripción con los parámetros recibidos.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

    }

    @Path("publico/inscripcion/{uuid}")
    @GET
    @Produces("application/json")
    public InscripcionProductorConverter getInscripcionPublica(@PathParam("uuid") String uuid) {
        return getInscripcion(uuid, 3, false, true, true, true, false, false, true, true);
    }

    @Path("/maiz/inscripcion/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionProductorConverter registra(InscripcionProductorConverter converter) {
        InscripcionProductor inscripcion = converter.getEntity();
        inscripcion.setCultivo(cultivoHelper.getCultivo());
        if (Objects.nonNull(inscripcion.getUuid())) {
            return converter;
        }

        helper.inicializaCatalogos(inscripcion);
        for (ValidadorInscripcion v : validadadores) {
            v.valida(inscripcion);
        }

        List<InscripcionProductor> duplicados = helper.getDuplicados(inscripcion);
        if (!inscripcion.isForzada() && !duplicados.isEmpty()) {
            List<String> folios = new ArrayList<>();
            for (InscripcionProductor ip : duplicados) {
                folios.add(ip.getFolio());
            }
            LOGGER.error("Intentando registrar un productor con la misma CURP: {}",
                    inscripcion.getDatosProductor().getCurp());
            List<String> motivos = new ArrayList<>();
            if (inscripcion.getDatosProductor().getTipoPersona().getClave().equals(TipoPersonaEnum.MORAL.getClave())) {
                motivos.add("El RFC de la empresa ya se encuentra registrada con los folios: "
                        + String.join(", ", folios));
            } else {
                motivos.add("La CURP ya se encuentra registrada con los folios: "
                        + String.join(", ", folios));
            }

            throw new SegalmexRuntimeException("Error al realizar el registro.", motivos);
        }

        procesadorInscripcion.procesa(inscripcion);
        return new InscripcionProductorConverter(inscripcion, 1);
    }

    @Path("/maiz/inscripcion/{uuid}")
    @GET
    @Produces("application/json")
    public InscripcionProductorConverter getInscripcion(@PathParam("uuid") String uuid,
            @DefaultValue("2") @QueryParam("expand") int expandLevel,
            @QueryParam("archivos") @DefaultValue("false") boolean archivos,
            @QueryParam("datos") @DefaultValue("false") boolean datos,
            @QueryParam("historial") @DefaultValue("false") boolean historial,
            @QueryParam("contratos") @DefaultValue("false") boolean contratos,
            @QueryParam("comentarios") @DefaultValue("false") boolean comentarios,
            @QueryParam("archivosCargados") @DefaultValue("false") boolean archivosCargados,
            @QueryParam("productor") @DefaultValue("false") boolean productor,
            @QueryParam("publico") @DefaultValue("false") boolean publico) {
        InscripcionProductor inscripcion = buscadorInscripcionProductor.buscaElemento(uuid);
        InscripcionProductorConverter converter = new InscripcionProductorConverter(inscripcion, expandLevel);
        if (publico) {
            helper.quitaInformacionPrivada(inscripcion);
        }

        if (archivos) {
            List<ArchivoRelacionado> relacionados = buscadorArchivoRelacionado.busca(inscripcion.getClass(), inscripcion.getUuid());
            LOGGER.debug("Archivos relacionados: {}", relacionados.size());
            List<Archivo> ars = new ArrayList<>();
            for (ArchivoRelacionado ar : relacionados) {
                Archivo archivo = ar.getArchivo();
                archivo.setTipo(ar.getTipo());
                ars.add(archivo);
            }
            converter.setArchivos(ars);
        }

        if (datos) {
            List<DatoCapturado> dts = buscadorDatoCapturado.busca(InscripcionProductor.class, uuid, true);
            converter.setDatos(dts);
        }

        if (historial) {
            List<HistoricoRegistro> historicos = buscadorHistorico.busca(inscripcion);
            if (publico) {
                helper.quitaInformacionPrivada(historicos);
            }
            converter.setHistorial(historicos);
        }

        if (contratos) {
            for (ContratoFirmado c : inscripcion.getContratos()) {
                InscripcionContrato ic = buscadorInscripcionContrato.buscaElementoPorFolio(c.getFolio());
                c.setArchivo(ic.getContrato());
                c.setEstatus(ic.getEstatus());
            }
        }
        if (comentarios) {
            List<ComentarioInscripcion> cis = buscadorComentarioInscripcion.busca(inscripcion);
            converter.setComentarios(cis);
        }
        if (productor) {
            try {
                Productor sp = new Productor();
                sp.setUuid(buscadorProductor.busca(inscripcion.getDatosProductor().getRfc()).getUuid());
                converter.setProductor(sp);
            } catch (EmptyResultDataAccessException ouch) {
                LOGGER.debug("La inscripción todavía no tiene productor.");
            }
        }

        if (archivosCargados) {
            String[] clavesArchivos = inscripcion.getClaveArchivos().split("\\|");
            List<ArchivoRelacionado> relacionados = buscadorArchivoRelacionado.busca(inscripcion.getClass(), inscripcion.getUuid());
            List<Archivo> arcs = new ArrayList<>();
            for (String ca : clavesArchivos[0].split(",")) {
                arcs.add(getArchivo(ca, relacionados));
            }
            converter.setArchivosCargados(arcs);
            if (inscripcion.getClaveArchivos().contains("|")) {
                List<Archivo> opcionales = new ArrayList<>();
                for (String ca : clavesArchivos[1].split(",")) {
                    opcionales.add(getArchivo(ca, relacionados));
                }
                converter.setArchivosOpcionales(opcionales);
            }
        }

        converter.setPrediosDocumentos(buscadorPredioDocumento.busca(inscripcion));

        return converter;
    }

    @Path("/maiz/inscripcion/{uuid}/inscripcion-productor.pdf")
    @GET
    @Produces("application/pdf")
    public Response getComprobante(@PathParam("uuid") String uuid) {
        InscripcionProductor inscripcion = buscadorInscripcionProductor.buscaElemento(uuid);
        InscripcionProductorPdfCreator creator
                = new InscripcionProductorPdfCreator(ambienteUtils.getValor(AmbienteUtils.AMBIENTE));
        byte[] pdf = creator.create(inscripcion);

        StreamingOutput so = (OutputStream out) -> {
            IOUtils.write(pdf, out);
            out.close();
        };

        return Response.ok(so)
                .header("Content-Disposition", "attachment;filename=productor-"
                        + inscripcion.getFolio() + ".pdf")
                .build();
    }

    @Path("/maiz/inscripcion/{uuid}/anexos/{archivo}/pdf/")
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces({"application/json;charset=utf-8"})
    public RespuestaConverter registraAnexos(@PathParam("uuid") String uuid,
            @PathParam("archivo") String tipoArchivo, MultipartFormDataInput input) throws IOException {
        InscripcionProductor inscripcion = buscadorInscripcionProductor.buscaElemento(uuid);

        InputStream is = ArchivoFormUtils.getInputStrem(input);
        String nombre = ArchivoFormUtils.getFilename(input, "pdf");
        String subidirectorio = SubDirectorioHelper.getSubdirectorio(inscripcion.getCiclo().getClave(),
                "productores", inscripcion.getFolio());
        Archivo archivo = ArchivoUtils.getInstance(nombre, CultivoClaveUtil.getClaveCorta(inscripcion.getCultivo()),
                subidirectorio, false);

        ArchivoRelacionado ar = new ArchivoRelacionado();
        ar.setArchivo(archivo);
        ar.setUsuarioRegistra(contextoSeguridad.getUsuario());
        ar.setClase(inscripcion.getClass().getSimpleName());
        ar.setReferencia(inscripcion.getUuid());
        ar.setTipo(tipoArchivo);

        registroArchivoRelacionado.registra(archivo, ar, is);

        switch (tipoArchivo) {
            case "inscripcion-productor":
                if (inscripcion.getEstatus().getClave().equals(EstatusInscripcionEnum.PENDIENTE.getClave())) {
                    procesadorInscripcion.generaAsignacion(inscripcion, contextoSeguridad.getUsuario());
                }
                break;
        }

        RespuestaConverter respuesta = new RespuestaConverter();
        respuesta.setMensaje("Archivo almacenado correctamente: " + tipoArchivo);
        return respuesta;
    }

    @Path("/maiz/inscripcion/{uuid}/validacion/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionProductorConverter asignaValidador(@PathParam("uuid") String uuid, UsuarioConverter converter) throws IOException {
        InscripcionProductor inscripcion = buscadorInscripcionProductor.buscaElemento(uuid);
        Usuario validador = buscadorCatalogo.buscaElemento(Usuario.class, converter.getEntity().getId());
        procesadorInscripcion.asigna(inscripcion, contextoSeguridad.getUsuario(), validador);

        return new InscripcionProductorConverter(inscripcion, 1);
    }

    @Path("/maiz/inscripcion/{uuid}/validacion/positiva/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionProductorConverter validaPositivo(@PathParam("uuid") String uuid, ValidacionConverter datos) {
        InscripcionProductor inscripcion = buscadorInscripcionProductor.buscaElemento(uuid);
        helper.getValidaEstatus(inscripcion.getEstatus(),
                EstatusInscripcionEnum.VALIDACION, EstatusInscripcionEnum.SOLVENTADA);
        validadorProductorDeudor.valida(helper.getParametrosDeudor(inscripcion, "rfc"), inscripcion);
        validadorProductorDeudor.valida(helper.getParametrosDeudor(inscripcion, "curp"), inscripcion);
        List<DatoCapturado> dcs = new ArrayList<>();
        for (DatoCapturadoConverter dc : datos.getDatosCapturados()) {
            dcs.add(dc.getEntity());
        }
        if (Objects.nonNull(datos.getSolicitado())) {
            inscripcion.setPredios(helper.validaRendimientoSolicitado(inscripcion.getPredios(), datos.getSolicitado()));
        }
        procesadorInscripcion.validaPositivo(inscripcion, contextoSeguridad.getUsuario(), dcs);
        return new InscripcionProductorConverter(inscripcion, 0);
    }

    @Path("/maiz/inscripcion/{uuid}/validacion/negativa/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionProductorConverter validaNegativo(@PathParam("uuid") String uuid, List<DatoCapturadoConverter> datos) {
        InscripcionProductor inscripcion = buscadorInscripcionProductor.buscaElemento(uuid);
        helper.getValidaEstatus(inscripcion.getEstatus(),
                EstatusInscripcionEnum.VALIDACION, EstatusInscripcionEnum.SOLVENTADA);

        List<DatoCapturado> dcs = new ArrayList<>();
        for (DatoCapturadoConverter dc : datos) {
            dcs.add(dc.getEntity());
        }
        procesadorInscripcion.validaNegativo(inscripcion, contextoSeguridad.getUsuario(), dcs);
        return new InscripcionProductorConverter(inscripcion, 0);
    }

    @Path("/maiz/inscripcion/{uuid}/validacion/negativa/")
    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionProductorConverter corrigeValidacionNegativa(@PathParam("uuid") String uuid, List<DatoCapturadoConverter> datos) {
        InscripcionProductor inscripcion = buscadorInscripcionProductor.buscaElemento(uuid);
        helper.getValidaEstatus(inscripcion.getEstatus(),
                EstatusInscripcionEnum.CORRECCION);

        List<DatoCapturado> dcs = new ArrayList<>();
        for (DatoCapturadoConverter dc : datos) {
            dcs.add(dc.getEntity());
        }
        procesadorInscripcion.corrige(inscripcion, contextoSeguridad.getUsuario(), dcs);
        return new InscripcionProductorConverter(inscripcion, 0);
    }

    @Path("/maiz/inscripcion/{uuid}/validacion/actualizacion/")
    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionProductorConverter actualizaValidacionNegativa(@PathParam("uuid") String uuid, List<DatoCapturadoConverter> datos) {
        InscripcionProductor inscripcion = buscadorInscripcionProductor.buscaElemento(uuid);
        List<DatoCapturado> dcs = new ArrayList<>();
        for (DatoCapturadoConverter dc : datos) {
            dcs.add(dc.getEntity());
        }
        procesadorInscripcion.actualiza(inscripcion, dcs);
        return new InscripcionProductorConverter(inscripcion, 0);
    }

    @Path("/maiz/inscripcion/{uuid}/validacion/reasignacion/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionProductorConverter cambiaValidador(@PathParam("uuid") String uuid, UsuarioConverter converter) {
        InscripcionProductor inscripcion = buscadorInscripcionProductor.buscaElemento(uuid);
        Usuario validador = buscadorCatalogo.buscaElemento(Usuario.class, converter.getEntity().getId());
        Usuario validadorActual = inscripcion.getUsuarioValidador();
        if (Objects.isNull(validadorActual)) {
            throw new SegalmexRuntimeException("Error: no fue posible reasignar.",
                    "Aún no ha sido asignado el registro: " + inscripcion.getFolio() + ".");
        }
        inscripcion.setUsuarioValidador(validador);
        if (inscripcion.getUsuarioAsignado().getNombre().equals(validadorActual.getNombre())) {
            inscripcion.setUsuarioAsignado(validador);
        }
        registroEntidad.actualiza(inscripcion);
        return new InscripcionProductorConverter(inscripcion, 1);
    }

    @Path("/maiz/inscripcion/{uuid}/revalidacion/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionProductorConverter revalida(@PathParam("uuid") String uuid) {
        InscripcionProductor inscripcion = buscadorInscripcionProductor.buscaElemento(uuid);
        procesadorInscripcion.revalida(inscripcion, contextoSeguridad.getUsuario());

        return new InscripcionProductorConverter(inscripcion, 1);
    }

    @Path("/maiz/inscripcion/{uuid}/cancelacion/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionProductorConverter cancela(@PathParam("uuid") String uuid, InscripcionProductorConverter converter) {
        return cancela(uuid, converter, buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.CANCELADA.getClave()));
    }

    @Path("/maiz/inscripcion/{uuid}/comentario/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionProductorConverter agregaComentario(@PathParam("uuid") String uuid, ComentarioInscripcionConverter converter) {
        InscripcionProductor inscripcion = buscadorInscripcionProductor.buscaElemento(uuid);
        ComentarioInscripcion ci = converter.getEntity();
        procesadorComentarioInscripcion.procesa(ci, inscripcion, contextoSeguridad.getUsuario());
        return new InscripcionProductorConverter(inscripcion, 0);
    }

    @Path("/maiz/inscripcion/csv/resultados-productor.xlsx")
    @GET
    @Produces("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    public byte[] indexXls(@BeanParam InscripcionParam params) {
        ParametrosInscripcionProductor parametros = helper.getInstance(params);
        List<InscripcionProductor> resultados = buscadorInscripcionProductor.busca(parametros);
        return inscripcionProductorXls.exporta(resultados);
    }

    @Path("/reporte/{ciclo}.xlsx")
    @GET
    @Produces("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    public Response reporteProductorCompleto(@PathParam("ciclo") Integer ciclo) {
        try {
            CicloAgricola c = buscadorCatalogo.buscaElemento(CicloAgricola.class, ciclo);
            String tipo = "registro-productores-" + cultivoHelper.getCultivo().getClave() + "-" + c.getClave() + "-completo";
            ArchivoRelacionado ar = buscadorArchivoRelacionado.buscaElemento(InscripcionProductor.class.getSimpleName(),
                    ReferenciaReporteEnum.PRODUCTOR.getClave(), tipo);
            return getXlsx(ar.getArchivo());
        } catch (IOException ex) {
            LOGGER.error("No fue posible generar el reporte de productores.");
            throw new SegalmexRuntimeException("Error:", "No fue posible generar el reporte de productores.");
        }
    }

    @Path("/reporte/predio/{ciclo}.xlsx")
    @GET
    @Produces("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    public Response reporteProductorPredio(@PathParam("ciclo") Integer ciclo) {
        try {
            CicloAgricola c = buscadorCatalogo.buscaElemento(CicloAgricola.class, ciclo);
            String tipo = "registro-predios-" + cultivoHelper.getCultivo().getClave() + "-" + c.getClave() + "-completo";
            ArchivoRelacionado ar = buscadorArchivoRelacionado.buscaElemento(InscripcionProductor.class.getSimpleName(),
                    ReferenciaReporteEnum.PRODUCTOR_PREDIO.getClave(), tipo);
            return getXlsx(ar.getArchivo());
        } catch (IOException ex) {
            LOGGER.error("No fue posible generar el reporte de predios.");
            throw new SegalmexRuntimeException("Error:", "No fue posible generar el reporte de predios.");
        }
    }

    @Path("/reporte/contrato/{ciclo}.xlsx")
    @GET
    @Produces("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    public Response reporteProductorContrato(@PathParam("ciclo") Integer ciclo) {
        try {
            CicloAgricola c = buscadorCatalogo.buscaElemento(CicloAgricola.class, ciclo);
            String tipo = "registro-productor-contratos-" + cultivoHelper.getCultivo().getClave() + "-" + c.getClave() + "-completo";
            ArchivoRelacionado ar = buscadorArchivoRelacionado.buscaElemento(InscripcionProductor.class.getSimpleName(),
                    ReferenciaReporteEnum.PRODUCTOR_CONTRATO.getClave(), tipo);
            return getXlsx(ar.getArchivo());
        } catch (IOException ex) {
            LOGGER.error("No fue posible generar el reporte de productor contratos.");
            throw new SegalmexRuntimeException("Error:", "No fue posible generar el reporte de productor contratos.");
        }
    }

    @Path("/reporte/predio/planeacion/{ciclo}.xlsx")
    @GET
    @Produces("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    public Response reportePredioPlaneacion(@PathParam("ciclo") Integer ciclo) {
        try {
            CicloAgricola c = buscadorCatalogo.buscaElemento(CicloAgricola.class, ciclo);
            String tipo = "registro-predios-planeacion-" + cultivoHelper.getCultivo().getClave() + "-" + c.getClave() + "-completo";
            ArchivoRelacionado ar = buscadorArchivoRelacionado.buscaElemento(InscripcionProductor.class.getSimpleName(),
                    ReferenciaReporteEnum.PREDIO_PLANEACION.getClave(), tipo);
            return getXlsx(ar.getArchivo());
        } catch (IOException ex) {
            LOGGER.error("No fue posible generar el reporte de predios planeación.");
            throw new SegalmexRuntimeException("Error:", "No fue posible generar el reporte de predios planeación.");
        }
    }

    @Path("/maiz/inscripcion/{uuid}/no-elegible/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionProductorConverter noEligible(@PathParam("uuid") String uuid, InscripcionProductorConverter converter) {
        return cancela(uuid, converter, buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.NO_ELIGIBLE.getClave()));
    }

    @Path("/maiz/inscripcion/{uuid}/negativo/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionProductorConverter negativo(@PathParam("uuid") String uuid, InscripcionProductorConverter converter) {
        return cancela(uuid, converter, buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.NEGATIVO.getClave()));
    }

    @Path("/maiz/inscripcion/{uuid}/negativo/deudor/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionProductorConverter negativoDeudor(@PathParam("uuid") String uuid, InscripcionProductorConverter converter) {
        return cancela(uuid, converter, buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.NEGATIVO_DEUDOR.getClave()));
    }

    private InscripcionProductorConverter cancela(String uuid, InscripcionProductorConverter converter, EstatusInscripcion estatus) {
        InscripcionProductor inscripcion = buscadorInscripcionProductor.buscaElemento(uuid);
        inscripcion.setComentarioEstatus(converter.getComentarioEstatus());
        switch (estatus.getClave()) {
            case "no-elegible":
            case "cancelada":
            case "negativo":
            case "pendiente-revision":
            case "negativo-deudor":
                break;
            default:
                throw new IllegalArgumentException("Estatus no permitido para cancelar");
        }
        procesadorInscripcion.cancela(inscripcion, contextoSeguridad.getUsuario(), estatus);
        return new InscripcionProductorConverter(inscripcion, 0);
    }

    @Path("/maiz/inscripcion/{uuid}/datos/duplicados/")
    @DELETE
    @Produces("application/json")
    public InscripcionProductorConverter eliminaDatosDuplicados(@PathParam("uuid") String uuid) {
        InscripcionProductor inscripcion = buscadorInscripcionProductor.buscaElemento(uuid);
        procesadorDatoCapturado.eliminaDuplicados(inscripcion);
        return new InscripcionProductorConverter(inscripcion, 0);
    }

    @Path("/maiz/inscripcion/{uuid}/supervision/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionProductorConverter enviaSupervision(@PathParam("uuid") String uuid, List<DatoCapturadoConverter> datos) {
        InscripcionProductor inscripcion = buscadorInscripcionProductor.buscaElemento(uuid);
        List<DatoCapturado> dcs = new ArrayList<>();
        for (DatoCapturadoConverter dc : datos) {
            dcs.add(dc.getEntity());
        }
        procesadorInscripcion.enviaSupervision(inscripcion, contextoSeguridad.getUsuario(), dcs);
        return new InscripcionProductorConverter(inscripcion, 1);
    }

    @Path("/maiz/inscripcion/{uuid}/edicion/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionProductorConverter creaEdicion(@PathParam("uuid") String uuid, InscripcionProductorConverter converter) {
        InscripcionProductor editado = converter.getEntity();

        InscripcionProductor registrado = buscadorInscripcionProductor.buscaElemento(uuid);
        helper.validaEditable(registrado, editado.getNumeroContratos());
        registrado.setNumeroContratos(editado.getNumeroContratos());
        registrado.setNumeroPredios(editado.getNumeroPredios());
        registrado.setNumeroEmpresas(editado.getNumeroEmpresas());

        procesadorInscripcion.creaEdicion(registrado, contextoSeguridad.getUsuario());
        return new InscripcionProductorConverter(registrado, 0);
    }

    @Path("/maiz/inscripcion/{uuid}/edicion/")
    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionProductorConverter finalizaEdicion(@PathParam("uuid") String uuid, InscripcionProductorConverter converter) {
        InscripcionProductor registrado = buscadorInscripcionProductor.buscaElemento(uuid);
        InscripcionProductor editado = converter.getEntity();
        helper.inicializaEdicion(editado, registrado.getCultivo());
        if (Objects.nonNull(editado.getClaveArchivos())) {
            registrado.setClaveArchivos(editado.getClaveArchivos());
        }
        procesadorInscripcion.finalizaEdicion(registrado, editado, contextoSeguridad.getUsuario());
        return new InscripcionProductorConverter(registrado, 0);
    }

    @Path("/maiz/inscripcion/{uuid}/forzada/")
    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionProductorConverter cambiaForzada(@PathParam("uuid") String uuid) {
        InscripcionProductor registrado = buscadorInscripcionProductor.buscaElemento(uuid);
        procesadorInscripcion.cambiaForzada(registrado, true);
        return new InscripcionProductorConverter(registrado, 0);
    }

    @Path("/maiz/inscripcion/{uuid}/positivo/deudor/")
    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionProductorConverter permitePositivoDeudor(@PathParam("uuid") String uuid) {
        InscripcionProductor ip = buscadorInscripcionProductor.buscaElemento(uuid);
        helper.getValidaEstatus(ip.getEstatus(),
                EstatusInscripcionEnum.VALIDACION, EstatusInscripcionEnum.SOLVENTADA);
        procesadorInscripcion.permitePositivoDeudor(ip, contextoSeguridad.getUsuario());
        return new InscripcionProductorConverter(ip, 0);
    }

    @Path("/maiz/inscripcion/{uuid}/pendiente/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionProductorConverter marcaPendiente(@PathParam("uuid") String uuid, InscripcionProductorConverter converter) {
        return cancela(uuid, converter, buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.PENDIENTE_REVISION.getClave()));
    }

    public void setValidadadores(List<ValidadorInscripcion> validadadores) {
        this.validadadores = validadadores;
    }

    public void setProcesadorInscripcion(ProcesadorInscripcionProductor procesadorInscripcion) {
        this.procesadorInscripcion = procesadorInscripcion;
    }

    @Path("/curp/{curp}")
    @GET
    @Produces("application/json")
    public ProductorConverter getProductor(@PathParam("curp") String curp,
            @DefaultValue("2") @QueryParam("expand") int expandLevel, @QueryParam("ciclo") int ciclo) {
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setCurp(curp);
        Productor productor = buscadorProductor.buscaElemento(parametros);
        ProductorConverter converter = new ProductorConverter(helper.simplificaProductor(productor), expandLevel);
        return converter;
    }

    @Path("/inscripcion/administracion")
    @GET
    @Produces("application/json")
    public List<InscripcionProductorConverter> buscaPorFolio(@BeanParam InscripcionParam params) {
        Objects.requireNonNull(params.getFolio(), "El folio no puede ser nulo.");
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setFolio(params.getFolio().getValue());
        List<InscripcionProductor> resultados = buscadorInscripcionProductor.busca(parametros);
        return CollectionConverter.convert(InscripcionProductorConverter.class,
                helper.simplificaPropieades(resultados), 2);
    }

    @GET
    @Produces("application/json")
    public List<ProductorConverter> index(@BeanParam InscripcionParam params) {
        ParametrosInscripcionProductor parametros = helper.getInstance(params);
        List<Productor> productores = buscadorProductor.busca(parametros);
        return CollectionConverter.convert(ProductorConverter.class,
                productores, 2);

    }

    @Path("/{uuid}")
    @GET
    @Produces("application/json")
    public List<ProductorCicloConverter> buscaProductorCiclo(@PathParam("uuid") String uuid) {
        ParametrosInscripcionProductor params = new ParametrosInscripcionProductor();
        params.setUuid(uuid);
        Productor p = buscadorProductor.buscaElemento(params);
        List<ProductorCiclo> ciclos = buscadorProductor.busca(p);
        List<ProductorCiclo> simplificados = new ArrayList<>();
        for (ProductorCiclo pc : ciclos) {
            simplificados.add(helper.simplificaProductorCicloSeguimiento(pc));
        }
        // Tal vez simplificar ciclos
        return CollectionConverter.convert(ProductorCicloConverter.class,
                simplificados, 2);
    }

    @Path("/inscripcion/{uuid}/productor-ciclo/actualizacion/")
    @PUT
    @Produces("application/json")
    public InscripcionProductorConverter actualizaProductorCiclo(@PathParam("uuid") String uuid) {
        try {
            InscripcionProductor ip = buscadorInscripcionProductor.buscaElemento(uuid);
            ProductorCiclo pc = buscadorProductor.buscaElemento(ip.getFolio());
            procesadorProductor.actualizaProductorCiclo(ip, pc);
            return new InscripcionProductorConverter(ip, 0);
        } catch (EmptyResultDataAccessException e) {
            throw new SegalmexRuntimeException("Error:", "No se encontró el productorCiclo del registro.");
        }
    }

    @GET
    @Path("/inscripcion/resumen")
    @Produces("application/json")
    public List<InscripcionResumenConverter> buscaResumen() {
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        List<InscripcionResumen> resultados = buscadorInscripcionProductorHelper.buscaResumen(parametros);
        return InscripcionResumenConverter.convert(resultados, 2);
    }

    /**
     * Se encarga de obtener por clave un archivo si es que existe en los
     * archivos relacionados, si no, regresa un archivo vacio e indicando que no
     * fue cargado.
     *
     * @param clave la clave del archivo a obtener.
     * @param relacionados la lista de los archivos que estan cargados en
     * sistema.
     * @return Un archivo real sí se encuentra, si no, un archivo indicando que
     * no se cargo.
     */
    private Archivo getArchivo(String clave, List<ArchivoRelacionado> relacionados) {
        for (ArchivoRelacionado ar : relacionados) {
            if (clave.equals(ar.getTipo())) {
                Archivo a = ar.getArchivo();
                a.setTipo(clave);
                a.setCargado(true);
                return a;
            }
        }
        Archivo nc = new Archivo();
        nc.setTipo(clave);
        nc.setCargado(false);
        return nc;
    }

    private Response getXlsx(Archivo archivo) throws IOException {
        byte[] xls = manejadorArchivo.obten(archivo);
        StreamingOutput so = (OutputStream out) -> {
            IOUtils.write(xls, out);
            out.close();
        };
        return Response.ok(so)
                .header("Content-Disposition", "attachment;filename="
                        + archivo.getNombreOriginal())
                .build();
    }
}
