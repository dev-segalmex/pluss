/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.inscripcion;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.archivos.ArchivoUtils;
import mx.gob.segalmex.common.core.archivos.BuscadorArchivoRelacionado;
import mx.gob.segalmex.common.core.archivos.ManejadorArchivo;
import mx.gob.segalmex.common.core.archivos.RegistroArchivoRelacionado;
import mx.gob.segalmex.common.core.archivos.SubDirectorioHelper;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.common.core.util.AmbienteUtils;
import mx.gob.segalmex.common.core.util.CultivoClaveUtil;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.RespuestaConverter;
import mx.gob.segalmex.common.web.modelo.seguridad.UsuarioConverter;
import mx.gob.segalmex.common.web.resources.archivos.ArchivoFormUtils;
import mx.gob.segalmex.granos.core.contrato.ProcesadorContratoProductorXls;
import mx.gob.segalmex.granos.core.contrato.ProcesadorInscripcionContrato;
import mx.gob.segalmex.granos.core.contrato.busqueda.BuscadorDatoCapturado;
import mx.gob.segalmex.granos.core.contrato.busqueda.BuscadorInscripcionContrato;
import mx.gob.segalmex.granos.core.contrato.busqueda.BuscadorInscripcionContratoHelper;
import mx.gob.segalmex.granos.core.contrato.busqueda.ParametrosContratoProductor;
import mx.gob.segalmex.granos.core.contrato.busqueda.ParametrosInscripcionContrato;
import mx.gob.segalmex.granos.core.contrato.pdf.InscripcionContratoPdfCreator;
import mx.gob.segalmex.granos.core.contrato.xls.InscripcionContratoXlsCreator;
import mx.gob.segalmex.granos.core.historico.BuscadorHistoricoRegistro;
import mx.gob.segalmex.granos.core.inscripcion.BuscadorCicloAgricolaActivo;
import mx.gob.segalmex.granos.core.inscripcion.BuscadorComentarioInscripcion;
import mx.gob.segalmex.granos.core.inscripcion.ProcesadorComentarioInscripcion;
import mx.gob.segalmex.granos.web.modelo.contrato.ContratoProductorConverter;
import mx.gob.segalmex.granos.web.modelo.contrato.DatoCapturadoConverter;
import mx.gob.segalmex.granos.web.modelo.contrato.InscripcionContratoConverter;
import mx.gob.segalmex.granos.web.modelo.inscripcion.ComentarioInscripcionConverter;
import mx.gob.segalmex.granos.web.modelo.productor.InscripcionAgrupadaConverter;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import mx.gob.segalmex.pluss.cultivos.web.resources.productor.CultivoHelper;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import mx.gob.segalmex.pluss.modelo.archivos.ArchivoRelacionado;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricolaEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.ReferenciaReporteEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoPrecioEnum;
import mx.gob.segalmex.pluss.modelo.historico.HistoricoRegistro;
import mx.gob.segalmex.pluss.modelo.inscripcion.ComentarioInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.ContratoProductor;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionAgrupada;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.apache.commons.io.IOUtils;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Path("/contratos/")
@Component
@Slf4j
public class ContratoResource {

    private static final String INSCRIPCION = "productor";

    @Autowired
    private ContratoResourceHelper resourceHelper;

    @Autowired
    private BuscadorInscripcionContrato buscadorContrato;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorInscripcionContrato buscadorInscripcionContrato;

    @Autowired
    private BuscadorArchivoRelacionado buscadorArchivoRelacionado;

    @Autowired
    private BuscadorDatoCapturado buscadorDatoCapturado;

    @Autowired
    private BuscadorHistoricoRegistro buscadorHistorico;

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private ProcesadorInscripcionContrato procesadorInscripcionContrato;

    @Autowired
    private RegistroArchivoRelacionado registroArchivoRelacionado;

    @Autowired
    private UsuarioSesionContextoHolder contextoSeguridad;

    @Autowired
    private AmbienteUtils ambienteUtils;

    @Autowired
    private BuscadorComentarioInscripcion buscadorComentarioInscripcion;

    @Autowired
    private ProcesadorComentarioInscripcion procesadorComentarioInscripcion;

    @Autowired
    CultivoHelper cultivoHelper;

    @Autowired
    private BuscadorCicloAgricolaActivo buscadorCicloAgricola;

    @Autowired
    private BuscadorInscripcionContratoHelper buscadorInscripcionContratoHelper;

    @Autowired
    private ManejadorArchivo manejadorArchivo;

    @Autowired
    private ComplementoSubResource complementoSubResource;

    @Autowired
    private ProcesadorContratoProductorXls procesadorContratoProductorXls;

    @Path("/maiz/inscripcion/{uuid}/complemento/")
    public ComplementoSubResource getComplementoSubResource() {
        return complementoSubResource;
    }

    @Path("/maiz/inscripcion/")
    @GET
    @Produces("application/json")
    public List<InscripcionContratoConverter> index(@BeanParam InscripcionParam params) {
        ParametrosInscripcionContrato parametros = resourceHelper.getInstance(params);
        List<InscripcionContrato> resultados = buscadorInscripcionContrato.busca(parametros);

        return CollectionConverter.convert(InscripcionContratoConverter.class,
                resourceHelper.simplificaPropiedades(resultados), 2);
    }

    @GET
    @Path("/maiz/inscripcion/agrupada")
    @Produces("application/json")
    public List<InscripcionAgrupadaConverter> buscaAgrupada(@BeanParam InscripcionParam params) {
        ParametrosInscripcionContrato parametros = resourceHelper.getInstance(params);
        List<InscripcionAgrupada> resultados = buscadorInscripcionContratoHelper.busca(parametros);

        return InscripcionAgrupadaConverter.convert(resultados, 2);
    }

    @Path("/maiz/inscripcion/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionContratoConverter registra(InscripcionContratoConverter converter) {
        InscripcionContrato inscripcion = converter.getEntity();

        if (Objects.isNull(inscripcion.getUuid())) {
            inscripcion.setCultivo(cultivoHelper.getCultivo());
            resourceHelper.inicializaCatalogos(inscripcion);
            resourceHelper.validaContratosProductor(inscripcion.getContratosProductor());
            validaCobertura(inscripcion);
            procesadorInscripcionContrato.procesa(inscripcion);
            return new InscripcionContratoConverter(inscripcion, 1);
        } else {
            return converter;
        }
    }

    @Path("/maiz/inscripcion/{uuid}/contrato/pdf/")
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces({"application/json;charset=utf-8"})
    public RespuestaConverter registraContrato(@PathParam("uuid") String uuid, MultipartFormDataInput input) throws IOException {
        return registraArchivo("contrato", uuid, input);
    }

    private RespuestaConverter registraArchivo(String tipo, String uuid, MultipartFormDataInput input) throws IOException {
        InscripcionContrato inscripcion = buscadorInscripcionContrato.buscaElemento(uuid);

        InputStream is = ArchivoFormUtils.getInputStrem(input);
        String nombre = ArchivoFormUtils.getFilename(input, "pdf");
        String subidirectorio = SubDirectorioHelper.getSubdirectorio(inscripcion.getCiclo().getClave(),
                "contratos", inscripcion.getFolio());
        Archivo archivo = ArchivoUtils.getInstance(nombre, "maiz", subidirectorio, false);

        ArchivoRelacionado ar = new ArchivoRelacionado();
        ar.setArchivo(archivo);
        ar.setUsuarioRegistra(contextoSeguridad.getUsuario());
        ar.setClase(InscripcionContrato.class.getSimpleName());
        ar.setReferencia(inscripcion.getUuid());
        ar.setTipo(tipo);

        registroArchivoRelacionado.registra(archivo, ar, is);

        if ("contrato".equals(tipo)) {
            inscripcion.setContrato(archivo);
            registroEntidad.actualiza(inscripcion);
        }

        RespuestaConverter respuesta = new RespuestaConverter();
        respuesta.setMensaje("Archivo almacenado correctamente: " + tipo);
        return respuesta;
    }

    @Path("/maiz/inscripcion/{uuid}/inscripcion-contrato.pdf")
    @GET
    @Produces("application/pdf")
    public Response getComprobante(@PathParam("uuid") String uuid) {
        InscripcionContrato inscripcion = buscadorInscripcionContrato.buscaElemento(uuid);
        InscripcionContratoPdfCreator creator
                = new InscripcionContratoPdfCreator(ambienteUtils.getValor(AmbienteUtils.AMBIENTE));
        byte[] pdf = creator.create(inscripcion);

        StreamingOutput so = (OutputStream out) -> {
            IOUtils.write(pdf, out);
            out.close();
        };

        return Response.ok(so)
                .header("Content-Disposition", "attachment;filename=contrato-"
                        + inscripcion.getFolio() + ".pdf")
                .build();
    }

    @Path("/maiz/inscripcion/{uuid}")
    @GET
    @Produces("application/json")
    public InscripcionContratoConverter getInscripcion(@PathParam("uuid") String uuid,
            @DefaultValue("2") @QueryParam("expand") int expandLevel,
            @QueryParam("archivos") @DefaultValue("false") boolean archivos,
            @QueryParam("datos") @DefaultValue("false") boolean datos,
            @QueryParam("historial") @DefaultValue("false") boolean historial,
            @QueryParam("comentarios") @DefaultValue("false") boolean comentarios) {
        InscripcionContrato inscripcion = buscadorInscripcionContrato.buscaElemento(uuid);
        inscripcion.setContratosProductor(resourceHelper.getContratosProductor(inscripcion));
        InscripcionContratoConverter converter = new InscripcionContratoConverter(inscripcion, expandLevel);
        if (archivos) {
            List<ArchivoRelacionado> relacionados = buscadorArchivoRelacionado.busca(InscripcionContrato.class, uuid);
            List<Archivo> ars = new ArrayList<>();
            for (ArchivoRelacionado a : relacionados) {
                Archivo archivo = a.getArchivo();
                archivo.setTipo(a.getTipo());
                ars.add(archivo);
            }
            converter.setArchivos(ars);
        }
        if (datos) {
            List<DatoCapturado> dcs = buscadorDatoCapturado.busca(InscripcionContrato.class, uuid, true);
            converter.setDatos(dcs);
        }
        if (historial) {
            List<HistoricoRegistro> his = buscadorHistorico.busca(inscripcion);
            converter.setHistorial(his);
        }
        if (comentarios) {
            List<ComentarioInscripcion> cis = buscadorComentarioInscripcion.busca(inscripcion);
            converter.setComentarios(cis);
        }
        return converter;
    }

    @Path("/maiz/inscripcion/{uuid}/anexos/{archivo}/pdf/")
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces({"application/json;charset=utf-8"})
    public RespuestaConverter registraAnexos(@PathParam("uuid") String uuid,
            @PathParam("archivo") String tipoArchivo, MultipartFormDataInput input) throws IOException {
        InscripcionContrato inscripcion = buscadorInscripcionContrato.buscaElemento(uuid);

        InputStream is = ArchivoFormUtils.getInputStrem(input);
        String nombre = ArchivoFormUtils.getFilename(input, "pdf");
        String subidirectorio = SubDirectorioHelper.getSubdirectorio(inscripcion.getCiclo().getClave(),
                "contratos", inscripcion.getFolio());
        Archivo archivo = ArchivoUtils.getInstance(nombre, CultivoClaveUtil.getClaveCorta(inscripcion.getCultivo()),
                subidirectorio, false);

        ArchivoRelacionado ar = new ArchivoRelacionado();
        ar.setArchivo(archivo);
        ar.setUsuarioRegistra(contextoSeguridad.getUsuario());
        ar.setClase(InscripcionContrato.class.getSimpleName());
        ar.setReferencia(inscripcion.getUuid());
        ar.setTipo(tipoArchivo);

        registroArchivoRelacionado.registra(archivo, ar, is);

        if ("contrato".equals(tipoArchivo)) {
            inscripcion.setContrato(archivo);
            registroEntidad.actualiza(inscripcion);
        }

        RespuestaConverter respuesta = new RespuestaConverter();
        respuesta.setMensaje("Archivo almacenado correctamente: " + tipoArchivo);
        return respuesta;
    }

    @Path("/maiz/inscripcion/{uuid}/validacion/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionContratoConverter asignaValidador(@PathParam("uuid") String uuid, UsuarioConverter converter) {
        InscripcionContrato inscripcion = buscadorInscripcionContrato.buscaElemento(uuid);
        Usuario validador = buscadorCatalogo.buscaElemento(Usuario.class, converter.getEntity().getId());
        procesadorInscripcionContrato.asigna(inscripcion, contextoSeguridad.getUsuario(), validador);

        return new InscripcionContratoConverter(inscripcion, 1);
    }

    @Path("/maiz/inscripcion/{uuid}/validacion/positiva/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionContratoConverter validaPositivo(@PathParam("uuid") String uuid, List<DatoCapturadoConverter> datos) {
        InscripcionContrato inscripcion = buscadorInscripcionContrato.buscaElemento(uuid);
        resourceHelper.getValidaEstatus(inscripcion.getEstatus(),
                EstatusInscripcionEnum.VALIDACION, EstatusInscripcionEnum.SOLVENTADA);
        List<DatoCapturado> dcs = new ArrayList<>();
        for (DatoCapturadoConverter dc : datos) {
            dcs.add(dc.getEntity());
        }
        procesadorInscripcionContrato.validaPositivo(inscripcion, contextoSeguridad.getUsuario(), dcs);
        return new InscripcionContratoConverter(inscripcion, 0);
    }

    @Path("/maiz/inscripcion/{uuid}/validacion/negativa/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionContratoConverter validaNegativo(@PathParam("uuid") String uuid, List<DatoCapturadoConverter> datos) {
        InscripcionContrato inscripcion = buscadorInscripcionContrato.buscaElemento(uuid);
        resourceHelper.getValidaEstatus(inscripcion.getEstatus(),
                EstatusInscripcionEnum.VALIDACION, EstatusInscripcionEnum.SOLVENTADA);
        List<DatoCapturado> dcs = new ArrayList<>();
        for (DatoCapturadoConverter dc : datos) {
            dcs.add(dc.getEntity());
        }
        procesadorInscripcionContrato.solicitaCorreccion(inscripcion, contextoSeguridad.getUsuario(), dcs);
        return new InscripcionContratoConverter(inscripcion, 0);
    }

    @Path("/maiz/inscripcion/{uuid}/validacion/negativa/")
    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionContratoConverter corrigeValidacionNegativa(@PathParam("uuid") String uuid, List<DatoCapturadoConverter> datos) {
        InscripcionContrato inscripcion = buscadorInscripcionContrato.buscaElemento(uuid);
        resourceHelper.getValidaEstatus(inscripcion.getEstatus(), EstatusInscripcionEnum.CORRECCION);
        List<DatoCapturado> dcs = new ArrayList<>();
        for (DatoCapturadoConverter dc : datos) {
            dcs.add(dc.getEntity());
        }
        procesadorInscripcionContrato.corrige(inscripcion, contextoSeguridad.getUsuario(), dcs);
        return new InscripcionContratoConverter(inscripcion, 0);
    }

    @Path("/maiz/inscripcion/{uuid}/validacion/actualizacion/")
    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionContratoConverter actualizaValidacionNegativa(@PathParam("uuid") String uuid, List<DatoCapturadoConverter> datos) {
        InscripcionContrato inscripcion = buscadorInscripcionContrato.buscaElemento(uuid);
        List<DatoCapturado> dcs = new ArrayList<>();
        for (DatoCapturadoConverter dc : datos) {
            dcs.add(dc.getEntity());
        }
        procesadorInscripcionContrato.actualiza(inscripcion, dcs);
        return new InscripcionContratoConverter(inscripcion, 0);
    }

    @Path("/maiz/inscripcion/{uuid}/validacion/reasignacion/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionContratoConverter cambiaValidador(@PathParam("uuid") String uuid, UsuarioConverter converter) {
        InscripcionContrato inscripcion = buscadorInscripcionContrato.buscaElemento(uuid);
        Usuario validador = buscadorCatalogo.buscaElemento(Usuario.class, converter.getEntity().getId());
        Usuario validadorActual = inscripcion.getUsuarioValidador();
        if (Objects.isNull(validadorActual)) {
            throw new IllegalArgumentException("El usuario validador no ha sigo asignado todavía.");
        }
        inscripcion.setUsuarioValidador(validador);
        if (inscripcion.getUsuarioAsignado().getNombre().equals(validadorActual.getNombre())) {
            inscripcion.setUsuarioAsignado(validador);
        }
        registroEntidad.actualiza(inscripcion);
        return new InscripcionContratoConverter(inscripcion, 1);
    }

    /**
     * Forza una revalidación para los casos en que se quiere regresar a
     * validación para hacer alguna modificación.
     *
     * @param uuid
     * @return
     */
    @Path("/maiz/inscripcion/{uuid}/revalidacion/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionContratoConverter revalida(@PathParam("uuid") String uuid) {
        InscripcionContrato inscripcion = buscadorInscripcionContrato.buscaElemento(uuid);
        procesadorInscripcionContrato.revalida(inscripcion, contextoSeguridad.getUsuario());

        return new InscripcionContratoConverter(inscripcion, 1);
    }

    @Path("/maiz/inscripcion/{uuid}/cancelacion/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionContratoConverter cancela(@PathParam("uuid") String uuid, InscripcionContratoConverter converter) {
        return cancela(uuid, converter, buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.CANCELADA.getClave()));
    }

    @Path("/maiz/inscripcion/{uuid}/negativo/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionContratoConverter negativo(@PathParam("uuid") String uuid, InscripcionContratoConverter converter) {
        return cancela(uuid, converter, buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.NEGATIVO.getClave()));
    }

    private InscripcionContratoConverter cancela(String uuid, InscripcionContratoConverter converter, EstatusInscripcion estatus) {
        InscripcionContrato inscripcion = buscadorInscripcionContrato.buscaElemento(uuid);
        inscripcion.setComentarioEstatus(converter.getComentarioEstatus());
        switch (estatus.getClave()) {
            case "cancelada":
            case "negativo":
                break;
            default:
                throw new IllegalArgumentException("Estatus no permitido para cancelar");
        }
        procesadorInscripcionContrato.cancela(inscripcion, contextoSeguridad.getUsuario(), estatus);
        return new InscripcionContratoConverter(inscripcion, 0);
    }

    @Path("/maiz/inscripcion/{uuid}/comentario/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionContratoConverter agregaComentario(@PathParam("uuid") String uuid, ComentarioInscripcionConverter converter) {
        InscripcionContrato inscripcion = buscadorInscripcionContrato.buscaElemento(uuid);
        ComentarioInscripcion ci = converter.getEntity();
        procesadorComentarioInscripcion.procesa(ci, inscripcion, contextoSeguridad.getUsuario());
        return new InscripcionContratoConverter(inscripcion, 0);
    }

    @Path("/maiz/simplificados/")
    @GET
    @Produces("application/json")
    public List<InscripcionContratoConverter> getSimplficiados() {
        ParametrosInscripcionContrato parametros = new ParametrosInscripcionContrato();
        List<CicloAgricola> ciclosActivos = buscadorCicloAgricola.busca(cultivoHelper.getCultivo(), INSCRIPCION, contextoSeguridad.getUsuario());
        CicloAgricola seleccionado
                = buscadorCicloAgricola.getCicloSeleccionado(cultivoHelper.getCultivo(), ciclosActivos, contextoSeguridad.getUsuario(), true);
        parametros.setCiclo(seleccionado);
        parametros.setCultivo(cultivoHelper.getCultivo());
        parametros.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.POSITIVA.getClave()));
        List<InscripcionContrato> contratos = buscadorContrato.busca(parametros);

        List<InscripcionContratoConverter> converters = new ArrayList<>();
        for (InscripcionContrato c : contratos) {
            InscripcionContrato s = new InscripcionContrato();
            s.setNumeroContrato(c.getNumeroContrato());
            s.setFolio(c.getFolio());
            s.setEstatus(new EstatusInscripcion());
            s.getEstatus().setNombre(c.getEstatus().getNombre());
            s.getEstatus().setClave(c.getEstatus().getClave());

            InscripcionContratoConverter cvt = new InscripcionContratoConverter(s, 1);
            cvt.setNombreEmpresa(c.getSucursal().getEmpresa().getNombre());
            converters.add(cvt);
        }
        return converters;
    }

    @Path("/maiz/inscripcion/csv/resultados-contrato.xlsx")
    @GET
    @Produces("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    public byte[] indexXls(@BeanParam InscripcionParam params) {
        ParametrosInscripcionContrato parametros = resourceHelper.getInstance(params);
        List<InscripcionContrato> resultados = buscadorInscripcionContrato.busca(parametros);
        InscripcionContratoXlsCreator xlsCreator = new InscripcionContratoXlsCreator();
        return xlsCreator.exporta(resultados, cultivoHelper.getCultivo());
    }

    @Path("/reporte/{ciclo}.xlsx")
    @GET
    @Produces("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    public Response getReporteCompleto(@PathParam("ciclo") Integer ciclo) {
        try {
            CicloAgricola c = buscadorCatalogo.buscaElemento(CicloAgricola.class, ciclo);
            String tipo = "registro-contratos-" + cultivoHelper.getCultivo().getClave() + "-" + c.getClave() + "-completo";
            ArchivoRelacionado ar = buscadorArchivoRelacionado.buscaElemento(InscripcionContrato.class.getSimpleName(),
                    ReferenciaReporteEnum.CONTRATO.getClave(), tipo);
            byte[] xls = manejadorArchivo.obten(ar.getArchivo());
            StreamingOutput so = (OutputStream out) -> {
                IOUtils.write(xls, out);
                out.close();
            };
            return Response.ok(so)
                    .header("Content-Disposition", "attachment;filename="
                            + ar.getArchivo().getNombreOriginal())
                    .build();
        } catch (IOException ex) {
            throw new SegalmexRuntimeException("Error:", "No fue posible generar el reporte de contratos.");
        }
    }

    /**
     * Se encarga de validar que todos tengan cobertura si es que pertenecen a
     * un ciclo que no sea OI2020.
     *
     * @param inscripcion
     */
    private void validaCobertura(InscripcionContrato inscripcion) {
        switch (CicloAgricolaEnum.getInstance(inscripcion.getCiclo().getClave())) {
            case OI2020:
                return;
        }
        switch (CultivoEnum.getInstance(inscripcion.getCultivo().getClave())) {
            case TRIGO:
            case MAIZ_COMERCIAL:
                switch (TipoPrecioEnum.getInstance(inscripcion.getTipoPrecio().getClave())) {
                    case PRECIO_CERRADO_DOLARES:
                    case PRECIO_CERRADO_PESOS:
                        return;
                }
        }
        if (!inscripcion.getCobertura()) {
            throw new SegalmexRuntimeException("Error de registro.",
                    "Se requiere cobertura para todos los productores.");
        }
        if (inscripcion.getCoberturas().isEmpty()) {
            throw new SegalmexRuntimeException("Error de registro.",
                    "Se requiere al menos la información de una cobertura.");
        }
    }

    @Path("/inscripcion/administracion")
    @GET
    @Produces("application/json")
    public List<InscripcionContratoConverter> buscaPorFolio(@BeanParam InscripcionParam params) {
        Objects.requireNonNull(params.getFolio(), "El folio no puede ser nulo.");
        ParametrosInscripcionContrato parametros = new ParametrosInscripcionContrato();
        parametros.setFolio(params.getFolio().getValue());
        List<InscripcionContrato> resultados = buscadorInscripcionContrato.busca(parametros);
        return CollectionConverter.convert(InscripcionContratoConverter.class,
                resourceHelper.simplificaPropiedades(resultados), 2);
    }

    @Path("/maiz/inscripcion/productor/curp-xlsx")
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces({"application/json;charset=utf-8"})
    public List<ContratoProductorConverter> procesaCurpsXlsx(MultipartFormDataInput input)
            throws IOException {
        LOGGER.info("Recibiendo archivo para su procesamiento...");
        InputStream is = ArchivoFormUtils.getInputStrem(input);
        String nombre = ArchivoFormUtils.getFilename(input, "xlsx");
        if (!(nombre.contains(".xls") || nombre.contains(".xlsx"))) {
            throw new SegalmexRuntimeException("Error",
                    "El archivo no tiene extensión xls o xlsx.");
        }
        CicloAgricola ciclo = buscadorCicloAgricola.getCicloSeleccionadoActivo(
                cultivoHelper.getCultivo(), contextoSeguridad.getUsuario());
        List<ContratoProductor> elementos = procesadorContratoProductorXls.procesa(IOUtils.toByteArray(is),
                cultivoHelper.getCultivo(), ciclo);
        if (elementos.isEmpty()) {
            throw new SegalmexRuntimeException("Error al validar el archivo.",
                    "El archivo no tiene información para procesar.");
        }
        return CollectionConverter.convert(ContratoProductorConverter.class,
                elementos, 2);
    }

    @Path("/maiz/inscripcion/{uuid}/correccion/contrato-productor/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionContratoConverter correccionContratoProductor(@PathParam("uuid") String uuid, InscripcionContratoConverter converter) {
        InscripcionContrato inscripcion = buscadorInscripcionContrato.buscaElemento(uuid);
        switch (EstatusInscripcionEnum.getInstance(inscripcion.getEstatus().getClave())) {
            case VALIDACION:
            case SOLVENTADA:
                procesadorInscripcionContrato.solicitaCorreccionContratoProductor(inscripcion, contextoSeguridad.getUsuario());
                return new InscripcionContratoConverter(inscripcion, 0);
            default:
                throw new SegalmexRuntimeException("Error al enviar a corrección:",
                        "Estatus no permitido para corregir el listado de productores.");
        }
    }

    @Path("/maiz/inscripcion/{uuid}/corrige/contrato-productor/")
    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionContratoConverter corrigeContratoProductor(InscripcionContratoConverter converter) {
        InscripcionContrato inscripcion = converter.getEntity();
        List<ContratoProductor> contratosProductor = inscripcion.getContratosProductor();
        inscripcion = buscadorInscripcionContrato.buscaElemento(inscripcion.getUuid());
        switch (EstatusInscripcionEnum.getInstance(inscripcion.getEstatus().getClave())) {
            case CORRECCION:
                resourceHelper.validaContratosProductor(contratosProductor);
                procesadorInscripcionContrato.corrigeContratoProductor(inscripcion, contextoSeguridad.getUsuario(), contratosProductor);
                return new InscripcionContratoConverter(inscripcion, 1);
            default:
                throw new SegalmexRuntimeException("Error al enviar a corrección:",
                        "Estatus no permitido para corregir el listado de productores.");
        }

    }

    @Path("/maiz/contratos-productor/")
    @GET
    @Produces("application/json")
    public List<ContratoProductorConverter> getContratosProductor(@BeanParam InscripcionParam params) {
        CicloAgricola seleccionado = buscadorCicloAgricola.getCicloSeleccionadoActivo(cultivoHelper
                .getCultivo(), contextoSeguridad.getUsuario());
        ParametrosContratoProductor parametros = new ParametrosContratoProductor();
        parametros.setCurp(params.getCurp());
        parametros.setRfc(params.getRfcProductor());
        parametros.setCultivo(cultivoHelper.getCultivo());
        parametros.setCiclo(Objects.nonNull(params.getCiclo())
                ? buscadorCatalogo.buscaElemento(CicloAgricola.class, params.getCiclo())
                : seleccionado);
        parametros.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.POSITIVA));
        return resourceHelper.getContratosProductor(parametros);
    }
}
