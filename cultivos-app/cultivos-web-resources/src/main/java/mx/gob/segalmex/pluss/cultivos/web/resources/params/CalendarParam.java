/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.params;

import java.util.Calendar;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;

/**
 *
 * @author ismael
 */
public class CalendarParam extends AbstractParam<Calendar> {

    /**
     * Formato dela fecha
     */
    public static final String DATE_FORMAT = "yyyy-MM-dd";

    /**
     * Formato de fecha con hora
     */
    public static final String DATE_HOUR_FORMAT = "yyyy-MM-dd'T'HH:mm";

    /**
     * Constructor de la clase
     * @param param
     */
    public CalendarParam(String param) {
        super(param);
    }

    /**
     *
     * {@inheritDoc}
     *
     */
    @Override
    protected Calendar parse(String param) {
        param = StringUtils.trimToNull(param);

        Calendar value = null;
        if (param != null) {
            if (param.length() == DATE_FORMAT.length()) {
                value = SegalmexDateUtils.parseCalendar(param, DATE_FORMAT);
                value = DateUtils.truncate(value, Calendar.DATE);
            } else if (param.length() == DATE_HOUR_FORMAT.length() - 2) {
                value = SegalmexDateUtils.parseCalendar(param, DATE_HOUR_FORMAT);
                DateUtils.truncate(value, Calendar.SECOND);
            } else {
                throw new IllegalArgumentException("El parámetro: "
                        + param + " no cumple con el formato.");
            }
        }

        return value;
    }

}
