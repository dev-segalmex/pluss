/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.paginas;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.seguridad.busqueda.BuscadorPermiso;
import mx.gob.segalmex.common.web.modelo.EnumConverter;
import mx.gob.segalmex.common.web.modelo.inscripcion.CatalogosBandejasConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoRegistroEnum;
import mx.gob.segalmex.pluss.modelo.seguridad.Accion;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component
public class BandejaAsignacionSubResource {

    @Autowired
    private BuscadorPermiso buscadorPermiso;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @GET
    @Produces("application/json")
    public CatalogosBandejasConverter getCatalogos() {
        CatalogosBandejasConverter converter = new CatalogosBandejasConverter();
        Accion a = buscadorCatalogo.buscaElemento(Accion.class, "validar_inscripcion");
        List<Usuario> usuarios = buscadorPermiso.buscaUsuarioAutorizado(a, Boolean.TRUE);
        List<Cultivo> cultivos = buscadorCatalogo.busca(Cultivo.class);
        List<CicloAgricola> ciclos = buscadorCatalogo.busca(CicloAgricola.class);
        Collections.sort(usuarios, (o1, o2) -> {
            return o1.getNombre().compareTo(o2.getNombre());
        });
        converter.setValidadores(filtraActivos(usuarios));
        converter.setCultivos(cultivos);
        converter.setCiclos(ciclos);
        converter.setTiposRegistro(getTiposRegistro());
        return converter;
    }

    @Path("/ventanilla/")
    @GET
    @Produces("application/json")
    public CatalogosBandejasConverter getUsuariosAsinacion() {
        CatalogosBandejasConverter converter = new CatalogosBandejasConverter();
        Accion a = buscadorCatalogo.buscaElemento(Accion.class, "validar_empresa");
        List<Usuario> usuarios = buscadorPermiso.buscaUsuarioAutorizado(a, Boolean.TRUE);
        Collections.sort(usuarios, (o1, o2) -> {
            return o1.getNombre().compareTo(o2.getNombre());
        });
        converter.setValidadores(usuarios);
        return converter;
    }

    private List<Usuario> filtraActivos(List<Usuario> usuarios) {
        List<Usuario> activos = new ArrayList<>();
        for (Usuario u : usuarios) {
            if (Objects.nonNull(u.getFechaBloqueo())) {
                continue;
            }
            if (!u.isActivo()) {
                continue;
            }
            activos.add(u);
        }
        return activos;
    }

    private List<EnumConverter> getTiposRegistro() {
        List<EnumConverter> tipos = new ArrayList<>();
        for (TipoRegistroEnum e : TipoRegistroEnum.values()) {
            EnumConverter tipo = new EnumConverter();
            tipo.setClave(e.getClave());
            tipo.setNombre(e.getNombre());
            tipos.add(tipo);
        }
        return tipos;
    }
}
