/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.inscripcion;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.archivos.BuscadorArchivoRelacionado;
import mx.gob.segalmex.common.core.archivos.ManejadorArchivo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorUsoFactura;
import mx.gob.segalmex.common.core.pago.ProcesadorRequerimientoPago;
import mx.gob.segalmex.common.core.pago.busqueda.BuscadorRequerimientoPago;
import mx.gob.segalmex.common.core.pago.busqueda.ParametrosRequerimientoPago;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.factura.RequerimientoPagoConverter;
import mx.gob.segalmex.common.web.modelo.factura.UsoFacturaConverter;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import mx.gob.segalmex.pluss.modelo.archivos.ArchivoRelacionado;
import mx.gob.segalmex.pluss.modelo.factura.EstatusUsoFacturaEnum;
import mx.gob.segalmex.pluss.modelo.factura.UsoFactura;
import mx.gob.segalmex.pluss.modelo.pago.EstatusRequerimientoPagoEnum;
import mx.gob.segalmex.pluss.modelo.pago.RequerimientoPago;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Slf4j
@Path("/requerimientos-pago/")
@Component
public class RequerimientoPagoResource {

    @Autowired
    private BuscadorRequerimientoPago buscadorRequerimientoPago;

    @Autowired
    private ProcesadorRequerimientoPago procesadorRequerimientoPago;

    @Autowired
    private UsuarioSesionContextoHolder contextoSeguridad;

    @Autowired
    private RequerimientoPagoResourceHelper helper;

    @Autowired
    private BuscadorArchivoRelacionado buscadorArchivoRelacionado;

    @Autowired
    private ManejadorArchivo manejadorArchivo;

    @Autowired
    private BuscadorUsoFactura buscadorUsoFactura;

    @GET
    @Produces("application/json")
    public List<RequerimientoPagoConverter> index() {
        List<RequerimientoPago> resultados
                = buscadorRequerimientoPago.busca(new ParametrosRequerimientoPago());
        return CollectionConverter.convert(RequerimientoPagoConverter.class,
                helper.simplificaPropiedades(resultados), 2);

    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public RequerimientoPagoConverter registra(RequerimientoPagoConverter converter) throws IOException {
        RequerimientoPago rp = converter.getEntity();
        if (Objects.isNull(rp.getId())) {
            List<UsoFactura> usosFactura
                    = helper.getUsosFactura(converter.getUsosFacturasUuid());

            LOGGER.info("NUEVO REQUERIMIENTO DE PAGO.");
            helper.inicializaCatalogos(rp);
            RequerimientoPago requerimiento
                    = procesadorRequerimientoPago.procesa(usosFactura, contextoSeguridad.getUsuario(), rp);

            return new RequerimientoPagoConverter(requerimiento, 1);
        } else {
            return converter;
        }
    }

    @PUT
    @Path("/{uuid}")
    @Consumes("application/json")
    @Produces("application/json")
    public RequerimientoPagoConverter aprueba(@PathParam("uuid") String uuid) {
        RequerimientoPago requerimiento = buscadorRequerimientoPago.buscaElemento(uuid);
        procesadorRequerimientoPago.aprueba(requerimiento);
        return new RequerimientoPagoConverter(requerimiento, 1);
    }

    @PUT
    @Path("/pagar/{uuid}")
    @Consumes("application/json")
    @Produces("application/json")
    public RequerimientoPagoConverter marcaPago(@PathParam("uuid") String uuid) {
        RequerimientoPago requerimiento = buscadorRequerimientoPago.buscaElemento(uuid);
        procesadorRequerimientoPago.marcaPago(requerimiento);
        return new RequerimientoPagoConverter(requerimiento, 1);
    }

    @DELETE
    @Path("/{uuid}")
    @Consumes("application/json")
    @Produces("application/json")
    public RequerimientoPagoConverter cancela(@PathParam("uuid") String uuid) {
        RequerimientoPago requerimiento = buscadorRequerimientoPago.buscaElemento(uuid);
        Calendar limite = Calendar.getInstance();
        limite.add(Calendar.MINUTE, -10);
        if (requerimiento.getEstatus().equals(EstatusRequerimientoPagoEnum.APROBADO.getClave())
                && requerimiento.getFechaActualizacion().compareTo(limite) <= 0) {
            throw new SegalmexRuntimeException("Error al cancelar.",
                    "El requerimiento se encuentra en un estatus incorrecto para ser cancelado.");
        }
        procesadorRequerimientoPago.cancela(requerimiento);
        return new RequerimientoPagoConverter(requerimiento, 1);
    }

    @Path("/{uuid}")
    @GET
    @Produces("application/json")
    public RequerimientoPagoConverter getRequerimiento(@PathParam("uuid") String uuid,
            @DefaultValue("2") @QueryParam("expand") int expandLevel) {
        RequerimientoPago requerimiento = buscadorRequerimientoPago.buscaElemento(uuid);
        RequerimientoPagoConverter converter = new RequerimientoPagoConverter(requerimiento, expandLevel);

        return converter;
    }

    @Path("/{uuid}/totales")
    @PUT
    @Produces("application/json")
    public RequerimientoPagoConverter getTotales(@PathParam("uuid") String uuid,
            @DefaultValue("2") @QueryParam("expand") int expandLevel) {
        RequerimientoPago requerimiento = buscadorRequerimientoPago.buscaElemento(uuid);
        procesadorRequerimientoPago.actualizaTotales(requerimiento);
        RequerimientoPagoConverter converter = new RequerimientoPagoConverter(requerimiento, expandLevel);

        return converter;
    }

    @Path("/{uuid}/requerimiento-pago.xlsx")
    @GET
    @Produces("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    public Response indexXls(@PathParam("uuid") String uuid) {
        LOGGER.info("Obteniendo Archivo xlsx del RequerimientoPago {}", uuid);
        RequerimientoPago requerimiento = buscadorRequerimientoPago.buscaElemento(uuid);

        try {
            ArchivoRelacionado ar = buscadorArchivoRelacionado.buscaElemento(RequerimientoPago.class.getSimpleName(),
                    requerimiento.getUuid(), "requerimiento-pago");
            return getXlsx(ar.getArchivo());
        } catch (IOException ex) {
            LOGGER.error("No fue posible obtener el reporte del requerimiento de pago {}.", requerimiento.getUuid());
            throw new SegalmexRuntimeException("Error:", "No fue posible obtener el reporte del requerimiento de pago.");
        }
    }

    private Response getXlsx(Archivo archivo) throws IOException {
        byte[] xls = manejadorArchivo.obten(archivo);
        StreamingOutput so = (OutputStream out) -> {
            IOUtils.write(xls, out);
            out.close();
        };
        return Response.ok(so)
                .header("Content-Disposition", "attachment;filename="
                        + archivo.getNombreOriginal())
                .build();
    }

    @Path("/administracion/")
    @GET
    @Produces("application/json")
    public RequerimientoPagoConverter buscaRequerimiento(@BeanParam RequerimientoPagoParam params) {
        ParametrosRequerimientoPago parametros = new ParametrosRequerimientoPago();
        parametros.setCegap(Objects.nonNull(params.getCegap()) ? params.getCegap() : null);
        parametros.setId(Objects.nonNull(params.getId()) ? params.getId() : null);
        try {
            return new RequerimientoPagoConverter(buscadorRequerimientoPago.buscaElemento(parametros), 2);
        } catch (EmptyResultDataAccessException ouch) {
            throw new SegalmexRuntimeException("Error:", "No se encontró el requerimiento con los parámetros de búsqueda.");
        }
    }

    @PUT
    @Path("/{uuid}/uso-factura/no-pagado-cancelado")
    @Consumes("application/json")
    @Produces("application/json")
    public RequerimientoPagoConverter cancelaNoPagado(@PathParam("uuid") String uuid,
            UsoFacturaConverter usoConverter) {
        UsoFactura uso = buscadorUsoFactura.buscaElementoUuid(usoConverter.getUuid());
        RequerimientoPago rp = buscadorRequerimientoPago.buscaElemento(uuid);
        procesadorRequerimientoPago.cancela(rp, uso,
                EstatusUsoFacturaEnum.NO_PAGADO_CANCELADO, usoConverter.getComentarioEstatus());
        rp = buscadorRequerimientoPago.buscaElemento(uuid);
        return new RequerimientoPagoConverter(rp, 1);
    }

    @PUT
    @Path("/{uuid}/uso-factura/no-pagado-devuelto")
    @Consumes("application/json")
    @Produces("application/json")
    public RequerimientoPagoConverter devuelveNoPagado(@PathParam("uuid") String uuid,
            UsoFacturaConverter usoConverter) {
        UsoFactura uso = buscadorUsoFactura.buscaElementoUuid(usoConverter.getUuid());
        RequerimientoPago rp = buscadorRequerimientoPago.buscaElemento(uuid);
        procesadorRequerimientoPago.cancela(rp, uso,
                EstatusUsoFacturaEnum.NO_PAGADO_DEVUELTO, usoConverter.getComentarioEstatus());
        rp = buscadorRequerimientoPago.buscaElemento(uuid);
        return new RequerimientoPagoConverter(rp, 1);
    }

}
