/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.productor;

import java.io.IOException;
import java.io.OutputStream;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.archivos.BuscadorArchivoRelacionado;
import mx.gob.segalmex.common.core.archivos.ManejadorArchivo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorUsoCatalogo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.util.AmbienteUtils;
import mx.gob.segalmex.common.core.validador.ValidadorPreRegistro;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.productor.PreRegistroAgrupadoConverter;
import mx.gob.segalmex.common.web.modelo.productor.PreRegistroProductorConverter;
import mx.gob.segalmex.granos.core.pre.registro.pdf.PreRegistroPdfCreator;
import mx.gob.segalmex.granos.core.productor.ProcesadorPreRegistroProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorPreRegistroProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorPreRegistroProductorHelper;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.xls.PreRegistroProductorXls;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import mx.gob.segalmex.pluss.cultivos.web.resources.inscripcion.InscripcionParam;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import mx.gob.segalmex.pluss.modelo.archivos.ArchivoRelacionado;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.ReferenciaReporteEnum;
import mx.gob.segalmex.pluss.modelo.productor.PreRegistroAgrupado;
import mx.gob.segalmex.pluss.modelo.productor.PreRegistroProductor;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Slf4j
@Component
public class ProductorPreRegistroResource {

    private static final String CLAVE_USUARIO = "anonimo@segalmex.gob.mx";

    private static final String CLAVE_USO = "usuario:registro-anonimo-default";

    @Autowired
    private ProcesadorPreRegistroProductor procesadorPreRegistro;

    @Autowired
    private BuscadorPreRegistroProductor buscadorPreRegistro;

    @Autowired
    private UsuarioSesionContextoHolder contextoSeguridad;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private AmbienteUtils ambienteUtils;

    @Autowired
    private ProductorPreRegistroResourceHelper preRegistroHelper;

    @Autowired
    private PreRegistroProductorXls preRegistroProductorXls;

    @Autowired
    private BuscadorUsoCatalogo buscadorUso;

    @Autowired
    private CultivoHelper cultivoHelper;

    @Autowired
    private BuscadorArchivoRelacionado buscadorArchivoRelacionado;

    @Autowired
    private ManejadorArchivo manejadorArchivo;

    private List<ValidadorPreRegistro> validadadores;

    @Autowired
    private BuscadorPreRegistroProductorHelper buscadorPreRegistroProductorHelper;

    public void setValidadadores(List<ValidadorPreRegistro> validadadores) {
        this.validadadores = validadadores;
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public PreRegistroProductorConverter registra(PreRegistroProductorConverter converter) {
        PreRegistroProductor pre = converter.getEntity();
        pre.setCultivo(cultivoHelper.getCultivo());
        preRegistroHelper.inicializaCatalogos(pre);
        for (ValidadorPreRegistro v : validadadores) {
            v.valida(pre);
        }
        LOGGER.info("Registrando Pre registro");
        Usuario u = buscadorUso.buscaElemento(Usuario.class, CLAVE_USUARIO, CLAVE_USO);
        procesadorPreRegistro.procesa(pre, u, true);
        return new PreRegistroProductorConverter(pre, 1);
    }

    @GET
    @Consumes("application/json")
    @Produces("application/json")
    public List<PreRegistroProductorConverter> busca(@BeanParam InscripcionParam params) {
        ParametrosInscripcionProductor parametros = preRegistroHelper.getInstance(params);
        List<PreRegistroProductor> resultados = buscadorPreRegistro.busca(parametros);
        return CollectionConverter.convert(PreRegistroProductorConverter.class,
                parametros.getSimplifica() ? preRegistroHelper.simplificaPropieades(resultados) : resultados, 2);
    }

    @GET
    @Path("/agrupado")
    @Produces("application/json")
    public List<PreRegistroAgrupadoConverter> buscaAgrupado(@BeanParam InscripcionParam params) {
        ParametrosInscripcionProductor parametros = preRegistroHelper.getInstance(params);
        List<PreRegistroAgrupado> resultados = buscadorPreRegistroProductorHelper.busca(parametros);

        return PreRegistroAgrupadoConverter.convert(resultados, 2);
    }

    @Path("/{uuid}/")
    @GET
    @Produces("application/json")
    public PreRegistroProductorConverter getPreRegistro(@PathParam("uuid") String uuid,
            @DefaultValue("2") @QueryParam("expand") int expandLevel) {
        PreRegistroProductor pre = buscadorPreRegistro.buscaElementoUuid(uuid);
        PreRegistroProductorConverter converter = new PreRegistroProductorConverter(pre, expandLevel);
        return converter;
    }

    @Path("/{uuid}/")
    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public PreRegistroProductorConverter actualiza(@PathParam("uuid") String uuid, PreRegistroProductorConverter converter) {
        PreRegistroProductor original = buscadorPreRegistro.buscaElementoUuid(uuid);
        LOGGER.info("Editando pre registro del cultivo: {}", original.getCultivo().getNombre());
        PreRegistroProductor editado = converter.getEntity();
        editado.setCultivo(original.getCultivo());
        preRegistroHelper.inicializaCatalogos(editado);
        editado.setCiclo(original.getCiclo());
        procesadorPreRegistro.sustituye(original, editado, contextoSeguridad.getUsuario());
        return new PreRegistroProductorConverter(editado, 1);
    }

    @Path("/{uuid}/pre-registro.pdf")
    @GET
    @Produces("application/pdf")
    public Response getComprobante(@PathParam("uuid") String uuid) {
        PreRegistroProductor pre = buscadorPreRegistro.buscaElementoUuid(uuid);
        return buildPdf(pre);
    }

    @Path("/{curp}/reimpresion/{rfc}/{ciclo}/pre-registro.pdf")
    @GET
    @Produces("application/pdf")
    public Response getComprobanteReimpresion(@PathParam("curp") String curp, @PathParam("rfc") String rfc, @PathParam("ciclo") int ciclo) {
        Objects.requireNonNull(curp = StringUtils.stripToNull(curp), "La curp no puede ser nula.");
        Objects.requireNonNull(rfc = StringUtils.stripToNull(rfc), "El rfc no puede ser nulo.");
        ParametrosInscripcionProductor p = new ParametrosInscripcionProductor();
        p.setCurp(StringUtils.upperCase(curp));
        p.setRfcProductor(rfc);
        p.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, ciclo));
        p.setCultivo(buscadorCatalogo.buscaElemento(Cultivo.class, cultivoHelper.getCultivo().getClave()));
        List<PreRegistroProductor> registrados = buscadorPreRegistro.busca(p);
        if (!registrados.isEmpty()) {
            return buildPdf(registrados.get(0));
        }
        throw new WebApplicationException(404);
    }

    private Response buildPdf(PreRegistroProductor pre) {
        PreRegistroPdfCreator creator
                = new PreRegistroPdfCreator(ambienteUtils.getValor(AmbienteUtils.AMBIENTE));
        byte[] pdf = creator.create(pre);

        StreamingOutput so = (OutputStream out) -> {
            IOUtils.write(pdf, out);
            out.close();
        };

        return Response.ok(so)
                .header("Content-Disposition", "attachment;filename=pre-registro-"
                        + pre.getFolio() + ".pdf")
                .build();
    }

    @Path("/csv/resultados-pre-registro.xlsx")
    @GET
    @Produces("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    public byte[] indexXls(@BeanParam InscripcionParam params) {
        ParametrosInscripcionProductor parametros = preRegistroHelper.getInstance(params);
        List<PreRegistroProductor> resultados = buscadorPreRegistro.busca(parametros);
        return preRegistroProductorXls.exporta(resultados);
    }

    @Path("/reporte/{ciclo}.xlsx")
    @GET
    @Produces("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    public Response reporteProductorCompleto(@PathParam("ciclo") Integer ciclo) {
        try {
            CicloAgricola c = buscadorCatalogo.buscaElemento(CicloAgricola.class, ciclo);
            String tipo = "registro-pre-registros-" + cultivoHelper.getCultivo().getClave() + "-" + c.getClave() + "-completo";
            ArchivoRelacionado ar = buscadorArchivoRelacionado.buscaElemento(PreRegistroProductor.class.getSimpleName(),
                    ReferenciaReporteEnum.PRE_REGISTRO.getClave(), tipo);
            return getXlsx(ar.getArchivo());
        } catch (IOException ex) {
            LOGGER.error("No fue posible generar el reporte de pre registros.");
            throw new SegalmexRuntimeException("Error:", "No fue posible generar el reporte de pre registros.");
        }
    }

    @Path("/curp/{curp}/")
    @GET
    @Produces("application/json")
    public PreRegistroProductorConverter getPreRegistroIncripcion(@PathParam("curp") String curp,
            @DefaultValue("2") @QueryParam("expand") int expandLevel, @QueryParam("ciclo") int ciclo) {
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setCurp(curp);
        parametros.setCultivo(cultivoHelper.getCultivo());
        parametros.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, ciclo));
        PreRegistroProductor pre = buscadorPreRegistro.buscaElemento(parametros);
        PreRegistroProductorConverter converter = new PreRegistroProductorConverter(preRegistroHelper.simplificaPreRegistro(pre), expandLevel);
        return converter;
    }

    private Response getXlsx(Archivo archivo) throws IOException {
        byte[] xls = manejadorArchivo.obten(archivo);
        StreamingOutput so = (OutputStream out) -> {
            IOUtils.write(xls, out);
            out.close();
        };
        return Response.ok(so)
                .header("Content-Disposition", "attachment;filename="
                        + archivo.getNombreOriginal())
                .build();
    }
}
