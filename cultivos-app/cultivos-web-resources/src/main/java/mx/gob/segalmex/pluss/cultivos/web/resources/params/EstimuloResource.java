/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.params;

import java.util.List;
import java.util.Objects;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.estimulo.ProcesadorEstimulo;
import mx.gob.segalmex.common.core.estimulo.busqueda.BuscadorEstimulo;
import mx.gob.segalmex.common.core.estimulo.busqueda.ParametrosEstimulo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.pago.EstimuloConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.pago.Estimulo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Path("/estimulo/")
@Component
public class EstimuloResource {

    @Autowired
    private BuscadorEstimulo buscadorEstimulo;

    @Autowired
    private ProcesadorEstimulo procesadorEstimulo;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @GET
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public List<EstimuloConverter> getEstimulos() {
        //Obtenemos una lista de la entidad Estimulo.
        ParametrosEstimulo parametros = new ParametrosEstimulo();
        List<Estimulo> estimulos = buscadorEstimulo.busca(parametros);
        return CollectionConverter.convert(EstimuloConverter.class, estimulos, 2);
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public EstimuloConverter registra(EstimuloConverter converter) {
        Estimulo estimulo = converter.getEntity();
        estimulo.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, estimulo.getCiclo().getId()));
        estimulo.setEstado(buscadorCatalogo.buscaElemento(Estado.class, estimulo.getEstado().getId()));
        procesadorEstimulo.procesa(estimulo);
        return new EstimuloConverter(estimulo, 1);
    }

    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public EstimuloConverter modifica(EstimuloConverter converter) {
        Estimulo estimulo = converter.getEntity();
        if (Objects.isNull(estimulo.getId())) {
            throw new SegalmexRuntimeException("Error al registrar.", "El ID no puede ser nulo.");
        }

        ParametrosEstimulo parametros = new ParametrosEstimulo();
        parametros.setId(estimulo.getId());
        Estimulo actual = buscadorEstimulo.buscaElemento((parametros));
        estimulo.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, estimulo.getCiclo().getId()));
        estimulo.setEstado(buscadorCatalogo.buscaElemento(Estado.class, estimulo.getEstado().getId()));
        procesadorEstimulo.modifica(actual, estimulo);
        return new EstimuloConverter(actual, 1);
    }

    @DELETE
    @Consumes("application/json")
    @Produces("application/json")
    public EstimuloConverter elimina(EstimuloConverter converter) {
        if (Objects.isNull(converter.getId())) {
            throw new SegalmexRuntimeException("Error al registrar.", "El ID no puede ser nulo.");
        }

        ParametrosEstimulo parametros = new ParametrosEstimulo();
        parametros.setId(converter.getId());
        Estimulo estimulo = buscadorEstimulo.buscaElemento(parametros);
        procesadorEstimulo.elimina(estimulo);
        return new EstimuloConverter(estimulo, 1);
    }

}
