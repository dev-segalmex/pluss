/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.productor;

import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;

/**
 * Bean configurable por cada cultivo/grano para indicar que cultivo es el que
 * debemos usar en el sistema. Cada war/grano inyecta la claveCultivo que le
 * corresponde.
 *
 * @author oscar
 */
@Slf4j
public class CultivoHelper {

    private final String claveCultivo;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    public CultivoHelper(String claveCultivo) {
        this.claveCultivo = claveCultivo;
    }

    /**
     * Obtiene el cultivo adecuado.
     *
     * @return el cultivo que debe usarse.
     */
    public Cultivo getCultivo() {
        try {
            LOGGER.info("Obteniendo el cultivo {}", claveCultivo);
            return buscadorCatalogo.buscaElemento(Cultivo.class, claveCultivo);
        } catch (EmptyResultDataAccessException ouch) {
            LOGGER.info("No existe cultivo para {}, se regresa null", claveCultivo);
            return null;
        }
    }
}
