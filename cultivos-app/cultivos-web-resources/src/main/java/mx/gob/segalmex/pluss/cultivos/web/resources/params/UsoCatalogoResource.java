/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.params;

import java.util.List;
import java.util.Objects;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorUsoCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.ParametrosUsoCatalogo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.parametros.ProcesadorUsoCatalogo;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.UsoCatalogoConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.UsoCatalogo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Path("/uso-catalogo/")
@Component
public class UsoCatalogoResource {


    @Autowired
    private BuscadorUsoCatalogo buscadorUsoCatalogo;

    @Autowired
    private ProcesadorUsoCatalogo procesadorUsoCatalogo;

    @GET
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public List<UsoCatalogoConverter> getUsos() {
        //Obtenemos una lista con todos los usos de DB.
        ParametrosUsoCatalogo parametros = new ParametrosUsoCatalogo();
        List<UsoCatalogo> usos = buscadorUsoCatalogo.buscaUsos(parametros);
        return CollectionConverter.convert(UsoCatalogoConverter.class, usos, 2);
    }


    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public UsoCatalogoConverter registra(UsoCatalogoConverter converter) {
        UsoCatalogo uso = converter.getEntity();
        procesadorUsoCatalogo.procesa(uso);
        return new UsoCatalogoConverter(uso, 1);
    }

    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public UsoCatalogoConverter modifica(UsoCatalogoConverter converter) {
        UsoCatalogo uso = converter.getEntity();
        if (Objects.isNull(uso.getId())) {
            throw new SegalmexRuntimeException("Error al registrar.", "El ID no puede ser nulo.");
        }

        ParametrosUsoCatalogo parametros = new ParametrosUsoCatalogo();
        parametros.setId(uso.getId());
        UsoCatalogo actual = buscadorUsoCatalogo.buscaElementoUso(parametros);
        procesadorUsoCatalogo.modifica(actual, uso);
        return new UsoCatalogoConverter(actual, 1);
    }

    @DELETE
    @Consumes("application/json")
    @Produces("application/json")
    public UsoCatalogoConverter elimina(UsoCatalogoConverter converter) {
        if (Objects.isNull(converter.getId())) {
            throw new SegalmexRuntimeException("Error al registrar.", "El ID no puede ser nulo.");
        }

        ParametrosUsoCatalogo parametros = new ParametrosUsoCatalogo();
        parametros.setId(converter.getId());
        UsoCatalogo uso = buscadorUsoCatalogo.buscaElementoUso(parametros);
        procesadorUsoCatalogo.elimina(uso);
        return new UsoCatalogoConverter(uso, 1);
    }

}
