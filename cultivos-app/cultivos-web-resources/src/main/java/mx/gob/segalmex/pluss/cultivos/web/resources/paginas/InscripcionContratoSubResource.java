/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.paginas;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorUsoCatalogo;
import mx.gob.segalmex.common.core.empresas.busqueda.BuscadorSucursal;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.util.CultivoClaveUtil;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.common.web.modelo.inscripcion.CatalogosInscripcionConverter;
import mx.gob.segalmex.granos.core.inscripcion.BuscadorCicloAgricolaActivo;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import mx.gob.segalmex.pluss.cultivos.web.resources.productor.CultivoHelper;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoEmpresa;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoPrecio;
import mx.gob.segalmex.pluss.modelo.contrato.TipoCompradorCobertura;
import mx.gob.segalmex.pluss.modelo.contrato.TipoOperacionCobertura;
import mx.gob.segalmex.pluss.modelo.productor.EntidadCobertura;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author cuecho
 */
@Slf4j
@Component
public class InscripcionContratoSubResource {

    private static final String INSCRIPCION = "contrato";

    private static final String GPO_BASE = ":base";

    private static final String GPO_PRECIO = ":precio";

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private UsuarioSesionContextoHolder contextoSeguridad;

    @Autowired
    private BuscadorUsoCatalogo buscadorUso;

    @Autowired
    private BuscadorCicloAgricolaActivo buscadorCicloAgricola;

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Autowired
    private BuscadorSucursal buscadorSucursal;

    @Autowired
    private CultivoHelper cultivoHelper;

    @GET
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public CatalogosInscripcionConverter getCatalogos() {
        LOGGER.info("carga de catalogo de para el registro contrato.");

        CatalogosInscripcionConverter converter = new CatalogosInscripcionConverter();
        Cultivo c = cultivoHelper.getCultivo();
        Usuario u = contextoSeguridad.getUsuario();
        try {
            converter.setSucursal(getSucursal());
            List<CicloAgricola> ciclosActivos = buscadorCicloAgricola.busca(c, INSCRIPCION, u);
            converter.setCiclos(ciclosActivos);
            converter.setCultivos(buscadorCatalogo.busca(Cultivo.class));
            converter.setEstados(buscadorCatalogo.busca(Estado.class));
            converter.setTipos(buscadorUso.busca(TipoCultivo.class, "cultivo:" + CultivoClaveUtil.getClaveCorta(c)));
            converter.setTiposEmpresaComprador(buscadorUso.busca(TipoEmpresa.class, "tipo-empresa-comprador:" + cultivoHelper.getCultivo().getClave()));
            converter.setTiposEmpresaVendedor(buscadorUso.busca(TipoEmpresa.class, "tipo-empresa-vendedor:" + cultivoHelper.getCultivo().getClave()));
            converter.setTiposPrecio(buscadorUso.busca(TipoPrecio.class, "tipo-precio:" + cultivoHelper.getCultivo().getClave()));
            converter.setCoberturas(buscadorCatalogo.busca(EntidadCobertura.class));
            converter.setTiposComprador(buscadorUso.busca(TipoCompradorCobertura.class, "tipo-comprador:" + cultivoHelper.getCultivo().getClave()));
            converter.setTiposOperacion(buscadorCatalogo.busca(TipoOperacionCobertura.class));
            CicloAgricola seleccionado = buscadorCicloAgricola.getCicloSeleccionado(c, ciclosActivos, u, true);
            if (Objects.nonNull(seleccionado)) {
                LOGGER.info(c.getClave() + ":" + seleccionado.getClave());
                converter.setBases(buscadorParametro.busca(c.getClave() + ":" + seleccionado.getClave() + GPO_BASE));
                converter.setPrecios(buscadorParametro.busca(c.getClave() + ":" + seleccionado.getClave() + GPO_PRECIO));
                converter.setEstadosPredio(buscadorUso.busca(Estado.class,
                        "estado:" + c.getClave() + ":" + seleccionado.getClave()));
                converter.setCicloSeleccionado(seleccionado);
                converter.setRegistroCerrado(getRegistroCerrado(c.getClave()
                        + ":" + seleccionado.getClave() + ":contratos:cierre"));
            }
        } catch (SegalmexRuntimeException ouch) {
            LOGGER.error("El usuario {} no tiene ciclo seleccionado.", u.getClave());
        }
        return converter;
    }

    private Sucursal getSucursal() {
        List<Sucursal> sucursales = buscadorSucursal.busca(contextoSeguridad.getUsuario());
        return sucursales.isEmpty() ? null : sucursales.get(0);
    }

    private Parametro getRegistroCerrado(String clave) {
        Parametro parametro = buscadorParametro.buscaElementoOpcional(clave);
        if (Objects.nonNull(parametro)) {
            Calendar cierre = SegalmexDateUtils
                    .parseCalendar(parametro.getValor(), "yyyy-MM-dd'T'HH:mm:ss");
            if (Calendar.getInstance().after(cierre)) {
                return parametro;
            }
        }
        return null;
    }

}
