/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.empresas;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.archivos.ArchivoUtils;
import mx.gob.segalmex.common.core.archivos.BuscadorArchivoRelacionado;
import mx.gob.segalmex.common.core.archivos.RegistroArchivoRelacionado;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.empresas.ProcesadorRegistroEmpresa;
import mx.gob.segalmex.common.core.empresas.busqueda.BuscadorRegistroEmpresa;
import mx.gob.segalmex.common.core.empresas.busqueda.ParametrosRegistroEmpresa;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.RespuestaConverter;
import mx.gob.segalmex.common.web.modelo.empresas.RegistroEmpresaConverter;
import mx.gob.segalmex.common.web.modelo.seguridad.UsuarioConverter;
import mx.gob.segalmex.common.web.resources.archivos.ArchivoFormUtils;
import mx.gob.segalmex.granos.core.contrato.busqueda.BuscadorDatoCapturado;
import mx.gob.segalmex.granos.core.empresa.xls.VentanillaXls;
import mx.gob.segalmex.granos.web.modelo.contrato.DatoCapturadoConverter;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import mx.gob.segalmex.pluss.modelo.archivos.ArchivoRelacionado;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroEmpresa;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroEmpresaBasico;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroSucursal;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Slf4j
@Path("/empresas/")
@Component("empresaResource")
public class EmpresaResource {

    @Autowired
    private BuscadorRegistroEmpresa buscadorRegistroEmpresa;

    @Autowired
    private ProcesadorRegistroEmpresa procesadorRegistroEmpresa;

    @Autowired
    private RegistroArchivoRelacionado registroArchivoRelacionado;

    @Autowired
    private EmpresaResourceHelper empresaHelper;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorArchivoRelacionado buscadorArchivoRelacionado;

    @Autowired
    private UsuarioSesionContextoHolder contextoSeguridad;

    @Autowired
    private BuscadorDatoCapturado buscadorDatoCapturado;

    @Autowired
    private VentanillaXls ventanillaXls;

    @Autowired
    private EmpresaSubResource empresaSubResource;

    private String url;

    public void setUrl(String url) {
        this.url = url;
    }

    @Path("/{rfc}/registro/{uuid}")
    @GET
    @Produces("application/json")
    public RegistroEmpresaConverter getRegistro(@PathParam("rfc") String rfc,
            @PathParam("uuid") String uuid, @DefaultValue("2") @QueryParam("expand") int expand) {
        RegistroEmpresaBasico registro = buscadorRegistroEmpresa.buscaElementoBasico(rfc, uuid);
        RegistroEmpresaConverter converter = new RegistroEmpresaConverter(empresaHelper.getInstance(registro), expand);

        if (!registro.isEditable()) {
            RegistroEmpresa re = buscadorRegistroEmpresa.buscaElemento(rfc, uuid);
            converter = new RegistroEmpresaConverter(re, expand);
            List<ArchivoRelacionado> relacionados = buscadorArchivoRelacionado.busca(RegistroEmpresa.class, registro.getUuid());
            List<Archivo> archivos = new ArrayList<>();
            for (ArchivoRelacionado ar : relacionados) {
                Archivo archivo = ar.getArchivo();
                archivo.setTipo(ar.getTipo());
                archivos.add(archivo);
            }
            converter.setArchivos(archivos);
            if (re.getEstatus().getClave().equals(EstatusInscripcionEnum.CORRECCION.getClave())) {
                List<DatoCapturado> dts = buscadorDatoCapturado.busca(RegistroEmpresa.class, registro.getUuid(), true);
                converter.setDatos(dts);
            }
        }
        return converter;
    }

    @Path("/{rfc}/registro/")
    @POST
    @Produces("application/json")
    public RegistroEmpresaConverter creaRegistro(@PathParam("rfc") String rfc, RegistroEmpresaConverter registro) {
        RegistroEmpresa entidad = registro.getEntity();
        RegistroEmpresaBasico basico = new RegistroEmpresaBasico();
        empresaHelper.inicializaCatalogosRegistroBasico(basico, entidad);
        basico.setRfc(rfc);
        procesadorRegistroEmpresa.registra(basico, contextoSeguridad.getUsuario());
        registro = new RegistroEmpresaConverter(entidad, 1);
        registro.setUrl(url);

        return registro;
    }

    @Path("/{rfc}/registro/")
    @GET
    @Produces("application/json")
    @Consumes("application/json")
    public RegistroEmpresaConverter getRegistroBasico(@PathParam("rfc") String rfc, @BeanParam EmpresaParam params) {
        ParametrosRegistroEmpresa parametros = empresaHelper.getInstance(params);
        parametros.setRfc(rfc);
        RegistroEmpresaBasico r = buscadorRegistroEmpresa.buscaElementoBasico(parametros);
        RegistroEmpresa entidad = empresaHelper.getInstance(r);
        RegistroEmpresaConverter converter = new RegistroEmpresaConverter(entidad, 2);
        converter.setUrl(url);
        return converter;
    }

    @Path("/{rfc}/registro/{uuid}")
    @PUT
    @Produces("application/json")
    @Consumes("application/json")
    public RegistroEmpresaConverter actualizaRegistro(@PathParam("rfc") String rfc,
            @PathParam("uuid") String uuid, RegistroEmpresaConverter registro) {
        RegistroEmpresaBasico basico = buscadorRegistroEmpresa.buscaElementoBasico(rfc, uuid);
        if (!basico.isEditable()) {
            throw new SegalmexRuntimeException("Error al actualizar el registro.", "El registro no es editable.");
        }

        RegistroEmpresa re = registro.getEntity();
        List<RegistroSucursal> duplicados =  empresaHelper.getDuplicados(re);
        if (!duplicados.isEmpty()){
            List<String> nombresDuplicados = new ArrayList<>();
            for (RegistroSucursal s : duplicados){
                nombresDuplicados.add(s.getNombre() + String.join(",", ""));
            }
            throw new SegalmexRuntimeException("Error al actualizar el registro: estos nombres de sucursales ya existen:", nombresDuplicados);
        }
        re.setRfc(basico.getRfc());
        re.setUuid(basico.getUuid());
        re.setCiclo(basico.getCiclo());
        re.setCultivo(basico.getCultivo());
        empresaHelper.inicializaCatalogos(registro, re);
        procesadorRegistroEmpresa.procesa(basico, re);

        return new RegistroEmpresaConverter(re, 2);
    }

    @Path("/{rfc}/registro/{uuid}/anexos/{archivo}/pdf/")
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces({"application/json;charset=utf-8"})
    public RespuestaConverter registraAnexos(@PathParam("rfc") String rfc,
            @PathParam("uuid") String uuid, @PathParam("archivo") String tipoArchivo, MultipartFormDataInput input) throws IOException {
        RegistroEmpresa re = buscadorRegistroEmpresa.buscaElemento(rfc, uuid);
        re.setEditable(true);
        if (!re.isEditable()) {
            throw new SegalmexRuntimeException("Error al actualizar el registro.", "El registro no es editable.");
        }

        InputStream is = ArchivoFormUtils.getInputStrem(input);
        String nombre = ArchivoFormUtils.getFilename(input, "pdf");
        Archivo archivo = ArchivoUtils.getInstance(nombre, "maiz", "/empresas/" + re.getRfc() + "/"
                + SegalmexDateUtils.format(Calendar.getInstance(), "yyyy-MM-dd") + "/", false);

        ArchivoRelacionado ar = new ArchivoRelacionado();
        ar.setArchivo(archivo);
        ar.setUsuarioRegistra(null);
        ar.setClase(re.getClass().getSimpleName());
        ar.setReferencia(re.getUuid());
        ar.setTipo(tipoArchivo);
        LOGGER.info("UUID Archivos : " + re.getUuid());
        registroArchivoRelacionado.registra(archivo, ar, is);

        RespuestaConverter respuesta = new RespuestaConverter();
        respuesta.setMensaje("Archivo almacenado correctamente: " + tipoArchivo);
        return respuesta;
    }

    @Path("/{uuid}/ventanilla/")
    @GET
    @Produces("application/json")
    public RegistroEmpresaConverter buscaRegistro(@PathParam("uuid") String uuid) {
        ParametrosRegistroEmpresa parametros = new ParametrosRegistroEmpresa();
        parametros.setUuid(uuid);
        RegistroEmpresa registro = buscadorRegistroEmpresa.buscaElemento(parametros);
        RegistroEmpresaConverter converter = new RegistroEmpresaConverter(registro, 3);
        List<ArchivoRelacionado> relacionados = buscadorArchivoRelacionado.busca(RegistroEmpresa.class, registro.getUuid());
        List<Archivo> archivos = new ArrayList<>();
        for (ArchivoRelacionado ar : relacionados) {
            Archivo archivo = ar.getArchivo();
            archivo.setTipo(ar.getTipo());
            archivos.add(archivo);
        }
        converter.setArchivos(archivos);

        List<DatoCapturado> dts = buscadorDatoCapturado.busca(RegistroEmpresa.class, registro.getUuid(), true);
        converter.setDatos(dts);
        return converter;
    }

    @Path("/{uuid}/validacion/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public RegistroEmpresaConverter asignaValidador(@PathParam("uuid") String uuid, UsuarioConverter converter) {
        ParametrosRegistroEmpresa parametros = new ParametrosRegistroEmpresa();
        parametros.setUuid(uuid);
        RegistroEmpresa registro = buscadorRegistroEmpresa.buscaElemento(parametros);
        Usuario validador = buscadorCatalogo.buscaElemento(Usuario.class, converter.getEntity().getId());
        procesadorRegistroEmpresa.asigna(registro, contextoSeguridad.getUsuario(), validador);

        return new RegistroEmpresaConverter(registro, 1);
    }

    @Path("/{uuid}/validacion/positiva/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public RegistroEmpresaConverter validaPositivo(@PathParam("uuid") String uuid, List<DatoCapturadoConverter> datos) {
        ParametrosRegistroEmpresa parametros = new ParametrosRegistroEmpresa();
        parametros.setUuid(uuid);
        RegistroEmpresa registro = buscadorRegistroEmpresa.buscaElemento(parametros);
        List<DatoCapturado> dcs = new ArrayList<>();
        for (DatoCapturadoConverter dc : datos) {
            dcs.add(dc.getEntity());
        }
        procesadorRegistroEmpresa.validaPositivo(registro, contextoSeguridad.getUsuario(), dcs);
        return new RegistroEmpresaConverter(registro, 0);
    }

    @Path("/{uuid}/validacion/negativa/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public RegistroEmpresaConverter validaNegativo(@PathParam("uuid") String uuid, List<DatoCapturadoConverter> datos) {
        ParametrosRegistroEmpresa parametros = new ParametrosRegistroEmpresa();
        parametros.setUuid(uuid);
        RegistroEmpresa registro = buscadorRegistroEmpresa.buscaElemento(parametros);
        List<DatoCapturado> dcs = new ArrayList<>();
        for (DatoCapturadoConverter dc : datos) {
            dcs.add(dc.getEntity());
        }
        procesadorRegistroEmpresa.validaNegativo(registro, contextoSeguridad.getUsuario(), dcs);
        return new RegistroEmpresaConverter(registro, 0);
    }

    @Path("/{uuid}/correccion/")
    @PUT
    @Produces("application/json")
    @Consumes("application/json")
    public RegistroEmpresaConverter corrigeValidacionNegativa(@PathParam("uuid") String uuid, List<DatoCapturadoConverter> datos) {
        LOGGER.info("BUSCANDO REGISTRO CON UUID " + uuid);
        ParametrosRegistroEmpresa parametros = new ParametrosRegistroEmpresa();
        parametros.setUuid(uuid);
        RegistroEmpresa registro = buscadorRegistroEmpresa.buscaElemento(parametros);

        List<DatoCapturado> dcs = new ArrayList<>();
        for (DatoCapturadoConverter dc : datos) {
            dcs.add(dc.getEntity());
        }
        procesadorRegistroEmpresa.corrige(registro, dcs);
        return new RegistroEmpresaConverter(registro, 0);
    }

    @GET
    @Produces("application/json")
    @Consumes("application/json")
    public List<RegistroEmpresaConverter> getVentanillas(@BeanParam EmpresaParam params) {
        ParametrosRegistroEmpresa parametros = empresaHelper.getInstanceSearch(params);
        List<RegistroEmpresa> ventanillas =  buscadorRegistroEmpresa.busca(parametros);
        return CollectionConverter.convert(RegistroEmpresaConverter.class,
                empresaHelper.simplificaResultados(ventanillas), 2);
    }

    @Path("/csv/resultados.xlsx")
    @GET
    @Produces("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    public byte[] indexXls(@BeanParam EmpresaParam params) {
        ParametrosRegistroEmpresa parametros = empresaHelper.getInstanceSearch(params);
        List<RegistroEmpresa> ventanillas =  buscadorRegistroEmpresa.busca(parametros);
        return ventanillaXls.exporta(ventanillas);
    }

    @Path("/administracion-empresa/")
    public EmpresaSubResource getEmpresaSubResource() {
        return empresaSubResource;
    }
}
