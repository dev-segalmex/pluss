/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.inscripcion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.empresas.busqueda.BuscadorSucursal;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.seguridad.busqueda.BuscadorPermiso;
import mx.gob.segalmex.common.core.validador.ValidadorCurpHelper;
import mx.gob.segalmex.common.core.validador.ValidadorPreRegistroContrato;
import mx.gob.segalmex.common.core.validador.ValidadorRfcHelper;
import mx.gob.segalmex.granos.core.accion.util.AccionConsultaHelper;
import mx.gob.segalmex.granos.core.contrato.busqueda.BuscadorContratoProductor;
import mx.gob.segalmex.granos.core.contrato.busqueda.ParametrosContratoProductor;
import mx.gob.segalmex.granos.core.contrato.busqueda.ParametrosInscripcionContrato;
import mx.gob.segalmex.granos.core.inscripcion.BuscadorCicloAgricolaActivo;
import mx.gob.segalmex.granos.core.rfc.busqueda.BuscadorRfcExcepcion;
import mx.gob.segalmex.granos.web.modelo.contrato.ContratoProductorConverter;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import mx.gob.segalmex.pluss.cultivos.web.resources.productor.CultivoHelper;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.Empresa;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoEmpresa;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoPrecio;
import mx.gob.segalmex.pluss.modelo.contrato.CoberturaInscripcionContrato;
import mx.gob.segalmex.pluss.modelo.contrato.TipoCompradorCobertura;
import mx.gob.segalmex.pluss.modelo.contrato.TipoOperacionCobertura;
import mx.gob.segalmex.pluss.modelo.inscripcion.ContratoProductor;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContratoEstado;
import mx.gob.segalmex.pluss.modelo.productor.EntidadCobertura;
import mx.gob.segalmex.pluss.modelo.seguridad.Accion;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Slf4j
@Component("inscripcionContratoHelper")
public class ContratoResourceHelper {

    private static final String CONSULTAR_CONTRATOS = "consultar_contratos";

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private UsuarioSesionContextoHolder contextoSeguridad;

    @Autowired
    private BuscadorPermiso buscadorPermiso;

    @Autowired
    private BuscadorSucursal buscadorSucursal;

    @Autowired
    CultivoHelper cultivoHelper;

    @Autowired
    private AccionConsultaHelper accionHelper;

    @Autowired
    private BuscadorRfcExcepcion buscadorRfcExcepcion;

    @Autowired
    private BuscadorContratoProductor buscadorContratoProductor;

    @Autowired
    private ValidadorPreRegistroContrato validadorPreRegistroContrato;

    @Autowired
    private BuscadorCicloAgricolaActivo buscadorCicloAgricola;

    public void inicializaCatalogos(InscripcionContrato inscripcion) {
        LOGGER.debug("Inicializando catálogos...");
        LOGGER.info("TIPO PRECIO " + inscripcion.getTipoPrecio().getId() + "  <-----");
        if (Objects.isNull(inscripcion.getTipoPrecio())) {
            throw new IllegalArgumentException("El tipo de precio no puede ser nulo.");
        }

        try {
            for (InscripcionContratoEstado e : inscripcion.getEstadosDestino()) {
                e.setEstado(buscadorCatalogo.buscaElemento(Estado.class, e.getEstado().getId()));
                e.setInscripcion(inscripcion);
            }

            inicializaIar(inscripcion, inscripcion.getCoberturas());

            Usuario usuario = contextoSeguridad.getUsuario();
            inscripcion.setSucursal(buscadorSucursal.buscaElemento(usuario));
            inscripcion.setBodega(inscripcion.getSucursal());
            inscripcion.setUsuarioRegistra(usuario);
            inscripcion.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, inscripcion.getCiclo().getId()));
            inscripcion.setCultivo(buscadorCatalogo.buscaElemento(Cultivo.class, inscripcion.getCultivo().getId()));
            inscripcion.setTipoCultivo(buscadorCatalogo.buscaElemento(TipoCultivo.class, inscripcion.getTipoCultivo().getId()));
            inscripcion.setEstado(buscadorCatalogo.buscaElemento(Estado.class, inscripcion.getEstado().getId()));
            inscripcion.setTipoPrecio(buscadorCatalogo.buscaElemento(TipoPrecio.class, inscripcion.getTipoPrecio().getId()));
            inscripcion.setTipoEmpresaComprador(buscadorCatalogo.buscaElemento(TipoEmpresa.class, inscripcion.getTipoEmpresaComprador().getId()));
            inscripcion.setTipoEmpresaVendedor(buscadorCatalogo.buscaElemento(TipoEmpresa.class, inscripcion.getTipoEmpresaVendedor().getId()));
            inscripcion.setFolioSnics(inscripcion.getFolioSnics());
        } catch (EmptyResultDataAccessException e) {
            throw new IllegalArgumentException("Un elemento en los catálogos no es válido.", e);
        }
    }

    public void inicializaIar(InscripcionContrato inscripcion, List<CoberturaInscripcionContrato> coberturas) {
        for (CoberturaInscripcionContrato c : coberturas) {
            c.setEntidadCobertura(buscadorCatalogo.buscaElemento(EntidadCobertura.class,
                    c.getEntidadCobertura().getId()));
            c.setTipoComprador(buscadorCatalogo.buscaElemento(TipoCompradorCobertura.class,
                    c.getTipoComprador().getId()));
            c.setTipoOperacionCobertura(buscadorCatalogo.buscaElemento(TipoOperacionCobertura.class,
                    c.getTipoOperacionCobertura().getId()));
            c.setInscripcionContrato(inscripcion);
        }
    }

    public List<InscripcionContrato> simplificaPropiedades(List<InscripcionContrato> inscripciones) {
        List<InscripcionContrato> simplificados = new ArrayList<>();
        for (InscripcionContrato i : inscripciones) {
            InscripcionContrato simplificado = new InscripcionContrato();
            simplificado.setFolio(i.getFolio());
            simplificado.setUuid(i.getUuid());
            simplificado.setFechaCreacion(i.getFechaCreacion());
            simplificado.setNumeroContrato(i.getNumeroContrato());
            simplificado.setSucursal(new Sucursal());
            simplificado.getSucursal().setEmpresa(new Empresa());
            simplificado.getSucursal().getEmpresa().setRfc(i.getSucursal().getEmpresa().getRfc());
            simplificado.getSucursal().getEmpresa().setNombre(i.getSucursal().getEmpresa().getNombre());
            simplificado.setEstado(new Estado());
            simplificado.getEstado().setNombre(i.getEstado().getNombre());
            simplificado.setEstatus(new EstatusInscripcion());
            simplificado.getEstatus().setNombre(i.getEstatus().getNombre());
            simplificado.setTipoCultivo(new TipoCultivo());
            simplificado.getTipoCultivo().setNombre(i.getTipoCultivo().getNombre());
            if (Objects.nonNull(i.getContrato())) {
                simplificado.setContrato(new Archivo());
                simplificado.getContrato().setSistema(i.getContrato().getSistema());
                simplificado.getContrato().setNombre(i.getContrato().getNombre());
                simplificado.getContrato().setUuid(i.getContrato().getUuid());
            }
            simplificado.setVolumenTotal(i.getVolumenTotal());

            simplificados.add(simplificado);
        }
        return simplificados;
    }

    public ParametrosInscripcionContrato getInstance(InscripcionParam param) {
        ParametrosInscripcionContrato parametros = new ParametrosInscripcionContrato();
        parametros.setEstatus(Objects.nonNull(param.getEstatusInscripcion())
                ? buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                        param.getEstatusInscripcion()) : null);
        parametros.setFechaInicio(Objects.nonNull(param.getFechaInicio())
                ? param.getFechaInicio().getValue() : null);
        parametros.setFolio(Objects.nonNull(param.getFolio()) ? param.getFolio().getValue() : null);
        parametros.setNumeroContrato(param.getNumeroContrato());
        parametros.setRfcEmpresa(param.getRfcEmpresa());
        parametros.setCiclo(Objects.nonNull(param.getCiclo())
                ? buscadorCatalogo.buscaElemento(CicloAgricola.class, param.getCiclo()) : null);
        parametros.setEstado(Objects.nonNull(param.getEstado())
                ? buscadorCatalogo.buscaElemento(Estado.class, param.getEstado()) : null);
        Cultivo cultivo = cultivoHelper.getCultivo();
        parametros.setCultivo(cultivo);
        if (Objects.nonNull(param.getFechaFin())) {
            parametros.setFechaFin(param.getFechaFin().getValue());
            parametros.getFechaFin().add(Calendar.DATE, 1);
        }
        if (Objects.nonNull(param.getValidador())) {
            parametros.setValidador(buscadorCatalogo.buscaElemento(Usuario.class, param.getValidador()));
        }

        Usuario usuario = contextoSeguridad.getUsuario();
        Accion a = accionHelper.getAccion(cultivo, "global", CONSULTAR_CONTRATOS);
        if (buscadorPermiso.isAutorizadaGlobal(a, usuario)) {
            return parametros;
        }

        a = accionHelper.getAccion(cultivo, "empresa", CONSULTAR_CONTRATOS);
        if (buscadorPermiso.isAutorizadaGlobal(a, usuario)) {
            parametros.setRfcEmpresa(buscadorSucursal.buscaElemento(usuario).getEmpresa().getRfc());
            return parametros;
        }

        a = accionHelper.getAccion(cultivo, "sucursal", CONSULTAR_CONTRATOS);
        if (buscadorPermiso.isAutorizadaGlobal(a, usuario)) {
            parametros.setSucursal(buscadorSucursal.buscaElemento(usuario));
            return parametros;
        }

        return parametros;
    }

    public void getValidaEstatus(EstatusInscripcion ei, EstatusInscripcionEnum... estatus) {
        for (EstatusInscripcionEnum e : estatus) {
            if (ei.getClave().equals(e.getClave())) {
                return;
            }
        }
        throw new SegalmexRuntimeException("Error al validar.",
                "El registro se encuentra en estatus: " + ei.getNombre());
    }

    public void validaContratosProductor(List<ContratoProductor> contratosProductor) {
        CicloAgricola ciclo = buscadorCicloAgricola.getCicloSeleccionadoActivo(
                cultivoHelper.getCultivo(), contextoSeguridad.getUsuario());
        for (ContratoProductor cp : contratosProductor) {
            ValidadorCurpHelper.valida(cp.getCurp(), cp.getCurp());
            List<String> excepciones = buscadorRfcExcepcion.getExcepciones(cp.getRfc(), true);
            ValidadorRfcHelper.valida(cp.getRfc(), excepciones, cp.getRfc() + " del contrato");
            validadorPreRegistroContrato.verificaPreRegistro(cp.getCurp(),
                    cultivoHelper.getCultivo(), ciclo);
            cp.setCorrecto(true);
        }
        validaProductorDuplicado(contratosProductor);
        validaVolumenContratado(contratosProductor);
    }

    public void validaProductorDuplicado(List<ContratoProductor> contratosProductores) {
        List<String> curps = new ArrayList<>();
        List<String> rfcs = new ArrayList<>();
        List<String> duplicados = new ArrayList<>();

        for (ContratoProductor cp : contratosProductores) {
            curps.add(cp.getCurp());
            rfcs.add(cp.getRfc());
        }
        duplicados = curps.stream().collect(Collectors.groupingBy(s -> s)).entrySet().stream().filter(e -> e.getValue().size() > 1)
                .map(e -> e.getKey())
                .collect(Collectors.toList());

        if (!duplicados.isEmpty()) {
            throw new SegalmexRuntimeException("Error al registrar:",
                    "Las siguientes CURP se encuentran duplicadas en lo datos de los productores: "
                    + String.join(", ", duplicados));
        }

        duplicados = rfcs.stream().collect(Collectors.groupingBy(s -> s)).entrySet().stream().filter(e -> e.getValue().size() > 1)
                .map(e -> e.getKey())
                .collect(Collectors.toList());
        if (!duplicados.isEmpty()) {
            throw new SegalmexRuntimeException("Error al registrar:",
                    "Los siguientes RFC se encuentran duplicados en lo datos de los productores: "
                    + String.join(", ", duplicados));
        }
    }

    public List<ContratoProductor> getContratosProductor(InscripcionContrato inscripcion) {
        ParametrosContratoProductor parametros = new ParametrosContratoProductor();
        parametros.setInscripcion(inscripcion);
        List<ContratoProductor> contratos = buscadorContratoProductor.busca(parametros);
        List<ContratoProductor> simplificados = new ArrayList<>();
        for (ContratoProductor cp : contratos) {
            ContratoProductor simplificado = new ContratoProductor();
            simplificado.setNombre(cp.getNombre());
            simplificado.setPrimerApellido(cp.getPrimerApellido());
            simplificado.setSegundoApellido(cp.getSegundoApellido());
            simplificado.setRfc(cp.getRfc());
            simplificado.setCurp(cp.getCurp());
            simplificado.setSuperficie(cp.getSuperficie());
            simplificado.setVolumenContrato(cp.getVolumenContrato());
            simplificado.setVolumenIar(cp.getVolumenIar());
            simplificado.setCorrecto(cp.isCorrecto());
            simplificados.add(simplificado);
        }
        return simplificados;
    }

    public List<ContratoProductorConverter> getContratosProductor(ParametrosContratoProductor parametros) {
        List<ContratoProductor> contratosProductor = buscadorContratoProductor.busca(parametros);
        List<ContratoProductorConverter> simplificados = new ArrayList<>();
        for (ContratoProductor cp : contratosProductor) {
            ContratoProductorConverter cpc = new ContratoProductorConverter(cp, 1);
            cpc.setNumeroContrato(cp.getInscripcionContrato().getNumeroContrato());
            cpc.setFolioContrato(cp.getInscripcionContrato().getFolio());
            cpc.setNombreEmpresaContrato(cp.getInscripcionContrato().getSucursal().getEmpresa().getNombre());
            simplificados.add(cpc);
        }
        return simplificados;
    }

    private void validaVolumenContratado(List<ContratoProductor> elementos) {
        for (ContratoProductor cp : elementos) {
            BigDecimal volumenCvv = cp.getVolumenContrato();
            BigDecimal volumenIar = cp.getVolumenIar();
            if (volumenCvv.compareTo(volumenIar) < 0) {
                throw new SegalmexRuntimeException("Error", "El volumen contratado en "
                        + "el CCV no puede ser menor que el volumen cubierto por el "
                        + "I.A.R. [CURP: " + cp.getCurp() + "]");
            }

        }

    }
}
