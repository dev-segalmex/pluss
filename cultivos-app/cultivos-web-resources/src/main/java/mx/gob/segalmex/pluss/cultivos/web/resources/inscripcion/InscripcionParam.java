/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.inscripcion;

import javax.ws.rs.QueryParam;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.cultivos.web.resources.params.CalendarParam;
import mx.gob.segalmex.pluss.cultivos.web.resources.params.FolioParam;

/**
 *
 * @author ismael
 */
@Getter
@Setter
@XmlRootElement(name = "inscripcion-contrato-param")
public class InscripcionParam {

    @QueryParam("folio")
    private FolioParam folio;

    @QueryParam("fechaInicio")
    private CalendarParam fechaInicio;

    @QueryParam("fechaFin")
    private CalendarParam fechaFin;

    @QueryParam("numeroContrato")
    private String numeroContrato;

    @QueryParam("rfcEmpresa")
    private String rfcEmpresa;

    @QueryParam("rfcBodega")
    private String rfcBodega;

    @QueryParam("estatus")
    private String estatusInscripcion;

    @QueryParam("validador")
    private Integer validador;

    @QueryParam("curp")
    private String curp;

    @QueryParam("rfcEmisor")
    private String rfcEmisor;

    @QueryParam("rfcReceptor")
    private String rfcReceptor;

    @QueryParam("rfcProductor")
    private String rfcProductor;

    @QueryParam("tipoFactura")
    private String tipoFactura;

    @QueryParam("nombre")
    private String nombre;

    @QueryParam("papellido")
    private String papellido;

    @QueryParam("sapellido")
    private String sapellido;

    @QueryParam("simplifica")
    private Boolean simplifica;

    @QueryParam("ciclo")
    private Integer ciclo;

    @QueryParam("cultivo")
    private Integer cultivo;

    @QueryParam("clave")
    private String clave;

    @QueryParam("uuidTfd")
    private String uuidTimbreFiscalDigital;

    @QueryParam("tipoRegistro")
    private String tipoRegistro;

    @QueryParam("seguimiento")
    private boolean seguimiento = false;

    @QueryParam("uuidProductor")
    private String uuidProductor;

    @QueryParam("estado")
    private Integer estado;

}
