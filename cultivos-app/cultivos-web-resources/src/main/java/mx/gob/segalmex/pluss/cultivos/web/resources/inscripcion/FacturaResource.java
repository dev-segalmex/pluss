/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.inscripcion;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.BeanParam;
import javax.ws.rs.Consumes;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.archivos.ArchivoUtils;
import mx.gob.segalmex.common.core.archivos.BuscadorArchivoRelacionado;
import mx.gob.segalmex.common.core.archivos.ManejadorArchivo;
import mx.gob.segalmex.common.core.archivos.RegistroArchivoRelacionado;
import mx.gob.segalmex.common.core.archivos.SubDirectorioHelper;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.empresas.busqueda.BuscadorSucursal;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.factura.ProcesadorCfdi;
import mx.gob.segalmex.common.core.factura.ProcesadorInscripcionFactura;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorCfdi;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorComprobanteRecepcion;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorFactura;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorInscripcionFactura;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorInscripcionFacturaHelper;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorUsoFactura;
import mx.gob.segalmex.common.core.factura.busqueda.ParametrosComprobanteRecepcion;
import mx.gob.segalmex.common.core.factura.busqueda.ParametrosInscripcionFactura;
import mx.gob.segalmex.common.core.factura.busqueda.ParametrosUsoFactura;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.common.core.util.CultivoClaveUtil;
import mx.gob.segalmex.common.core.validador.ValidadorClabeHelper;
import mx.gob.segalmex.common.core.validador.ValidadorProductorDeudor;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.RespuestaConverter;
import mx.gob.segalmex.common.web.modelo.factura.CfdiConverter;
import mx.gob.segalmex.common.web.modelo.factura.ComprobanteRecepcionConverter;
import mx.gob.segalmex.common.web.modelo.factura.FacturaConverter;
import mx.gob.segalmex.common.web.modelo.factura.InscripcionFacturaConverter;
import mx.gob.segalmex.common.web.modelo.factura.UsoFacturaConverter;
import mx.gob.segalmex.common.web.modelo.productor.ProductorCicloConverter;
import mx.gob.segalmex.common.web.modelo.seguridad.UsuarioConverter;
import mx.gob.segalmex.common.web.resources.archivos.ArchivoFormUtils;
import mx.gob.segalmex.granos.core.contrato.busqueda.BuscadorDatoCapturado;
import mx.gob.segalmex.granos.core.factura.xls.InscripcionFacturaXls;
import mx.gob.segalmex.granos.core.historico.BuscadorHistoricoRegistro;
import mx.gob.segalmex.granos.core.inscripcion.BuscadorComentarioInscripcion;
import mx.gob.segalmex.granos.core.inscripcion.ProcesadorComentarioInscripcion;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.granos.web.modelo.contrato.DatoCapturadoConverter;
import mx.gob.segalmex.granos.web.modelo.inscripcion.ComentarioInscripcionConverter;
import mx.gob.segalmex.granos.web.modelo.productor.InscripcionAgrupadaConverter;
import mx.gob.segalmex.granos.web.modelo.productor.InscripcionResumenConverter;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import mx.gob.segalmex.pluss.cultivos.modelo.contrato.ValidacionConverter;
import mx.gob.segalmex.pluss.cultivos.web.resources.params.FolioParam;
import mx.gob.segalmex.pluss.cultivos.web.resources.productor.CultivoHelper;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import mx.gob.segalmex.pluss.modelo.archivos.ArchivoRelacionado;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.ReferenciaReporteEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.Region;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.factura.Cfdi;
import mx.gob.segalmex.pluss.modelo.factura.ComprobanteRecepcion;
import mx.gob.segalmex.pluss.modelo.factura.EstatusFacturaEnum;
import mx.gob.segalmex.pluss.modelo.factura.Factura;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import mx.gob.segalmex.pluss.modelo.factura.UsoFactura;
import mx.gob.segalmex.pluss.modelo.historico.HistoricoRegistro;
import mx.gob.segalmex.pluss.modelo.inscripcion.ComentarioInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionAgrupada;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionResumen;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.Productor;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Slf4j
@Path("/facturas")
@Component
public class FacturaResource {

    private static final String RFC_VENTA_PUBLICO = "XAXX010101000";

    private static final String PRODUCTOR = "productor";

    private static final String PRODUCTOR_LIQUIDACION = "productor-liquidacion";

    private static final String GLOBAL = "global";

    private static final String GLOBAL_EMPRESA = "global-empresa";

    @Autowired
    private ProcesadorCfdi procesadorCfdi;

    @Autowired
    private UsuarioSesionContextoHolder contextoSeguridad;

    @Autowired
    private BuscadorCfdi buscadorCfdi;

    @Autowired
    private BuscadorInscripcionFactura buscadorInscripcion;

    @Autowired
    private ProcesadorInscripcionFactura procesadorInscripcion;

    @Autowired
    private FacturaResourceHelper resourceHelper;

    @Autowired
    private BuscadorArchivoRelacionado buscadorArchivoRelacionado;

    @Autowired
    private BuscadorDatoCapturado buscadorDatoCapturado;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private BuscadorHistoricoRegistro buscadorHistorico;

    @Autowired
    private BuscadorComentarioInscripcion buscadorComentarioInscripcion;

    @Autowired
    private ProcesadorComentarioInscripcion procesadorComentarioInscripcion;

    @Autowired
    private RegistroArchivoRelacionado registroArchivoRelacionado;

    @Autowired
    private BuscadorProductor buscadorProductor;

    @Autowired
    private BuscadorFactura buscadorFactura;

    @Autowired
    private InscripcionFacturaXls inscripcionFacturaXls;

    @Autowired
    private BuscadorUsoFactura buscadorUsosFactura;

    @Autowired
    private BuscadorSucursal buscadorSucursal;

    @Autowired
    private CultivoHelper cultivoHelper;

    @Autowired
    private BuscadorComprobanteRecepcion buscadorComprobante;

    @Autowired
    private FacturaRestringidaSubresource facturaRestringidaSubresource;

    @Autowired
    private BuscadorInscripcionFacturaHelper buscadorInscripcionFacturaHelper;

    @Autowired
    private ManejadorArchivo manejadorArchivo;

    @Autowired
    private ConceptoSubResource conceptoSubResource;

    @Autowired
    private ValidadorProductorDeudor validadorProductorDeudor;

    @Path("restringidas")
    public FacturaRestringidaSubresource getFacturaRestringidaSubresource() {
        return facturaRestringidaSubresource;
    }

    @Path("/{uuid}/concepto")
    public ConceptoSubResource getConceptoSubResource() {
        return conceptoSubResource;
    }

    @Path("/cfdi/{tipo}")
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces({"application/json;charset=utf-8"})
    public CfdiConverter registra(@PathParam("tipo") String tipo, @QueryParam("sucursales") boolean sucursales, MultipartFormDataInput input) throws IOException {
        boolean invertir = false;
        Cultivo c = cultivoHelper.getCultivo();
        if (PRODUCTOR_LIQUIDACION.equals(tipo)) {
            invertir = true;
            tipo = PRODUCTOR;
        }
        LOGGER.debug("Filtrar por sucursales: {}", sucursales);
        InputStream is = ArchivoFormUtils.getInputStrem(input);
        Sucursal s = buscadorSucursal.buscaElemento(contextoSeguridad.getUsuario());
        Cfdi cfdi = procesadorCfdi.procesa(is, ArchivoFormUtils.getFilename(input, "xml"),
                contextoSeguridad.getUsuario(), s, invertir, c);
        resourceHelper.validaConceptos(cfdi, c, contextoSeguridad.getUsuario());
        CfdiConverter converter = new CfdiConverter(cfdi, 1);
        converter.setTipo(tipo);
        if (Objects.nonNull(cfdi.getTotalToneladas()) && cfdi.getTotalToneladas().compareTo(BigDecimal.ZERO) > 0) {
            converter.setPrecioTonelada(cfdi.getSubtotal().divide(cfdi.getTotalToneladas(),
                    3, RoundingMode.HALF_UP));
        }
        resourceHelper.determinaTipo(converter, s.getEmpresa());
        resourceHelper.asignaProductor(converter);
        resourceHelper.asignaFacturas(converter, sucursales ? s : null);
        return converter;
    }

    @Path("/maiz/inscripcion/{tipo}/")
    @POST
    @Consumes({"application/json;charset=utf-8"})
    @Produces({"application/json;charset=utf-8"})
    public InscripcionFacturaConverter asigna(@PathParam("tipo") String tipo, InscripcionFacturaConverter converter) throws IOException {
        InscripcionFactura tmp = converter.getEntity();
        if (Objects.nonNull(tmp.getUuid())) {
            return converter;
        }
        Cfdi cfdi = buscadorCfdi.buscaElemento(converter.getEntity().getCfdi().getUuid());
        if (cfdi.getRfcReceptor().equals(RFC_VENTA_PUBLICO)) {
            throw new SegalmexRuntimeException("Error de factura", "El RFC del receptor es de venta al público.");
        }
        resourceHelper.validaFactura(cfdi, tipo, contextoSeguridad.getUsuario());

        Productor productor = null;
        List<Factura> asociadas = new ArrayList<>();
        switch (tipo) {
            case GLOBAL:
                for (String uuid : converter.getFacturasUuid()) {
                    Factura f = buscadorFactura.buscaElemento(uuid);
                    LOGGER.debug("Factura: {} con estatus {}", f.getUuid(), f.getEstatus());
                    if (f.getEstatus().equals(EstatusFacturaEnum.NUEVA.getClave())) {
                        asociadas.add(f);
                    } else {
                        throw new SegalmexRuntimeException("Error de factura",
                                "La factura con UUID " + f.getUuidTimbreFiscalDigital()
                                + " ya se encuentra seleccionada.");
                    }
                }
                break;
            case PRODUCTOR:
                productor = resourceHelper.determinaProductor(converter, cfdi);
                cargaEstados(tmp, tmp.getRegion());
                resourceHelper.validaPredioProductor(productor, tmp.getTipoCultivo(), tmp.getEstado());
                resourceHelper.validaContrato(productor, tmp);
                resourceHelper.validaSociedad(productor, cfdi);
                break;
            case GLOBAL_EMPRESA:
                tmp.setCfdi(cfdi);
                resourceHelper.validaGlobalEmpresa(tmp);
                break;
            default:
                throw new SegalmexRuntimeException("Error de factura", "El tipo de factura no es válido.");
        }

        InscripcionFactura inscripcion = getInstanceFactura(converter, cfdi, productor, tipo);
        procesadorInscripcion.procesa(inscripcion, asociadas);

        return new InscripcionFacturaConverter(inscripcion, 1);
    }

    @Path("/maiz/inscripcion/{uuid}/anexos/{archivo}/pdf/")
    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces({"application/json;charset=utf-8"})
    public RespuestaConverter registraAnexos(@PathParam("uuid") String uuid,
            @PathParam("archivo") String tipoArchivo, MultipartFormDataInput input) throws IOException {
        InscripcionFactura inscripcion = buscadorInscripcion.buscaElemento(uuid);
        InputStream is = ArchivoFormUtils.getInputStrem(input);
        String nombre = ArchivoFormUtils.getFilename(input, "pdf");
        String subidirectorio = SubDirectorioHelper.getSubdirectorio(inscripcion.getCiclo().getClave(),
                "cfdi", inscripcion.getFolio());
        Archivo archivo = ArchivoUtils.getInstance(nombre, CultivoClaveUtil.getClaveCorta(inscripcion.getCultivo()), subidirectorio, false);

        ArchivoRelacionado ar = new ArchivoRelacionado();
        ar.setArchivo(archivo);
        ar.setUsuarioRegistra(contextoSeguridad.getUsuario());
        ar.setClase(inscripcion.getClass().getSimpleName());
        ar.setReferencia(inscripcion.getUuid());
        ar.setTipo(tipoArchivo);

        registroArchivoRelacionado.registra(archivo, ar, is);

        RespuestaConverter respuesta = new RespuestaConverter();
        respuesta.setMensaje("Archivo almacenado correctamente: " + tipoArchivo);
        return respuesta;
    }

    @Path("/maiz/inscripcion/{uuid}/comentario/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionFacturaConverter agregaComentario(@PathParam("uuid") String uuid, ComentarioInscripcionConverter converter) {
        InscripcionFactura inscripcion = buscadorInscripcion.buscaElemento(uuid);
        ComentarioInscripcion ci = converter.getEntity();
        procesadorComentarioInscripcion.procesa(ci, inscripcion, contextoSeguridad.getUsuario());
        return new InscripcionFacturaConverter(inscripcion, 0);
    }

    @Path("/maiz/inscripcion/")
    @GET
    @Produces("application/json")
    public List<InscripcionFacturaConverter> index(@BeanParam InscripcionParam params) {
        ParametrosInscripcionFactura parametros = resourceHelper.getInstance(params);
        List<InscripcionFactura> resultados = buscadorInscripcion.busca(parametros);

        return CollectionConverter.convert(InscripcionFacturaConverter.class,
                resourceHelper.simplificaPropiedades(resultados), 2);

    }

    @Path("/seguimiento/inscripcion/")
    @GET
    @Produces("application/json")
    public List<InscripcionFacturaConverter> seguimiento(@BeanParam InscripcionParam params) {
        ParametrosInscripcionProductor pip = new ParametrosInscripcionProductor();
        pip.setUuidProductor(params.getUuidProductor());
        pip.setCultivo(buscadorCatalogo.buscaElemento(Cultivo.class, params.getCultivo()));
        pip.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, params.getCiclo()));

        ParametrosInscripcionFactura parametros = new ParametrosInscripcionFactura();
        parametros.setProductorCiclo(buscadorProductor.buscaElementoPc(pip));

        List<InscripcionFactura> inscripciones = buscadorInscripcion.busca(parametros);
        return CollectionConverter.convert(InscripcionFacturaConverter.class,
                resourceHelper.simplificaPropiedades(inscripciones), 2);
    }

    @GET
    @Path("/maiz/inscripcion/agrupada")
    @Produces("application/json")
    public List<InscripcionAgrupadaConverter> buscaAgrupada(@BeanParam InscripcionParam params) {
        ParametrosInscripcionFactura parametros = resourceHelper.getInstance(params);
        List<InscripcionAgrupada> resultados = buscadorInscripcionFacturaHelper.busca(parametros);

        return InscripcionAgrupadaConverter.convert(resultados, 2);

    }

    @Path("/publico/inscripcion/")
    @GET
    @Produces("application/json")
    public List<InscripcionFacturaConverter> indexPublico(@QueryParam("uuid") String uuid,
            @QueryParam("cultivo") Integer cultivo, @QueryParam("ciclo") Integer ciclo) {

        if (Objects.isNull(uuid) || Objects.isNull(cultivo) || Objects.isNull(ciclo)) {
            LOGGER.warn("No se puede buscar sin parámetros.");
            throw new IllegalArgumentException("No se puede buscar sin parámetros.");
        }
        ParametrosInscripcionProductor paramProductor = new ParametrosInscripcionProductor();
        paramProductor.setUuid(uuid);
        ParametrosInscripcionFactura parametros = new ParametrosInscripcionFactura();
        parametros.setProductor(buscadorProductor.buscaElemento(paramProductor));
        parametros.setCultivo(buscadorCatalogo.buscaElemento(Cultivo.class, cultivo));
        parametros.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, ciclo));
        List<InscripcionFactura> resultados = buscadorInscripcion.busca(parametros);

        return CollectionConverter.convert(InscripcionFacturaConverter.class,
                resourceHelper.simplificaPropiedades(resultados), 2);

    }

    @Path("/publico/inscripcion/{uuid}")
    @GET
    @Produces("application/json")
    public InscripcionFacturaConverter getInscripcionPublica(@PathParam("uuid") String uuid) {
        return getInscripcion(uuid, 2, false, false, true, false, true, false, true);
    }

    @Path("/publico/inscripcion/usos-factura/")
    @GET
    @Produces("application/json")
    public List<UsoFacturaConverter> getPagosPublico(@BeanParam UsoFacturaParam params) {

        ParametrosInscripcionProductor pip = new ParametrosInscripcionProductor();
        pip.setUuidProductor(params.getUuidProductor());
        pip.setCultivo(buscadorCatalogo.buscaElemento(Cultivo.class, params.getCultivo()));
        pip.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, params.getCiclo()));

        ParametrosUsoFactura parametros = new ParametrosUsoFactura();
        parametros.setProductorCiclo(buscadorProductor.buscaElementoPc(pip));

        List<UsoFactura> usosFactura = buscadorUsosFactura.busca(parametros);
        return CollectionConverter.convert(UsoFacturaConverter.class, resourceHelper.simplificaUsos(usosFactura), 2);
    }

    @Path("/maiz/inscripcion/{uuid}")
    @GET
    @Produces("application/json")
    public InscripcionFacturaConverter getInscripcion(@PathParam("uuid") String uuid,
            @DefaultValue("2") @QueryParam("expand") int expandLevel,
            @QueryParam("archivos") @DefaultValue("false") boolean archivos,
            @QueryParam("datos") @DefaultValue("false") boolean datos,
            @QueryParam("historial") @DefaultValue("false") boolean historial,
            @QueryParam("comentarios") @DefaultValue("false") boolean comentarios,
            @QueryParam("productorCiclo") @DefaultValue("false") boolean productorCiclo,
            @QueryParam("uso_factura") @DefaultValue("false") boolean uso_factura,
            @QueryParam("productor") @DefaultValue("false") boolean productor) {
        InscripcionFactura inscripcion = buscadorInscripcion.buscaElemento(uuid);
        InscripcionFacturaConverter converter = new InscripcionFacturaConverter(inscripcion, expandLevel);

        if (archivos) {
            List<ArchivoRelacionado> relacionados = buscadorArchivoRelacionado.busca(inscripcion.getClass(), inscripcion.getUuid());
            LOGGER.debug("Archivos relacionados: {}", relacionados.size());
            List<Archivo> ars = new ArrayList<>();
            for (ArchivoRelacionado ar : relacionados) {
                Archivo archivo = ar.getArchivo();
                archivo.setTipo(ar.getTipo());
                ars.add(archivo);
                converter.setArchivos(ars);
            }
        }

        if (datos) {
            List<DatoCapturado> dcs = buscadorDatoCapturado.busca(InscripcionFactura.class, uuid, true);
            converter.setDatos(dcs);
        }
        if (historial) {
            List<HistoricoRegistro> hfs = buscadorHistorico.busca(inscripcion);
            converter.setHistorial(hfs);
        }
        if (comentarios) {
            List<ComentarioInscripcion> cis = buscadorComentarioInscripcion.busca(inscripcion);
            converter.setComentarios(cis);
        }
        if (!productorCiclo) {
            converter.setProductorCiclo(new ProductorCicloConverter());
        }
        if (Objects.isNull(inscripcion.getFactura())) {
            try {
                inscripcion.setFactura(buscadorFactura.buscaElementoPorUuidTfd(inscripcion.getCfdi().getUuidTimbreFiscalDigital()));
            } catch (EmptyResultDataAccessException ouch) {
            }
        }
        if (inscripcion.getTipoFactura().equals(PRODUCTOR) && uso_factura) {
            converter.setUsos(resourceHelper.buscaUsoFactura(inscripcion.getFolio()));
        }
        ParametrosComprobanteRecepcion parametrosCom = new ParametrosComprobanteRecepcion();
        parametrosCom.setInscripcionFactura(inscripcion);
        List<ComprobanteRecepcion> cr = buscadorComprobante.busca(parametrosCom);
        converter.setComprobantes(Objects.nonNull(cr) ? CollectionConverter.convert(ComprobanteRecepcionConverter.class, cr, 2) : null);
        resourceHelper.asignaEstatusInscripcionContrato(converter);
        resourceHelper.asignaFacturasRelacionadas(converter, expandLevel);

        return converter;
    }

    @Path("/maiz/inscripcion/{uuid}/validacion/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionFacturaConverter asignaValidador(@PathParam("uuid") String uuid, UsuarioConverter converter) {
        InscripcionFactura inscripcion = buscadorInscripcion.buscaElemento(uuid);
        Usuario validador = buscadorCatalogo.buscaElemento(Usuario.class, converter.getEntity().getId());
        procesadorInscripcion.asigna(inscripcion, contextoSeguridad.getUsuario(), validador);

        return new InscripcionFacturaConverter(inscripcion, 1);
    }

    @Path("/maiz/inscripcion/{uuid}/validacion/positiva/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionFacturaConverter validaPositivo(@PathParam("uuid") String uuid, ValidacionConverter datos) {
        InscripcionFactura inscripcion = buscadorInscripcion.buscaElemento(uuid);
        //validamos el estatus de la factura
        resourceHelper.getValidaEstatus(inscripcion.getEstatus(),
                EstatusInscripcionEnum.VALIDACION, EstatusInscripcionEnum.SOLVENTADA);
        validadorProductorDeudor.valida(resourceHelper.getParametrosDeudor(inscripcion, "rfc"),
                resourceHelper.getInscripcionProductor(inscripcion));
        validadorProductorDeudor.valida(resourceHelper.getParametrosDeudor(inscripcion, "curp"),
                resourceHelper.getInscripcionProductor(inscripcion));
        if (Objects.isNull(inscripcion.getFactura())) {
            try {
                inscripcion.setFactura(buscadorFactura.buscaElementoPorUuidTfd(inscripcion.getCfdi().getUuidTimbreFiscalDigital()));
            } catch (EmptyResultDataAccessException ouch) {
            }
        }
        List<DatoCapturado> dcs = new ArrayList<>();
        for (DatoCapturadoConverter dc : datos.getDatosCapturados()) {
            dcs.add(dc.getEntity());
        }
        if (Objects.nonNull(datos.getFechaPago()) && datos.getFechaPago()) {
            inscripcion.setFechaPagoEstimulo(inscripcion.getFechaPago());
        }
        procesadorInscripcion.validaPositivo(inscripcion, contextoSeguridad.getUsuario(), dcs);

        return new InscripcionFacturaConverter(inscripcion, 1);
    }

    @Path("/maiz/inscripcion/{uuid}/validacion/negativa/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionFacturaConverter validaNegativo(@PathParam("uuid") String uuid, List<DatoCapturadoConverter> datos) {
        InscripcionFactura inscripcion = buscadorInscripcion.buscaElemento(uuid);
        //validamos el estatus de la factura
        resourceHelper.getValidaEstatus(inscripcion.getEstatus(),
                EstatusInscripcionEnum.VALIDACION, EstatusInscripcionEnum.SOLVENTADA);
        List<DatoCapturado> dcs = new ArrayList<>();
        for (DatoCapturadoConverter dc : datos) {
            dcs.add(dc.getEntity());
        }
        procesadorInscripcion.validaNegativo(inscripcion, contextoSeguridad.getUsuario(), dcs);
        return new InscripcionFacturaConverter(inscripcion, 1);
    }

    @Path("/maiz/inscripcion/{uuid}/validacion/negativa/")
    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionFacturaConverter corrigeValidacionNegativa(@PathParam("uuid") String uuid, List<DatoCapturadoConverter> datos) {
        InscripcionFactura inscripcion = buscadorInscripcion.buscaElemento(uuid);
        resourceHelper.getValidaEstatus(inscripcion.getEstatus(),
                EstatusInscripcionEnum.CORRECCION);
        List<DatoCapturado> dcs = new ArrayList<>();
        for (DatoCapturadoConverter dc : datos) {
            dcs.add(dc.getEntity());
        }
        procesadorInscripcion.corrige(inscripcion, contextoSeguridad.getUsuario(), dcs);
        return new InscripcionFacturaConverter(inscripcion, 0);
    }

    @Path("/maiz/inscripcion/{uuid}/cancelacion/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionFacturaConverter cancela(@PathParam("uuid") String uuid, InscripcionFacturaConverter converter) {
        InscripcionFactura inscripcion = buscadorInscripcion.buscaElemento(uuid);
        inscripcion.setComentarioEstatus(converter.getEntity().getComentarioEstatus());
        procesadorInscripcion.cancela(inscripcion, contextoSeguridad.getUsuario());

        return new InscripcionFacturaConverter(inscripcion, 0);
    }

    @Path("/maiz/inscripcion/csv/resultados-factura.xlsx")
    @GET
    @Produces("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    public byte[] indexXls(@BeanParam InscripcionParam params) {
        ParametrosInscripcionFactura parametros = resourceHelper.getInstance(params);
        List<InscripcionFactura> resultados = buscadorInscripcion.busca(parametros);
        return inscripcionFacturaXls.exporta(resultados);
    }

    @Path("/reporte/{ciclo}.xlsx")
    @GET
    @Produces("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    public Response getReporteCompleto(@PathParam("ciclo") Integer ciclo) {
        try {
            CicloAgricola c = buscadorCatalogo.buscaElemento(CicloAgricola.class, ciclo);
            String tipo = "registro-facturas-" + cultivoHelper.getCultivo().getClave() + "-" + c.getClave() + "-completo";
            ArchivoRelacionado ar = buscadorArchivoRelacionado.buscaElemento(InscripcionFactura.class.getSimpleName(),
                    ReferenciaReporteEnum.FACTURA.getClave(), tipo);
            return getXlsx(ar.getArchivo());
        } catch (IOException ex) {
            throw new SegalmexRuntimeException("Error:", "No fue posible generar el reporte de facturas.");
        }
    }

    @Path("/reporte/pago/planeacion/{ciclo}.xlsx")
    @GET
    @Produces("application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
    public Response reportePagoPlaneacion(@PathParam("ciclo") Integer ciclo) {
        try {
            CicloAgricola c = buscadorCatalogo.buscaElemento(CicloAgricola.class, ciclo);
            String tipo = "registro-pagos-planeacion-" + cultivoHelper.getCultivo().getClave() + "-" + c.getClave() + "-completo";
            ArchivoRelacionado ar = buscadorArchivoRelacionado.buscaElemento(InscripcionProductor.class.getSimpleName(),
                    ReferenciaReporteEnum.PAGO_PLANEACION.getClave(), tipo);
            return getXlsx(ar.getArchivo());
        } catch (IOException ex) {
            LOGGER.error("No fue posible generar el reporte de pagos planeación.");
            throw new SegalmexRuntimeException("Error:", "No fue posible generar el reporte de pagos planeación.");
        }
    }

    @Path("/maiz/inscripcion/{uuid}/validacion/reasignacion/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionFacturaConverter cambiaValidador(@PathParam("uuid") String uuid, UsuarioConverter converter) {
        InscripcionFactura inscripcion = buscadorInscripcion.buscaElemento(uuid);
        Usuario validador = buscadorCatalogo.buscaElemento(Usuario.class, converter.getEntity().getId());
        Usuario validadorActual = inscripcion.getUsuarioValidador();
        if (Objects.isNull(validadorActual)) {
            throw new IllegalArgumentException("El usuario validador no ha sido asignado todavía.");
        }
        inscripcion.setUsuarioValidador(validador);
        if (inscripcion.getUsuarioAsignado().getNombre().equals(validadorActual.getNombre())) {
            inscripcion.setUsuarioAsignado(validador);
        }
        registroEntidad.actualiza(inscripcion);
        return new InscripcionFacturaConverter(inscripcion, 1);
    }

    @Path("/maiz/inscripcion/{uuid}/revalidacion/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionFacturaConverter revalida(@PathParam("uuid") String uuid) {
        InscripcionFactura inscripcion = buscadorInscripcion.buscaElemento(uuid);
        procesadorInscripcion.revalida(inscripcion, contextoSeguridad.getUsuario());

        return new InscripcionFacturaConverter(inscripcion, 1);
    }

    @Path("/seguimiento/usos-factura/")
    @GET
    @Produces("application/json")
    public List<UsoFacturaConverter> getSegimientoUsos(@BeanParam UsoFacturaParam params) {
        ParametrosInscripcionProductor pip = new ParametrosInscripcionProductor();
        pip.setUuidProductor(params.getUuidProductor());
        pip.setCultivo(buscadorCatalogo.buscaElemento(Cultivo.class, params.getCultivo()));
        pip.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, params.getCiclo()));

        ParametrosUsoFactura parametros = new ParametrosUsoFactura();
        parametros.setProductorCiclo(buscadorProductor.buscaElementoPc(pip));

        List<UsoFactura> usosFactura = buscadorUsosFactura.busca(parametros);
        return CollectionConverter.convert(UsoFacturaConverter.class, usosFactura, 2);
    }

    @Path("/usos-factura/")
    @GET
    @Produces("application/json")
    public List<UsoFacturaConverter> getUsos(@BeanParam UsoFacturaParam params) {
        ParametrosUsoFactura parametros = resourceHelper.getParametrosUsoFactura(params);

        List<UsoFactura> usosFactura = buscadorUsosFactura.busca(parametros);
        if (params.isSeguimiento()) {
            return CollectionConverter.convert(UsoFacturaConverter.class, usosFactura, 2);
        }

        List<UsoFactura> usos = new ArrayList<>();
        for (UsoFactura u : usosFactura) {
            // Si no se determinó estímulo por tonelada, no se incluye
            if (Objects.isNull(u.getEstimuloTonelada())) {
                continue;
            }
            // Si el estímulo es cero, no se incluye
            if (u.getEstimuloTonelada().compareTo(BigDecimal.ZERO) == 0) {
                continue;
            }
            // Si las toneladas son cero, no se incluye
            if (u.getToneladas().compareTo(BigDecimal.ZERO) == 0) {
                continue;
            }
            usos.add(resourceHelper.simplificaUsoFactura(u));
        }

        return CollectionConverter.convert(UsoFacturaConverter.class, usos, 2);

    }

    @Path("/maiz/uso-factura/{uuid}")
    @GET
    @Produces("application/json")
    public UsoFacturaConverter getUsoFactura(@PathParam("uuid") String uuid,
            @DefaultValue("2") @QueryParam("expand") int expandLevel,
            @QueryParam("archivos") @DefaultValue("false") boolean archivos,
            @QueryParam("datos") @DefaultValue("false") boolean datos,
            @QueryParam("historial") @DefaultValue("false") boolean historial,
            @QueryParam("comentarios") @DefaultValue("false") boolean comentarios,
            @QueryParam("productorCiclo") @DefaultValue("false") boolean productorCiclo) {
        UsoFactura usoFactura = buscadorUsosFactura.buscaElementoUuid(uuid);
        UsoFacturaConverter converter = new UsoFacturaConverter(usoFactura, expandLevel);

        return converter;
    }

    @Path("/empresa/recibidas/")
    @GET
    @Produces("application/json")
    public List<FacturaConverter> getFacturasRecibidas() {
        List<Factura> resultados = buscadorFactura.busca(resourceHelper.getRecibidas());
        return CollectionConverter.convert(FacturaConverter.class,
                resultados, 2);
    }

    @Path("/empresa/emitidas/")
    @GET
    @Produces("application/json")
    public List<FacturaConverter> getFacturasEmitidas() {
        List<Factura> resultados = buscadorFactura.busca(resourceHelper.getEmitidas());
        return CollectionConverter.convert(FacturaConverter.class,
                resultados, 2);
    }

    @Path("/inscripcion/administracion")
    @GET
    @Produces("application/json")
    public List<InscripcionFacturaConverter> buscaPorFolio(@BeanParam InscripcionParam params) {
        Objects.requireNonNull(params.getFolio(), "El folio no puede ser nulo.");
        ParametrosInscripcionFactura parametros = new ParametrosInscripcionFactura();
        parametros.setFolio(params.getFolio().getValue());
        List<InscripcionFactura> resultados = buscadorInscripcion.busca(parametros);
        return CollectionConverter.convert(InscripcionFacturaConverter.class,
                resourceHelper.simplificaPropiedades(resultados), 2);

    }

    @GET
    @Path("/inscripcion/resumen")
    @Produces("application/json")
    public List<InscripcionResumenConverter> buscaResumen() {
        ParametrosInscripcionFactura parametros = new ParametrosInscripcionFactura();
        List<InscripcionResumen> resultados = buscadorInscripcionFacturaHelper.buscaResumen(parametros);
        return InscripcionResumenConverter.convert(resultados, 2);
    }

    @Path("/uso-factura/{folio}")
    @GET
    @Produces("application/json")
    public List<UsoFacturaConverter> getUsosFactura(@PathParam("folio") FolioParam folio) {
        List<UsoFactura> usos = new ArrayList<>();
        try {
            usos = buscadorUsosFactura.buscaElementoFolio(folio.getValue());
        } catch (Exception e) {
        }
        return CollectionConverter.convert(UsoFacturaConverter.class, resourceHelper.simplificaUsosFactura(usos), 2);
    }

    private InscripcionFactura getInstanceFactura(InscripcionFacturaConverter converter,
            Cfdi cfdi, Productor productor, String tipo) {
        InscripcionFactura tmp = converter.getEntity();
        InscripcionFactura inscripcion = new InscripcionFactura();
        inscripcion.setCantidad(converter.getCantidad());
        inscripcion.setCfdi(cfdi);
        inscripcion.setTipoFactura(tipo);
        inscripcion.setUsuarioRegistra(contextoSeguridad.getUsuario());
        inscripcion.setSucursal(buscadorSucursal.buscaElemento(contextoSeguridad.getUsuario()));
        inscripcion.setBodega(inscripcion.getSucursal());
        inscripcion.setCantidad(tmp.getCantidad());
        inscripcion.setContrato(StringUtils.trimToNull(tmp.getContrato()));
        inscripcion.setClaveArchivos(tmp.getClaveArchivos());
        inscripcion.setTipoCultivo(Objects.nonNull(tmp.getTipoCultivo())
                ? buscadorCatalogo.buscaElemento(TipoCultivo.class, tmp.getTipoCultivo().getId()) : null);
        cargaEstados(inscripcion, tmp.getRegion());
        inscripcion.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class,
                tmp.getCiclo().getId()));
        inscripcion.setCultivo(cultivoHelper.getCultivo());
        inscripcion.setProductorCiclo(Objects.nonNull(productor)
                ? buscadorProductor.busca(productor, inscripcion.getCiclo(), inscripcion.getCultivo(), false) : null);
        inscripcion.setFechaPago(tmp.getFechaPago());
        inscripcion.setComprobantes(tmp.getComprobantes());
        inscripcion.setCantidadComprobada(tmp.getCantidadComprobada());
        inscripcion.setNumeroComprobantes(tmp.getNumeroComprobantes());
        inscripcion.setCantidadComprobantePago(Objects.nonNull(tmp.getCantidadComprobantePago())
                ? tmp.getCantidadComprobantePago() : BigDecimal.ZERO);
        resourceHelper.validaComprobantes(inscripcion);
        inscripcion.setPrecioTonelada(BigDecimal.ZERO);
        if (cfdi.getTotalToneladas().compareTo(BigDecimal.ZERO) > 0) {
            inscripcion.setPrecioTonelada(cfdi.getSubtotal().divide(cfdi.getTotalToneladas(), 3, RoundingMode.HALF_UP));
        }
        if (tipo.equals(PRODUCTOR)) {
            inscripcion.setPrecioAjuste(tmp.getPrecioToneladaReal().subtract(inscripcion.getPrecioTonelada()));
            inscripcion.setPrecioToneladaReal(tmp.getPrecioToneladaReal());
        }

        return inscripcion;
    }

    @Path("/uso-factura/duplica/{uuid}")
    @PUT
    @Produces("application/json")
    public UsoFacturaConverter duplica(@PathParam("uuid") String uuid, UsoFacturaConverter uso) {
        UsoFactura base = buscadorUsosFactura.buscaElementoUuid(uuid);
        UsoFactura nuevo = uso.getEntity();
        ValidadorClabeHelper.valida(nuevo.getClabe());
        procesadorInscripcion.duplica(base, nuevo, contextoSeguridad.getUsuario());
        return new UsoFacturaConverter(nuevo, 1);
    }

    private Response getXlsx(Archivo archivo) throws IOException {
        byte[] xls = manejadorArchivo.obten(archivo);
        StreamingOutput so = (OutputStream out) -> {
            IOUtils.write(xls, out);
            out.close();
        };
        return Response.ok(so)
                .header("Content-Disposition", "attachment;filename="
                        + archivo.getNombreOriginal())
                .build();
    }

    private void cargaEstados(InscripcionFactura inscripcion, Region region) {
        Region r = Objects.nonNull(region)
                ? buscadorCatalogo.buscaElemento(Region.class, region.getId()) : null;
        inscripcion.setEstado(Objects.nonNull(r) ? r.getEstado() : null);
        inscripcion.setEstadoEstimulo(Objects.nonNull(r) ? r.getEstadoEstimulo() : null);
    }
}
