package mx.gob.segalmex.pluss.cultivos.web.resources.common;

import java.util.List;
import java.util.Objects;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.LocalidadConverter;
import mx.gob.segalmex.pluss.cultivos.core.BuscadorLocalidad;
import mx.gob.segalmex.pluss.cultivos.core.ParametrosLocalidad;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.Localidad;
import mx.gob.segalmex.pluss.modelo.catalogos.Municipio;
import org.springframework.stereotype.Component;

/**
 * @author ismael
 */
@Slf4j
@Path("/localidades")
@Component
@RequiredArgsConstructor
public class LocalidadResource {

    private final BuscadorLocalidad buscadorLocalidad;

    private final BuscadorCatalogo buscadorCatalogo;

    @GET
    @Produces("application/json")
    public List<LocalidadConverter> index(@QueryParam("estadoId") Integer estadoId,
            @QueryParam("municipioId") Integer municipioId, @QueryParam("codigoPostal") String codigoPostal) {

        ParametrosLocalidad p = new ParametrosLocalidad();
        p.setEstado(Objects.nonNull(estadoId) ? buscadorCatalogo.buscaElemento(Estado.class, estadoId) : null);
        p.setMunicipio(Objects.nonNull(municipioId) ? buscadorCatalogo.buscaElemento(Municipio.class, municipioId) : null);
        p.setCodigoPostal(codigoPostal);

        List<Localidad> localidades = buscadorLocalidad.busca(p);
        return CollectionConverter.convert(LocalidadConverter.class, localidades, 2);
    }
}
