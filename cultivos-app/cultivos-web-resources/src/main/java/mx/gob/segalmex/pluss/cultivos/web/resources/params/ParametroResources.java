/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.params;

import java.util.List;
import java.util.Objects;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.parametros.ProcesadorParametro;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.ParametroConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Path("/parametro/")
@Component
public class ParametroResources {

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Autowired
    private ProcesadorParametro procesadorParametro;

    @GET
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public List<ParametroConverter> getParametros() {
        //Obtenemos una lista con todos los parámetros de DB.
        List<Parametro> parametros = buscadorParametro.busca(Boolean.TRUE);
        return CollectionConverter.convert(ParametroConverter.class, parametros, 2);
    }

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public ParametroConverter registra(ParametroConverter converter) {
        Parametro parametro = converter.getEntity();
        procesadorParametro.procesa(parametro);
        return new ParametroConverter(parametro, 1);
    }

    @Path("/edicion/")
    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public ParametroConverter modifica(ParametroConverter converter) {
        Parametro parametro = converter.getEntity();
        if (Objects.isNull(parametro.getClave())) {
            throw new SegalmexRuntimeException("Error al registrar.", "La clave no puede ser nula.");
        }

        Parametro editado = buscadorParametro.buscaElemento(parametro.getClave());
        editado.setValor(parametro.getValor());
        editado.setGrupo(parametro.getGrupo());
        editado.setDescripcion(parametro.getDescripcion());
        editado.setOrden(parametro.getOrden());
        procesadorParametro.modifica(editado);
        return new ParametroConverter(editado, 1);
    }

    @DELETE
    @Consumes("application/json")
    @Produces("application/json")
    public ParametroConverter elimina(ParametroConverter converter) {
        if (Objects.isNull(converter.getId())) {
            throw new SegalmexRuntimeException("Error al eliminar.", "El ID no puede ser nulo.");
        }
        Parametro parametro = buscadorParametro.buscaElemento(converter.getId());
        procesadorParametro.elimina(parametro);
        return new ParametroConverter(parametro, 1);
    }
}
