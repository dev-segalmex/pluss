/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.empresas;

import javax.ws.rs.QueryParam;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.cultivos.web.resources.params.CalendarParam;

/**
 *
 * @author jurgen
 */
@XmlRootElement(name = "empresa-param")
@Getter
@Setter
public class EmpresaParam {

    @QueryParam("cultivo")
    private Integer cultivo;

    @QueryParam("ciclo")
    private Integer ciclo;

    @QueryParam("id")
    private Integer id;

    @QueryParam("rfc")
    private String rfc;

    @QueryParam("uuid")
    private String uuid;

    @QueryParam("estatus")
    private String estatus;

    @QueryParam("usuario")
    private Integer usuario;

    @QueryParam("fechaInicio")
    private CalendarParam fechaInicio;

    @QueryParam("fechaFin")
    private CalendarParam fechaFin;

}
