/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.productor;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.empresas.busqueda.BuscadorSucursal;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.seguridad.busqueda.BuscadorPermiso;
import mx.gob.segalmex.common.core.validador.ValidadorCurpHelper;
import mx.gob.segalmex.granos.core.accion.util.AccionConsultaHelper;
import mx.gob.segalmex.granos.core.productor.TipoProductorHelper;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorPreRegistroProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInformacionAlerta;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import mx.gob.segalmex.pluss.cultivos.web.resources.inscripcion.InscripcionParam;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricolaEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.Empresa;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.GrupoIndigena;
import mx.gob.segalmex.pluss.modelo.catalogos.Localidad;
import mx.gob.segalmex.pluss.modelo.catalogos.Municipio;
import mx.gob.segalmex.pluss.modelo.catalogos.NivelEstudio;
import mx.gob.segalmex.pluss.modelo.catalogos.Pais;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoDocumento;
import mx.gob.segalmex.pluss.modelo.historico.HistoricoRegistro;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.personas.Persona;
import mx.gob.segalmex.pluss.modelo.personas.Sexo;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersona;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersonaEnum;
import mx.gob.segalmex.pluss.modelo.productor.DatosProductor;
import mx.gob.segalmex.pluss.modelo.productor.Domicilio;
import mx.gob.segalmex.pluss.modelo.productor.EstatusPredioProductorEnum;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionMolino;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PreRegistroProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import mx.gob.segalmex.pluss.modelo.productor.Productor;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;
import mx.gob.segalmex.pluss.modelo.productor.RegimenHidrico;
import mx.gob.segalmex.pluss.modelo.productor.TipoPosesion;
import mx.gob.segalmex.pluss.modelo.seguridad.Accion;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Slf4j
@Component
public class ProductorResourceHelper {

    private static final String CONSULTAR_PRODUCTORES = "consultar_productores";

    private static final String GRUPO_INDIGENA = "ninguno";

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private UsuarioSesionContextoHolder contextoSeguridad;

    @Autowired
    private BuscadorInscripcionProductor buscadorInscripcionProductor;

    @Autowired
    private BuscadorPermiso buscadorPermiso;

    @Autowired
    private BuscadorSucursal buscadorSucursal;

    @Autowired
    private CultivoHelper cultivoHelper;

    @Autowired
    private AccionConsultaHelper accionHelper;

    @Autowired
    private BuscadorPreRegistroProductor buscadorPreRegistro;

    @Autowired
    private TipoProductorHelper tipoProductorHelper;

    public void inicializaCatalogos(InscripcionProductor inscripcion) {
        Usuario usuario = contextoSeguridad.getUsuario();

        inscripcion.getDatosProductor().setSexo(buscadorCatalogo.buscaElemento(Sexo.class, inscripcion.getDatosProductor().getSexo().getClave()));
        inscripcion.setGrupoIndigena(Objects.nonNull(inscripcion.getGrupoIndigena())
                ? buscadorCatalogo.buscaElemento(GrupoIndigena.class, inscripcion.getGrupoIndigena().getId())
                : buscadorCatalogo.buscaElemento(GrupoIndigena.class, GRUPO_INDIGENA));
        inscripcion.setUsuarioRegistra(usuario);
        inscripcion.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, inscripcion.getCiclo().getId()));
        inscripcion.setCultivo(buscadorCatalogo.buscaElemento(Cultivo.class, inscripcion.getCultivo().getId()));
        inscripcion.setSucursal(buscadorSucursal.buscaElemento(contextoSeguridad.getUsuario()));
        inscripcion.setNivelEstudio(buscadorCatalogo.buscaElemento(NivelEstudio.class,
                inscripcion.getNivelEstudio().getId()));
        inscripcion.setBodega(getBodega(inscripcion));
        BigDecimal hectareasTotales = BigDecimal.ZERO;
        BigDecimal toneladasTotales = BigDecimal.ZERO;
        for (PredioProductor p : inscripcion.getPredios()) {
            hectareasTotales = hectareasTotales.add(p.getSuperficie());
            toneladasTotales = toneladasTotales.add(p.getVolumen());
            p.setTipoCultivo(buscadorCatalogo.buscaElemento(TipoCultivo.class, p.getTipoCultivo().getId()));
            p.setTipoPosesion(buscadorCatalogo.buscaElemento(TipoPosesion.class, p.getTipoPosesion().getId()));
            p.setTipoDocumentoPosesion(buscadorCatalogo.buscaElemento(TipoDocumento.class,
                    p.getTipoDocumentoPosesion().getId()));
            p.setRegimenHidrico(buscadorCatalogo.buscaElemento(RegimenHidrico.class, p.getRegimenHidrico().getId()));
            p.setEstado(buscadorCatalogo.buscaElemento(Estado.class, p.getEstado().getId()));
            p.setMunicipio(buscadorCatalogo.buscaElemento(Municipio.class, p.getMunicipio().getId()));
            p.setSolicitado(EstatusPredioProductorEnum.getInstance(p.getSolicitado()).getClave());
        }
        tipoProductorHelper.determinaTipoProductor(inscripcion, hectareasTotales, toneladasTotales);
        if (Objects.nonNull(inscripcion.getMolinos())) {
            for (InscripcionMolino m : inscripcion.getMolinos()) {
                m.setMolino(buscadorCatalogo.buscaElemento(Sucursal.class, m.getMolino().getId()));
            }
        }
        Domicilio domicilio = inscripcion.getDomicilio();
        if (Objects.nonNull(domicilio) && Objects.nonNull(domicilio.getCatalogoLocalidad())) {
            Localidad l = buscadorCatalogo.buscaElemento(Localidad.class, domicilio.getCatalogoLocalidad().getId());
            domicilio.setLocalidad(l.getNombre());
            domicilio.setClaveLocalidad(l.getClave());
            domicilio.setMunicipio(l.getMunicipio());
            domicilio.setEstado(l.getMunicipio().getEstado());
            domicilio.setCodigoPostal(l.getCodigoPostal());
        }
        actualizaDatosProductor(inscripcion);
        inscripcion.setFolioPreRegistro(getFolioPreRegistro(inscripcion));
    }

    private void actualizaDatosProductor(InscripcionProductor inscripcion) {
        String curp = StringUtils.trimToNull(inscripcion.getDatosProductor().getCurp());
        if (Objects.isNull(curp)) {
            return;
        }
        if (curp.length() != 18) {
            return;
        }
        DatosProductor dp = inscripcion.getDatosProductor();
        dp.setFechaNacimiento(ValidadorCurpHelper.getFechaNacimiento(curp));
        dp.setSexo(buscadorCatalogo.buscaElemento(Sexo.class, ValidadorCurpHelper.getSexo(curp)));
    }

    public ParametrosInscripcionProductor getInstance(InscripcionParam param) {
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setEstatus(Objects.nonNull(param.getEstatusInscripcion())
                ? buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                        param.getEstatusInscripcion()) : null);
        parametros.setFechaInicio(Objects.nonNull(param.getFechaInicio())
                ? param.getFechaInicio().getValue() : null);
        parametros.setCiclo(Objects.nonNull(param.getCiclo())
                ? buscadorCatalogo.buscaElemento(CicloAgricola.class, param.getCiclo()) : null);
        parametros.setFolio(Objects.nonNull(param.getFolio()) ? param.getFolio().getValue() : null);
        parametros.setRfcEmpresa(param.getRfcEmpresa());
        parametros.setRfcBodega(param.getRfcBodega());
        parametros.setCurp(param.getCurp());
        parametros.setRfcProductor(param.getRfcProductor());
        parametros.setNombre(param.getNombre());
        parametros.setPapellido(param.getPapellido());
        parametros.setSapellido(param.getSapellido());
        parametros.setTipoRegistro(param.getTipoRegistro());
        parametros.setCultivo(Objects.nonNull(param.getCultivo())
                ? buscadorCatalogo.buscaElemento(Cultivo.class, param.getCultivo()) : cultivoHelper.getCultivo());
        parametros.setEstado(Objects.nonNull(param.getEstado())
                ? buscadorCatalogo.buscaElemento(Estado.class, param.getEstado()) : null);
        if (Objects.nonNull(param.getFechaFin())) {
            parametros.setFechaFin(param.getFechaFin().getValue());
            parametros.getFechaFin().add(Calendar.DATE, 1);
        }
        if (Objects.nonNull(param.getValidador())) {
            parametros.setValidador(buscadorCatalogo.buscaElemento(Usuario.class, param.getValidador()));
        }

        if (param.isSeguimiento()) {
            LOGGER.info("Consultando desde seguimiento productor.");
            return parametros;
        }

        Cultivo cultivo = cultivoHelper.getCultivo();
        LOGGER.info("Consultando desde {}.", cultivo.getNombre());
        Usuario usuario = contextoSeguridad.getUsuario();

        Accion a = accionHelper.getAccion(cultivo, "global", CONSULTAR_PRODUCTORES);
        if (buscadorPermiso.isAutorizadaGlobal(a, usuario)) {
            return parametros;
        }

        a = accionHelper.getAccion(cultivo, "empresa", CONSULTAR_PRODUCTORES);
        if (buscadorPermiso.isAutorizadaGlobal(a, usuario)) {
            parametros.setRfcEmpresa(buscadorSucursal.buscaElemento(usuario).getEmpresa().getRfc());
            return parametros;
        }

        a = accionHelper.getAccion(cultivo, "sucursal", CONSULTAR_PRODUCTORES);
        if (buscadorPermiso.isAutorizadaGlobal(a, usuario)) {
            parametros.setSucursal(buscadorSucursal.buscaElemento(usuario));
            return parametros;
        }
        return parametros;
    }

    public InscripcionProductor simplificaPropieades(InscripcionProductor inscripcion) {
        InscripcionProductor simplificado = new InscripcionProductor();

        simplificado.setFolio(inscripcion.getFolio());
        simplificado.setUuid(inscripcion.getUuid());
        simplificado.setFechaCreacion(inscripcion.getFechaCreacion());
        simplificado.setDatosProductor(new DatosProductor());
        simplificado.getDatosProductor().setCurp(inscripcion.getDatosProductor().getCurp());
        simplificado.getDatosProductor().setRfc(inscripcion.getDatosProductor().getRfc());
        simplificado.getDatosProductor().setNombre(inscripcion.getDatosProductor().getNombre());
        simplificado.getDatosProductor().setPrimerApellido(inscripcion.getDatosProductor().getPrimerApellido());
        simplificado.getDatosProductor().setSegundoApellido(inscripcion.getDatosProductor().getSegundoApellido());
        simplificado.getDatosProductor().setNombreMoral(inscripcion.getDatosProductor().getNombreMoral());
        simplificado.getDatosProductor().setTipoPersona(new TipoPersona());
        simplificado.getDatosProductor().getTipoPersona().setNombre(inscripcion.getDatosProductor().getTipoPersona().getNombre());
        simplificado.getDatosProductor().getTipoPersona().setClave(inscripcion.getDatosProductor().getTipoPersona().getClave());
        simplificado.setEstatus(new EstatusInscripcion());
        simplificado.getEstatus().setNombre(inscripcion.getEstatus().getNombre());
        simplificado.setCiclo(inscripcion.getCiclo());
        simplificado.setCultivo(inscripcion.getCultivo());
        simplificado.setTipoRegistro(inscripcion.getTipoRegistro());
        return simplificado;
    }

    public List<InscripcionProductor> simplificaPropieades(List<InscripcionProductor> inscripciones) {

        List<InscripcionProductor> simplificados = new ArrayList<>();

        for (InscripcionProductor item : inscripciones) {
            simplificados.add(simplificaPropieades(item));
        }

        return simplificados;
    }

    public List<InscripcionProductor> getDuplicados(InscripcionProductor inscripcion) {
        ParametrosInscripcionProductor params = new ParametrosInscripcionProductor();

        if (inscripcion.getDatosProductor().getTipoPersona().getClave()
                .equals(TipoPersonaEnum.FISICA.getClave())) {
            params.setTipoPersona(buscadorCatalogo.buscaElemento(TipoPersona.class,
                    TipoPersonaEnum.FISICA.getClave()));
            params.setCurp(inscripcion.getDatosProductor().getCurp());
        } else {
            params.setRfcProductor(inscripcion.getDatosProductor().getRfc());
        }

        params.setCultivo(inscripcion.getCultivo());
        params.setCiclo(inscripcion.getCiclo());
        params.setNoEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.CANCELADA.getClave()));

        return buscadorInscripcionProductor.busca(params);
    }

    public void getValidaEstatus(EstatusInscripcion ei, EstatusInscripcionEnum... estatus) {
        for (EstatusInscripcionEnum e : estatus) {
            if (ei.getClave().equals(e.getClave())) {
                return;
            }
        }
        throw new SegalmexRuntimeException("Error al realizar la acción.",
                "El registro se encuentra en estatus: " + ei.getNombre());
    }

    public Productor simplificaProductorCiclo(ProductorCiclo pc) {
        Productor simplificado = new Productor();
        simplificado.setCurp(pc.getProductor().getCurp());
        simplificado.setRfc(pc.getProductor().getRfc());
        simplificado.setNombre(pc.getProductor().getNombre());
        simplificado.setClave(pc.getProductor().getClave());
        return simplificado;
    }

    public Productor simplificaProductor(Productor p) {
        Productor simplificado = new Productor();
        simplificado.setCurp(p.getCurp());
        simplificado.setRfc(p.getRfc());
        simplificado.setPersona(new Persona());
        simplificado.getPersona().setNombre(p.getPersona().getNombre());
        simplificado.getPersona().setPrimerApellido(p.getPersona().getPrimerApellido());
        simplificado.getPersona().setSegundoApellido(p.getPersona().getSegundoApellido());
        simplificado.setActivo(p.isActivo());
        return simplificado;
    }

    public List<PredioProductor> validaRendimientoSolicitado(List<PredioProductor> listaPredios, boolean solicitado) {

        for (PredioProductor pre : listaPredios) {
            if (pre.getSolicitado().equals(EstatusPredioProductorEnum.SOLICITADO.getClave())) {
                if (solicitado) {
                    pre.setRendimiento(pre.getRendimientoSolicitado());
                    pre.setVolumen(pre.getVolumenSolicitado());
                    pre.setSolicitado(EstatusPredioProductorEnum.APROBADO.getClave());
                } else {
                    pre.setSolicitado(EstatusPredioProductorEnum.NO_APROBADO.getClave());
                }
            }
        }

        return listaPredios;
    }

    /**
     * Se encarga de asignar la bodega correcta independientemente del cultivo.
     *
     * @param bodega
     * @return la bodega
     */
    private Sucursal getBodega(InscripcionProductor inscripcion) {
        switch (CultivoEnum.getInstance(inscripcion.getCultivo().getClave())) {
            case MAIZ_COMERCIAL:
            case TRIGO:
                return inscripcion.getSucursal();
            case ARROZ:
                if (Objects.isNull(inscripcion.getMolinos()) || inscripcion.getMolinos().isEmpty()) {
                    throw new SegalmexRuntimeException("Error:", "Es necesario al menos una bodega.");
                }
                return buscadorCatalogo.buscaElemento(Sucursal.class,
                        inscripcion.getMolinos().get(0).getMolino().getId());
            default:
                throw new SegalmexRuntimeException("Error:", "Cultivo incorrecto para asignar bodega.");
        }
    }

    public void inicializaEdicion(InscripcionProductor editado, Cultivo cultivo) {
        for (PredioProductor p : editado.getPredios()) {
            p.setTipoCultivo(buscadorCatalogo.buscaElemento(TipoCultivo.class, p.getTipoCultivo().getId()));
            p.setTipoPosesion(buscadorCatalogo.buscaElemento(TipoPosesion.class, p.getTipoPosesion().getId()));
            p.setTipoDocumentoPosesion(buscadorCatalogo.buscaElemento(TipoDocumento.class,
                    p.getTipoDocumentoPosesion().getId()));
            p.setRegimenHidrico(buscadorCatalogo.buscaElemento(RegimenHidrico.class, p.getRegimenHidrico().getId()));
            p.setEstado(buscadorCatalogo.buscaElemento(Estado.class, p.getEstado().getId()));
            p.setMunicipio(buscadorCatalogo.buscaElemento(Municipio.class, p.getMunicipio().getId()));
            p.setSolicitado(EstatusPredioProductorEnum.getInstance(p.getSolicitado()).getClave());
        }
    }

    public ProductorCiclo simplificaProductorCicloSeguimiento(ProductorCiclo pc) {
        ProductorCiclo simplificado = new ProductorCiclo();
        simplificado.setId(pc.getId());
        simplificado.setCiclo(new CicloAgricola());
        simplificado.getCiclo().setNombre(pc.getCiclo().getNombre());
        simplificado.getCiclo().setId(pc.getCiclo().getId());
        simplificado.setProductor(new Productor());
        simplificado.getProductor().setNombre(pc.getProductor().getNombre());
        simplificado.getProductor().setCurp(pc.getProductor().getCurp());
        simplificado.getProductor().setClave(pc.getProductor().getClave());
        simplificado.getProductor().setRfc(pc.getProductor().getRfc());
        simplificado.getProductor().setPersona(new Persona());
        simplificado.getProductor().getPersona().setFechaNacimiento(pc.getProductor().getPersona().getFechaNacimiento());
        simplificado.getProductor().getPersona().setNacionalidad(new Pais());
        simplificado.getProductor().getPersona().getNacionalidad().setNombre(pc.getProductor().getPersona().getNacionalidad().getNombre());
        simplificado.getProductor().setUuid(pc.getProductor().getUuid());
        simplificado.setCultivo(new Cultivo());
        simplificado.getCultivo().setNombre(pc.getCultivo().getNombre());
        simplificado.getCultivo().setId(pc.getCultivo().getId());
        simplificado.setGrupos(pc.getGrupos());
        simplificado.setActivo(pc.isActivo());
        simplificado.setTipoProductor(pc.getTipoProductor());
        return simplificado;
    }

    public void quitaInformacionPrivada(InscripcionProductor ip) {
        Sucursal s = new Sucursal();
        s.setEstado(ip.getSucursal().getEstado());
        s.setMunicipio(ip.getSucursal().getMunicipio());
        s.setEmpresa(new Empresa());
        s.getEmpresa().setNombre(ip.getSucursal().getEmpresa().getNombre());
        s.getEmpresa().setRfc(ip.getSucursal().getEmpresa().getRfc());
        s.setLocalidad(ip.getSucursal().getLocalidad());
        ip.getBodega().setLocalidad(null);
        ip.getBodega().setMunicipio(null);
        ip.getBodega().getEmpresa().setRfc(null);
        ip.setDomicilio(null);
        ip.setCuentaBancaria(null);
        ip.setUsuarioRegistra(null);
        ip.setUsuarioValidador(null);
        ip.setUsuarioAsignado(null);
        ip.setSucursal(s);
    }

    public void quitaInformacionPrivada(List<HistoricoRegistro> historicos) {
        for (HistoricoRegistro h : historicos) {
            h.setUsuarioRegistra(null);
            h.setUsuarioFinaliza(null);
        }
    }

    private String getFolioPreRegistro(InscripcionProductor inscripcion) {
        Cultivo c = inscripcion.getCultivo();
        CicloAgricola ca = inscripcion.getCiclo();
        String curp = inscripcion.getDatosProductor().getCurp();
        try {
            ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
            parametros.setCurp(curp);
            parametros.setCultivo(c);
            parametros.setCiclo(ca);
            PreRegistroProductor pre = buscadorPreRegistro.buscaElemento(parametros);
            return pre.getFolio();
        } catch (EmptyResultDataAccessException ouch) {
            LOGGER.warn("No existe PreRegistroProductor con: {}-{}-{}", curp, c.getClave(), ca.getClave());
        }
        return "--";
    }

    public void validaEditable(InscripcionProductor inscripcion, int contratos) {
        if (contratos > 0) {
            if (inscripcion.getCultivo().getClave().equals(CultivoEnum.ARROZ.getClave())) {
                throw new SegalmexRuntimeException("Error:", "No se puede agregar contratos para Arroz.");
            }

            if (inscripcion.getCiclo().getClave().equals(CicloAgricolaEnum.PV2021.getClave())
                    && inscripcion.getCultivo().getClave().equals(CultivoEnum.TRIGO.getClave())) {
                throw new SegalmexRuntimeException("Error:", "No se puede agregar contratos para Trigo en el ciclo PV-2021.");
            }
        }
    }

    public ParametrosInformacionAlerta getParametrosDeudor(InscripcionProductor ip, String tipo) {
        ParametrosInformacionAlerta parametros =  new ParametrosInformacionAlerta();
        List<String> valores = new ArrayList<>();
        switch (tipo) {
            case "rfc":
                valores.add(ip.getDatosProductor().getRfc());
                break;
            case "curp":
                valores.add(ip.getDatosProductor().getCurp());
                break;
        }
        parametros.setTipo(tipo);
        parametros.setValores(valores);
        return parametros;
    }
}
