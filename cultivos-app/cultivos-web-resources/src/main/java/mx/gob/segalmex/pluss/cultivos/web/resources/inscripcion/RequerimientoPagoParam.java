/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.inscripcion;

import javax.ws.rs.QueryParam;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author jurgen
 */
@Getter
@Setter
@XmlRootElement(name = "requerimiento-pago-param")
public class RequerimientoPagoParam {


    @QueryParam("cegap")
    private String cegap;

    @QueryParam("id")
    private Integer id;

}
