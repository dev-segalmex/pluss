/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.empresas;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.empresas.busqueda.BuscadorRegistroEmpresa;
import mx.gob.segalmex.common.core.empresas.busqueda.ParametrosRegistroEmpresa;
import mx.gob.segalmex.common.web.modelo.empresas.RegistroEmpresaConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.Municipio;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoSucursal;
import mx.gob.segalmex.pluss.modelo.empresas.DatosComercializacion;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroEmpresa;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroEmpresaBasico;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroSucursal;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.productor.Domicilio;
import mx.gob.segalmex.pluss.modelo.productor.TipoPosesion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Component
public class EmpresaResourceHelper {

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorRegistroEmpresa buscadorRegistroEmpresa;

    public void inicializaCatalogos(RegistroEmpresaConverter converter, RegistroEmpresa registro) {
        RegistroEmpresa empresa = converter.getEntity();
        for (RegistroSucursal s : empresa.getSucursales()) {
            Domicilio d = s.getDomicilio();
            d.setEstado(buscadorCatalogo.buscaElemento(Estado.class, d.getEstado().getId()));
            d.setMunicipio(buscadorCatalogo.buscaElemento(Municipio.class, d.getMunicipio().getClave()));
        }
        for (DatosComercializacion dc : empresa.getDatosComercializacion()) {
            dc.setEstado(buscadorCatalogo.buscaElemento(Estado.class, dc.getEstado().getId()));
            dc.setMunicipio(buscadorCatalogo.buscaElemento(Municipio.class, dc.getMunicipio().getClave()));
        }
        registro.getDomicilio().setEstado(buscadorCatalogo.buscaElemento(Estado.class, empresa.getDomicilio().getEstado().getId()));
        registro.getDomicilio().setMunicipio(buscadorCatalogo.buscaElemento(Municipio.class, empresa.getDomicilio().getMunicipio().getClave()));
        registro.setTipoPosesion(buscadorCatalogo.buscaElemento(TipoPosesion.class, empresa.getTipoPosesion().getId()));
        registro.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.NUEVO));
    }

    public RegistroEmpresa getInstance(RegistroEmpresaBasico basico) {
        RegistroEmpresa r = new RegistroEmpresa();
        r.setRfc(basico.getRfc());
        r.setUuid(basico.getUuid());
        r.setCiclo(basico.getCiclo());
        r.setCultivo(basico.getCultivo());
        r.setEditable(basico.isEditable());

        return r;
    }

    public void inicializaCatalogosRegistroBasico(RegistroEmpresaBasico basico, RegistroEmpresa empresa) {
        basico.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, empresa.getCiclo().getId()));
        basico.setCultivo(buscadorCatalogo.buscaElemento(Cultivo.class, empresa.getCultivo().getId()));
    }

    public ParametrosRegistroEmpresa getInstance(EmpresaParam param) {
        ParametrosRegistroEmpresa parametros = new ParametrosRegistroEmpresa();
        parametros.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, param.getCiclo()));
        parametros.setCultivo(buscadorCatalogo.buscaElemento(Cultivo.class, param.getCultivo()));
        return parametros;
    }

    public List<RegistroEmpresa> simplificaResultados(List<RegistroEmpresa> registros) {
        List<RegistroEmpresa> simplificados = new ArrayList<>();
        for (RegistroEmpresa item : registros) {
            RegistroEmpresa r = new RegistroEmpresa();
            r.setId(item.getId());
            r.setRfc(item.getRfc());
            r.setUuid(item.getUuid());
            r.setNombre(item.getNombre());
            r.setNombreResponsable(item.getNombreResponsable());
            r.setCiclo(item.getCiclo());
            r.setCultivo(item.getCultivo());
            r.setEstatus(item.getEstatus());
            r.setUsuarioValidador(item.getUsuarioValidador());
            simplificados.add(r);
        }
        return simplificados;
    }

    public List<RegistroSucursal> getDuplicados(RegistroEmpresa registro) {
        List<RegistroSucursal> duplicados = new ArrayList<>();
        List<String> nombres = new ArrayList<>();
        if (registro.getSucursales().isEmpty()) {
            return duplicados;
        } else {
            for (RegistroSucursal s : registro.getSucursales()) {
                nombres.add(s.getNombre());
            }
            return duplicados = buscadorRegistroEmpresa.busca(nombres);
        }
    }

    public ParametrosRegistroEmpresa getInstanceSearch(EmpresaParam params) {
        ParametrosRegistroEmpresa parametros = new ParametrosRegistroEmpresa();
        parametros.setRfc(Objects.nonNull(params.getRfc()) ? params.getRfc() : null);
        parametros.setFechaCreacion(Objects.nonNull(params.getFechaInicio())
                ? params.getFechaInicio().getValue() : null);
        parametros.setFechaFin(Objects.nonNull(params.getFechaFin())
                ? params.getFechaFin().getValue() : null);
        if (Objects.nonNull(params.getFechaFin())) {
            parametros.setFechaFin(params.getFechaFin().getValue());
            parametros.getFechaFin().add(Calendar.DATE, 1);
        }
        parametros.setEstatus(Objects.nonNull(params.getEstatus()) ? buscadorCatalogo.buscaElemento(EstatusInscripcion.class, params.getEstatus()) : null);
        parametros.setCiclo(Objects.nonNull(params.getCiclo())
                ? buscadorCatalogo.buscaElemento(CicloAgricola.class, params.getCiclo()) : null);
        return parametros;
    }

    public void cargaCatalogosSucursal(Sucursal sucursal) {
        sucursal.setEstado(buscadorCatalogo.buscaElemento(Estado.class, sucursal.getEstado().getId()));
        sucursal.setMunicipio(buscadorCatalogo.buscaElemento(Municipio.class, sucursal.getMunicipio().getId()));
        sucursal.setTipo(buscadorCatalogo.buscaElemento(TipoSucursal.class, sucursal.getTipo().getId()));
    }
}
