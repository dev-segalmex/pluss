/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.paginas;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorUsoCatalogo;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.common.web.modelo.inscripcion.CatalogosInscripcionConverter;
import mx.gob.segalmex.common.web.modelo.meta.ConsultasConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.Municipio;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import mx.gob.segalmex.pluss.modelo.catalogos.Parentesco;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoIar;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoPrecio;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.productor.EntidadCobertura;
import mx.gob.segalmex.pluss.modelo.productor.RegimenHidrico;
import mx.gob.segalmex.pluss.modelo.productor.TipoPosesion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component
public class PreRegistroProductorSubResource {

    private static final String USO_EDO = "estado:";

    //private static final String USO_EDO_ARROZ = "estado:arroz:";
    //private static final String USO_EDO_TRIGO = "estado:trigo:";
    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorUsoCatalogo buscadorUso;

    @Autowired
    private BuscadorParametro buscadorParametro;

    @GET
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public CatalogosInscripcionConverter getCatalogosMaiz(@QueryParam("ciclo") String ciclo,
            @QueryParam("cultivo") String cultivo) {
        CatalogosInscripcionConverter converter = new CatalogosInscripcionConverter();
        converter.setCultivos(buscadorCatalogo.busca(Cultivo.class));
        converter.setCiclos(buscadorCatalogo.busca(CicloAgricola.class));
        String tiposUso = cultivo.contains("maiz") ? "maiz" : cultivo;
        converter.setTipos(buscadorUso.busca(TipoCultivo.class, "cultivo:" + tiposUso));
        converter.setRegimenes(buscadorCatalogo.busca(RegimenHidrico.class));
        converter.setTiposPosesion(buscadorCatalogo.busca(TipoPosesion.class));
        CultivoEnum enumCultivo = cultivo.contains("maiz")
                ? CultivoEnum.MAIZ_COMERCIAL : CultivoEnum.getInstance(cultivo);
        CicloAgricola cicloActivo = Objects.nonNull(ciclo) ? buscadorCatalogo.buscaElemento(CicloAgricola.class, ciclo)
                : buscadorUso.busca(
                        CicloAgricola.class, enumCultivo.getClave() + ":pre-registro:ciclo-activo").get(0);
        converter.setCicloActivo(cicloActivo);
        List<Estado> estados = buscadorUso.busca(Estado.class, USO_EDO
                + enumCultivo.getClave() + ":" + cicloActivo.getClave());
        converter.setEstados(estados);
        converter.setMunicipios(getMunicipios(estados));
        converter.setCoberturas(buscadorCatalogo.busca(EntidadCobertura.class));
        converter.setTiposPrecio(buscadorUso.busca(TipoPrecio.class, "tipo-precio:" + enumCultivo.getClave()));
        converter.setRegistroCerrado(getRegistroCerrado(enumCultivo.getClave()
                + ":" + cicloActivo.getClave() + ":pre-registro:cierre"));
        converter.setParentescos(buscadorCatalogo.busca(Parentesco.class));
        converter.setTiposIar(buscadorCatalogo.busca(TipoIar.class));
        converter.setTiposRequierenIar(buscadorParametro.busca(enumCultivo.getClave()
                + ":tipos-requieren-iar"));
        return converter;
    }

    @Path("/catalogo/")
    @GET
    @Produces("application/json")
    public ConsultasConverter getCatalogos() {
        ConsultasConverter converter = new ConsultasConverter();
        List<EstatusInscripcion> lista = buscadorCatalogo.busca(EstatusInscripcion.class);
        converter.setEstatus(lista);
        converter.setCiclos(buscadorCatalogo.busca(CicloAgricola.class));
        converter.setCultivos(buscadorCatalogo.busca(Cultivo.class));
        return converter;
    }

    /*@Path("/trigo/")
    @GET
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public CatalogosInscripcionConverter getCatalogosTrigo(@QueryParam("inscripcion") @DefaultValue("true") boolean inscripcion,
            @QueryParam("ciclo") String ciclo) {
        CatalogosInscripcionConverter converter = new CatalogosInscripcionConverter();
        converter.setCultivos(inscripcion ? buscadorCatalogo.busca(Cultivo.class) : null);
        converter.setCiclos(buscadorCatalogo.busca(CicloAgricola.class));
        converter.setTipos(buscadorUso.busca(TipoCultivo.class, "cultivo:trigo"));
        converter.setRegimenes(buscadorCatalogo.busca(RegimenHidrico.class));
        converter.setTiposPosesion(buscadorCatalogo.busca(TipoPosesion.class));
        CicloAgricola cicloActivo = Objects.nonNull(ciclo) ? buscadorCatalogo.buscaElemento(CicloAgricola.class, ciclo)
                : buscadorUso.busca(
                        CicloAgricola.class, CultivoEnum.TRIGO.getClave() + ":pre-registro:ciclo-activo").get(0);
        converter.setCicloActivo(cicloActivo);
        List<Estado> estados = buscadorUso.busca(Estado.class, USO_EDO_TRIGO + cicloActivo.getClave());
        converter.setEstados(estados);
        converter.setMunicipios(getMunicipios(estados));
        converter.setCoberturas(buscadorCatalogo.busca(EntidadCobertura.class));
        converter.setTiposPrecio(buscadorUso.busca(TipoPrecio.class, "tipo-precio:" + CultivoEnum.TRIGO.getClave()));
        converter.setRegistroCerrado(getRegistroCerrado("trigo"
                + ":" + cicloActivo.getClave() + ":pre-registro:cierre"));
        converter.setParentescos(buscadorCatalogo.busca(Parentesco.class));
        return converter;
    }

    @Path("/arroz/")
    @GET
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public CatalogosInscripcionConverter getCatalogosArroz(@QueryParam("inscripcion") @DefaultValue("true") boolean inscripcion,
            @QueryParam("ciclo") String ciclo) {
        CatalogosInscripcionConverter converter = new CatalogosInscripcionConverter();
        converter.setCultivos(inscripcion ? buscadorCatalogo.busca(Cultivo.class) : null);
        converter.setCiclos(buscadorCatalogo.busca(CicloAgricola.class));
        converter.setTipos(buscadorUso.busca(TipoCultivo.class, "cultivo:arroz"));
        converter.setRegimenes(buscadorCatalogo.busca(RegimenHidrico.class));
        converter.setTiposPosesion(buscadorCatalogo.busca(TipoPosesion.class));
        CicloAgricola cicloActivo = Objects.nonNull(ciclo) ? buscadorCatalogo.buscaElemento(CicloAgricola.class, ciclo)
                : buscadorUso.busca(
                        CicloAgricola.class, CultivoEnum.ARROZ.getClave() + ":pre-registro:ciclo-activo").get(0);
        converter.setCicloActivo(cicloActivo);
        List<Estado> estados = buscadorUso.busca(Estado.class, Objects.nonNull(ciclo)
                ? USO_EDO_ARROZ + ciclo : USO_EDO_ARROZ + cicloActivo.getClave());
        converter.setEstados(estados);
        converter.setMunicipios(getMunicipios(estados));
        converter.setRegistroCerrado(getRegistroCerrado("arroz"
                + ":" + cicloActivo.getClave() + ":pre-registro:cierre"));
        converter.setParentescos(buscadorCatalogo.busca(Parentesco.class));
        return converter;
    }*/
    private List<Municipio> getMunicipios(List<Estado> estados) {
        List<Municipio> municipios = buscadorCatalogo.busca(Municipio.class);
        List<Municipio> filtrados = new ArrayList<>();
        for (Municipio m : municipios) {
            if (estados.contains(m.getEstado())) {
                filtrados.add(m);
            }
        }
        return filtrados;
    }

    /*private List<Cultivo> getCultivosConsulta() {
        List<Cultivo> cultivos = new ArrayList<>();
        cultivos.add(buscadorCatalogo.buscaElemento(Cultivo.class, CultivoEnum.ARROZ.getClave()));
        cultivos.add(buscadorCatalogo.buscaElemento(Cultivo.class, CultivoEnum.MAIZ_COMERCIAL.getClave()));
        return cultivos;
    }*/
    private Parametro getRegistroCerrado(String clave) {
        Parametro parametro = buscadorParametro.buscaElementoOpcional(clave);
        if (Objects.nonNull(parametro)) {
            Calendar cierre = SegalmexDateUtils
                    .parseCalendar(parametro.getValor(), "yyyy-MM-dd'T'HH:mm:ss");
            if (Calendar.getInstance().after(cierre)) {
                return parametro;
            }
        }
        return null;
    }
}
