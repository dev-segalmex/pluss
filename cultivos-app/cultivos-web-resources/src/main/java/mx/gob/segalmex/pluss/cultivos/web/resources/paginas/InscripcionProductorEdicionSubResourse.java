/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.paginas;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorUsoCatalogo;
import mx.gob.segalmex.common.core.util.CultivoClaveUtil;
import mx.gob.segalmex.common.web.modelo.inscripcion.CatalogosInscripcionConverter;
import mx.gob.segalmex.granos.core.contrato.busqueda.BuscadorInscripcionContrato;
import mx.gob.segalmex.granos.core.contrato.busqueda.ParametrosInscripcionContrato;
import mx.gob.segalmex.granos.core.inscripcion.BuscadorCicloAgricolaActivo;
import mx.gob.segalmex.granos.web.modelo.contrato.InscripcionContratoConverter;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import mx.gob.segalmex.pluss.cultivos.web.resources.productor.CultivoHelper;
import mx.gob.segalmex.pluss.modelo.catalogos.Banco;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.Municipio;
import mx.gob.segalmex.pluss.modelo.catalogos.Pais;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import mx.gob.segalmex.pluss.modelo.catalogos.Parentesco;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoDocumento;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;
import mx.gob.segalmex.pluss.modelo.personas.Sexo;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersona;
import mx.gob.segalmex.pluss.modelo.productor.RegimenHidrico;
import mx.gob.segalmex.pluss.modelo.productor.TipoPosesion;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Slf4j
@Component
public class InscripcionProductorEdicionSubResourse {


    private static final String INSCRIPCION = "productor";

    private static final String GPO_RENDIMIENTO = ":rendimiento";

    private static final String APLICA_CONTRATO = ":aplica-contratos";

    private static final String CONTRATO_REQUERIDO = ":contrato-requerido";

    private static final String PREDIO_SOLICITADO = ":predio-solicitado";

    private static final String ARCHIVOS_PREDIO = ":archivos-predio";

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private UsuarioSesionContextoHolder contextoSeguridad;

    @Autowired
    private BuscadorUsoCatalogo buscadorUso;

    @Autowired
    private BuscadorCicloAgricolaActivo buscadorCicloAgricola;

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Autowired
    private CultivoHelper cultivoHelper;

    @Autowired
    private BuscadorInscripcionContrato buscadorContrato;

    @Path("/{ciclo}/")
    @GET
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public CatalogosInscripcionConverter getCatalogos(@PathParam("ciclo") String ciclo) {
        LOGGER.info("carga de catalogo de para la inscripción contrato.");
        Cultivo c = cultivoHelper.getCultivo();
        Usuario u = contextoSeguridad.getUsuario();
        //insert a uso_catalogo para cada cultivo ej: ciclo-activo:clavecultivo:--:--
        List<CicloAgricola> ciclosActivos = buscadorCicloAgricola.busca(c, INSCRIPCION, u);
        CicloAgricola seleccionado = buscadorCatalogo.buscaElemento(CicloAgricola.class, ciclo);
        CatalogosInscripcionConverter converter = new CatalogosInscripcionConverter();
        converter.setCiclos(ciclosActivos);
        converter.setCultivos(buscadorCatalogo.busca(Cultivo.class));
        converter.setTipos(buscadorUso.busca(TipoCultivo.class, "cultivo:" + CultivoClaveUtil.getClaveCorta(c)));
        converter.setEstados(buscadorCatalogo.busca(Estado.class));
        converter.setTiposPersona(buscadorUso.busca(TipoPersona.class, "fisica-moral"));
        converter.setMunicipios(buscadorCatalogo.busca(Municipio.class));
        converter.setPaises(buscadorCatalogo.busca(Pais.class));
        converter.setTiposDocumentoIdentificacion(buscadorUso.busca(TipoDocumento.class, "documento-identificacion"));
        converter.setRegimenes(buscadorCatalogo.busca(RegimenHidrico.class));
        converter.setTiposPosesion(buscadorCatalogo.busca(TipoPosesion.class));
        converter.setBancos(buscadorCatalogo.busca(Banco.class));
        converter.setTiposDocumentoPropia(buscadorUso.busca(TipoDocumento.class, "documento-posesion-propia"));
        converter.setTiposDocumentoRentada(buscadorUso.busca(TipoDocumento.class, "documento-posesion-rentada"));
        converter.setSexos(buscadorCatalogo.busca(Sexo.class));
        converter.setParentescos(buscadorCatalogo.busca(Parentesco.class));
        converter.setVolumenPredio(buscadorParametro.buscaElemento(c.getClave() + ":" + "maximo-toneladas"));
        converter.setSuperficiePredio(buscadorParametro.buscaElemento(c.getClave() + ":" + "maximo-hectareas"));
        converter.setSuperficieTemporal(buscadorParametro.buscaElementoOpcional(c.getClave() + ":minimo-hectareas-temporal"));
        converter.setCantidadContratada(buscadorParametro.buscaElementoOpcional(c.getClave() + ":" + "maximo-cantidad-contratada"));
        if (Objects.nonNull(seleccionado)) {
            converter.setRendimientos(buscadorParametro.busca(c.getClave()
                    + ":" + seleccionado.getClave() + GPO_RENDIMIENTO));
            //Definir que estados son permitidos para los predios de cada cultivo. Ej:estado:clavecultivo:claveciclo
            converter.setEstadosPredio(buscadorUso.busca(Estado.class,
                    "estado:" + c.getClave() + ":" + seleccionado.getClave()));
            converter.setCicloSeleccionado(seleccionado);
            converter.setAplicaContrato(aplicaParametro(c, seleccionado, APLICA_CONTRATO));
            converter.setContratoRequerido(aplicaParametro(c, seleccionado, CONTRATO_REQUERIDO));
            converter.setPredioSolicitado(aplicaParametro(c, seleccionado, PREDIO_SOLICITADO));
            converter.setArchivosPredio(aplicaParametro(c, seleccionado, ARCHIVOS_PREDIO));
            converter.setArchivosRiego(buscadorParametro.busca(c.getClave() + ":" + seleccionado.getClave() + ":predio:riego-xml"));
            converter.setArchivosSiembra(buscadorParametro.busca(c.getClave() + ":" + seleccionado.getClave() + ":predio:siembra-xml"));
        }
        converter.setContratos(getContratos(seleccionado));

        return converter;
    }

    private boolean aplicaParametro(Cultivo c, CicloAgricola seleccionado, String clave) {
        Parametro p = buscadorParametro.buscaElementoOpcional(c.getClave() + ":" + seleccionado.getClave() + clave);
        return Objects.nonNull(p) && Boolean.valueOf(p.getValor());
    }

    private List<InscripcionContratoConverter> getContratos(CicloAgricola seleccionado) {
        ParametrosInscripcionContrato parametros = new ParametrosInscripcionContrato();
        parametros.setCiclo(seleccionado);
        parametros.setCultivo(cultivoHelper.getCultivo());
        parametros.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.POSITIVA.getClave()));
        List<InscripcionContrato> contratos = buscadorContrato.busca(parametros);
        List<InscripcionContratoConverter> simplificados = new ArrayList<>();
        for (InscripcionContrato c : contratos) {
            InscripcionContrato inc = new InscripcionContrato();
            inc.setNumeroContrato(c.getNumeroContrato());
            inc.setFolio(c.getFolio());
            inc.setEstatus(new EstatusInscripcion());
            inc.getEstatus().setNombre(c.getEstatus().getNombre());
            inc.getEstatus().setClave(c.getEstatus().getClave());
            InscripcionContratoConverter cvt = new InscripcionContratoConverter(inc, 1);
            cvt.setNombreEmpresa(c.getSucursal().getEmpresa().getNombre());
            simplificados.add(cvt);
        }
        return simplificados;
    }
}
