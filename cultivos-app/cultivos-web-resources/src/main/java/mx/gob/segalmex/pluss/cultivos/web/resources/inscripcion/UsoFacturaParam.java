/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.inscripcion;

import javax.ws.rs.QueryParam;
import javax.xml.bind.annotation.XmlRootElement;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author oscar
 */
@Getter
@Setter
@XmlRootElement(name = "uso-factura-param")
public class UsoFacturaParam {

    @QueryParam("folio")
    private String folio;

    @QueryParam("uuid")
    private String uuid;

    @QueryParam("estatusFactura")
    private String estatusFactura;

    @QueryParam("estatus")
    private String estatus;

    @QueryParam("productor")
    private Integer productor;

    @QueryParam("rfcProductor")
    private String rfcProductor;

    @QueryParam("cultivo")
    private Integer cultivo;

    @QueryParam("ciclo")
    private Integer ciclo;

    @QueryParam("seguimiento")
    private boolean seguimiento = false;

    @QueryParam("uuidProductor")
    private String uuidProductor;

}
