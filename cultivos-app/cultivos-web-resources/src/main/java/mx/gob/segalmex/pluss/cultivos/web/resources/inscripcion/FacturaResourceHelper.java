/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.inscripcion;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorUsoCatalogo;
import mx.gob.segalmex.common.core.empresas.busqueda.BuscadorSucursal;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorFactura;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorFacturaRestringida;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorInscripcionFactura;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorUsoFactura;
import mx.gob.segalmex.common.core.factura.busqueda.ParametrosCfdi;
import mx.gob.segalmex.common.core.factura.busqueda.ParametrosInscripcionFactura;
import mx.gob.segalmex.common.core.factura.busqueda.ParametrosUsoFactura;
import mx.gob.segalmex.common.core.seguridad.busqueda.BuscadorPermiso;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.common.web.modelo.factura.CfdiConverter;
import mx.gob.segalmex.common.web.modelo.factura.InscripcionFacturaConverter;
import mx.gob.segalmex.granos.core.accion.util.AccionConsultaHelper;
import mx.gob.segalmex.granos.core.contrato.busqueda.BuscadorInscripcionContrato;
import mx.gob.segalmex.granos.core.contrato.busqueda.ParametrosInscripcionContrato;
import mx.gob.segalmex.granos.core.inscripcion.BuscadorCicloAgricolaActivo;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorAsociado;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorSociedad;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInformacionAlerta;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import mx.gob.segalmex.pluss.cultivos.web.resources.productor.CultivoHelper;
import mx.gob.segalmex.pluss.modelo.catalogos.Banco;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.Empresa;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivoEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoPrecio;
import mx.gob.segalmex.pluss.modelo.factura.Cfdi;
import mx.gob.segalmex.pluss.modelo.factura.ComprobanteRecepcion;
import mx.gob.segalmex.pluss.modelo.factura.ConceptoCfdi;
import mx.gob.segalmex.pluss.modelo.factura.EstatusConceptoCfdiEnum;
import mx.gob.segalmex.pluss.modelo.factura.EstatusFacturaEnum;
import mx.gob.segalmex.pluss.modelo.factura.EstatusUsoFactura;
import mx.gob.segalmex.pluss.modelo.factura.EstatusUsoFacturaEnum;
import mx.gob.segalmex.pluss.modelo.factura.Factura;
import mx.gob.segalmex.pluss.modelo.factura.FacturaRelacionada;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import mx.gob.segalmex.pluss.modelo.factura.UsoFactura;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;
import mx.gob.segalmex.pluss.modelo.productor.Asociado;
import mx.gob.segalmex.pluss.modelo.productor.ContratoFirmado;
import mx.gob.segalmex.pluss.modelo.productor.EmpresaAsociada;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionMolino;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.PredioProductor;
import mx.gob.segalmex.pluss.modelo.productor.Productor;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;
import mx.gob.segalmex.pluss.modelo.seguridad.Accion;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Slf4j
@Component
public class FacturaResourceHelper {

    private static final String GLOBAL = "global";

    private static final String PRODUCTOR = "productor";

    private static final String GLOBAL_EMPRESA = "global-empresa";

    private static final String INSCRIPCION = "--";

    private static final String FECHA_MAX_EMISION = ":fecha-max-emision";

    private static final String FECHA_MIN_EMISION = ":fecha-min-emision";

    private static final String CONSULTAR_FACTURAS = "consultar_facturas";

    private static final String EMPRESAS_TRIGO = ":empresa:recibe-facturas";

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorSociedad buscadorSociedad;

    @Autowired
    private BuscadorAsociado buscadorAsociado;

    @Autowired
    private BuscadorFactura buscadorFactura;

    @Autowired
    private BuscadorInscripcionContrato buscadorContrato;

    @Autowired
    private BuscadorPermiso buscadorPermiso;

    @Autowired
    private UsuarioSesionContextoHolder contextoSeguridad;

    @Autowired
    private BuscadorUsoFactura buscadorUsosFactura;

    @Autowired
    private BuscadorProductor buscadorProductor;

    @Autowired
    private BuscadorInscripcionFactura buscadorInscripcion;

    @Autowired
    private BuscadorSucursal buscadorSucursal;

    @Autowired
    private BuscadorFacturaRestringida buscadorFacturaRestringida;

    @Autowired
    private CultivoHelper cultivoHelper;

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Autowired
    private BuscadorCicloAgricolaActivo buscadorCicloAgricola;

    @Autowired
    private BuscadorInscripcionProductor buscadorInscripcionProductor;

    @Autowired
    private BuscadorUsoCatalogo buscadorUsoCatalogo;

    @Autowired
    private AccionConsultaHelper accionHelper;

    public ParametrosInscripcionFactura getInstance(InscripcionParam param) {
        ParametrosInscripcionFactura parametros = new ParametrosInscripcionFactura();
        Cultivo cultivo = Objects.nonNull(param.getCultivo()) ? buscadorCatalogo.buscaElemento(Cultivo.class, param.getCultivo())
                : cultivoHelper.getCultivo();
        parametros.setCultivo(cultivo);
        parametros.setFechaInicio(Objects.nonNull(param.getFechaInicio())
                ? param.getFechaInicio().getValue() : null);
        if (Objects.nonNull(param.getFechaFin())) {
            parametros.setFechaFin(param.getFechaFin().getValue());
            parametros.getFechaFin().add(Calendar.DATE, 1);
        }
        if (Objects.nonNull(param.getValidador())) {
            parametros.setValidador(buscadorCatalogo.buscaElemento(Usuario.class, param.getValidador()));
        }
        parametros.setCiclo(Objects.nonNull(param.getCiclo())
                ? buscadorCatalogo.buscaElemento(CicloAgricola.class, param.getCiclo()) : null);
        parametros.setFolio(Objects.nonNull(param.getFolio()) ? param.getFolio().getValue() : null);
        parametros.setRfcEmisor(param.getRfcEmisor());
        parametros.setRfcReceptor(param.getRfcReceptor());
        parametros.setEstatus(Objects.nonNull(param.getEstatusInscripcion())
                ? buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                        param.getEstatusInscripcion()) : null);
        parametros.setTipoFactura(param.getTipoFactura());
        parametros.setUuidTimbreFiscalDigital(param.getUuidTimbreFiscalDigital());
        parametros.setEstado(Objects.nonNull(param.getEstado())
                ? buscadorCatalogo.buscaElemento(Estado.class, param.getEstado()) : null);
        if (Objects.nonNull(param.getUuidProductor())) {
            ParametrosInscripcionProductor paramProductor = new ParametrosInscripcionProductor();
            paramProductor.setUuid(param.getUuidProductor());
            parametros.setProductor(buscadorProductor.buscaElemento(paramProductor));
        }

        Usuario usuario = contextoSeguridad.getUsuario();
        Accion a = accionHelper.getAccion(cultivo, "global", CONSULTAR_FACTURAS);
        if (buscadorPermiso.isAutorizadaGlobal(a, usuario)) {
            return parametros;
        }

        a = accionHelper.getAccion(cultivo, "empresa", CONSULTAR_FACTURAS);
        if (buscadorPermiso.isAutorizadaGlobal(a, usuario)) {
            parametros.setRfcEmpresa(buscadorSucursal.buscaElemento(usuario).getEmpresa().getRfc());
            return parametros;
        }

        a = accionHelper.getAccion(cultivo, "sucursal", CONSULTAR_FACTURAS);
        if (buscadorPermiso.isAutorizadaGlobal(a, usuario)) {
            parametros.setSucursal(buscadorSucursal.buscaElemento(usuario));
            return parametros;
        }
        return parametros;
    }

    public List<InscripcionFactura> simplificaPropiedades(List<InscripcionFactura> inscripciones) {
        List<InscripcionFactura> simplificados = new ArrayList<>();

        for (InscripcionFactura item : inscripciones) {
            InscripcionFactura simplificado = new InscripcionFactura();
            simplificado.setCfdi(new Cfdi());
            simplificado.setFolio(item.getFolio());
            simplificado.setFechaCreacion(item.getFechaCreacion());
            simplificado.setEstatus(new EstatusInscripcion());
            simplificado.getEstatus().setNombre(item.getEstatus().getNombre());
            simplificado.getEstatus().setClave(item.getEstatus().getClave());
            simplificado.getCfdi().setNombreEmisor(item.getCfdi().getNombreEmisor());
            simplificado.getCfdi().setRfcEmisor(item.getCfdi().getRfcEmisor());
            simplificado.getCfdi().setNombreReceptor(item.getCfdi().getNombreReceptor());
            simplificado.getCfdi().setRfcReceptor(item.getCfdi().getRfcReceptor());
            simplificado.setUuid(item.getUuid());
            simplificado.setTipoFactura(item.getTipoFactura());
            simplificado.setCultivo(item.getCultivo());
            simplificado.setCiclo(item.getCiclo());
            if (Objects.nonNull(item.getFactura())) {
                simplificado.setFactura(new Factura());
                simplificado.getFactura().setEstatus(item.getFactura().getEstatus());
                simplificado.getFactura().setToneladasTotales(item.getFactura().getToneladasTotales());
            }
            simplificados.add(simplificado);
        }

        return simplificados;
    }

    public List<UsoFactura> simplificaUsos(List<UsoFactura> usos) {
        List<UsoFactura> simplificados = new ArrayList<>();
        for (UsoFactura uso : usos) {
            UsoFactura uf = new UsoFactura();
            uf.setFolio(uso.getFolio());
            uf.setFechaCreacion(uso.getFechaCreacion());
            uf.setEstatus(new EstatusUsoFactura());
            uf.getEstatus().setNombre(uso.getEstatus().getNombre());
            uf.getEstatus().setFactor(uso.getEstatus().getFactor());
            uf.setEstimuloTonelada(uso.getEstimuloTonelada());
            uf.setToneladas(uso.getToneladas());
            uf.setEstimuloTotal(uso.getEstimuloTotal());
            uf.setTipo(uso.getTipo());
            simplificados.add(uf);
        }
        return simplificados;
    }

    public void determinaTipo(CfdiConverter cfdi, Empresa e) {
        CultivoEnum cultivo = CultivoEnum.getInstance(cultivoHelper.getCultivo().getClave());
        switch (cfdi.getTipo()) {
            case PRODUCTOR:
                switch (cultivo) {
                    case MAIZ_COMERCIAL:
                        if (!cfdi.getRfcReceptor().equals(e.getRfc())) {
                            List<String> motivos = new ArrayList<>();
                            motivos.add("La factura NO es de un productor emitida para la empresa que la registra.");
                            motivos.add("La factura fue emitida para: " + cfdi.getRfcReceptor());
                            throw new SegalmexRuntimeException("Error del CFDI.", motivos);
                        }
                        break;
                    case ARROZ:
                        validaReceptorArroz(cfdi);
                        break;
                    case TRIGO:
                        validaReceptorTrigo(cfdi);
                        break;
                }
                break;
            case GLOBAL:
            case GLOBAL_EMPRESA:
                switch (cultivo) {
                    case TRIGO:
                        validaEmisorTrigoGlobal(cfdi);
                        break;
                    default:
                        if (!cfdi.getRfcEmisor().equals(e.getRfc())) {
                            List<String> motivos = new ArrayList<>();
                            motivos.add("La factura NO es una factura global emitida por la empresa que la registra.");
                            motivos.add("La factura fue emitida por: " + cfdi.getRfcEmisor());
                            throw new SegalmexRuntimeException("Error del CFDI.", motivos);
                        }
                        break;
                }
                break;
            default:
                throw new SegalmexRuntimeException("Error del CFDI", "El CFDI es de un tipo desconocido: " + cfdi.getTipo());
        }
    }

    public void asignaProductor(CfdiConverter cfdi) {
        switch (cfdi.getTipo()) {
            case GLOBAL:
            case GLOBAL_EMPRESA:
                return;
            case PRODUCTOR:
                break;
            default:
                throw new SegalmexRuntimeException("Error del CFDI", "El CFDI es de un tipo desconocido: " + cfdi.getTipo());
        }

        ProductorCiclo pc = null;
        List<CicloAgricola> ciclosActivos = buscadorCicloAgricola.busca(cultivoHelper.getCultivo(), INSCRIPCION, contextoSeguridad.getUsuario());
        CicloAgricola ciclo = buscadorCicloAgricola.getCicloSeleccionado(cultivoHelper.getCultivo(), ciclosActivos, contextoSeguridad.getUsuario(), true);
        Cultivo cultivo = cultivoHelper.getCultivo();
        try {
            Productor p = buscadorCatalogo.buscaElemento(Productor.class, cfdi.getRfcEmisor());
            pc = buscadorProductor.busca(p, ciclo, cultivo, false);
            cfdi.setProductor(pc.getProductor().getRfc());
        } catch (EmptyResultDataAccessException ouch) {
            LOGGER.debug("La factura no pertenece a un productor del ciclo especificado.");
        }

        switch (cfdi.getRfcEmisor().length()) {
            case 12: // Factura de productor persona moral
                List<Asociado> asociados = new ArrayList<>();
                for (Asociado a : buscadorSociedad.busca(cfdi.getRfcEmisor(), cultivo, ciclo)) {
                    if (!a.getRelacion().equals("socio")) {
                        asociados.add(a);
                    }
                }
                cfdi.setAsociados(asociados);
                break;
            case 13: // Factura de productor persona física.
                if (Objects.isNull(pc)) {
                    throw new SegalmexRuntimeException("Error de productor.",
                            "El RFC del productor que emitió la factura es incorrecto, verifique que el productor se encuentre en estatus positivo.");
                }
                break;
        }
    }

    public Productor determinaProductor(InscripcionFacturaConverter converter, Cfdi cfdi) {
        InscripcionFactura i = converter.getEntity();
        if (Objects.nonNull(i.getProductorCiclo())) {
            return buscadorCatalogo.buscaElemento(Productor.class, i.getProductorCiclo().getProductor().getRfc());
        }

        Asociado a = converter.getAsociado().getEntity();
        if (Objects.nonNull(a)) {
            a = buscadorAsociado.buscaElemento(a.getId());
        }

        // Buscamos primero un productor
        switch (a.getRelacion()) {
            case "productor": // Usando facturas de persona moral
                return buscadorCatalogo.buscaElemento(Productor.class, a.getRfc());
            case "representante":
                return buscadorCatalogo.buscaElemento(Productor.class, a.getSociedad().getClave());
        }
        return null;
    }

    public void asignaFacturas(CfdiConverter cfdi, Sucursal s) {
        ParametrosCfdi parametros = new ParametrosCfdi();
        switch (cfdi.getTipo()) {
            case PRODUCTOR:
            case GLOBAL_EMPRESA:
                return;
            case GLOBAL:
                parametros.setTipo("productor");
                break;
            default:
                throw new IllegalArgumentException("Error en el tipo de CFDI: " + cfdi.getTipo());
        }
        parametros.setEstatus(EstatusFacturaEnum.NUEVA.getClave());
        parametros.setRfcReceptor(cfdi.getEntity().getRfcEmisor());
        parametros.setTipo(PRODUCTOR);
        parametros.setSucursal(s);
        parametros.setCultivo(cultivoHelper.getCultivo());
        parametros.setCiclo(buscadorCicloAgricola.getCicloSeleccionadoActivo(cultivoHelper
                .getCultivo(), contextoSeguridad.getUsuario()));
        List<Factura> facturas = buscadorFactura.busca(parametros);
        Collections.sort(facturas, (o1, o2) -> {
            return o1.getRfcEmisor().compareTo(o2.getRfcEmisor());
        });

        cfdi.setFacturas(facturas);
    }

    public void asignaFacturasRelacionadas(InscripcionFacturaConverter converter, int expand) {
        InscripcionFactura inscripcion = converter.getEntity();
        if (!inscripcion.getTipoFactura().equals(GLOBAL)) {
            return;
        }

        List<Factura> facturas = new ArrayList<>();
        for (FacturaRelacionada rf : buscadorFactura.busca(converter.getEntity())) {
            facturas.add(rf.getFacturaProductor());
        }
        converter.setRelacionadas(facturas);
    }

    public void asignaEstatusInscripcionContrato(InscripcionFacturaConverter ifc) {
        InscripcionFactura i = ifc.getEntity();
        if (StringUtils.isEmpty(i.getContrato())) {
            return;
        }

        ParametrosInscripcionContrato parametros = new ParametrosInscripcionContrato();
        parametros.setNoEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.CANCELADA.getClave()));
        parametros.setNumeroContrato(i.getContrato());
        List<InscripcionContrato> contratos = buscadorContrato.busca(parametros);
        for (InscripcionContrato ic : contratos) {
            ifc.setEstatusInscripcionContrato(ic.getEstatus());
            if (ic.getEstatus().getClave().equals(EstatusInscripcionEnum.POSITIVA.getClave())) {
                return;
            }
        }
    }

    public List<UsoFactura> buscaUsoFactura(String folio) {
        try {
            return buscadorUsosFactura.buscaElementoFolio(folio);

        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    public ParametrosCfdi getEmitidas() {
        ParametrosCfdi parametros = new ParametrosCfdi();
        Usuario usuario = contextoSeguridad.getUsuario();
        CicloAgricola ciclo = buscadorCicloAgricola.getCicloSeleccionadoActivo(cultivoHelper.getCultivo(), usuario);
        parametros.setRfcEmisor(buscadorSucursal.buscaElemento(usuario).getEmpresa().getRfc());
        parametros.setCultivo(cultivoHelper.getCultivo());
        parametros.setCiclo(ciclo);
        // parametros.setTipoFActura("global");
        return parametros;
    }

    public ParametrosCfdi getRecibidas() {
        ParametrosCfdi parametros = new ParametrosCfdi();
        Usuario usuario = contextoSeguridad.getUsuario();
        CicloAgricola ciclo = buscadorCicloAgricola.getCicloSeleccionadoActivo(cultivoHelper.getCultivo(), usuario);
        parametros.setRfcReceptor(buscadorSucursal.buscaElemento(usuario).getEmpresa().getRfc());
        parametros.setCultivo(cultivoHelper.getCultivo());
        parametros.setCiclo(ciclo);
        //parametros.setTipoFActura("global");
        return parametros;
    }

    public void validaGlobalEmpresa(InscripcionFactura inscripcion) {
        ParametrosInscripcionFactura parametros = new ParametrosInscripcionFactura();
        parametros.setNoEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                EstatusInscripcionEnum.CANCELADA.getClave()));
        parametros.setUuidTimbreFiscalDigital(inscripcion.getCfdi().getUuidTimbreFiscalDigital());
        LOGGER.debug("UUID de factura: {}", inscripcion.getCfdi().getUuidTimbreFiscalDigital());
        String[] tipos = new String[]{"global", "global-empresa"};
        for (String tipo : tipos) {
            parametros.setTipoFactura(tipo);
            List<InscripcionFactura> resultados = buscadorInscripcion.busca(parametros);
            if (resultados.size() > 0) {
                throw new SegalmexRuntimeException("Error de factura",
                        "La factura ya ha sido registrada.");
            }
        }
    }

    public void validafechaMaxima(Cultivo c, CicloAgricola ciclo, Calendar fechaTimbrado) {
        Calendar fecha = null;
        try {
            Parametro limite = buscadorParametro.buscaElemento(c.getClave() + ":"
                    + ciclo.getClave() + FECHA_MAX_EMISION);
            fecha = SegalmexDateUtils.parseCalendar(limite.getValor(), "yyyy-MM-dd");
        } catch (EmptyResultDataAccessException ouch) {
            LOGGER.warn("No existe fecha de timbrado máxima para {}.", c.getClave());
            fecha = Calendar.getInstance();
            fecha.add(Calendar.DATE, 1);
        }

        Calendar fechaLimite = DateUtils.truncate(fecha, Calendar.DATE);
        if (fechaTimbrado.compareTo(fechaLimite) < 0) {
            return;
        }

        fechaLimite.add(Calendar.DAY_OF_YEAR, -1);
        String fechaError = SegalmexDateUtils.format(fechaLimite, "yyyy-MM-dd");
        throw new SegalmexRuntimeException("Error de factura",
                "No se pueden registrar facturas con fecha superior a " + fechaError);
    }

    public void validafechaMinima(Cultivo c, CicloAgricola ciclo, Calendar fechaTimbrado) {
        Calendar fecha = null;
        try {
            Parametro limite = buscadorParametro.buscaElemento(c.getClave() + ":"
                    + ciclo.getClave() + FECHA_MIN_EMISION);
            fecha = SegalmexDateUtils.parseCalendar(limite.getValor(), "yyyy-MM-dd");
        } catch (EmptyResultDataAccessException ouch) {
            LOGGER.warn("No existe fecha de timbrado mínima para {}.", c.getClave());
            return;
        }

        Calendar fechaLimite = DateUtils.truncate(fecha, Calendar.DATE);
        if (fechaTimbrado.compareTo(fechaLimite) > 0) {
            return;
        }

        fechaLimite.add(Calendar.DAY_OF_YEAR, 1);
        String fechaError = SegalmexDateUtils.format(fechaLimite, "yyyy-MM-dd");
        throw new SegalmexRuntimeException("Error de factura",
                "No se pueden registrar facturas con fecha menor a " + fechaError);
    }

    public void getValidaEstatus(EstatusInscripcion ei, EstatusInscripcionEnum... estatus) {
        for (EstatusInscripcionEnum e : estatus) {
            if (ei.getClave().equals(e.getClave())) {
                return;
            }
        }
        throw new SegalmexRuntimeException("Error:",
                "El registro se encuentra en un estatus incorrecto para realizar esta acción.");
    }

    public void validaFactura(Cfdi cfdi, String tipo, Usuario usuario) {
        Cultivo c = cultivoHelper.getCultivo();
        List<CicloAgricola> ciclosActivos = buscadorCicloAgricola.busca(c, INSCRIPCION, usuario);
        CicloAgricola ciclo = buscadorCicloAgricola.getCicloSeleccionado(c, ciclosActivos, usuario, true);
        switch (tipo) {
            case "productor":
                // Buscamos si existe el UUID con ciclco y cultivo
                if (buscadorFacturaRestringida.existe(cfdi.getUuidTimbreFiscalDigital(),
                        buscadorCatalogo.buscaElemento(CicloAgricola.class, ciclo.getClave()),
                        c)) {
                    return;
                }
                validafechaMaxima(c, ciclo, cfdi.getFecha());
                validafechaMinima(c, ciclo, cfdi.getFecha());
        }
    }

    /**
     * Valida que para facturas de arroz venga al menos un comprobante.
     *
     * @param i
     */
    void validaComprobantes(InscripcionFactura i) {
        if (i.getCultivo().getClave().equals(CultivoEnum.ARROZ.getClave())) {
            LOGGER.info("Validando comprobantes Arroz");
            if (Objects.isNull(i.getComprobantes()) || i.getComprobantes().isEmpty()) {
                throw new SegalmexRuntimeException("Error de factura",
                        "Es necesario agregar al menos un comprobante de recepción.");
            }

            // Si es de oi-2021 del estado de Nayarit (18) deben estar en un rango de fechas los comprobantes.
            Parametro param = buscadorParametro.buscaElementoOpcional(i.getCultivo().getClave() + ":"
                    + i.getCiclo().getClave() + ":fecha-comprobante");
            if (i.getTipoCultivo().getClave().equals(TipoCultivoEnum.ARROZ_LARGO.getClave())
                    && i.getEstado().getClave().equals("18") && Objects.nonNull(param)) {
                Calendar fecha = SegalmexDateUtils.parseCalendar(param.getValor(), "yyyy-MM-dd'T'HH:mm:ss");
                Calendar comprobante = i.getComprobantes().get(0).getFechaComprobante();
                if (comprobante.compareTo(fecha) < 0) {
                    for (ComprobanteRecepcion cr : i.getComprobantes()) {
                        if (cr.getFechaComprobante().compareTo(fecha) >= 0) {
                            throw new SegalmexRuntimeException("Error de factura",
                                    "La fecha del comprobante " + cr.getDescripcion() + " es mayor a la permitida.");
                        }
                    }
                } else {
                    for (ComprobanteRecepcion cr : i.getComprobantes()) {
                        if (cr.getFechaComprobante().before(fecha)) {
                            throw new SegalmexRuntimeException("Error de factura",
                                    "La fecha del comprobante " + cr.getDescripcion() + " es menor a la permitida.");
                        }
                    }
                }
            }
        }
    }

    public void validaReceptorArroz(CfdiConverter cfdi) {
        if (cfdi.getRfcEmisor().length() == 12) {
            return;
        }
        InscripcionProductor ip = getInscripcionProductor(cfdi.getRfcEmisor());

        boolean isCorrecto = false;
        for (InscripcionMolino m : ip.getMolinos()) {
            if (cfdi.getRfcReceptor().equals(m.getMolino().getEmpresa().getRfc())) {
                isCorrecto = true;
            }
        }
        if (!isCorrecto) {
            List<String> motivos = new ArrayList<>();
            motivos.add("La factura NO es de un productor emitida para las bodegas registradas.");
            motivos.add("La factura fue emitida para: " + cfdi.getRfcReceptor());
            throw new SegalmexRuntimeException("Error del CFDI.", motivos);
        }
    }

    public void validaReceptorTrigo(CfdiConverter cfdi) {
        List<CicloAgricola> ciclosActivos = buscadorCicloAgricola.busca(cultivoHelper.getCultivo(), INSCRIPCION, contextoSeguridad.getUsuario());
        CicloAgricola ciclo = buscadorCicloAgricola.getCicloSeleccionado(cultivoHelper.getCultivo(), ciclosActivos, contextoSeguridad.getUsuario(), true);
        String uso = cultivoHelper.getCultivo().getClave() + ":" + ciclo.getClave() + EMPRESAS_TRIGO;
        List<Empresa> empresasTrigo = buscadorUsoCatalogo.busca(Empresa.class, uso);
        boolean isCorrecto = false;
        for (Empresa e : empresasTrigo) {
            if (cfdi.getRfcReceptor().equals(e.getRfc())) {
                isCorrecto = true;
            }
        }
        if (!isCorrecto) {
            List<String> motivos = new ArrayList<>();
            motivos.add("El RFC del receptor no es de una empresa válida.");
            motivos.add("La factura fue emitida para: " + cfdi.getRfcReceptor());
            throw new SegalmexRuntimeException("Error del CFDI.", motivos);
        }
    }

    public void validaEmisorTrigoGlobal(CfdiConverter cfdi) {
        List<CicloAgricola> ciclosActivos = buscadorCicloAgricola.busca(cultivoHelper.getCultivo(), INSCRIPCION, contextoSeguridad.getUsuario());
        CicloAgricola ciclo = buscadorCicloAgricola.getCicloSeleccionado(cultivoHelper.getCultivo(), ciclosActivos, contextoSeguridad.getUsuario(), true);
        String uso = cultivoHelper.getCultivo().getClave() + ":" + ciclo.getClave() + EMPRESAS_TRIGO;
        List<Empresa> empresasTrigo = buscadorUsoCatalogo.busca(Empresa.class, uso);
        boolean isCorrecto = false;
        for (Empresa e : empresasTrigo) {
            if (cfdi.getRfcEmisor().equals(e.getRfc())) {
                isCorrecto = true;
            }
        }
        if (!isCorrecto) {
            List<String> motivos = new ArrayList<>();
            motivos.add("La factura NO es una factura global emitida por una empresa válida.");
            motivos.add("La factura fue emitida por: " + cfdi.getRfcEmisor());
            throw new SegalmexRuntimeException("Error del CFDI.", motivos);
        }
    }

    /**
     * Se encarga de validar que los conecptos sean correctos.
     *
     * @param cfdi
     */
    void validaConceptos(Cfdi cfdi, Cultivo c, Usuario u) {
        List<CicloAgricola> ciclosActivos = buscadorCicloAgricola.busca(c, INSCRIPCION, u);
        CicloAgricola ciclo = buscadorCicloAgricola.getCicloSeleccionado(c, ciclosActivos, u, true);

        if (buscadorFacturaRestringida.existe(cfdi.getUuidTimbreFiscalDigital(),
                buscadorCatalogo.buscaElemento(CicloAgricola.class, ciclo.getClave()),
                c)) {
            LOGGER.debug("No se válidan claves servicio, existe excepción para esta factura.");
            return;
        }
        for (ConceptoCfdi cc : cfdi.getConceptos()) {
            if (cc.getEstatus().equals(EstatusConceptoCfdiEnum.NEGATIVO.getClave())) {
                List<String> motivos = new ArrayList<>();
                motivos.add("La clave producto servicio "
                        + cc.getClaveProductoServicio() + " es incorrecta.");
                throw new SegalmexRuntimeException("Error de factura.", motivos);
            }
        }
    }

    public void validaPredioProductor(Productor p, TipoCultivo tc, Estado e) {
        if (Objects.isNull(p)) {
            return;
        }

        tc = buscadorCatalogo.buscaElemento(TipoCultivo.class, tc.getId());
        e = buscadorCatalogo.buscaElemento(Estado.class, e.getId());
        InscripcionProductor ip = getInscripcionProductor(p.getRfc());
        boolean existe = false;

        for (PredioProductor pp : ip.getPrediosActivos()) {
            if (pp.getTipoCultivo().getClave().equals(tc.getClave())
                    && pp.getEstado().getClave().equals(e.getClave())) {
                existe = true;
            }
        }
        if (!existe) {
            List<String> motivos = new ArrayList<>();
            motivos.add("El productor no cuenta con un predio de "
                    + tc.getNombre() + " en " + e.getNombre());
            throw new SegalmexRuntimeException("Error de factura.", motivos);
        }
    }

    public ParametrosUsoFactura getParametrosUsoFactura(UsoFacturaParam params) {
        ParametrosUsoFactura parametros = new ParametrosUsoFactura();

        if (Objects.nonNull(params.getEstatus())) {
            parametros.setEstatus(buscadorCatalogo.buscaElemento(EstatusUsoFactura.class,
                    EstatusUsoFacturaEnum.getInstance(params.getEstatusFactura())));
        }

        if (!params.isSeguimiento()) {
            parametros.setEstatus(buscadorCatalogo.buscaElemento(EstatusUsoFactura.class,
                    EstatusUsoFacturaEnum.NUEVO));
        }

        if (Objects.nonNull(params.getEstatusFactura())) {
            parametros.setEstatusFactura(params.getEstatusFactura());
        }

        if (Objects.nonNull(params.getCiclo())) {
            parametros.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, params.getCiclo()));
        }

        if (Objects.nonNull(params.getCultivo())) {
            parametros.setCultivo(buscadorCatalogo.buscaElemento(Cultivo.class, params.getCultivo()));
        }

        if (Objects.nonNull(params.getFolio())) {
            parametros.setFolio(params.getFolio());
        }

        if (Objects.nonNull(params.getProductor())) {
            parametros.setProductor(buscadorCatalogo.buscaElemento(Productor.class, params.getProductor()));
        }

        if (Objects.nonNull(params.getRfcProductor())) {
            parametros.setRfcProductor(params.getRfcProductor());
        }

        if (Objects.nonNull(params.getUuid())) {
            parametros.setUuid(params.getUuid());
        }

        if (Objects.nonNull(params.getUuidProductor())) {
            ParametrosInscripcionProductor paramProductor = new ParametrosInscripcionProductor();
            paramProductor.setUuid(params.getUuidProductor());
            parametros.setProductor(buscadorProductor.buscaElemento(paramProductor));
        }

        return parametros;
    }

    public UsoFactura simplificaUsoFactura(UsoFactura uso) {
        UsoFactura u = new UsoFactura();

        u.setUuid(uso.getUuid());
        u.setEstatus(uso.getEstatus());
        u.setFolio(uso.getFolio());
        u.setFechaCreacion(uso.getFechaCreacion());
        u.setProductor(new Productor());
        u.getProductor().setNombre(uso.getProductor().getNombre());
        u.getProductor().setCurp(uso.getProductor().getCurp());
        u.getProductor().setRfc(uso.getProductor().getRfc());
        u.setEstimuloTonelada(uso.getEstimuloTonelada());
        u.setToneladas(uso.getToneladas());
        u.setEstimuloTotal(uso.getEstimuloTotal());
        u.setEstado(uso.getEstado());
        u.setTipoCultivo(uso.getTipoCultivo());
        u.setOrdenEstimulo(uso.getOrdenEstimulo());
        u.setProductorCiclo(new ProductorCiclo());
        u.getProductorCiclo().setTipoProductor(uso.getProductorCiclo().getTipoProductor());
        return u;

    }

    public List<UsoFactura> simplificaUsosFactura(List<UsoFactura> usos) {
        List<UsoFactura> simplificados = new ArrayList<>();
        for (UsoFactura uso : usos) {
            UsoFactura uf = new UsoFactura();
            uf.setFolio(uso.getFolio());
            uf.setUuid(uso.getUuid());
            uf.setOrdenEstimulo(uso.getOrdenEstimulo());
            uf.setProductorCiclo(uso.getProductorCiclo());
            uf.setEstado(new Estado());
            uf.getEstado().setNombre(uso.getEstado().getNombre());
            uf.setTipoCultivo(new TipoCultivo());
            uf.getTipoCultivo().setNombre(uso.getTipoCultivo().getNombre());
            uf.setContrato(uso.getContrato());
            if (Objects.nonNull(uso.getTipoPrecio())) {
                uf.setTipoPrecio(new TipoPrecio());
                uf.getTipoPrecio().setNombre(uso.getTipoPrecio().getNombre());
            }
            uf.setToneladas(uso.getToneladas());
            uf.setEstimuloTonelada(uso.getEstimuloTonelada());
            uf.setEstimuloTotal(uso.getEstimuloTotal());
            uf.setBanco(new Banco());
            uf.getBanco().setNombre(uso.getBanco().getNombre());
            uf.setClabe(uso.getClabe());
            uf.setTipo(uso.getTipo());
            uf.setComentarioEstatus(uso.getComentarioEstatus());
            simplificados.add(uf);
        }
        return simplificados;
    }

    void validaSociedad(Productor productor, Cfdi cfdi) {
        if (Objects.isNull(productor) || cfdi.getRfcEmisor().length() == 13) {
            return;
        }
        InscripcionProductor ip = getInscripcionProductor(productor.getRfc());
        boolean existe = false;
        for (EmpresaAsociada e : ip.getEmpresasActivas()) {
            if (e.getRfc().equals(cfdi.getRfcEmisor())) {
                existe = true;
            }
        }
        if (!existe) {
            List<String> motivos = new ArrayList<>();
            motivos.add("El productor no tiene registrada como sociedad a: "
                    + cfdi.getRfcEmisor());
            throw new SegalmexRuntimeException("Error de factura.", motivos);
        }
    }

    private InscripcionProductor getInscripcionProductor(String rfcProductor) {
        List<CicloAgricola> ciclosActivos = buscadorCicloAgricola.busca(cultivoHelper.getCultivo(), INSCRIPCION, contextoSeguridad.getUsuario());
        CicloAgricola ciclo = buscadorCicloAgricola.getCicloSeleccionado(cultivoHelper.getCultivo(), ciclosActivos, contextoSeguridad.getUsuario(), true);
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setRfcProductor(rfcProductor);
        parametros.setCiclo(ciclo);
        parametros.setCultivo(cultivoHelper.getCultivo());
        parametros.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.POSITIVA));
        try {
            return buscadorInscripcionProductor.buscaElemento(parametros);
        } catch (EmptyResultDataAccessException ouch) {
            LOGGER.error("No existe InscripcionProductor con el RFC: {} y ciclo: {}.", rfcProductor, ciclo.getNombre());
            throw new SegalmexRuntimeException("Error de productor.",
                    "El RFC del productor que emitió la factura es incorrecto, verifique que el productor se encuentre en estatus positivo.");
        }

    }

    void validaContrato(Productor productor, InscripcionFactura f) {
        if (Objects.isNull(productor) || Objects.isNull(StringUtils.trimToNull(f.getContrato()))) {
            return;
        }
        InscripcionProductor ip = getInscripcionProductor(productor.getRfc());
        boolean existe = false;
        for (ContratoFirmado c : ip.getContratosActivos()) {
            if (c.getNumeroContrato().equals(f.getContrato())) {
                verificaContratoPositivo(f.getContrato());
                existe = true;
            }
        }
        if (!existe) {
            List<String> motivos = new ArrayList<>();
            motivos.add("El productor no tiene registrado el número de contrato: "
                    + f.getContrato());
            throw new SegalmexRuntimeException("Error de factura.", motivos);
        }
    }

    private void verificaContratoPositivo(String contrato) {
        try {
            ParametrosInscripcionContrato parametros = new ParametrosInscripcionContrato();
            parametros.setCultivo(cultivoHelper.getCultivo());
            parametros.setNumeroContrato(contrato);
            parametros.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.POSITIVA));
            buscadorContrato.buscaElemento(parametros);
        } catch (EmptyResultDataAccessException ouch) {
            LOGGER.error("El contrato: {} no está como positivo.", contrato);
            throw new SegalmexRuntimeException("Error de factura.", "El contrato: "
                    + contrato + " no está en estatus positivo.");
        }
    }

    public ParametrosInformacionAlerta getParametrosDeudor(InscripcionFactura ifa, String tipo) {
        ParametrosInformacionAlerta parametros = new ParametrosInformacionAlerta();
        List<String> valores = new ArrayList<>();
        InscripcionProductor ip = getInscripcionProductor(ifa);
        switch (tipo) {
            case "rfc":
                valores.add(ifa.getCfdi().getRfcEmisor());
                valores.add(ifa.getProductorCiclo().getProductor().getRfc());
                valores.add(ip.getDatosProductor().getRfc());
                break;
            case "curp":
                valores.add(ifa.getProductorCiclo().getProductor().getCurp());
                valores.add(ip.getDatosProductor().getCurp());
                break;
        }
        parametros.setTipo(tipo);
        parametros.setValores(valores);
        return parametros;
    }

    public InscripcionProductor getInscripcionProductor(InscripcionFactura ifa) {
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setFolio(ifa.getProductorCiclo().getFolio());
        return buscadorInscripcionProductor.buscaElemento(parametros);
    }
}
