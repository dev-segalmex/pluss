/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.params;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import lombok.extern.slf4j.Slf4j;

/**
 *
 * @author ismael
 */
@Slf4j
public abstract class AbstractParam<T> {

    /**
     * Valor del parametro
     */
    private final T value;

    /**
     * Parametro original
     */
    private final String originalParam;

    /**
     * Constructor de la clase
     * @param param
     * @throws WebApplicationException
     */
    public AbstractParam(String param) {
        this.originalParam = param;
        try {
            this.value = parse(param);
        } catch (Exception e) {
            LOGGER.debug("Error: ", e);
            throw new WebApplicationException(onError(param, e));
        }
    }

    /**
     * Obtiene el valor del parametro
     * @return
     */
    public T getValue() {
        return value;
    }

    /**
     * Obtiene el valor original del parametro
     * @return
     */
    public String getOriginalParam() {
        return originalParam;
    }

    /**
     *
     * {@inheritDoc}
     *
     */
    @Override
    public String toString() {
        return value != null ? value.toString() : "";
    }

    /**
     * Parser del valor al parametro
     * @param param
     * @return
     * @throws Exception
     */
    protected abstract T parse(String param);

    /**
     * Si se encuentra en error
     * @param param
     * @param e
     * @return
     */
    private Response onError(String param, Throwable e) {
        return Response
                .status(Status.BAD_REQUEST)
                .entity(getErrorMessage(param, e))
                .build();
    }

    /**
     * Obtiene el mensaje de error
     * @param param
     * @param e
     * @return
     */
    private String getErrorMessage(String param, Throwable e) {
        return "Parámetro inválido: " + param + " (" + e.getMessage() + ")";
    }
}
