/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.paginas;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.seguridad.busqueda.BuscadorPermiso;
import mx.gob.segalmex.common.web.modelo.EnumConverter;
import mx.gob.segalmex.common.web.modelo.meta.ConsultasConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoRegistroEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.seguridad.Accion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Component
public class ConsultaSubResource {

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorPermiso buscadorPermiso;

    @GET
    @Produces("application/json")
    public ConsultasConverter getCatalogos() {
        ConsultasConverter converter = new ConsultasConverter();
        Accion a = buscadorCatalogo.buscaElemento(Accion.class, "validar_inscripcion");
        converter.setValidadores(buscadorPermiso.buscaUsuarioAutorizado(a, Boolean.TRUE));
        List<EstatusInscripcion> lista = buscadorCatalogo.busca(EstatusInscripcion.class);
        converter.setEstatus(lista);
        converter.setCiclos(buscadorCatalogo.busca(CicloAgricola.class));
        converter.setCultivos(buscadorCatalogo.busca(Cultivo.class));
        converter.setTiposRegistro(getTiposRegistro());
        return converter;

    }

    private List<EnumConverter> getTiposRegistro() {
        List<EnumConverter> tipos = new ArrayList<>();
        for (TipoRegistroEnum e : TipoRegistroEnum.values()) {
            EnumConverter tipo = new EnumConverter();
            tipo.setClave(e.getClave());
            tipo.setNombre(e.getNombre());
            tipos.add(tipo);
        }
        return tipos;
    }

}
