/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.inscripcion;

import java.io.InputStream;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.xml.bind.JAXBException;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.factura.CfdiFactory;
import mx.gob.segalmex.common.core.factura.CfdiParser;
import mx.gob.segalmex.common.core.factura.ProcesadorFacturaRestringida;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorFacturaRestringida;
import mx.gob.segalmex.common.core.factura.busqueda.ParametrosFacturaRestringida;
import mx.gob.segalmex.common.core.factura.modelo.CfdiComprobante;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.factura.FacturaRestringidaConverter;
import mx.gob.segalmex.common.web.resources.archivos.ArchivoFormUtils;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.factura.FacturaRestringida;
import org.apache.commons.lang3.StringUtils;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Slf4j
@Component
public class FacturaRestringidaSubresource {

    @Autowired
    private UsuarioSesionContextoHolder contextoSeguridad;

    @Autowired
    private CfdiParser cfdiParser;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorFacturaRestringida buscadorFacturaRestringida;

    @Autowired
    private ProcesadorFacturaRestringida procesadorFacturaRestringida;

    @GET
    @Produces("application/json")
    public List<FacturaRestringidaConverter> index() {
        ParametrosFacturaRestringida params = new ParametrosFacturaRestringida();
        params.setActivo(Boolean.TRUE);
        List<FacturaRestringida> facturasRestringidas = buscadorFacturaRestringida.busca(params);
        return CollectionConverter.convert(FacturaRestringidaConverter.class,
                facturasRestringidas, 2);
    }

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces({"application/json;charset=utf-8"})
    public FacturaRestringidaConverter registra(@QueryParam("cultivo") Integer cultivo, @QueryParam("ciclo") Integer ciclo,
            MultipartFormDataInput input) {

        if (Objects.isNull(cultivo) || Objects.isNull(ciclo)) {
            LOGGER.warn("No se puede guardar Factura restringida con  Cultivo o Ciclo nulos.");
            throw new IllegalArgumentException("Cultivo o Ciclo no pueden ser nulos.");
        }
        InputStream is = ArchivoFormUtils.getInputStrem(input);
        FacturaRestringida fr = new FacturaRestringida();
        try {
            CfdiComprobante cc = cfdiParser.parse(is);
            fr.setUuidTimbreFiscalDigital(CfdiFactory.formatoCfdi(cc.getComplemento().
                    getTimbreFiscalDigital().getUuid()));
            fr.setUsuarioRegistra(contextoSeguridad.getUsuario());
            fr.setCultivo(buscadorCatalogo.buscaElemento(Cultivo.class, cultivo));
            fr.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, ciclo));
            procesadorFacturaRestringida.procesa(fr);
            return new FacturaRestringidaConverter(fr, 1);
        } catch (SegalmexRuntimeException ouch) {
            throw new SegalmexRuntimeException(ouch.getMessage(),
                    ouch.getMotivos());
        } catch (JAXBException | RuntimeException ouch) {
            throw new SegalmexRuntimeException("Error al guardar la Factura.",
                    "El archivo no es válido.");
        }
    }

    @Path("/{uuid}")
    @DELETE
    @Produces("application/json")
    public FacturaRestringidaConverter elimina(@PathParam("uuid") String uuid) {
        if (Objects.isNull(StringUtils.trimToNull(uuid))) {
            LOGGER.warn("El UUID no puede ser nulo.");
            throw new IllegalArgumentException("El UUID no puede ser nulo.");
        }
        ParametrosFacturaRestringida params = new ParametrosFacturaRestringida();
        params.setActivo(Boolean.TRUE);
        params.setUuidTimbreFiscalDigital(uuid);
        FacturaRestringida fr = buscadorFacturaRestringida.buscaElemento(params);
        procesadorFacturaRestringida.elimina(fr, contextoSeguridad.getUsuario());
        return new FacturaRestringidaConverter(fr, 1);
    }
}
