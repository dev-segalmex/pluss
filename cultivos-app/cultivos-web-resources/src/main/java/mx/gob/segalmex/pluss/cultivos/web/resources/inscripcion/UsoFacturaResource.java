/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.inscripcion;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.factura.ProcesadorUsoFactura;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorUsoFactura;
import mx.gob.segalmex.common.web.modelo.RespuestaConverter;
import mx.gob.segalmex.common.web.modelo.factura.AdministracionUsoFacturaConverter;
import mx.gob.segalmex.pluss.modelo.factura.EstatusUsoFacturaEnum;
import mx.gob.segalmex.pluss.modelo.factura.UsoFactura;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Slf4j
@Path("/uso-factura/")
@Component
public class UsoFacturaResource {

    @Autowired
    private BuscadorUsoFactura buscador;

    @Autowired
    private ProcesadorUsoFactura procesadorUsoFactura;

    @PUT
    @Path("/estatus/pendiente")
    @Consumes("application/json")
    @Produces("application/json")
    public RespuestaConverter marcarPendientes(AdministracionUsoFacturaConverter converter) {
        LOGGER.info("Marcando como pendientes usos facturas...");
        List<UsoFactura> usosFactura
                = getUsosFactura(converter.getUsosFacturasUuid(), converter.getComentario());
        procesadorUsoFactura.cambiaEstatus(usosFactura, EstatusUsoFacturaEnum.PENDIENTE,
                EstatusUsoFacturaEnum.NUEVO, converter.getComentario());
        RespuestaConverter rc = new RespuestaConverter();
        rc.setMensaje("Se actualizaron con éxito los usos factura.");
        return rc;
    }

    public List<UsoFactura> getUsosFactura(List<String> uuidUsos, String comentario) {
        List<UsoFactura> usosFactura = new ArrayList<>();
        for (String uuid : uuidUsos) {
            UsoFactura uf = buscador.buscaElementoUuid(uuid);
            usosFactura.add(uf);
        }
        return usosFactura;
    }

}
