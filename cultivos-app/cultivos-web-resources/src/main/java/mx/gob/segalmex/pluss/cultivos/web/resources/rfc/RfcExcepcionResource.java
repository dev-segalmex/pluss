/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.rfc;

import java.io.InputStream;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.resources.archivos.ArchivoFormUtils;
import mx.gob.segalmex.granos.core.rfc.ProcesadorRfcExcepcion;
import mx.gob.segalmex.granos.core.rfc.busqueda.BuscadorRfcExcepcion;
import mx.gob.segalmex.granos.core.rfc.busqueda.ParametrosRfcExcepcion;
import mx.gob.segalmex.granos.web.modelo.productor.RfcExcepcionConverter;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import mx.gob.segalmex.pluss.modelo.productor.RfcExcepcion;
import org.apache.commons.lang3.StringUtils;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Slf4j
@Path("/rfc-excepcion/")
@Component
public class RfcExcepcionResource {

    @Autowired
    private UsuarioSesionContextoHolder contextoSeguridad;

    @Autowired
    private BuscadorRfcExcepcion buscadorRfcExcepcion;

    @Autowired
    private ProcesadorRfcExcepcion procesadorRfcExcepcion;

    @POST
    @Consumes(MediaType.MULTIPART_FORM_DATA)
    @Produces({"application/json;charset=utf-8"})
    public RfcExcepcionConverter registra(@QueryParam("rfc") String rfc,
            MultipartFormDataInput input) {
        if (Objects.isNull(StringUtils.trimToNull(rfc))) {
            LOGGER.warn("No se puede guardar sin rfc.");
            throw new IllegalArgumentException("No se puede guardar sin rfc.");
        }
        InputStream is = ArchivoFormUtils.getInputStrem(input);
        RfcExcepcion re = procesadorRfcExcepcion.procesa(rfc, contextoSeguridad.getUsuario(),
                is, ArchivoFormUtils.getFilename(input, "pdf"));

        return new RfcExcepcionConverter(re, 1);
    }

    @GET
    @Produces("application/json")
    public List<RfcExcepcionConverter> index() {

        ParametrosRfcExcepcion parametros = new ParametrosRfcExcepcion();
        parametros.setActivo(Boolean.TRUE);
        List<RfcExcepcion> excepciones = buscadorRfcExcepcion.busca(parametros);
        return CollectionConverter.convert(RfcExcepcionConverter.class,
                excepciones, 2);
    }

    @Path("/{rfc}")
    @DELETE
    @Produces("application/json")
    public RfcExcepcionConverter elimina(@PathParam("rfc") String rfc) {
        if (Objects.isNull(StringUtils.trimToNull(rfc))) {
            LOGGER.warn("No se puede desactivar sin rfc.");
            throw new IllegalArgumentException("No se puede desactivar sin rfc.");
        }
        RfcExcepcion re = buscadorRfcExcepcion.buscaElemento(rfc);
        procesadorRfcExcepcion.elimina(re, contextoSeguridad.getUsuario());
        return new RfcExcepcionConverter(re, 1);
    }
}
