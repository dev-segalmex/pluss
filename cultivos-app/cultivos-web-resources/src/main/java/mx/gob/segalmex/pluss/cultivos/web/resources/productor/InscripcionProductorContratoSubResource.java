/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.productor;

import javax.ws.rs.DELETE;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.granos.core.productor.ProcesadorInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInscripcionProductor;
import mx.gob.segalmex.granos.web.modelo.productor.InscripcionProductorConverter;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Slf4j
@Component
public class InscripcionProductorContratoSubResource {

    @Autowired
    private BuscadorInscripcionProductor buscadorInscripcionProductor;

    @Autowired
    private ProcesadorInscripcionProductor procesadorInscripcionProductor;

    @Autowired
    private UsuarioSesionContextoHolder contextoSeguridad;

    @Path("/{index}")
    @DELETE
    public InscripcionProductorConverter elimina(@PathParam("uuid") String uuid, @PathParam("index") Integer index) {
        LOGGER.debug("Eliminando contrato de la inscripción: {}", uuid);
        InscripcionProductor inscripcion = buscadorInscripcionProductor.buscaElemento(uuid);
        procesadorInscripcionProductor.eliminaContrato(inscripcion, index, contextoSeguridad.getUsuario());
        return new InscripcionProductorConverter(inscripcion, 1);
    }

}
