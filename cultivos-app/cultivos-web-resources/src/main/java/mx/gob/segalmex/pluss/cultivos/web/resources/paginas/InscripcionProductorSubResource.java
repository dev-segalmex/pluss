/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.paginas;

import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorUsoCatalogo;
import mx.gob.segalmex.common.core.empresas.busqueda.BuscadorSucursal;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.util.CultivoClaveUtil;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.common.web.modelo.inscripcion.CatalogosInscripcionConverter;
import mx.gob.segalmex.granos.core.inscripcion.BuscadorCicloAgricolaActivo;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import mx.gob.segalmex.pluss.cultivos.web.resources.productor.CultivoHelper;
import mx.gob.segalmex.pluss.modelo.catalogos.Banco;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.GrupoIndigena;
import mx.gob.segalmex.pluss.modelo.catalogos.Municipio;
import mx.gob.segalmex.pluss.modelo.catalogos.NivelEstudio;
import mx.gob.segalmex.pluss.modelo.catalogos.Pais;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import mx.gob.segalmex.pluss.modelo.catalogos.Parentesco;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoDocumento;
import mx.gob.segalmex.pluss.modelo.personas.Sexo;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersona;
import mx.gob.segalmex.pluss.modelo.productor.RegimenHidrico;
import mx.gob.segalmex.pluss.modelo.productor.TipoPosesion;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Slf4j
@Component
public class InscripcionProductorSubResource {

    private static final String INSCRIPCION = "productor";

    private static final String GPO_RENDIMIENTO = ":rendimiento";

    private static final String GPO_SUCURSAL = "sucursal:";

    private static final String APLICA_CONTRATO = ":aplica-contratos";

    private static final String APLICA_PERSONA_MORAL = ":aplica-persona-moral";

    private static final String CONTRATO_REQUERIDO = ":contrato-requerido";

    private static final String PREDIO_SOLICITADO = ":predio-solicitado";

    private static final String ARCHIVOS_PREDIO = ":archivos-predio";

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private UsuarioSesionContextoHolder contextoSeguridad;

    @Autowired
    private BuscadorUsoCatalogo buscadorUso;

    @Autowired
    private BuscadorCicloAgricolaActivo buscadorCicloAgricola;

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Autowired
    private BuscadorSucursal buscadorSucursal;

    @Autowired
    private CultivoHelper cultivoHelper;

    @GET
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public CatalogosInscripcionConverter getCatalogos(@QueryParam("inscripcion") @DefaultValue("true") boolean inscripcion) {
        LOGGER.info("carga de catalogo de para la inscripción contrato.");
        Cultivo c = cultivoHelper.getCultivo();
        Usuario u = contextoSeguridad.getUsuario();
        CatalogosInscripcionConverter converter = new CatalogosInscripcionConverter();
        try {
            List<CicloAgricola> ciclosActivos = buscadorCicloAgricola.busca(c, INSCRIPCION, u);
            converter.setCiclos(inscripcion ? ciclosActivos : null);
            converter.setCultivos(inscripcion ? buscadorCatalogo.busca(Cultivo.class) : null);
            converter.setTipos(buscadorUso.busca(TipoCultivo.class, "cultivo:" + CultivoClaveUtil.getClaveCorta(c)));
            converter.setEstados(buscadorCatalogo.busca(Estado.class));
            converter.setTiposPersona(buscadorUso.busca(TipoPersona.class, "fisica-moral"));
            converter.setMunicipios(inscripcion ? buscadorCatalogo.busca(Municipio.class) : null);
            converter.setPaises(buscadorCatalogo.busca(Pais.class));
            converter.setTiposDocumentoIdentificacion(buscadorUso.busca(TipoDocumento.class, "documento-identificacion"));
            converter.setRegimenes(buscadorCatalogo.busca(RegimenHidrico.class));
            converter.setTiposPosesion(buscadorCatalogo.busca(TipoPosesion.class));
            converter.setBancos(buscadorCatalogo.busca(Banco.class));
            converter.setTiposDocumentoPropia(buscadorUso.busca(TipoDocumento.class, "documento-posesion-propia"));
            converter.setTiposDocumentoRentada(buscadorUso.busca(TipoDocumento.class, "documento-posesion-rentada"));
            converter.setSexos(inscripcion ? buscadorCatalogo.busca(Sexo.class) : null);
            converter.setParentescos(buscadorCatalogo.busca(Parentesco.class));
            converter.setSucursal(inscripcion ? getSucursal() : null);
            converter.setVolumenPredio(buscadorParametro.buscaElemento(c.getClave() + ":" + "maximo-toneladas"));
            converter.setSuperficiePredio(buscadorParametro.buscaElemento(c.getClave() + ":" + "maximo-hectareas"));
            converter.setSuperficieTemporal(buscadorParametro.buscaElementoOpcional(c.getClave() + ":minimo-hectareas-temporal"));
            converter.setCantidadContratada(buscadorParametro.buscaElementoOpcional(c.getClave() + ":" + "maximo-cantidad-contratada"));
            CicloAgricola seleccionado = buscadorCicloAgricola.getCicloSeleccionado(c, ciclosActivos, u, true);
            if (Objects.nonNull(seleccionado)) {
                converter.setSucursales(buscadorUso.busca(Sucursal.class,
                        GPO_SUCURSAL + c.getClave() + ":" + seleccionado.getClave()));
                converter.setRendimientos(buscadorParametro.busca(c.getClave()
                        + ":" + seleccionado.getClave() + GPO_RENDIMIENTO));
                //Definir que estados son permitidos para los predios de cada cultivo. Ej:estado:clavecultivo:claveciclo
                converter.setEstadosPredio(buscadorUso.busca(Estado.class,
                        "estado:" + c.getClave() + ":" + seleccionado.getClave()));
                converter.setCicloSeleccionado(seleccionado);
                converter.setAplicaContrato(aplicaParametro(c, seleccionado, APLICA_CONTRATO));
                converter.setAplicaPersonaMoral(aplicaParametro(c, seleccionado, APLICA_PERSONA_MORAL));
                converter.setContratoRequerido(aplicaParametro(c, seleccionado, CONTRATO_REQUERIDO));
                converter.setPredioSolicitado(aplicaParametro(c, seleccionado, PREDIO_SOLICITADO));
                converter.setArchivosPredio(aplicaParametro(c, seleccionado, ARCHIVOS_PREDIO));
                converter.setArchivosRiego(buscadorParametro.busca(c.getClave() + ":" + seleccionado.getClave() + ":predio:riego-xml"));
                converter.setArchivosSiembra(buscadorParametro.busca(c.getClave() + ":" + seleccionado.getClave() + ":predio:siembra-xml"));
                converter.setRegistroCerrado(getRegistroCerrado(c.getClave()
                        + ":" + seleccionado.getClave() + ":productores:cierre"));
                converter.setGruposIndigenas(buscadorCatalogo.busca(GrupoIndigena.class));
                converter.setNivelEstudios(buscadorCatalogo.busca(NivelEstudio.class));
            }
        } catch (SegalmexRuntimeException ouch) {
            LOGGER.error("El usuario {} no tiene ciclo seleccionado.", u.getClave());
        }
        return converter;
    }

    private Sucursal getSucursal() {
        //Tomaría la sucursal ya asociada al usuario
        List<Sucursal> sucursales = buscadorSucursal.busca(contextoSeguridad.getUsuario());
        return sucursales.isEmpty() ? null : sucursales.get(0);
    }

    private boolean aplicaParametro(Cultivo c, CicloAgricola seleccionado, String clave) {
        Parametro p = buscadorParametro.buscaElementoOpcional(c.getClave() + ":" + seleccionado.getClave() + clave);
        return Objects.nonNull(p) && Boolean.valueOf(p.getValor());
    }

    private Parametro getRegistroCerrado(String clave) {
        Parametro parametro = buscadorParametro.buscaElementoOpcional(clave);
        if (Objects.nonNull(parametro)) {
            Calendar cierre = SegalmexDateUtils
                    .parseCalendar(parametro.getValor(), "yyyy-MM-dd'T'HH:mm:ss");
            if (Calendar.getInstance().after(cierre)) {
                return parametro;
            }
        }
        return null;
    }
}
