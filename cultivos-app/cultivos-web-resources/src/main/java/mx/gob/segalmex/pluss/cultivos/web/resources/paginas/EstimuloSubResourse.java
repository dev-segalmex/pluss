/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.paginas;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.web.modelo.inscripcion.CatalogosInscripcionConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author erikcam
 */
@Component
public class EstimuloSubResourse {
  @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @GET
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public CatalogosInscripcionConverter getCatalogos() {

        CatalogosInscripcionConverter catalgos = new CatalogosInscripcionConverter();
        catalgos.setCiclos(buscadorCatalogo.busca(CicloAgricola.class));
        catalgos.setCultivos(buscadorCatalogo.busca(Cultivo.class));
        catalgos.setTipos(buscadorCatalogo.busca(TipoCultivo.class));
        catalgos.setEstados(buscadorCatalogo.busca(Estado.class));
        return catalgos;
    }
}
