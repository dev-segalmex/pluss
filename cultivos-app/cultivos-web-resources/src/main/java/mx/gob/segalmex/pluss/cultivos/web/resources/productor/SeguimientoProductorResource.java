package mx.gob.segalmex.pluss.cultivos.web.resources.productor;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.BeanParam;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorUsoFactura;
import mx.gob.segalmex.common.core.factura.busqueda.ParametrosUsoFactura;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.factura.UsoFacturaConverter;
import mx.gob.segalmex.common.web.modelo.productor.ProductorConverter;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.cultivos.web.resources.inscripcion.InscripcionParam;
import mx.gob.segalmex.pluss.modelo.factura.UsoFactura;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersona;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersonaEnum;
import mx.gob.segalmex.pluss.modelo.productor.Productor;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author erikcam
 */
@Path("/seguimiento-productor/")
@Component
public class SeguimientoProductorResource {

    @Autowired
    private ProductorResourceHelper helper;

    @Autowired
    private BuscadorProductor buscadorProductor;

    @Autowired
    private BuscadorUsoFactura buscadorUsosFactura;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @GET
    @Produces("application/json")
    public List<ProductorConverter> getProductores(@BeanParam InscripcionParam params) {
        ParametrosInscripcionProductor parametros = helper.getInstance(params);
        parametros.setTipoPersona(buscadorCatalogo.buscaElemento(TipoPersona.class,
                TipoPersonaEnum.FISICA.getClave()));
        parametros.setActivo(true);
        List<ProductorCiclo> resultados = buscadorProductor.buscaProductorCiclo(parametros);
        List<Productor> simplificados = new ArrayList<>();
        for(ProductorCiclo pc:resultados){
            simplificados.add(helper.simplificaProductorCiclo(pc));
        }

        return CollectionConverter.convert(ProductorConverter.class,
                simplificados, 2);
    }

    @Path("/pagos/")
    @GET
    @Produces("application/json")
    public List<UsoFacturaConverter> getUsos(@BeanParam InscripcionParam params) {
        List<UsoFactura> usos = getUsosFactura(params);
        return CollectionConverter.convert(UsoFacturaConverter.class, usos, 2);
    }

    @Path("/productor/")
    @GET
    @Produces("application/json")
    public ProductorConverter getProductor(@BeanParam InscripcionParam params) {
        Productor p = buscadorProductor.busca(params.getClave());
        return new ProductorConverter(p, 1);
    }

    public List<UsoFactura> getUsosFactura(InscripcionParam params) {
        TipoPersona tipo = buscadorCatalogo.buscaElemento(TipoPersona.class, TipoPersonaEnum.FISICA.getClave());
        Productor p = buscadorProductor.busca(params.getRfcEmisor(), tipo);

        ParametrosUsoFactura parametros = new ParametrosUsoFactura();
        parametros.setProductor(p);
        List<UsoFactura> usos = buscadorUsosFactura.busca(parametros);
        return usos;
    }

}
