/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.common;

import java.util.List;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorUsoCatalogo;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.common.web.modelo.catalogos.CicloAgricolaConverter;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import mx.gob.segalmex.pluss.cultivos.web.resources.productor.CultivoHelper;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.UsoCatalogo;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Path("/cultivos/")
@Component
public class CultivoResource {

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private UsuarioSesionContextoHolder contextoSeguridad;

    @Autowired
    private BuscadorUsoCatalogo buscadorUso;

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private CultivoHelper cultivoHelper;

    @Path("/ciclo-agricola/{ciclo}/seleccion/")
    @POST
    public CicloAgricolaConverter selecciona(@PathParam("ciclo") String ciclo) {
        Cultivo c = cultivoHelper.getCultivo();
        CicloAgricola ca = buscadorCatalogo.buscaElemento(CicloAgricola.class, ciclo);
        Usuario u = contextoSeguridad.getUsuario();

        UsoCatalogo uso = new UsoCatalogo();
        uso.setClase(CicloAgricola.class.getName());
        uso.setUso("ciclo-seleccionado:" + c.getClave() + ":" + u.getId());
        uso.setClave(ca.getClave());
        seleccionaCiclo(uso);

        return new CicloAgricolaConverter(ca, 1);
    }


    private void seleccionaCiclo(UsoCatalogo uso) {
        // Buscamos si existe el uso
        List<UsoCatalogo> usos = buscadorUso.buscaUsos(CicloAgricola.class, uso.getUso());

        // Si es vacío, creamos el nuevo y terminamos
        if (usos.isEmpty()) {
            registroEntidad.guarda(uso);
            return;
        }

        UsoCatalogo u = usos.get(0);
        u.setClave(uso.getClave());
        registroEntidad.actualiza(u);
    }
}
