/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.paginas;

import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.web.modelo.inscripcion.CatalogosInscripcionConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Component
public class InformacionAlertaSubResource {

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @GET
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public CatalogosInscripcionConverter getCatalogos (){
        CatalogosInscripcionConverter catalogos = new CatalogosInscripcionConverter();
        catalogos.setCultivos(buscadorCatalogo.busca(Cultivo.class));
        catalogos.setCiclos(buscadorCatalogo.busca(CicloAgricola.class));
        return  catalogos;

    }
}
