/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.productor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorPredioDocumento;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosPredioDocumento;
import mx.gob.segalmex.pluss.modelo.productor.PredioDocumento;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Slf4j
@Path("/predio-documento/")
@Component
public class InscripcionProductorPredioDocumentoResource {

    @Autowired
    private BuscadorPredioDocumento buscadorPredioDocumento;

    @GET
    @Path("/duplicados/{uuidFiscal}")
    public List<String> getFoliosDuplicados(@PathParam("uuidFiscal") String uuidFiscal) {
        LOGGER.info("Buscando duplicados...");
        ParametrosPredioDocumento params = new ParametrosPredioDocumento();
        params.setUuidTimbreFiscalDigital(uuidFiscal);
        List<PredioDocumento> documentos = buscadorPredioDocumento.busca(params);
        List<String> folios = new ArrayList<>();
        for (PredioDocumento pd : documentos) {
            folios.add(pd.getInscripcionProductor().getFolio());
        }
        return folios.stream().distinct().collect(Collectors.toList());
    }
}
