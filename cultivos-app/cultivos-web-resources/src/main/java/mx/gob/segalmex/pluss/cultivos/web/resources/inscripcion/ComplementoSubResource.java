/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.web.resources.inscripcion;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.granos.core.contrato.ProcesadorInscripcionContrato;
import mx.gob.segalmex.granos.core.contrato.busqueda.BuscadorInscripcionContrato;
import mx.gob.segalmex.granos.core.contrato.busqueda.ParametrosInscripcionContrato;
import mx.gob.segalmex.granos.web.modelo.contrato.DatoCapturadoConverter;
import mx.gob.segalmex.granos.web.modelo.contrato.InscripcionContratoConverter;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import mx.gob.segalmex.pluss.modelo.inscripcion.DatoCapturado;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Component
public class ComplementoSubResource {

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorInscripcionContrato buscadorInscripcionContrato;

    @Autowired
    private UsuarioSesionContextoHolder contextoSeguridad;

    @Autowired
    private ProcesadorInscripcionContrato procesadorInscripcionContrato;

    @Autowired
    private ContratoResourceHelper resourceHelper;

    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionContratoConverter crea(@PathParam("uuid") String uuid) {
        ParametrosInscripcionContrato params = new ParametrosInscripcionContrato();
        params.setUuid(uuid);
        params.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.POSITIVA.getClave()));
        InscripcionContrato inscripcion = buscadorInscripcionContrato.buscaElemento(params);

        procesadorInscripcionContrato.solicitaComplemento(inscripcion, contextoSeguridad.getUsuario());
        return new InscripcionContratoConverter(inscripcion, 0);
    }

    @Path("/aceptado/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionContratoConverter acepta(@PathParam("uuid") String uuid) {
        ParametrosInscripcionContrato params = new ParametrosInscripcionContrato();
        params.setUuid(uuid);
        params.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.POSITIVA.getClave()));
        InscripcionContrato inscripcion = buscadorInscripcionContrato.buscaElemento(params);
        procesadorInscripcionContrato.aceptaComplemento(inscripcion, contextoSeguridad.getUsuario());
        return new InscripcionContratoConverter(inscripcion, 0);
    }

    @Path("/rechazado/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionContratoConverter rechaza(@PathParam("uuid") String uuid) {
        ParametrosInscripcionContrato params = new ParametrosInscripcionContrato();
        params.setUuid(uuid);
        params.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.POSITIVA.getClave()));
        InscripcionContrato inscripcion = buscadorInscripcionContrato.buscaElemento(params);
        procesadorInscripcionContrato.rechazaComplemento(inscripcion, contextoSeguridad.getUsuario());
        return new InscripcionContratoConverter(inscripcion, 0);
    }

    @Path("/iar/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionContratoConverter registraIar(@PathParam("uuid") String uuid, InscripcionContratoConverter converter) {
        ParametrosInscripcionContrato params = new ParametrosInscripcionContrato();
        params.setUuid(uuid);
        params.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.POSITIVA.getClave()));
        InscripcionContrato inscripcion = buscadorInscripcionContrato.buscaElemento(params);
        resourceHelper.inicializaIar(inscripcion, converter.getEntity().getCoberturas());
        inscripcion.setCoberturas(new ArrayList<>());
        inscripcion.getCoberturas().addAll(converter.getEntity().getCoberturas());
        procesadorInscripcionContrato.procesaIar(inscripcion, contextoSeguridad.getUsuario());
        return new InscripcionContratoConverter(inscripcion, 0);
    }

    @Path("/validacion/negativa/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionContratoConverter validaNegativo(@PathParam("uuid") String uuid, List<DatoCapturadoConverter> datos) {
        InscripcionContrato inscripcion = buscadorInscripcionContrato.buscaElemento(uuid);

        List<DatoCapturado> dcs = new ArrayList<>();
        for (DatoCapturadoConverter dc : datos) {
            dcs.add(dc.getEntity());
        }
        procesadorInscripcionContrato.solicitaCorreccionComplemento(inscripcion, contextoSeguridad.getUsuario(), dcs);
        return new InscripcionContratoConverter(inscripcion, 0);
    }

    @Path("/validacion/positiva/")
    @POST
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionContratoConverter validaPositivo(@PathParam("uuid") String uuid, List<DatoCapturadoConverter> datos) {
        InscripcionContrato inscripcion = buscadorInscripcionContrato.buscaElemento(uuid);
        resourceHelper.getValidaEstatus(inscripcion.getEstatus(), EstatusInscripcionEnum.POSITIVA);
        List<DatoCapturado> dcs = new ArrayList<>();
        for (DatoCapturadoConverter dc : datos) {
            dcs.add(dc.getEntity());
        }
        procesadorInscripcionContrato.validaComplementoPositivo(inscripcion, contextoSeguridad.getUsuario(), dcs);
        return new InscripcionContratoConverter(inscripcion, 0);
    }

    @Path("/validacion/negativa/")
    @PUT
    @Consumes("application/json")
    @Produces("application/json")
    public InscripcionContratoConverter corrigeValidacionNegativa(@PathParam("uuid") String uuid, List<DatoCapturadoConverter> datos) {
        InscripcionContrato inscripcion = buscadorInscripcionContrato.buscaElemento(uuid);
        resourceHelper.getValidaEstatus(inscripcion.getEstatus(), EstatusInscripcionEnum.POSITIVA);
        List<DatoCapturado> dcs = new ArrayList<>();
        for (DatoCapturadoConverter dc : datos) {
            dcs.add(dc.getEntity());
        }
        procesadorInscripcionContrato.corrigeComplemento(inscripcion, contextoSeguridad.getUsuario(), dcs);
        return new InscripcionContratoConverter(inscripcion, 0);
    }

}
