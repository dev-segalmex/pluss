CREATE TABLE `localidad` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fecha_actualizacion` datetime NOT NULL,
  `fecha_creacion` datetime NOT NULL,
  `activo` bit(1) NOT NULL,
  `clave` varchar(63) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `orden` int(11) NOT NULL,
  `codigo_postal` varchar(255) NOT NULL,
  `municipio_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `UK_18ptvptfhaoqwjxiti8d4ot6b` (`clave`),
  KEY `FK_hrgy55b3vx5ml1fysq66dl0jc` (`municipio_id`),
  CONSTRAINT `FK_hrgy55b3vx5ml1fysq66dl0jc` FOREIGN KEY (`municipio_id`) REFERENCES `municipio` (`id`)
);
