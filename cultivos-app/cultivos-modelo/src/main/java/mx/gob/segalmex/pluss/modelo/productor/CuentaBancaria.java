/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.productor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.catalogos.Banco;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "cuenta_bancaria")
@Getter
@Setter
public class CuentaBancaria extends AbstractEntidad {

    /**
     * La información del banco al que pertenece la cuenta.
     */
    @ManyToOne
    @JoinColumn(name = "banco_id", nullable = false)
    private Banco banco;

    /**
     * El número de cuenta.
     */
    @Column(name = "numero", nullable = false)
    private String numero;

    /**
     * La clabe interbancaria.
     */
    @Column(name = "clabe", nullable = false)
    private String clabe;

}
