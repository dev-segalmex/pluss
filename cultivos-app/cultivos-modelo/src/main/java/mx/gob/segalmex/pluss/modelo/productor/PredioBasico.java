package mx.gob.segalmex.pluss.modelo.productor;

import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;

import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import java.math.BigDecimal;

@MappedSuperclass
@Getter
@Setter
public class PredioBasico extends AbstractEntidad {

    /**
     * La información del estado del predio.
     */
    @ManyToOne
    @JoinColumn(name = "estado_id", nullable = false)
    private Estado estado;

    /**
     * La cantidad de superficie sembrada del predio.
     */
    @Column(name = "superficie", precision = 19, scale = 3)
    private BigDecimal superficie;

}
