/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.productor;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;

/**
 *
 * @author jurgen
 */
@Entity
@Table(name = "predio_documento")
@Getter
@Setter
public class PredioDocumento extends AbstractEntidad {

    /**
     * El número del predio al que pertenece este predioDocumento.
     */
    @Column(name = "numero", nullable = false)
    private String numero;

    /**
     * El tipo de documento, siembra o riego.
     */
    @Column(name = "tipo", nullable = false)
    private String tipo;

    /**
     * El RFC del emisor.
     */
    @Column(name = "rfc_emisor")
    private String rfcEmisor;

    /**
     * El RFC del receptor.
     */
    @Column(name = "rfc_receptor")
    private String rfcReceptor;

    /**
     * El UUID del timbre fiscal digital generando por el SAT.
     */
    @Column(name = "uuid_timbre_fiscal_digital")
    private String uuidTimbreFiscalDigital;

    /**
     * El total.
     */
    @Column(name = "total", scale = 3)
    private BigDecimal total;

    /**
     * Indica si el documento es o no válio según el SAT.
     */
    @Column(name = "validacion_sat")
    private Boolean validacionSat;

    /**
     * Indica si el documento está duplicado, registrado previamente.
     */
    @Column(name = "duplicado")
    private Boolean duplicado;

    /**
     * La propiedad para indicar que este PredioDocumento es monitoreable.
     */
    @Column(name = "monitoreable", nullable = false)
    private String monitoreable = "--";

    /**
     * La InscripcionProductor a la que pertenece el documento.
     */
    @ManyToOne
    @JoinColumn(name = "inscripcion_productor_id", nullable = false)
    private InscripcionProductor inscripcionProductor;

    /**
     * Indica si el PredioDocumento está activo.
     */
    @Column(name = "activo", nullable = false)
    private boolean activo = true;

}
