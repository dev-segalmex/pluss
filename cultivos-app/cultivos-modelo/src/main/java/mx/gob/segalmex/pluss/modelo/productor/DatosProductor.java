/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.productor;

import java.util.Calendar;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.catalogos.Pais;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoDocumento;
import mx.gob.segalmex.pluss.modelo.personas.Sexo;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersona;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "datos_productor")
@Getter
@Setter
public class DatosProductor extends AbstractEntidad {

    /**
     * El tipo de persona física o moral que es el productor.
     */
    @ManyToOne
    @JoinColumn(name = "tipo_persona_id")
    private TipoPersona tipoPersona;

    /**
     * El nombre de la persona moral en el caso en que el productor sea una sociedad.
     */
    @Column(name = "nombre_moral")
    private String nombreMoral;

    /**
     * El RFC del productor, ya sea persona física o moral.
     */
    @Column(name = "rfc")
    private String rfc;

    /**
     * El CURP del productor o representante legal.
     */
    @Column(name = "curp")
    private String curp;

    /**
     * El nombre del productor o representante legal.
     */
    @Column(name = "nombre")
    private String nombre;

    /**
     * El primer apellido del productor o representante legal.
     */
    @Column(name = "primer_apellido")
    private String primerApellido;

    /**
     * El segundo apellido del productor o representante legal.
     */
    @Column(name = "segundo_apellido")
    private String segundoApellido;

    /**
     * La fecha de nacimiento del productor o representante legal.
     */
    @Column(name = "fecha_nacimiento")
    @Temporal(TemporalType.DATE)
    private Calendar fechaNacimiento;

    /**
     * El sexo del productor o representante legal.
     */
    @ManyToOne
    @JoinColumn(name = "sexo_id")
    private Sexo sexo;

    /**
     * La nacionalidad del productor o representante legal.
     */
    @ManyToOne
    @JoinColumn(name = "pais_id")
    private Pais pais;

    /**
     * El tipo de documento de identificación del productor o representante legal.
     */
    @ManyToOne
    @JoinColumn(name = "tipo_documento_id", nullable = false)
    private TipoDocumento tipoDocumento;

    /**
     * El número de documento de identificación del productor o representante legal.
     */
    @Column(name = "numero_documento", nullable = false)
    private String numeroDocumento;

    /**
     * Los socios de la persona moral.
     */
    @OneToMany(mappedBy = "datosProductor")
    private List<SocioDatosProductor> socios;

}
