/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.catalogos;

import mx.gob.segalmex.pluss.modelo.base.EnumCatalogo;

/**
 *
 * @author jurgen
 */
public enum TipoPrecioEnum implements EnumCatalogo {

    PRECIO_ABIERTO_DOLARES("precio-abierto", "Precio abierto (dólares)"),
    PRECIO_ABIERTO_PESOS("precio-abierto-pesos", "Precio abierto (pesos mexicanos)"),
    PRECIO_CERRADO_DOLARES("precio-cerrado", "Precio cerrado (dólares)"),
    PRECIO_CERRADO_PESOS("precio-cerrado-pesos", "Precio cerrado (pesos mexicanos)"),
    PRECIO_REFERENCIA("precio-referencia", "Precio de referencia"),
    NO_DISPONIBLE("no-disponible", "No Disponible");

    private final String clave;

    private final String nombre;

    private TipoPrecioEnum(String clave, String nombre) {
        this.clave = clave;
        this.nombre = nombre;
    }

    @Override
    public String getClave() {
        return clave;
    }

    public String getNombre() {
        return nombre;
    }

    public static TipoPrecioEnum getInstance(String clave) {
        for (TipoPrecioEnum e : TipoPrecioEnum.values()) {
            if (e.getClave().equals(clave)) {
                return e;
            }
        }

        throw new IllegalArgumentException("No existe un tipo de precio con clave: " + clave);
    }

}
