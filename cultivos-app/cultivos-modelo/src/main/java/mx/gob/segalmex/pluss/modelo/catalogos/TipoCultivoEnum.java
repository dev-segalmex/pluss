/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.catalogos;

import mx.gob.segalmex.pluss.modelo.base.EnumCatalogo;

/**
 *
 * @author oscar
 */
public enum TipoCultivoEnum implements EnumCatalogo {

    MAIZ_BLANCO("maiz-blanco", "Maíz blanco"),
    MAIZ_AMARILLO("maiz-amarillo", "Maíz amarillo"),
    ARROZ_GRUESO("arroz-grueso", "Arroz grueso"),
    ARROZ_LARGO("arroz-largo", "Arroz largo"),
    TRIGO_PANIFICABLE("trigo-panificable", "Trigo panificable"),
    TRIGO_CRISTALINO("trigo-cristalino", "Trigo cristalino"),
    TRIGO_SEMILLA("trigo-semilla", "Semilla certificada"),
    TRIGO_SUAVE("trigo-suave", "Trigo Suave");

    private final String clave;

    private final String nombre;

    private TipoCultivoEnum(String clave, String nombre) {
        this.clave = clave;
        this.nombre = nombre;
    }

    @Override
    public String getClave() {
        return clave;
    }

    public String getNombre() {
        return nombre;
    }

    public static TipoCultivoEnum getInstance(String clave) {
        for (TipoCultivoEnum e : TipoCultivoEnum.values()) {
            if (e.getClave().equals(clave)) {
                return e;
            }
        }

        throw new IllegalArgumentException("No existe un tipo cultivo con la clave: " + clave);
    }

}
