/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.productor;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.GrupoIndigena;
import mx.gob.segalmex.pluss.modelo.catalogos.NivelEstudio;
import mx.gob.segalmex.pluss.modelo.catalogos.Parentesco;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoProductor;
import mx.gob.segalmex.pluss.modelo.inscripcion.AbstractInscripcion;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "inscripcion_productor")
@Getter
@Setter
public class InscripcionProductor extends AbstractInscripcion {

    /**
     * La etiqueta de los tiposCultivo seleccionado en los predios.
     */
    @Column(name = "etiqueta_tipo_cultivo", nullable = false)
    private String etiquetaTipoCultivo;

    /**
     * Los datos del productor.
     */
    @ManyToOne
    @JoinColumn(name = "datos_productor_id", nullable = false)
    private DatosProductor datosProductor;

    /**
     * La dirección del productor.
     */
    @ManyToOne
    @JoinColumn(name = "domicilio_id", nullable = false)
    private Domicilio domicilio;

    /**
     * El número de teléfono o celular.
     */
    @Column(name = "numero_telefono")
    private String numeroTelefono;

    /**
     * El correo electrónico de contacto.
     */
    @Column(name = "correo_electronico")
    private String correoElectronico;

    /**
     * La lista de contratos firmados del productor.
     */
    @OneToMany(mappedBy = "inscripcionProductor")
    @OrderBy("orden ASC")
    private List<ContratoFirmado> contratos;

    /**
     * La lista de predios del productor.
     */
    @OneToMany(mappedBy = "inscripcionProductor")
    @OrderBy("orden ASC")
    private List<PredioProductor> predios;

    /**
     * La lista de empresas asociadas con el productor.
     */
    @OneToMany(mappedBy = "inscripcionProductor")
    @OrderBy("orden ASC")
    private List<EmpresaAsociada> empresas;

    /**
     * Los datos de la cuenta bancaria.
     */
    @ManyToOne
    @JoinColumn(name = "cuenta_bancaria_id", nullable = false)
    private CuentaBancaria cuentaBancaria;

    /**
     * El nombre del beneficiario.
     */
    @Column(name = "nombre_beneficiario")
    private String nombreBeneficiario;

    /**
     * Los apellidos del beneficiario.
     */
    @Column(name = "apellidos_beneficiario")
    private String apellidosBeneficiario;

    /**
     * El curo del beneficiario.
     */
    @Column(name = "curp_beneficiario", nullable = false, length = 63)
    private String curpBeneficiario;

    /**
     * El parentesco del beneficiario.
     */
    @ManyToOne
    @JoinColumn(name = "parentesco_id")
    private Parentesco parentesco;

    /**
     * Indica si el beneficiario ya ha sidoregistrado en otra
     * {@link InscripcionProductor}.
     */
    @Column(name = "beneficiario_duplicado")
    private Boolean beneficiarioDuplicado;

    /**
     * El nombre del encargado del registro.
     */
    @Column(name = "encargado", nullable = false)
    private String encargado;

    /**
     * Indica si el registro se puede completar de manera forzada.
     */
    @Column(name = "forzada")
    private boolean forzada = false;

    /**
     * El número de contratos del productor. Debera coincidir con la lista de
     * contratos, excepto en los casos en que se autorice agregar o eliminar
     * contratos.
     */
    @Column(name = "numero_contratos", nullable = false)
    private int numeroContratos;

    /**
     * El número de predios del productor. Deberá coincidir con la lista de
     * predios, excepto en los casos en que se autorice agregar o eliminar
     * contratos.
     */
    @Column(name = "numero_predios", nullable = false)
    private int numeroPredios;

    /**
     * El número de empresas del productor. Deberá coincidir con la lista de
     * empresas, excepto en los casos en que se autorice agregar o eliminar
     * empresas.
     */
    @Column(name = "numero_empresas", nullable = false)
    private int numeroEmpresas;

    /**
     * Los diferentes tipos de cultivos seleccionados en la inscripción.
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "inscripcion_prod_tipo_cultivo", joinColumns = {
        @JoinColumn(name = "inscripcion_prod_id", referencedColumnName = "id")},
            inverseJoinColumns = {
                @JoinColumn(name = "tipo_cultivo_id", referencedColumnName = "id")})
    private List<TipoCultivo> tiposCultivo;

    /**
     * La información de la semilla. NOTA: Solo es capturada en Trigo.
     */
    @ManyToOne
    @JoinColumn(name = "informacion_semilla_id")
    private InformacionSemilla informacionSemilla;

    /**
     * Tipo registro persona-fisica, persona-moral, asociado
     */
    @Column(name = "tipo_registro", nullable = false, length = 63)
    private String tipoRegistro;

    /**
     * La lista de molinos de esta inscripcion.
     */
    @OneToMany(mappedBy = "inscripcionProductor")
    @OrderBy("orden ASC")
    private List<InscripcionMolino> molinos;

    /**
     * El estado donde la {@link InscripcionProductor} tiene más superficie.
     */
    @ManyToOne
    @JoinColumn(name = "estado_id")
    private Estado estado;

    /**
     * Lista Transient de las facturas asociadas al productor de lesta
     * InscripcionProductor.
     */
    @Transient
    private List<InscripcionFactura> facturas;

    /**
     * En caso de tener un preReistro, aquí se guarda el folio de ese
     * preRegistro.
     */
    @Column(name = "folio_pre_registro", length = 63)
    private String folioPreRegistro;

    /**
     * El grupo indigena al que pertenece esste {
     *
     * @llink InscripcionProductor}
     */
    @ManyToOne
    @JoinColumn(name = "grupo_indigena_id", nullable = false)
    private GrupoIndigena grupoIndigena;

    /**
     * El nivel de estudios que posee el productor.
     */
    @ManyToOne
    @JoinColumn(name = "nivel_estudio_id", nullable = false)
    private NivelEstudio nivelEstudio;

    /**
     * El tipo de productor(mediano.pequeno o pequeno-no-empadronado).
     */
    @ManyToOne
    @JoinColumn(name = "tipo_productor_id", nullable = false)
    private TipoProductor tipoProductor;

    @Transient
    public List<ContratoFirmado> getContratosActivos() {
        List<ContratoFirmado> activos = new ArrayList<>();
        for (ContratoFirmado c : contratos) {
            if (Objects.isNull(c.getFechaCancelacion())) {
                activos.add(c);
            }
        }
        return activos;
    }

    @Transient
    public List<PredioProductor> getPrediosActivos() {
        List<PredioProductor> activos = new ArrayList<>();
        for (PredioProductor p : predios) {
            if (Objects.isNull(p.getFechaCancelacion())) {
                activos.add(p);
            }
        }
        return activos;
    }

    @Transient
    public List<EmpresaAsociada> getEmpresasActivas() {
        List<EmpresaAsociada> activas = new ArrayList<>();
        for (EmpresaAsociada e : empresas) {
            if (Objects.isNull(e.getFechaCancelacion())) {
                activas.add(e);
            }
        }
        return activas;
    }

}
