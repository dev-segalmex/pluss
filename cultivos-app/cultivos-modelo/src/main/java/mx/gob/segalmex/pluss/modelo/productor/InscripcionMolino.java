/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.productor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;

/**
 * Clase para manter la relación de todos los molinos seleccionados en el
 * registro de una {@link InscripcionProductor}.
 *
 * @author oscar
 */
@Entity
@Table(name = "inscripcion_molino")
@Getter
@Setter
public class InscripcionMolino extends AbstractEntidad {

    /**
     * La {@link InscripcionProductor} a la que pertenece esta
     * {@link InscripcionMolino}.
     */
    @ManyToOne
    @JoinColumn(name = "inscripcion_productor_id", nullable = false)
    private InscripcionProductor inscripcionProductor;

    /**
     * El molino seleccionado en el registro.
     */
    @ManyToOne
    @JoinColumn(name = "molino_id", nullable = false)
    private Sucursal molino;

    /**
     * El orden de los molinos seleccionados.
     */
    @Column(name = "orden", nullable = false)
    private int orden;

}
