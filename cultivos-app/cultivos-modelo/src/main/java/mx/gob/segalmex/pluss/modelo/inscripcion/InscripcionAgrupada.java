/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.inscripcion;

import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;

/**
 * Entidad para obtener una consulta agrupada de las inscripciones.
 *
 * @author oscar
 */
@Getter
@Setter
public class InscripcionAgrupada {

    private CicloAgricola ciclo;

    private EstatusInscripcion estatus;

    private Estado estado;

    private Integer total;
}
