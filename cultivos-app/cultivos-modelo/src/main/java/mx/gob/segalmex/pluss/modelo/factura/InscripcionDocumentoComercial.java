/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.factura;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.inscripcion.AbstractInscripcion;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "inscripcion_documento_comercial")
@Getter
@Setter
public class InscripcionDocumentoComercial extends AbstractInscripcion {

    /**
     * La cantidad de toneladas de las que hace uso esta factura.
     */
    @Column(name = "cantidad", precision = 19, scale = 3)
    private BigDecimal cantidad;

    /**
     * El tipo de documento comercial.
     */
    @Column(name = "tipo", length = 255)
    private String tipo;

}
