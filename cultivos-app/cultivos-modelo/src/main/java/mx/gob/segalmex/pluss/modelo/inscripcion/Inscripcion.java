/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.inscripcion;

import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.historico.EntidadHistorico;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;

/**
 *
 * @author ismael
 */
public interface Inscripcion extends EntidadHistorico {

    EstatusInscripcion getEstatus();

    void setEstatus(EstatusInscripcion estatus);

    String getFolio();

    void setFolio(String folio);

    void setUuid(String uuid);

    Usuario getUsuarioRegistra();

    void setUsuarioRegistra(Usuario usuarioRegistra);

    Usuario getUsuarioAsignado();

    void setUsuarioAsignado(Usuario usuarioAsignado);

    Usuario getUsuarioValidador();

    void setUsuarioValidador(Usuario usuarioValidador);

    Cultivo getCultivo();

    void setCultivo(Cultivo cultivo);

    CicloAgricola getCiclo();

    void setCiclo(CicloAgricola ciclo);

    Sucursal getSucursal();

    void setSucursal(Sucursal sucursal);

    String getRfcEmpresa();

    void setRfcEmpresa(String rfcEmpresa);

    String getComentarioEstatus();

    void setComentarioEstatus(String comentarioEstatus);

    EstatusInscripcion getEstatusPrevio();

    void setEstatusPrevio(EstatusInscripcion estatusPrevio);
}
