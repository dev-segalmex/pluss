/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.pago;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "requerimiento_pago")
@Getter
@Setter
public class RequerimientoPago extends AbstractEntidad {

    /**
     * El uuid de este requerimiento de pago.
     */
    @Column(name = "uuid", nullable = false)
    private String uuid;

    /**
     * El usuario que genera este requerimiento de pago.
     */
    @ManyToOne
    @JoinColumn(name = "usuario_id", nullable = false)
    private Usuario usuario;

    /**
     * El estatus del requerimiento.
     */
    @Column(name = "estatus")
    private String estatus;

    /**
     * El número de cegap asignado.
     */
    @Column(name = "cegap")
    private String cegap;

    /**
     * La fecha en que se envía el requerimiento hacia el sistema de finanzas.
     */
    @Column(name = "fecha_envio")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fechaEnvio;

    /**
     * Los requerimientos de pago para cada uso factura.
     */
    @OneToMany(mappedBy = "requerimiento")
    private List<RequerimientoUsoFactura> requerimientos;

    /**
     * El nombre del layout.
     */
    @Column(name = "layout", nullable = false)
    private String layout;

    /**
     * La clave de la unidad operativa que envía el pago.
     */
    @Column(name = "clave_unidad_operativa", nullable = false)
    private String claveUnidadOperativa;

    /**
     * El número de cuenta bancaria de la cual se obtiene los fondos.
     */
    @Column(name = "numero_cuenta", nullable = false)
    private String numeroCuenta;

    /**
     * La clave del área la cual hace el requerimiento de pago.
     */
    @Column(name = "clave_area", nullable = false)
    private String claveArea;

    /**
     * La clave de usuario el cual hace el requerimiento de pago.
     */
    @Column(name = "clave_usuario", nullable = false)
    private String claveUsuario;

    /**
     * La clave del programa.
     */
    @Column(name = "programa", nullable = false)
    private String programa;

    /**
     * La clave del centro de acopio.
     */
    @Column(name = "clave_centro_acopio")
    private String claveCentroAcopio;

    /**
     * La propiedad para monitorear los requerimientos de pago nuevos.
     */
    @Column(name = "monitoreable")
    private String monitoreable;

    /**
     * El número de facturas a pagar en este requerimiento de pago.
     */
    @Column(name = "facturas", nullable = false)
    private int facturas = 0;

    /**
     * El total de las toneladas de este requerimiento de pago.
     */
    @Column(name = "toneladas_totales", scale = 3, nullable = false)
    private BigDecimal toneladasTotales = BigDecimal.ZERO;

    /**
     * El monto total de este requerimiento de pago.
     */
    @Column(name = "monto_total", scale = 3, nullable = false)
    private BigDecimal montoTotal = BigDecimal.ZERO;

    /**
     * El número de usos factura que no fueron pagados.
     */
    @Column(name = "facturas_no_pago", nullable = false)
    private int facturasNoPago = 0;

    /**
     * EL total de toneladas que no fueron pagados.
     */
    @Column(name = "toneladas_no_pago", scale = 3, nullable = false)
    private BigDecimal toneladasNoPago = BigDecimal.ZERO;

    /**
     * El total de pesos que no fueron pagados.
     */
    @Column(name = "monto_total_no_pago", nullable = false)
    private BigDecimal montoTotalNoPago = BigDecimal.ZERO;

    /**
     * El ciclo al que pertenece este {@link RequerimientoPago}.
     */
    @ManyToOne
    @JoinColumn(name = "ciclo_id", nullable = false)
    private CicloAgricola ciclo;

    /**
     * El cultivo al que pertenece este {@link RequerimientoPago}.
     */
    @ManyToOne
    @JoinColumn(name = "cultivo_id", nullable = false)
    private Cultivo cultivo;

    /**
     * Indica si se requiere o ya se realizó un reporte del requerimineto.
     */
    @Column(name = "reporte_generado")
    private String reporteGenerado;

}
