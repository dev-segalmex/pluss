/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.factura;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractCatalogo;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "estatus_uso_factura")
@Getter
@Setter
public class EstatusUsoFactura extends AbstractCatalogo {

    /**
     * El factor por el cual se debe de multiplicar el total.
     */
    @Column(name = "factor", nullable = false)
    private BigDecimal factor = BigDecimal.ONE;

}
