/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.productor;

import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.inscripcion.EntidadCancelable;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "empresa_asociada")
@Getter
@Setter
public class EmpresaAsociada extends AbstractEntidad implements EntidadCancelable {

    /**
     * La información de InscripcionProductor.
     */
    @ManyToOne
    @JoinColumn(name = "inscripcion_productor_id", nullable = false)
    private InscripcionProductor inscripcionProductor;

    /**
     * El nombre de la empresa asociada.
     */
    @Column(name = "nombre", nullable = false)
    private String nombre;

    /**
     * El RFC de la empresa asociada.
     */
    @Column(name = "rfc", nullable = false)
    private String rfc;

    /**
     * El orden en el que fue agregada.
     */
    @Column(name = "orden", nullable = false)
    int orden;

    /**
     * La fecha de cancaleción de la empresa.
     */
    @Column(name = "fecha_cancelacion", nullable = true)
    private Calendar fechaCancelacion;

    /**
     * Indica si el empresa asociada fue corregida durante el proceso de
     * validación.
     */
    @Transient
    private boolean corregida = false;

}
