package mx.gob.segalmex.pluss.modelo.pago;

import java.math.BigDecimal;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;

/**
 *
 * @author oscar
 */
@Entity
@Table(name = "estimulo")
@Getter
@Setter
public class Estimulo extends AbstractEntidad {

    @ManyToOne
    @JoinColumn(name = "ciclo_id", nullable = false)
    private CicloAgricola ciclo;

    @ManyToOne
    @JoinColumn(name = "estado_id", nullable = false)
    private Estado estado;

    @Column(name = "fecha_inicio", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fechaInicio;

    @Column(name = "fecha_fin", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fechaFin;

    @Column(name = "cantidad", precision = 19, scale = 3)
    private BigDecimal cantidad;

    /**
     * La fecha de la baja del Estímulo, o "--" sí esta activo.
     */
    @Column(name = "estatus_fecha_baja", nullable = false, length = 63)
    private String estatusFechaBaja = "--";

    @Column(name = "orden", nullable = false)
    private int orden;

    @Column(name = "aplica_estimulo", nullable = false)
    private String aplicaEstimulo;

    /**
     * El tipo (factura o contrato) de estímulo.
     */
    @Column(name = "tipo", nullable = false)
    private String tipo;

    /**
     * El número de contrato al que se debe aplicar este estímulo. Debe haber un
     * número contrato si el tipo es "contrato".
     */
    @Column(name = "contrato", nullable = false)
    private String contrato = "--";

    @Column(name = "tipo_productor", length = 63)
    private String tipoProductor;

}
