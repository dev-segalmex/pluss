/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.pago;

import mx.gob.segalmex.pluss.modelo.base.EnumCatalogo;

/**
 *
 * @author ismael
 */
public enum EstatusRequerimientoPagoEnum implements EnumCatalogo {

    NUEVO("nuevo"),

    APROBADO("aprobado"),

    ENVIADO("enviado"),

    CANCELADO("cancelado"),

    PAGADO("pagado");

    private final String clave;

    private EstatusRequerimientoPagoEnum(String clave) {
        this.clave = clave;
    }

    @Override
    public String getClave() {
        return clave;
    }


    public static EstatusRequerimientoPagoEnum getInstance(String clave) {
        for (EstatusRequerimientoPagoEnum e : EstatusRequerimientoPagoEnum.values()) {
            if (e.getClave().equals(clave)) {
                return e;
            }
        }

        throw new IllegalArgumentException("No existe un EstatusRequerimientoPagoEnum con clave: " + clave);
    }

}
