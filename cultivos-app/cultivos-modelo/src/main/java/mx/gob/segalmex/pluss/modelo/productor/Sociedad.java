/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.productor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractCatalogo;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "sociedad")
@Getter
@Setter
public class Sociedad extends AbstractCatalogo {

    /**
     * El folio de la primer inscripción que generó esta sociedad.
     */
    @Column(name = "folio", nullable = false, length = 63)
    private String folio;

}
