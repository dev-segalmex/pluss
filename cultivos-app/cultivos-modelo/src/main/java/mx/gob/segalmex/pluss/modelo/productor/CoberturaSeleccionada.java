/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.productor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoIar;

/**
 * Clase que mantiene la relación de las coberturas seleccionadas en un
 * {@link PreRegistroProductor}.
 *
 * @author oscar
 */
@Entity
@Table(name = "cobertura_seleccionada")
@Getter
@Setter
public class CoberturaSeleccionada extends AbstractEntidad {

    /**
     * El {@link PreRegistroProductor} al que pertenece esta
     * {@link CoberturaSeleccionada}.
     */
    @ManyToOne
    @JoinColumn(name = "pre_registro_id", nullable = false)
    private PreRegistroProductor preRegistro;

    /**
     * La {@link EntidadCobertura} seleccionada en el pre-registro.
     */
    @ManyToOne
    @JoinColumn(name = "entidad_cobertura_id", nullable = false)
    private EntidadCobertura entidadCobertura;

    /**
     * El orden de la {@link CoberturaSeleccionada}.
     */
    @Column(name = "orden", nullable = false)
    private Integer orden;

    /**
     * El tipo de IAR.
     */
    @ManyToOne
    @JoinColumn(name = "tipo_iar_id", nullable = false)
    private TipoIar tipoIar;

    /**
     * LA cantidad de coberturas.
     */
    @Column(name = "cantidad", nullable = false)
    private Integer cantidad;

}
