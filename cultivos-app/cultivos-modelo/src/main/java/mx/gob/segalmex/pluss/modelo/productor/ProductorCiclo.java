/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.productor;

import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoProductor;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "productor_ciclo")
@Getter
@Setter
public class ProductorCiclo extends AbstractEntidad {

    /**
     * El folio del productor en el ciclo y cultivo especificado.
     */
    @Column(name = "folio", nullable = false)
    private String folio;

    /**
     * El productor validado positivamente.
     */
    @ManyToOne
    @JoinColumn(name = "productor_id", nullable = false)
    private Productor productor;

    /**
     * El ciclo agrícola en el que está registrado.
     */
    @ManyToOne
    @JoinColumn(name = "ciclo_id", nullable = false)
    private CicloAgricola ciclo;

    /**
     * El cultivo en el que está registrado.
     */
    @ManyToOne
    @JoinColumn(name = "cultivo_id", nullable = false)
    private Cultivo cultivo;

    /**
     * Indica si este productor ciclo se encuentra activo.
     */
    @Column(name = "activo")
    private boolean activo = true;

    /**
     * El número de inscripciones.
     */
    @Column(name = "inscripciones", nullable = false)
    private int inscripciones = 0;

    /**
     * El número de predios.
     */
    @Column(name = "predios", nullable = false)
    private int predios = 0;

    /**
     * Las hectáreas totales declaradas con base a sus predios.
     */
    @Column(name = "hectareas_totales", nullable = false, precision = 19, scale = 3)
    private BigDecimal hectareasTotales = BigDecimal.ZERO;

    /**
     * Las toneladas totales declaradas con base a sus predios.
     */
    @Column(name = "toneladas_totales", nullable = false, precision = 19, scale = 3)
    private BigDecimal toneladasTotales = BigDecimal.ZERO;

    /**
     * Las toneladas facturadas.
     */
    @Column(name = "toneladas_facturadas", nullable = false, precision = 19, scale = 3)
    private BigDecimal toneladasFacturadas = BigDecimal.ZERO;

    /**
     * Las toneladas que se comprometieron en un contrato.
     */
    @Column(name = "toneladas_contratadas", nullable = false, precision = 19, scale = 3)
    private BigDecimal toneladasContratadas = BigDecimal.ZERO;

    /**
     * Las toneladas que cuentan con cobertura.
     */
    @Column(name = "toneladas_cobertura", nullable = false, precision = 19, scale = 3)
    private BigDecimal toneladasCobertura = BigDecimal.ZERO;

    /**
     * La lista
     */
    @OneToMany(mappedBy = "productorCiclo")
    private List<ProductorCicloCultivo> cultivos;

    /**
     * La lista de grupos.
     */
    @OneToMany(mappedBy = "productorCiclo")
    private List<ProductorCicloGrupo> grupos;

    /**
     * El tipo de productor(mediano.pequeno o pequeno-no-empadronado).
     */
    @ManyToOne
    @JoinColumn(name = "tipo_productor_id", nullable = false)
    private TipoProductor tipoProductor;

}
