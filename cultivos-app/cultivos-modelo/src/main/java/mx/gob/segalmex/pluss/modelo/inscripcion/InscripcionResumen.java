package mx.gob.segalmex.pluss.modelo.inscripcion;

import lombok.Getter;
import lombok.Setter;

/**
 * Entidad para obtener una consulta resumen de las inscripciones por estatus.
 *
 * @author oscar
 */
@Getter
@Setter
public class InscripcionResumen {

    private String cultivo;
    private String estatus;
    private String ciclo;
    private String tipo;
    private Integer total;

}
