/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.factura;

import java.math.BigDecimal;
import java.util.Calendar;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author oscar
 */
@Getter
@Setter
public class InscripcionFacturaSimple {

    private String folio;

    private Calendar fechaCreacion;

    private String cultivo;

    private String ciclo;

    private String estado;

    private String nombreEmpresa;

    private String rfcEmpresa;

    private String tipoSucursal;

    private String contrato;

    /**
     * Productor o global
     */
    private String tipoFactura;

    /**
     * Indica si tiene productorCiclo.
     */
    private boolean productorCiclo;

    /**
     * Física o moral solo en caso de tipoFactura productorCiclo.
     */
    private String claveTipoPersona;

    /**
     * Física o moral solo en caso de tipoFactura productorCiclo.
     */
    private String nombreTipoPersona;

    /**
     * En caso de persona física.
     */
    private String personaClave;

    /**
     * Para morales y físicas.
     */
    private String personaRfc;

    /**
     * Para física.
     */
    private String personaNombre;

    /**
     * Para física.
     */
    private String personaApellido;

    /**
     * Para morales.
     */
    private String productorNombre;

    private String cfdiNombreEmisor;

    private String cfdiRfcEmisor;

    private String cfdiNombreReceptor;

    private String cfdiRfcReceptor;

    private Calendar cfdiFecha;

    private String cfdiMoneda;

    private BigDecimal cfdiTotal;

    private Calendar cfdiFechaTimbrado;

    private String cfdiUuidTimbreFiscal;

    private BigDecimal cfdiTotalToneladas;

    private String estatusNombre;

    private String comentarioEstatus;

    private String nombreUsuarioRegistra;

    private String nombreUsuarioValidador;

}
