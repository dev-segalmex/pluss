package mx.gob.segalmex.pluss.modelo.secuencias;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author oscar
 */
@Entity
@Table(name = "seq_folio_informacion_alerta")
@Getter
@Setter
public class SecuenciaFolioInformacionAlerta implements Secuencia {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
}
