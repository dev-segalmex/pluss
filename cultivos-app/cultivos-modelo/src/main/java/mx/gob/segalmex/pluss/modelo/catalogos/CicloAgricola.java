package mx.gob.segalmex.pluss.modelo.catalogos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractCatalogo;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "ciclo_agricola")
@Getter
@Setter
public class CicloAgricola extends AbstractCatalogo {

    @Column(name = "abreviatura")
    private String abreviatura;

}
