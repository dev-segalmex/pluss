/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.pago;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.factura.UsoFactura;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "requerimiento_uso_factura")
@Getter
@Setter
public class RequerimientoUsoFactura extends AbstractEntidad {

    /**
     * El uso de factura que se va a pagar.
     */
    @ManyToOne
    @JoinColumn(name = "uso_factura_id", nullable = false)
    private UsoFactura usoFactura;

    /**
     * El requerimiento de pago al que está asociado el uso de la factura.
     */
    @ManyToOne
    @JoinColumn(name = "requerimiento_id", nullable = false)
    private RequerimientoPago requerimiento;

}
