/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.productor;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "productor_ciclo_cultivo")
@Getter
@Setter
public class ProductorCicloCultivo extends AbstractEntidad {

    /**
     * El productor y ciclo al que pertenece el registro de su producción.
     */
    @ManyToOne
    @JoinColumn(name = "productor_ciclo_id", nullable = false)
    private ProductorCiclo productorCiclo;

    /**
     * El cultivo al que pertenece la producción.
     */
    @ManyToOne
    @JoinColumn(name = "cultivo_id", nullable = false)
    private Cultivo cultivo;

    /**
     * El tipo de cultivo al que pertenece su producción.
     */
    @ManyToOne
    @JoinColumn(name = "tipo_cultivo_id", nullable = false)
    private TipoCultivo tipoCultivo;

    /**
     * El estado al que pertenece su producción.
     */
    @ManyToOne
    @JoinColumn(name = "estado_id", nullable = false)
    private Estado estado;

    /**
     * El número de predios del productor del tipo de cultivo indicado.
     */
    @Column(name = "predios", nullable = false)
    private int predios = 0;

    /**
     * El total de hectáreas de tipo de cultivo indicado.
     */
    @Column(name = "hectareas", nullable = false, precision = 19, scale = 3)
    private BigDecimal hectareas = BigDecimal.ZERO;

    /**
     * El total de toneladas del tipo de cultivo indicado.
     */
    @Column(name = "toneladas", nullable = false, precision = 19, scale = 3)
    private BigDecimal toneladas = BigDecimal.ZERO;

    /**
     * El total de toneladas facturas del tipo de cultivo indicado.
     */
    @Column(name = "toneladas_facturadas", nullable = false, precision = 19, scale = 3)
    private BigDecimal toneladasFacturadas = BigDecimal.ZERO;

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", this.id)
                .append("cultivo", this.cultivo)
                .append("tipoCultivo", this.tipoCultivo)
                .append("estado", this.estado)
                .append("predios", this.predios)
                .toString();
    }

}
