/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.empresas;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;

/**
 * Entidad mediante la cual se realiza el registro de empresa básico.
 *
 * @author ismael
 */
@Entity
@Table(name = "registro_empresa_basico")
@Getter
@Setter
public class RegistroEmpresaBasico extends AbstractEntidad {

    /**
     * El RFC de la empresa que se registra.
     */
    @Column(name = "rfc", nullable = false)
    private String rfc;

    /**
     * El ciclo agrícola en el que se registra.
     */
    @ManyToOne
    @JoinColumn(name = "ciclo_id", nullable = false)
    private CicloAgricola ciclo;

    /**
     * El cultivo en el que se registra.
     */
    @ManyToOne
    @JoinColumn(name = "cultivo_id", nullable = false)
    private Cultivo cultivo;

    /**
     * El uuid del registro.
     */
    @Column(name = "uuid", nullable = false)
    private String uuid;

    /**
     * Indica si el registro es editable.
     */
    @Column(name = "editable", nullable = false)
    private boolean editable;

    /**
     * El usuario que realizó el registro.
     */
    @ManyToOne
    @JoinColumn(name = "usuario_id", nullable = false)
    private Usuario usuario;

}
