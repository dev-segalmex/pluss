/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.productor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "asociado")
@Getter
@Setter
public class Asociado extends AbstractEntidad {

    /**
     * El rfc del asociado.
     */
    @Column(name = "rfc")
    private String rfc;
    /**
     * La CURP del asociado.
     */
    @Column(name = "curp")
    private String curp;

    /**
     * El nombre del asociado.
     */
    @Column(name = "nombre")
    private String nombre;

    /**
     * El tipo de relación del asociado(representante,socio o productor).
     */
    @Column(name = "relacion", nullable = false)
    private String relacion;

    /**
     * Determina si el asociado está activo.
     */
    @Column(name = "activo")
    private boolean activo = true;

    /**
     * La información de la sociedad.
     */
    @ManyToOne
    @JoinColumn(name = "sociedad_id", nullable = false)
    private Sociedad sociedad;

    /**
     * El ciclo agrícola del asociado.
     */
    @ManyToOne
    @JoinColumn(name = "ciclo_id")
    private CicloAgricola ciclo;

    /**
     * el cultivo al que pertenece el asociado.
     */
    @ManyToOne
    @JoinColumn(name = "cultivo_id")
    private Cultivo cultivo;

    /**
     * El folio de la primer inscripción que generó esta sociedad.
     */
    @Column(name = "folio", nullable = false, length = 63)
    private String folio;

}
