/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.productor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractCatalogo;
import mx.gob.segalmex.pluss.modelo.personas.Persona;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersona;

/**
 * Entidad que representa un productor identificando los datos generales del
 * mismo. Un productor puede producir distintos granos en diferentes ciclos.
 *
 * @author ismael
 */
@Entity
@Table(name = "productor")
@Getter
@Setter
public class Productor extends AbstractCatalogo {

    /**
     * El tipo de persona que es el productor.
     */
    @ManyToOne
    @JoinColumn(name = "tipo_persona_id", nullable = false)
    private TipoPersona tipoPersona;

    /**
     * El RFC del productor.
     */
    @Column(name = "rfc")
    private String rfc;

    /**
     * Los datos personales del productor o del representante legal.
     */
    @ManyToOne
    @JoinColumn(name = "persona_id")
    private Persona persona;

    /**
     * El CURP del productor.
     */
    @Column(name = "curp")
    private String curp;

    /**
     * Uuid para identificar este productor.
     */
    @Column(name = "uuid", length = 63, nullable = false)
    private String uuid;

}
