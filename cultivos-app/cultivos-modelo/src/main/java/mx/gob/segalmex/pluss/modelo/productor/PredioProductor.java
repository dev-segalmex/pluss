/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.productor;

import java.math.BigDecimal;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.catalogos.LocalidadGeoestadistica;
import mx.gob.segalmex.pluss.modelo.catalogos.Municipio;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoDocumento;
import mx.gob.segalmex.pluss.modelo.inscripcion.EntidadCancelable;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "predio_productor")
@Getter
@Setter
public class PredioProductor extends PredioBasico implements EntidadCancelable {

    /**
     * El folio del predio.
     */
    @Column(name = "folio", nullable = false)
    private String folio;

    /**
     * El tipo de cultivo del predio.
     */
    @ManyToOne
    @JoinColumn(name = "tipo_cultivo_id")
    private TipoCultivo tipoCultivo;

    /**
     * El tipo de posesión del predio.
     */
    @ManyToOne
    @JoinColumn(name = "tipo_posesion_id", nullable = false)
    private TipoPosesion tipoPosesion;

    /**
     * El tipo de documento de posesión del predio.
     */
    @ManyToOne
    @JoinColumn(name = "tipo_documento_posesion", nullable = false)
    private TipoDocumento tipoDocumentoPosesion;

    /**
     * El régimen hídrico del predio.
     */
    @ManyToOne
    @JoinColumn(name = "regimen_hidrico", nullable = false)
    private RegimenHidrico regimenHidrico;

    /**
     * La cantidad de volumen del predio.
     */
    @Column(name = "volumen", precision = 19, scale = 3)
    private BigDecimal volumen;

    /**
     * El rendimiento del predio.
     */
    @Column(name = "rendimiento", precision = 19, scale = 3)
    private BigDecimal rendimiento;

    /**
     * La información del municipio del predio.
     */
    @ManyToOne
    @JoinColumn(name = "muncipio_id")
    private Municipio municipio;

    @Transient
    private LocalidadGeoestadistica catalogoLocalidad;

    /**
     * La clave de la localidad seleccionada.
     */
    @Column(name = "clave_localidad")
    private String claveLocalidad;

    /**
     * La localidad del predio.
     */
    @Column(name = "localidad")
    private String localidad;

    /**
     * La latitud del predio.
     */
    @Column(name = "latitud", precision = 19, scale = 6)
    private BigDecimal latitud;

    /**
     * La longitud del predio.
     */
    @Column(name = "longitud", precision = 19, scale = 6)
    private BigDecimal longitud;

    /**
     * La latitud 1 del predio.
     */
    @Column(name = "latitud_1", precision = 19, scale = 6)
    private BigDecimal latitud1;

    /**
     * La longitud 1 del predio.
     */
    @Column(name = "longitud_1", precision = 19, scale = 6)
    private BigDecimal longitud1;

    /**
     * La latitud 2 del predio.
     */
    @Column(name = "latitud_2", precision = 19, scale = 6)
    private BigDecimal latitud2;

    /**
     * La longitud 2 del predio.
     */
    @Column(name = "longitud_2", precision = 19, scale = 6)
    private BigDecimal longitud2;

    /**
     * La latitud 3 del predio.
     */
    @Column(name = "latitud_3", precision = 19, scale = 6)
    private BigDecimal latitud3;

    /**
     * La longitud 3 del predio.
     */
    @Column(name = "longitud_3", precision = 19, scale = 6)
    private BigDecimal longitud3;

    /**
     * La latitud 4 del predio.
     */
    @Column(name = "latitud_4", precision = 19, scale = 6)
    private BigDecimal latitud4;

    /**
     * La longitud 4 del predio.
     */
    @Column(name = "longitud_4", precision = 19, scale = 6)
    private BigDecimal longitud4;

    /**
     * La información de InscripcionProductor.
     */
    @ManyToOne
    @JoinColumn(name = "inscripcion_productor_id")
    private InscripcionProductor inscripcionProductor;

    /**
     * El orden del predio.
     */
    @Column(name = "orden", nullable = false)
    private int orden;

    /**
     * La fecha de cancalación del predio.
     */
    @Column(name = "fecha_cancelacion")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fechaCancelacion;

    /**
     * La cantidad de volumen solicitado.
     */
    @Column(name = "volumen_solicitado", precision = 19, scale = 3)
    private BigDecimal volumenSolicitado;

    /**
     * El valor del rendimiento solicitado.
     */
    @Column(name = "rendimiento_solicitado", precision = 19, scale = 3)
    private BigDecimal rendimientoSolicitado;

    /**
     * Si tiene rendimiento solicitado.
     */
    @Column(name = "solicitado", nullable = false, length = 63)
    private String solicitado;

    /**
     * La cantidad de rendimiento máximo permitido.
     */
    @Column(name = "rendimiento_maximo", nullable = false, precision = 19, scale = 3)
    private BigDecimal rendimientoMaximo = BigDecimal.ZERO;

    /**
     * Total de archivos Permiso de siembra.
     */
    @Column(name = "cantidad_siembra", nullable = false)
    private int cantidadSiembra = 1;

    /**
     * Total de archivos de pago de riego.
     */
    @Column(name = "cantidad_riego", nullable = false)
    private int cantidadRiego = 1;

    /**
     * Indica si el predio fue corregido durante el proceso de validación.
     */
    @Transient
    private boolean corregido = false;

    /**
     * Indica si se modificó el volumen.
     */
    @Transient
    private boolean volumenCorregido = false;

}
