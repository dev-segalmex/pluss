/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.pago;

/**
 *
 * @author oscar
 */
public enum TipoEstimuloEnum {

    FACTURA("factura"),
    CONTRATO("contrato");

    private final String clave;

    private TipoEstimuloEnum(String clave) {
        this.clave = clave;
    }

    public String getClave() {
        return clave;
    }

    public static TipoEstimuloEnum getInstance(String clave) {
        for (TipoEstimuloEnum e : TipoEstimuloEnum.values()) {
            if (e.getClave().equals(clave)) {
                return e;
            }
        }

        throw new IllegalArgumentException("No existe un TipoEstimuloEnum con clave: " + clave);
    }

}
