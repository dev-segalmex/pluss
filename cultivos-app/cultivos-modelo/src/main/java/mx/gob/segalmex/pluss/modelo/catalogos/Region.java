/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.catalogos;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractCatalogo;

/**
 *
 * @author erikcamacho XD
 */
@Entity
@Table(name = "region")
@Getter
@Setter
public class Region extends AbstractCatalogo {

    @ManyToOne
    @JoinColumn(name = "ciclo_id")
    private CicloAgricola ciclo;

    @ManyToOne
    @JoinColumn(name = "tipo_cultivo_id")
    private TipoCultivo tipoCultivo;

    @ManyToOne
    @JoinColumn(name = "estado_id", nullable = false)
    private Estado estado;

    @ManyToOne
    @JoinColumn(name = "estado_estimulo_id", nullable = false)
    private Estado estadoEstimulo;

}
