/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.catalogos;

/**
 *
 * @author jurgen
 */
public enum TipoRegistroEnum {

    FISICA("fisica", "Persona física"),
    MORAL("moral", "Persona moral"),
    ASOCIADO("asociado", "Asociado");

    private final String clave;

    private final String nombre;

    private TipoRegistroEnum(String clave, String nombre) {
        this.clave = clave;
        this.nombre = nombre;
    }

    public String getClave() {
        return clave;
    }

    public String getNombre() {
        return nombre;
    }

    public static TipoRegistroEnum getInstance(String clave) {
        for (TipoRegistroEnum e : TipoRegistroEnum.values()) {
            if (e.getClave().equals(clave)) {
                return e;
            }
        }

        throw new IllegalArgumentException("No existe un tipo registro con clave : " + clave);
    }
}
