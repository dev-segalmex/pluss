/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.catalogos;

/**
 * Enum con todos los prefijos que se usan en el sistema para armar cadenas de
 * búsqueda.
 *
 * @author oscar
 */
public enum PrefijoEnum {

    SUCURSAL_USUARIO("sucursal_usuario:");

    private String clave;

    private PrefijoEnum(String clave) {
        this.clave = clave;
    }

    public String getClave() {
        return clave;
    }

    public static PrefijoEnum getInstance(String clave) {
        for (PrefijoEnum e : PrefijoEnum.values()) {
            if (e.getClave().equals(clave)) {
                return e;
            }
        }

        throw new IllegalArgumentException("No existe un PrefijoEnum con clave: "
                + clave);
    }

}
