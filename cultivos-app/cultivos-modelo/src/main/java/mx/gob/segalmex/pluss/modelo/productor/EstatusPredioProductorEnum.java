/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.productor;

import mx.gob.segalmex.pluss.modelo.base.EnumCatalogo;

/**
 *
 * @author oscar
 */
public enum EstatusPredioProductorEnum implements EnumCatalogo {

    SOLICITADO("solicitado"),
    NO_SOLICITADO("no-solicitado"),
    APROBADO("aprobado"),
    NO_APROBADO("no-aprobado");

    private final String clave;

    private EstatusPredioProductorEnum(String clave) {
        this.clave = clave;
    }

    @Override
    public String getClave() {
        return clave;
    }

    public static EstatusPredioProductorEnum getInstance(String clave) {
        for (EstatusPredioProductorEnum e : EstatusPredioProductorEnum.values()) {
            if (e.getClave().equals(clave)) {
                return e;
            }
        }

        throw new IllegalArgumentException("No existe un EstatusPredioProductorEnum con clave: " + clave);
    }

}
