/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.contrato;

import java.math.BigDecimal;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;
import mx.gob.segalmex.pluss.modelo.productor.EntidadCobertura;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "cobertura_inscripcion_contrato")
@Getter
@Setter
public class CoberturaInscripcionContrato extends AbstractEntidad {

    /**
     * La información de la InscripcionContrato.
     */
    @ManyToOne
    @JoinColumn(name = "inscripcion_contrato_id", nullable = false)
    private InscripcionContrato inscripcionContrato;

    /**
     * El número de contratos.
     */
    @Column(name = "numero")
    private String numero;

    /**
     * La información de la EntidadCobertura.
     */
    @ManyToOne
    @JoinColumn(name = "entidad_cobertura_id", nullable = false)
    private EntidadCobertura entidadCobertura;

    /**
     * La información del TipoComprador.
     */
    @ManyToOne
    @JoinColumn(name = "tipo_comprador_id", nullable = false)
    private TipoCompradorCobertura tipoComprador;

    /**
     * El nombre del comprador.
     */
    @Column(name = "nombre_comprador", nullable = false)
    private String nombreComprador;

    /**
     * La fecha de compra.
     */
    @Column(name = "fecha_compra", nullable = false)
    @Temporal(TemporalType.DATE)
    private Calendar fechaCompra;

    /**
     * La fecha de vencimiento.
     */
    @Column(name = "fecha_vencimiento", nullable = false)
    @Temporal(TemporalType.DATE)
    private Calendar fechaVencimiento;

    /**
     * La cantidad de contratos.
     */
    @Column(name = "numero_contratos", precision = 19, scale = 3)
    private BigDecimal numeroContratos;

    /**
     * El valor de toneladas por cada contrato.
     */
    @Column(name = "numero_contratos_toneladas", precision = 19, scale = 3)
    private BigDecimal numeroContratosToneladas;

    /**
     * El precio de ejercicio en dólares.
     */
    @Column(name = "precio_ejercicio_dolares", precision = 19, scale = 3)
    private BigDecimal precioEjercicioDolares;

    /**
     * El precio de ejercicio en pesos mexicanos.
     */
    @Column(name = "precio_ejercicio_pesos", precision = 19, scale = 3)
    private BigDecimal precioEjercicioPesos;

    /**
     * El precio de la prima en dólares.
     */
    @Column(name = "prima_dolares", precision = 19, scale = 3)
    private BigDecimal primaDolares;

    /**
     * El precio de la prima en pesos mexicanos.
     */
    @Column(name = "prima_pesos", precision = 19, scale = 3)
    private BigDecimal primaPesos;

    /**
     * El precio de la comisión en dólares.
     */
    @Column(name = "comisiones_dolares", precision = 19, scale = 3)
    private BigDecimal comisionesDolares;

    /**
     * El precio de la comisión en pesos mexicanos.
     */
    @Column(name = "comisiones_pesos", precision = 19, scale = 3)
    private BigDecimal comisionesPesos;

    /**
     * El tipo de operación.
     */
    @ManyToOne
    @JoinColumn(name = "tipo_operacion_id", nullable = false)
    private TipoOperacionCobertura tipoOperacionCobertura;

    /**
     * El orden.
     */
    @Column(name = "orden", nullable = false)
    private int orden;

}
