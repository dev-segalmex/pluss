/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.productor;

import mx.gob.segalmex.pluss.modelo.base.EnumCatalogo;

/**
 *
 * @author ismael
 */
public enum TipoProductorEnum implements EnumCatalogo {

    PEQUENO("pequeno"),
    MEDIANO("mediano"),
    PEQUENO_NO_EMPADRONADO("pequeno-no-empadronado");

    private String clave;

    private TipoProductorEnum(String clave) {
        this.clave = clave;
    }

    @Override
    public String getClave() {
        return clave;
    }

    public static TipoProductorEnum getInstance(String clave) {
        for (TipoProductorEnum e : TipoProductorEnum.values()) {
            if (e.getClave().equals(clave)) {
                return e;
            }
        }

        throw new IllegalArgumentException("No existe un TipoProductorEnum con clave: " + clave);
    }
}
