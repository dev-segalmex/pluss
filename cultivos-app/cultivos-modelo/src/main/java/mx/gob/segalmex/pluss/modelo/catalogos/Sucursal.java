/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.catalogos;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractCatalogo;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "sucursal")
@Getter
@Setter
public class Sucursal extends AbstractCatalogo {

    /**
     * El tipo de sucursal.
     */
    @ManyToOne
    @JoinColumn(name = "tipo_id", nullable = false)
    private TipoSucursal tipo;

    /**
     * La empresa a la cual pertence esta sucursal.
     */
    @ManyToOne
    @JoinColumn(name = "empresa_id", nullable = false)
    private Empresa empresa;

    /**
     * El estado en el cual se encuentra esta sucursal.
     */
    @ManyToOne
    @JoinColumn(name = "estado_id", nullable = false)
    private Estado estado;

    /**
     * El municipio en el cual se encuentra esta sucursal.
     */
    @ManyToOne
    @JoinColumn(name = "municipio_id", nullable = false)
    private Municipio municipio;

    /**
     * La localidad en la cual se encuentra esta sucursal.
     */
    @Column(name = "localidad")
    private String localidad;

    /**
     * La latitud en la cual se encuentra esta sucursal.
     */
    @Column(name = "latitud", nullable = false, precision = 19, scale = 6)
    private BigDecimal latitud;

    /**
     * La longitud en la cual se encuentra esta sucursal.
     */
    @Column(name = "longitud", nullable = false, precision = 19, scale = 6)
    private BigDecimal longitud;

    /**
     * La direccion en la cual se encuentra esta sucursal.
     */
    @Column(name = "direccion", nullable = false)
    private String direccion;

}
