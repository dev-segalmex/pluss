/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.inscripcion;

import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Sucursal;

/**
 *
 * @author ismael
 */
@MappedSuperclass
@Getter
@Setter
public abstract class AbstractInscripcion extends AbstractEntidad implements Inscripcion {

    /**
     * El estatus de la inscripción.
     */
    @ManyToOne
    @JoinColumn(name = "estatus_id", nullable = false)
    protected EstatusInscripcion estatus;

    /**
     * El folio de la inscripción.
     */
    @Column(name = "folio", nullable = false, unique = true, length = 63)
    protected String folio;

    /**
     * El uuid de la inscripción.
     */
    @Column(name = "uuid", nullable = false, length = 63)
    protected String uuid;

    /**
     * El usuario que registra la inscripción.
     */
    @ManyToOne
    @JoinColumn(name = "usuario_registra_id", nullable = false)
    protected Usuario usuarioRegistra;

    /**
     * El usuario que tienen asignada la inscripción para operarla.
     */
    @ManyToOne
    @JoinColumn(name = "usuario_asignado_id")
    private Usuario usuarioAsignado;

    /**
     * El usuario encargado de la validación.
     */
    @ManyToOne
    @JoinColumn(name = "usuario_validador_id")
    private Usuario usuarioValidador;

    /**
     * El cultivo al que pertenece la inscripción.
     */
    @ManyToOne
    @JoinColumn(name = "cultivo_id", nullable = false)
    protected Cultivo cultivo;

    /**
     * El ciclo al que pertenece la inscripción.
     */
    @ManyToOne
    @JoinColumn(name = "ciclo_id", nullable = false)
    protected CicloAgricola ciclo;

    /**
     * La sucursal/ventanilla que realiza la inscripción.
     */
    @ManyToOne
    @JoinColumn(name = "sucursal_id", nullable = false)
    protected Sucursal sucursal;

    /**
     * La bodega/molino
     */
    @ManyToOne
    @JoinColumn(name = "bodega_id", nullable = false)
    protected Sucursal bodega;

    /**
     * El RFC de la empresa a la cual pertenece la sucursal que registra el contrato.
     */
    @Column(name = "rfc_empresa", nullable = false)
    protected String rfcEmpresa;

    /**
     * El comentario para el estatus asignado.
     */
    @Column(name = "comentario_estatus", length = 1023)
    protected String comentarioEstatus;

    /**
     * El estatus previo de la inscripción.
     */
    @ManyToOne
    @JoinColumn(name = "estatus_previo_id", nullable = false)
    protected EstatusInscripcion estatusPrevio;

    /**
     * Clave de los archivos anexados a la inscripcion.
     */
    @Column(name = "clave_archivos", length = 1024)
    protected String claveArchivos;

    /**
     * La fecha de inicio para comenzar a validar la inscripción.
     */
    @Column(name = "fecha_validacion")
    @Temporal(TemporalType.TIMESTAMP)
    protected Calendar fechaValidacion;

    /**
     * La fecha de inicio en que se finalizó por última vez la inscripción.
     */
    @Column(name = "fecha_finalizacion")
    protected Calendar fechaFinalizacion;

}
