/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.productor;

import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;

/**
 * Entidad para guardar los RFCs que a pesar de ser emitidos por el SAT, no
 * cumplen con la validación del dígito verificador. Entonces Se meten ene sta
 * tabla para considerarse válidos.
 *
 * @author oscar
 */
@Entity
@Table(name = "rfc_excepcion")
@Getter
@Setter
public class RfcExcepcion extends AbstractEntidad {

    /**
     * El rfc que debe considerarse válido.
     */
    @Column(name = "rfc", nullable = false, length = 63)
    private String rfc;

    /**
     * El {@link Usuario} que registra esta {@link RfcExcepcion}.
     */
    @ManyToOne
    @JoinColumn(name = "usuario_registra_id", nullable = false)
    private Usuario usuarioRegistra;

    /**
     * El estatus de este {@link RfcExcepcion}. True se considerá válido en otro
     * caso no se toma en cuenta.
     */
    @Column(name = "activo")
    private boolean activo = true;

    /**
     * El archivo PDF con la constancia del RFC.
     */
    @ManyToOne
    @JoinColumn(name = "constancia_id", nullable = false)
    private Archivo constancia;

    /**
     * El usuario que desactiva el {@link RfcExcepcion}.
     */
    @ManyToOne
    @JoinColumn(name = "usuario_baja_id")
    private Usuario usuarioBaja;

    /**
     * La fecha en que se desactiva el {@link RfcExcepcion}.
     */
    @Column(name = "fecha_baja")
    @Temporal(TemporalType.DATE)
    private Calendar fechaBaja;

}
