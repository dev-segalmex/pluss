/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.productor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "socio_datos_productor")
@Getter
@Setter
public class SocioDatosProductor extends AbstractEntidad {

    /**
     * La información de DatosProductor.
     */
    @ManyToOne
    @JoinColumn(name = "datos_productor_id", nullable = false)
    private DatosProductor datosProductor;

    /**
     * La CURP de cada socio.
     */
    @Column(name = "curp", nullable = false)
    private String curp;

}
