package mx.gob.segalmex.pluss.modelo.catalogos;

import javax.persistence.Entity;
import javax.persistence.Table;
import mx.gob.segalmex.pluss.modelo.base.AbstractCatalogo;

/**
 *
 * @author oscar
 */
@Entity
@Table(name = "nivel_estudio")
public class NivelEstudio extends AbstractCatalogo {

}
