/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.factura;

/**
 * Contiene los diferentes tipos de UsoFactura.
 *
 * @author oscar
 */
public enum TipoUsoFacturaEnum {

    /**
     * El tipo por defecto.
     */
    ESTIMULO("estimulo"),
    /**
     * El tipo cuando se tiene que hacer un envío extra.
     */
    COMPLEMENTO("complemento");

    private final String clave;

    private TipoUsoFacturaEnum(String clave) {
        this.clave = clave;
    }

    public String getClave() {
        return clave;
    }

    public static TipoUsoFacturaEnum getInstance(String clave) {
        for (TipoUsoFacturaEnum e : TipoUsoFacturaEnum.values()) {
            if (e.getClave().equals(clave)) {
                return e;
            }
        }

        throw new IllegalArgumentException("No existe un tipo uso factura con la clave: " + clave);
    }

}
