/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.factura;

import mx.gob.segalmex.pluss.modelo.base.EnumCatalogo;

/**
 *
 * @author ismael
 */
public enum EstatusCfdiEnum implements EnumCatalogo {

    VIGENTE("vigente"),

    CANCELADO("cancelado"),

    PENDIENTE("pendiente");

    private final String clave;

    @Override
    public String getClave() {
        return clave;
    }

    private EstatusCfdiEnum(String clave) {
        this.clave = clave;
    }
}
