/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.historico;

import mx.gob.segalmex.pluss.modelo.base.Entidad;

/**
 *
 * @author ismael
 */
public interface EntidadHistorico extends Entidad {

    String getUuid();

}
