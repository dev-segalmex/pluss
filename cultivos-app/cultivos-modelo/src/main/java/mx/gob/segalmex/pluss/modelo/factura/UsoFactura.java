/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.factura;

import java.math.BigDecimal;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.catalogos.Banco;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoPrecio;
import mx.gob.segalmex.pluss.modelo.productor.Productor;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;

/**
 * Clase que representa los usos que se le dan a una factura.
 *
 * @author ismael
 */
@Entity
@Table(name = "uso_factura")
@Getter
@Setter
public class UsoFactura extends AbstractEntidad {

    /**
     * El uuid del UsoFactura.
     */
    @Column(name = "uuid")
    private String uuid;

    /**
     * La factura de la cual se hace uso.
     */
    @ManyToOne
    @JoinColumn(name = "factura_id", nullable = false)
    private Factura factura;

    /**
     * El productor que hace uso de la factura.
     */
    @ManyToOne
    @JoinColumn(name = "productor_id", nullable = false)
    private Productor productor;

    /**
     * El productor y ciclo en el que se hace uso de la factura.
     */
    @ManyToOne
    @JoinColumn(name = "productor_ciclo_id", nullable = false)
    private ProductorCiclo productorCiclo;

    /**
     * El estatus del uso factura.
     */
    @ManyToOne
    @JoinColumn(name = "estatus_id", nullable = false)
    private EstatusUsoFactura estatus;

    /**
     * La toneladas de las que se hace uso en la factura.
     */
    @Column(name = "toneladas", scale = 3)
    private BigDecimal toneladas;

    /**
     * El folio de la inscripción que dio origen a este uso factura.
     */
    @Column(name = "folio", nullable = false)
    private String folio;

    /**
     * El tipo de cultivo de este UsoFactura. Este dato se copia a partir de la
     * InscripcionFactura.
     */
    @ManyToOne
    @JoinColumn(name = "tipo_cultivo_id", nullable = false)
    private TipoCultivo tipoCultivo;

    /**
     * El estímulo por tonelada para este uso de factura.
     */
    @Column(name = "estimulo_tonelada", scale = 3, nullable = false)
    private BigDecimal estimuloTonelada;

    /**
     * El estímulo total por las toneladas para este uso de factura.
     */
    @Column(name = "estimulo_total", scale = 3, nullable = false)
    private BigDecimal estimuloTotal;

    @ManyToOne
    @JoinColumn(name = "tipo_id", nullable = false)
    private TipoUsoFactura tipo;

    /**
     *
     * Estado al cual pertenece el uso_factura
     */
    @ManyToOne
    @JoinColumn(name = "estado_id", nullable = false)
    private Estado estado;

    /**
     *
     * Comentario del estatus
     */
    @Column(name = "comentario_estatus")
    private String comentarioEstatus;

    /**
     * El orden del estímulo que le tocó a este UsoFactura.
     */
    @Column(name = "orden_estimulo", nullable = false)
    private int ordenEstimulo;

    /**
     * Banco de pago de este UsoFactura.
     */
    @ManyToOne
    @JoinColumn(name = "banco_id")
    private Banco banco;

    /**
     * La clabe a donde se pagará este UsoFactura.
     */
    @Column(name = "clabe", length = 63)
    private String clabe;

    /**
     * La superficie del predio correspondiente a este Uso.
     */
    @Column(name = "superficie_predio", precision = 19, scale = 3)
    private BigDecimal superficiePredio;

    /**
     * El contrato al que está asignada la factura.
     */
    @Column(name = "contrato")
    private String contrato;

    /**
     * El tipo de precio contratado.
     */
    @ManyToOne
    @JoinColumn(name = "tipo_precio_id")
    private TipoPrecio tipoPrecio;

    /**
     * La fecha con la que se paga este elemento.
     */
    @Column(name = "fecha_estimulo", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fechaEstimulo;

}
