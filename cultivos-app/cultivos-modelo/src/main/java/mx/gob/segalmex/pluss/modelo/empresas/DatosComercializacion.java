/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.empresas;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.Municipio;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "datos_comercializacion")
@Getter
@Setter
public class DatosComercializacion extends AbstractEntidad {

    /**
     * El registro de empresa al que pertenecen los datos de comercialización.
     */
    @ManyToOne
    @JoinColumn(name = "registro_empresa_id", nullable = false)
    private RegistroEmpresa registroEmpresa;

    /**
     * El tipo según corresponda de compra o venta.
     */
    @Column(name = "tipo", nullable = false)
    private String tipo;

    /**
     * El nombre del actor.
     */
    @Column(name = "nombre_actor", nullable = false)
    private String nombreActor;

    /**
     * El año de la comercialización.
     */
    @Column(name = "anio", nullable = false)
    private int anio;

    /**
     * La cantidad comercializada.
     */
    @Column(name = "cantidad", nullable = false, precision = 19, scale = 4)
    private BigDecimal cantidad;

    /**
     * El estado del que se realiza la comercialización.
     */
    @ManyToOne
    @JoinColumn(name = "estaod_id", nullable = false)
    private Estado estado;

    /**
     * El municipio del que se realiza la comercialización.
     */
    @ManyToOne
    @JoinColumn(name = "municipio_id", nullable = false)
    private Municipio municipio;

    /**
     * El orden los datos de comercialización.
     */
    @Column(name = "orden", nullable = false)
    private int orden;

}
