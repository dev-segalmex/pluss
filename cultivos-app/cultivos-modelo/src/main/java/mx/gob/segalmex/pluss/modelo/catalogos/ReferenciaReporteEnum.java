/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.catalogos;

import mx.gob.segalmex.pluss.modelo.base.EnumCatalogo;

/**
 *
 * @author oscar
 */
public enum ReferenciaReporteEnum implements EnumCatalogo {

    PRODUCTOR("pppp-pppp-pppp-pppp"),

    CONTRATO("cccc-cccc-cccc-cccc"),

    FACTURA("ffff-ffff-ffff-ffff"),

    PRODUCTOR_PREDIO("prpr-prpr-prpr-prpr"),

    PRODUCTOR_CONTRATO("pcpc-pcpc-pcpc-pcpc"),

    PREDIO_PLANEACION("plpl-plpl-plpl-plpl"),

    PAGO_PLANEACION("pgpl-pgpl-pgpl-pgpl"),

    PRE_REGISTRO("pre-registro-pre-registro");

    private final String clave;

    private ReferenciaReporteEnum(String clave) {
        this.clave = clave;
    }

    @Override
    public String getClave() {
        return clave;
    }

    public static ReferenciaReporteEnum getInstance(String clave) {
        for (ReferenciaReporteEnum e : ReferenciaReporteEnum.values()) {
            if (e.getClave().equals(clave)) {
                return e;
            }
        }

        throw new IllegalArgumentException("No existe un Referencia Reporte con la clave: " + clave);
    }

}
