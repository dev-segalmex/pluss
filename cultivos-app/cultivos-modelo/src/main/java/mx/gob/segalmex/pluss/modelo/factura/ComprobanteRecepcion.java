/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.factura;

import java.math.BigDecimal;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "comprobante_recepcion")
@Getter
@Setter
public class ComprobanteRecepcion extends AbstractEntidad {

    /**
     * La inscripcion de factura a la que pertenece este comprobante de
     * recepción.
     */
    @ManyToOne
    @JoinColumn(name = "inscripcion_factura_id", nullable = false)
    private InscripcionFactura inscripcionFactura;

    /**
     * El folio de este comprobante.
     */
    @Column(name = "folio", length = 63)
    private String folio;

    /**
     * La descripción del comprobante.
     */
    @Column(name = "descripcion", nullable = false, length = 255)
    private String descripcion;

    /**
     * El volumen que se recibió de producto.
     */
    @Column(name = "volumen", nullable = false, precision = 19, scale = 4)
    private BigDecimal volumen;

    /**
     * El estatus de este comprobante.
     */
    @Column(name = "estatus", nullable = false, length = 63)
    private String estatus;

    /**
     * El orden de este comprobante.
     */
    @Column(name = "orden", nullable = false)
    private int orden;

    /**
     * Fecha de comprobante de recepción
     */
    @Column(name = "fecha_comprobante", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fechaComprobante;

}
