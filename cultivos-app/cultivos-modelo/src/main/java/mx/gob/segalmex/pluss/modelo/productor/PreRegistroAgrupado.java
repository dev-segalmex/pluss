package mx.gob.segalmex.pluss.modelo.productor;

import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;

/**
 * Entidad para obtener una consulta agrupada de pre registros.
 * @author oscar
 */
@Getter
@Setter
public class PreRegistroAgrupado {

    private CicloAgricola ciclo;

    private Estado estado;

    private Integer total;
}
