/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.factura;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "factura_relacionada")
@Getter
@Setter
public class FacturaRelacionada extends AbstractEntidad {

    /**
     * La inscripción que genera la relación de facturas.
     */
    @ManyToOne
    @JoinColumn(name = "inscripcion_id", nullable = false)
    private InscripcionFactura inscripcion;

    /**
     * El estatus de esta relación de facturas.
     */
    @Column(name = "estatus", nullable = false)
    private String estatus;

    /**
     * La factura del productor.
     */
    @ManyToOne
    @JoinColumn(name = "factura_productor_id", nullable = false)
    private Factura facturaProductor;

    /**
     * La factura global de la empresa.
     */
    @ManyToOne
    @JoinColumn(name = "factura_global_id")
    private Factura facturaGlobal;


}
