package mx.gob.segalmex.pluss.modelo.factura;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractCatalogo;

/**
 *
 * @author oscar
 */
@Entity
@Table(name = "tipo_uso_factura")
@Getter
@Setter
public class TipoUsoFactura extends AbstractCatalogo {

    /**
     * El factor.
     */
    @Column(name = "factor", nullable = false)
    private BigDecimal factor;

}
