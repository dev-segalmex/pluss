/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.factura;

import java.math.BigDecimal;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "factura")
@Getter
@Setter
public class Factura extends AbstractEntidad {

    /**
     * El uuid de esta factura.
     */
    @Column(name = "uuid", nullable = false)
    private String uuid;

    /**
     * La fecha de emisión de la factura.
     */
    @Column(name = "fecha", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecha;

    /**
     * La fecha de timbrado de la factura.
     */
    @Column(name = "fecha_timbrado", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fechaTimbrado;

    /**
     * El tipo de factura si es de productor o global.
     */
    @Column(name = "tipo", nullable = false)
    private String tipo;

    /**
     * El RFC del emisor de la factura según el CFDI.
     */
    @Column(name = "rfc_emisor", nullable = false)
    private String rfcEmisor;

    /**
     * El nombre del emisor de la factura según el CFDI.
     */
    @Column(name = "nombre_emisor")
    private String nombreEmisor;

    /**
     * El RFC del receptor de la factura según el CFDI.
     */
    @Column(name = "rfc_receptor")
    private String rfcReceptor;

    /**
     * El nombre del receptor de la factura según el CFDI.
     */
    @Column(name = "nombre_receptor")
    private String nombreReceptor;

    /**
     * El total de la factura.
     */
    @Column(name = "total")
    private BigDecimal total;

    /**
     * El UUID del timbre fiscal digital del SAT.
     */
    @Column(name = "uuid_timbre_fiscal_digital")
    private String uuidTimbreFiscalDigital;

    /**
     * El total de toneladas de esta factura.
     */
    @Column(name = "toneladas_totales", scale = 3)
    private BigDecimal toneladasTotales = BigDecimal.ZERO;

    /**
     * El total de toneladas disponibles para usar de esta factura.
     */
    @Column(name = "toneladas_disponibles", scale = 3)
    private BigDecimal toneladasDisponibles = BigDecimal.ZERO;

    /**
     * La referencia que indica mediante cual CFDI fue generada esta factura.
     */
    @Column(name = "referencia", nullable = false)
    private String referencia;

    /**
     * Indica el estatus asignado a una factura lo cual permite relacionarlas entre facturas de
     * productor y facturas globales.
     */
    @Column(name = "estatus", nullable = false)
    private String estatus;

}
