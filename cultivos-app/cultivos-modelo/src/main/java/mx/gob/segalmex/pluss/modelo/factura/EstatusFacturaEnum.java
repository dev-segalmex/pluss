/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.factura;

import mx.gob.segalmex.pluss.modelo.base.EnumCatalogo;

/**
 * Enum que representa los estatus de una factura.
 *
 * @author ismael
 */
public enum EstatusFacturaEnum implements EnumCatalogo {

    /**
     * Indica que una factura es nueva.
     */
    NUEVA("nueva"),
    /**
     * Indica que una factura es seleccionada.
     */
    SELECCIONADA("seleccionada"),
    /**
     * Indica que una factura es asignada.
     */
    ASIGNADA("asignada");

    /**
     * La clave del estatus de una factura.
     */
    private final String clave;

    private EstatusFacturaEnum(String clave) {
        this.clave = clave;
    }

    @Override
    public String getClave() {
        return clave;
    }

}
