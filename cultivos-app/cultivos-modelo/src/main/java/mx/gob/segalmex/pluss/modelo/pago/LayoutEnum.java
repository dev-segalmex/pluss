/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.pago;

import mx.gob.segalmex.pluss.modelo.base.EnumCatalogo;

/**
 *
 * @author oscar
 */
public enum LayoutEnum implements EnumCatalogo {

    PAGOS_MASIVOS("PAGOS_MASIVOS"),

    ORDENES_PAGO("ORDENES_PAGO");

    private final String clave;

    private LayoutEnum(String clave) {
        this.clave = clave;
    }

    @Override
    public String getClave() {
        return clave;
    }

    public static LayoutEnum getInstance(String clave) {
        for (LayoutEnum e : LayoutEnum.values()) {
            if (e.getClave().equals(clave)) {
                return e;
            }
        }

        throw new IllegalArgumentException("No existe un LayoutEnum con clave: " + clave);
    }

}
