/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.inscripcion;

import mx.gob.segalmex.pluss.modelo.base.EnumCatalogo;

/**
 *
 * @author ismael
 */
public enum TipoHistoricoInscripcionEnum implements EnumCatalogo {

    DOCUMENTACION("documentacion"),

    VALIDACION_SAT("validacion-sat"),

    VALIDACION_SAT_POSITIVA("validacion-sat-positiva"),

    VALIDACION_SAT_NEGATIVA("validacion-sat-negativa"),

    VALIDACION_SAT_MODIFICADA("validacion-sat-modificada"),

    CREACION("creacion"),

    ASIGNACION("asignacion"),

    VALIDACION("validacion"),

    CORRECCION("correccion"),

    REVALIDACION("revalidacion"),

    FINALIZACION("finalizacion"),

    SUPERVISION("supervision"),

    ESPERA("espera"),

    EDICION("edicion"),

    CANCELADO("cancelado"),

    NEGATIVO("negativo"),

    NEGATIVO_DEUDOR("negativo-deudor"),

    PENDIENTE("pendiente"),

    NO_ELEGIBLE("no-elegible"),
    /**
     * Histórico para contratos del productor.
     */
    ELIMINACION_CONTRATO("eliminicacion-contrato"),
    /**
     * Tipos histoircos de predios del productor.
     */
    ELIMINACION_PREDIO("elmininacion-predio"),

    ACTUALIZACION_SIEMBRA("actualizacion-siembra"),

    ACTUALIZACION_RIEGO("actualizacion-riego"),

    CAMBIO_GEORREFERECNIA("cambio-georreferencia"),

    SOLICITUD_COMPLEMENTO("solicitud-complemento"),

    DOCUMENTACION_COBERTURA("documentacion-cobertura"),

    COMPLEMENTO_AUTORIZADO("complemento-autorizado"),

    COMPLEMENTO_RECHAZADO("complemento-rechazado"),

    VALIDACION_COMPlEMENTO("validacion-complemento"),

    REVALIDACION_COMPlEMENTO("revalidacion-complemento"),

    CORRECION_COMPlEMENTO("correccion-complemento"),

    CORRECION_CONTRATO_PRODUCTOR("correccion-contrato-productor"),

    AUTORIZA_POSITIVO_DEUDOR("autorizacion-positivo-deudor");

    private final String clave;

    private TipoHistoricoInscripcionEnum(String clave) {
        this.clave = clave;
    }

    @Override
    public String getClave() {
        return clave;
    }

    public static TipoHistoricoInscripcionEnum getInstance(String clave) {
        for (TipoHistoricoInscripcionEnum e : TipoHistoricoInscripcionEnum.values()) {
            if (e.getClave().equals(clave)) {
                return e;
            }
        }

        throw new IllegalArgumentException("No existe una calse resolución con la clave: " + clave);
    }

}
