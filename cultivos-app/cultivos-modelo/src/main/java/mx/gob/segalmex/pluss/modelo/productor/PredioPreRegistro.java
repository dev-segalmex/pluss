/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.productor;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.catalogos.Municipio;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "predio_pre_registro")
@Getter
@Setter
public class PredioPreRegistro extends PredioBasico {

    /**
     * El pre registro al que pertenece este predio.
     */
    @ManyToOne
    @JoinColumn(name = "pre_registro_productor_id", nullable = false)
    private PreRegistroProductor preRegistroProductor;

    /**
     * El tipo de cultivo del predio.
     */
    @ManyToOne
    @JoinColumn(name = "tipo_cultivo_id", nullable = false)
    private TipoCultivo tipoCultivo;

    /**
     * El tipo de posesión del predio.
     */
    @ManyToOne
    @JoinColumn(name = "tipo_posesion_id", nullable = false)
    private TipoPosesion tipoPosesion;

    /**
     * El regimen hídrico del predio.
     */
    @ManyToOne
    @JoinColumn(name = "regimen_hidrico_id", nullable = false)
    private RegimenHidrico regimenHidrico;

    /**
     * El municipio del predio.
     */
    @ManyToOne
    @JoinColumn(name = "municipio_id", nullable = false)
    private Municipio municipio;

    /**
     * El volumen del predio.
     */
    @Column(name = "volumen", scale = 3, nullable = false)
    private BigDecimal volumen;

    /**
     * El orden del predio.
     */
    @Column(name = "orden", nullable = false)
    private int orden;

}
