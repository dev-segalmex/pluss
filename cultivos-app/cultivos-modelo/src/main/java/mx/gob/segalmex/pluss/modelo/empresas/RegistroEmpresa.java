/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.empresas;

import java.math.BigDecimal;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoSucursal;
import mx.gob.segalmex.pluss.modelo.historico.EntidadHistorico;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.productor.Domicilio;
import mx.gob.segalmex.pluss.modelo.productor.TipoPosesion;

/**
 * Entidad que define el registro de una empresa en un ciclo y para un grano.
 *
 * @author ismael
 */
@Entity
@Table(name = "registro_empresa")
@Getter
@Setter
public class RegistroEmpresa extends AbstractEntidad implements EntidadHistorico {

    /**
     * El tipo de la empresa: comercializadora, industria, bodega
     */
    @ManyToOne
    @JoinColumn(name = "tipo_sucursal_id", nullable = false)
    private TipoSucursal tipoSucursal;

    /**
     * El RFC de la empresa que se registra.
     */
    @Column(name = "rfc", nullable = false)
    private String rfc;

    /**
     * El nombre de la empresa
     */
    @Column(name = "nombre", nullable = false)
    private String nombre;

    /**
     * El ciclo agrícola en el que se registra.
     */
    @ManyToOne
    @JoinColumn(name = "ciclo_id", nullable = false)
    private CicloAgricola ciclo;

    /**
     * El cultivo en el que se registra.
     */
    @ManyToOne
    @JoinColumn(name = "cultivo_id", nullable = false)
    private Cultivo cultivo;

    /**
     * El uuid del registro.
     */
    @Column(name = "uuid", nullable = false)
    private String uuid;

    /**
     * Indica si el registro es editable.
     */
    @Column(name = "editable", nullable = false)
    private boolean editable;

    /**
     * La lista de sucursales de la empresa.
     */
    @OneToMany(mappedBy = "registroEmpresa")
    private List<RegistroSucursal> sucursales;

    /**
     * El nombre del responsable de la empresa.
     */
    @Column(name = "nombre_responsable", nullable = false)
    private String nombreResponsable;

    /**
     * El número de teléfono de la oficina.
     */
    @Column(name = "telefono_oficina", nullable = false)
    private String telefonoOficina;

    /**
     * El número de teléfono adicional o extension.
     */
    @Column(name = "extension_adicional", nullable = true)
    private String extensionAdicional;

    /**
     * El domicilio de la empresa.
     */
    @ManyToOne
    @JoinColumn(name = "domicilio_id", nullable = false)
    private Domicilio domicilio;

    /**
     * El nombre de la persona que funje como enlace con SEGALMEX.
     */
    @Column(name = "nombre_enlace", nullable = false)
    private String nombreEnlace;

    /**
     * El número célular del enlace con SEGALMEX.
     */
    @Column(name = "numero_celular_enlace", nullable = false)
    private String numeroCelularEnlace;

    /**
     * El correo electrónico del enlace con SEGALMEX.
     */
    @Column(name = "correo_enlace", nullable = false)
    private String correoEnlace;

    /**
     * Indica si existe equipo de análisis en operación.
     */
    @Column(name = "existe_equipo_analisis", nullable = false)
    private boolean existeEquipoAnalisis;

    /**
     * Indica si aplica normas de calidad.
     */
    @Column(name = "aplica_normas_calidad", nullable = false)
    private boolean aplicaNormasCalidad;

    /**
     * Indica el tipo de posesión de la infraesctura de la empresa.
     */
    @ManyToOne
    @JoinColumn(name = "tipo_posesion_id", nullable = false)
    private TipoPosesion tipoPosesion;

    /**
     * Indica el tipo de almacenamiento de la empresa.
     */
    @Column(name = "tipo_almacenamiento", nullable = false)
    private String tipoAlmacenamiento;

    /**
     * La latitud de la ubicación.
     */
    @Column(name = "latitud", scale = 6, nullable = false)
    private BigDecimal latitud;

    /**
     * La longitud de la ubicación.
     */
    @Column(name = "longitud", scale = 6, nullable = false)
    private BigDecimal longitud;

    /**
     * El número de almacenes con los que cuenta la empresa.
     */
    @Column(name = "numero_almacenes", nullable = false)
    private int numeroAlmacenes;

    /**
     * La capacidad instalada en toneladas.
     */
    @Column(name = "capacidad", scale = 3, nullable = false)
    private BigDecimal capacidad;

    /**
     * El volumen almacenado en toneladas actual.
     */
    @Column(name = "volumen_actual", scale = 3, nullable = false)
    private BigDecimal volumenActual;

    /**
     * La superficie de almacenamiento en hectáreas.
     */
    @Column(name = "superficie", scale = 3, nullable = false)
    private BigDecimal superficie;

    /**
     * Indica si esta habilitada por alguna almacenadora.
     */
    @Column(name = "habilita_almacenadora", nullable = false)
    private boolean habilitaAlmacenadora;

    /**
     * El nombre de la almacenadora.
     */
    @Column(name = "nombre_almacenadora", nullable = false)
    private String nombreAlmacenadora;

    /**
     * Estatus del regisro.
     */
    @ManyToOne
    @JoinColumn(name = "estatus_id", nullable = false)
    private EstatusInscripcion estatus;

    /**
     * Usuario que valida el registro.
     */
    @ManyToOne
    @JoinColumn(name = "usuario_validador_id", nullable = true)
    private Usuario usuarioValidador;

    /**
     * Los datos de comercialización de la empresa.
     */
    @OneToMany(mappedBy = "registroEmpresa")
    private List<DatosComercializacion> datosComercializacion;

}
