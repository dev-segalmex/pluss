/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.factura;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "cfdi")
@Getter
@Setter
public class Cfdi extends AbstractEntidad {

    /**
     * El UUID del CFDI en la base de datos.
     */
    @Column(name = "uuid", nullable = false)
    private String uuid;

    /**
     * El estatus del CFDI.
     */
    @Column(name = "estatus")
    private String estatus;

    /**
     * El RFC del emisor del CFDI.
     */
    @Column(name = "rfc_emisor")
    private String rfcEmisor;

    /**
     * El nombre del emisor del CFDI.
     */
    @Column(name = "nombre_emisor")
    private String nombreEmisor;

    /**
     * El RFC del receptor del CFDI.
     */
    @Column(name = "rfc_receptor")
    private String rfcReceptor;

    /**
     * El nombre del receptor del CFDI.
     */
    @Column(name = "nombre_receptor")
    private String nombreReceptor;

    /**
     * El UUID del timbre fiscal digital generando por el SAT.
     */
    @Column(name = "uuid_timbre_fiscal_digital")
    private String uuidTimbreFiscalDigital;

    /**
     * El total del CFDI.
     */
    @Column(name = "total", scale = 3)
    private BigDecimal total;

    /**
     *  Indica la suma de los conceptos que se encuentran en estatus positivo.
     */
    @Column(name = "subtotal", scale = 3)
    private BigDecimal subtotal;

    /**
     * La moneda en la que se meitió el CFDI.
     */
    @Column(name = "moneda")
    private String moneda;

    /**
     * La lista de conceptos del CFDI.
     */
    @OneToMany(mappedBy = "cfdi")
    private List<ConceptoCfdi> conceptos;

    /**
     * El total de toneladas del CFDI.
     */
    @Column(name = "total_toneladas", scale = 3)
    private BigDecimal totalToneladas;

    /**
     * El archivo en donde se encuentra el CFDI.
     */
    @ManyToOne
    @JoinColumn(name = "archivo_id", nullable = false)
    private Archivo archivo;

    /**
     * El usuario que registró el CFDI.
     */
    @ManyToOne
    @JoinColumn(name = "usuario_id", nullable = false)
    private Usuario usuario;

    /**
     * El método de pago de la factura.
     */
    @Column(name = "metodo_pago", nullable = false)
    private String metodoPago;

    /**
     * La fecha con la que se genera el tibre fisical digital de la factura.
     */
    @Column(name = "fecha_timbrado", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fechaTimbrado;

    /**
     * La fecha en que se emite la factura.
     */
    @Column(name = "fecha", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fecha;

    /**
     * Indica si los datos del receptor y emisor tuvieron que ser invertidos (para el caso de
     * algunas liquidaciones).
     */
    @Column(name = "invertida")
    private Boolean invertida;

}
