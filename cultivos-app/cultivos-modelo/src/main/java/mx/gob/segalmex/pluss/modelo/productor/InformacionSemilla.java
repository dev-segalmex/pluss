/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.productor;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;

/**
 *
 * @author oscar
 */
@Entity
@Table(name = "informacion_semilla")
@Getter
@Setter
public class InformacionSemilla extends AbstractEntidad {

    /**
     * Folio del snics.
     */
    @Column(name = "snics", nullable = false)
    private String snics;

    /**
     * Las toneladas d ela semilla.
     */
    @Column(name = "toneladas", scale = 3, nullable = false)
    private BigDecimal toneladas;

    /**
     * Cantidad de folios.
     */
    @Column(name = "cantidad_folios", nullable = false)
    private String cantidadFolios;

    /**
     * Nombre y/o razón social.
     */
    @Column(name = "nombre", nullable = false)
    private String nombre;

    /**
     * El rfc.
     */
    @Column(name = "rfc", nullable = false)
    private String rfc;

    /**
     * El cultivo al que pertenece la información de la semilla.
     */
    @ManyToOne
    @JoinColumn(name = "cultivo_id", nullable = false)
    private Cultivo cultivo;

    /**
     * El ciclo al que pertenece la información de la semilla.
     */
    @ManyToOne
    @JoinColumn(name = "ciclo_id", nullable = false)
    private CicloAgricola ciclo;

}