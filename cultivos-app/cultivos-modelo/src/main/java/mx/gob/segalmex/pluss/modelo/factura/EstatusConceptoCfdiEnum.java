/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.factura;

/**
 *
 * @author oscar
 */
public enum EstatusConceptoCfdiEnum {

    POSITIVO("positivo"),
    NEGATIVO("negativo"),
    IGNORADO("ignorado");

    private final String clave;

    private EstatusConceptoCfdiEnum(String clave) {
        this.clave = clave;
    }

    public String getClave() {
        return clave;
    }

}
