/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.factura;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.catalogos.Region;
import mx.gob.segalmex.pluss.modelo.inscripcion.AbstractInscripcion;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "inscripcion_factura")
@Getter
@Setter
public class InscripcionFactura extends AbstractInscripcion {

    /**
     * El CFDI asociada a esta inscripción de factura.
     */
    @ManyToOne
    @JoinColumn(name = "cfdi_id", nullable = false)
    private Cfdi cfdi;

    /**
     * La factura asociada a esta inscripción.
     */
    @ManyToOne
    @JoinColumn(name = "factura_id")
    private Factura factura;

    /**
     * La cantidad de toneladas de las que hace uso esta factura.
     */
    @Column(name = "cantidad", precision = 19, scale = 3, nullable = false)
    private BigDecimal cantidad;

    /**
     * El número de comprobantes asociados a esta inscripción de factura.
     */
    @Column(name = "numero_comprobantes")
    private int numeroComprobantes;

    /**
     * La cantidad de toneladas comprobadas de las que hace uso esta factura,
     * las cuales no pueden ser mayores a la cantidad de toneladas.
     */
    @Column(name = "cantidad_comprobada", precision = 19, scale = 3, nullable = false)
    private BigDecimal cantidadComprobada = BigDecimal.ZERO;

    /**
     * Indica si la factura es o no válida según el SAT.
     */
    @Column(name = "valida")
    private Boolean valida;

    /**
     * Indica si la factura está duplicada, es decir, si ya se había registrado
     * previamente.
     */
    @Column(name = "duplicada")
    private Boolean duplicada;

    /**
     * El contrato al que está asignada la factura.
     */
    @Column(name = "contrato")
    private String contrato;

    /**
     * Las toneladas facturables de esta inscripción con base en la cantidad, la
     * cantidad comprobada y el productor.
     */
    @Column(name = "facturables", scale = 3)
    private BigDecimal facturables;

    /**
     * El tipo de factura que se intenta inscribir.
     */
    @Column(name = "tipo_factura", nullable = false)
    private String tipoFactura;

    /**
     * El tipo de cultivo al que pertenece esta {@link InscripcionFactura}.
     * NOTA: Este dato sirve para determinar como se pagará el incentivo.
     */
    @ManyToOne
    @JoinColumn(name = "tipo_cultivo_id")
    private TipoCultivo tipoCultivo;

    /**
     * El estado de la {@link InscripcionFactura}.
     */
    @ManyToOne
    @JoinColumn(name = "estado_id")
    private Estado estado;

    /**
     * El estado que determina como se pagará el incentivo. En general estado y
     * estadoEstimulo casi siempre son iguales, salvo las facturas que a pesar
     * de ser de un estado se pagan con incentivo de otro estado.
     */
    @ManyToOne
    @JoinColumn(name = "estado_estimulo_id")
    private Estado estadoEstimulo;

    /**
     * La fecha en que se paga esta inscripción.
     */
    @Column(name = "fecha_pago")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fechaPago;

    /**
     * Lista auxiliar para manejar los comprobantes de la factura.
     */
    @Transient
    private List<ComprobanteRecepcion> comprobantes;

    /**
     * El importe total de los comprobante de pago del productor, los cuales
     * representan el total de los pagos que se le hayan hecho al productor por
     * su cosecha.
     */
    @Column(name = "cantidad_comprobante_pago", precision = 19, scale = 3, nullable = false)
    private BigDecimal cantidadComprobantePago = BigDecimal.ZERO;

    /**
     * El precio de la tonelada (total/totalTonelada).
     */
    @Column(name = "precio_tonelada", precision = 19, scale = 3)
    private BigDecimal precioTonelada;

    /**
     * El valor para calcular el precioToneladaReal.
     */
    @Column(name = "precio_ajuste", precision = 19, scale = 3)
    private BigDecimal precioAjuste = BigDecimal.ZERO;

    /**
     * El precio de la tonelada considerando el ajuste.
     */
    @Column(name = "precio_tonelada_real", precision = 19, scale = 3)
    private BigDecimal precioToneladaReal = BigDecimal.ZERO;

    /**
     * La fecha que se condiera para el pago del estímulo.
     */
    @Column(name = "fecha_pago_estimulo", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fechaPagoEstimulo;

    /**
     * Propiedad auxiliar para conocer la región de esta InscripcionFactura.
     * NOTA: Ayuda a definir el estado y estadoEstimulo.
     */
    @Transient
    private Region region;

    /**
     * El productor ciclo al que pertenece la inscripcionFactura.
     */
    @ManyToOne
    @JoinColumn(name = "productor_ciclo_id")
    private ProductorCiclo productorCiclo;

}
