/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.secuencias;

/**
 *
 * @author oscar
 */
public enum TipoFolioEnum {

    SEQ_FOLIO_MAIZ("seq_folio_maiz"),

    SEQ_FOLIO_PRODUCTOR_MAIZ("seq_folio_productor_maiz"),

    SEQ_FOLIO_FACTURA("seq_folio_factura"),

    SEQ_FOLIO_PRE_REGISTRO("seq_folio_pre_registro"),

    SEQ_FOLIO_DOCUMENTO_COMERCIAL("seq_folio_documento_comercial"),

    SEQ_FOLIO_EMPRESA("seq_folio_empresa"),

    SEQ_FOLIO_COMPROBANTE_RECEPCION("seq_folio_comprobante_recepcion"),

    SEQ_FOLIO_SUCURSAL("seq_folio_sucursal"),

    SEQ_FOLIO_ALERTA("seq_folio_alerta");

    private final String clave;

    private TipoFolioEnum(String clave) {
        this.clave = clave;
    }

    public String getClave() {
        return clave;
    }

    public static TipoFolioEnum getInstance(String clave) {
        for (TipoFolioEnum e : TipoFolioEnum.values()) {
            if (e.getClave().equals(clave)) {
                return e;
            }
        }

        throw new IllegalArgumentException("No existe un tipo de folio con la clave: " + clave);
    }
}
