package mx.gob.segalmex.pluss.modelo.catalogos;

import javax.persistence.Entity;
import javax.persistence.Table;
import mx.gob.segalmex.pluss.modelo.base.AbstractCatalogo;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "cultivo")
public class Cultivo extends AbstractCatalogo {

}