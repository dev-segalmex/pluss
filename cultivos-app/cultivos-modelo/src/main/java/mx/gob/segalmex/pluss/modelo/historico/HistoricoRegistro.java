/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.historico;

import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcion;

/**
 *
 * @author jurgen
 */
@Entity
@Table(name = "historico_registro")
@Getter
@Setter
public class HistoricoRegistro extends AbstractEntidad {

    /**
     * La clase a la cual pertenece el histórico registro.
     */
    @Column(name = "clase", nullable = false)
    private String clase;

    /**
     * La referencia a la cual pertenece el histórico registro.
     */
    @Column(name = "referencia", nullable = false, length = 63)
    private String referencia;

    /**
     * El tipo de histórico de inscripción.
     */
    @ManyToOne
    @JoinColumn(name = "tipo_id", nullable = false)
    private TipoHistoricoInscripcion tipo;

    /**
     * El usuario que registra el histórico de inscripción.
     */
    @ManyToOne
    @JoinColumn(name = "usuario_registra_id")
    private Usuario usuarioRegistra;

    /**
     * El usuario que finaliza el histórico de inscripción.
     */
    @ManyToOne
    @JoinColumn(name = "usuario_finaliza_id")
    private Usuario usuarioFinaliza;

    /**
     * La fecha en que finaliza el histórico de inscripción.
     */
    @Column(name = "fecha_finaliza")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fechaFinaliza;

    /**
     * La propiedad para indicar que un histórico es monitoreable.
     */
    @Column(name = "monitoreable", nullable = false)
    private String monitoreable = "--";

    /**
     * Indica si el histórico se encuentra activo o no.
     */
    @Column(name = "activo", nullable = false)
    private boolean activo = true;

    /**
     * La entidad a la cual pertenece el histórico.
     */
    @Transient
    private EntidadHistorico entidad;

    /**
     * Cadena con la información de a que cultivo y ciclo pertenece este
     * histórico o en el futuro puede almacenar una clave diferente.
     */
    @Column(name = "etiqueta_grupo", nullable = false)
    private String etiquetaGrupo;

}
