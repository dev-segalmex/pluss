/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.inscripcion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "dato_capturado")
@Getter
@Setter
public class DatoCapturado extends AbstractEntidad {

    /**
     * La clase a la cual pertenece un dato capturado.
     */
    @Column(name = "clase", nullable = false)
    private String clase;

    /**
     * La referencia a la cual pertenece el dato capturado.
     */
    @Column(name = "referencia", nullable = false)
    private String referencia;

    /**
     * El nombre del dato capturado.
     */
    @Column(name = "nombre", nullable = false)
    private String nombre;

    /**
     * La clave del dato capturado.
     */
    @Column(name = "clave", nullable = false)
    private String clave;

    /**
     * El valor del dato capturado.
     */
    @Column(name = "valor", nullable = false)
    private String valor;

    /**
     * El valor en formato texto del dato capturado.
     */
    @Column(name = "valor_texto")
    private String valorTexto;

    /**
     * El comentario del dato capturado.
     */
    @Column(name = "comentario", length = 1023)
    private String comentario;

    /**
     * Indica si el dato capturado es correcto o no.
     */
    @Column(name = "correcto")
    private Boolean correcto;

    /**
     * EL orden en la lista de datos capturados.
     */
    @Column(name = "orden", nullable = false)
    private int orden;

    @Column(name = "actual", nullable = false)
    private boolean actual = true;

    /**
     * El grupo al que pertenece el dato capturado.
     */
    @Column(name = "grupo")
    private String grupo;

    /**
     * El tipo de dato capturado.
     */
    @Column(name = "tipo")
    private String tipo;

    /**
     * Propiedad auxiliar para saber si es requerido el dato capturado.
     */
    @Transient
    private boolean requerido = true;

}
