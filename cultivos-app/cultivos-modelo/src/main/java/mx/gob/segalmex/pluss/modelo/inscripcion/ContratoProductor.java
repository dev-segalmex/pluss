/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.inscripcion;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;

/**
 *
 * @author jurgen
 */
@Entity
@Table(name = "contrato_productor")
@Getter
@Setter
public class ContratoProductor extends AbstractEntidad {

    private static final String EMPTY = "--";

    /**
     * La InscripcionContrato a la que pertenece este ContratoProductor.
     */
    @ManyToOne
    @JoinColumn(name = "inscripcion_contrato_id", nullable = false)
    private InscripcionContrato inscripcionContrato;

    /**
     * El nombre del productor.
     */
    @Column(name = "nombre")
    private String nombre = EMPTY;

    /**
     * El primer apellido del productor.
     */
    @Column(name = "primer_apellido")
    private String primerApellido = EMPTY;

    /**
     * El segundo apellido del productor.
     */
    @Column(name = "segundo_apellido")
    private String segundoApellido = EMPTY;

    /**
     * El CURP del productor.
     */
    @Column(name = "curp")
    private String curp = EMPTY;

    /**
     * El RFC del productor.
     */
    @Column(name = "rfc")
    private String rfc = EMPTY;

    /**
     * La superficie sembrada.
     */
    @Column(name = "superficie", precision = 19, scale = 3, nullable = false)
    private BigDecimal superficie = BigDecimal.ZERO;

    /**
     * El volumen contratado en el CCV.
     */
    @Column(name = "volumen_contrato", precision = 19, scale = 3, nullable = false)
    private BigDecimal volumenContrato = BigDecimal.ZERO;

    /**
     * El volumen contratado cubierto por el IAR.
     */
    @Column(name = "volumen_iar", precision = 19, scale = 3, nullable = false)
    private BigDecimal volumenIar = BigDecimal.ZERO;

    /**
     * La fecha de baja del ContratoProductor.
     */
    @Column(name = "estatus_fecha_baja", nullable = false, length = 63)
    private String estatusFechaBaja = "--";

    /**
     * Indica si el ContratoProductor es correcto.
     */
    @Column(name = "correcto")
    private boolean correcto;

    /**
     * Detalles de un posible error.
     */
    @Transient
    private String error;

    /**
     * El orden del ContratoProductor.
     */
    @Column(name = "orden", nullable = false)
    private int orden;

}
