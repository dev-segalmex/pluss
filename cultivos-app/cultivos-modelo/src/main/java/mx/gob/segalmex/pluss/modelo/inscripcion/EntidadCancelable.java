/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.inscripcion;

import java.util.Calendar;
/**
 *
 * @author jurgen
 */
public interface EntidadCancelable {

    Calendar getFechaCancelacion();

}
