/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.productor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.Localidad;
import mx.gob.segalmex.pluss.modelo.catalogos.Municipio;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "domicilio")
@Getter
@Setter
public class Domicilio extends AbstractEntidad {

    /**
     * La calle del domicilio.
     */
    @Column(name = "calle")
    private String calle;

    /**
     * El número interior del domicilio.
     */
    @Column(name = "numero_interior")
    private String numeroInterior;

    /**
     * El número exterior del domicilio.
     */
    @Column(name = "numero_exterior")
    private String numeroExterior;

    /**
     * La clave de la localidad del domicilio.
     */
    @Column(name = "clave_localidad", length = 31)
    private String claveLocalidad;

    /**
     * La localidad del domicilio.
     */
    @Column(name = "localidad")
    private String localidad;

    /**
     * El estado al que pertenece el domicilio.
     */
    @ManyToOne
    @JoinColumn(name = "estado_id", nullable = false)
    private Estado estado;

    /**
     * El municipio al que pertenece el domicilio.
     */
    @ManyToOne
    @JoinColumn(name = "municipio_id", nullable = false)
    private Municipio municipio;

    /**
     * El código postal del domicilio.
     */
    @Column(name = "codigo_postal")
    private String codigoPostal;

    @Transient
    private Localidad catalogoLocalidad;

}
