/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.catalogos;

import mx.gob.segalmex.pluss.modelo.base.EnumCatalogo;

/**
 *
 * @author ismael
 */
public enum CultivoEnum implements EnumCatalogo {

    MAIZ_COMERCIAL("maiz-comercial", "Maíz"),

    ARROZ("arroz", "Arroz"),

    TRIGO("trigo", "Trigo");

    private final String clave;

    private final String nombre;

    private CultivoEnum(String clave, String nombre) {
        this.clave = clave;
        this.nombre = nombre;
    }

    @Override
    public String getClave() {
        return clave;
    }

    public String getNombre() {
        return nombre;
    }

    public static CultivoEnum getInstance(String clave) {
        for (CultivoEnum e : CultivoEnum.values()) {
            if (e.getClave().equals(clave)) {
                return e;
            }
        }

        throw new IllegalArgumentException("No existe un cultivo con la clave: " + clave);
    }
}
