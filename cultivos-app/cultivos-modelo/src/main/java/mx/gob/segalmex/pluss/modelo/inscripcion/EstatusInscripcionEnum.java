/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.inscripcion;

import mx.gob.segalmex.pluss.modelo.base.EnumCatalogo;

/**
 *
 * @author ismael
 */
public enum EstatusInscripcionEnum implements EnumCatalogo {

    DESCONOCIDO("desconocido"),

    PENDIENTE("pendiente"),

    NUEVO("nuevo"),

    VALIDACION("validacion"),

    CORRECCION("correccion"),

    SOLVENTADA("solventada"),

    CORRECTO("correcto"),

    POSITIVA("positiva"),

    CANCELADA("cancelada"),

    NO_ELIGIBLE ("no-elegible"),

    SUPERVISION("supervision"),

    NEGATIVO("negativo"),

    NEGATIVO_DEUDOR("negativo-deudor"),

    PENDIENTE_REVISION("pendiente-revision");

    private final String clave;

    private EstatusInscripcionEnum(String clave) {
        this.clave = clave;
    }

    @Override
    public String getClave() {
        return clave;
    }

    public static EstatusInscripcionEnum getInstance(String clave) {
        for (EstatusInscripcionEnum e : EstatusInscripcionEnum.values()) {
            if (e.getClave().equals(clave)) {
                return e;
            }
        }

        throw new IllegalArgumentException("No existe una calse resolución con la clave: " + clave);
    }

}
