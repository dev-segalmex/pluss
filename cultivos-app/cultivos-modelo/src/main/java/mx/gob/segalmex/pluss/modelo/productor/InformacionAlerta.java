package mx.gob.segalmex.pluss.modelo.productor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;

/**
 *
 * @author oscar
 */
@Entity
@Table(name = "informacion_alerta")
@Getter
@Setter
public class InformacionAlerta extends AbstractEntidad {

    @Column(name = "folio", length = 63, nullable = false)
    private String folio;

    @Column(name = "tipo", nullable = false)
    private String tipo;

    @Column(name = "valor", nullable = false)
    private String valor;

    @Column(name = "comentario")
    private String comentario;

    /**
     * La fecha de la baja de la Alerta, o "--" sí esta activo.
     */
    @Column(name = "estatus_fecha_baja", nullable = false, length = 63)
    private String estatusFechaBaja = "--";

    /**
     * El ciclo al que pertenece la Alerta.
     */
    @ManyToOne
    @JoinColumn(name = "ciclo_id")
    private CicloAgricola ciclo;

    /**
     * El cultivo al que pertenece la Alerta.
     */
    @ManyToOne
    @JoinColumn(name = "cultivo_id")
    private Cultivo cultivo;

}
