/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.inscripcion;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OrderBy;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoEmpresa;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoPrecio;
import mx.gob.segalmex.pluss.modelo.contrato.CoberturaInscripcionContrato;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "inscripcion_contrato")
@Getter
@Setter
public class InscripcionContrato extends AbstractInscripcion {

    @ManyToOne
    @JoinColumn(name = "tipo_cultivo_id", nullable = false)
    protected TipoCultivo tipoCultivo;

    @Column(name = "numero_contrato")
    private String numeroContrato;

    @Column(name = "fecha_firma")
    @Temporal(TemporalType.DATE)
    private Calendar fechaFirma;

    @ManyToOne
    @JoinColumn(name = "tipo_empresa_comprador_id", nullable = false)
    private TipoEmpresa tipoEmpresaComprador;

    @Column(name = "nombre_comprador")
    private String nombreComprador;

    @Column(name = "rfc_comprador")
    private String rfcComprador;

    @ManyToOne
    @JoinColumn(name = "tipo_empresa_vendedor_id", nullable = false)
    private TipoEmpresa tipoEmpresaVendedor;

    @Column(name = "nombre_vendedor")
    private String nombreVendedor;

    @Column(name = "rfc_vendedor")
    private String rfcVendedor;

    @OneToMany(mappedBy = "inscripcion")
    private List<InscripcionContratoEstado> estadosDestino;

    @Column(name = "estados_destino_texto")
    private String estadosDestinoTexto;

    /**
     * El archivo actual del contrato.
     */
    @ManyToOne
    @JoinColumn(name = "contrato_id")
    private Archivo contrato;

    /**
     * Indica si todos cuentan con cobertura.
     */
    @Transient
    private Boolean cobertura;

    /**
     * El número de productores que respaldan el contrato.
     */
    @Column(name = "numero_productores", nullable = false)
    private Integer numeroProductores;

    @Column(name = "volumen_total", scale = 3, nullable = false)
    private BigDecimal volumenTotal;

    @ManyToOne
    @JoinColumn(name = "estado_id", nullable = false)
    private Estado estado;

    /**
     * La lista de IAR.
     */
    @OneToMany(mappedBy = "inscripcionContrato")
    @OrderBy("orden ASC")
    private List<CoberturaInscripcionContrato> coberturas;

    @Column(name = "folio_snics")
    private String folioSnics;

    /* Propiedades que se encontraban en la entidad de Predio. */
    /**
     * Valor de la superficie de la entidad.
     */
    @Column(name = "superficie", scale = 3)
    private BigDecimal superficie;

    /**
     * El tipo de precio contratado.
     */
    @ManyToOne
    @JoinColumn(name = "tipo_precio_id", nullable = false)
    private TipoPrecio tipoPrecio;

    /**
     * Valor del precio futuro.
     */
    @Column(name = "precio_futuro", scale = 3)
    private BigDecimal precioFuturo;

    /**
     * Valor de la base acordada.
     */
    @Column(name = "base_acordada", scale = 3)
    private BigDecimal baseAcordada;

    /**
     * Valor del precio fijo.
     */
    @Column(name = "precio_fijo", scale = 3)
    private BigDecimal precioFijo;

    /**
     * Lista de ContratosProductor.
     */
    @OneToMany(mappedBy = "inscripcionContrato")
    private List<ContratoProductor> contratosProductor;

}
