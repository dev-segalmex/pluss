package mx.gob.segalmex.pluss.modelo.catalogos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractCatalogo;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "tipo_cultivo")
@Getter
@Setter
public class TipoCultivo extends AbstractCatalogo {

    @Column(name = "abreviatura", nullable = false, length = 63)
    private String abreviatura;

    /**
     * Indica el grupo de estímulo en el que se encuentra este tipo de cultivo.
     */
    @Column(name = "grupo_estimulo", nullable = false, length = 63)
    private String grupoEstimulo;

}
