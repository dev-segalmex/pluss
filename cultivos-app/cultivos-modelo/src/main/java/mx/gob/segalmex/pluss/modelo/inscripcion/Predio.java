/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.inscripcion;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoPrecio;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "predio")
@Getter
@Setter
public class Predio extends AbstractEntidad {

    @ManyToOne
    @JoinColumn(name = "estado_id")
    private Estado estado;

    @Column(name = "superficie", scale = 3)
    private BigDecimal superficie;

    @Column(name = "volumen", scale = 3)
    private BigDecimal volumen;

    @ManyToOne
    @JoinColumn(name = "tipo_precio_id", nullable = false)
    private TipoPrecio tipoPrecio;

    @Column(name = "precio_futuro", scale = 3)
    private BigDecimal precioFuturo;

    @Column(name = "base_acordada", scale = 3)
    private BigDecimal baseAcordada;

    @Column(name = "precio_fijo", scale = 3)
    private BigDecimal precioFijo;

    @ManyToOne
    @JoinColumn(name = "inscripcion_contrato_id", nullable = false)
    private InscripcionContrato inscripcionContrato;

    @Column(name = "orden", nullable = false)
    private int orden;

    @ManyToOne
    @JoinColumn(name = "tipo_cultivo_id", nullable = false)
    private TipoCultivo tipoCultivo;

}
