/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.factura;

import mx.gob.segalmex.pluss.modelo.base.EnumCatalogo;

/**
 * Enum que representa los estatus de una factura.
 *
 * @author ismael
 */
public enum EstatusUsoFacturaEnum implements EnumCatalogo {

    /**
     * Indica que un uso de factura es nuevo (Validado).
     */
    NUEVO("nuevo"),

    /**
     * Indica que el uso factura se ha cancelado, ya que fue cancelada su
     * factura de la que proviene.
     */
    NO_VALIDO("no-valido"),

    /**
     * Indica que un uso factura esta en pendiente, lo que quiere decir
     * que no se puede seleccionar para un requerimiento de pago.
     */
    PENDIENTE("pendiente"),

    /**
     * Indica que un uso de factura fue seleccionado para pago.
     */
    SELECCIONADO("seleccionado"),

    /**
     * Indica que un uso de factura está en proceso de pago.
     */
    EN_PROCESO("en-proceso"),

    /**
     * Indica que un uso de factura fue pagado.
     */
    PAGADO("pagado"),

    /**
     * Indica que un uso de factura no fue pagado por cancelacion.
     */
    NO_PAGADO_CANCELADO("no-pagado-cancelado"),

    /**
     * Indica que un uso de factura no fue pagado por devolución.
     */
    NO_PAGADO_DEVUELTO("no-pagado-devuelto");

    /**
     * La clave del estatus del uso de una factura.
     */
    private final String clave;

    private EstatusUsoFacturaEnum(String clave) {
        this.clave = clave;
    }

    @Override
    public String getClave() {
        return clave;
    }

    public static EstatusUsoFacturaEnum getInstance(String clave) {
        for (EstatusUsoFacturaEnum e : EstatusUsoFacturaEnum.values()) {
            if (e.getClave().equals(clave)) {
                return e;
            }
        }

        throw new IllegalArgumentException("No existe un ciclo estatus uso factura con clave: " + clave);
    }

}
