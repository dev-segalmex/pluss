/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.factura;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "concepto_cfdi")
@Getter
@Setter
public class ConceptoCfdi extends AbstractEntidad {

    /**
     * La cantidad del concepto.
     */
    @Column(name = "cantidad")
    private BigDecimal cantidad;

    /**
     * El valor unitario del concepto.
     */
    @Column(name = "valor_unitario")
    private BigDecimal valorUnitario;

    /**
     * El nombre de la unidad de medida.
     */
    @Column(name = "unidad")
    private String unidad;

    /**
     * La clave de la unidad de medida.
     */
    @Column(name = "clave_unidad")
    private String claveUnidad;

    /**
     * La descripción del concepto.
     */
    @Column(name = "descripcion", length = 2047)
    private String descripcion;

    /**
     * El importe del concepto.
     */
    @Column(name = "importe", scale = 3)
    private BigDecimal importe;

    /**
     * La información del cfdi.
     */
    @ManyToOne
    @JoinColumn(name = "cfdi_id", nullable = false)
    private Cfdi cfdi;

    /**
     * La clave del producto de servicio.
     */
    @Column(name = "clave_producto_servicio", length = 63)
    private String claveProductoServicio;

    /**
     * El estatus del concepto.
     */
    @Column(name = "estatus", length = 63)
    private String estatus;

    /**
     * El orden del concepto.
     */
    @Column(name = "orden", nullable = false)
    private int orden;

}
