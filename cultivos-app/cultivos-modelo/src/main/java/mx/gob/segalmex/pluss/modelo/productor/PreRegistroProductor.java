/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.productor;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.Parentesco;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoPrecio;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "pre_registro_productor")
@Getter
@Setter
public class PreRegistroProductor extends AbstractEntidad {

    /**
     * El uuid del pre registro.
     */
    @Column(name = "uuid", nullable = false)
    private String uuid;

    /**
     * El ciclo agrícola del pre registro.
     */
    @ManyToOne
    @JoinColumn(name = "ciclo_id", nullable = false)
    private CicloAgricola ciclo;

    /**
     * El cultivo del pre registro.
     */
    @ManyToOne
    @JoinColumn(name = "cultivo_id", nullable = false)
    private Cultivo cultivo;

    /**
     * Los tipos de cultivo del pre registro.
     */
    @Column(name = "etiqueta_tipo_cultivo", nullable = false)
    private String etiquetaTipoCultivo;

    /**
     * EL folio del pre registro.
     */
    @Column(name = "folio", nullable = false)
    private String folio;

    /**
     * El curp del productor que se registra.
     */
    @Column(name = "curp", nullable = false)
    private String curp;

    /**
     * El RFC del productor.
     */
    @Column(name = "rfc", nullable = false)
    private String rfc;

    /**
     * El nombre del productor.
     */
    @Column(name = "nombre", nullable = false)
    private String nombre;

    /**
     * El primer apellido del productor.
     */
    @Column(name = "primer_apellido", nullable = false)
    private String primerApellido;

    /**
     * El segundo apellido del productor.
     */
    @Column(name = "segundo_apellido")
    private String segundoApellido;

    /**
     * Los predio del pre-registro.
     */
    @OneToMany(mappedBy = "preRegistroProductor")
    private List<PredioPreRegistro> predios;

    /**
     * El teléfono de contacto del productor.
     */
    @Column(name = "telefono")
    private String telefono;

    /**
     * El correo electrónico del productor.
     */
    @Column(name = "correo_electronico")
    private String correoElectronico;

    /**
     * Verifica si firmo contrato.
     */
    @Column(name = "firmo_contrato")
    private Boolean firmoContrato;

    /**
     * La fecha de cancalación del pre-registro.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_cancelacion")
    private Calendar fechaCancelacion;

    /**
     * El usuario que registra la edicion del pre registro.
     */
    @ManyToOne
    @JoinColumn(name = "usuario_registra_id", nullable = true)
    private Usuario usuarioRegistra;

    /**
     * Los diferentes tipos de cultivos seleccionados en los predios.
     */
    @ManyToMany(fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    @JoinTable(name = "pre_registro_tipo_cultivo", joinColumns = {
        @JoinColumn(name = "pre_registro_id", referencedColumnName = "id")},
            inverseJoinColumns = {
                @JoinColumn(name = "tipo_cultivo_id", referencedColumnName = "id")})
    private List<TipoCultivo> tiposCultivo;

    /**
     * Tipos de precio. Precio abierto dolares.
     */
    @Column(name = "precio_abierto")
    private int precioAbierto = 0;

    /**
     * Precio cerrado dolares.
     */
    @Column(name = "precio_cerrado_dolares")
    private int precioCerradoDolares = 0;

    /**
     * Precio cerrado pesos.
     */
    @Column(name = "precio_cerrado_pesos")
    private int precioCerradoPesos = 0;

    /**
     * Tipos de precio. Precio abierto pesos.
     */
    @Column(name = "precio_abierto_pesos")
    private int precioAbiertoPesos = 0;

    /**
     * Precio de referencia (solo trigo).
     */
    @Column(name = "precio_referencia")
    private int precioReferencia = 0;

    /*
    *Cantidad de contratos.
     */
    @Column(name = "cantidad_contratos", nullable = false)
    private int cantidadContratos = 0;

    /**
     * Los tipos de precios elegidos.
     */
    @Transient
    private List<TipoPrecio> tiposPrecio = new ArrayList<>();

    /**
     * Las coberturas seleccionadas en el pre-registro.
     */
    @OneToMany(mappedBy = "preRegistro")
    private List<CoberturaSeleccionada> coberturas;

    /**
     * El nombre del beneficiario.
     */
    @Column(name = "nombre_beneficiario", nullable = false)
    private String nombreBeneficiario;

    /**
     * Los apellidos del beneficiario.
     */
    @Column(name = "apellidos_beneficiario", nullable = false)
    private String apellidosBeneficiario;

    /**
     * La CURP del beneficiario.
     */
    @Column(name = "curp_beneficiario", nullable = false, length = 63)
    private String curpBeneficiario;

    /**
     * El parentesco del beneficiario.
     */
    @ManyToOne
    @JoinColumn(name = "parentesco_id", nullable = false)
    private Parentesco parentesco;


    /**
     * El estado donde el {@link PreRegistroProductor} tiene más superficie.
     */
    @ManyToOne
    @JoinColumn(name = "estado_id")
    private Estado estado;


    @Transient
    public String getEtiquetaCoberturas() {
        if (Objects.isNull(coberturas) || coberturas.isEmpty()) {
            return "--";
        }

        List<String> etiqueta = new ArrayList<>();
        Collections.sort(coberturas, (CoberturaSeleccionada c1, CoberturaSeleccionada c2) -> c1.getOrden().compareTo(c2.getOrden()));
        for (CoberturaSeleccionada cs : coberturas) {
            etiqueta.add(cs.getEntidadCobertura().getNombre());
        }
        return String.join(", ", etiqueta);
    }

}
