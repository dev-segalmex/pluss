/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.factura;

import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;

/**
 * Clase que representa aquellas facturas que están restringidas y se permite
 * que las puedan cargar para cobrarlas.
 *
 * @author ismael
 */
@Entity
@Table(name = "factura_restringida")
@Getter
@Setter
public class FacturaRestringida extends AbstractEntidad {

    /**
     * El {@link Usuario} que registra esta {@link facturaRestringida}.
     */
    @ManyToOne
    @JoinColumn(name = "usuario_registra_id", nullable = false)
    private Usuario usuarioRegistra;

    /**
     * El estatus de este {@link facturaRestringida}.
     */
    @Column(name = "activo")
    private boolean activo = true;

    /**
     * El UUID del timbre fiscal digital de la factura restringida.
     */
    @Column(name = "uuid_timbre_fiscal_digital")
    private String uuidTimbreFiscalDigital;

    /**
     * El cultivo para el que aplica la factura.
     */
    @ManyToOne
    @JoinColumn(name = "cultivo_id")
    private Cultivo cultivo;

    /**
     * El ciclo agrícola para el que aplica la factura.
     */
    @ManyToOne
    @JoinColumn(name = "ciclo_id")
    private CicloAgricola ciclo;

    /**
     * El usuario que desactiva el {@link facturaRestringida}.
     */
    @ManyToOne
    @JoinColumn(name = "usuario_baja_id")
    private Usuario usuarioBaja;

    /**
     * La fecha en que se desactiva el {@link facturaRestringida}.
     */
    @Column(name = "fecha_baja")
    @Temporal(TemporalType.DATE)
    private Calendar fechaBaja;

}
