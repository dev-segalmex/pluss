/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.inscripcion;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "inscripcion_contrato_estado")
@Getter
@Setter
public class InscripcionContratoEstado extends AbstractEntidad {

    @ManyToOne
    @JoinColumn(name = "inscripcion_id", nullable = false)
    private InscripcionContrato inscripcion;

    @ManyToOne
    @JoinColumn(name = "estado_id", nullable = false)
    private Estado estado;

    private int orden;

    private boolean activo = true;

}
