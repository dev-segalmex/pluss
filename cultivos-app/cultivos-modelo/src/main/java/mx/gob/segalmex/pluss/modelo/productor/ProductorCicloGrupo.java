/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.productor;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "productor_ciclo_grupo")
@Getter
@Setter
public class ProductorCicloGrupo extends AbstractEntidad {

    /**
     * El productor ciclo al que pertenece este grupo.
     */
    @ManyToOne
    @JoinColumn(name = "productor_ciclo_id", nullable = false)
    private ProductorCiclo productorCiclo;

    /**
     * El grupo de estímulo.
     */
    @Column(name = "grupo_estimulo", nullable = false)
    private String grupoEstimulo;

    /**
     * Las toneladas totales declaradas con base a sus predios del grupo especificado.
     */
    @Column(name = "toneladas_totales", nullable = false, precision = 19, scale = 3)
    private BigDecimal toneladasTotales = BigDecimal.ZERO;

    /**
     * Las toneladas facturadas del grupo especificado.
     */
    @Column(name = "toneladas_facturadas", nullable = false, precision = 19, scale = 3)
    private BigDecimal toneladasFacturadas = BigDecimal.ZERO;

    /**
     * Las toneladas que se comprometieron en un contrato del grupo especificado.
     */
    @Column(name = "toneladas_contratadas", nullable = false, precision = 19, scale = 3)
    private BigDecimal toneladasContratadas = BigDecimal.ZERO;

    /**
     * Las toneladas que cuentan con cobertura del grupo especificado.
     */
    @Column(name = "toneladas_cobertura", nullable = false, precision = 19, scale = 3)
    private BigDecimal toneladasCobertura = BigDecimal.ZERO;

    /**
     * El límite general.
     */
    @Column(name = "limite", nullable = false, precision = 19, scale = 3)
    private BigDecimal limite = BigDecimal.ZERO;

}
