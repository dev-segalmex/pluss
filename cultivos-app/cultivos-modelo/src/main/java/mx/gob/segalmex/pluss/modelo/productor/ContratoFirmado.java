/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.productor;

import java.math.BigDecimal;
import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.Transient;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoCultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoPrecio;
import mx.gob.segalmex.pluss.modelo.inscripcion.EntidadCancelable;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;

/**
 * Clase que representa un contrato firmado por un productor para un cultivo y
 * un ciclo específico.
 *
 * @author ismael
 */
@Entity
@Table(name = "contrato_firmado")
@Getter
@Setter
public class ContratoFirmado extends AbstractEntidad implements EntidadCancelable {

    /**
     * El número del contrato firmado.
     */
    @Column(name = "numero_contrato")
    private String numeroContrato;

    /**
     * El folio del contrato firmado.
     */
    @Column(name = "folio")
    private String folio;

    /**
     * El nombre de la empresa que dio de alta el formato.
     */
    @Column(name = "empresa")
    private String empresa;

    /**
     * La cantidad contratada entre el productor y el contrato.
     */
    @Column(name = "cantidad_contratada")
    private BigDecimal cantidadContratada;

    /**
     * La cantidad contratada que cuenta con cobertura.
     */
    @Column(name = "cantidad_contratada_cobertura")
    private BigDecimal cantidadContratadaCobertura;

    /**
     * La inscripción del productor a la que pertenece este contrato.
     */
    @ManyToOne
    @JoinColumn(name = "inscripcion_productor_id", nullable = false)
    private InscripcionProductor inscripcionProductor;

    /**
     * El archivo del contrato.
     */
    @Transient
    private Archivo archivo;

    /**
     * El estatus de inscripción del contrato.
     */
    @Transient
    private EstatusInscripcion estatus;

    /**
     * El orden del contrato.
     */
    @Column(name = "orden")
    private Integer orden;

    /**
     * La fecha en que el contrato queda cancelado para el registro del
     * productor.
     */
    @Column(name = "fecha_cancelacion", nullable = true)
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fechaCancelacion;

    /**
     * Indica si el contrato fue corregido durante el proceso de validación.
     */
    @Transient
    private boolean corregido = false;

    /**
     * El tipo de precio de InscripcionContrato.
     */
    @ManyToOne
    @JoinColumn(name = "tipo_precio_id", nullable = false)
    private TipoPrecio tipoPrecio;

    /**
     * El tipo de cultivo del contrato.
     */
    @ManyToOne
    @JoinColumn(name = "tipo_cultivo_id", nullable = false)
    private TipoCultivo tipoCultivo;

}
