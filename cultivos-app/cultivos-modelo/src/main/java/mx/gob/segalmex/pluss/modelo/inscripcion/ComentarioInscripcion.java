/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.inscripcion;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "comentario_inscripcion")
@Getter
@Setter
public class ComentarioInscripcion extends AbstractEntidad {

    /**
     * La clase a la cual pertenece el comentario.
     */
    @Column(name = "clase", nullable = false)
    private String clase;

    /**
     * La referencia a la cual pertenece el comentario.
     */
    @Column(name = "referencia", nullable = false)
    private String referencia;

    /**
     * El tipo de comentario.
     */
    @Column(name = "tipo")
    private String tipo;

    /**
     * El usuario que registra el comentario.
     */
    @ManyToOne
    @JoinColumn(name = "usuario_id", nullable = false)
    private Usuario usuario;

    /**
     * El contenido del comentario.
     */
    @Column(name = "contenido", length = 2047)
    private String contenido;

    /**
     * Indica si el comentario se encuentra activo o no.
     */
    @Column(name = "activo")
    private boolean activo = true;

}
