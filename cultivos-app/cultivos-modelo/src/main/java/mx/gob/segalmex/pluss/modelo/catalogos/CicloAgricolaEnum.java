/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.catalogos;

import mx.gob.segalmex.pluss.modelo.base.EnumCatalogo;

/**
 *
 * @author ismael
 */
public enum CicloAgricolaEnum implements EnumCatalogo {

    OI2020("oi-2020"),

    PV2020("pv-2020"),

    OI2021("oi-2021"),

    PV2021("pv-2021"),

    OI2022("oi-2022"),

    PV2022("pv-2022");

    private final String clave;

    private CicloAgricolaEnum(String clave) {
        this.clave = clave;
    }

    @Override
    public String getClave() {
        return clave;
    }

    public static CicloAgricolaEnum getInstance(String clave) {
        for (CicloAgricolaEnum e : CicloAgricolaEnum.values()) {
            if (e.getClave().equals(clave)) {
                return e;
            }
        }

        throw new IllegalArgumentException("No existe un ciclo agrícola con clave: " + clave);
    }
}
