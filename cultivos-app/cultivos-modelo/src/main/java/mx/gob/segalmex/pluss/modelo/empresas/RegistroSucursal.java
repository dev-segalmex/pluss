/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.empresas;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.productor.Domicilio;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "registro_sucursal")
@Getter
@Setter
public class RegistroSucursal extends AbstractEntidad {

    /**
     * El registro empresa al que pertenece esta sucursal.
     */
    @ManyToOne
    @JoinColumn(name = "registro_empresa_id", nullable = false)
    private RegistroEmpresa registroEmpresa;

    /**
     * El nombre de la sucursal.
     */
    @Column(name = "nombre", nullable = false)
    private String nombre;

    /**
     * La dirección de la sucursal.
     */
    @ManyToOne
    @JoinColumn(name = "domicilio_id", nullable = false)
    private Domicilio domicilio;

    /**
     * La latitud en la que se ubica la sucursal.
     */
    @Column(name = "latitud", scale = 6, nullable = false)
    private BigDecimal latitud;

    /**
     * La longitud en la que se ubica la sucursal.
     */
    @Column(name = "longitud", scale = 6, nullable = false)
    private BigDecimal longitud;

    /**
     * El nombre de la persona que funje como enlace con SEGALMEX.
     */
    @Column(name = "nombre_enlace", nullable = false)
    private String nombreEnlace;

    /**
     * El número célular del enlace con SEGALMEX.
     */
    @Column(name = "numero_celular_enlace", nullable = false)
    private String numeroCelularEnlace;

    /**
     * El correo electrónico del enlace con SEGALMEX.
     */
    @Column(name = "correo_enlace", nullable = false)
    private String correoEnlace;

    /**
     * El orden los datos de RegistroSucursal.
     */
    @Column(name = "orden", nullable = false)
    private int orden;

}
