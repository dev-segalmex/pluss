/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.seguridad;

import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;

/**
 *
 * @author ismael
 */
public interface UsuarioSesionHolder {

    /**
     * Regresa el usuario registrado en sesión.
     *
     * @return el usuario registrado en sesión.
     */
    Usuario getUsuario();
}
