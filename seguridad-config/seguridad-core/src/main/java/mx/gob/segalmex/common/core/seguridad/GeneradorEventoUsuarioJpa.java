/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.seguridad;

import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.pluss.modelo.seguridad.EventoUsuario;
import mx.gob.segalmex.pluss.modelo.seguridad.TipoEventoEnum;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * Implementación de la interfaz {@link GeneradorEventoUsuario}.
 *
 * @author oscar
 */
@Slf4j
@Service("generadorEventoUsuario")
public class GeneradorEventoUsuarioJpa implements GeneradorEventoUsuario {

    /**
     * Para el manejo de entidades.
     */
    @Autowired
    private RegistroEntidad registroEntidad;

    @Override
    @Transactional
    public void genera(Usuario usuario, TipoEventoEnum tipo, String sistema) {
        LOGGER.debug("Generando evento de usuario del tipo {}", tipo.getClave());
        EventoUsuario evento = new EventoUsuario();
        evento.setUsuario(usuario);
        evento.setTipo(tipo.getClave());
        evento.setSistema(sistema);
        registroEntidad.guarda(evento);
    }

}
