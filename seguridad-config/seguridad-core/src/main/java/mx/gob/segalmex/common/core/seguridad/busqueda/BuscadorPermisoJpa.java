/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.seguridad.busqueda;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.seguridad.Accion;
import mx.gob.segalmex.pluss.modelo.seguridad.Area;
import mx.gob.segalmex.pluss.modelo.seguridad.EstatusPermisoEnum;
import mx.gob.segalmex.pluss.modelo.seguridad.Permiso;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ismael
 */
@Slf4j
@Repository("buscadorPermiso")
public class BuscadorPermisoJpa implements BuscadorPermiso {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Permiso> buscaAutorizados(Usuario usuario, Area area) {
        String s = "SELECT p "
                + "FROM Permiso p "
                + "WHERE "
                + "p.usuario = :usuario "
                + "AND p.estatus = :estatus "
                + "AND p.fechaCancelacion IS NULL "
                + "AND p.usuarioCancela IS NULL ";
        Map<String, Object> params = new HashMap<>();
        params.put("usuario", usuario);
        params.put("estatus", EstatusPermisoEnum.ACTIVO.getClave());

        TypedQuery<Permiso> q = entityManager.createQuery(s, Permiso.class);
        QueryUtils.setParametros(q, params);

        // Obtenemos todos los permisos activos del usuario
        List<Permiso> permisos = q.getResultList();

        // Si no se especificó área, son todos
        if (Objects.isNull(area)) {
            LOGGER.debug("El número de permisos autorizados: {}", permisos.size());
            return permisos;
        }

        // Nos quedamos con los globlales y del área especificada
        List<Permiso> filtrados = new ArrayList<>();
        for (Permiso p : permisos) {
            if (Objects.isNull(p.getArea())) {
                filtrados.add(p);
                continue;
            }
            if (p.getArea().equals(area)) {
                filtrados.add(p);
            }
        }

        LOGGER.debug("El número de permisos autorizados: {}", filtrados.size());
        return filtrados;
    }

    @Override
    public List<Permiso> busca(ParametrosPermiso parametros) {
        LOGGER.debug("Buscando Permisos por parámetros...");
        StringBuilder sb = new StringBuilder();
        sb.append(" SELECT p FROM Permiso p ");
        Map<String, Object> params = new HashMap<>();
        boolean primero = true;

        if (Objects.nonNull(parametros.getId())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "p.id", "id", parametros.getId(), params);
        }

        if (Objects.nonNull(parametros.getUsuario())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "p.usuario", "usuario", parametros.getUsuario(), params);
        }

        if (Objects.nonNull(parametros.getAccion())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "p.accion", "accion", parametros.getAccion(), params);
        }

        if (Objects.nonNull(parametros.getActivos()) && parametros.getActivos()) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            sb.append(" p.fechaCancelacion IS NULL ");
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            sb.append(" p.usuarioCancela IS NULL ");
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "p.estatus", "estatus", EstatusPermisoEnum.ACTIVO.getClave(), params);
        }

        if (Objects.nonNull(parametros.getArea())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "p.area", "area", parametros.getArea(), params);
        }

        if (Objects.nonNull(parametros.getGlobal())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            if (parametros.getGlobal()) {
                sb.append("p.area IS NULL ");
            } else {
                sb.append("p.area IS NOT NULL ");
            }
        }

        TypedQuery<Permiso> query = entityManager.createQuery(sb.toString(), Permiso.class);
        QueryUtils.setParametros(query, params);
        LOGGER.debug("Ejecutando query: {}", sb);
        long inicio = System.currentTimeMillis();
        List<Permiso> permisos = query.getResultList();
        LOGGER.debug("Se encontraron {} Permisos en {} ms.", permisos.size(),
                System.currentTimeMillis() - inicio);
        return permisos;
    }

    @Override
    public Permiso buscaElemento(ParametrosPermiso parametros) {
        LOGGER.debug("BuscandoElemento Permiso por parámetros...");
        List<Permiso> permisos = busca(parametros);

        if (permisos.isEmpty()) {
            throw new EmptyResultDataAccessException("No existe permiso con los parámetros "
                    + "especificados.", 1);
        }

        if (permisos.size() > 1) {
            throw new IncorrectResultSizeDataAccessException("Existe más de un permiso con los"
                    + " parámetros especificados.", 1);
        }

        return permisos.get(0);

    }

    @Override
    public boolean isAutorizadaGlobal(Accion accion, Usuario usuario) {
        ParametrosPermiso params = new ParametrosPermiso();
        params.setAccion(accion);
        params.setGlobal(Boolean.TRUE);
        params.setUsuario(usuario);
        params.setActivos(Boolean.TRUE);
        try {
            buscaElemento(params);
        } catch (EmptyResultDataAccessException empty) {
            return false;
        }
        return true;
    }

    @Override
    public List<Area> getAreasAutorizadas(Usuario usuario, Accion accion, List<Area> objetivo,
            boolean verificarObjetivo) {
        if (isAutorizadaGlobal(accion, usuario)) {
            //si tiene permiso global regresamos null
            return null;
        }

        //obtenemos las areas autorizadas
        List<Area> autorizadas = buscaAreasAutorizadas(usuario, accion);
        if (!verificarObjetivo) {
            // si no se verifica el objetivo nos limitamos a regresar las áreas autorizadas.
            return autorizadas;
        } else {
            // regresamos la intersección del objetivo vs las areas que si tiene permiso el usuario.
            objetivo.retainAll(autorizadas);
            return objetivo;
        }
    }

    private List<Area> buscaAreasAutorizadas(Usuario usuario, Accion accion) {
        LOGGER.debug("Buscando Areas por parámetros...");

        TypedQuery<Area> q = entityManager
                .createQuery("SELECT p.area "
                        + "FROM Permiso p "
                        + "WHERE "
                        + "p.usuario = :usuario "
                        + "AND p.estatus = :estatus "
                        + "AND p.accion = :accion "
                        + "AND p.fechaCancelacion IS NULL "
                        + "AND p.usuarioCancela IS NULL "
                        + "AND p.area IS NOT NULL ", Area.class)
                .setParameter("usuario", usuario)
                .setParameter("accion", accion)
                .setParameter("estatus", EstatusPermisoEnum.ACTIVO.getClave());
        return q.getResultList();
    }

    @Override
    public List<Usuario> buscaUsuarioAutorizado(Accion accion, Boolean global) {
        ParametrosPermiso parametros = new ParametrosPermiso();
        parametros.setAccion(accion);
        parametros.setActivos(Boolean.TRUE);
        parametros.setGlobal(global);

        List<Permiso> permisos = busca(parametros);
        List<Usuario> usuarios = new ArrayList<>();
        for (Permiso p : permisos) {
            usuarios.add(p.getUsuario());
        }

        return usuarios;
    }

}
