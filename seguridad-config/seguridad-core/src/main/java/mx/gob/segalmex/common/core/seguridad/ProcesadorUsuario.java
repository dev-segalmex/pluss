/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.seguridad;

import mx.gob.segalmex.pluss.modelo.personas.Persona;
import mx.gob.segalmex.pluss.modelo.seguridad.Area;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;

/**
 *
 * @author ismael
 */
public interface ProcesadorUsuario {

    /**
     * Se encarga actualizar el password del usuario, siempre que todo sea correcto.
     *
     * @param usuario el usuario al que se le actualiza el password.
     * @param passwordActual indicado por el usuario, debe coincidircon el actual.
     * @param passwordNuevo el password nuevo.
     */
    void actualizaPassword(Usuario usuario, String passwordActual, String passwordNuevo);

    /**
     * Se encarga de establecer un password para el usuario indicado.
     *
     * @param usuario el usuario al que se le actualiza el password.
     * @param password el password nuevo.
     */
    void actualizaPassword(Usuario usuario, String password);

    /**
     * Se encarga de actualizar el área de ubicación del usuario.
     *
     * @param usuario el usuario para actualizar su área de ubicación.
     * @param areaUbicacion la nueva área de ubicación del usuario.
     */
    void actualizaAreaUbicacion(Usuario usuario, Area areaUbicacion);

    /**
     * Guarda un usuario.
     *
     * @param usuario el usuario a registrar.
     * @param persona los datos personales del usuario.
     */
    void guarda(Usuario usuario, Persona persona);

    /**
     * Actualiza los datos personales de un usuario.
     *
     * @param usuario el usuario al que se le actualiza la información.
     * @param persona los datos personales del usuario.
     */
    void actualiza(Usuario usuario, Persona persona);

    /**
     * Se encarga de bloquear/desbloquear al usuario recibido.
     *
     * @param usuario el usuario al que se cambia su estatus a bloqueado ó desbloqueado.
     * @param bloqueado indica si debe ser bloqueado <code>true</code> o desbloqueado
     * <code>false</code>.
     */
    void bloquea(Usuario usuario, boolean bloqueado);

}
