/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.seguridad.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.seguridad.Accion;
import mx.gob.segalmex.pluss.modelo.seguridad.Area;
import mx.gob.segalmex.pluss.modelo.seguridad.Permiso;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;

/**
 *
 * @author ismael
 */
public interface BuscadorPermiso {

    /**
     * Busca los permisos autorizados de un usuario.
     *
     * @param usuario el usuario del que se buscan los permisos.
     * @param area el área en la que se buscan los permisos.
     * @return la lista de permisos autorizados.
     */
    List<Permiso> buscaAutorizados(Usuario usuario, Area area);

    /**
     * Busca una lista de permisos que cumplan con los filtros especificados en los párametros
     * recibidos.
     *
     * @param parametros los filtros a aplicar.
     * @return la lista de permisos que cumplen con los filtros.
     */
    List<Permiso> busca(ParametrosPermiso parametros);

    /**
     * Se encarga de obtener solo un ejemplar de {@link Permiso} que cumpla con los paramtros
     * recibidos.
     *
     * @param parametros los parametros para filtrar la busqueda.
     * @return el Permiso que se encuentra con los parametros aplicados.
     */
    Permiso buscaElemento(ParametrosPermiso parametros);

    /**
     * Permite saber si para una {@link Accion} y un {@link Usuario} el permiso es global.
     *
     * @param accion la accion que deseamos saber si es global.
     * @param usuario el usuario ligado a la acción.
     * @return true si es autorizada global, false si no.
     */
    boolean isAutorizadaGlobal(Accion accion, Usuario usuario);

    /**
     * Permite recuperar las áreas en las cuales determinado {@link Usuario} tiene permiso de
     * realizar cierta {@link Accion}. En adicional permite comparar vs una lista objetivo de
     * {@link Area} sobre las cuales presuntamente el {@link Usuario} cree tener permiso.
     *
     * @param usuario el {@link Usuario} que se desea conocer sus áreas autorizadas.
     * @param accion la {@link Accion} sobre la cual se requiere saber los permisos.
     * @param objetivo la lista de {@link Area} vs la cual comparar las {@link Area} con permiso
     * reales contra los que cree tener el usuario.
     * @param verificarObjetivo bandera que define si queremos realizar o no el cruce de objetivo vs
     * reales.
     * @return Si verificarObjetivo es false, regresa lista con las {@link Area} autorizadas.En caso
     * de que no existan permisos regresa una lista vacía. Si verificarObjetivo es true, regresa el
     * cruce de {@link Area} con permisos que sí tiene el usuario vs la lista de {@link Area}
     * objetivos. En caso de que no exista dicha interseccion regresa una lista vacía. Regresa null
     * si el usuario tiene permiso global.
     */
    List<Area> getAreasAutorizadas(Usuario usuario, Accion accion, List<Area> objetivo, boolean verificarObjetivo);

    List<Usuario> buscaUsuarioAutorizado(Accion accion, Boolean global);

}
