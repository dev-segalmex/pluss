/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.seguridad;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.common.core.persona.procesador.ProcesadorPersona;
import mx.gob.segalmex.pluss.modelo.personas.Persona;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersona;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersonaEnum;
import mx.gob.segalmex.pluss.modelo.seguridad.Area;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ismael
 */
@Slf4j
@Service("procesadorUsuario")
public class ProcesadorUsuarioJpa implements ProcesadorUsuario {

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private ProcesadorPersona procesadorPersona;

    @Transactional
    @Override
    public void actualizaPassword(Usuario usuario, String passwordActual, String passwordNuevo) {
        LOGGER.debug("Actualizando contraseña para el usuario {}.", usuario.getClave());
        List<String> motivos = new ArrayList<>();
        if (!BCrypt.checkpw(passwordActual, usuario.getPassword())) {
            LOGGER.error("La constraseña actual es incorrecta.");
            motivos.add("La constraseña actual es incorrecta.");
        }

        if (Objects.isNull(StringUtils.trimToNull(passwordNuevo))) {
            LOGGER.error("La contraseña no puede ser vacía.");
            motivos.add("La contraseña no puede ser vacía.");
        }

        if (StringUtils.equals(passwordActual, passwordNuevo)) {
            LOGGER.error("La contraseña nueva no puede ser la misma que la actual.");
            motivos.add("La contraseña nueva no puede ser la misma que la actual.");
        }

        if (!motivos.isEmpty()) {
            throw new SegalmexRuntimeException("No es posible actualizar la contraseña por los "
                    + "siguientes motivos:", motivos);
        }

        actualizaPassword(usuario, passwordNuevo);
    }

    @Transactional
    @Override
    public void actualizaAreaUbicacion(Usuario usuario, Area areaUbicacion) {
        LOGGER.debug("Actualizando área de ubicación para el usuario {}.", usuario.getClave());
        usuario.setAreaUbicacion(areaUbicacion);
        registroEntidad.actualiza(usuario);
        LOGGER.debug("Ubicación actualizada.");
    }

    @Override
    @Transactional
    public void guarda(Usuario usuario, Persona personaNueva) {
        LOGGER.debug("Guardando persona {}.", personaNueva.getNombre());
        Persona persona = procesadorPersona.procesa(personaNueva, buscadorCatalogo
                .buscaElemento(TipoPersona.class, TipoPersonaEnum.USUARIO.getClave()));
        usuario.setPersona(persona);
        usuario.setFechaActivo(Calendar.getInstance());
        usuario.setActivo(true);
        usuario.setClave(usuario.getClave().toLowerCase());
        usuario.setNombre(usuario.getClave());
        registroEntidad.guarda(usuario);
    }

    @Override
    @Transactional
    public void actualiza(Usuario usuario, Persona personaNueva) {
        LOGGER.debug("Actualizando usuario {}.", usuario.getClave());
        Persona persona = procesadorPersona.procesa(personaNueva, buscadorCatalogo
                .buscaElemento(TipoPersona.class, TipoPersonaEnum.USUARIO.getClave()));
        usuario.setPersona(persona);
        registroEntidad.actualiza(usuario);
    }

    @Override
    @Transactional
    public void actualizaPassword(Usuario usuario, String password) {
        usuario.setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
        registroEntidad.actualiza(usuario);
        LOGGER.debug("Contraseña actualizada.");
    }

    @Override
    @Transactional
    public void bloquea(Usuario usuario, boolean bloqueado) {
        usuario.setFechaBloqueo(bloqueado ? Calendar.getInstance() : null);
        registroEntidad.actualiza(usuario);
        LOGGER.debug("Se ha {} al usuario {}.", (bloqueado ? "bloqueado" : "desbloqueado"),
                usuario.getNombre());
    }

}
