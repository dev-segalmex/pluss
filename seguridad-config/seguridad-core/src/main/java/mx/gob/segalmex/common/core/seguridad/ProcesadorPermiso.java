/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.seguridad;

import mx.gob.segalmex.pluss.modelo.seguridad.Permiso;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;

/**
 *
 * @author ismael
 */
public interface ProcesadorPermiso {

    /**
     * Se encarga de guardar un nuevo {@link Permiso} asociado al {@link Usuario} recibido.
     *
     * @param usuario el usuario que tendra el nuevo permiso.
     * @param permiso el permiso a crear.
     * @param usuarioAutoriza el usuario que otorga el permiso.
     */
    void agrega(Usuario usuario, Permiso permiso, Usuario usuarioAutoriza);

    /**
     * Se encarga de eliminar (solo se desactiva) el permiso.
     *
     * @param permiso el permiso a eliminar.
     * @param usuarioCancela el usuario que cancela/elimina el permiso.
     */
    void elimina(Permiso permiso, Usuario usuarioCancela);

}
