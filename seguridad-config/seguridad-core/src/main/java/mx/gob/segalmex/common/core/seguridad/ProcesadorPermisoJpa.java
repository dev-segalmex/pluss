/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.seguridad;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.common.core.seguridad.busqueda.BuscadorPermiso;
import mx.gob.segalmex.common.core.seguridad.busqueda.ParametrosPermiso;
import mx.gob.segalmex.pluss.modelo.seguridad.Accion;
import mx.gob.segalmex.pluss.modelo.seguridad.Area;
import mx.gob.segalmex.pluss.modelo.seguridad.EstatusPermisoEnum;
import mx.gob.segalmex.pluss.modelo.seguridad.Permiso;
import mx.gob.segalmex.pluss.modelo.seguridad.TipoAccionEnum;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ismael
 */
@Slf4j
@Service("procesadorPermiso")
public class ProcesadorPermisoJpa implements ProcesadorPermiso {

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorPermiso buscadorPermiso;

    @Override
    @Transactional
    public void agrega(Usuario usuario, Permiso permiso, Usuario usuarioAutoriza) {
        LOGGER.debug("Creando permiso para {}", usuario.getClave());
        permiso.setUsuario(usuario);
        permiso.setUsuarioAutoriza(usuarioAutoriza);
        permiso.setFechaAutorizacion(Calendar.getInstance());
        permiso.setEstatus(EstatusPermisoEnum.ACTIVO.getClave());
        permiso.setAccion(buscadorCatalogo.buscaElemento(Accion.class, permiso.getAccion().getId()));
        List<String> motivos = new ArrayList<>();
        if (StringUtils.equals(permiso.getAccion().getTipoAccion(), TipoAccionEnum.LOCAL.getClave())
                && Objects.isNull(permiso.getArea())) {
            LOGGER.debug("Error no se puede tener un permiso local sin un área.");
            motivos.add("No se puede generar un permiso local sin un área.");

            throw new SegalmexRuntimeException("No es posible guardar el permiso por los siguientes "
                    + "motivos:", motivos);

        }

        if (StringUtils.equals(permiso.getAccion().getTipoAccion(), TipoAccionEnum.GLOBAL.getClave())
                && Objects.nonNull(permiso.getArea())) {
            LOGGER.debug("Error no se puede tener un permiso global con un área.");
            motivos.add("No se puede tener un permiso global con un área.");

            throw new SegalmexRuntimeException("No es posible guardar el permiso por los siguientes "
                    + "motivos:", motivos);

        }
        permiso.setArea(Objects.nonNull(permiso.getArea())
                ? buscadorCatalogo.buscaElemento(Area.class, permiso.getArea().getId()) : null);

        if (existe(permiso)) {
            LOGGER.debug("Error el permiso ya existe para este usuario.");
            motivos.add("El usuario ya cuenta con este permiso.");

            throw new SegalmexRuntimeException("No es posible guardar el permiso por los siguientes "
                    + "motivos:", motivos);
        } else {
            registroEntidad.guarda(permiso);
        }
    }

    @Override
    @Transactional
    public void elimina(Permiso permiso, Usuario usuarioCancela) {
        LOGGER.debug("Eliminando permiso {} al usuario {}",
                permiso.getAccion().getClave(), permiso.getUsuario().getClave());
        permiso.setEstatus(EstatusPermisoEnum.INACTIVO.getClave());
        permiso.setFechaCancelacion(Calendar.getInstance());
        permiso.setUsuarioCancela(usuarioCancela);
        registroEntidad.actualiza(permiso);
    }

    /**
     * Se encarga de verificar si el permiso ya existe en BD, con sus mismas propiedades (usuario,
     * acción, area y estatus).
     *
     * @param permiso
     * @return Si existe true y false en el otro caso.
     */
    private boolean existe(Permiso permiso) {
        LOGGER.debug("Verificando si existe permiso.");
        ParametrosPermiso parametros = new ParametrosPermiso();
        parametros.setUsuario(permiso.getUsuario());
        parametros.setAccion(permiso.getAccion());
        parametros.setActivos(Boolean.TRUE);
        parametros.setArea(permiso.getArea());

        List<Permiso> encontrados = buscadorPermiso.busca(parametros);
        for (Permiso encontrado : encontrados) {
            // Las dos áreas son nulas
            if (Objects.isNull(permiso.getArea()) && Objects.isNull(encontrado.getArea())) {
                return true;
            }

            // Las dos áreas no son nulas e iguales
            if (Objects.nonNull(permiso.getArea()) && Objects.nonNull(encontrado.getArea())) {
                return true;
            }
        }
        return false;
    }

}
