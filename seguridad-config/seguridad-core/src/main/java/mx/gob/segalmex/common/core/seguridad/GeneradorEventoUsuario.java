/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.seguridad;

import mx.gob.segalmex.pluss.modelo.seguridad.EventoUsuario;
import mx.gob.segalmex.pluss.modelo.seguridad.TipoEventoEnum;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;

/**
 * Interfaz para la definición métodos/sobre sobre ejemplares de
 * {@link EventoUsuario}.
 *
 * @author oscar
 */
public interface GeneradorEventoUsuario {

    /**
     * Se encarga de generar un {@link  EventoUsuario} del tipo recibido y para
     * el usuario recibido.
     *
     * @param usuario el usuario que genera el evento.
     * @param tipo el tipo de evento.
     * @param sistema el sistema desde el cual se esta generando el evento.
     */
    void genera(Usuario usuario, TipoEventoEnum tipo, String sistema);
}
