/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.seguridad.busqueda;

import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.seguridad.Accion;
import mx.gob.segalmex.pluss.modelo.seguridad.Area;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;

/**
 *
 * @author oscar
 */
@Getter
@Setter
public class ParametrosPermiso {

    /**
     * El ide de un permiso.
     */
    private Integer id;

    /**
     * El usuario del permiso.
     */
    private Usuario usuario;

    /**
     * La accion del permiso.
     */
    private Accion accion;

    /**
     * El area del permiso.
     */
    private Area area;

    /**
     * Indica que solo los permisos activos.
     */
    private Boolean activos;

    /**
     * Indica que deseamos saber si es global el permiso.
     */
    private Boolean global;
}
