package mx.gob.segalmex.common.core.seguridad.busqueda;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author ismael
 */
@Getter
@Setter
public class ParametrosUsuario {

    private String nombreUsuario;

    private String nombre;

    private String apellidos;

}
