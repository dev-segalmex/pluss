package mx.gob.segalmex.common.core.seguridad.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;

/**
 *
 * @author ismael
 */
public interface BuscadorUsuario {

    /**
     *
     * @param parametros
     * @return
     */
    List<Usuario> busca(ParametrosUsuario parametros);

    Usuario buscaElemento(String username);

}
