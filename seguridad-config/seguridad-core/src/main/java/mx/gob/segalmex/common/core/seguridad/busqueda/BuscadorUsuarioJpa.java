package mx.gob.segalmex.common.core.seguridad.busqueda;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.persona.util.Transliterador;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ismael
 */
@Slf4j
@Repository("buscadorUsuario")
public class BuscadorUsuarioJpa implements BuscadorUsuario {

    @Autowired
    private Transliterador transliterador;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Usuario> busca(ParametrosUsuario parametros) {
        LOGGER.debug("Buscando Usuarios por parámetros...");

        StringBuilder sb = new StringBuilder();
        sb.append("SELECT u FROM Usuario u WHERE ");
        Map<String, Object> params = new HashMap<>();
        boolean primero = true;

        if (Objects.nonNull(StringUtils.trimToNull(parametros.getNombreUsuario()))) {
            if (!primero) {
                sb.append("OR ");
            }
            primero = false;
            sb.append("u.nombre = :nombreUsuario ");
            params.put("nombreUsuario", parametros.getNombreUsuario());
        }

        if (Objects.nonNull(StringUtils.trimToNull(parametros.getNombre()))) {
            if (!primero) {
                sb.append("OR ");
            }
            primero = false;
            sb.append("u.persona.nombreIcao = :nombre ");
            params.put("nombre", parametros.getNombre());
        }

        if (Objects.nonNull(StringUtils.trimToNull(parametros.getApellidos()))) {
            if (!primero) {
                sb.append("OR ");
            }
            primero = false;
            sb.append("u.persona.apellidosIcao = :apellidos ");
            params.put("apellidos", transliterador.translitera(parametros.getApellidos()));
        }

        TypedQuery<Usuario> query = entityManager.createQuery(sb.toString(), Usuario.class);
        QueryUtils.setParametros(query, params);

        LOGGER.debug("Ejecutando query: {}", sb);
        long inicio = System.currentTimeMillis();
        List<Usuario> usuarios = query.getResultList();
        LOGGER.debug("Se encontraron {} Permisos en {} ms.", usuarios.size(),
                System.currentTimeMillis() - inicio);

        return usuarios;

    }

    @Override
    public Usuario buscaElemento(String username) {
        Objects.requireNonNull(username, "El nombre de usuario no puede ser nulo.");
        ParametrosUsuario parametrosUsuario = new ParametrosUsuario();
        parametrosUsuario.setNombreUsuario(username);

        List<Usuario> usuarios = busca(parametrosUsuario);

        if (usuarios.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontró el usuario con los parámetros "
                    + "indicados.", 1);
        }

        if (usuarios.size() > 1) {
            throw new IncorrectResultSizeDataAccessException("Existe más de un el usuario con los "
                    + "parámetros especificados.", 1);
        }

        return usuarios.get(0);

    }

}
