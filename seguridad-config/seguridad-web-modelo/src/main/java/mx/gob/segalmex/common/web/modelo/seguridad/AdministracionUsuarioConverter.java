package mx.gob.segalmex.common.web.modelo.seguridad;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.pluss.modelo.catalogos.Pais;
import mx.gob.segalmex.pluss.modelo.personas.Sexo;
import mx.gob.segalmex.pluss.modelo.seguridad.Area;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.SexoConverter;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "administracion")
public class AdministracionUsuarioConverter {

    private List<Sexo> sexo = new ArrayList<>();

    private List<Area> area = new ArrayList<>();

    private List<Pais> pais = new ArrayList<>();

    @XmlElement
    public List<SexoConverter> getSexo() {
        return CollectionConverter.convert(SexoConverter.class, this.sexo, 1);
    }

    public void setSexo(List<Sexo> sexo) {
        this.sexo = sexo;
    }

    @XmlElement
    public List<AreaConverter> getArea() {
        return CollectionConverter.convert(AreaConverter.class, this.area, 1);
    }

    public void setArea(List<Area> area) {
        this.area = area;
    }

    @XmlElement
    public List<Pais> getPais() {
        return pais;
    }

    public void setPais(List<Pais> pais) {
        this.pais = pais;
    }

}
