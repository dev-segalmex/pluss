/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.seguridad;

import java.util.Calendar;
import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.pluss.modelo.seguridad.Permiso;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;

/**
 *
 * @author oscar
 */
@XmlRootElement(name = "permiso")
public class PermisoConverter extends AbstractEntidadConverter {

    /**
     * La {@link Permiso} a representar en este converter.
     */
    private final Permiso entity;

    public PermisoConverter() {
        entity = new Permiso();
        expandLevel = 1;
    }

    public PermisoConverter(Permiso entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Permiso no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    /**
     * Recupera la entidad representada.
     *
     * @return
     */
    @Override
    public Permiso getEntity() {
        return this.entity;
    }

    public UsuarioConverter getUsuario() {
        return expandLevel > 0 && getEntity().getUsuario() != null
                ? new UsuarioConverter(getEntity().getUsuario(), expandLevel - 1) : null;
    }

    public void setUsuario(UsuarioConverter usuario) {
        getEntity().setUsuario(usuario.getEntity());
    }

    public AccionConverter getAccion() {
        return expandLevel > 0 && getEntity().getAccion() != null
                ? new AccionConverter(getEntity().getAccion(), expandLevel - 1) : null;
    }

    public void setAccion(AccionConverter accion) {
        getEntity().setAccion(accion.getEntity());
    }

    public AreaConverter getArea() {
        return expandLevel > 0 && getEntity().getArea() != null
                ? new AreaConverter(getEntity().getArea(), expandLevel - 1) : null;
    }

    public void setArea(AreaConverter area) {
        getEntity().setArea(area.getEntity());
    }

    public String getEstatus() {
        return getEntity().getEstatus();
    }

    public void setEstatus(String estatus) {
        getEntity().setEstatus(estatus);
    }

    public UsuarioConverter getUsuarioAutoriza() {
        return expandLevel > 0 && getEntity().getUsuarioAutoriza() != null
                ? new UsuarioConverter(getEntity().getUsuarioAutoriza(), expandLevel - 1) : null;
    }

    public void setUsuarioAutoriza(UsuarioConverter usuarioAutoriza) {
        getEntity().setUsuarioAutoriza(usuarioAutoriza.getEntity());
    }

    public Calendar getFechaAutorizacion() {
        return getEntity().getFechaAutorizacion();
    }

    public void setFechaAutorizacion(Calendar fechaAutorizacion) {
        getEntity().setFechaAutorizacion(fechaAutorizacion);
    }

    public UsuarioConverter getUsuarioCancela() {
        return expandLevel > 0 && getEntity().getUsuarioCancela() != null
                ? new UsuarioConverter(getEntity().getUsuarioCancela(), expandLevel - 1) : null;
    }

    public void setUsuarioCancela(UsuarioConverter usuarioCancela) {
        getEntity().setUsuarioCancela(usuarioCancela.getEntity());
    }

    public Calendar getFechaCancelacion() {
        return getEntity().getFechaCancelacion();
    }

    public void setFechaCancelacion(Calendar fechaCancelacion) {
        getEntity().setFechaCancelacion(fechaCancelacion);
    }

}
