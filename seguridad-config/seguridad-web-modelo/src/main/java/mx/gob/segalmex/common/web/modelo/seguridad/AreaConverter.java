/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.seguridad;

import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.pluss.modelo.seguridad.Area;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.EstadoConverter;

/**
 *
 * @author oscar
 */
@XmlRootElement(name = "area")
public class AreaConverter extends AbstractCatalogoConverter {

    /**
     * La {@link Area} a representar en este converter.
     */
    private final Area entity;

    public AreaConverter() {
        entity = new Area();
        expandLevel = 1;
    }

    public AreaConverter(Area entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Area no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    /**
     * Recupera la entidad representada.
     *
     * @return
     */
    @Override
    public Area getEntity() {
        return this.entity;
    }

    @XmlElement
    public EstadoConverter getEstado() {
        return Objects.nonNull(entity.getEstado()) && expandLevel > 0
                ? new EstadoConverter(entity.getEstado(), expandLevel - 1) : null;
    }

}
