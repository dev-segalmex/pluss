/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.seguridad;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import mx.gob.segalmex.pluss.modelo.personas.Persona;
import mx.gob.segalmex.pluss.modelo.seguridad.Permiso;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;
import mx.gob.segalmex.common.web.modelo.persona.PersonaConverter;

/**
 *
 * @author oscar
 */
@XmlRootElement(name = "usuario")
public class UsuarioConverter extends AbstractCatalogoConverter {

    /**
     * La {@link Usuario} a representar en este converter.
     */
    private final Usuario entity;

    /**
     * La contraseña nueva en texto plano del usuario.
     */
    private String plainPassword;

    /**
     * La contraseña actual del usuario en texto plano. Sirve para el cambio de contraseña por parte
     * del usuario.
     */
    private String plainPasswordActual;

    /**
     * La lista de permisos asociados al usuario.
     */
    private List<Permiso> permisos = new ArrayList<>();

    public UsuarioConverter() {
        entity = new Usuario();
        expandLevel = 1;
    }

    public UsuarioConverter(Usuario entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Usuario no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    /**
     * Recupera la entidad representada.
     *
     * @return
     */
    @Override
    public Usuario getEntity() {
        return this.entity;
    }

    /**
     *
     * @return
     */
    @XmlTransient
    public String getPassword() {
        return getEntity().getPassword();
    }

    /**
     *
     * @param password
     */
    public void setPassword(String password) {
        getEntity().setPassword(password);
    }

    /**
     * Recupera a la {@link Persona} de este {@link Usuario}.
     *
     * @return
     */
    public PersonaConverter getPersona() {
        return expandLevel > 0 && getEntity().getPersona() != null
                ? new PersonaConverter(getEntity().getPersona(), expandLevel - 1) : null;
    }

    /**
     * Establece a la {@link Persona} de este {@link Usuario}.
     *
     * @param persona
     */
    public void setPersona(PersonaConverter persona) {
        getEntity().setPersona(persona.getEntity());
    }

    public AreaConverter getAreaUbicacion() {
        return expandLevel > 0 && getEntity().getAreaUbicacion() != null
                ? new AreaConverter(getEntity().getAreaUbicacion(), expandLevel - 1) : null;
    }

    public void setAreaUbicacion(AreaConverter areaUbicacion) {
        getEntity().setAreaUbicacion(areaUbicacion.getEntity());
    }

    public Calendar getFechaActivo() {
        return getEntity().getFechaActivo();
    }

    public void setFechaActivo(Calendar fechaActivo) {
        getEntity().setFechaActivo(fechaActivo);
    }

    public Calendar getFechaExpiracion() {
        return getEntity().getFechaExpiracion();
    }

    public void setFechaExpiracion(Calendar fechaExpiracion) {
        getEntity().setFechaExpiracion(fechaExpiracion);
    }

    public Calendar getFechaBloqueo() {
        return getEntity().getFechaBloqueo();
    }

    public void setFechaBloqueo(Calendar fechaBloqueo) {
        getEntity().setFechaBloqueo(fechaBloqueo);
    }

    public Calendar getFechaExpiracionPassword() {
        return getEntity().getFechaExpiracionPassword();
    }

    public void setFechaExpiracionPassword(Calendar fechaExpiracionPassword) {
        getEntity().setFechaExpiracionPassword(fechaExpiracionPassword);
    }

    public String getPlainPassword() {
        return plainPassword;
    }

    public void setPlainPassword(String plainPassword) {
        this.plainPassword = plainPassword;
    }

    public String getPlainPasswordActual() {
        return plainPasswordActual;
    }

    public void setPlainPasswordActual(String plainPasswordActual) {
        this.plainPasswordActual = plainPasswordActual;
    }

    public List<PermisoConverter> getPermisos() {
        return expandLevel > 0 && Objects.nonNull(this.permisos) && !permisos.isEmpty()
                ? CollectionConverter.convert(PermisoConverter.class,
                        this.permisos, expandLevel - 1) : null;
    }

    public void setPermisos(List<Permiso> permisos) {
        this.permisos = permisos;
    }

    public String getPuesto() {
        return getEntity().getPuesto();
    }

    public void setPuesto(String puesto) {
        getEntity().setPuesto(puesto);
    }

}
