/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.seguridad;

import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.pluss.modelo.seguridad.Accion;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;

/**
 *
 * @author oscar
 */
@XmlRootElement(name = "accion")
public class AccionConverter extends AbstractCatalogoConverter {

    /**
     * La {@link Accion} a representar en este converter.
     */
    private final Accion entity;

    public AccionConverter() {
        entity = new Accion();
        expandLevel = 1;
    }

    public AccionConverter(Accion entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Accion no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    /**
     * Recupera la entidad representada.
     *
     * @return
     */
    @Override
    public Accion getEntity() {
        return this.entity;
    }

    public String getTipoAccion() {
        return getEntity().getTipoAccion();
    }

    public void setTipoAccion(String tipoAccion) {
        getEntity().setTipoAccion(tipoAccion);
    }

    @XmlElement
    public String getSistema() {
        return getEntity().getSistema();
    }

    public String getDescripcion() {
        return getEntity().getDescripcion();
    }

    public void setDescripcion(String descripcion) {
        getEntity().setDescripcion(descripcion);
    }
}
