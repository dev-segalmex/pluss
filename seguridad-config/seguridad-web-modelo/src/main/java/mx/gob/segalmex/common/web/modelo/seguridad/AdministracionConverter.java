/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.seguridad;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.pluss.modelo.seguridad.Accion;
import mx.gob.segalmex.pluss.modelo.seguridad.Area;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "administracion-usuarios")
public class AdministracionConverter {

    private List<Usuario> usuarios = new ArrayList<>();

    private List<Area> areas = new ArrayList<>();

    private List<Accion> locales = new ArrayList<>();

    private List<Accion> globales = new ArrayList<>();

    @XmlElement
    public List<UsuarioConverter> getUsuarios() {
        return CollectionConverter.convert(UsuarioConverter.class, this.usuarios, 2);
    }

    public void setUsuarios(List<Usuario> usuarios) {
        this.usuarios = usuarios;
    }

    @XmlElement
    public List<AreaConverter> getAreas() {
        return CollectionConverter.convert(AreaConverter.class, this.areas, 1);
    }

    public void setAreas(List<Area> areas) {
        this.areas = areas;
    }

    @XmlElement
    public List<AccionConverter> getLocales() {
        return CollectionConverter.convert(AccionConverter.class, this.locales, 1);
    }

    public void setLocales(List<Accion> locales) {
        this.locales = locales;
    }

    @XmlElement
    public List<AccionConverter> getGlobales() {
        return CollectionConverter.convert(AccionConverter.class, this.globales, 1);
    }

    public void setGlobales(List<Accion> globales) {
        this.globales = globales;
    }

}
