/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.common.web.seguridad;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.util.StringUtils;

/**
 *
 * @author cuecho
 */
@Slf4j
public class AuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {

  private RequestCache requestCache = new HttpSessionRequestCache();

  /**
   * {@inheritDoc}
   */
  @Override
  public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
          Authentication authentication) throws ServletException, IOException {

    if (request == null) {
      return;
    }

    SavedRequest savedRequest = requestCache.getRequest(request, response);

    if (savedRequest == null) {
      LOGGER.debug("SAVED REQUEST NULL ENVIANDO AL PADRE");
      super.onAuthenticationSuccess(request, response, authentication);
      return;
    }

    if (isAlwaysUseDefaultTargetUrl() || (getTargetUrlParameter() != null
            && StringUtils.hasText(request.getParameter(getTargetUrlParameter())))) {
      LOGGER.debug("SIEMPRE ENVIANDO AL DEFAULT URL");
      requestCache.removeRequest(request, response);
      super.onAuthenticationSuccess(request, response, authentication);
      return;
    }

    clearAuthenticationAttributes(request);

    // Use the DefaultSavedRequest URL
    String targetUrl = savedRequest.getRedirectUrl();
    String context = request.getContextPath();
    LOGGER.debug("CONTEXT {}", context);
    if (StringUtils.endsWithIgnoreCase(targetUrl, context)
            || StringUtils.endsWithIgnoreCase(targetUrl, context + "/")) {
      targetUrl = getDefaultTargetUrl();
    }
    LOGGER.debug("Redirecting to DefaultSavedRequest Url: {}", targetUrl);

    getRedirectStrategy().sendRedirect(request, response, targetUrl);
  }

  //@Override
  public void setRequestCache(RequestCache requestCache) {
    this.requestCache = requestCache;
  }

}
