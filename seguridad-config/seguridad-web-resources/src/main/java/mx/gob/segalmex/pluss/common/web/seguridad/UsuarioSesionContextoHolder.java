/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.common.web.seguridad;

import mx.gob.segalmex.common.core.seguridad.UsuarioSesionHolder;
import org.springframework.security.core.context.SecurityContext;

/**
 *
 * @author oscar
 */
public interface UsuarioSesionContextoHolder extends UsuarioSesionHolder {

    /**
     * Obtiene el contexto de seguridad de la aplicacion.
     *
     * @return el contexto de seguridad de la aplicacion.
     */
    SecurityContext getContext();

    /**
     * Define el contexto de seguridad de la aplicacion.
     *
     * @param securityContext
     */

    void setContext(SecurityContext securityContext);
}
