/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.common.web.config;

/**
 * Bean para confirgurar el sistema actual.
 * @author oscar
 */
public class SistemaHelper {

    private final String sistema;

    public SistemaHelper(String sistema) {
        this.sistema = sistema;
    }

    public String getSistema() {
        return sistema;
    }
}
