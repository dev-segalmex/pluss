/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.common.web.seguridad;

import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

/**
 * @author oscar
 */
@Slf4j
@Component("usuarioSesionContextoHolder")
public class UsuarioSesionContextoHolderImpl implements UsuarioSesionContextoHolder {

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Override
    public SecurityContext getContext() {
        return SecurityContextHolder.getContext();
    }

    @Override
    public void setContext(SecurityContext contextoSeguridad) {
        SecurityContextHolder.setContext(contextoSeguridad);
    }

    @Override
    public Usuario getUsuario() {
        Object principal = getContext().getAuthentication().getPrincipal();
        if (principal instanceof UserDetails) {
            String username = ((UserDetails) principal).getUsername();
            LOGGER.debug("Regresando usuario {} de bd.", username);
            return buscadorCatalogo.buscaElemento(Usuario.class, username);
        }
        return null;
    }

}
