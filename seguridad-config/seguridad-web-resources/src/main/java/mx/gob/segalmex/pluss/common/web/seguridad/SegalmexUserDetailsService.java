/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.common.web.seguridad;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.seguridad.busqueda.BuscadorPermiso;
import mx.gob.segalmex.pluss.modelo.seguridad.Permiso;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.access.hierarchicalroles.RoleHierarchy;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author ismael
 */
@Slf4j
@Service("userDetailsService")
public class SegalmexUserDetailsService implements UserDetailsService {

    @Autowired
    private RoleHierarchy roleHierarchy;

    /**
     * El buscador de usuarios.
     */
    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    /**
     * El buscador de permisos.
     */
    @Autowired
    private BuscadorPermiso buscadorPermiso;

    /**
     *
     * @param correo
     * @return
     * @throws UsernameNotFoundException
     */
    @Override
    public UserDetails loadUserByUsername(String correo) throws UsernameNotFoundException {
        try {
            Usuario usuario = buscadorCatalogo.buscaElemento(Usuario.class, correo);

            List<Permiso> permisos = buscadorPermiso.buscaAutorizados(usuario,
                    usuario.getAreaUbicacion());
            Collection<GrantedAuthority> roles = new HashSet<>();
            for (Permiso p : permisos) {
                String role = "ROLE_" + StringUtils.upperCase(p.getAccion().getClave());
                roles.add(new SimpleGrantedAuthority(role));
            }
            roles.add(new SimpleGrantedAuthority("ROLE_AUTENTICADO"));

            Calendar ahora = DateUtils.truncate(Calendar.getInstance(), Calendar.SECOND);
            Calendar fechaActivo = usuario.getFechaActivo();
            Calendar fechaExpiracion = usuario.getFechaExpiracion();
            Calendar fechaExpiracionPassword = usuario.getFechaExpiracionPassword();
            Calendar fechaBloqueo = usuario.getFechaBloqueo();

            if (Objects.nonNull(roleHierarchy.getReachableGrantedAuthorities(roles))) {
                roles = new ArrayList<>(roleHierarchy.getReachableGrantedAuthorities(roles));
            }

            User u = new User(usuario.getNombre(),
                    usuario.getPassword(),
                    ahora.compareTo(fechaActivo) >= 0,
                    fechaExpiracion == null || ahora.compareTo(fechaExpiracion) < 0,
                    fechaExpiracionPassword == null || ahora.compareTo(fechaExpiracionPassword) < 0,
                    fechaBloqueo == null,
                    roles);
            LOGGER.debug("User: {}", u);

            return u;
        } catch (EmptyResultDataAccessException ouch) {
            throw new UsernameNotFoundException("El usuario no existe: " + correo);
        }
    }

}
