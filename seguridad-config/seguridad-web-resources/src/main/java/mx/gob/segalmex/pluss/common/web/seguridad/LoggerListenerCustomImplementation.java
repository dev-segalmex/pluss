/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.common.web.seguridad;

import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.seguridad.GeneradorEventoUsuario;
import mx.gob.segalmex.common.core.seguridad.busqueda.BuscadorUsuario;
import mx.gob.segalmex.pluss.common.web.config.SistemaHelper;
import mx.gob.segalmex.pluss.modelo.seguridad.TipoEventoEnum;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationListener;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.security.authentication.event.AbstractAuthenticationEvent;
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;

/**
 *
 * @author oscar
 */
@Slf4j
public class LoggerListenerCustomImplementation implements ApplicationListener {

    /**
     * El buscador de usuario.
     */
    @Autowired
    private BuscadorUsuario buscadorUsuario;

    @Autowired
    private GeneradorEventoUsuario generadorEventoUsuario;

    @Autowired
    private SistemaHelper sistema;

    @Override
    public void onApplicationEvent(ApplicationEvent event) {
        if (event instanceof AbstractAuthenticationEvent) {
            AbstractAuthenticationEvent authEvent = (AbstractAuthenticationEvent) event;
            String userName = authEvent.getAuthentication().getName();
            try {
                if (event instanceof AuthenticationSuccessEvent) {
                    LOGGER.debug("Login exitoso: {}", userName);
                    Usuario usuario = buscadorUsuario.buscaElemento(userName);
                    generadorEventoUsuario.genera(usuario, TipoEventoEnum.LOGIN_EXITOSO, sistema.getSistema());
                }
                if (event instanceof AbstractAuthenticationFailureEvent) {
                    LOGGER.debug("Login erróneo: {}", userName);
                    //Por si queremos implementar registro de esto.
                }
            } catch (EmptyResultDataAccessException ouch) {
                LOGGER.info("No se encontró el usuario con nombre: {}", userName);
            }
        }
    }
}
