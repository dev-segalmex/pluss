/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.seguridad;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "area")
@Getter
@Setter
public class Area extends AbstractCatalogo {

    /**
     * El estado al que pertenece esta {@link Area}.
     */
    @ManyToOne
    @JoinColumn(name = "estado_id", nullable = false)
    private Estado estado;

}
