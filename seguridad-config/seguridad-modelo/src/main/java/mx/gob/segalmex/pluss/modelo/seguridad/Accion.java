/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.seguridad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractCatalogo;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "accion")
@Getter
@Setter
public class Accion extends AbstractCatalogo {

    /**
     * El tipo de acción (local/global).
     */
    @Column(name = "tipo_accion", nullable = false)
    private String tipoAccion;

    /**
     * El tipo de sistema.
     */
    @Column(name = "sistema", nullable = false)
    private String sistema;

    /**
     * La descripción (a que da acceso y que permite hacer) de la acción.
     */
    @Column(name = "descripcion")
    private String descripcion;

}
