/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.seguridad;

/**
 * Enum para los tipos de evento que puede tener un {@link Usuario}.
 *
 * @author oscar
 */
public enum TipoEventoEnum {

    /**
     * El tipo de evento cuando se loguean con éxito.
     */
    LOGIN_EXITOSO("login-exitoso"),
    /**
     * Tipo de evento cuando se intentan loguear pero hay error.
     */
    LOGIN_ERROR("login-error");

    private final String clave;

    private TipoEventoEnum(String clave) {
        this.clave = clave;
    }

    public String getClave() {
        return clave;
    }
}
