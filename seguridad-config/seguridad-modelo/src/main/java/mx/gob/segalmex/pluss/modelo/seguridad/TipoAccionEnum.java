/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.seguridad;

/**
 *
 * @author ismael
 */
public enum TipoAccionEnum {
    /**
     * El tipo de acción para indicar que es local.
     */
    LOCAL("local"),
    /**
     * El tipo de acción para indicar que es global.
     */
    GLOBAL("global"),
    /**
     * El tipo de acción para indicar que es global y local.
     */
    AMBAS("ambas");

    private final String clave;

    private TipoAccionEnum(String clave) {
        this.clave = clave;
    }

    public String getClave() {
        return clave;
    }

    public static TipoAccionEnum getInstance(String clave) {
        for (TipoAccionEnum e : TipoAccionEnum.values()) {
            if (e.getClave().equals(clave)) {
                return e;
            }
        }

        throw new IllegalArgumentException("No existe un tipo de acción con la clave: " + clave);
    }
}
