/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.seguridad;

import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "permiso")
@Getter
@Setter
public class Permiso extends AbstractEntidad {

    /**
     * El usuario al que se otorga el permiso.
     */
    @ManyToOne
    @JoinColumn(name = "usuario_id", nullable = false)
    private Usuario usuario;

    /**
     * La acción a la que tiene permiso.
     */
    @ManyToOne
    @JoinColumn(name = "accion_id", nullable = false)
    private Accion accion;

    /**
     * El área en la que tiene el permiso.
     */
    @ManyToOne
    @JoinColumn(name = "area_id")
    private Area area;

    /**
     * El estatus del permiso.
     */
    @Column(name = "estatus", nullable = false)
    private String estatus;

    /**
     * El usuario que autoriza el permiso.
     */
    @ManyToOne
    @JoinColumn(name = "usuario_autoriza_id", nullable = false)
    private Usuario usuarioAutoriza;

    /**
     * La fecha de autorización del permiso.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_autorizacion", nullable = false)
    private Calendar fechaAutorizacion;

    /**
     * El usuario que cancela el permiso.
     */
    @ManyToOne
    @JoinColumn(name = "usuario_cancela_id")
    private Usuario usuarioCancela;

    /**
     * La fecha de cancelación del permiso.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_cancelacion")
    private Calendar fechaCancelacion;

}
