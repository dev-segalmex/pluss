/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.seguridad;

/**
 *
 * @author ismael
 */
public enum EstatusPermisoEnum {

    ACTIVO("activo"),

    INACTIVO("inactivo");

    private final String clave;

    private EstatusPermisoEnum(String clave) {
        this.clave = clave;
    }

    public String getClave() {
        return clave;
    }

}
