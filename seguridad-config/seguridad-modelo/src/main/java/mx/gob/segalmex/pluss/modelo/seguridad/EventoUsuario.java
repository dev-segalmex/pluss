/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.seguridad;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;

/**
 * Clase para el registro de eventos de los usuarios.
 *
 * @author oscar
 */
@Entity
@Table(name = "evento_usuario")
@Getter
@Setter
public class EventoUsuario extends AbstractEntidad {

    /**
     * El usuario que genera el evento.
     */
    @ManyToOne
    @JoinColumn(name = "usuario_id", nullable = false)
    private Usuario usuario;

    /**
     * El tipo del evento ({@link TipoEventoEnum}).
     */
    @Column(name = "tipo")
    private String tipo;

    /**
     * El sistema desde el cual se genera el evento.
     */
    @Column(name = "sistema")
    private String sistema;

}
