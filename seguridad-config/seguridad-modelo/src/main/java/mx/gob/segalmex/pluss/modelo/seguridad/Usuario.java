/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.seguridad;

import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractCatalogo;
import mx.gob.segalmex.pluss.modelo.personas.Persona;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "usuario")
@Getter
@Setter
public class Usuario extends AbstractCatalogo {

    /**
     * El password del usuario.
     */
    @Column(name = "password")
    private String password;

    /**
     * La persona a la que pertenece el usuario.
     */
    @ManyToOne
    @JoinColumn(name = "persona_id", nullable = false)
    private Persona persona;

    /**
     * El área de ubicación del usuario.
     */
    @ManyToOne
    @JoinColumn(name = "area_ubicacion_id", nullable = false)
    private Area areaUbicacion;

    /**
     * Indica la fecha a partir de la cual es activo el usuario.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_activo")
    private Calendar fechaActivo;

    /**
     * La fecha de expiración de la cuenta. Podría no expirar.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_expiracion")
    private Calendar fechaExpiracion;

    /**
     * La fecha en que se bloquea la cuenta.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_bloqueo")
    private Calendar fechaBloqueo;

    /**
     * La fecha de expiración del password. Podría no expirar.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "fecha_expiracion_password")
    private Calendar fechaExpiracionPassword;

    /**
     * El puesto que tiene el usuario.
     */
    @Column(name = "puesto")
    private String puesto;

}
