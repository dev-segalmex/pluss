/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.maiz.consultas');
    var data = {
    };
    var handlers = {};
    var utils = {};


    $.segalmex.maiz.consultas.init = function (params) {
        utils.inicializaValidaciones();
        utils.cargaCatalogos();
        $('#button-buscar').click(handlers.busca);
        $('#button-limpiar').click(handlers.limpia);
        $('#button-regresar-resultados').click(handlers.regresaResultados);
        $('#menu-bandejas a.nav-link').click(handlers.cambiaBandeja);
        $('#button-buscar-agrupado').click(handlers.buscaAgrupados);
        $('#button-regresa-agrupados').click(handlers.regresaAgrupados);
        $('#button-exportar').click(handlers.exporta);
        $('#ciclo').change(handlers.exportaCiclo);
    };

    handlers.busca = function (e) {
        e.preventDefault();
        $('#resultados-busqueda').hide();
        utils.configuraCiclo(true);
        utils.desHabilitaCtrls(true);
        var errores = [];
        $('.valid-field').valida(errores, false);
        $.segalmex.validaFechaInicialContraFinal(errores, 'fecha-inicio-registro', 'fecha-fin-registro');
        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            utils.desHabilitaCtrls(false);
            $('#' + errores[0].campo).focus();
            $('#cargando-resultados').hide();
            return;
        }

        if ($('#div-folio').is(':visible')) {
            if ($('#no-contrato').val() === '' && $('#folio').val() === '') {
                alert('Ingrese un folio o número de contrato.');
                utils.desHabilitaCtrls(false);
                return;
            }
        }

        $('#cargando-resultados').show();
        data.busquedaAgrupada = false;
        var filtros = utils.getFiltros();
        data.params = filtros;
        utils.desHabilitaCtrls(false);
        utils.buscaRegistros(filtros);
        $('#cargando-resultados,#div-regresar,#resultados-busqueda-agrupados').hide();
        $('#div-exportar').show();

    };

    handlers.buscaAgrupados = function (e) {
        e.preventDefault();
        utils.desHabilitaCtrls(true);
        utils.configuraCiclo(false);
        var errores = [];
        $('.valid-field').limpiaErrores();
        $('.valid-field').valida(errores, false);
        $.segalmex.validaFechaInicialContraFinal(errores, 'fecha-inicio-registro', 'fecha-fin-registro');
        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            $('#cargando-resultados').hide();
            utils.desHabilitaCtrls(false);
            return;
        }

        var filtros = utils.getFiltros(e);
        if (!filtros) {
            utils.desHabilitaCtrls(false);
            return;
        }
        $('#cargando-resultados').show();
        $.ajax({
            url: '/maiz/resources/contratos/maiz/inscripcion/agrupada',
            type: 'GET',
            data: filtros,
            dataType: 'json'
        }).done(function (response) {
            utils.desHabilitaCtrls(false);
            $('#cargando-resultados,#resultados-busqueda,#div-exportar').hide();
            data.agrupaciones = response;
            for (var i = 0; i < data.agrupaciones.length; i++) {
                data.agrupaciones[i].id = i + 1;
            }
            var renglones = '';
            if (response.length === 0) {
                renglones = '<tr><td colspan="3" class="text-center">Sin resultados</td></tr>';
                $('#resultados-busqueda-agrupados').html(renglones);
            } else {
                renglones = utils.creaDatatableAgrupaciones(data.agrupaciones);
                $('#resultados-busqueda-agrupados').html(renglones);
                $('#table-resultados-busqueda-agrupaciones').on('click', 'a.link-id', handlers.buscaDetalleAgrupados);
            }
            data.busquedaAgrupada = true;
            $('#resultados-busqueda-agrupados').slideDown();
        }).fail(function () {
            alert('Error: No fue posible realizar la consulta.');
            utils.desHabilitaCtrls(false);
            $('#cargando-resultados').hide();
        });
    };

    handlers.buscaDetalleAgrupados = function (e) {
        e.preventDefault();
        var id = e.target.id.split('.')[1];
        var agrupacion = $.segalmex.get(data.agrupaciones, id);
        $('#cargando-resultados').show();
        $('#div-cards,#resultados-busqueda').hide();
        if (parseInt(agrupacion.total) > 10000) {
            alert('Solo se podrán visualizar 10,000 registros.');
        }

        var filtros = utils.getFiltros(e);
        filtros.estatus = agrupacion.estatus.clave;
        filtros.ciclo = agrupacion.ciclo.id;
        filtros.estado = agrupacion.estado.id;

        utils.buscaRegistros(filtros);
        $('#cargando-resultados,#botones-detalle,#resultados-busqueda,#resultados-busqueda-agrupados').hide();
        $('#div-regresar').show();
    };

    handlers.muestraDetalle = function (e) {
        e.preventDefault();
        var uuid = e.target.id.split('.')[1];
        $.ajax({
            url: '/maiz/resources/contratos/maiz/inscripcion/' + uuid,
            data: {archivos: true, datos: true, historial: true},
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            // Mostramos el detalle
            $('#detalle-inscripcion-contrato').html($.segalmex.maiz.inscripcion.vista.construyeInscripcion(response, true));
            $('#estatus-inscripcion-contrato').html($.segalmex.maiz.inscripcion.vista.construyeEstatus(response));
            $('#historial-inscripcion-contrato').html($.segalmex.maiz.inscripcion.vista.construyeHistorial(response.historial));
            $('#datos-capturados-inscripcion-contrato').html($.segalmex.maiz.inscripcion.vista.construyeDatosCapturados(response.datos));

            $('#detalle-elemento').show();
            $('#datos-busqueda-form,#resultados-busqueda,#div-cards').hide();
            $('#button-ver-productores').click(handlers.muestraDetalleContratoProductor);
            $('#detalle-div').html('');
            $('#detalle-div').html($.segalmex.maiz.inscripcion.vista.generaTablaDetalleContratosProductor(response.contratosProductor));
            utils.configuraTablaContratosProductor(false);
        }).fail(function () {
            alert('Error: No fue posible obtener el registro.');
        });
    }

    handlers.limpia = function (e) {
        utils.limpiar();
    }

    handlers.regresaResultados = function (e) {
        e.preventDefault();
        if (!data.busquedaAgrupada) {
            $('#detalle-elemento').hide();
            $('#div-cards,#datos-busqueda-form,#resultados-busqueda,#div-exportar').show();
        }
        if (data.busquedaAgrupada) {
            $('#detalle-elemento').hide();
            $('#resultados-busqueda,#div-regresar').show();
        }
    };

    handlers.regresaAgrupados = function (e) {
        e.preventDefault();
        $('#resultados-busqueda').hide();
        $('#resultados-busqueda-agrupados,#div-cards,#datos-busqueda-form').show();
    };

    utils.buscaRegistros = function (filtros) {
        $.ajax({
            url: '/maiz/resources/contratos/maiz/inscripcion/',
            type: 'GET',
            data: filtros,
            dataType: 'json'
        }).done(function (response) {
            var renglones = '';
            if (response.length === 0) {
                renglones = '<tr><td colspan="10" class="text-center">Sin resultados</td></tr>';
                $('#data').html(renglones);
            } else {
                renglones = utils.creaDatatable(response);
                $('#data').html(renglones);
                utils.configuraTablaResultados();
                $('#table-resultados-busqueda').on('click', 'a.link-uuid', handlers.muestraDetalle);
            }
            $('#resultados-busqueda').slideDown();
        }).fail(function () {
            $('#cargando-resultados').hide();
            alert('Error: No fue posible realizar la consulta.');
        });
    };

    utils.construyeRenglones = function (resultados) {
        var buffer = [];
        for (var i = 0; i < resultados.length; i++) {
            var resultado = resultados[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td><a class="link-uuid" href="#" id="uuid.' + resultado.uuid + '">');
            buffer.push(resultado.folio);
            buffer.push('</a></td>');
            buffer.push('<td>');
            buffer.push($.segalmex.date.isoToFecha(resultado.fechaCreacion));
            buffer.push('</td>');
            buffer.push('<td>');
            if (resultado.contrato) {
                buffer.push('<a href="/maiz/resources/archivos/maiz/' + resultado.contrato.uuid + '/'
                        + resultado.contrato.nombre + '" target="_blank">');
                buffer.push(resultado.numeroContrato);
                buffer.push('</a>');
            } else {
                buffer.push(resultado.numeroContrato);
            }
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.sucursal.empresa.rfc);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.sucursal.empresa.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.estado.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.volumenTotal);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.tipoCultivo.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.estatus.nombre);
            buffer.push('</td>');
            buffer.push('</tr>');
        }

        return buffer.join('\n');
    }

    utils.inicializaValidaciones = function () {
        $(".valid-field").configura({required: false});

        $('#no-contrato').configura({
            pattern: /^[A-Z]{3}-[A-Z]{2}[0-9]{2}-[A-Z0-9]{3}-\d{6}-(A|B|T)-\d{3}$/,
            minlength: 25,
            maxlength: 25,
            required: false
        });
        $('#fecha-inicio-registro,#fecha-fin-registro').configura({
            type: 'date',
            required: false
        });
        $('#fecha-inicio-registro,#fecha-fin-registro').datepicker({
            language: "es",
            autoclose: true,
            forceParse: false,
            format: 'dd/mm/yyyy',
            yearRange: "c-10:c"
        });
        $('#rfc-empresa').configura({
            type: 'rfc-moral',
            required: false
        });
        $('#folio').configura({
            pattern: /^\d{1,6}$/,
            minlength: 1,
            maxlength: 6
        });

        $('.valid-field').validacion();
    };

    utils.limpiar = function () {
        $('#estatus').val('0').change();
        $('#datos-busqueda-form input').val('');
        $('#ciclo').val('0').change();
        $('.valid-field').limpiaErrores();
        $('#resultados-busqueda,#resultados-busqueda-agrupados').hide();
        $('#cargando-resultados').hide();
        $('#table-resultados-busqueda tbody').html('<tr><td colspan="6" class="text-center">Sin resultados</td></tr>');
        $('#detalle-inscripcion-contrato,#historial-inscripcion-contrato,#datos-capturados-inscripcion-contrato').html('');
        $('#validador').val('0').change();
    };

    utils.cargaCatalogos = function () {

        $.ajax({
            url: '/maiz/resources/paginas/consultas',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            $('#button-exportar-ciclo,#button-flecha').prop('disabled', true);
            $('#validador').actualizaCombo(response.validadores);
            $('#estatus').actualizaCombo(response.estatus, {value: 'clave'});
            $('#ciclo').actualizaCombo(response.ciclos);
            data.ciclos = response.ciclos;
        }).fail(function () {
            alert('Error: No se pudo obtener los catálogos de la página.');
        });

    };

    utils.creaDatatable = function (resultados) {
        var buffer = [];
        buffer.push('<table id="table-resultados-busqueda"');
        buffer.push(' class="table table-striped" width="100%">');
        buffer.push('<thead class = "table-success">');
        buffer.push('<tr class="success">');
        buffer.push('<th>No.</th>');
        buffer.push('<th>Folio</th>');
        buffer.push('<th>Fecha de registro</th>');
        buffer.push('<th>No. de contrato</th>');
        buffer.push('<th>RFC de la empresa</th>');
        buffer.push('<th>Empresa</th>');
        buffer.push('<th>Estado</th>');
        buffer.push('<th>Toneladas</th>');
        buffer.push('<th>Tipo</th>');
        buffer.push('<th>Estatus</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');
        for (var i = 0; i < resultados.length; i++) {
            var resultado = resultados[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push('</td>');
            buffer.push('<td><a class="link-uuid" href="#" id="uuid.' + resultado.uuid + '">');
            buffer.push(resultado.folio);
            buffer.push('</a></td>');
            buffer.push('<td>');
            buffer.push($.segalmex.date.isoToFecha(resultado.fechaCreacion));
            buffer.push('</td>');
            buffer.push('<td>');
            if (resultado.contrato) {
                buffer.push('<a href="/maiz/resources/archivos/maiz/' + resultado.contrato.uuid + '/'
                        + resultado.contrato.nombre + '" target="_blank">');
                buffer.push(resultado.numeroContrato);
                buffer.push('</a>');
            } else {
                buffer.push(resultado.numeroContrato);
            }
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.sucursal.empresa.rfc);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.sucursal.empresa.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.estado.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.volumenTotal);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.tipoCultivo.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.estatus.nombre);
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        buffer.push('</tbody>');
        buffer.push('</table>');

        buffer.push('<br/>');
        return buffer.join('');

    };

    utils.configuraTablaResultados = function () {
        var aoColumns = [{"bSortable": false}, {"bSortable": true}, {"bSortable": false},
            {"bSortable": true}, {"bSortable": true}, {"bSortable": true}, {"bSortable": true},
            {"bSortable": true}, {"bSortable": true}, {"bSortable": true}];
        utils.configuraTablaFactura('table-resultados-busqueda', aoColumns);
    };

    utils.configuraTablaFactura = function (tablaId, aoColumns) {
        var table = $('#' + tablaId).DataTable({
            "oLanguage": {
                "sEmptyTable": "No se encontró información para mostrar en la tabla",
                "sSearch": "Buscar ",
                "sZeroRecords": "No se encontraron resultados."
            },
            "bFilter": true,
            "bInfo": false,
            "bPaginate": false,
            "aoColumns": aoColumns,
            "aoColumnDefs": [{"bSortable": false, "aTargets": ["no-sort"]}]
        });
        table.on('order.dt search.dt', function () {
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).order([[1, 'asc']]).draw(false);
        $("#" + tablaId + "_filter input").addClass('input-buscar');
        $(".dataTables_empty").attr("colspan", "100%");
    };

    handlers.cambiaBandeja = function (e) {
        e.preventDefault();
        utils.limpiar();
        $('#div-generales,#div-folio').hide();
        var id = e.target.id.substring('muestra-'.length);
        $('#menu-bandejas a.nav-link').removeClass('active');
        $(e.target).addClass('active');
        $('#' + id).show();
        $('#button-exportar-ciclo,#button-flecha').prop('disabled', true);
        utils.ocultaAgrupado(id);
    };

    utils.ocultaAgrupado = function (bandeja) {
        if (bandeja !== 'div-generales') {
            $('#button-buscar-agrupado').hide();
        } else {
            $('#button-buscar-agrupado').show();
        }
    };

    utils.desHabilitaCtrls = function (disabled) {
        $('#button-buscar').prop('disabled', disabled);
        $('#button-limpiar').prop('disabled', disabled);
    };

    utils.creaDatatableAgrupaciones = function (resultados) {
        var buffer = [];
        buffer.push('<table id="table-resultados-busqueda-agrupaciones"');
        buffer.push(' class="table table-striped" width="100%">');
        buffer.push('<thead class = "table-success">');
        buffer.push('<tr class="success">');
        buffer.push('<th>Ciclo</th>');
        buffer.push('<th>Estado</th>');
        buffer.push('<th>Estatus</th>');
        buffer.push('<th>Total</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');
        var total = 0;
        for (var a = 0; a < resultados.length; a++) {
            var resultado = resultados[a];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(resultado.ciclo.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.estado.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.estatus.nombre);
            buffer.push('</td>');
            buffer.push('<td class="text-right"><a class="link-id" href="#" id="id.' + resultado.id + '">');
            buffer.push(resultado.total);
            buffer.push('</a></td>');
            buffer.push('</tr>');
            total += parseInt(resultado.total);
        }
        buffer.push('</tbody>');
        buffer.push(utils.agregaTotal(resultados));
        buffer.push('</table>');

        buffer.push('<br/>');
        return buffer.join('');
    };

    utils.getFiltros = function () {
        var datos = {
            folio: $('#folio').val(),
            numeroContrato: $('#no-contrato').val(),
            rfcEmpresa: $('#rfc-empresa').val(),
            estatus: $('#estatus').val(),
            fechaInicio: $('#fecha-inicio-registro').val(),
            fechaFin: $('#fecha-fin-registro').val(),
            validador: $('#validador').val(),
            ciclo: $('#ciclo').val()
        };

        for (var prop in datos) {
            if (datos[prop] === '') {
                delete datos[prop];
            }
        }
        if (datos.estatus === '0') {
            delete datos.estatus;
        }
        if (datos.validador === '0') {
            delete datos.validador;
        }
        if (datos.ciclo === '0') {
            delete datos.ciclo;
        }
        if (datos.fechaInicio) {
            datos.fechaInicio = $.segalmex.date.fechaToIso(datos.fechaInicio);
        }
        if (datos.fechaFin) {
            datos.fechaFin = $.segalmex.date.fechaToIso(datos.fechaFin);
        }
        return datos;
    };

    utils.agregaTotal = function (agrupados) {
        var total = 0;
        for (var a in agrupados) {
            total += parseInt(agrupados[a].total);
        }
        var buffer = [];
        buffer.push('<tfoot class="table-success">');
        buffer.push('<tr>');
        buffer.push('<th colspan="3">Total</th>');
        buffer.push('<th class="text-right">' + total + '</th>');

        buffer.push('</tfoot>');
        return buffer.join('');
    };

    utils.configuraCiclo = function (requerido) {
        $('#ciclo').configura({
            required: requerido
        });
        $('#ciclo').validacion();
    };

    handlers.exporta = function (e) {
        e.preventDefault();
        utils.desHabilitaCtrls(true);
        utils.configuraCiclo(true);

        var errores = [];
        $('.valid-field').valida(errores, false);
        $.segalmex.validaFechaInicialContraFinal(errores, 'fecha-inicio-registro', 'fecha-fin-registro');
        if (errores.length !== 0) {
            utils.desHabilitaCtrls(false);
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            $('#cargando-resultados').hide();
            return;
        }

        if ($('#div-folio').is(':visible')) {
            if ($('#no-contrato').val() === '' && $('#folio').val() === '') {
                utils.desHabilitaCtrls(false);
                alert('Ingrese un número de contrato o folio.');
                return;
            }
        }

        var filtros = utils.getFiltros(e);
        if (!filtros) {
            utils.desHabilitaCtrls(false);
            return;
        }

        $.segalmex.openPdf('/maiz/resources/contratos/maiz/inscripcion/csv/resultados-contrato.xlsx?' + $.param(filtros));
        utils.desHabilitaCtrls(false);
        $('#cargando-resultados').hide();
    };

    handlers.exportaCiclo = function (e) {
        e.preventDefault();
        var ciclo = $.segalmex.get(data.ciclos, $(e.target).val());
        if (!ciclo) {
            $('#button-exportar-ciclo').attr('href', '#');
            $('#button-exportar-ciclo,#button-flecha').prop('disabled', true);
            return;
        }
        $('#button-exportar-ciclo,#button-flecha').prop('disabled', false);
        $('#button-exportar-ciclo').attr('href', '/maiz/resources/contratos/reporte/' + ciclo.id + '.xlsx');
    };

    handlers.muestraDetalleContratoProductor = function (e) {
        e.preventDefault();
        $('#detalle-contrato-productor-modal').modal('show').focus();
    };

    utils.configuraTablaContratosProductor = function () {
        var aoColumns = [{"bSortable": false}, {"bSortable": true},
            {"bSortable": true}, {"bSortable": true}, {"bSortable": false},
            {"bSortable": false}, {"bSortable": false}];
        utils.configuraTablaContratoProductor('detalle-productores-table', aoColumns);
    };

    utils.configuraTablaContratoProductor = function (tablaId, aoColumns) {
        var table = $('#' + tablaId).DataTable({
            "oLanguage": {
                "sEmptyTable": "No se encontró información para mostrar en la tabla",
                "sSearch": "Buscar ",
                "sZeroRecords": "No se encontraron resultados."
            },
            "bFilter": true,
            "bInfo": false,
            "bPaginate": false,
            "aoColumns": aoColumns,
            "aoColumnDefs": [{"bSortable": false, "aTargets": ["no-sort"]}]
        });
        table.on('order.dt search.dt', function () {
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).order([[1, 'asc']]).draw(false);
        $("#" + tablaId + "_filter input").addClass('input-buscar');
        $(".dataTables_empty").attr("colspan", "100%");
    };
})(jQuery);