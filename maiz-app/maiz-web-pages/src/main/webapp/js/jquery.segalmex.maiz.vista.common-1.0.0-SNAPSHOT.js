(function ($) {
    $.segalmex.namespace('segalmex.maiz.inscripcion.vista');

    var utils = {};

    utils.formatoNombreUsuario = function (nombre) {
        var n = nombre.split('@')[0];
        if (n.length > 4) {
            return n.substring(0, 4) + '...';
        } else {
            return n + '...';
        }
    }

    $.segalmex.maiz.inscripcion.vista.construyeEstatus = function (inscripcion) {
        var buffer = [];
        buffer.push('<div class="card mb-4 shadow-sm">'); // card
        buffer.push('<h4 class="card-header">Estatus</h4>');
        buffer.push('<div class="card-body">'); // card-body

        buffer.push('<h3 class="card-title">' + inscripcion.estatus.nombre + '</h3>');

        if (inscripcion.comentarioEstatus) {
            buffer.push('<div class="row">'); // row
            buffer.push('<div class="col-md-12">');
            buffer.push('<strong>Comentario:</strong><br/>');
            buffer.push(inscripcion.comentarioEstatus);
            buffer.push('</div>');
            buffer.push('</div>');
        }

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Usuario registra:</strong><br/>');
        buffer.push(inscripcion.usuarioRegistra.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Usuario validador:</strong><br/>');
        buffer.push(inscripcion.usuarioValidador ? utils.formatoNombreUsuario(inscripcion.usuarioValidador.nombre) : '--');
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Usuario asignado (actual):</strong><br/>');
        buffer.push(inscripcion.usuarioAsignado ? utils.formatoNombreUsuario(inscripcion.usuarioAsignado.nombre) : '--');
        buffer.push('</div>');
        buffer.push('</div>'); // end-row

        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card

        return buffer.join('\n');
    }

    $.segalmex.maiz.inscripcion.vista.construyeHistorial = function (historial) {
        var buffer = [];
        buffer.push('<div class="card mb-4 shadow-sm">'); // card
        buffer.push('<h4 class="card-header">Historial</h4>');

        buffer.push('<div class="card-body">'); // card-body
        buffer.push('<div class="table-responsive">')
        buffer.push('<table class="table table-striped table-bordered table-hover">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<th>#</th>');
        buffer.push('<th>Tipo</th>');
        buffer.push('<th>Fecha inicio</th>');
        buffer.push('<th>Fecha fin</th>');
        buffer.push('<th>Usuario inicia</th>');
        buffer.push('<th>Usuario finaliza</th>');
        buffer.push('</thead>');

        buffer.push('<tbody>');
        if (historial && historial.length > 0) {
            buffer.push($.segalmex.maiz.inscripcion.vista.construyeRenglonesHistorial(historial));
        } else {
            buffer.push('<tr><td colspan="6">Sin historial</td></tr>')
        }
        buffer.push('</tbody>');
        buffer.push('</table>');
        buffer.push('</div>')
        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card

        return buffer.join('\n');
    }

    $.segalmex.maiz.inscripcion.vista.construyeRenglonesHistorial = function (historial) {
        var buffer = [];
        for (var i = 0; i < historial.length; i++) {
            var h = historial[i];

            buffer.push('<tr>');
            buffer.push('<td class="text-right">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(h.tipo.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push($.segalmex.date.isoToFechaHora(h.fechaCreacion));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(h.fechaFinaliza ? $.segalmex.date.isoToFechaHora(h.fechaFinaliza) : '--');
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(utils.formatoNombreUsuario(h.usuarioRegistra.nombre));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(h.usuarioFinaliza ? utils.formatoNombreUsuario(h.usuarioFinaliza.nombre) : '--');
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        return buffer.join('\n');
    }

    $.segalmex.maiz.inscripcion.vista.construyeDatosCapturados = function (datos) {
        if (!datos) {
            return '';
        }

        var buffer = [];
        buffer.push('<div class="card mb-4 shadow-sm">'); // card
        buffer.push('<h4 class="card-header">Datos capturados</h4>');

        buffer.push('<div class="card-body">'); // card-body
        buffer.push('<div class="table-responsive">')
        buffer.push('<table class="table table-striped table-bordered table-hover">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<th>#</th>');
        buffer.push('<th>Campo</th>');
        buffer.push('<th>Valor</th>');
        buffer.push('<th>Estatus</th>');
        buffer.push('<th>Comentario</th>');
        buffer.push('</thead>');

        buffer.push('<tbody>');
        buffer.push($.segalmex.maiz.inscripcion.vista.construyeRenglonesDatosCapturados(datos));
        buffer.push('</tbody>');
        buffer.push('</table>');
        buffer.push('</div>')
        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card

        return buffer.join('\n');
    }

    $.segalmex.maiz.inscripcion.vista.construyeRenglonesDatosCapturados = function (datos) {
        var buffer = [];
        for (var i = 0; i < datos.length; i++) {
            var dato = datos[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-right">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(dato.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(dato.valorTexto ? dato.valorTexto : dato.valor);
            buffer.push('</td>');
            buffer.push('<td class="text-center">');
            buffer.push(dato.correcto
                    ? '<i class="fas fa-check-circle text-success"></i>'
                    : (dato.correcto !== undefined ? '<i class="fas fa-times-circle text-danger"></i>' : '--'));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(dato.comentario ? dato.comentario : '--');
            buffer.push('</td>');
            buffer.push('</tr>');
        }

        return buffer.join('\n');
    }


    $.segalmex.maiz.inscripcion.vista.construyeComentarios = function (comentarios, usuario) {
        if (!comentarios) {
            return '';
        }

        var buffer = [];
        buffer.push('<div class="card mb-4 shadow-sm">'); // card
        buffer.push('<h4 class="card-header">Comentarios</h4>');

        buffer.push('<div class="card-body">'); // card-body
        buffer.push($.segalmex.maiz.inscripcion.vista.construyeToastComentarios(comentarios, usuario.nombre));
        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card

        return buffer.join('\n');
    }

    $.segalmex.maiz.inscripcion.vista.construyeToastComentarios = function (comentarios, nombreUsuario) {
        var buffer = [];
        for (var i = 0; i < comentarios.length; i++) {
            var comentario = comentarios[i];
            buffer.push('<div class="mb-2 clearfix">');
            buffer.push('<div role="alert" aria-live="assertive" aria-atomic="true" class="toast show' + (comentario.usuario.nombre === nombreUsuario ? ' float-right' : '') + '" data-autohide="false">');
            buffer.push('<div class="toast-header">');
            buffer.push('<svg class="bd-placeholder-img rounded mr-2" width="20" height="20" xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="xMidYMid slice" focusable="false" role="img"><rect width="100%" height="100%" fill="' + (comentario.usuario.nombre === nombreUsuario ? '#13322B' : '#007aff') + '"></rect></svg>');
            buffer.push('<strong class="mr-auto">' + utils.formatoNombreUsuario(comentario.usuario.nombre) + '</strong>');
            buffer.push('<small>' + $.segalmex.date.isoToFecha(comentario.fechaCreacion) + '</small>');
            buffer.push('</div>');
            buffer.push('<div class="toast-body">');
            buffer.push('<div><strong>En ' + utils.colocaAcento(comentario.tipo) + ':</strong></div><div>');
            buffer.push(comentario.contenido);
            buffer.push('</div>');
            buffer.push('</div>');
            buffer.push('</div>');
            buffer.push('</div>');
        }

        return buffer.join('\n');
    }

    utils.colocaAcento = function (tipo){
        switch (tipo) {
            case 'asignacion':
                return 'asignación';
                break;
            case 'validacion':
                return 'validación';
                break;
            case 'documentacion':
                return 'documentación';
                break;
            case 'revalidacion':
                return 'revalidación';
                break;
                break;
            case 'correccion':
                return 'corrección';
                break;
            case 'edicion':
                return 'edición';
                break;
            case 'supervicion':
                return 'supervición';
                break;
            default:
                return tipo;

        };
    };

    $.segalmex.maiz.inscripcion.vista.generaMensajeExcepcion = function (response) {
        var texto = "";
        if (response.motivos) {
            $.each(response.motivos, function () {
                texto += "\t• " + this + "\n";
            });
        }
        var mensaje = "";
        if (texto === "") {
            mensaje = response.mensaje;
        } else {
            mensaje = response.mensaje + "\n\n" + texto;
        }
        return mensaje;
    };

    $.segalmex.maiz.inscripcion.vista.construyeTipoProductor = function (tipoProductor) {
        var buffer = [];
        buffer.push('<div class="card mb-4 shadow-sm">'); // card
        buffer.push('<h4 class="card-header">Tipo de productor</h4>');
        buffer.push('<div class="card-body">'); // card-body
        buffer.push('<h3 class="card-title">' + tipoProductor.nombre + '</h3>');
        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card
        return buffer.join('\n');
    };
})(jQuery);