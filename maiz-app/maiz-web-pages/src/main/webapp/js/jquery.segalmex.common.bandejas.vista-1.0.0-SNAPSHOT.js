(function ($) {
    $.segalmex.namespace('segalmex.common.bandejas.vista');

    var utils = {};

    $.segalmex.common.bandejas.vista.construyeTabla = function (params) {
        var buffer = [];
        if (params.filtro) {
            buffer.push('<div class="row">');
            buffer.push('<div class="col-md-6 form-group">');
            buffer.push('<label for="cultivo" class="control-label">Cultivo</label>');
            buffer.push('<select id="cultivo" class="form-control valid-field">');
            buffer.push('<option value="0">Seleccione</option>');
            buffer.push('</select>');
            buffer.push('</div>');
            buffer.push('<div class="col-md-6 form-group">');
            buffer.push('<label for="ciclo" class="control-label">Ciclo</label>');
            buffer.push('<select id="ciclo" class="form-control valid-field">');
            buffer.push('<option value="0">Seleccione</option>');
            buffer.push('</select>');
            buffer.push('</div>');
            buffer.push('</div>');
        }
        buffer.push(utils.agregaBotones(params));
        buffer.push('<div class="table-responsive"><table id="');
        buffer.push(params.id ? params.id : 'table-resultados');
        buffer.push('" class="table table-striped" width="100%">');
        buffer.push('<thead class = "table-success">');
        buffer.push('<tr class="success">');
        if (params.checkbox) {
            buffer.push('<th><input class="checkbox-select-all" type="checkbox"/></th>')
        }
        buffer.push('<th>#</th>');
        buffer.push('<th>Fecha</th>');
        buffer.push('<th>Folio</th>');
        switch (params.tipoInscripcion) {
            case 'contratos':
                buffer.push('<th>Empresa</th>');
                break;
            case 'productores':
                buffer.push('<th>Productor</th>');
                buffer.push('<th>Tipo</th>');
                buffer.push('<th>Sociedad</th>');
                break
            case 'facturas':
                buffer.push('<th>Emisor</ht>');
                buffer.push('<th>Receptor</th>');
                break;
        }
        buffer.push('<th>Cultivo - Ciclo</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');

        if (params.lista && params.lista.length > 0) {
            $.each(params.lista, function () {
                buffer.push('<tr class="text-left">');
                if (params.checkbox) {
                    buffer.push('<td><input class="checkbox-cur" id="checkbox-id-');
                    buffer.push(this.folio);
                    buffer.push('" type="checkbox" /></td>')
                }
                buffer.push('<td>');
                buffer.push('');
                buffer.push('</td>')
                buffer.push('<td>');
                buffer.push(this.fechaCreacion ? '<span class="d-none">' + this.fechaCreacion + '</span> ' + $.segalmex.date.isoToFecha(this.fechaCreacion) : '--');
                buffer.push('</td>');
                buffer.push('<td>');
                if (params.link) {
                    buffer.push('<a href="#" class="detalle-entrada" id="link-id-');
                    buffer.push(this.folio);
                    buffer.push('">');
                    buffer.push(this.folio);
                    buffer.push('</a>');
                } else {
                    buffer.push(this.folio);
                }
                buffer.push('</td>');
                buffer.push('<td>');
                switch (params.tipoInscripcion) {
                    case 'contratos':
                        buffer.push(this.sucursal.empresa.nombre);
                        break;
                    case 'productores':
                        buffer.push(this.datosProductor.nombreMoral
                                ? this.datosProductor.nombreMoral
                                : utils.construyeNombreCompleto(this.datosProductor));
                        buffer.push('</td>');
                        buffer.push('<td>');
                        buffer.push(this.datosProductor.tipoPersona.nombre);
                        buffer.push('<td>');
                        buffer.push(this.rfcEmpresas ? this.rfcEmpresas.join(', ') : '--');
                        break
                    case 'facturas':
                        buffer.push(this.cfdi.nombreEmisor);
                        buffer.push('</td>');
                        buffer.push('<td>');
                        buffer.push(this.cfdi.nombreReceptor);
                        break;
                }

                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(this.cultivo.nombre.substring(0, 1) + '-' + this.ciclo.clave.toUpperCase());
                buffer.push('</td>');
                buffer.push('</tr>');
            });
        }

        buffer.push('</tbody>');
        buffer.push('</table></div>');

        return buffer.join('');
    }

    $.segalmex.common.bandejas.vista.configuraTablaInscripcion = function (idTabla, tipoInscripcion) {
        var aoColumns = [
            {"bSortable": false},
            {"bSortable": true},
            {"bSortable": true},
            {"bSortable": true}
        ];
        switch (tipoInscripcion) {
            case 'productores':
                aoColumns.push({"bSortable": true});
                aoColumns.push({"bSortable": true});
                break;
            case 'facturas':
                aoColumns.push({"bSortable": true});
                break;
        }
        aoColumns.push({"bSortable": false});
        $.segalmex.common.bandejas.vista.configuraTabla(idTabla, aoColumns);
    };

    $.segalmex.common.bandejas.vista.configuraTabla = function (idTabla, aoColumns, indexOrden, order) {
        var table = $('#' + idTabla).DataTable({
            "oLanguage": {
                "sEmptyTable": "No se encontró información para mostrar en la tabla",
                "sSearch": "Buscar ",
                "sZeroRecords": "No se encontraron resultados."
            },
            "bFilter": true,
            "bInfo": false,
            "bPaginate": false,
            "aoColumns": aoColumns,
            "aoColumnDefs": [{"bSortable": false, "aTargets": ["no-sort"]}]
        });
        table.on('order.dt search.dt', function () {
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).order([[indexOrden ? indexOrden : 1, order ? order : 'asc']]).draw(false);
        $("#" + idTabla + "_filter input").addClass('input-buscar');
        $(".dataTables_empty").attr("colspan", "100%");
    };

    utils.construyeNombreCompleto = function (dp) {
        var buffer = [];
        if (dp.nombre) {
            buffer.push(dp.nombre);
        }
        if (dp.primerApellido) {
            buffer.push(dp.primerApellido);
        }
        if (dp.segundoApellido) {
            buffer.push(dp.segundoApellido);
        }
        return buffer.join(' ');
    }

    utils.agregaBotones = function (params) {
        var botones = '';
        if (params.btnActualizar) {
            botones += '<button type="button" class="btn btn-outline-secondary" id="actualizar-lista"><i class="fas fa-sync"></i> Actualizar</button> ';
        }
        if (params.btnAsignar) {
            botones += '<button type="button" class="btn btn-outline-secondary" id="asignar-modal">Asignacion múltiple</button> ';
        }
        if (params.filtro) {
            botones += '<button type="button" class="btn btn-outline-primary" id="filtrar">Filtrar</button> ';
        }
        if (botones) {
            return '<p class="text-right">' + botones + '</p>';
        }
        return botones;
    };
})(jQuery);
