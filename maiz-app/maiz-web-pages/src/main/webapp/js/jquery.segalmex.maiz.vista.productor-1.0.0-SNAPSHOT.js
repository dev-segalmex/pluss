(function ($) {
    $.segalmex.namespace('segalmex.maiz.inscripcion.productor.vista');

    var utils = {};
    var handlers = {};

    $.segalmex.maiz.inscripcion.productor.vista.muestraInscripcion = function (inscripcion, idDetalle) {
        $('#' + idDetalle).html($.segalmex.maiz.inscripcion.productor.vista.construyeInscripcion(inscripcion));
        $('#' + idDetalle + ' a.muestra-ubicacion').click(handlers.muestraUbicacion);
    };

    $.segalmex.maiz.inscripcion.productor.vista.construyeInscripcion = function (inscripcion) {
        var buffer = [];
        buffer.push('<div class="card mb-4 shadow-sm">');
        buffer.push('<h4 class="card-header">Detalle de Registro de Productor</h4>');

        buffer.push('<div class="card-body">'); // card-body
        buffer.push('<h3 class="card-title">Folio: ');
        buffer.push(inscripcion.folio);
        buffer.push('</h3>');
        buffer.push('<h4>Programa de Precios de Garantía a Productos Alimentarios Básicos</h4>');

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Cultivo:</strong><br/>');
        buffer.push(inscripcion.cultivo.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Ciclo agrícola:</strong><br/>');
        buffer.push(inscripcion.ciclo.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Tipo:</strong><br/>');
        buffer.push(inscripcion.etiquetaTipoCultivo);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<h4>Datos de la ventanilla</h4>');
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-8">');
        buffer.push('<strong>Nombre:</strong><br/> ');
        buffer.push(inscripcion.sucursal.empresa.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>RFC:</strong><br/> ');
        buffer.push(inscripcion.sucursal.empresa.rfc);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Entidad:</strong><br/> ');
        buffer.push(inscripcion.sucursal.estado.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Municipio:</strong><br/> ');
        buffer.push(inscripcion.sucursal.municipio.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Localidad:</strong><br/> ');
        buffer.push(inscripcion.sucursal.localidad);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<h4>Datos del productor</h4>');
        var dp = inscripcion.datosProductor;
        switch (dp.tipoPersona.clave) {
            case 'moral':
                buffer.push('<h5>Productor (persona moral)</h5>');

                buffer.push('<div class="row">'); // row
                buffer.push('<div class="col-md-4">');
                buffer.push('<strong>Nombre de la sociedad:</strong><br/>');
                buffer.push(dp.nombreMoral);
                buffer.push('</div>');
                buffer.push('<div class="col-md-4">');
                buffer.push('<strong>RFC de la sociedad:</strong><br/>');
                buffer.push(dp.rfc);
                buffer.push('</div>');
                buffer.push('</div>'); // end-row
                buffer.push('<br/>');

                buffer.push('<h5>Representante legal</h5>');
                buffer.push('<div class="row">'); // row
                buffer.push('<div class="col-md-4">');
                buffer.push('<strong>CURP:</strong><br/>');
                buffer.push(dp.curp);
                buffer.push('</div>');
                buffer.push('</div>'); // end-row
                break;
            case 'fisica':
                if (inscripcion.tipoRegistro === 'asociado') {
                    buffer.push('<h5>Datos del productor (persona física) / Asociado</h5>');
                } else {
                    buffer.push('<h5>Datos del productor (persona física)</h5>');
                }
                buffer.push('<div class="row">'); // row
                buffer.push('<div class="col-md-4">');
                buffer.push('<strong>CURP:</strong><br/>');
                buffer.push(dp.curp);
                buffer.push('</div>');
                buffer.push('<div class="col-md-4">');
                buffer.push('<strong>RFC:</strong><br/>');
                buffer.push(dp.rfc);
                buffer.push('</div>');
                buffer.push('</div>'); // end-row
                break;
        }
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Nombre:</strong><br/>');
        buffer.push(dp.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Primer apellido:</strong><br/>');
        buffer.push(dp.primerApellido);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Segundo apellido:</strong><br/>');
        buffer.push(dp.segundoApellido);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Fecha de nacimiento:</strong><br/>');
        buffer.push($.segalmex.date.isoToFecha(dp.fechaNacimiento));
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Sexo:</strong><br/>');
        buffer.push(dp.sexo.nombre);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Tipo de identificación:</strong><br/>');
        buffer.push(dp.tipoDocumento.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Número de identificación:</strong><br/>');
        buffer.push(dp.numeroDocumento);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Grupo indígena:</strong><br/>');
        buffer.push(inscripcion.grupoIndigena.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Grado de estudios:</strong><br/>');
        buffer.push(inscripcion.nivelEstudio.nombre);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push(utils.construyeSocios(dp.socios));

        buffer.push('<h4>Datos de contacto</h4>');
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Número de teléfono / celular:</strong><br/>');
        buffer.push(inscripcion.numeroTelefono);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Correo electrónico:</strong><br/>');
        buffer.push(inscripcion.correoElectronico);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        var domicilio = inscripcion.domicilio;
        buffer.push('<h4>Domicilio del productor</h4>');
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Calle:</strong><br/>');
        buffer.push(domicilio.calle);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Número exterior:</strong><br/>');
        buffer.push(domicilio.numeroExterior);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Número interior:</strong><br/>');
        buffer.push(domicilio.numeroInterior || '');
        buffer.push('</div>');
        buffer.push('</div>'); // end-row

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Localidad:</strong><br/>');
        buffer.push(domicilio.localidad);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Municipio:</strong><br/>');
        buffer.push(domicilio.municipio.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Estado:</strong><br/>');
        buffer.push(domicilio.estado.nombre || '');
        buffer.push('</div>');
        buffer.push('</div>'); // end-row

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Código postal:</strong><br/>');
        buffer.push(domicilio.codigoPostal);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        if (inscripcion.contratos && inscripcion.contratos.length > 0) {
            buffer.push('<h4>Contratos del productor</h4>');
            buffer.push('<div class="table-responsive">');
            buffer.push('<table class="table table-bordered table-striped">');
            buffer.push('<thead class="thead-dark"><tr>');
            buffer.push('<th>No. contrato</th>');
            buffer.push('<th>Folio</th>');
            buffer.push('<th>Registrante</th>');
            buffer.push('<th>Cantidad contratada</th>');
            buffer.push('<th>Cantidad contratada cobertura</th>');
            buffer.push('<th>Estatus</th>');
            buffer.push('</tr></thead>');
            buffer.push('<tbody>');
            buffer.push(utils.construyeTablaContratos(inscripcion.contratos));
            buffer.push('<tbody>');
            buffer.push('</table>');
            buffer.push('</div>')
            buffer.push('<br/>');
        }

        var cuenta = inscripcion.cuentaBancaria;
        buffer.push('<h4>Datos bancarios del productor</h4>');
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Banco:</strong><br/>');
        buffer.push(cuenta.banco.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>CLABE:</strong><br/>');
        buffer.push(cuenta.clabe);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push(utils.construyeEmpresasAsociadas(inscripcion.empresas));

        buffer.push('<h4>Predios del productor</h4>');
        buffer.push('<div class="table-responsive">');
        buffer.push('<table class="table table-bordered table-striped">');
        buffer.push('<thead class="thead-dark"><tr>');
        buffer.push('<th>#</th>');
        buffer.push('<th>Folio</th>');
        buffer.push('<th>Tipo cultivo</th>');
        buffer.push('<th>Posesión</th>');
        buffer.push('<th>R. hídrico</th>');
        buffer.push('<th>Municipio, estado</th>');
        buffer.push('<th>Ubicación</th>');
        buffer.push('<th>V&nbsp;(t)</th>');
        buffer.push('<th>S&nbsp;(ha)</th>');
        buffer.push('<th>R&nbsp;(t/ha)</th>');
        buffer.push('</tr></thead>');
        buffer.push('<tbody>');
        buffer.push(utils.construyeTablaPredios(inscripcion.predios, {incluirIndex: true}));
        buffer.push('<tbody>');
        buffer.push(utils.agregaTotales(inscripcion.predios));
        buffer.push('</table>');
        buffer.push('</div>');
        buffer.push('<br/>');

        if (inscripcion.prediosDocumentos.length > 0) {
            buffer.push('<h5>Documentos de predios</h5>');
            buffer.push('<div class="table-responsive">');
            buffer.push('<table id="predios-documento-table" class="table table-bordered table-striped">');
            buffer.push('<thead class="thead-dark"><tr>');
            buffer.push('<th>No. Predio</th>');
            buffer.push('<th>Emisor</th>');
            buffer.push('<th>Receptor</th>');
            buffer.push('<th>Tipo</th>');
            buffer.push('<th>SAT</th>');
            buffer.push('<th>Duplicado</th>');
            buffer.push('<th>Total</th>');
            buffer.push('</tr></thead>');
            buffer.push('<tbody>');
            buffer.push(utils.construyeTablaDocumentosPredios(inscripcion.prediosDocumentos));
            buffer.push('<tbody>');
            buffer.push('</table>');
            buffer.push('</div>');
            buffer.push('<br/>');
        }

        if (inscripcion.informacionSemilla) {
            buffer.push('<h4>Semilla cerificada SNICS</h4>');
            buffer.push('<div class="row">'); // row
            buffer.push('<div class="col-md-4">');
            buffer.push('<strong>No. de folio de registro en el DPOCS:</strong><br/>');
            buffer.push(inscripcion.informacionSemilla.snics);
            buffer.push('</div>');
            buffer.push('<div class="col-md-4">');
            buffer.push('<strong>Cantidad (Toneladas):</strong><br/>');
            buffer.push(inscripcion.informacionSemilla.toneladas);
            buffer.push('</div>');
            buffer.push('<div class="col-md-4">');
            buffer.push('<strong>No. de folios:</strong><br/>');
            buffer.push(inscripcion.informacionSemilla.cantidadFolios);
            buffer.push('</div>');
            buffer.push('</div>'); // end-row

            buffer.push('<div class="row">'); // row
            buffer.push('<div class="col-md-4">');
            buffer.push('<strong>Nombre y razon social:</strong><br/>');
            buffer.push(inscripcion.informacionSemilla.nombre);
            buffer.push('</div>');
            buffer.push('<div class="col-md-4">');
            buffer.push('<strong>RFC:</strong><br/>');
            buffer.push(inscripcion.informacionSemilla.rfc);
            buffer.push('</div>');
            buffer.push('</div>'); // end-row
            buffer.push('<br/>');
        }

        buffer.push('<h4>Beneficiario</h4>');
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Nombre:</strong><br/>');
        buffer.push(inscripcion.nombreBeneficiario);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Apellidos:</strong><br/>');
        buffer.push(inscripcion.apellidosBeneficiario);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>CURP:</strong><br/>');
        buffer.push(inscripcion.curpBeneficiario);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Parentesco:</strong><br/>');
        buffer.push(inscripcion.parentesco.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Duplicado:</strong><br/>');
        buffer.push(inscripcion.beneficiarioDuplicado ?
        '<strong class="text-danger"><i class="fas fa-times-circle"></i> Sí</strong>'
        : '<strong class="text-success"><i class="fas fa-check-circle"></i> No</strong>');
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        utils.marcaArchivosCancelados(inscripcion.archivos, inscripcion.predios, 'documentacion-predio');
        utils.marcaArchivosCancelados(inscripcion.archivos, inscripcion.empresas, 'acta-constitutiva-sociedad');
        utils.marcaArchivosCancelados(inscripcion.archivos, inscripcion.empresas, 'lista-socios');
        buffer.push(utils.construyeAnexos(inscripcion.archivos));

        buffer.push('<div class="text-right">');
        buffer.push('<a role="button" class="btn btn-outline-success" href="/maiz/resources/productores/maiz/inscripcion/'
                + inscripcion.uuid + '/inscripcion-productor.pdf" target="_blank"><i class="fas fa-file-pdf"></i> Descargar registro de productor</a>');
        buffer.push('</div>');
        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card

        return buffer.join('\n');
    }

    handlers.muestraUbicacion = function (e) {
        e.preventDefault();
        var ubicacion = $(e.target).html();
        $('#maps').muestraMaps({
            ubicacion: ubicacion
        });
    };

    utils.construyeSocios = function (socios) {
        if (!socios || socios.length === 0) {
            return '';
        }

        var buffer = [];
        buffer.push('<h5>Socios</h5>');
        buffer.push('<div class="row">');
        buffer.push('<div class="col-md-12">');
        for (var i = 0; i < socios.length; i++) {
            buffer.push(socios[i].curp);
            if (i + 1 < socios.length) {
                buffer.push(', ');
            }
        }
        buffer.push('</div>');
        buffer.push('</div>');
        buffer.push('<br/>');

        return buffer.join('');
    }
    utils.construyeTablaContratos = function (contratos) {
        var buffer = [];
        for (var i = 0; i < contratos.length; i++) {
            var contrato = contratos[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            if (contrato.archivo) {
                buffer.push('<a href="/maiz/resources/archivos/maiz/' + contrato.archivo.uuid + '/' + contrato.archivo.nombre + '" target="_blank">');
                buffer.push(contrato.fechaCancelacion ? '<strike>' + contrato.numeroContrato + '</strike>' : contrato.numeroContrato);
                buffer.push('</a>');
            } else {
                buffer.push(contrato.numeroContrato);
            }
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(contrato.folio);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(contrato.empresa);
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(contrato.cantidadContratada);
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(contrato.cantidadContratadaCobertura);
            buffer.push('</td>');
            if (contrato.estatus) {
                buffer.push(contrato.estatus.clave === 'positiva'
                        ? '<td class="text-success"><i class="fas fa-check-circle"></i> '
                        : '<td class="text-danger"><i class="fas fa-exclamation-circle"></i> ');
                buffer.push(contrato.estatus.nombre);
            } else {
                buffer.push('<td>');
                buffer.push('--');
            }
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        return buffer.join('');
    }

    utils.construyeTablaPredios = function (predios, params) {
        params = params || {};
        var buffer = [];
        for (var i = 0; i < predios.length; i++) {
            var predio = predios[i];
            buffer.push('<tr>');
            if (params.incluirIndex) {
                buffer.push('<td>');
                buffer.push(i + 1);
                buffer.push('</td>');
            }
            buffer.push('<td>');
            buffer.push(predio.fechaCancelacion ? '<strike>' + predio.folio + '</strike>' : predio.folio);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.fechaCancelacion ? '<strike>' + predio.tipoCultivo.nombre + '</strike>' : predio.tipoCultivo.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.fechaCancelacion ? '<strike>' + predio.tipoPosesion.nombre + ', ' + predio.tipoDocumentoPosesion.nombre + '</strike>' : predio.tipoPosesion.nombre + ', ' + predio.tipoDocumentoPosesion.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.fechaCancelacion ? '<strike>' + predio.regimenHidrico.nombre + '</strike>' : predio.regimenHidrico.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.fechaCancelacion ? '<strike>' + predio.municipio.nombre + ', ' + predio.estado.nombre + '</strike>' : predio.municipio.nombre + ', ' + predio.estado.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            if (predio.latitud) {
                buffer.push(predio.fechaCancelacion ? '<strike>' + '<a class="muestra-ubicacion" href="#">[' + predio.latitud + ',' + predio.longitud + ']</a>' + '</strike>' : '<a class="muestra-ubicacion" href="#">[' + predio.latitud + ',' + predio.longitud + ']</a>');
            } else {
                buffer.push(predio.fechaCancelacion ? '<strike>' + '<a class="muestra-ubicacion" href="#">[' + predio.latitud1 + ',' + predio.longitud1 + ']</a>,' + '</strike>' : '<a class="muestra-ubicacion" href="#">[' + predio.latitud1 + ',' + predio.longitud1 + ']</a>,');
                buffer.push(predio.fechaCancelacion ? '<strike>' + '<a class="muestra-ubicacion" href="#">[' + predio.latitud2 + ',' + predio.longitud2 + ']</a>,' + '</strike>' : '<a class="muestra-ubicacion" href="#">[' + predio.latitud2 + ',' + predio.longitud2 + ']</a>,');
                buffer.push(predio.fechaCancelacion ? '<strike>' + '<a class="muestra-ubicacion" href="#">[' + predio.latitud3 + ',' + predio.longitud3 + ']</a>,' + '</strike>' : '<a class="muestra-ubicacion" href="#">[' + predio.latitud3 + ',' + predio.longitud3 + ']</a>,');
                buffer.push(predio.fechaCancelacion ? '<strike>' + '<a class="muestra-ubicacion" href="#">[' + predio.latitud4 + ',' + predio.longitud4 + ']</a>,' + '</strike>' : '<a class="muestra-ubicacion" href="#">[' + predio.latitud4 + ',' + predio.longitud4 + ']</a>');
            }
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            var volumen = predio.solicitado === 'solicitado' ? (utils.format(predio.volumen, 3) + utils.setRojo(utils.format(predio.volumenSolicitado, 3))) : utils.format(predio.volumen, 3);
            buffer.push(predio.fechaCancelacion ? '<strike>' + volumen + '</strike>' : volumen);

            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(predio.fechaCancelacion ? '<strike>' + utils.format(predio.superficie, 3) + '</strike>' : utils.format(predio.superficie, 3));
            buffer.push('</td>');

            buffer.push(predio.fechaCancelacion
                    ? '<strike>' + predio.solicitado !== 'solicitado' && predio.rendimientoMaximo > 0 && predio.rendimiento > predio.rendimientoMaximo
                    ? '<td class="text-right bg-danger">' : '<td class="text-right">' + '</strike>'
                    : predio.solicitado !== 'solicitado' && predio.rendimientoMaximo > 0 && predio.rendimiento > predio.rendimientoMaximo
                    ? '<td class="text-right bg-danger">' : '<td class="text-right">');
            var rendimiento = predio.solicitado === 'solicitado' ? (utils.format(predio.rendimiento, 3) + utils.setRojo(utils.format(predio.rendimientoSolicitado, 3))) : (utils.format(predio.rendimiento, 3));
            buffer.push(predio.fechaCancelacion
                    ? (predio.solicitado !== 'solicitado' && predio.rendimientoMaximo > 0 && predio.rendimiento > predio.rendimientoMaximo) ? '<strike> <span class="font-weight-bold text-white">' + rendimiento + '</span> </strike>'
                    : '<strike>' + rendimiento + '</strike>'
                    : (predio.solicitado !== 'solicitado' && predio.rendimientoMaximo > 0 && predio.rendimiento > predio.rendimientoMaximo) ? '<span class="font-weight-bold text-white">' + rendimiento + '</span>' : rendimiento);
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        buffer.push('<tr>');
        buffer.push('</tr>');
        return buffer.join('');
    };

    utils.construyeEmpresasAsociadas = function (empresas) {
        if (!empresas || empresas.length === 0) {
            return '';
        }

        var buffer = [];
        buffer.push('<h4>Sociedades</h4>');
        buffer.push('<table class="table table-bordered table-striped">');
        buffer.push('<thead class="thead-dark"><tr>');
        buffer.push('<th>RFC</th>');
        buffer.push('<th>Nombre</th>');
        buffer.push('</tr></thead>');
        buffer.push('<tbody>');
        for (var i = 0; i < empresas.length; i++) {
            var empresa = empresas[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(empresa.fechaCancelacion ? '<strike>' + empresa.rfc + '</strike>' : empresa.rfc);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(empresa.nombre);
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        buffer.push('</tbody></table><br/>');
        return buffer.join('')
    }

    utils.construyeAnexos = function (archivos) {
        if (!archivos || archivos.length === 0) {
            return '';
        }

        var buffer = [];
        buffer.push('<h4>Archivos anexos</h4>');
        buffer.push('<ul>');
        for (var i = 0; i < archivos.length; i++) {
            var archivo = archivos[i];
            buffer.push('<li><a href="/maiz/resources/archivos/maiz/' + archivo.uuid + '/'
                    + archivo.nombre + '?inline=true" target="_blank">');
            buffer.push(!archivo.activo ? '<strike>' + archivo.nombreOriginal + '</strike>' : archivo.nombreOriginal);
            buffer.push(' [' + archivo.tipo + '] ');
            buffer.push(' <i class="fas fa-file"></i></a></li>');
        }
        buffer.push('</ul><br/>');
        return buffer.join('');
    }

    utils.marcaArchivosCancelados = function (archivos, predios, tipoArchivo) {
        archivos = archivos || [];
        predios = predios || [];
        for (var i = 0; i < archivos.length; i++) {
            var archivo = archivos[i];
            var tmp = archivo.tipo.split('_');
            if (tmp.length === 1) {
                continue;
            }
            var tipo = tmp[1];
            if (tipo !== tipoArchivo) {
                continue;
            }
            var index = parseInt(tmp[0], 10);
            var predio = utils.getPredio(predios, index);
            if (predio && !predio.fechaCancelacion) {
                continue;
            }
            archivo.activo = false;
        }
    };

    utils.getPredio = function (predios, index) {
        for (var i = 0; i < predios.length; i++) {
            if (predios[i].orden === index) {
                return predios[i];
            }
        }
        return null;
    };

    utils.setRojo = function (val) {
        return '\n<p style="color:Red;"> (' + val + ')</p>';
    };

    utils.agregaTotales = function (predios) {
        var volumenTot = 0;
        var volumenSolTot = 0;
        var superficieTot = 0;
        var rendimientoTot = 0;
        var rendimientoSolTot = 0;
        for (var p in predios) {
            if (!predios[p].fechaCancelacion) {
                volumenTot += parseFloat(predios[p].volumen);
                volumenSolTot += parseFloat(predios[p].volumenSolicitado);
                superficieTot += parseFloat(predios[p].superficie);
            }
        }

        rendimientoTot = parseFloat(volumenTot / superficieTot);
        rendimientoSolTot += parseFloat(volumenSolTot / superficieTot);
        var isSolicitado = volumenTot !== volumenSolTot;
        var buffer = [];
        buffer.push('<tfoot class="thead-dark">');
        buffer.push('<tr>');
        buffer.push('<th colspan="7">Total</th>');
        buffer.push('<th class="text-right">' + utils.format(volumenTot, 3)
                + (isSolicitado ? utils.setRojo(utils.format(volumenSolTot, 3)) : '') + '</th>');
        buffer.push('<th class="text-right">' + utils.format(superficieTot, 3) + '</th>');
        buffer.push('<th class="text-right">' + utils.format(rendimientoTot, 3)
                + (isSolicitado ? utils.setRojo(utils.format(rendimientoSolTot, 3)) : '') + '</th>');
        buffer.push('</tr>');
        buffer.push('</tfoot>');
        return buffer.join('');
    };

    utils.format = function (str, fix) {
        return str.toFixed(fix).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    };

    utils.construyeTablaDocumentosPredios = function (pDocumetos) {
        var buffer = [];

        for (var i = 0; i < pDocumetos.length; i++) {
            var a = pDocumetos[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(a.numero);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(a.rfcEmisor);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(a.rfcReceptor);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(a.tipo);
            buffer.push('</td>');
            buffer.push('<td class="text-center">');
            buffer.push(a.validacionSat
                    ? '<i class="fas fa-check-circle text-success"></i>'
                    : (a.validacionSat !== undefined ? '<i class="fas fa-times-circle text-danger"></i>' : '--'));
            buffer.push('</td>');
            buffer.push('<td class="text-center">');
            buffer.push(a.duplicado === undefined ? '--' : !a.duplicado
                    ? '<i class="fas fa-check-circle text-success"></i>'
                    : '<button id="mostrar-repetidos-' + i + '" class="btn btn-outline-danger btn-sm mostrar-repetidos"><i class="fas fa-times-circle"></i></button>');
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(a.total === undefined ? '--' : utils.format(a.total, 3));
            buffer.push('</td>');
            buffer.push('</tr>');
        }


        return buffer.join('');
    };

})(jQuery);