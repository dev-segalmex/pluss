/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.maiz.consultas.productor');
    var data = {
        tipoBusqueda: 'datos-generales',
        busquedaAgrupada: false
    };
    var handlers = {};
    var utils = {};

    $.segalmex.maiz.consultas.productor.init = function (params) {
        utils.inicializaValidaciones();
        utils.cargaCatalogos();
        $('#button-buscar').click(handlers.busca);
        $('#button-buscar-agrupado').click(handlers.buscaAgrupaciones);
        $('#button-limpiar').click(handlers.limpia);
        $('#button-exportar').click(handlers.exporta);
        $('#button-regresar-resultados').click(handlers.regresaResultados);
        $('#button-regresar-resultados-agrupaciones').click(handlers.regresaResultadosAgrupaciones);
        $('#menu-bandejas a.nav-link').click(handlers.cambiaBandeja);
        $('#ciclo').change(handlers.exportaCiclo);
    };

    handlers.cambiaBandeja = function (e) {
        e.preventDefault();
        $('#div-consultas #' + 'div-' + data.tipoBusqueda).hide();
        var id = e.target.id.substring('muestra-'.length);
        if (data.tipoBusqueda === id) {
            $('#div-consultas #' + 'div-' + data.tipoBusqueda).show();
            return;
        }
        utils.limpiar();
        data.tipoBusqueda = id;
        $('#div-consultas #' + 'div-' + data.tipoBusqueda).show();
        $('#menu-bandejas a.nav-link').removeClass('active');
        $(e.target).addClass('active');
        $('#button-exportar-ciclo,#button-flecha,#button-exportar-predios,#button-exportar-contratos,#button-exportar-predios-planeacion').prop('disabled', true);
        utils.ocultaAgrupado(id);
    };

    utils.ocultaAgrupado = function (bandeja) {
        if (bandeja !== 'datos-generales') {
            $('#button-buscar-agrupado').hide();
        } else {
            $('#button-buscar-agrupado').show();
        }
    };

    handlers.exporta = function (e) {
        e.preventDefault();
        utils.desHabilitaCtrls(true);
        utils.configuraCiclo(true);
        if (!utils.validaFiltros(e)) {
            return;
        }
        var filtros = utils.getFiltros(e);
        if (!filtros) {
            utils.desHabilitaCtrls(false);
            return;
        }
        $.segalmex.openPdf('/maiz/resources/productores/maiz/inscripcion/csv/resultados-productor.xlsx?' + $.param(filtros));
        utils.desHabilitaCtrls(false);
        $('#cargando-resultados').hide();
    };

    handlers.exportaCiclo = function (e) {
        e.preventDefault();
        var ciclo = $.segalmex.get(data.ciclos, $(e.target).val());
        if (!ciclo) {
            $('#button-exportar-ciclo,#button-exportar-predios,#button-exportar-contratos,#button-exportar-predios-planeacion').attr('href', '#');
            $('#button-exportar-ciclo,#button-flecha,#button-exportar-predios,#button-exportar-contratos,#button-exportar-predios-planeacion').prop('disabled', true);
            return;
        }
        $('#button-exportar-ciclo,#button-flecha,#button-exportar-predios,#button-exportar-contratos,#button-exportar-predios-planeacion').prop('disabled', false);
        $('#button-exportar-ciclo').attr('href', '/maiz/resources/productores/reporte/' + ciclo.id + '.xlsx');
        $('#button-exportar-predios').attr('href', '/maiz/resources/productores/reporte/predio/' + ciclo.id + '.xlsx');
        $('#button-exportar-contratos').attr('href', '/maiz/resources/productores/reporte/contrato/' + ciclo.id + '.xlsx');
        $('#button-exportar-predios-planeacion').attr('href', '/maiz/resources/productores/reporte/predio/planeacion/' + ciclo.id + '.xlsx');
    };

    utils.desHabilitaCtrls = function (disabled) {
        $('#button-exportar').prop('disabled', disabled);
        $('#button-buscar').prop('disabled', disabled);
        $('#button-buscar-agrupado').prop('disabled', disabled);
        $('#button-limpiar').prop('disabled', disabled);
    };

    utils.validaFiltros = function (e) {
        $('#resultados-busqueda').hide();
        var errores = [];
        $('.valid-field').valida(errores, false);
        $.segalmex.validaFechaInicialContraFinal(errores, 'fecha-inicio-registro', 'fecha-fin-registro');
        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            utils.desHabilitaCtrls(false);
            $('#cargando-resultados').hide();
            return false;
        }

        return true;
    };

    handlers.busca = function (e) {
        e.preventDefault();
        $('#div-war-resultados').hide();
        utils.desHabilitaCtrls(true);
        utils.configuraCiclo(true);

        if (!utils.validaFiltros(e)) {
            return;
        }
        var filtros = utils.getFiltros(e);
        if (!filtros) {
            utils.desHabilitaCtrls(false);
            return;
        }
        $('#cargando-resultados').show();
        $.ajaxq('busquedasQueue', {
            url: '/maiz/resources/productores/maiz/inscripcion/',
            type: 'GET',
            data: filtros,
            dataType: 'json'
        }).done(function (response) {
            utils.desHabilitaCtrls(false);
            $('#cargando-resultados').hide();
            var renglones = '';
            if (response.length === 0) {
                renglones = '<tr><td colspan="8" class="text-center">Sin resultados</td></tr>';
                $('#data').html(renglones);
            } else {
                renglones = utils.creaDatatable(response);
                $('#data').html(renglones);
                utils.configuraTablaResultados();
                $('#table-resultados-busqueda').on('click', 'a.link-uuid', handlers.muestraDetalle);
                if (response.length === 10000){
                    $('#div-war-resultados').show();
                }
            }
            data.busquedaAgrupada = false;
            $('#resultados-busqueda').slideDown();
        }).fail(function () {
            alert('Error: No fue posible realizar la consulta.');
            utils.desHabilitaCtrls(false);
            $('#cargando-resultados').hide();
        });
    };

    handlers.buscaAgrupaciones = function (e) {
        e.preventDefault();
        $('#div-war-resultados').hide();
        utils.desHabilitaCtrls(true);
        utils.configuraCiclo(false);
        $('.valid-field').limpiaErrores();
        if (!utils.validaFiltros(e)) {
            return;
        }
        var filtros = utils.getFiltros(e);
        if (!filtros) {
            utils.desHabilitaCtrls(false);
            return;
        }
        $('#cargando-resultados').show();
        $.ajaxq('busquedasQueue', {
            url: '/maiz/resources/productores/maiz/inscripcion/agrupada',
            type: 'GET',
            data: filtros,
            dataType: 'json'
        }).done(function (response) {
            utils.desHabilitaCtrls(false);
            $('#cargando-resultados').hide();
            data.agrupaciones = response;
            for (var i = 0; i < data.agrupaciones.length; i++) {
                data.agrupaciones[i].id = i + 1;
            }
            var renglones = '';
            if (response.length === 0) {
                renglones = '<tr><td colspan="5" class="text-center">Sin resultados</td></tr>';
                $('#data').html(renglones);
                $('#table-resultados-busqueda-agrupaciones').on('click', 'a.link-id', handlers.buscaAgrupados);
            } else {
                renglones = utils.creaDatatableAgrupaciones(data.agrupaciones);
                $('#data').html(renglones);
                $('#table-resultados-busqueda-agrupaciones').on('click', 'a.link-id', handlers.buscaAgrupados);
            }
            data.busquedaAgrupada = true;
            $('#resultados-busqueda').slideDown();
        }).fail(function () {
            alert('Error: No fue posible realizar la consulta.');
            utils.desHabilitaCtrls(false);
            $('#cargando-resultados').hide();
        });
    };

    handlers.buscaAgrupados = function (e) {
        e.preventDefault();
        var id = e.target.id.split('.')[1];
        var agrupacion = $.segalmex.get(data.agrupaciones, id);

        $('#resultados-busqueda-agrupados').show();
        $('#datos-busqueda-form,#resultados-busqueda').hide();
        $('#cargando-resultados').show();
        $('#data-agrupados').html('');

        if (parseInt(agrupacion.total) > 10000) {
            alert('Solo se podrán visualizar 10,000 registros.');
        }

        var filtros = utils.getFiltros(e);
        filtros.estatus = agrupacion.estatus.clave;
        filtros.ciclo = agrupacion.ciclo.id;
        filtros.estado = agrupacion.estado.id;

        $.ajaxq('busquedasQueue', {
            url: '/maiz/resources/productores/maiz/inscripcion/',
            type: 'GET',
            data: filtros,
            dataType: 'json'
        }).done(function (response) {
            utils.desHabilitaCtrls(false);
            $('#resultados-busqueda-agrupados').show();
            $('#datos-busqueda-form,#resultados-busqueda').hide();
            var renglones = '';
            if (response.length === 0) {
                renglones = '<tr><td colspan="5" class="text-center">Sin resultados</td></tr>';
                $('#data-agrupados').html(renglones);
            } else {
                renglones = utils.creaDatatable(response);
                $('#data-agrupados').html(renglones);
                utils.configuraTablaResultados();
                $('#table-resultados-busqueda').on('click', 'a.link-uuid', handlers.muestraDetalle);
            }
            $('#cargando-resultados').hide();
        }).fail(function () {
            alert('Error: No fue posible realizar la consulta.');
            utils.desHabilitaCtrls(false);
            $('#cargando-resultados').hide();
        });
    };

    utils.getFiltros = function (e) {

        var datos = {
            folio: $('#folio').val(),
            estatus: $('#estatus').val(),
            fechaInicio: $('#fecha-inicio-registro').val(),
            fechaFin: $('#fecha-fin-registro').val(),
            validador: $('#validador').val(),
            curp: $('#curp').val(),
            rfcProductor: $('#rfc-productor').val(),
            nombre: $('#nombre').val(),
            papellido: $('#papellido').val(),
            sapellido: $('#sapellido').val(),
            ciclo: $('#ciclo').val(),
            rfcEmpresa: $('#rfc-ventanilla').val(),
            tipoRegistro: $('#tipo-registro').val()
        };
        if (data.tipoBusqueda === 'datos-personales') {
            if (datos.nombre === '' && datos.papellido === '' && datos.sapellido === '' && datos.curp === '' && datos.rfcProductor === '') {
                alert('* Se requiere al menos un campo');
                $('#cargando-resultados').hide();
                utils.desHabilitaCtrls(false);
                return;
            }
        }
        for (var prop in datos) {
            if (datos[prop] === '') {
                delete datos[prop];
            }
        }
        if (datos.estatus === '0') {
            delete datos.estatus;
        }
        if (datos.validador === '0') {
            delete datos.validador;
        }
        if (datos.fechaInicio) {
            datos.fechaInicio = $.segalmex.date.fechaToIso(datos.fechaInicio);
        }
        if (datos.fechaFin) {
            datos.fechaFin = $.segalmex.date.fechaToIso(datos.fechaFin);
        }
        if (datos.ciclo === '0') {
            delete datos.ciclo;
        }
        if (datos.tipoRegistro === '0') {
            delete datos.tipoRegistro;
        }
        return datos;
    };

    handlers.muestraDetalle = function (e) {
        e.preventDefault();
        var uuid = e.target.id.split('.')[1];
        $.ajax({
            url: '/maiz/resources/productores/maiz/inscripcion/' + uuid,
            data: {archivos: true, datos: true, historial: true, contratos: true, comentarios: true},
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            // Mostramos el detalle
            data.prediosDocumentos = response.prediosDocumentos;
            $.segalmex.maiz.inscripcion.productor.vista.muestraInscripcion(response, 'detalle-inscripcion-productor');
            $('#detalle-tipo-productor').html($.segalmex.maiz.inscripcion.vista.construyeTipoProductor(response.tipoProductor));
            $('#estatus-inscripcion-productor').html($.segalmex.maiz.inscripcion.vista.construyeEstatus(response));
            $('#historial-inscripcion-productor').html($.segalmex.maiz.inscripcion.vista.construyeHistorial(response.historial));
            $('#datos-capturados-inscripcion-productor').html($.segalmex.maiz.inscripcion.vista.construyeDatosCapturados(response.datos));
            $('#comentarios-inscripcion-productor').html($.segalmex.maiz.inscripcion.vista.construyeComentarios(response.comentarios, $.segalmex.common.pagina.comun.registrado));
            $('#predios-documento-table').on('click', 'button.mostrar-repetidos', handlers.muestraFoliosRepetidos);
            $('#detalle-elemento').show();
            $('#datos-busqueda-form,#resultados-busqueda,#resultados-busqueda-agrupados').hide();
        }).fail(function () {
            alert('Error: No fue posible obtener el registro.');
        });
    };

    handlers.limpia = function (e) {
        utils.limpiar();
    };

    handlers.regresaResultados = function (e) {
        e.preventDefault();
        if (!data.busquedaAgrupada) {
            $('#detalle-elemento').hide();
            $('#datos-busqueda-form,#resultados-busqueda').show();
        }
        if (data.busquedaAgrupada) {
            $('#detalle-elemento').hide();
            $('#resultados-busqueda-agrupados').show();
        }
    };

    handlers.regresaResultadosAgrupaciones = function (e) {
        e.preventDefault();
        $('#resultados-busqueda-agrupados').hide();
        $('#datos-busqueda-form,#resultados-busqueda').show();
    };

    handlers.muestraFoliosRepetidos = function (e) {
        e.preventDefault();
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('mostrar-repetidos-'.length), 10);
        for (var i = 0; i < data.prediosDocumentos.length; i++) {
            if (idx === i) {
                var pd = data.prediosDocumentos[i];
            }
        }
        $.ajax({
            url: '/maiz/resources/predio-documento/duplicados/' + pd.uuidTimbreFiscalDigital,
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            var folios = response.join(',');
            $('#label-folios').html('<strong >' + folios + '</strong>');
            $('#muestra-repetidos-modal').modal('show');
        }).fail(function () {
            alert('Error: No se pudo obtener los folios duplicados.');
        });
    };

    utils.construyeRenglones = function (resultados) {
        var buffer = [];
        for (var i = 0; i < resultados.length; i++) {
            var resultado = resultados[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td><a class="link-uuid" href="#" id="uuid.' + resultado.uuid + '">');
            buffer.push(resultado.folio);
            buffer.push('</a></td>');
            buffer.push('<td>');
            buffer.push($.segalmex.date.isoToFecha(resultado.fechaCreacion));
            buffer.push('</td>');
            buffer.push('<td>');
            var dp = resultado.datosProductor;
            buffer.push(dp.tipoPersona.clave === 'fisica' ? utils.construyeNombreCompleto(dp) : dp.nombreMoral);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(dp.curp);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(dp.rfc);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(dp.tipoPersona.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.estatus.nombre);
            buffer.push('</td>');
            buffer.push('</tr>');
        }

        return buffer.join('\n');
    }

    utils.construyeNombreCompleto = function (dp) {
        var buffer = [];
        if (dp.nombre) {
            buffer.push(dp.nombre);
        }
        if (dp.primerApellido) {
            buffer.push(dp.primerApellido);
        }
        if (dp.segundoApellido) {
            buffer.push(dp.segundoApellido);
        }
        return buffer.join(' ');
    }

    utils.inicializaValidaciones = function () {
        $(".valid-field").configura({required: false});

        $('#fecha-inicio-registro,#fecha-fin-registro').configura({
            type: 'date',
            required: false
        });
        $('#fecha-inicio-registro,#fecha-fin-registro').datepicker({
            language: "es",
            autoclose: true,
            forceParse: false,
            format: 'dd/mm/yyyy',
            yearRange: "c-10:c"
        });
        $('#folio').configura({
            pattern: /^\d{1,6}$/,
            minlength: 1,
            maxlength: 6,
            required: true
        });
        $('#curp').configura({
            type: 'curp',
            required: false
        });

        $('#rfc-productor').configura({
            type: 'rfc',
            required: false
        });
        $('#nombre,#papellido,#sapellido').configura({
            required: false
        });

        $('#rfc-ventanilla').configura({
            type: 'rfc',
            required: false
        });

        $('.valid-field').validacion();
        $('#div-consultas #div-datos-personales, #div-folio').hide();
    };

    utils.limpiar = function () {
        $('#estatus').val('0').change();
        $('#datos-busqueda-form input').val('');
        $('#ciclo').val('0').change();
        $('.valid-field').limpiaErrores();
        $('#resultados-busqueda').hide();
        $('#cargando-resultados').hide();
        $('#div-war-resultados').hide();
        $('#table-resultados-busqueda tbody').html('<tr><td colspan="6" class="text-center">Sin resultados</td></tr>');
        $('#detalle-inscripcion-contrato,#historial-inscripcion-contrato,#datos-capturados-inscripcion-contrato').html('');
        $('#validador').val('0').change();
        $('#tipo-registro').val('0');
    };

    utils.cargaCatalogos = function () {

        $.ajax({
            url: '/maiz/resources/paginas/consultas',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            $('#button-exportar-ciclo,#button-flecha,#button-exportar-predios,#button-exportar-contratos,#button-exportar-predios-planeacion').prop('disabled', true);
            $('#validador').actualizaCombo(response.validadores);
            $('#estatus').actualizaCombo(response.estatus, {value: 'clave'});
            $('#ciclo').actualizaCombo(response.ciclos);
            $('#tipo-registro').actualizaCombo(response.tiposRegistro, {value: 'clave'});
            data.ciclos = response.ciclos;
        }).fail(function () {
            alert('Error: No se pudo obtener los catálogos de la página.');
        });

    };

    utils.creaDatatable = function (resultados) {
        var buffer = [];
        buffer.push('<table id="table-resultados-busqueda"');
        buffer.push(' class="table table-striped" width="100%">');
        buffer.push('<thead class = "table-success">');
        buffer.push('<tr class="success">');
        buffer.push('<th>No.</th>');
        buffer.push('<th>Folio</th>');
        buffer.push('<th>Fecha de Registro</th>');
        buffer.push('<th>Nombre productor</th>');
        buffer.push('<th>CURP</th>');
        buffer.push('<th>RFC</th>');
        buffer.push('<th>Tipo</th>');
        buffer.push('<th>Estatus</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');
        for (var i = 0; i < resultados.length; i++) {
            var resultado = resultados[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push('</td>');
            buffer.push('<td><a class="link-uuid" href="#" id="uuid.' + resultado.uuid + '">');
            buffer.push(resultado.folio);
            buffer.push('</a></td>');
            buffer.push('<td>');
            buffer.push($.segalmex.date.isoToFecha(resultado.fechaCreacion));
            buffer.push('</td>');
            buffer.push('<td>');
            var dp = resultado.datosProductor;
            buffer.push(dp.tipoPersona.clave === 'fisica' ? utils.construyeNombreCompleto(dp) : dp.nombreMoral);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(dp.curp);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(dp.rfc);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(utils.getTipoRegistro(resultado.tipoRegistro));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.estatus.nombre);
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        buffer.push('</tbody>');
        buffer.push('</table>');

        buffer.push('<br/>');
        return buffer.join('');

    };

    utils.creaDatatableAgrupaciones = function (resultados) {
        var buffer = [];
        buffer.push('<table id="table-resultados-busqueda-agrupaciones"');
        buffer.push(' class="table table-striped" width="100%">');
        buffer.push('<thead class = "table-success">');
        buffer.push('<tr class="success">');
        buffer.push('<th>Ciclo</th>');
        buffer.push('<th>Estado</th>');
        buffer.push('<th>Estatus</th>');
        buffer.push('<th>Total</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');
        var total = 0;
        for (var a = 0; a < resultados.length; a++) {
            var resultado = resultados[a];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(resultado.ciclo.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.estado.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.estatus.nombre);
            buffer.push('</td>');
            buffer.push('<td class="text-right"><a class="link-id" href="#" id="id.' + resultado.id + '">');
            buffer.push(resultado.total);
            buffer.push('</a></td>');
            buffer.push('</tr>');
            total += parseInt(resultado.total);
        }
        buffer.push('<tfoot class="table-success">');
        buffer.push('<tr>');
        buffer.push('<th colspan="3">Total</th>');
        buffer.push('<th class="text-right">' + total + '</th>');
        buffer.push('</tr>');
        buffer.push('</tfoot>');
        buffer.push('</tbody>');
        buffer.push('</table>');

        buffer.push('<br/>');
        return buffer.join('');

    };

    utils.configuraTablaResultados = function () {
        var aoColumns = [{"bSortable": false}, {"bSortable": true}, {"bSortable": false},
            {"bSortable": true}, {"bSortable": true}, {"bSortable": true}, {"bSortable": true}, {"bSortable": true}];
        utils.configuraTablaFactura('table-resultados-busqueda', aoColumns);
    };

    utils.configuraTablaFactura = function (tablaId, aoColumns) {
        var table = $('#' + tablaId).DataTable({
            "oLanguage": {
                "sEmptyTable": "No se encontró información para mostrar en la tabla",
                "sSearch": "Buscar ",
                "sZeroRecords": "No se encontraron resultados."
            },
            "bFilter": true,
            "bInfo": false,
            "bPaginate": false,
            "aoColumns": aoColumns,
            "aoColumnDefs": [{"bSortable": false, "aTargets": ["no-sort"]}]
        });
        table.on('order.dt search.dt', function () {
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).order([[1, 'asc']]).draw(false);
        $("#" + tablaId + "_filter input").addClass('input-buscar');
        $(".dataTables_empty").attr("colspan", "100%");
    };

    utils.getTipoRegistro = function (tipoRegistro) {
        switch (tipoRegistro) {
            case 'fisica':
                return 'Física';
            case 'moral':
                return 'Moral';
            case 'asociado':
                return 'Asociado';
            default :
                return tipoRegistro;
        }
    };

    utils.configuraCiclo = function (requerido) {
        $('#ciclo').configura({
            required: requerido
        });
        $('#ciclo').validacion();
    };

})(jQuery);