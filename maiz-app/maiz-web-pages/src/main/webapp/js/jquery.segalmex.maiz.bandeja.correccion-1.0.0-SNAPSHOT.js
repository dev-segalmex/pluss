/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


(function ($) {
    $.segalmex.namespace('segalmex.maiz.inscripcion.correccion');

    var data = {
        tipoInscripcion: 'contratos',
        catalogos: null,
        catalogosProductor: null,
        catalogosFactura: null,
        entradas: [],
        requiereAnexos: false,
        inscripcion: null,
        rendimientos: {},
        edicionContratos: [],
        contratosRegistrados: [],
        edicionPredios: [],
        edicionAnexosPredio: [],
        edicionSociedades: [],
        anexosActaConstitutiva: [],
        anexosListaSocios: [],
        contratosPermitidos: null,
        prediosPermitidos: null,
        empresasPermitidos: null,
        uuid: '',
        archivosCargados: [],
        limiteArchivo: 24 * 1024 * 1024,
        contratosInscripcion: [],
        //solicitados
        permitePredioSolicitado: false,
        rendimientoSolValido: false,
        solicitado: false,
        rendimientoMaximo: 0,
        anexosPrediosolicitado: [],
        volumenPredio: 0,
        superficiePredio: 0,
        archivosPredio: false,
        anexosSiembraXlm: [],
        anexosRiegoXlm: [],
        archivosTmpSiembraXml: [],
        archivosTmpRiegoXml: [],
        archivosRequeridosRiego: [],
        archivosRqueridosSiembra: [],
        contratosProductores: [],
        totalProductoresContrato: 0,
        superficieContrato: 0.0,
        volumenContrato: 0.0,
        numeroProductores: 0
    };
    var handlers = {};
    var utils = {};

    $.segalmex.maiz.inscripcion.correccion.init = function () {
        $.segalmex.common.bandejas.init({
            configuracion: utils.configuracionBandejas(),
            namespace: $.segalmex.maiz.inscripcion.correccion
        });
        $('#menu-bandejas a.nav-link').click(handlers.cambiaBandeja);
        $('#menu-edicion a.nav-link').click(handlers.cambiaSeccionEdicion);
        $('#button-corrige').click(handlers.corrige);
        $('#button-regresar').click(handlers.regresar);
        $('#actualizar-button').click(handlers.actualiza);
        $('#nuevo-comentario-button').click(handlers.nuevoComentario);
        $('#agregar-comentario-button').click(handlers.agregaComentario);
        $('#adjuntar-button').click(handlers.adjuntar);
        $('#subir-button').click(handlers.subirArchivos);
        $('#enviar-button').click(handlers.enviar);
        $('#inscripcion-datos-capturados-incorrectos').on('click', 'button.btn-comentario', handlers.muestraComentario);
        $('#contenido-comentario').configura({
            minlength: 1,
            maxlength: 2047,
            allowSpace: true,
            textTransform: null,
            pattern: /^[A-Za-z0-9ÑñÁÉÍÓÚáéíóúÄËÏÖÜäëïöü@\.,:\n\-\(\)" ]*$/
        }).validacion();

        //Configuracion para la parte de edición
        utils.inicializaValidaciones();
        $('#edicion-tipo-posesion-predio').change(handlers.cambiaTipoPosesion);
        $('#edicion-estado-predio').change(handlers.cambiaEstadoPredio);
        $('#edicion-volumen-predio,#edicion-superficie-predio').change(handlers.cambiaRendimiento);
        $('#edicion-agregar-contrato-button').click(handlers.agregaContrato);
        $('#edicion-contratos-table').on('click', 'button.eliminar-contrato', handlers.eliminaContrato);
        $('#menu-tabs-predios a.nav-link').click(handlers.cambiaTipoGeorreferencia);
        $('#edicion-predios-table').on('click', 'button.eliminar-predio', handlers.eliminaPredio);
        $('#edicion-predios-table').on('click', 'a.muestra-ubicacion', handlers.muestraUbicacion);
        $('#agregar-predio-button').click(handlers.agregaPredio);
        $('#agregar-sociedad-button').click(handlers.agregaSociedad);
        $('#sociedades-table').on('click', 'button.eliminar-sociedad', handlers.eliminaSociedad);
        $('#button-guarda-edicion').click(handlers.guardaEdicion);
        //rendimiento solicitado
        $('#aceptar-rendimiento-solicitado').click(utils.confirmaRendimientoSolicitado);
        $('#cancelar-rendimiento-solicitado').click(utils.cancelarRendimientoSolicitado);
        $('#carga-siembra').click(utils.muestraModalSiembra);
        $('#carga-riego').click(utils.muestraModalRiego);
        $('#agregar-archivo-siembra').click(utils.agregaArchivoSiembra);
        $('#table-archivos-siembra-xml').on('click', 'button.eliminar-siembra', handlers.eliminaArchivoSiembra);
        $('#agregar-archivo-riego').click(utils.agregaArchivoRiego);
        $('#table-archivos-riego-xml').on('click', 'button.eliminar-riego', handlers.eliminaArchivoRiego);
        $('#permiso-siembra,#pago-riego').prop('disabled', true);
        $('#btn-mostrar-correctos').click(handlers.muestraCorrectos);
        $('#button-corrige-productores').click(handlers.corrigeContratosProductor);
        $('#productor-detalle-table').on('click', 'a.link-detalle-contrato-productor', handlers.muestraDetalleContratoProductor);
        $('#agregar-productor-button').click(handlers.agregaProductor);
        $('#button-eliminar-productores').click(handlers.eliminaContratosProductores);
        $('#subir-contrato-xls').click(utils.subirXlsxContratoProductor);
        $('#enviar-contrato-productor-xlsx').click(utils.enviarContratoPRoductorXlsx);
        $.ajax({
            type: 'GET',
            url: '/maiz/resources/paginas/inscripcion/',
            dataType: 'json'
        }).done(function (response) {
            data.catalogos = response;
        }).fail(function () {
            alert('Error: Al descargar los recursos de la página.');
        });
        $.ajax({
            type: 'GET',
            url: '/maiz/resources/paginas/inscripcion-productor/',
            dataType: 'json'
        }).done(function (response) {
            //cargamos los catalogos para los combos de factura
            $.ajax({
                type: 'GET',
                url: '/maiz/resources/paginas/inscripcion-factura/',
                dataType: 'json'
            }).done(function (response) {
                data.catalogosFactura = response;
            }).fail(function () {
                alert('Error: Al descargar los recursos para factura.');
            });

            $('#ciclo-agricola-global').cicloAgricola({
                ciclos: response.ciclos,
                actual: response.cicloSeleccionado,
                reload: true,
                sistema: 'maiz',
                showButton: false,
                onlyRead: true
            });
        }).fail(function () {
            alert('Error: Al descargar los recursos de la página.');
        });
        $('#detalle-elemento-documentacion input.form-control-file').change(handlers.verificaTamanoArchivo);
    };

    handlers.enviar = function (e) {
        e.preventDefault();
        for (var i = 0; i < data.archivosCargados.length; i++) {
            var a = data.archivosCargados[i];
            if (a.cargado === false) {
                alert('Error: es necesario subir todos los archivos anexos pendientes.');
                return;
            }
        }
        $('#enviar-modal').modal('show');
    };

    handlers.subirArchivos = function (e) {
        e.preventDefault();
        var archivos = [];
        var files = $('#archivos-cargados input.form-control-file');
        var filesOpcionales = $('#archivos-opcionales input.form-control-file');

        for (var i = 0; i < files.length; i++) {
            var a = files[i];
            if (a.files.length !== 0) {
                archivos.push(a);
            }
        }

        if (filesOpcionales.length > 0) {
            for (var i = 0; i < filesOpcionales.length; i++) {
                var a = filesOpcionales[i];
                if (a.files.length !== 0) {
                    archivos.push(a);
                }
            }
        }

        if (archivos.length === 0) {
            alert('Se requiere seleccionar al menos un archivo para subir.');
            return;
        }
        $('#div-carga-archivos').cargaArchivos({
            archivos: utils.getArchivos(archivos),
            callBackCerrar: handlers.cierraPluginArchivos,
            btnDescarga: false,
            indicaciones: 'Por favor espere mientras se suben los documentos anexos.'
        });
    };

    handlers.cierraPluginArchivos = function () {
        var actual = $.segalmex.common.bandejas.actual();
        var datos = {
            datos: true,
            comentarios: true,
            archivosCargados: true,
            expand: 3
        };

        $.ajax({
            url: '/maiz/resources/productores/maiz/inscripcion/' + data.uuid,
            data: datos,
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            // Construimos el detalle
            utils.construyeInscripcion(response, 'productores');
            $('#botones-entrada button.btn,#div-carta-acuerdo,.detalle-elemento-form').hide();
            $('#button-regresar,#nuevo-comentario-button').show();
            $('#carta-acuerdo-tmp-pdf').prop('disabled', true);
            switch (actual) {
                case 'documentacion':
                    if (response.empresas.length > 0) {
                        $('#div-carta-acuerdo').show();
                        $('#carta-acuerdo-tmp-pdf').prop('disabled', false);
                    }
                    $('#subir-button').show();
                    $('#enviar-button').show();
                    break;
            }

            $('#lista-entradas').hide();
            $('#detalle-elemento-' + actual).show();
            $('#detalles-entrada,#botones-entrada').show();
        }).fail(function () {
            alert('Error: No fue posible obtener el registro.');
        });
    };

    $.segalmex.maiz.inscripcion.correccion.cargaDatos = function (fn) {
        $.ajax({
            url: '/maiz/resources/paginas/bandejas/correccion/' + data.tipoInscripcion,
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            data.entradas = response;
            fn(response);
        }).fail(function () {
            alert('Error: No se pudo obtener la lista de registros de contrato.');
        });
        return data.entradas;
    };

    $.segalmex.maiz.inscripcion.correccion.limpiar = function () {
        data.inscripcion = null;
        data.requiereAnexos = false;
        $('#detalle-elemento').html('');
        $('#detalle-div').html('');
        $('#detalles-entrada,#botones-entrada').hide();
    };

    $.segalmex.maiz.inscripcion.correccion.mostrar = function (e) {
        e.preventDefault();
        var folio = e.target.id.substring('link-id-'.length);
        data.inscripcion = $.segalmex.get(data.entradas, folio, 'folio');
        var actual = $.segalmex.common.bandejas.actual();

        // Obtenemos la inscripcion
        var tipoInscripcion = data.tipoInscripcion;
        var datos = {
            datos: true,
            comentarios: true,
            expand: 3,
            contratos: true
        };
        if (tipoInscripcion === 'productores') {
            datos.archivosCargados = true;
        }
        $.ajax({
            url: '/maiz/resources/' + tipoInscripcion + '/maiz/inscripcion/' + data.inscripcion.uuid,
            data: datos,
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            $('#detalle-div').html('');
            $('#div-datos-capturados-correctos').hide();
            $('#btn-mostrar-correctos').html('Mostrar <span class="fas fa-check-circle text-secondary" aria-hidden="true">');
            // Construimos el detalle
            utils.construyeInscripcion(response, tipoInscripcion);
            $('#botones-entrada button.btn,#div-carta-acuerdo,.detalle-elemento-form').hide();
            $('#button-regresar,#nuevo-comentario-button').show();
            $('#carta-acuerdo-tmp-pdf').prop('disabled', true);
            $('#detalle-correccion-contratos-productor,#button-eliminar-productores').hide();
            $('#div-tabla-anexos').hide();
            switch (tipoInscripcion) {
                case "contratos":
                case "facturas":
                    data.precioTonelada = response.precioTonelada;
                    data.contratosProductores = response.contratosProductor;
                    utils.configuraBandeja(actual, response);
                    break;
                case "productores":
                    utils.cargaCatalogosProductor(actual, response);
                    data.prediosDocumentos = response.prediosDocumentos;
                    $('#predios-documento-table').on('click', 'button.mostrar-repetidos', handlers.muestraFoliosRepetidos);
                    break;
            }

            $('#detalle-elemento-edicion input.form-control-file').change(handlers.verificaTamanoArchivo);
        }).fail(function () {
            alert('Error: No fue posible obtener el registro.');
        });
    };

    utils.configuraBandeja = function (actual, inscripcion) {
        switch (actual) {
            case 'edicion':
                $('#button-guarda-edicion').show();
                $('#menu-edicion a.nav-link').removeClass('active');
                $('#muestra-contratos-productor').addClass('active');
                $('#muestra-contratos-productor').click();
                utils.resetPredios();
                break;
            case 'documentacion':
                if (inscripcion.empresas.length > 0) {
                    $('#carta-acuerdo-tmp-pdf').prop('disabled', false);
                    $('#div-carta-acuerdo').show();
                }
                $('#subir-button').show();
                $('#enviar-button').show();
                $('#div-tabla-anexos').show();
                break;
            case 'correccion':
                $('#button-corrige').show();
                if (data.tipoInscripcion !== 'facturas') {
                    $('#actualizar-button').show();
                }
                var correctos = utils.filtraDatosCapturados(inscripcion.datos, true);
                var inCorrectos = utils.filtraDatosCapturados(inscripcion.datos, false);
                $('#inscripcion-datos-capturados-incorrectos tbody').html(utils.creaTablaCaptura(inCorrectos));
                $('#inscripcion-datos-capturados-correctos tbody').html(utils.creaTablaCaptura(correctos));
                utils.configuraTablaCaptura(inscripcion.datos);
                break;
            case 'correccion-contrato-productor':
                utils.agregaDetalleContratoProductor();
                $('#detalle-div').html($.segalmex.maiz.inscripcion.vista.generaTablaDetalleContratosProductor(data.contratosProductores, true));
                $('#detalle-productores-table').on('click', 'button.eliminar-productor', handlers.eliminaProductor);
                utils.configuraTablaContratosProductor(true);
                $('#button-corrige-productores').show();
                $('#detalle-correccion-contratos-productor,#button-eliminar-productores').show();
                data.superficieContrato = inscripcion.superficie;
                data.volumenContrato = inscripcion.volumenTotal;
                data.numeroProductores = inscripcion.numeroProductores;
                break;
        }

        $('#lista-entradas').hide();
        $('#detalle-elemento-' + actual).show();
        $('#detalles-entrada,#botones-entrada').show();

    };

    utils.agregaDetalleContratoProductor = function () {
        var buffer = [];
        var totalSuperficie = 0.0;
        var totalVolumenContrato = 0.0;
        var totalVolumenIar = 0.0;
        var totalProductores = 0;
        for (var i = 0; i < data.contratosProductores.length; i++) {
            totalProductores++;
            totalSuperficie += parseFloat(data.contratosProductores[i].superficie);
            totalVolumenContrato += parseFloat(data.contratosProductores[i].volumenContrato);
            totalVolumenIar += parseFloat(data.contratosProductores[i].volumenIar);
        }
        buffer.push('<tr>');
        buffer.push('<td><a class="link-detalle-contrato-productor" href="#">');
        buffer.push(totalProductores);
        buffer.push('</a></td>');
        buffer.push('<td><a class="link-detalle-contrato-productor" href="#">');
        buffer.push(totalSuperficie.toFixed(3));
        buffer.push('</a></td>');
        buffer.push('<td><a class="link-detalle-contrato-productor" href="#">');
        buffer.push(totalVolumenContrato.toFixed(3));
        buffer.push('</a></td>');
        buffer.push('<td><a class="link-detalle-contrato-productor" href="#">');
        buffer.push(totalVolumenIar.toFixed(3));
        buffer.push('</a></td>');
        buffer.push('</tr>');
        $('#productor-detalle-table tbody').html(buffer.join(''));
    };

    utils.filtraDatosCapturados = function (datos, correcto) {
        var datosFiltrados = [];
        for (var i = 0; i < datos.length; i++) {
            var d = datos[i];
            if (d.correcto && correcto) {
                datosFiltrados.push(d);
            } else if (!d.correcto && !correcto) {
                datosFiltrados.push(d);
            }
        }
        return datosFiltrados;
    };

    $.segalmex.maiz.inscripcion.correccion.construyeTabla = function (lista) {
        return $.segalmex.common.bandejas.vista.construyeTabla({
            id: 'table-inscripciones',
            lista: lista,
            link: true,
            btnActualizar: true,
            tipoInscripcion: data.tipoInscripcion
        });
    };

    $.segalmex.maiz.inscripcion.correccion.configuraTabla = function () {
        $.segalmex.common.bandejas.vista.configuraTablaInscripcion('table-inscripciones', data.tipoInscripcion);
    };

    handlers.cambiaBandeja = function (e) {
        e.preventDefault();
        var id = e.target.id.substring('muestra-'.length);
        if (data.tipoInscripcion === id) {
            return;
        }
        data.tipoInscripcion = id;
        data.folio = null;

        $('#menu-bandejas a.nav-link').removeClass('active');
        $(e.target).addClass('active');
        $.segalmex.common.bandejas.init({
            configuracion: utils.configuracionBandejas(),
            namespace: $.segalmex.maiz.inscripcion.correccion
        });
    };

    handlers.cambiaSeccionEdicion = function (e) {
        e.preventDefault();
        var id = e.target.id.substring('muestra-'.length);
        $('#menu-edicion a.nav-link').removeClass('active');
        $(e.target).addClass('active');
        $('.tab-edicion').hide();
        $('#' + id).show();
    };

    handlers.verificaTamanoArchivo = function (e) {
        var extension = e.target.id.includes('xlsx') ? 'xlsx' : (e.target.id.includes('xml') ? 'xml' : 'pdf');
        $.segalmex.archivos.verificaArchivo(e, extension, data.limiteArchivo);
    };

    handlers.adjuntar = function (e) {
        e.preventDefault();
        $('#botones-entrada button').prop('disabled', true);

        var files = $('#detalle-elemento-documentacion input.form-control-file:enabled');
        var texto = [];
        var errores = false;
        var fds = [];
        for (var i = 0; i < files.length; i++) {
            if ($(files[i]).val() === '') {
                errores = true;
                texto.push(' * Seleccione el archivo PDF para: ' + $('label[for=' + files[i].id + ']').html() + '\n');
            } else {
                var id = files[i].id;
                var tipo = id.substring(0, id.length - '-tmp-pdf'.length);
                var fd = new FormData();
                fd.append('file', files[i].files[0]);
                fds.push({id: id, fd: fd, tipo: tipo});
            }
        }
        if (errores) {
            alert('Verifique lo siguiente:\n\n' + texto.join(''));
            $('#botones-entrada button').prop('disabled', false);
            return;
        }

        var fails = 0;
        for (i = 0; i < fds.length; i++) {
            (function (dato, ultimo) {
                $.ajaxq('archivosQueue', {
                    url: '/maiz/resources/productores/maiz/inscripcion/' + data.inscripcion.uuid + '/anexos/' + dato.tipo + '/pdf/',
                    type: 'POST',
                    data: dato.fd,
                    contentType: false,
                    processData: false
                }).done(function () {
                }).fail(function () {
                    fails++;
                }).always(function () {
                    // Si no es el último, continuamos
                    if (!ultimo) {
                        return;
                    }
                    // Si no hubo fallos, lo indicamos
                    if (fails === 0) {
                        alert('Los archivos del registro se han agregado correctamente.');
                        $('#botones-entrada button').prop('disabled', false);

                        $.segalmex.common.bandejas.actualizar();
                        $('#enviar-modal').modal('hide');
                        $('.detalle-elemento-form').hide();
                        $('#lista-entradas').show();
                    } else {
                        alert('Error: No fue posible registrar el archivo.');
                        $('#botones-entrada button').prop('disabled', false);
                    }
                });
            })(fds[i], i === fds.length - 1);
        }
    }

    handlers.corrige = function (e) {
        e.preventDefault();
        $('#botones-entrada button').prop('disabled', true);

        if (!utils.validaTablaCaptura()) {
            $('#botones-entrada button').prop('disabled', false);
            return;
        }

        var mensaje = [];
        $('#inscripcion-datos-capturados-incorrectos input.form-control-file').each(function () {
            if ($(this).val() === '') {
                mensaje.push(' * El archivo ' + $(this).attr('id') + ' es requerido.');
            }
            data.requiereAnexos = true;
        });

        if (mensaje.length > 0) {
            alert('Error:\n\n' + mensaje.join('\n'));
            $('#botones-entrada button').prop('disabled', false);
            return;
        }

        var uuid = data.inscripcion.uuid;
        if (data.requiereAnexos) {
            var files = $('#inscripcion-datos-capturados-incorrectos input.form-control-file');
            var fds = [];

            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                var id = file.id;
                var tipo = id.substring(0, id.length - '-pdf'.length);
                var url = '/maiz/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + uuid + '/anexos/' + tipo + '/pdf/';
                var fd = new FormData();
                var archivo = $('#' + id)[0].files[0];
                fd.append('file', archivo);
                var etiqueta = $('label[for=' + file.id + ']').html();
                fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
            }
            $('#div-carga-archivos').cargaArchivos({
                archivos: fds,
                callBackCerrar: function () {
                    var capturados = utils.getDatosCapturados();
                    utils.enviaCorreccion(data.inscripcion.uuid, capturados, false);
                },
                btnDescarga: false,
                indicaciones: 'Por favor espere mientras se suben los documentos anexos.'
            });
        } else {
            var capturados = utils.getDatosCapturados();
            utils.enviaCorreccion(data.inscripcion.uuid, capturados, false);
        }
    };

    handlers.actualiza = function (e) {
        e.preventDefault();
        $('#botones-entrada button').prop('disabled', true);

        if (!utils.validaTablaCaptura()) {
            $('#botones-entrada button').prop('disabled', false);
            return;
        }

        var capturados = utils.getDatosCapturados();
        utils.enviaCorreccion(data.inscripcion.uuid, capturados, true);
    }

    handlers.nuevoComentario = function (e) {
        $('#comentario-nuevo-modal').modal('show');
        $('#contenido-comentario').val('');
    }

    handlers.agregaComentario = function (e) {
        $('#contenido-comentario').limpiaErrores();
        var errores = [];
        $('#contenido-comentario').valida(errores, true);
        if (errores.length > 0) {
            return;
        }

        var comentario = {
            tipo: $.segalmex.common.bandejas.actual(),
            contenido: $('#contenido-comentario').val()
        }

        $.ajax({
            url: '/maiz/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + data.inscripcion.uuid + '/comentario/',
            data: JSON.stringify(comentario),
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            $('#comentario-nuevo-modal').modal('hide');
        }).fail(function () {
            alert('Error: No fue posible agregar el comentario.');
            $('#botones-entrada button').prop('disabled', false);
        });
    }

    handlers.muestraComentario = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var clave = id.substring('btn-comentario-'.length);
        $('#comentario-modal .modal-body p').html($('#comentario-' + clave).val());
        $('#comentario-modal').modal('show');
    }

    utils.enviaCorreccion = function (uuid, capturados, soloActualizar) {
        var url = '/maiz/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + uuid + '/validacion/';
        if (soloActualizar) {
            url += 'actualizacion/';
        } else {
            url += 'negativa/'
        }

        $.ajax({
            url: url,
            data: JSON.stringify(capturados),
            type: 'PUT',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('El registro ha sido ' + (soloActualizar ? 'actualizado.' : 'enviado a revalidación.'));
            $('#botones-entrada button').prop('disabled', false);

            if (!soloActualizar) {
                $.segalmex.common.bandejas.actualizar();
                $('.detalle-elemento-form').hide();
                $('#detalle-elemento-comentarios').html('');
                $('#lista-entradas').show();
            }
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Error: No fue posible ' + (soloActualizar ? 'actualizar el registro.' : 'corregir el registro.'));
            }
            $('#botones-entrada button').prop('disabled', false);
        });
    }

    handlers.regresar = function (e) {
        e.preventDefault();

        $.segalmex.maiz.inscripcion.correccion.limpiar();
        $('#lista-entradas').show();
    };

    utils.getDatosCapturados = function () {
        var datos = $('#inscripcion-datos-capturados-incorrectos .dato-valor');
        var datosCorrectos = $('#inscripcion-datos-capturados-correctos .dato-valor');
        for (var i = 0; i < datosCorrectos.length; i++) {
            datos.push(datosCorrectos[i]);
        }
        var dcs = [];
        for (var i = 0; i < datos.length; i++) {
            var dato = datos[i];
            var valor = $(dato).val();
            var clave = dato.id;
            var d = {clave: clave, valor: valor};
            if ($(dato).is('select')) {
                d.valorTexto = $(dato).find('option:selected').text();
            }
            dcs.push(d);
        }
        return dcs;
    }

    utils.configuracionBandejas = function () {
        switch (data.tipoInscripcion) {
            case 'contratos':
                return {
                    idBandeja: 'entradas-bandeja',
                    idLista: 'lista-entradas',
                    defaultBandeja: 'correccion',
                    entradas: [
                        {nombre: 'Corrección', titulo: 'Corrección de contratos', prefijoId: 'correccion', descripcion: 'Lista de registros de contratos para corrección.'},
                        {nombre: 'Corrección de productores', titulo: 'Corrección de productores', prefijoId: 'correccion-contrato-productor', descripcion: 'Lista de registros de contratos para corrección de listado productores.'}
                    ]
                };
            case 'productores':
                return {
                    idBandeja: 'entradas-bandeja',
                    idLista: 'lista-entradas',
                    defaultBandeja: 'documentacion',
                    entradas: [
                        {nombre: 'Edición', titulo: 'Edición de productores', prefijoId: 'edicion', descripcion: 'Lista de registros de productores para agregar contratos, predios y/o uso de facturas de persona moral.'},
                        {nombre: 'Documentación', titulo: 'Documentación de productores', prefijoId: 'documentacion', descripcion: 'Lista de registros de productores para agregar documentación.'},
                        {nombre: 'Corrección', titulo: 'Corrección de productores', prefijoId: 'correccion', descripcion: 'Lista de registros de productores para corrección.'}
                    ]
                };
            case 'facturas':
                return {
                    idBandeja: 'entradas-bandeja',
                    idLista: 'lista-entradas',
                    defaultBandeja: 'correccion',
                    entradas: [
                        {nombre: 'Corrección', titulo: 'Corrección de facturas', prefijoId: 'correccion', descripcion: 'Lista de registros de facturas para corrección.'}
                    ]
                };
        }
    };

    utils.creaTablaCaptura = function (datos) {
        var buffer = [];
        var grupo = 'general';
        for (var i = 0; i < datos.length; i++) {
            var dato = datos[i];
            if (dato.clave === 'numero-cuenta-productor') {
                continue;
            }
            if (dato.grupo === undefined) {
                dato.grupo = 'General';
            }

            if (dato.grupo !== grupo) {
                buffer.push('<tr class="table-secondary"><th colspan="4">' + dato.grupo + '</th></tr>');
                grupo = dato.grupo;
            }
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push('<label for="' + dato.clave + '">' + dato.nombre + '</label>');
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(utils.creaCampoCaptura(dato));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(dato.correcto
                    ? '<i class="fas fa-check-circle text-success"></i>'
                    : '<i class="fas fa-times-circle text-danger"></i>')
            buffer.push('</td>');
            buffer.push('<td><div class="form-inline">');
            buffer.push('<input id="comentario-' + dato.clave
                    + '" type="text" class="form-control" readonly="readonly" value="'
                    + (dato.comentario ? dato.comentario : '') + '"/>');
            if (dato.comentario) {
                buffer.push('<button type="button" id="btn-comentario-' + dato.clave + '" class="btn-comentario btn btn-outline-secondary ml-2"><i class="fas fa-comment-alt"></i></button>');
            }
            buffer.push('</div></td>');
            buffer.push('</tr>');
        }

        return buffer.join('\n');
    }

    utils.creaCampoCaptura = function (dato) {
        var buffer = [];

        switch (dato.tipo) {
            case 'texto':
                buffer.push('<input id="');
                buffer.push(dato.clave);
                buffer.push('" type="text" class="form-control dato-valor valid-field"/>');
                break;
            case 'catalogo':
                buffer.push('<select id="');
                buffer.push(dato.clave);
                buffer.push('" class="form-control valid-field dato-valor">');
                buffer.push('<option value="0">Seleccione</option>');
                buffer.push('</select>');
                break;
            case 'archivo':
                var accept = dato.clave.includes('xlsx')
                        ? 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
                        : (dato.clave.includes('xml') ? 'text/xml' : 'application/pdf');
                buffer.push('<input id="');
                buffer.push(dato.clave);
                buffer.push('" ');
                if (dato.correcto) {
                    buffer.push('type="text" class="form-control dato-valor" value="');
                    buffer.push(dato.valor);
                    buffer.push('" disabled="disabled" />');
                } else {
                    buffer.push('type="file" class="form-control-file" accept="' + accept + '" aria-describedby="' + dato.clave + '-ayuda"/>');
                    buffer.push('<small id="' + dato.clave + '-ayuda" class="form-text text-muted">El archivo no deberá ser mayor a 24 MB (25164800 bytes).</small>')
                }
                break;
        }

        return buffer.join('');
    }

    utils.configuraTablaCaptura = function (datos) {
        // SE AGREGA LA FECHA MINIMA PARA LA FECHA DE VENCIMIENTO DE LAS COBERTURAS
        var fechaMinima = new Date($.segalmex.fecha);
        fechaMinima.setFullYear(2021);
        fechaMinima.setMonth(2);
        fechaMinima.setDate(01);
        // Iteramos los datos capturados y ponemos los seleccionados en los combos
        for (var i = 0; i < datos.length; i++) {
            var dato = datos[i];
            var valores = [];
            var tmp = dato.clave.split('_');
            var clave = tmp.length === 1 ? tmp[0] : tmp[1];
            var params = null;
            var catalogos = utils.getCatalogos();
            var numeroClabe;
            switch (clave) {
                case 'tipo':
                case 'tipo-cultivo-predio':
                case 'tipo-cultivo':
                    valores = catalogos.tipos;
                    break;
                case 'entidad-produce':
                case 'estado-predio':
                    valores = catalogos.estadosPredio;
                    break;
                case 'entidad-destino':
                case 'estado-domicilio':
                    valores = catalogos.estados;
                    break;
                case 'estado-factura':
                    valores = catalogos.estadosCultivo;
                    break;
                case 'municipio-predio':
                    valores = utils.filtraMunicipios(catalogos.municipios, dato.valor);
                    break;
                case 'tipo-empresa-comprador':
                    valores = catalogos.tiposEmpresaComprador;
                    break;
                case 'tipo-empresa-vendedor':
                    valores = catalogos.tiposEmpresaVendedor;
                    break;
                case 'tipo-precio':
                    valores = catalogos.tiposPrecio;
                    break;
                case 'tipo-identificacion-productor':
                case 'tipo-identificacion-representante':
                    valores = catalogos.tiposDocumentoIdentificacion;
                    break;
                case 'tipo-posesion-predio':
                    valores = catalogos.tiposPosesion;
                    break;
                case 'documento-posesion-predio':
                    valores = catalogos.tiposDocumentoPropia.concat(catalogos.tiposDocumentoRentada);
                    break;
                case 'regimen-hidrico-predio':
                    valores = catalogos.regimenes;
                    break;
                case 'banco-productor':
                    valores = catalogos.bancos;
                    break;
                case 'parentesco-beneficiario':
                    valores = catalogos.parentescos;
                    break;
                case 'entidad-cobertura':
                    valores = catalogos.coberturas;
                    break;
                case 'tipo-comprador':
                    valores = catalogos.tiposComprador;
                    break;
                case 'tipo-operacion':
                    valores = catalogos.tiposOperacion;
                    break;
                case 'numero-contrato':
                    params = {
                        pattern: /^[A-Z]{3}-[A-Z]{2}[0-9]{2}-[A-Z0-9]{3}-\d{6}-(A|B|T)-\d{3}$/,
                        minlength: 25,
                        maxlength: 25
                    };
                    break;
                case 'fecha-firma':
                case 'fecha-compra':
                    params = {
                        type: 'date'
                    };
                    break;
                case 'fecha-vencimiento':
                    params = {
                        type: 'date',
                        min: fechaMinima
                    };
                    break;
                case 'numero-productores':
                    params = {
                        type: 'number',
                        min: 1,
                        max: 2000,
                        maxlength: 4
                    };
                    break;
                case 'nombre-comprador':
                case 'nombre-vendedor':
                case 'nombre-representante':
                case 'papellido-representante':
                case 'sapellido-representante':
                case 'calle-domicilio':
                case 'numero-exterior-domicilio':
                case 'numero-interior-domicilio':
                case 'localidad-domicilio':
                case 'folio-predio':
                case 'localidad-predio':
                case 'nombre-sociedad':
                    params = {
                        type: 'name-moral'
                    };
                    break;
                case 'nombre-productor':
                case 'papellido-productor':
                case 'sapellido-productor':
                case 'nombre-beneficiario':
                case 'apellidos-beneficiario':
                    params = {
                        type: 'nombre-icao',
                        textTransform: 'upper'
                    };
                    break;
                case 'rfc-comprador':
                case 'rfc-vendedor':
                    params = {
                        type: 'rfc',
                        minlength: 12,
                        maxlength: 13
                    };
                    break;
                case 'rfc-productor':
                    params = {
                        type: 'rfc-fisica'
                    };
                    break;
                case 'rfc-sociedad':
                case 'factura-rfc-sociedad':
                    params = {
                        type: 'rfc-moral'
                    };
                    break;
                case 'cantidad-contratada':
                case 'cantidad-contratada-cobertura':
                case 'superficie-total':
                case 'volumen-total':
                case 'superficie-predio':
                case 'volumen-obtenido':
                case 'rendimiento':
                case 'precio-futuro':
                case 'base-acordada':
                case 'precio-fijo':
                case 'numero':
                case 'numero-contratos':
                case 'precio-ejercicio-dolares':
                case 'precio-ejercicio-pesos':
                case 'prima-dolares':
                case 'prima-pesos':
                case 'comisiones-dolares':
                case 'comisiones-pesos':
                    params = {
                        type: "number",
                        min: 0
                    };
                    break;
                case 'precio-tonelada-real':
                    params = {
                        type: "number",
                        min: data.precioTonelada
                    };
                    break;
                case 'curp-productor':
                case 'curp-representante':
                case 'curp-beneficiario':
                    params = {
                        type: 'curp'
                    };
                    break;
                case 'numero-telefono':
                    params = {
                        pattern: /^\d{10}$/,
                        minlength: 10,
                        maxlength: 10
                    }
                    break;
                case 'correo-electronico':
                    params = {
                        type: 'email'
                    }
                    break;
                case 'codigo-postal-domicilio':
                    params = {
                        pattern: /^\d{5}$/,
                        minlength: 5,
                        maxlength: 5
                    }
                    break;
                case 'latitud-predio':
                case 'latitud-1-predio':
                case 'latitud-2-predio':
                case 'latitud-3-predio':
                case 'latitud-4-predio':
                    params = {
                        type: 'point',
                        min: 14.5,
                        max: 32.72
                    };
                    break;
                case 'longitud-predio':
                case 'longitud-1-predio':
                case 'longitud-2-predio':
                case 'longitud-3-predio':
                case 'longitud-4-predio':
                    params = {
                        type: 'point',
                        min: -117.2,
                        max: -86.7
                    }
                    break;
                case 'clabe-productor':
                    params = {
                        pattern: /^\d{18}$/,
                        minlength: 18,
                        maxlength: 18
                    };
                    break;
            }
            switch (dato.tipo) {
                case 'catalogo':
                    $('#' + dato.clave).actualizaCombo(valores, {value: 'clave'}).prop('disabled', dato.correcto).val(dato.valor).configura(params);
                    if (dato.clave.includes('estado-')) {
                        $('#' + dato.clave).change(handlers.cambiaEstadoCorreccion);
                    }
                    break;
                case 'texto':
                    if (dato.clave === 'clabe-productor') {
                        numeroClabe = dato.valor.substring(0, 3);
                    }
                    $('#' + dato.clave).prop('disabled', dato.correcto).val(dato.valor).configura(params);
                    break;
                case 'archivo':
                    if (dato.correcto) {
                        $('#' + dato.clave).prop('disabled', dato.correcto).val(dato.valor);
                    }
                    break;
            }
        }
        $('#banco-productor').val(numeroClabe);
        $('#inscripcion-datos-capturados-incorrectos tbody .valid-field').validacion();
        $('#inscripcion-datos-capturados-incorrectos tbody input.form-control-file').change(handlers.verificaTamanoArchivo);
    };

    handlers.cambiaEstadoCorreccion = function (e) {
        var v = $(e.target).val();
        var estado = v !== '0' ? $.segalmex.get(data.estadosPredio, v) : {clave: '0'};
        var municipios = [];
        if (estado.clave !== '0') {
            for (var i = 0; i < data.municipiosEdicion.length; i++) {
                var m = data.municipiosEdicion[i];
                if (m.estado.id === estado.id) {
                    municipios.push(m);
                }
            }
        }
        var id = e.target.id.split('_')[0];
        $('#' + id + '_municipio-predio').actualizaCombo(municipios, {value: 'clave'}).val('0');
    };

    utils.getCatalogos = function () {
        switch (data.tipoInscripcion) {
            case 'contratos':
                return data.catalogos;
            case 'productores':
                return data.catalogosProductor;
            case 'facturas':
                return data.catalogosFactura;
        }
        return null;
    };

    utils.filtraMunicipios = function (municipios, clave) {
        var estado = clave.length === 5 ? clave.substring(0, 2) : clave.substring(0, 1);
        var filtrados = [];
        for (var i = 0; i < municipios.length; i++) {
            var m = municipios[i];
            if (m.estado.clave === estado) {
                filtrados.push(m);
            }
        }
        return filtrados;
    }

    utils.validaTablaCaptura = function () {
        $('#inscripcion-datos-capturados-incorrectos tbody .valid-field').limpiaErrores();

        var errores = [];
        $('#inscripcion-datos-capturados-incorrectos tbody .valid-field').valida(errores, true);

        return errores.length === 0;
    };

    utils.construyeInscripcion = function (inscripcion, tipoInscripcion) {
        switch (tipoInscripcion) {
            case 'contratos':
                var corrreccion = $.segalmex.common.bandejas.actual() === 'correccion' ? true : false;
                $('#detalle-elemento').html($.segalmex.maiz.inscripcion.vista.construyeInscripcion(inscripcion, corrreccion));
                if (corrreccion) {
                    $('#detalle-div').html('');
                    $('#detalle-div').html($.segalmex.maiz.inscripcion.vista.generaTablaDetalleContratosProductor(inscripcion.contratosProductor, false));
                    utils.configuraTablaContratosProductor(false);
                    $('#button-ver-productores').click(handlers.muestraDetalleContratoProductor);
                }
                break;
            case 'productores':
                utils.limpiaEdicion();
                $.segalmex.maiz.inscripcion.productor.vista.muestraInscripcion(inscripcion, 'detalle-elemento');
                utils.muestraPermitidos(inscripcion);
                data.claveArchivos = inscripcion.claveArchivos;
                data.uuid = inscripcion.uuid;
                data.archivosCargados = inscripcion.archivosCargados;
                $('#div-tabla-anexos').html('');
                if ($.segalmex.common.bandejas.actual() === 'documentacion') {
                    $('#div-tabla-anexos').html(inscripcion.archivosCargados ? utils.construyeTablaAnexos(inscripcion.archivosCargados, inscripcion.archivosOpcionales) : '');
                    $('#div-tabla-anexos input.form-control-file').change(handlers.verificaTamanoArchivo);
                }
                if (inscripcion.numeroContratos > 0 && $.segalmex.common.bandejas.actual() === 'edicion') {
                    utils.getContratosProductor(inscripcion);
                }
                break;
            case 'facturas':
                $('#detalle-elemento').html($.segalmex.maiz.inscripcion.factura.vista.construyeInscripcion(inscripcion));
                break;
            default:
                alert('Registro desconocido');
        }
        $('#detalle-elemento-comentarios').html($.segalmex.maiz.inscripcion.vista.construyeComentarios(inscripcion.comentarios, $.segalmex.common.pagina.comun.registrado));
    };

    //--------------------------AGREGAR CONTRATOS-------------------------------
    utils.inicializaValidaciones = function () {
        $(".valid-field").configura();

        $('#edicion-cantidad-contratada-contrato,#edicion-rendimiento-predio,'
                + '#edicion-cantidad-contratada-cobertura').configura({
            type: 'number',
            min: 0
        });

        $('#edicion-volumen-predio').configura({
            type: 'number',
            min: 0.1
        });

        $('#edicion-superficie-predio').configura({
            type: 'number',
            min: 0.1,
            max: 50
        });

        $('#edicion-folio-predio,#edicion-localidad-predio,#nombre-sociedad,#factura-nombre-sociedad').configura({
            type: 'name-moral'
        });
        $('#factura-rfc-sociedad,#confirmacion-factura-rfc-sociedad').configura({
            type: 'rfc-moral'
        });

        $('#predio-georreferencia input.valid-field').configura({
            type: 'point'
        });

        $('#latitud-predio,#latitud-1-predio,#latitud-2-predio,#latitud-3-predio,#latitud-4-predio').configura({
            min: 14.5,
            max: 32.72
        });
        $('#longitud-predio,#longitud-1-predio,#longitud-2-predio,#longitud-3-predio,#longitud-4-predio').configura({
            min: -117.2,
            max: -86.7
        });
        //Detalle contratos productor.
        $('#nombre-productor,#papellido-productor,#sapellido-productor').configura({
            type: 'nombre-icao'
        }).configura({textTransform: 'upper'});
        ;
        $('#sapellido-productor').configura({
            required: false
        });
        $('#superficie-contrato,#volumen-contrato,#volumen-contrato-iar').configura({
            type: 'number',
            min: 0.1
        });
        $('#curp-productor').configura({
            type: 'curp'
        });
        $('#rfc-productor').configura({
            type: 'rfc-fisica'
        });
        $('.valid-field').validacion();
    };

    handlers.agregaContrato = function (e) {
        var numero = $('#edicion-numero-contrato').val();
        $('#edicion-numero-contrato').typeahead('val', numero.toUpperCase());

        $('#edicion-numero-contrato,#edicion-cantidad-contratada-contrato,#edicion-cantidad-contratada-cobertura').limpiaErrores();
        var errores = [];
        $('#edicion-numero-contrato,#edicion-cantidad-contratada-contrato,#edicion-cantidad-contratada-cobertura').valida(errores, true);
        if (errores.length > 0) {
            return;
        }

        var duplicado = utils.contratoDuplicado(numero);
        if (duplicado) {
            alert('Error: El contrato ya ha sido agregado.');
            return;
        }

        var contrato = utils.contratoExistente(numero);
        if (!contrato) {
            alert('Error: El contrato no está registrado.');
            return;
        }

        if (data.edicionContratos.length >= data.contratosPermitidos) {
            alert('Error: No se pueden agregar más contratos.');
            return;
        }

        if (parseFloat($("#edicion-cantidad-contratada-cobertura").val()) > parseFloat($("#edicion-cantidad-contratada-contrato").val())) {
            alert('Error: La cantidad contratada con cobertura no puede ser mayor que la cantidad contratada.');
            return;
        }

        if (utils.isInvalidaCantidadContratada()) {
            alert('Error: La cantidad contratada total es mayor a la considerada por el Programa.');
            return;
        }

        var contrato = {
            numeroContrato: contrato.numeroContrato,
            folio: contrato.folio,
            empresa: contrato.nombreEmpresa,
            cantidadContratada: $('#edicion-cantidad-contratada-contrato').val(),
            cantidadContratadaCobertura: $('#edicion-cantidad-contratada-cobertura').val()
        };
        data.edicionContratos.push(contrato);
        utils.construyeContratos();
        utils.limpiaContrato();
    };

    utils.contratoDuplicado = function (numero) {
        for (var i = 0; i < data.edicionContratos.length; i++) {
            if (data.edicionContratos[i].numeroContrato === numero) {
                return true;
            }
        }

        for (var i = 0; i < data.contratosInscripcion.length; i++) {
            if (data.contratosInscripcion[i].numeroContrato === numero) {
                return true;
            }
        }
        return false;
    };

    utils.contratoExistente = function (numero) {
        for (var i = 0; i < data.contratosRegistrados.length; i++) {
            if (data.contratosRegistrados[i].numeroContrato === numero) {
                return data.contratosRegistrados[i];
            }
        }
        return null;
    };

    utils.construyeContratos = function () {
        var buffer = [];
        if (data.edicionContratos.length === 0) {
            buffer.push('<tr><td class="text-center" colspan="6">Sin contratos</td></tr>');
            return $('#edicion-contratos-table tbody').html(buffer.join(''));
        }
        for (var i = 0; i < data.edicionContratos.length; i++) {
            var contrato = data.edicionContratos[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-right">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(contrato.numeroContrato);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(contrato.folio);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(contrato.empresa);
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(contrato.cantidadContratada);
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(contrato.cantidadContratadaCobertura);
            buffer.push('</td>');
//            buffer.push('<td class="text-center"><button id="eliminar-contrato-');
//            buffer.push(i);
//            buffer.push('" class="btn btn-danger btn-sm eliminar-contrato"><i class="fa fa-minus-circle"></i></button></td>');
//            buffer.push('</td>');
            buffer.push('</tr>');
        }
        $('#edicion-contratos-table tbody').html(buffer.join(''));
    };

    utils.limpiaContrato = function () {
        $('#edicion-numero-contrato').typeahead('val', '');
        $('#edicion-numero-contrato,#edicion-cantidad-contratada-contrato,#edicion-cantidad-contratada-cobertura').limpiaErrores();
        $('#edicion-cantidad-contratada-contrato,#edicion-cantidad-contratada-cobertura').val('');
    };

    handlers.eliminaContrato = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-contrato-'.length), 10);
        var contratos = [];
        for (var i = 0; i < data.edicionContratos.length; i++) {
            if (i !== idx) {
                contratos.push(data.edicionContratos[i]);
            }
        }
        data.edicionContratos = contratos;
        utils.construyeContratos();
    };

    utils.cargaContratos = function (contratos) {
        var inputNumeroContrato = `
        <label for="edicion-numero-contrato" class="control-label">Número de contrato</label>
        <input id="edicion-numero-contrato" type="text" class="form-control valid-field" maxlength="255" placeholder="XXX-XXXX-XXX-XXXXXX-X-XXX"/>
        `;
        $('#input-numero-contrato').html(inputNumeroContrato);
        data.contratosRegistrados = contratos;
        $('#edicion-numero-contrato').typeahead({
            hint: true,
            highlight: true,
            minLength: 3
        }, {
            name: 'contratos',
            source: utils.contratosMatcher(data.contratosRegistrados)
        });
        //se configura
        $('#edicion-numero-contrato').configura({
            pattern: /^[A-Z]{3}-[A-Z]{2}[0-9]{2}-[A-Z0-9]{3}-\d{6}-(A|B|T)-\d{3}$/,
            minlength: 25,
            maxlength: 25
        }).configura({textTransform: 'upper'});
    };

    utils.contratosMatcher = function (contratos) {
        return function (q, cb) {
            var matches = [];
            // regex used to determine if a string contains the substring `q`
            var substrRegex = new RegExp(q, 'i');
            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(contratos, function (i, contrato) {
                if (substrRegex.test(contrato.numeroContrato)) {
                    matches.push(contrato.numeroContrato);
                }
            });
            cb(matches);
        };
    };

    //----------------------------AGREGAR PREDIOS-------------------------------
    handlers.cambiaTipoPosesion = function (e) {
        var v = $(e.target).val();
        var tipo = v !== '0' ? $.segalmex.get(data.tiposPosesion, v) : {clave: '0'};
        var tipos = [];
        switch (tipo.clave) {
            case 'propia':
                $('#contrato-arrendamiento-pdf').val('').prop('disabled', false);
                tipos = data.tiposDocumentoPropia;
                break;
            case 'rentada':
                $('#contrato-arrendamiento-pdf').val('').prop('disabled', true);
                tipos = data.tiposDocumentoRentada;
                break;
        }

        if (!data.archivosPredio) {
            $('#contrato-arrendamiento-pdf').val('').prop('disabled', true);
        }
        $('#edicion-documento-posesion-predio').actualizaCombo(tipos).val('0');
    };

    handlers.cambiaEstadoPredio = function (e) {
        utils.cambiaEstadoEdicion(e.target);
    };

    utils.cambiaEstadoEdicion = function (target) {
        var v = $(target).val();
        var estado = v !== '0' ? $.segalmex.get(data.estadosPredio, v) : {clave: '0'};
        var municipios = [];
        if (estado.clave !== '0') {
            for (var i = 0; i < data.municipiosEdicion.length; i++) {
                var m = data.municipiosEdicion[i];
                if (m.estado.id === estado.id) {
                    municipios.push(m);
                }
            }
        }
        $('#edicion-municipio-predio').actualizaCombo(municipios).val('0');
    };

    handlers.cambiaRendimiento = function () {
        var volumen = parseFloat($('#edicion-volumen-predio').val());
        var superficie = parseFloat($('#edicion-superficie-predio').val());
        var rendimiento = volumen / superficie;
        var rendimientoFxd = rendimiento.toFixed(3);
        $('#edicion-rendimiento-predio').val(!isNaN(rendimientoFxd) && rendimientoFxd !== Infinity ? rendimientoFxd : '');
    };

    utils.getRendimiento = function (estado) {
        return estado ? data.rendimientos['edicion-estado' + estado.clave] : 0;
    };

    utils.setRendimiento = function (rendimientos) {
        if (!rendimientos) {
            return;
        }
        for (var i = 0; i < rendimientos.length; i++) {
            var a = 'edicion-estado' + rendimientos[i].clave.substring(35, 37);
            data.rendimientos[a] = parseFloat(rendimientos[i].valor);
        }
    };

    handlers.cambiaTipoGeorreferencia = function (e) {
        e.preventDefault();
        $('#punto-medio-predio input.valid-field,#poligono-predio input.valid-field').limpiaErrores().val('');
        $('#menu-tabs-predios a.nav-link').removeClass('active');
        $(e.target).addClass('active');
        $('.georreferencia').hide();
        var id = e.target.id.substring('muestra-'.length);
        $('#' + id).show();
    };

    handlers.agregaPredio = function (e) {
        $('#predios-productor-valid .valid-field').limpiaErrores();
        var errores = [];
        $('#predios-productor-valid .valid-field').valida(errores, true);
        if (errores.length > 0) {
            return;
        }
        data.estadoActual = $.segalmex.get(data.estadosPredio, $("#edicion-estado-predio").val());
        if ($.segalmex.archivos.validaArchivos('#div-documentacion-predio-pdf input.form-control-file:enabled')) {
            return;
        }

        if (data.archivosTmpSiembraXml.length < utils.getTotalArchivosPredio(data.estadoActual, data.archivosRqueridosSiembra)) {
            alert('Error: El total de archivos de Permiso de siembra (XML) es menor al considerado por el programa.');
            return;
        }

        if (data.archivosTmpRiegoXml.length < utils.getTotalArchivosPredio(data.estadoActual, data.archivosRequeridosRiego)) {
            alert('Error: El total de archivos de Pago de riego (XML) es menor al considerado por el programa.');
            return;
        }

        if (data.edicionPredios.length >= data.prediosPermitidos) {
            alert('Error: No se pueden agregar más predios.');
            return;
        }

        if (utils.isInvalidaVolumen()) {
            alert('Error: El volumen total es mayor al considerado por el Programa.');
            return;
        }

        if (utils.isInvalidaSuperficie()) {
            alert('Error: La superficie total es mayor al considerado por el Programa.');
            return;
        }

        //validamos si el ciclo permite rendimientos solicitados para los predios.
        if (!data.permitePredioSolicitado) {
            var estado = $.segalmex.get(data.estadosPredio, $("#edicion-estado-predio").val());
            var rendimiento = utils.getRendimiento(estado);
            if (parseFloat(parseFloat($('#edicion-rendimiento-predio').val())) > rendimiento) {
                alert('Error: Rendimiento mayor al considerado por el Programa de acuerdo a la región.');
                return;
            }
        }

        if (data.solicitado && $('#documentacion-rendimiento-solicitado-pdf').val() === '') {
            alert('Documentación permiso de rendimiento es requerido.');
            data.rendimientoSolValido = false;
            return;
        }

        if (data.permitePredioSolicitado) {
            data.rendimientoSolValido = true;
            data.estadoActual = $.segalmex.get(data.estadosPredio, $("#edicion-estado-predio").val());
            data.rendimientoMaximo = utils.getRendimiento(data.estadoActual);
            if (parseFloat($('#edicion-rendimiento-predio').val()) > data.rendimientoMaximo && !data.solicitado) {
                $('#documentacion-rendimiento-solicitado-pdf').val('');
                $('#rendimientoSol').html($("#edicion-rendimiento-predio").val());
                $('#rendimientoMax').html(parseFloat(data.rendimientoMaximo).toFixed(2));
                $('#edoNombre').html(data.estadoActual.nombre);
                $('#rendimineto-solicitado-modal').modal('show');
                return;
            }
        }

        var idxPredio = data.prediosPrevios + data.edicionPredios.length + 1;

        var anexoPredio = utils.getAnexoConstruido(idxPredio, 'documentacion-predio-pdf');
        if (data.archivosPredio) {
            //Armar los archivos dada la lista de siembra y riego
            if (data.archivosTmpSiembraXml.length > 0) {
                utils.construyeAnexosXml(data.edicionPredios.length + 1, 'permiso-siembra-xml-tmp', data.archivosTmpSiembraXml, data.estadoActual.id);
            }
            if (data.archivosTmpRiegoXml.length > 0) {
                utils.construyeAnexosXml(data.edicionPredios.length + 1, 'pago-riego-xml-tmp', data.archivosTmpRiegoXml, data.estadoActual.id);
            }
        }

        if (data.solicitado) {
            var anexoSolicitado = $('#documentacion-rendimiento-solicitado-pdf').clone();
            anexoSolicitado.attr('id', idxPredio + '_documentacion-rendimiento-solicitado-pdf');
            anexoSolicitado.idx = data.edicionPredios.length;
            data.volumenMaximo = (data.rendimientoMaximo * parseFloat($('#edicion-superficie-predio').val())).toFixed(3);
        }

        var tc = $.segalmex.get(data.tiposCultivo, $('#edicion-tipo-cultivo-predio').val());
        var predio = {
            folio: $('#edicion-folio-predio').val(),
            tipoCultivo: tc,
            tipoPosesion: {id: $('#edicion-tipo-posesion-predio').val(), nombre: $("#edicion-tipo-posesion-predio option:selected").text()},
            tipoDocumentoPosesion: {id: $('#edicion-documento-posesion-predio').val(), nombre: $("#edicion-documento-posesion-predio option:selected").text()},
            regimenHidrico: {id: $('#edicion-regimen-hidrico-predio').val(), nombre: $("#edicion-regimen-hidrico-predio option:selected").text()},
            superficie: $("#edicion-superficie-predio").val(),
            rendimiento: data.solicitado ? data.rendimientoMaximo : $("#edicion-rendimiento-predio").val(),
            volumen: data.solicitado ? data.volumenMaximo : $("#edicion-volumen-predio").val(),
            volumenSolicitado: $("#edicion-volumen-predio").val(),
            rendimientoSolicitado: $("#edicion-rendimiento-predio").val(),
            solicitado: data.solicitado ? 'solicitado' : 'no-solicitado',
            estado: {id: $("#edicion-estado-predio").val(), nombre: $("#edicion-estado-predio option:selected").text()},
            municipio: {id: $('#edicion-municipio-predio').val(), nombre: $("#edicion-municipio-predio option:selected").text()},
            localidad: $('#edicion-localidad-predio').val(),
            latitud: $('#latitud-predio').val(),
            longitud: $('#longitud-predio').val(),
            latitud1: $('#latitud-1-predio').val(),
            longitud1: $('#longitud-1-predio').val(),
            latitud2: $('#latitud-2-predio').val(),
            longitud2: $('#longitud-2-predio').val(),
            latitud3: $('#latitud-3-predio').val(),
            longitud3: $('#longitud-3-predio').val(),
            latitud4: $('#latitud-4-predio').val(),
            longitud4: $('#longitud-4-predio').val(),
            cantidadSiembra: data.archivosTmpSiembraXml.length === 0 ? 1 : data.archivosTmpSiembraXml.length,
            cantidadRiego: data.archivosTmpRiegoXml.length === 0 ? 1 : data.archivosTmpRiegoXml.length
        };
        data.edicionPredios.push(predio);
        data.edicionAnexosPredio.push(anexoPredio);
        if (data.archivosPredio) {
            //Se agregan a la lista final de siembra y riego
            if (data.archivosTmpSiembraXml.length > 0) {
                for (var siembra in data.archivosTmpSiembraXml) {
                    data.anexosSiembraXlm.push(data.archivosTmpSiembraXml[siembra]);
                }
            }
            if (data.archivosTmpRiegoXml.length > 0) {
                for (var riego in data.archivosTmpRiegoXml) {
                    data.anexosRiegoXlm.push(data.archivosTmpRiegoXml[riego]);
                }
            }
        }

        if (data.solicitado) {
            data.anexosPrediosolicitado.push(anexoSolicitado);
        }
        utils.limpiaPredio();
        utils.construyePredios();
    };

    utils.limpiaPredio = function () {
        var disabled = $('#edicion-tipo-cultivo-predio').prop('disabled');
        var valor = $('#edicion-tipo-cultivo-predio').val();
        $('#predios-productor-valid input.valid-field').limpiaErrores().val('');
        $('#predios-productor-valid select.valid-field').limpiaErrores().val('0');
        $('#edicion-tipo-cultivo-predio').prop('disabled', disabled);
        if (disabled) {
            $('#edicion-tipo-cultivo-predio').val(valor);
        }
        $('#div-archivos-predio input.form-control-file').val('');
        data.solicitado = false;
        data.volumenMaximo = 0;
        data.rendimientoMaximo = 0;
        data.estadoActual = null;
        utils.limpiaXmls();
    };

    utils.construyePredios = function () {
        var buffer = [];
        if (data.edicionPredios.length === 0) {
            buffer.push('<tr><td class="text-center" colspan="12">Sin predios</td></tr>');
            return $('#edicion-predios-table tbody').html(buffer.join(''));
        }
        for (var i = 0; i < data.edicionPredios.length; i++) {
            var predio = data.edicionPredios[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-right">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.folio);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.tipoCultivo.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.tipoPosesion.nombre);
            buffer.push(', ');
            buffer.push(predio.tipoDocumentoPosesion.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.regimenHidrico.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.municipio.nombre);
            buffer.push(', ');
            buffer.push(predio.estado.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.localidad);
            buffer.push('</td>');
            buffer.push('<td>');
            if (predio.latitud !== '') {
                buffer.push('<a class="muestra-ubicacion" href="#">[' + predio.latitud + ',' + predio.longitud + ']</a>');
            } else {
                buffer.push('<a class="muestra-ubicacion" href="#">[' + predio.latitud1 + ',' + predio.longitud1 + ']</a>,');
                buffer.push('<a class="muestra-ubicacion" href="#">[' + predio.latitud2 + ',' + predio.longitud2 + ']</a>,');
                buffer.push('<a class="muestra-ubicacion" href="#">[' + predio.latitud3 + ',' + predio.longitud3 + ']</a>,');
                buffer.push('<a class="muestra-ubicacion" href="#">[' + predio.latitud4 + ',' + predio.longitud4 + ']</a>');
            }
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.solicitado === 'solicitado' ? (predio.volumen + utils.setRojo(predio.volumenSolicitado)) : predio.volumen);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.superficie);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.solicitado === 'solicitado' ? (predio.rendimiento + utils.setRojo(predio.rendimientoSolicitado)) : predio.rendimiento);
            buffer.push('</td>');
            buffer.push('<td class="text-center"><button id="eliminar-predio-');
            buffer.push(i);
            buffer.push('" class="btn btn-danger btn-sm eliminar-predio"><i class="fa fa-minus-circle"></i></button></td>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        $('#edicion-predios-table tbody').html(buffer.join(''));
    };

    handlers.eliminaPredio = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-predio-'.length), 10);
        var predios = [];
        var anexosPredio = [];
        var anexosSolicitados = [];
        var anexosSiembraXlm = [];
        var anexosRiegoXlm = [];
        var siembras = [];
        var riegos = [];
        for (var i = 0; i < data.edicionPredios.length; i++) {
            var pa = data.edicionPredios[i];
            if (i !== idx) {
                var anexoPredio = data.edicionAnexosPredio[i];
                anexoPredio.attr('id', (data.prediosPrevios + predios.length + 1) + '_documentacion-predio-pdf');
                anexosPredio.push(anexoPredio);
                if (data.archivosPredio) {
                    // Volver a armar dada la lista de archivos siembra y riego
                    siembras = utils.getAnexosIndice(i, data.anexosSiembraXlm);
                    riegos = utils.getAnexosIndice(i, data.anexosRiegoXlm);
                    if (siembras.length > 0) {
                        utils.construyeAnexosXml(predios.length + 1, 'permiso-siembra-xml-tmp', siembras, pa.estado.id);
                        for (var siembra in siembras) {
                            anexosSiembraXlm.push(siembras[siembra]);
                        }
                    }
                    if (riegos.length > 0) {
                        utils.construyeAnexosXml(predios.length + 1, 'pago-riego-xml-tmp', riegos, pa.estado.id);
                        for (var riego in riegos) {
                            anexosRiegoXlm.push(riegos[riego]);
                        }
                    }

                }
                if (pa.solicitado === 'solicitado') {
                    var anexoSolicitado = utils.getAnexoIndice(i, data.anexosPrediosolicitado);
                    anexoSolicitado.attr('id', (data.prediosPrevios + predios.length + 1) + '_documentacion-rendimiento-solicitado-pdf');
                    anexoSolicitado.idx = predios.length;
                    anexosSolicitados.push(anexoSolicitado);
                }
                predios.push(data.edicionPredios[i]);
            }
        }
        data.edicionPredios = predios;
        data.edicionAnexosPredio = anexosPredio;
        data.anexosPrediosolicitado = anexosSolicitados;
        data.anexosSiembraXlm = anexosSiembraXlm;
        data.anexosRiegoXlm = anexosRiegoXlm;
        utils.construyePredios();
        utils.resetPredios();
    };

    handlers.muestraUbicacion = function (e) {
        e.preventDefault();
        var ubicacion = $(e.target).html();
        $('#maps').muestraMaps({
            ubicacion: ubicacion
        });
    };

    //--------------------------AGREGAR FACTURAS--------------------------------

    handlers.agregaSociedad = function (e) {
        $('#uso-factura-persona-moral .valid-field').limpiaErrores();
        var errores = [];
        $('#uso-factura-persona-moral .valid-field').valida(errores, true);
        if (errores.length > 0) {
            return;
        }
        var rfc = $('#factura-rfc-sociedad').val();
        var confirmacion = $('#confirmacion-factura-rfc-sociedad').val();
        if (rfc !== confirmacion) {
            alert('Error: El RFC y la confirmación no coinciden.');
            return;
        }

        if (utils.sociedadDuplicada(rfc)) {
            alert('Error: El RFC ya ha sido agregado.');
            return;
        }

        if ($('#acta-constitutiva-sociedad-pdf').val() === '') {
            alert('Acta constitutiva de la sociedad (PDF) es requerida.');
            return;
        }
        if ($('#lista-socios-pdf').val() === '') {
            alert('Lista de socios (PDF) es requerida.');
            return;
        }

        if (data.edicionSociedades.length >= data.empresasPermitidos) {
            alert('Error: No se pueden agregar más empresas.');
            return;
        }

        var idxEmpresas = data.empresasPrevias + data.edicionSociedades.length;
        var anexoActaConstitutiva = $('#acta-constitutiva-sociedad-pdf').clone();
        anexoActaConstitutiva.attr('id', idxEmpresas + '_acta-constitutiva-sociedad-pdf');

        var anexoListaSocios = $('#lista-socios-pdf').clone();
        anexoListaSocios.attr('id', idxEmpresas + '_lista-socios-pdf');

        var sociedad = {
            rfc: rfc,
            nombre: $('#factura-nombre-sociedad').val()
        };
        data.edicionSociedades.push(sociedad);
        data.anexosActaConstitutiva.push(anexoActaConstitutiva);
        data.anexosListaSocios.push(anexoListaSocios);
        utils.limpiaSociedad();
        utils.construyeSociedades();
    };

    utils.sociedadDuplicada = function (rfc) {
        for (var i = 0; i < data.edicionSociedades.length; i++) {
            if (data.edicionSociedades[i].rfc === rfc) {
                return true;
            }
        }
        return false;
    };

    utils.limpiaSociedad = function () {
        $('#uso-factura-persona-moral input.valid-field').limpiaErrores().val('');
        $('#uso-factura-persona-moral input.form-control-file').val('');
    };

    utils.construyeSociedades = function () {
        var buffer = [];
        if (data.edicionSociedades.length === 0) {
            buffer.push('<tr><td class="text-center" colspan="4">Sin sociedades</td></tr>');
            return $('#sociedades-table tbody').html(buffer.join(''));
        }
        for (var i = 0; i < data.edicionSociedades.length; i++) {
            var sociedad = data.edicionSociedades[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-right">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(sociedad.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(sociedad.rfc);
            buffer.push('</td>');
            buffer.push('<td class="text-center"><button id="eliminar-sociedad-');
            buffer.push(i);
            buffer.push('" class="btn btn-danger btn-sm eliminar-sociedad"><i class="fa fa-minus-circle"></i></button></td>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        $('#sociedades-table tbody').html(buffer.join(''));
    };

    handlers.eliminaSociedad = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-sociedad-'.length), 10);
        var sociedades = [];
        var anexosActaConstitutiva = [];
        var anexosListaSocios = [];
        for (var i = 0; i < data.edicionSociedades.length; i++) {
            if (i !== idx) {
                sociedades.push(data.edicionSociedades[i]);
                var indiceEmpresas = data.empresasPrevias + anexosActaConstitutiva.length;
                var anexoActa = data.anexosActaConstitutiva[i];
                var anexoSocio = data.anexosListaSocios[i];
                anexoActa.attr('id', indiceEmpresas + '_acta-constitutiva-sociedad-pdf');
                anexoSocio.attr('id', indiceEmpresas + '_lista-socios-pdf');
                anexosActaConstitutiva.push(anexoActa);
                anexosListaSocios.push(anexoSocio);
            }
        }

        data.edicionSociedades = sociedades;
        data.anexosActaConstitutiva = anexosActaConstitutiva;
        data.anexosListaSocios = anexosListaSocios;
        utils.construyeSociedades();
    };

    utils.muestraPermitidos = function (inscripcion) {
        data.contratosPermitidos = inscripcion.numeroContratos - utils.getActivos(inscripcion.contratos).length;
        data.prediosPermitidos = inscripcion.numeroPredios - utils.getActivos(inscripcion.predios).length;
        data.empresasPermitidos = inscripcion.numeroEmpresas - utils.getActivos(inscripcion.empresas).length;
        $('#contratos-permitidos').html(data.contratosPermitidos);
        $('#predios-permitidos').html(data.prediosPermitidos);
        $('#uso-facturas-permitidos').html(data.empresasPermitidos);
    };

    utils.getActivos = function (registros) {
        var activos = [];
        for (var i = 0; i < registros.length; i++) {
            var r = registros[i];
            if (!r.fechaCancelacion) {
                activos.push(r);
            }
        }
        return activos;
    };

    //GUARDAMOS LA EDICION CON LAS NUEVAS LISTAS----------------------------
    handlers.guardaEdicion = function (e) {
        e.preventDefault();
        $('#button-guarda-edicion').prop('disabled', true);
        if (data.edicionContratos.length !== data.contratosPermitidos) {
            alert('Error: Aún no se agregan todos los contratos permitidos.');
            $('#button-guarda-edicion').prop('disabled', false);
            return;
        }
        if (data.edicionPredios.length !== data.prediosPermitidos) {
            alert('Error: Aún no se agregan todos los predios permitidos.');
            $('#button-guarda-edicion').prop('disabled', false);
            return;
        }
        if (data.edicionSociedades.length !== data.empresasPermitidos) {
            alert('Error: Aún no se agregan todas las empresas permitidas.');
            $('#button-guarda-edicion').prop('disabled', false);
            return;
        }

        var registro = {
            contratos: data.edicionContratos,
            predios: data.edicionPredios,
            empresas: data.edicionSociedades,
            claveArchivos: utils.getClavesArchivosEdicion()
        };
        $.ajax({
            url: '/maiz/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + data.inscripcion.uuid + '/edicion/',
            data: JSON.stringify(registro),
            type: 'PUT',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            $('#div-carga-archivos').cargaArchivos({
                archivos: utils.getArchivosEdicion(),
                urlComprobante: '/maiz/resources/productores/maiz/inscripcion/' + data.uuid + '/inscripcion-productor.pdf',
                callBackCerrar: handlers.cierraDescarga
            });
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Ocurrió un error al editar el registro.');
            }
        });
        $('#button-guarda-edicion').prop('disabled', false);
    };

    handlers.cierraDescarga = function (e) {
        $.segalmex.common.bandejas.actualizar();
        $('.detalle-elemento-form').hide();
        $('#detalle-elemento-comentarios').html('');
        $('#lista-entradas').show();
    };

    utils.limpiaEdicion = function () {
        data.edicionContratos = [];
        data.edicionPredios = [];
        data.edicionSociedades = [];
        data.contratosInscripcion = [];
        utils.construyeContratos();
        utils.construyePredios();
        utils.construyeSociedades();
        utils.resetPredios();
        data.edicionAnexosPredio = [];
        data.anexosSiembraXlm = [];
        data.anexosArrendamientoPdf = [];
        data.anexosActaConstitutiva = [];
        data.anexosListaSocios = [];
        data.anexosPrediosolicitado = [];
        data.anexosRiegoXlm = [];
        $('#edicion-numero-contrato,#edicion-cantidad-contratada-contrato,#edicion-cantidad-contratada-cobertura').val('').limpiaErrores();
        $('#predios-productor-valid input').val('').limpiaErrores();
        $('#predios-productor-valid select').val('0').limpiaErrores();
        $('#uso-factura-persona-moral input').val('').limpiaErrores();
        $('input.form-control-file').val('');
        $('input-numero-contrato').html('');
        data.rendimientoSolValido = false;
        data.solicitado = false;
    };

    utils.construyeTablaAnexos = function (anexos, opcionales) {
        var buffer = `
                <h4>Archivos anexos</h4>
                <div class="alert alert-info">
                <p>La siguiente tabla muestra los anexos que debe llevar el registro:</p>
                <ul class="mb-1">
                  <li><strong class="text-success"><i class="fas fa-check-circle"></i></strong> Archivo registrado en sistema</li>
                  <li><strong class="text-danger"><i class="fas fa-times-circle"></i></strong> Archivo pendiente de subir a sistema</li>
                </ul>
                </div>
                <div class="table-responsive">
                <table id="archivos-cargados" class="table table-bordered table-striped">
                <thead class="thead-dark"><tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Estatus</th>
                </tr></thead>
                <tbody>
        `;

        for (var i = 0; i < anexos.length; i++) {
            var anexo = anexos[i];
            buffer += `
                <tr>
                <td>${i + 1}</td>
                <td>
                <label for="${anexo.tipo}-pdf" class="control-label">${utils.getNombreArchivo(anexo.tipo)}</label>
                <input id="${anexo.tipo}-pdf" type="file" class="form-control-file" accept="${utils.getAccept(anexo.tipo)}"/>
                ${anexo.cargado ?
                    '<a href="/maiz/resources/archivos/maiz/' + anexo.uuid + '/' + anexo.nombre + '?inline=true" target="_blank" > ' + anexo.nombre + '</a>'
                    : '' }
                </td>
                <td>${anexo.cargado ? '<strong class="text-success"><i class="fas fa-check-circle"></i> Enviado</strong></td>'
                    : '<strong class="text-danger"><i class="fas fa-times-circle"></i> Pendiente</strong></td>'}
                </tr>
            `;
        }
        buffer += `<tbody>
                  </table>
                  </div><br/>
        `;
        //Si la inscripcion tiene archivos opcionales se pinta la tabla.
        if (opcionales.length > 0) {
            buffer += `
            <h5>Archivos opcionales</h5>
            <div class="table-responsive">
                <table id="archivos-opcionales" class="table table-bordered table-striped">
                <thead class="thead-dark"><tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Estatus</th>
                </tr></thead>
                <tbody>
            `;
            for (var i = 0; i < opcionales.length; i++) {
                var anexo = opcionales[i];
                buffer += `
                <tr>
                <td>${i + 1}</td>
                <td>
                <label for="${anexo.tipo}-pdf" class="control-label">${utils.getNombreArchivo(anexo.tipo)}</label>
                <input id="${anexo.tipo}-pdf" type="file" class="form-control-file" accept="${utils.getAccept(anexo.tipo)}"/>
                ${anexo.cargado ?
                        '<a href="/maiz/resources/archivos/maiz/' + anexo.uuid + '/' + anexo.nombre + '?inline=true" target="_blank" > ' + anexo.nombre + '</a>'
                        : '' }
                </td>
                <td>${anexo.cargado ? '<strong class="text-success"><i class="fas fa-check-circle"></i> Enviado</strong></td>'
                        : '<strong class="text-danger"><i class="fas fa-times-circle"></i> Pendiente</strong></td>'}
                </tr>
            `;
            }
            buffer += `<tbody>
                  </table>
                  </div><br/>
        `;

        }


        return buffer;
    };

    utils.getAccept = function (tipo) {
        if (tipo && tipo.includes('xml')) {
            return 'text/xml';
        }
        return 'application/pdf';
    };

    utils.getNombreArchivo = function (archivo) {
        var nombreArchivo = '';
        switch (utils.clasifica(archivo)) {
            case "curp-productor":
                nombreArchivo = 'CURP del productor (PDF)';
                break;
            case "rfc-productor":
                nombreArchivo = 'RFC del productor (PDF)';
                break;
            case "documento-identificacion-productor":
                nombreArchivo = 'Documento de identificación (PDF)';
                break;
            case "datos-bancarios":
                nombreArchivo = ' Datos bancarios (PDF)';
                break;
            case "documentacion-predio":
                var n = archivo.split('_')[0];
                nombreArchivo = 'Documentación predio (PDF) [' + n + ']';
                break;
            case "acta-constitutiva-sociedad":
                var n = archivo.split('_')[0];
                var c = parseInt(n) + 1;
                nombreArchivo = 'Acta constitutiva (PDF) [' + c + ']';
                break;
            case "lista-socios":
                var n = archivo.split('_')[0];
                var c = parseInt(n) + 1;
                nombreArchivo = 'Lista de socios (PDF) [' + c + ']';
                break;
            case "documentacion-rendimiento-solicitado":
                var n = archivo.split('_')[0];
                nombreArchivo = 'Documentación permiso rendimiento superior (PDF) [' + n + ']';
                break;
                //Archivos extra de predios
            case "permiso-siembra-xml":
                var n = archivo.split('_')[0];
                nombreArchivo = 'Permiso de siembra (XML) [' + n + ']';
                break;
            case "permiso-siembra":
                var n = archivo.split('_')[0];
                nombreArchivo = 'Permiso de siembra (PDF) [' + n + ']';
                break;
            case "pago-riego-xml":
                var n = archivo.split('_')[0];
                nombreArchivo = 'Pago de riego (XML) [' + n + ']';
                break;
            case "pago-riego":
                var n = archivo.split('_')[0];
                nombreArchivo = 'Pago de riego (PDF) [' + n + ']';
                break;
            case "acreditacion-predio":
                var n = archivo.split('_')[0];
                nombreArchivo = 'Acreditación de la posesión del predio (PDF) [' + n + ']';
                break;
            case "contrato-arrendamiento":
                var n = archivo.split('_')[0];
                nombreArchivo = 'Contrato de arrendamiento (PDF) [' + n + ']';
                break;
            default :
                nombreArchivo = archivo;
        }
        return nombreArchivo;
    };

    utils.clasifica = function (nombreArchivo) {
        if (nombreArchivo.includes('documentacion-predio')) {
            return 'documentacion-predio';
        }
        if (nombreArchivo.includes('acta-constitutiva-sociedad')) {
            return 'acta-constitutiva-sociedad';
        }
        if (nombreArchivo.includes('lista-socios')) {
            return 'lista-socios';
        }
        if (nombreArchivo.includes('documentacion-rendimiento-solicitado')) {
            return 'documentacion-rendimiento-solicitado';
        }
        if (nombreArchivo.includes('permiso-siembra-xml')) {
            return 'permiso-siembra-xml';
        }
        if (nombreArchivo.includes('permiso-siembra')) {
            return 'permiso-siembra';
        }
        if (nombreArchivo.includes('pago-riego-xml')) {
            return 'pago-riego-xml';
        }
        if (nombreArchivo.includes('pago-riego')) {
            return 'pago-riego';
        }
        if (nombreArchivo.includes('acreditacion-predio')) {
            return 'acreditacion-predio';
        }
        if (nombreArchivo.includes('contrato-arrendamiento')) {
            return 'contrato-arrendamiento';
        }
        return nombreArchivo;
    };

    utils.getArchivos = function (archivos) {
        var fds = [];
        // Agregamos los archivos faltantes o que se reemplazaran.
        for (var i = 0; i < archivos.length; i++) {
            var file = archivos[i];
            var id = file.id;
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/maiz/resources/productores/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            var archivo = $('#' + id)[0].files[0];
            fd.append('file', archivo);
            var etiqueta = $('label[for=' + file.id + ']').html();
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }
        return fds;
    };

    utils.cargaCatalogosProductor = function (bandeja, inscripcion) {
        $.ajax({
            type: 'GET',
            url: '/maiz/resources/paginas/inscripcion-productor/edicion/' + inscripcion.ciclo.clave,
            dataType: 'json'
        }).done(function (response) {
            data.contratosInscripcion = inscripcion.contratos;
            data.catalogosProductor = response;
            data.estadosPredio = response.estadosPredio;
            data.tiposPosesion = response.tiposPosesion;
            data.tiposDocumentoPropia = response.tiposDocumentoPropia;
            data.tiposDocumentoRentada = response.tiposDocumentoRentada;
            data.municipiosEdicion = response.municipios;
            data.tiposCultivo = response.tipos;
            data.permitePredioSolicitado = response.predioSolicitado;
            data.archivosPredio = response.archivosPredio;
            $('#edicion-tipo-cultivo-predio').actualizaCombo(response.tipos);
            $('#edicion-tipo-posesion-predio').actualizaCombo(response.tiposPosesion);
            $('#edicion-regimen-hidrico-predio').actualizaCombo(response.regimenes);
            $('#edicion-estado-predio').actualizaCombo(response.estadosPredio);
            data.volumenPredio = parseFloat(response.volumenPredio.valor);
            data.superficiePredio = parseFloat(response.superficiePredio.valor);
            utils.setRendimiento(response.rendimientos);
            utils.setPrevios(inscripcion);
            utils.actualizaMaximosPredio(utils.getActivos(inscripcion.predios));
            //utils.cargaContratos(response.contratos);
            utils.configuraBandeja(bandeja, inscripcion);
            data.archivosRqueridosSiembra = response.archivosSiembra;
            data.archivosRequeridosRiego = response.archivosRiego;
            data.cantidadContratada = parseFloat(response.cantidadContratada.valor);
            utils.actualizaCantidadContratada(inscripcion.contratos);
            //Si archivosPredio = true mostramos la seccion de archivos extras
            utils.muestraArchivosPredio();
        }).fail(function () {
            alert('Error: Al descargar recursos de la página.');
        });
    };

    utils.muestraArchivosPredio = function () {
        if (data.archivosPredio) {
            $('#div-documentacion-nueva').show();
        }
    };

    utils.resetPredios = function () {
        $('#muestra-punto-medio-predio').addClass('active');
        $('#muestra-punto-medio-predio').click();
    };

    utils.isInvalidaVolumen = function () {
        var volumen = 0;
        for (var i = 0; i < data.edicionPredios.length; i++) {
            volumen = volumen + parseFloat(data.edicionPredios[i].volumen);
        }
        volumen = volumen + parseFloat($("#edicion-volumen-predio").val());
        return volumen > parseFloat(data.volumenPredio);
    };

    utils.isInvalidaSuperficie = function () {
        var superficie = 0;
        for (var i = 0; i < data.edicionPredios.length; i++) {
            superficie = superficie + parseFloat(data.edicionPredios[i].superficie);
        }
        superficie = superficie + parseFloat($("#edicion-superficie-predio").val());
        return superficie > parseFloat(data.superficiePredio);
    };

    //actualizamos los valores maximos de acuerdo a los predios que ya estaban agregados
    utils.actualizaMaximosPredio = function (predios) {
        var volumenTotal = 0.0;
        var superficieTotal = 0.0;
        for (var i = 0; i < predios.length; i++) {
            var p = predios[i];
            volumenTotal += parseFloat(p.volumen);
            superficieTotal += parseFloat(p.superficie);
        }
        data.volumenPredio = (data.volumenPredio - volumenTotal).toFixed(3);
        data.superficiePredio = (data.superficiePredio - superficieTotal).toFixed(3);
    };

    utils.setPrevios = function (inscripcion) {
        data.prediosPrevios = (inscripcion.predios && inscripcion.predios.length > 0)
                ? inscripcion.predios.length : 0;
        data.empresasPrevias = (inscripcion.empresas && inscripcion.empresas.length > 0)
                ? inscripcion.empresas.length : 0;
    };

    utils.getArchivosEdicion = function () {
        var fds = [];
        // Agregamos archivos predios.
        for (var i = 0; i < data.edicionAnexosPredio.length; i++) {
            var anexo = data.edicionAnexosPredio[i];
            var id = anexo.attr('id');
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/maiz/resources/productores/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            var etiqueta = 'Documentación predio (PDF) [' + (i + 1) + ']';
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }

        // Agregamos archivos de siembra xml
        for (i = 0; i < data.anexosSiembraXlm.length; i++) {
            var anexo = data.anexosSiembraXlm[i];
            var id = anexo.attr('id');
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/maiz/resources/productores/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            var etiqueta = 'Permiso de siembra (XML) [' + (i + 1) + ']';
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }

        // Agregamos archivos de riego xml
        for (i = 0; i < data.anexosRiegoXlm.length; i++) {
            var anexo = data.anexosRiegoXlm[i];
            var id = anexo.attr('id');
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/maiz/resources/productores/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            var etiqueta = 'Pago de riego (XML) [' + (i + 1) + ']';
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }

        // Agregamos archivos actas constitutivas.
        for (i = 0; i < data.anexosActaConstitutiva.length; i++) {
            var anexo = data.anexosActaConstitutiva[i];
            var id = anexo.attr('id');
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/maiz/resources/productores/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            var etiqueta = 'Acta constitutiva (PDF) [' + (i + 1) + ']';
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }

        // Agregamos archivos lista de socios.
        for (i = 0; i < data.anexosListaSocios.length; i++) {
            var anexo = data.anexosListaSocios[i];
            var id = anexo.attr('id');
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/maiz/resources/productores/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            var etiqueta = 'Lista de socios (PDF) [' + (i + 1) + ']';
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }

        for (i = 0; i < data.anexosPrediosolicitado.length; i++) {
            var anexo = data.anexosPrediosolicitado[i];
            var id = anexo.attr('id');
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/maiz/resources/productores/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            var etiqueta = 'Documentación permiso rendimiento superior (PDF) [' + (i + 1) + ']';
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }

        return fds;
    };

    utils.getClavesArchivosEdicion = function () {
        var archivosRequeridos = utils.getArchivosClaves(true);
        var archivosOpcionales = utils.getArchivosClaves(false);
        var claveArchivos = data.claveArchivos.split('|');
        var clavesObligatorias = claveArchivos[0].split(',');
        var clavesOpcionales = claveArchivos[1] === undefined ? [] : claveArchivos[1].split(',');

        for (var i = 0; i < archivosRequeridos.length; i++) {
            var file = archivosRequeridos[i];
            var id = file.id;
            var tipo = id.substring(0, id.length - '-pdf'.length);
            clavesObligatorias.push(tipo);
        }

        for (var i = 0; i < archivosOpcionales.length; i++) {
            var file = archivosOpcionales[i];
            var id = file.id;
            var tipo = id.substring(0, id.length - '-pdf'.length);
            clavesOpcionales.push(tipo);
        }
        return clavesOpcionales.length > 0
                ? clavesObligatorias.join(',').concat('|').concat(clavesOpcionales.join(','))
                : clavesObligatorias.join(',');
    };

    //Métodos para confirmar/cancelar rendimiento solicitado.
    utils.confirmaRendimientoSolicitado = function () {
        data.solicitado = true;
        handlers.agregaPredio();
        if (data.rendimientoSolValido) {
            $('#rendimineto-solicitado-modal').modal('hide');
        }
    };

    utils.cancelarRendimientoSolicitado = function () {
        data.solicitado = false;
        data.rendimientoSolValido = false;
        $('#rendimineto-solicitado-modal').modal('hide');
    };

    utils.getAnexoIndice = function (idx, arreglo) {
        for (var i = 0; i < arreglo.length; i++) {
            var anexo = arreglo[i];
            if (anexo.idx === idx) {
                return anexo;
            }
        }
    };

    utils.setRojo = function (val) {
        return '\n<p style="color:Red;"> (' + val + ')</p>';
    };

    utils.getAnexoConstruido = function (indice, id) {
        var anexo = $('#' + id).clone();
        anexo.attr('id', (indice) + '_' + id);
        anexo.attr('estado', data.estadoActual.id);
        return anexo;
    };

    utils.isCapturado = function (id) {
        return $('#' + id).val() !== '';
    };

    utils.getArchivosClaves = function (requerido) {
        var sinaloa = $.segalmex.get(data.estadosPredio, '25');
        $('#acta-constitutiva-sociedad-pdf,#lista-socios-pdf,#documentacion-rendimiento-solicitado-pdf,#inscripcion-productor-tmp-pdf,#curps-xlsx-tmp').prop('disabled', true);
        $('#permiso-siembra-xml-tmp,#pago-riego-xml-tmp').prop('disabled', true);
        $('#div-archivos-predio input.form-control-file').prop('disabled', true);
        var files = $('input.form-control-file:enabled');
        $('#acta-constitutiva-sociedad-pdf,#lista-socios-pdf,#documentacion-rendimiento-solicitado-pdf,#inscripcion-productor-tmp-pdf,#curps-xlsx-tmp').prop('disabled', false);
        $('#permiso-siembra-xml-tmp,#pago-riego-xml-tmp').prop('disabled', false);
        $('#div-archivos-predio input.form-control-file').prop('disabled', false);
        var fdsRequeridos = [];
        var fdsOpcionales = [];

        // Agregamos archivos del documento
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            var id = file.id;
            var fd = new FormData();
            var archivo = $('#' + id)[0].files[0];
            fd.append('file', archivo);
            fdsRequeridos.push({id: id, fd: fd});
        }

        // Agregamos archivos predios.
        for (i = 0; i < data.edicionAnexosPredio.length; i++) {
            var anexo = data.edicionAnexosPredio[i];
            var id = anexo.attr('id');
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            fdsRequeridos.push({id: id, fd: fd});
        }

        // Agregamos archivos de maximos solicitados.
        for (i = 0; i < data.anexosPrediosolicitado.length; i++) {
            var anexo = data.anexosPrediosolicitado[i];
            var id = anexo.attr('id');
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            fdsRequeridos.push({id: id, fd: fd});
        }

        // Agregamos archivos de siembra xml
        for (i = 0; i < data.anexosSiembraXlm.length; i++) {
            var anexo = data.anexosSiembraXlm[i];
            var id = anexo.attr('id');
            var estado = anexo.attr('estado');
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            if (sinaloa.id === parseInt(estado)) {
                fdsRequeridos.push({id: id, fd: fd});
            } else {
                fdsOpcionales.push({id: id, fd: fd});
            }
        }

        // Agregamos archivos de riego xml
        for (i = 0; i < data.anexosRiegoXlm.length; i++) {
            var anexo = data.anexosRiegoXlm[i];
            var id = anexo.attr('id');
            var estado = anexo.attr('estado');
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            if (sinaloa.id === parseInt(estado)) {
                fdsRequeridos.push({id: id, fd: fd});
            } else {
                fdsOpcionales.push({id: id, fd: fd});
            }
        }
        // Agregamos archivos actas constitutivas.
        for (i = 0; i < data.anexosActaConstitutiva.length; i++) {
            var anexo = data.anexosActaConstitutiva[i];
            var id = anexo.attr('id');
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            fdsRequeridos.push({id: id, fd: fd});
        }

        // Agregamos archivos lista de socios.
        for (i = 0; i < data.anexosListaSocios.length; i++) {
            var anexo = data.anexosListaSocios[i];
            var id = anexo.attr('id');
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            fdsRequeridos.push({id: id, fd: fd});
        }

        if (requerido) {
            return fdsRequeridos;
        } else {
            return fdsOpcionales;
        }
    };

    utils.muestraModalSiembra = function (e) {
        e.preventDefault();
        $('#permiso-siembra-xml-tmp').val('');
        $('#carga-siembra-modal').modal('show');
    };

    utils.muestraModalRiego = function (e) {
        e.preventDefault();
        $('#pago-riego-xml-tmp').val('');
        $('#carga-riego-modal').modal('show');
    };

    utils.construyeAnexosXml = function (indice, id, archivos, idEstado) {
        for (var i = 0; i < archivos.length; i++) {
            var anexo = archivos[i];
            anexo.attr('id', (data.prediosPrevios + indice) + '-' + (i + 1) + '_' + id);
            anexo.idx = indice - 1;
            anexo.attr('estado', idEstado);
        }
    };

    utils.agregaArchivoSiembra = function () {
        if ($('#permiso-siembra-xml-tmp').val() === '') {
            alert('Seleccione un archivo.');
            return;
        }
        var archivoSiembra = $('#permiso-siembra-xml-tmp').clone();
        archivoSiembra.attr('id', 'permiso-siembra-xml-tmp');
        data.archivosTmpSiembraXml.push(archivoSiembra);
        $('#permiso-siembra-xml-tmp').val('');
        utils.construyeTablaArchivosSiembra();
        utils.pintaNumeroArchivos('permiso-siembra', data.archivosTmpSiembraXml, 'Permiso(s)');
    };

    utils.construyeTablaArchivosSiembra = function () {
        var buffer = [];
        for (var i = 0; i < data.archivosTmpSiembraXml.length; i++) {
            buffer.push('<tr>');
            buffer.push('<td class="text-right">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push('Permiso de siembra (XML) [' + (i + 1) + ']');
            buffer.push('</td>');
            buffer.push('<td class="text-center"><button id="eliminar-siembra-');
            buffer.push(i);
            buffer.push('" class="btn btn-danger btn-sm eliminar-siembra"><i class="fa fa-minus-circle"></i></button></td>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        $('#table-archivos-siembra-xml tbody').html(buffer.join(''));
    };

    handlers.eliminaArchivoSiembra = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-siembra-'.length), 12);
        var archivosSiembra = [];
        for (var i = 0; i < data.archivosTmpSiembraXml.length; i++) {
            if (i !== idx) {
                archivosSiembra.push(data.archivosTmpSiembraXml[i]);
            }
        }
        data.archivosTmpSiembraXml = archivosSiembra;
        utils.construyeTablaArchivosSiembra();
        utils.pintaNumeroArchivos('permiso-siembra', data.archivosTmpSiembraXml, 'Permiso(s)');
    };

    utils.agregaArchivoRiego = function () {
        if ($('#pago-riego-xml-tmp').val() === '') {
            alert('Seleccione un archivo.');
            return;
        }
        var archivoRiego = $('#pago-riego-xml-tmp').clone();
        archivoRiego.attr('id', 'pago-riego-xml-tmp');
        data.archivosTmpRiegoXml.push(archivoRiego);
        $('#pago-riego-xml-tmp').val('');
        utils.construyeTablaArchivosRiego();
        utils.pintaNumeroArchivos('pago-riego', data.archivosTmpRiegoXml, 'Pago(s)');
    };

    utils.construyeTablaArchivosRiego = function () {
        var buffer = [];
        for (var i = 0; i < data.archivosTmpRiegoXml.length; i++) {
            buffer.push('<tr>');
            buffer.push('<td class="text-right">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push('Pago de riego (XML) [' + (i + 1) + ']');
            buffer.push('</td>');
            buffer.push('<td class="text-center"><button id="eliminar-riego-');
            buffer.push(i);
            buffer.push('" class="btn btn-danger btn-sm eliminar-riego"><i class="fa fa-minus-circle"></i></button></td>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        $('#table-archivos-riego-xml tbody').html(buffer.join(''));
    };

    handlers.eliminaArchivoRiego = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-riego-'.length), );
        var archivosRiego = [];
        for (var i = 0; i < data.archivosTmpRiegoXml.length; i++) {
            if (i !== idx) {
                archivosRiego.push(data.archivosTmpRiegoXml[i]);
            }
        }
        data.archivosTmpRiegoXml = archivosRiego;
        utils.construyeTablaArchivosRiego();
        utils.pintaNumeroArchivos('pago-riego', data.archivosTmpRiegoXml, 'Pago(s)');
    };

    utils.pintaNumeroArchivos = function (id, archivos, label) {
        var txt = '';
        if (archivos.length > 0) {
            txt = archivos.length + ' ' + label;
        }
        $('#' + id).val(txt);
    };

    utils.limpiaXmls = function () {
        data.archivosTmpSiembraXml = [];
        data.archivosTmpRiegoXml = [];
        utils.construyeTablaArchivosSiembra();
        utils.construyeTablaArchivosRiego();
    };

    utils.getAnexosIndice = function (idx, arreglo) {
        var recuperados = [];
        for (var i = 0; i < arreglo.length; i++) {
            var anexo = arreglo[i];
            if (anexo.idx === idx) {
                recuperados.push(anexo);
            }
        }
        return recuperados;
    };

    utils.getTotalArchivosPredio = function (estado, paramsArchivos) {
        var valor = 0;
        for (var r = 0; r < paramsArchivos.length; r++) {
            var param = paramsArchivos[r];
            if (param.clave.includes(estado.clave)) {
                valor = parseInt(param.valor);
            }
        }
        return valor;
    };

    utils.getAnexoConstruido = function (indice, id) {
        var anexo = $('#' + id).clone();
        anexo.attr('id', (indice) + '_' + id);
        anexo.attr('estado', data.estadoActual.id);
        return anexo;
    };

    utils.actualizaCantidadContratada = function (contratos) {
        var cantidadContratada = 0.0;
        for (var i = 0; i < contratos.length; i++) {
            var c = contratos[i];
            cantidadContratada += c.cantidadContratada;
        }
        data.cantidadContratada = data.cantidadContratada - cantidadContratada;
    };

    utils.isInvalidaCantidadContratada = function () {
        var cantidad = 0;
        for (var i = 0; i < data.edicionContratos.length; i++) {
            cantidad = cantidad + parseFloat(data.edicionContratos[i].cantidadContratada);
        }
        cantidad = cantidad + parseFloat($("#edicion-cantidad-contratada-contrato").val());
        return cantidad > data.cantidadContratada;
    };

    handlers.muestraCorrectos = function (e) {
        e.preventDefault();
        if ($('#div-datos-capturados-correctos').is(":visible")) {
            $('#btn-mostrar-correctos').html('Mostrar <span class="fas fa-check-circle text-secondary" aria-hidden="true">');
            $('#div-datos-capturados-correctos').hide();
        } else {
            $('#btn-mostrar-correctos').html('Ocultar <span class="fas fa-check-circle text-secondary" aria-hidden="true">');
            $('#div-datos-capturados-correctos').show();
        }
    };

    handlers.muestraFoliosRepetidos = function (e) {
        e.preventDefault();
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('mostrar-repetidos-'.length), 10);
        for (var i = 0; i < data.prediosDocumentos.length; i++) {
            if (idx === i) {
                var pd = data.prediosDocumentos[i];
            }
        }
        $.ajax({
            url: '/maiz/resources/predio-documento/duplicados/' + pd.uuidTimbreFiscalDigital,
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            var folios = response.join(',');
            $('#label-folios').html('<strong >' + folios + '</strong>');
            $('#muestra-repetidos-modal').modal('show');
        }).fail(function () {
            alert('Error: No se pudo obtener los folios duplicados.');
        });
    };

    utils.configuraTablaContratosProductor = function (correccion) {
        var aoColumns = [{"bSortable": false}, {"bSortable": true}, {"bSortable": true},
            {"bSortable": true}, {"bSortable": false},
            {"bSortable": false}, {"bSortable": false}];
        if (correccion) {
            aoColumns.push({"bSortable": false});
            aoColumns.push({"bSortable": false});
        }
        utils.configuraTablaContratoProductor('detalle-productores-table', aoColumns);
    };

    utils.configuraTablaContratoProductor = function (tablaId, aoColumns) {
        var table = $('#' + tablaId).DataTable({
            "oLanguage": {
                "sEmptyTable": "No se encontró información para mostrar en la tabla",
                "sSearch": "Buscar ",
                "sZeroRecords": "No se encontraron resultados."
            },
            "bFilter": true,
            "bInfo": false,
            "bPaginate": false,
            "aoColumns": aoColumns,
            "aoColumnDefs": [{"bSortable": false, "aTargets": ["no-sort"]}]
        });
        table.on('order.dt search.dt', function () {
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).order([[1, 'asc']]).draw(false);
        $("#" + tablaId + "_filter input").addClass('input-buscar');
        $(".dataTables_empty").attr("colspan", "100%");
    };

    handlers.muestraDetalleContratoProductor = function (e) {
        e.preventDefault();
        $('#detalle-contrato-productor-modal').modal('show').focus();
    };

    handlers.eliminaProductor = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-productor-'.length), 10);
        var productores = [];
        for (var i = 0; i < data.contratosProductores.length; i++) {
            if (i !== idx) {
                productores.push(data.contratosProductores[i]);
            }
        }
        data.contratosProductores = productores;
        if (data.contratosProductores.length === 0) {
            $('#detalle-contrato-productor-modal').modal('hide');
            $('#productor-detalle-table tbody').html('');
        } else {
            $('#detalle-div').html('');
            $('#detalle-div').html($.segalmex.maiz.inscripcion.vista.generaTablaDetalleContratosProductor(data.contratosProductores, true));
            utils.configuraTablaContratosProductor(true);
            utils.agregaDetalleContratoProductor();
            $('#detalle-productores-table').on('click', 'button.eliminar-productor', handlers.eliminaProductor);
        }
    };

    handlers.agregaProductor = function (e) {
        e.preventDefault();
        $('#div-contrato-productor .valid-field').limpiaErrores();
        var errores = [];
        $('#div-contrato-productor .valid-field').valida(errores, true);
        if (errores.length > 0) {
            return;
        }
        if (parseFloat($('#volumen-contrato-iar').val()) > parseFloat($('#volumen-contrato').val())) {
            alert('El volumen contratado en el CCV no puede ser menor que el volumen cubierto por el I.A.R.');
            return;
        }

        var productor = {
            nombre: $('#nombre-productor').val(),
            primerApellido: $('#papellido-productor').val(),
            segundoApellido: $('#sapellido-productor').val(),
            curp: $('#curp-productor').val(),
            rfc: $('#rfc-productor').val(),
            superficie: $('#superficie-contrato').val(),
            volumenContrato: $('#volumen-contrato').val(),
            volumenIar: $('#volumen-contrato-iar').val()
        };

        if (utils.validaProductor(productor)) {
            return;
        }
        data.contratosProductores.push(productor);
        utils.agregaDetalleContratoProductor();
        utils.limpiaContratoProductor();
        $('#detalle-div').html('');
        $('#detalle-div').html($.segalmex.maiz.inscripcion.vista.generaTablaDetalleContratosProductor(data.contratosProductores, true));
        utils.configuraTablaContratosProductor(true);
        $('#detalle-productores-table').on('click', 'button.eliminar-productor', handlers.eliminaProductor);
    };

    utils.limpiaContratoProductor = function () {
        $('#div-contrato-productor input.valid-field').val('').limpiaErrores();
    };

    utils.validaProductor = function (productor) {
        if (data.contratosProductores.length > 0) {
            for (var i = 0; i < data.contratosProductores.length; i++) {
                if (productor.curp === data.contratosProductores[i].curp) {
                    alert('El productor con CURP ' + productor.curp + ' ya fue agregado.');
                    return  true;
                }
                if (productor.rfc === data.contratosProductores[i].rfc) {
                    alert('El productor con RFC ' + productor.rfc + ' ya fue agregado.');
                    return  true;
                }
            }
        }
        return false;
    };

    handlers.eliminaContratosProductores = function (e) {
        e.preventDefault();
        if (!confirm("¿Está seguro de eliminar todos los productores agregados?")) {
            return;
        }
        data.contratosProductores = [];
        $('#detalle-div').html('');
        $('#productor-detalle-table tbody').html('');
        $('#detalle-contrato-productor-modal').modal('hide');
    };

    handlers.corrigeContratosProductor = function (e) {
        e.preventDefault();
        $('#button-corrige-productores').prop('disabled', true);

        var superficieTotal = 0.0;
        var volumenTotal = 0.0;
        for (var i = 0; i < data.contratosProductores.length; i++) {
            superficieTotal += parseFloat(data.contratosProductores[i].superficie);
            volumenTotal += parseFloat(data.contratosProductores[i].volumenContrato);
        }
        if (data.numeroProductores !== data.contratosProductores.length) {
            alert('La cantidad de productores que respaldan el contrato es diferente a los productores agregados.');
            $('#button-corrige-productores').prop('disabled', false);
            return;
        }
        if (volumenTotal > data.volumenContrato) {
            alert('El volumen de los productores debe ser menor o igual al volumen total contratado.');
            $('#button-corrige-productores').prop('disabled', false);
            return;
        }
        if (superficieTotal > data.superficieContrato) {
            alert('La superficie total de los productores debe ser menor o igual a la superficie contratada.');
            $('#button-corrige-productores').prop('disabled', false);
            return;
        }
        var inscripcion = {
            uuid: data.inscripcion.uuid,
            contratosProductor: data.contratosProductores
        };

        $.ajax({
            url: '/maiz/resources/contratos/maiz/inscripcion/' + inscripcion.uuid + '/corrige/contrato-productor/',
            data: JSON.stringify(inscripcion),
            type: 'PUT',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('Se corrigió el listado de productores.');
            $.segalmex.common.bandejas.actualizar();
            $('.detalle-elemento-form').hide();
            $('#lista-entradas').show();
            $('#button-corrige-productores').prop('disabled', false);
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Ocurrió un error al corregir el listado de productores.');
            }
            $('#button-corrige-productores').prop('disabled', false);
        });
    };

    utils.subirXlsxContratoProductor = function () {
        $('#curps-xlsx-tmp').val('');
        $('#contrato-productor-modal').modal('show');
    };

    utils.enviarContratoPRoductorXlsx = function () {
        $('#enviar-contrato-productor-xlsx').prop('disabled', true);
        if ($('#curps-xlsx-tmp').val() === '') {
            alert('El archivo CURPs de productores (XLSX) es obligatorio.');
            $('#enviar-contrato-productor-xlsx').prop('disabled', false);
            return;
        }
        var fd = new FormData();
        var archivo = $('#curps-xlsx-tmp')[0].files[0];
        fd.append('file', archivo);
        $.ajax({
            url: '/maiz/resources/contratos/maiz/inscripcion/productor/curp-xlsx',
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false
        }).done(function (response) {
            data.contratosProductores = data.contratosProductores.concat(response);
            $('#contrato-productor-modal').modal('hide');
            utils.agregaDetalleContratoProductor();
            $('#detalle-div').html($.segalmex.maiz.inscripcion.vista.generaTablaDetalleContratosProductor(data.contratosProductores, true));
            utils.configuraTablaContratosProductor(true);
            $('#detalle-productores-table').on('click', 'button.eliminar-productor', handlers.eliminaProductor);
            $('#enviar-contrato-productor-xlsx').prop('disabled', false);
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Error, no fue posible enviar el archivo.');
            }
            $('#enviar-contrato-productor-xlsx').prop('disabled', false);
        });
    };

    utils.getContratosProductor = function (productor) {
        var datos = {
            curp: productor.datosProductor.curp,
            rfc: productor.datosProductor.rfc,
            ciclo: productor.ciclo.id
        };
        $.ajax({
            url: '/maiz/resources/contratos/maiz/contratos-productor/',
            type: 'GET',
            data: datos,
            dataType: 'json'
        }).done(function (response) {
            if (response.length > 0) {
                for (var i = 0; i < response.length; i++) {
                    var cp = response[i];
                    var c = {
                        numeroContrato: cp.numeroContrato,
                        folio: cp.folioContrato,
                        empresa: cp.nombreEmpresaContrato,
                        cantidadContratada: cp.volumenContrato,
                        cantidadContratadaCobertura: cp.volumenIar
                    };
                    data.edicionContratos.push(c);
                }
            }
            utils.filtraContratosProductor(productor);
        }).fail(function () {
            alert('Error: No se encontró información de contratos del productor.');
        });
    };

    utils.filtraContratosProductor = (inscripcion) => {
        var contratosProductor = [];
        for (var i = 0; i < data.edicionContratos.length; i++) {
            var cp = $.segalmex.get(inscripcion.contratos, data.edicionContratos[i].numeroContrato, 'numeroContrato');
            if (cp === undefined || cp === null) {
                contratosProductor.push(data.edicionContratos[i]);
            }
        }
        data.edicionContratos = contratosProductor;
        utils.construyeContratos();
    };
})(jQuery);