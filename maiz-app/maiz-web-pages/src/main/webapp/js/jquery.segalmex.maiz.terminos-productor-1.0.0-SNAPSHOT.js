/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($) {
    $.segalmex.namespace('segalmex.maiz.terminos.productor');

    var utils = {};
    var handlers = {};

    $.segalmex.maiz.terminos.productor.getTerminos = function (clave) {
        var terminos = '';
        switch (clave) {
            case 'oi-2022':
                terminos = `
                        <p>A través del presente registro, autorizo que los predios referidos e información
                        productiva y personal, sean considerados en el Programa de Precios de Garantía a
                        Productos Alimentarios Básicos. Me obligo a proporcionar la información y
                        documentación que me sea requerida por la SADER-SEGALMEX y a notificar cualquier
                        cambio que sufra la información proporcionada. A la vez, manifiesto bajo protesta
                        de, decir la verdad que los datos contenidos en este registro son ciertos, y que
                        adquirí un Instrumento de Administración de Riesgos (IAR). Acepto la
                        responsabilidad, en la veracidad de la información proporcionada a SEGALMEX. En
                        caso de incumplimiento total o parcial, así como de SIMULACIÓN, me comprometo a
                        devolver sin reserva alguna el incentivo recibido y a aceptar la sanción
                        administrativa que conforme a derecho proceda. Soy consciente que, en caso de
                        alteración de documentación y firmas, mi registro será cancelado. También me
                        comprometo a que en caso de que me realicen algún pago en DEMASÍA, lo
                        reintegraré completamente, a través de un depósito bancario.</p>
                        <p>Estoy de acuerdo en beneficiarme con el incentivo exclusivamente al IAR en del ciclo agrícola
                        vigente, publicado en el Diario Oficial de la federación 2022 y la mecánica
                        operativa de MAIZ del ciclo correspondiente. Firmo mi registro que ampara el
                        total de toneladas de maíz (grano o semilla) producidas y comercializadas del
                        ciclo mencionado, y estarán reflejadas en el sistema SEGALMEX. Estoy consciente
                        que entraré en un buró de incumplimiento, que limita mi participación, si no
                        cumplo con lo estipulado en el Contrato de Compra Venta firmado. Los datos
                        personales recabados serán protegidos, incorporados y tratados en el marco de la
                        Ley General de Protección de Documentos Personales en Posesión de Sujetos
                        Obligados, con fundamento en los artículos 3, 27 y 28.</p>`;
                break;
            case 'oi-2021':
                terminos =
                        `<p>A través del presente registro, autorizo que los predios referidos e información productiva y personal,
                sean considerados en el Programa de Precios de Garantía a Productos Alimentarios Básicos. Me obligo a proporcionar
                la información y/o documentación que me sea requerida por la SADER-SEGALMEX y a notificar cualquier cambio que sufra
                la información proporcionada. A la vez, manifiesto bajo protesta de decir la verdad que,<strong> soy mediano productor de Maíz,
                que los datos contenidos en este registro son ciertos y reales, y que adquirí un Instrumento de Administración de Riesgos
                (IAR) para participar o tengo un Contrato a Precio Fijo.</strong> Acepto la responsabilidad en la veracidad de la información
                proporcionada a SEGALMEX. En caso de falsificar total o parcialmente la información o realizar <strong>SIMULACIÓN</strong>,
                me comprometo a devolver sin reserva alguna el incentivo recibido y aceptar la sanción administrativa que conforme
                a derecho proceda (<strong>CANCELACIÓN</strong>). En caso de alterar la documentación y/o firmas, mi registro será cancelado. En situaciones de
                <strong>PAGO(S) EN DEMASÍA</strong> (en cumplimiento a las obligaciones aceptadas en este registro), me comprometo a reintegrar el recurso, a través de depósito bancario
                a “SEGALMEX”. Estoy de acuerdo en beneficiarme con el incentivo, para la adquisición del IAR ($100/t), a los medianos productores de maíz, publicado
                en las Reglas de Operación del Programa de Precios de Garantía a Productos Alimentarios Básicos, para el ejercicio fiscal 2021 y en la Mecánica
                Operativa para Maíz comercializado por medianos productores, ciclo otoño-invierno 2020-2021. Envío mi registro
                <strong>FIRMADO por mi puño y letra</strong>, que ampara el total de toneladas de maíz que cosecharé en el ciclo mencionado, y estarán reflejadas en el sistema SEGALMEX-Maíz. Estoy consciente que
                podré ser incluido en un <strong>BURÓ DE INCUMPLIMIENTO</strong>, que puede limitar mi participación, si no cumplo con lo estipulado en el Contrato de Compra Venta
                (CCV) firmado. Considero que puedo recibir o no el incentivo con la información presentada, soportada y de acuerdo a los criterios de elegibilidad.</p>
                <p>Los datos personales recabados serán protegidos, incorporados y tratados en el marco de la Ley General de Protección de Documentos Personales en
                Posesión de Sujetos Obligados, con fundamento en los artículos 3, 27 y 28.</p>
                `;
                break;
            default:
                terminos =
                        `<p>A través del presente registro, autorizo que los predios referidos sean considerados en el
                Programa de Precios de Garantía a Productos Alimentarios Básicos. Me obligo a proporcionar
                la información y/o documentación que me sea requerida por la AGRICULTURA-SEGALMEX y a notificar
                cualquier cambio que sufra la información proporcionada. A la vez, manifiesto
                <strong>bajo protesta de decir la verdad</strong> que los datos contenidos en este registro
                son ciertos y reales, por lo que acepto mi responsabilidad, en la veracidad de la información
                proporcionada a SEGALMEX. En caso de incumplimiento total o parcial, así como de
                <strong>SIMULACIÓN</strong>, me comprometo a devolver sin reserva alguna el incentivo
                recibido y a aceptar la sanción administrativa que conforme a derecho proceda. También me
                comprometo a que en caso de que me realicen algún <strong>PAGO EN DEMASÍA</strong>, lo
                reintegraré a través de un depósito bancario. Estoy de acuerdo en beneficiarme con el precio
                de Garantía del ciclo agrícola <a class="nombre-ciclo"> </a>, publicados en el Diario Oficial de la
                Federación 2020. Envío mi registro que ampara el total de toneladas de <strong>Maíz producido
                  por medianos productores</strong> que obtendré EXCLUSIVAMENTE en la cosecha <a class="nombre-ciclo"> </a>
                , y estarán reflejadas en el sistema SEGALMEX, cuyo monto del incentivo será la
                diferencia entre el precio real de mercado, establecido como referencia por SEGALMEX, y el Precio
                de Garantía de <strong>$4,150</strong> por tonelada. Considerando que de hasta 600 toneladas
                recibiré el incentivo. </p>
              <p>Los datos personales recabados serán protegidos, incorporados y tratados en el marco de la
                Ley General de Protección de Documentos Personales en Posesión de Sujetos Obligados,
                con fundamento en los artículos 3, 27 y 28.</p>

                `;
        }
        return terminos;
    };

})(jQuery);

