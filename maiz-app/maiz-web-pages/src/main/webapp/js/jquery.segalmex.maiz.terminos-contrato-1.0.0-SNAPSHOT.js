/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($) {
    $.segalmex.namespace('segalmex.maiz.terminos.contrato');

    $.segalmex.maiz.terminos.contrato.getTerminos = function (ciclo) {
        var terminos = '';
        switch (ciclo.clave) {
            case 'oi-2022':
                terminos =
                        `
                        <p>El presente registro de contrato es el inicio de un proceso que se realiza
                        para participar en el Programa Precios de Garantía a Productos Alimentarios Básicos,
                        en maíz de medianos productores, para el <strong>Ciclo
                        <span id="condiciones-ciclo">Otoño - Invierno 2019-2020</span></strong>. Este documento
                        no obliga a SEGALMEX a brindar el incentivo a productores que respalda este,
                        a menos que cada uno cumpla con el registro firmado, los requisitos
                        y la normativa establecida en la mecánica operativa y en las Reglas de Operación 2022,
                        publicadas en el DOF el 31 de diciembre del 2021.</p>
                        <p>Los datos recabados serán protegidos, incorporados y tratados en el marco de la Ley
                        General de Protección de Documentos Personales en Posesión de Sujetos Obligados,
                        con fundamento en los artículos 3, 27 y 28.</p>
                        `;
                       
                break;
            default:
                terminos =
                        `
                        <p>El presente registro de contrato es el inicio de un proceso que se deberá
                        realizar para participar en el Programa Precios de Garantía a Productos Alimentarios
                        Básicos, en maíz comercializado por medianos productores, para el <strong>Ciclo
                        <span id="condiciones-ciclo">Otoño - Invierno 2019-2020</span></strong>. Este documento
                        no obliga a SEGALMEX a brindar el incentivo a productores que respalda este, a menos que
                        cada uno cumpla con el registro firmado, los requisitos y la normativa establecida en la
                        mecánica operativa y en las Reglas de Operación 2020, publicadas en el DOF el 24 de
                        febrero del 2020.</p>
                        <p>Los datos recabados serán protegidos, incorporados y tratados en el marco de la Ley
                        General de Protección de Documentos Personales en Posesión de Sujetos Obligados, con
                        fundamento en los artículos 3, 27 y 28.
                        `;
        }
        return terminos;
    };
})(jQuery);