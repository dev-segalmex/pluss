/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.maiz.pre.registro');
    var data = {};
    var handlers = {};
    var utils = {};

    $.segalmex.maiz.pre.registro.init = function (params) {
        $.segalmex.cultivos.pre.registro.init({
            cultivo: 'maiz-comercial'
        });
        
        $('#guardar-button').click(handlers.guarda);
        $('#si-acepto-condiciones').click($.segalmex.cultivos.pre.registro.aceptoCondiciones);
        $('#no-acepto-condiciones').click($.segalmex.cultivos.pre.registro.noAceptoCondiciones);
        $('#limpiar-button').click($.segalmex.cultivos.pre.registro.limpiar);
        $('#agregar-predio-button').click($.segalmex.cultivos.pre.registro.agregaPredio);
        $('#estado-predio').change($.segalmex.cultivos.pre.registro.cambiaEstadoPredio);
        $('#predios-table').on('click', 'button.eliminar-predio', $.segalmex.cultivos.pre.registro.eliminaPredio);
        $('#cerrar-descarga').click($.segalmex.cultivos.pre.registro.cerrarDescarga);
        $('#cobertura').change($.segalmex.cultivos.pre.registro.validaSiCobertura);
        $('#total-contratos').change($.segalmex.cultivos.pre.registro.habilitaTipoPrecio);
        $('#volumen-predio,#superficie-predio').change($.segalmex.cultivos.pre.registro.calculaRendimiento);
        $('#reimprimir-button').click($.segalmex.cultivos.pre.registro.reimprime);
        $('#reimpresion-confirmar').click($.segalmex.cultivos.pre.registro.confirmaReimpresion);
        $('#agregar-contrato-button').click($.segalmex.cultivos.pre.registro.agregaTipoPrecio);
        $('#agregar-iar-button').click($.segalmex.cultivos.pre.registro.agregaIar);
        $('#precios-table').on('click', 'button.eliminar-precio', $.segalmex.cultivos.pre.registro.eliminaTipoPrecio);
        $('#iars-table').on('click', 'button.eliminar-iar', $.segalmex.cultivos.pre.registro.eliminaIar);
        $('#tipo-iar').change($.segalmex.cultivos.pre.registro.validaTipoCobertura);
        $('#curp').change($.segalmex.cultivos.pre.registro.validaCurpRfc);
        $('#rfc').change($.segalmex.cultivos.pre.registro.validaCurpRfc);
    };

    handlers.guarda = function (e) {
        e.preventDefault();
        $(e.target).html("Guardar").prop('disabled', true);

        $('#predios-div .valid-field').prop('disabled', true);
        var errores = [];
        $('.valid-field').valida(errores, true);
        $('#predios-div .valid-field').prop('disabled', false);
        if (errores.length > 0) {
            $('#rendimiento').prop('disabled', true);
            $('#guardar-button').html("Guardar").prop('disabled', false);
            return;
        }
        $.segalmex.cultivos.pre.registro.guarda();
    };
})(jQuery);