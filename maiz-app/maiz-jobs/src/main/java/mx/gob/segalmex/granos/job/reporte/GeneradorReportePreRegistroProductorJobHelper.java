/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.job.reporte;

import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricolaEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.cultivos.job.config.AbstractJobHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Slf4j
@Component
public class GeneradorReportePreRegistroProductorJobHelper extends AbstractJobHelper {

    private static final String PARAM_JOB_ACTIVO = "job:generador-reporte-pre-registro";

    private static final String PARAM_JOB_NODO_ACTIVO = "job:generador-reporte-pre-registro:nodo";

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private ReportePreRegistroHelper reportePreRegistroHelper;

    @Override
    public void execute() {
        for (CultivoEnum c : CultivoEnum.values()) {
            for (CicloAgricolaEnum ca : CicloAgricolaEnum.values()) {
                try {
                    CicloAgricola ciclo = buscadorCatalogo.buscaElemento(CicloAgricola.class, ca.getClave());
                    Cultivo cultivo = buscadorCatalogo.buscaElemento(Cultivo.class, c.getClave());
                    reportePreRegistroHelper.genera(ciclo, cultivo);
                } catch (IOException ex) {
                    LOGGER.error("Error al generar el reporte Pre registro productor. {}", ex);
                }
            }
        }
    }

    @Override
    protected String getJobActivoParametro() {
        return PARAM_JOB_ACTIVO;
    }

    @Override
    protected String getJobNodoActivoParametro() {
        return PARAM_JOB_NODO_ACTIVO;
    }
}
