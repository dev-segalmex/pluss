/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.job.factura;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.factura.ProcesadorInscripcionFactura;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import mx.gob.segalmex.granos.core.historico.BuscadorHistoricoRegistro;
import mx.gob.segalmex.granos.core.historico.ParametrosHistoricoRegistro;
import mx.gob.segalmex.pluss.modelo.historico.HistoricoRegistro;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcionEnum;
import mx.gob.segalmex.pluss.cultivos.job.config.AbstractJobHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Slf4j
@Component
public class ValidadorFacturaJobHelper extends AbstractJobHelper {

    private static final String PARAM_JOB_ACTIVO = "job:validador-factura-sat";

    private static final String PARAM_JOB_NODO_ACTIVO = "job:validador-factura-sat:nodo";

    private static final int LIMITE = 10;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorHistoricoRegistro buscadorHistorico;

    @Autowired
    private ProcesadorInscripcionFactura procesadorInscripcion;

    @Override
    public void execute() {
        ParametrosHistoricoRegistro parametros = new ParametrosHistoricoRegistro();
        parametros.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.PENDIENTE.getClave()));
        parametros.setTipo(buscadorCatalogo.buscaElemento(TipoHistoricoInscripcion.class, TipoHistoricoInscripcionEnum.VALIDACION_SAT.getClave()));
        parametros.setMonitoreable(Boolean.TRUE);
        parametros.setClase(InscripcionFactura.class);

        List<HistoricoRegistro> historicos = buscadorHistorico.busca(parametros);
        LOGGER.info("Facturas pendientes de validación: {}", historicos.size());
        int index = 0;
        for (HistoricoRegistro h : historicos) {
            InscripcionFactura i = (InscripcionFactura) h.getEntidad();
            String folio = i.getFolio();
            try {
                LOGGER.info("Validando factura: {}", folio);
                procesadorInscripcion.verifica(i);
                if (++index > LIMITE) {
                    break;
                }
            } catch (Exception ouch) {
                LOGGER.error("NO fue posibiel validar la factura: {}", folio);
                LOGGER.error("Validando próxima factura.", ouch);
            }
        }
        LOGGER.info("Ejecutando job de verificación de facturas.");
    }

    @Override
    protected String getJobActivoParametro() {
        return PARAM_JOB_ACTIVO;
    }

    @Override
    protected String getJobNodoActivoParametro() {
        return PARAM_JOB_NODO_ACTIVO;
    }

}
