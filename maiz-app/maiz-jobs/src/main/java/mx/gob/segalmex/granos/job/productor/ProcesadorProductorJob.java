/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.job.productor;

import lombok.extern.slf4j.Slf4j;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 *
 * @author ismael
 */
@Slf4j
@DisallowConcurrentExecution
public class ProcesadorProductorJob extends QuartzJobBean {

    private ProcesadorProductorJobHelper helper;

    public void setHelper(ProcesadorProductorJobHelper helper) {
        this.helper = helper;
    }

    @Override
    protected void executeInternal(JobExecutionContext jec) throws JobExecutionException {
        helper.executeConditional("procesamiento de productores", LOGGER);
    }

}
