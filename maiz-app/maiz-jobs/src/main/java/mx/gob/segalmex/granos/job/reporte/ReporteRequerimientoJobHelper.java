/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.job.reporte;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.common.core.pago.ProcesadorRequerimientoPago;
import mx.gob.segalmex.common.core.pago.busqueda.BuscadorRequerimientoPago;
import mx.gob.segalmex.common.core.pago.busqueda.ParametrosRequerimientoPago;
import mx.gob.segalmex.pluss.modelo.pago.RequerimientoPago;
import mx.gob.segalmex.pluss.cultivos.job.config.AbstractJobHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Slf4j
@Component
public class ReporteRequerimientoJobHelper extends AbstractJobHelper {

    private static final String PARAM_JOB_ACTIVO = "job:generador-reporte-requerimiento-pago";

    private static final String PARAM_JOB_NODO_ACTIVO = "job:generador-reporte-requerimiento-pago:nodo";

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Autowired
    private BuscadorRequerimientoPago buscadorRequerimientoPago;

    @Value("${pluss.jobs.requerimiento.max.resultados}")
    private int maxResultados;

    @Autowired
    private ProcesadorRequerimientoPago procesadorRequerimientoPago;

    @Override
    public void execute() {
        ParametrosRequerimientoPago parametros = new ParametrosRequerimientoPago();
        parametros.setReportePendiente(Boolean.TRUE);
        parametros.setMaximoResultados(maxResultados);
        List<RequerimientoPago> requerimientos = buscadorRequerimientoPago.busca(parametros);
        LOGGER.info("Reportes requerimientos pago por generar: {}", requerimientos.size());
        for (RequerimientoPago r : requerimientos) {
            try {
                LOGGER.info("Generando reporte: {}", r.getId());
                procesadorRequerimientoPago.generaXlsx(r);
                LOGGER.info("Finalizado reporte: {}", r.getId());
            } catch (Exception ex) {
                LOGGER.error("Error al generar el reporte RequerimientoPago {}", r.getUuid());
                LOGGER.error("Error", ex);
            }
        }

    }

    @Override
    protected String getJobActivoParametro() {
        return PARAM_JOB_ACTIVO;
    }

    @Override
    protected String getJobNodoActivoParametro() {
        return PARAM_JOB_NODO_ACTIVO;
    }

}
