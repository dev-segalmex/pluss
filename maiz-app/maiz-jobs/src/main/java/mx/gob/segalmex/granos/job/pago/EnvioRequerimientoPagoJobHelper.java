/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.job.pago;

import java.util.Calendar;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.pago.ProcesadorRequerimientoPago;
import mx.gob.segalmex.common.core.pago.busqueda.BuscadorRequerimientoPago;
import mx.gob.segalmex.common.core.pago.busqueda.ParametrosRequerimientoPago;
import mx.gob.segalmex.pluss.modelo.pago.EstatusRequerimientoPagoEnum;
import mx.gob.segalmex.pluss.modelo.pago.RequerimientoPago;
import mx.gob.segalmex.pluss.cultivos.job.config.AbstractJobHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Slf4j
@Component
public class EnvioRequerimientoPagoJobHelper extends AbstractJobHelper {

    private static final String PARAM_JOB_ACTIVO = "job:envio-requerimiento-pago";

    private static final String PARAM_JOB_NODO_ACTIVO = "job:envio-requerimiento-pago:nodo";

    @Autowired
    private BuscadorRequerimientoPago buscadorRequerimientoPago;

    @Autowired
    private ProcesadorRequerimientoPago procesadorRequerimientoPago;

    @Value("${pluss.jobs.requerimiento.tiempo}")
    private int tiempo;

    @Override
    public void execute() {
        ParametrosRequerimientoPago params = new ParametrosRequerimientoPago();
        params.setMonitoreable(Boolean.TRUE);
        params.setEstatus(EstatusRequerimientoPagoEnum.APROBADO.getClave());

        List<RequerimientoPago> requerimientos = buscadorRequerimientoPago.busca(params);
        for (RequerimientoPago r : requerimientos) {
            Calendar inicio = Calendar.getInstance();
            inicio.add(Calendar.MINUTE, -tiempo);
            if (r.getFechaActualizacion().compareTo(inicio) > 0) {
                LOGGER.info("El requerimiento: {} aún no se puede enviar", r.getId());
                continue;
            }
            LOGGER.info("Enviando requerimiento de pago: {}", r.getId());
            try {
                procesadorRequerimientoPago.envia(r);
            } catch (Exception ouch) {
                LOGGER.error("No fue posible generar el formato JSON.", ouch);
            }
        }
    }

    @Override
    protected String getJobNodoActivoParametro() {
        return PARAM_JOB_NODO_ACTIVO;
    }

    @Override
    protected String getJobActivoParametro() {
        return PARAM_JOB_ACTIVO;
    }

}
