/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.job.factura;

import java.util.Comparator;
import java.util.Map;
import java.util.List;
import java.util.HashMap;
import java.util.Objects;
import java.util.Collections;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.factura.ProcesadorInscripcionFactura;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import mx.gob.segalmex.granos.core.historico.BuscadorHistoricoRegistro;
import mx.gob.segalmex.granos.core.historico.ParametrosHistoricoRegistro;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricolaEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.modelo.historico.HistoricoRegistro;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcionEnum;
import mx.gob.segalmex.pluss.cultivos.job.config.AbstractJobHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Slf4j
@Component
public class ProcesadorFacturaValidacionPositivaJobHelper extends AbstractJobHelper {

    private static final String PARAM_JOB_ACTIVO = "job:procesador-factura-validacion-positiva";

    private static final String PARAM_JOB_NODO_ACTIVO = "job:procesador-factura-validacion-positiva:nodo";

    private static final String PARAM_CULTIVO_CICLO_PAGO = "job:procesador-factura-validacion-positiva:cultivo-ciclo:";

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorHistoricoRegistro buscadorHistorico;

    @Autowired
    private ProcesadorInscripcionFactura procesadorInscripcionFactura;

    @Override
    public void execute() {
        for (Cultivo c : buscadorCatalogo.busca(Cultivo.class)) {
            for (CicloAgricola ca : buscadorCatalogo.busca(CicloAgricola.class)) {
                if (!isPagoActivo(c, ca)) {
                    continue;
                }
                ParametrosHistoricoRegistro parametros = new ParametrosHistoricoRegistro();
                parametros.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class,
                        EstatusInscripcionEnum.CORRECTO.getClave()));
                parametros.setTipo(buscadorCatalogo.buscaElemento(TipoHistoricoInscripcion.class,
                        TipoHistoricoInscripcionEnum.FINALIZACION.getClave()));
                parametros.setCultivo(buscadorCatalogo.buscaElemento(Cultivo.class, c.getClave()));
                parametros.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, ca.getClave()));
                parametros.setMonitoreable(Boolean.TRUE);
                parametros.setClase(InscripcionFactura.class);

                List<HistoricoRegistro> historicos = buscadorHistorico.busca(parametros);
                LOGGER.info("Procesando cultivo: {}, ciclo: {}, total: {}", c.getNombre(), ca.getClave(), historicos.size());
                if (historicos.isEmpty()) {
                    continue;
                }
                Collections.sort(historicos, Comparator.comparing(HistoricoRegistro::getFechaCreacion));
                try {
                    Map<String, String> map = new HashMap<>();
                    for (HistoricoRegistro h : historicos) {
                        InscripcionFactura i = (InscripcionFactura) h.getEntidad();
                        // Si está en el mapa, ya fue procesado previamente
                        if (Objects.nonNull(map.get(i.getFolio()))) {
                            continue;
                        }
                        // No procesamos las facturas globales de empresa (por el momento)
                        if (i.getTipoFactura().equals("global-empresa")) {
                            continue;
                        }
                        LOGGER.info("Finalizando registro de factura correcta: {}", i.getFolio());
                        procesadorInscripcionFactura.finalizaPositivo(i, h.getUsuarioRegistra());
                        // Agregamos el folio como procesado
                        map.put(i.getFolio(), i.getFolio());
                    }
                } catch (RuntimeException ouch) {
                    LOGGER.error("Ocurrió un error al procesar InscripcionFactura de cultivo {} y ciclo {}",
                            c.getNombre(), ca.getClave());
                    LOGGER.error("Error:", ouch);
                }
            }
        }
    }

    private boolean isPagoActivo(Cultivo cultivo, CicloAgricola ciclo) {
        Boolean isPagoActivo = buscadorParametro.getValorOpcional(PARAM_CULTIVO_CICLO_PAGO + cultivo.getClave()
                + ":" + ciclo.getClave(), Boolean.class);
        return Objects.nonNull(isPagoActivo) && isPagoActivo;
    }

    @Override
    protected String getJobActivoParametro() {
        return PARAM_JOB_ACTIVO;
    }

    @Override
    protected String getJobNodoActivoParametro() {
        return PARAM_JOB_NODO_ACTIVO;
    }

}
