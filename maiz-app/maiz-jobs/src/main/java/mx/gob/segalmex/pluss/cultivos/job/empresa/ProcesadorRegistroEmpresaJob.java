/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.job.empresa;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.UUID;
import java.util.stream.Collectors;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.pluss.modelo.empresas.RegistroEmpresa;
import mx.gob.segalmex.pluss.modelo.catalogos.Empresa;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 *`
 * @author ismael
 */
@DisallowConcurrentExecution
public class ProcesadorRegistroEmpresaJob extends QuartzJobBean {

    private BuscadorCatalogo buscadorCatalogo;

    @Override
    protected void executeInternal(JobExecutionContext jec) throws JobExecutionException {
       List<RegistroEmpresa> correctos = new ArrayList<>();

        Map<String, Empresa> empresas = buscadorCatalogo.busca(Empresa.class).stream()
                .collect(Collectors.toMap(Empresa::getRfc, e -> e));
        for (RegistroEmpresa re : correctos) {
            Empresa e = empresas.get(re.getRfc());
            if (Objects.isNull(e)) {
                // Registramos la empresa
                e = new Empresa();
                e.setNombre(re.getNombre());
                e.setClave(UUID.randomUUID().toString());
                e.setRfc(re.getRfc());
            }
        }
        // Buscamos si existe la empresa
        // Si no existe, la registramos
        // Si no tiene sucursales, generamos una sucursal del tipo especificado
        // Si tiene sucursales, creamos las sucursales
        // Marcamos como positivo el registro
    }
}
