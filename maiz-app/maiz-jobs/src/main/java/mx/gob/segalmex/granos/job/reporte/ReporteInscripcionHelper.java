/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.job.reporte;

import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInscripcionProductor;
import mx.gob.segalmex.granos.core.contrato.busqueda.ParametrosInscripcionContrato;
import mx.gob.segalmex.granos.core.contrato.busqueda.BuscadorInscripcionContrato;
import mx.gob.segalmex.granos.core.productor.xls.PrediosProductorPlaneacionXls;
import mx.gob.segalmex.granos.core.contrato.xls.InscripcionContratoXlsCreator;
import mx.gob.segalmex.granos.core.productor.xls.PagosProductorPlaneacionXls;
import mx.gob.segalmex.granos.core.productor.xls.InscripcionProductorXls;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.granos.core.productor.xls.ContratosProductorXls;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorUsoFactura;
import mx.gob.segalmex.granos.core.productor.xls.PrediosProductorXls;
import mx.gob.segalmex.granos.core.factura.xls.InscripcionFacturaXls;
import mx.gob.segalmex.pluss.modelo.inscripcion.InscripcionContrato;
import mx.gob.segalmex.pluss.modelo.catalogos.ReferenciaReporteEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.factura.EstatusUsoFacturaEnum;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFactura;
import org.springframework.transaction.annotation.Transactional;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.factura.InscripcionFacturaSimpleFactory;
import mx.gob.segalmex.common.core.factura.busqueda.BuscadorInscripcionFactura;
import mx.gob.segalmex.common.core.factura.busqueda.ParametrosInscripcionFactura;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.pluss.modelo.factura.InscripcionFacturaSimple;
import org.apache.commons.lang3.time.DateUtils;

/**
 *
 * @author oscar
 */
@Slf4j
public class ReporteInscripcionHelper {

    private BuscadorInscripcionProductor buscadorInscripcionProductor;

    private BuscadorInscripcionContrato buscadorInscripcionContrato;

    private BuscadorInscripcionFactura buscadorInscripcionFactura;

    private InscripcionProductorXls inscripcionProductorXls;

    private PrediosProductorXls prediosProductorXls;

    private ContratosProductorXls contratosProductorXls;

    private InscripcionFacturaXls inscripcionFacturaXls;

    private PrediosProductorPlaneacionXls prediosPlaneacionXls;

    private PagosProductorPlaneacionXls pagosPlaneacionXls;

    private BuscadorCatalogo buscadorCatalogo;

    private BuscadorUsoFactura buscadorUsoFactura;

    private ArchivoRelacionadoHelper archivoRelacionadoHelper;

    private RegistroEntidad registroEntidad;

    @Transactional
    public void generaReporte(CicloAgricola ciclo, Cultivo cultivo, String tipo) throws IOException {

        switch (tipo) {
            case "productores":
                ParametrosInscripcionProductor params = new ParametrosInscripcionProductor();
                params.setCultivo(cultivo);
                params.setCiclo(ciclo);
                params.setMaximoResultados(-1);
                List<InscripcionProductor> productores = buscadorInscripcionProductor.busca(params);
                byte[] rowProductores = inscripcionProductorXls.exporta(productores);
                InputStream inputProductores = new ByteArrayInputStream(rowProductores);
                archivoRelacionadoHelper.guardaArchivo(inputProductores, ciclo, cultivo, tipo,
                        ReferenciaReporteEnum.PRODUCTOR.getClave(), InscripcionProductor.class);
                generaProductorPredio(productores, ciclo, cultivo);
                generaProductorContrato(productores, ciclo, cultivo);
                generaPredioPlaneacion(ciclo, cultivo);
                generaPagosPlaneacion(ciclo, cultivo);
                break;
            case "contratos":
                if (cultivo.getClave().equals(CultivoEnum.ARROZ.getClave())) {
                    LOGGER.info("Arroz no genera reporte de contratos.");
                    break;
                }
                ParametrosInscripcionContrato pc = new ParametrosInscripcionContrato();
                pc.setCultivo(cultivo);
                pc.setCiclo(ciclo);
                List<InscripcionContrato> contratos = buscadorInscripcionContrato.busca(pc);
                InscripcionContratoXlsCreator xlsContratoCreator = new InscripcionContratoXlsCreator();
                byte[] rowContratos = xlsContratoCreator.exporta(contratos, cultivo);
                InputStream inputContratos = new ByteArrayInputStream(rowContratos);
                archivoRelacionadoHelper.guardaArchivo(inputContratos, ciclo, cultivo, tipo,
                        ReferenciaReporteEnum.CONTRATO.getClave(), InscripcionContrato.class);
                break;
            case "facturas":
                List<InscripcionFacturaSimple> facturas = getFacturasSimple(ciclo, cultivo);
                byte[] rowFacturas = inscripcionFacturaXls.exportaSimple(facturas);
                InputStream inputFacturas = new ByteArrayInputStream(rowFacturas);
                archivoRelacionadoHelper.guardaArchivo(inputFacturas, ciclo, cultivo, tipo,
                        ReferenciaReporteEnum.FACTURA.getClave(), InscripcionFactura.class);
                break;
            default:
                throw new IllegalArgumentException("El tipo de reporte no existe.");
        }

    }

    private List<InscripcionFacturaSimple> getFacturasSimple(CicloAgricola ciclo, Cultivo cultivo) {
        List<InscripcionFacturaSimple> facturasSimple = new ArrayList<>();
        Calendar fechaActual = DateUtils.truncate(Calendar.getInstance(), Calendar.DATE);
        Calendar fechaInicio = DateUtils.truncate(Calendar.getInstance(), Calendar.DATE);
        fechaActual.add(Calendar.DAY_OF_YEAR, 1);
        fechaInicio.set(Calendar.YEAR, 2020);
        fechaInicio.set(Calendar.MONTH, Calendar.JUNE);
        fechaInicio.set(Calendar.DAY_OF_MONTH, 24);
        Calendar fechaFin = (Calendar) fechaInicio.clone();
        fechaFin.add(Calendar.MONTH, 1);
        ParametrosInscripcionFactura pf = new ParametrosInscripcionFactura();
        pf.setCultivo(cultivo);
        pf.setCiclo(ciclo);
        boolean continuar = true;

        do {
            pf.setFechaInicio(fechaInicio);
            pf.setFechaFin(fechaFin);
            if (fechaFin.compareTo(fechaActual) >= 0) {
                pf.setFechaFin(fechaActual);
                continuar = false;
            }
            List<InscripcionFactura> facturas = buscadorInscripcionFactura.busca(pf);
            //agregamos los resultados a la entidad simple
            facturasSimple.addAll(InscripcionFacturaSimpleFactory.getInstance(facturas));
            //sumamos un mes a cada fecha
            fechaInicio.add(Calendar.MONTH, 1);
            fechaFin.add(Calendar.MONTH, 1);
            registroEntidad.clear();
        } while (continuar);

        return facturasSimple;
    }

    private void generaProductorPredio(List<InscripcionProductor> productores,
            CicloAgricola ciclo, Cultivo cultivo) throws IOException {
        byte[] rowPredios = prediosProductorXls.exporta(productores);
        InputStream inputPredios = new ByteArrayInputStream(rowPredios);
        archivoRelacionadoHelper.guardaArchivo(inputPredios, ciclo, cultivo, "predios",
                ReferenciaReporteEnum.PRODUCTOR_PREDIO.getClave(), InscripcionProductor.class);
    }

    private void generaProductorContrato(List<InscripcionProductor> productores,
            CicloAgricola ciclo, Cultivo cultivo) throws IOException {
        if (cultivo.getClave().equals(CultivoEnum.ARROZ.getClave())) {
            LOGGER.info("Arroz no genera reporte de productor contratos.");
            return;
        }
        byte[] rowContratos = contratosProductorXls.exporta(productores);
        InputStream inputContratos = new ByteArrayInputStream(rowContratos);
        archivoRelacionadoHelper.guardaArchivo(inputContratos, ciclo, cultivo, "productor-contratos",
                ReferenciaReporteEnum.PRODUCTOR_CONTRATO.getClave(), InscripcionProductor.class);
    }

    private void generaPredioPlaneacion(CicloAgricola ciclo, Cultivo cultivo) throws IOException {
        ParametrosInscripcionProductor params = new ParametrosInscripcionProductor();
        params.setCultivo(cultivo);
        params.setCiclo(ciclo);
        params.setMaximoResultados(-1);
        params.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.POSITIVA));
        List<InscripcionProductor> productores = buscadorInscripcionProductor.busca(params);
        byte[] rowPrediosPlaneacion = prediosPlaneacionXls.exporta(productores);
        InputStream inputPrediosPlaneacion = new ByteArrayInputStream(rowPrediosPlaneacion);
        archivoRelacionadoHelper.guardaArchivo(inputPrediosPlaneacion, ciclo, cultivo, "predios-planeacion",
                ReferenciaReporteEnum.PREDIO_PLANEACION.getClave(), InscripcionProductor.class);
    }

    private void generaPagosPlaneacion(CicloAgricola ciclo, Cultivo cultivo) throws IOException {
        List<Object[]> resultados = buscadorUsoFactura.buscaReportePlaneacion(ciclo, cultivo,
                EstatusUsoFacturaEnum.NUEVO, EstatusUsoFacturaEnum.SELECCIONADO,
                EstatusUsoFacturaEnum.EN_PROCESO, EstatusUsoFacturaEnum.PAGADO);
        byte[] rowPagosPlaneacion = pagosPlaneacionXls.exporta(resultados);
        InputStream inputPagosPlaneacion = new ByteArrayInputStream(rowPagosPlaneacion);
        archivoRelacionadoHelper.guardaArchivo(inputPagosPlaneacion, ciclo, cultivo, "pagos-planeacion",
                ReferenciaReporteEnum.PAGO_PLANEACION.getClave(), InscripcionProductor.class);
    }

    public void setBuscadorInscripcionProductor(BuscadorInscripcionProductor buscadorInscripcionProductor) {
        this.buscadorInscripcionProductor = buscadorInscripcionProductor;
    }

    public void setBuscadorInscripcionContrato(BuscadorInscripcionContrato buscadorInscripcionContrato) {
        this.buscadorInscripcionContrato = buscadorInscripcionContrato;
    }

    public void setBuscadorInscripcionFactura(BuscadorInscripcionFactura buscadorInscripcionFactura) {
        this.buscadorInscripcionFactura = buscadorInscripcionFactura;
    }

    public void setInscripcionProductorXls(InscripcionProductorXls inscripcionProductorXls) {
        this.inscripcionProductorXls = inscripcionProductorXls;
    }

    public void setPrediosProductorXls(PrediosProductorXls prediosProductorXls) {
        this.prediosProductorXls = prediosProductorXls;
    }

    public void setContratosProductorXls(ContratosProductorXls contratosProductorXls) {
        this.contratosProductorXls = contratosProductorXls;
    }

    public void setInscripcionFacturaXls(InscripcionFacturaXls inscripcionFacturaXls) {
        this.inscripcionFacturaXls = inscripcionFacturaXls;
    }

    public void setBuscadorCatalogo(BuscadorCatalogo buscadorCatalogo) {
        this.buscadorCatalogo = buscadorCatalogo;
    }

    public void setPrediosPlaneacionXls(PrediosProductorPlaneacionXls prediosPlaneacionXls) {
        this.prediosPlaneacionXls = prediosPlaneacionXls;
    }

    public void setPagosPlaneacionXls(PagosProductorPlaneacionXls pagosPlaneacionXls) {
        this.pagosPlaneacionXls = pagosPlaneacionXls;
    }

    public void setBuscadorUsoFactura(BuscadorUsoFactura buscadorUsoFactura) {
        this.buscadorUsoFactura = buscadorUsoFactura;
    }

    public void setArchivoRelacionadoHelper(ArchivoRelacionadoHelper archivoRelacionadoHelper) {
        this.archivoRelacionadoHelper = archivoRelacionadoHelper;
    }

    public void setRegistroEntidad(RegistroEntidad registroEntidad) {
        this.registroEntidad = registroEntidad;
    }
}
