/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.job.reporte;

import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import mx.gob.segalmex.common.core.archivos.ArchivoUtils;
import mx.gob.segalmex.common.core.archivos.RegistroArchivoRelacionado;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.util.CultivoClaveUtil;
import mx.gob.segalmex.common.core.util.SegalmexDateUtils;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import mx.gob.segalmex.pluss.modelo.archivos.ArchivoRelacionado;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;

/**
 *
 * @author oscar
 */
public class ArchivoRelacionadoHelper {

    private static final String CLAVE_USUARIO = "anonimo@segalmex.gob.mx";

    private RegistroArchivoRelacionado registroArchivoRelacionado;

    private BuscadorCatalogo buscadorCatalogo;

    public void guardaArchivo(InputStream is, CicloAgricola ca, Cultivo c,
            String tipo, String referencia, Class clase) throws IOException {
        ArchivoRelacionado ar = new ArchivoRelacionado();
        Archivo archivo = getArchivo(ca, c, tipo);
        ar.setArchivo(archivo);
        ar.setUsuarioRegistra(buscadorCatalogo.buscaElemento(Usuario.class, CLAVE_USUARIO));
        ar.setClase(clase.getSimpleName());
        ar.setReferencia(referencia);
        ar.setTipo(getTipo(ca, c, tipo));

        registroArchivoRelacionado.registra(archivo, ar, is);
    }

    private Archivo getArchivo(CicloAgricola ca, Cultivo c, String tipo) {
        String fecha = SegalmexDateUtils.format(Calendar.getInstance(), "yyyMMdd");
        String nombre = "registros-" + tipo + "-" + CultivoClaveUtil.getClaveCorta(c)
                + "-" + ca.getClave() + "-" + fecha + ".xlsx";
        Archivo archivo = ArchivoUtils.getInstance(nombre,
                CultivoClaveUtil.getClaveCorta(c), "/" + ca.getClave()
                + "/reportes/" + fecha, false);
        return archivo;
    }

    private String getTipo(CicloAgricola ca, Cultivo c, String tipo) {
        return "registro-" + tipo + "-" + c.getClave() + "-" + ca.getClave() + "-completo";
    }

    public void setRegistroArchivoRelacionado(RegistroArchivoRelacionado registroArchivoRelacionado) {
        this.registroArchivoRelacionado = registroArchivoRelacionado;
    }

    public void setBuscadorCatalogo(BuscadorCatalogo buscadorCatalogo) {
        this.buscadorCatalogo = buscadorCatalogo;
    }
}
