/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.job.productor;

import java.math.BigDecimal;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.granos.core.productor.ProcesadorProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricolaEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;
import mx.gob.segalmex.pluss.cultivos.job.config.AbstractJobHelper;
import org.springframework.stereotype.Component;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author jurgen
 */
@Slf4j
@Component
public class ProcesadorProductorCicloGrupoJobHelper extends AbstractJobHelper {

    private static final String PARAM_JOB_ACTIVO = "job:procesador-productor-ciclo-grupo";

    private static final String PARAM_JOB_NODO_ACTIVO = "job:procesador-productor-ciclo-grupo:nodo";

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorProductor buscadorProductor;

    @Autowired
    private ProcesadorProductor procesadorProductor;

    @Override
    public void execute() {

        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setCultivo(buscadorCatalogo.buscaElemento(Cultivo.class, CultivoEnum.TRIGO.getClave()));
        parametros.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, CicloAgricolaEnum.OI2021.getClave()));
        parametros.setActivo(true);
        List<ProductorCiclo> productoresCiclo = buscadorProductor.buscaProductorCiclo(parametros);

        int i = 0;
        for (ProductorCiclo pc : productoresCiclo) {
            i++;
            if (pc.getToneladasTotales().compareTo(BigDecimal.ZERO) > 0) {
                LOGGER.info("Generando {}. ProductorCicloGrupo de productor ciclo {}", i, pc.getFolio());
                procesadorProductor.procesaGrupos(pc);
            } else {
                LOGGER.warn("Omitiendo {}. ProductorCicloGrupo de productor ciclo: {}", i, pc.getFolio());
            }
        }
    }

    @Override
    protected String getJobActivoParametro() {
        return PARAM_JOB_ACTIVO;
    }

    @Override
    protected String getJobNodoActivoParametro() {
        return PARAM_JOB_NODO_ACTIVO;
    }


}
