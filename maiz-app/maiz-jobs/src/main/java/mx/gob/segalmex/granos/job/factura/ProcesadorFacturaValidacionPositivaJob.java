/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.job.factura;

import lombok.extern.slf4j.Slf4j;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.quartz.DisallowConcurrentExecution;
import mx.gob.segalmex.pluss.cultivos.job.config.JobHelper;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 *
 * @author ismael
 */
@Slf4j
@DisallowConcurrentExecution
public class ProcesadorFacturaValidacionPositivaJob extends QuartzJobBean {

    private JobHelper helper;

    public void setHelper(JobHelper helper) {
        this.helper = helper;
    }

    @Override
    protected void executeInternal(JobExecutionContext jec) throws JobExecutionException {
        helper.executeConditional("procesamiento de facturas positivas", LOGGER);
    }

}
