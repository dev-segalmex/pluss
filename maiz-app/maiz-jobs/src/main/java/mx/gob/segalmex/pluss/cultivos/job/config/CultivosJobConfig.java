/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.job.config;

import java.util.HashMap;
import java.util.Map;
import mx.gob.segalmex.granos.job.documento.ValidadorPredioDocumentoJob;
import mx.gob.segalmex.granos.job.documento.ValidadorPredioDocumentoJobHelper;
import mx.gob.segalmex.granos.job.factura.ProcesadorFacturaValidacionPositivaJob;
import mx.gob.segalmex.granos.job.factura.ProcesadorFacturaValidacionPositivaJobHelper;
import mx.gob.segalmex.granos.job.factura.ValidadorFacturaJob;
import mx.gob.segalmex.granos.job.factura.ValidadorFacturaJobHelper;
import mx.gob.segalmex.granos.job.pago.EnvioRequerimientoPagoJob;
import mx.gob.segalmex.granos.job.pago.EnvioRequerimientoPagoJobHelper;
import mx.gob.segalmex.granos.job.productor.ProcesadorProductorCicloGrupoJob;
import mx.gob.segalmex.granos.job.productor.ProcesadorProductorCicloGrupoJobHelper;
import mx.gob.segalmex.granos.job.productor.ProcesadorProductorJob;
import mx.gob.segalmex.granos.job.productor.ProcesadorProductorJobHelper;
import mx.gob.segalmex.granos.job.productor.VerificadorProductorJob;
import mx.gob.segalmex.granos.job.productor.VerificadorProductorJobHelper;
import mx.gob.segalmex.granos.job.reporte.GeneradorReporteInscripcionContratoJob;
import mx.gob.segalmex.granos.job.reporte.GeneradorReporteInscripcionContratoJobHelper;
import mx.gob.segalmex.granos.job.reporte.GeneradorReporteInscripcionFacturaJob;
import mx.gob.segalmex.granos.job.reporte.GeneradorReporteInscripcionFacturaJobHelper;
import mx.gob.segalmex.granos.job.reporte.GeneradorReporteInscripcionProductorJob;
import mx.gob.segalmex.granos.job.reporte.GeneradorReporteInscripcionProductorJobHelper;
import mx.gob.segalmex.granos.job.reporte.GeneradorReportePreRegistroProductorJob;
import mx.gob.segalmex.granos.job.reporte.GeneradorReportePreRegistroProductorJobHelper;
import mx.gob.segalmex.granos.job.reporte.GeneradorReporteRequerimientoPagoJob;
import mx.gob.segalmex.granos.job.reporte.ReporteRequerimientoJobHelper;
import org.quartz.Job;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.scheduling.quartz.JobDetailFactoryBean;

/**
 *
 * @author ismael
 */
@Configuration
@ImportResource({
    "classpath:maizPersistenceApplicationContext.xml",
    "classpath:datoCapturadoApplicationContext.xml",
    "/WEB-INF/applicationContext.xml",
    "/WEB-INF/requiredApplicationContext.xml"
})
@ComponentScans({
    @ComponentScan("mx.gob.segalmex.granos.job"),
    @ComponentScan("mx.gob.segalmex.common.core.archivos"),
    @ComponentScan("mx.gob.segalmex.common.core.catalogos"),
    @ComponentScan("mx.gob.segalmex.common.core.estimulo.busqueda"),
    @ComponentScan("mx.gob.segalmex.common.core.factura"),
    @ComponentScan("mx.gob.segalmex.common.core.folio"),
    @ComponentScan("mx.gob.segalmex.common.core.persistencia"),
    @ComponentScan("mx.gob.segalmex.common.core.persona"),
    @ComponentScan("mx.gob.segalmex.granos.core.contrato"),
    @ComponentScan("mx.gob.segalmex.granos.core.historico"),
    @ComponentScan("mx.gob.segalmex.granos.core.inscripcion"),
    @ComponentScan("mx.gob.segalmex.granos.core.productor"),
    @ComponentScan("mx.gob.segalmex.granos.core.factura"),
    @ComponentScan("mx.gob.segalmex.common.core.validador"),
    @ComponentScan("mx.gob.segalmex.common.core.pago"),
    @ComponentScan("mx.gob.segalmex.granos.core.rfc"),
    @ComponentScan("mx.gob.segalmex.granos.core.requerimiento")
})
@PropertySources({
    @PropertySource("/WEB-INF/pluss.properties"),
    @PropertySource("/WEB-INF/cultivos-jobs.properties")
})
public class CultivosJobConfig {

    @Value("${pluss.jobs.pago.url}")
    private String urlPago;

    @Value("${pluss.jobs.pago.cronExpression}")
    private String pagoCronExpression;

    @Autowired
    private ProcesadorProductorJobHelper procesadorProductorJobHelper;

    @Autowired
    private EnvioRequerimientoPagoJobHelper envioRequerimientoPagoJobHelper;

    @Autowired
    private ReporteRequerimientoJobHelper reporteRequerimientoJobHelper;

    @Autowired
    private ProcesadorProductorCicloGrupoJobHelper procesadorProductorCicloGrupoJobHelper;

    @Autowired
    private VerificadorProductorJobHelper verificadorProductorJobHelper;

    @Autowired
    private ValidadorPredioDocumentoJobHelper validadorPredioDocumentoJobHelper;

    @Autowired
    private ValidadorFacturaJobHelper validadorFacturaJobHelper;

    @Autowired
    private ProcesadorFacturaValidacionPositivaJobHelper procesadorFacturaValidacionPositivaJobHelper;

    @Autowired
    private GeneradorReporteInscripcionContratoJobHelper generadorReporteInscripcionContratoJobHelper;

    @Autowired
    private GeneradorReporteInscripcionFacturaJobHelper generadorReporteInscripcionFacturaJobHelper;

    @Autowired
    private GeneradorReporteInscripcionProductorJobHelper generadorReporteInscripcionProductorJobHelper;

    @Autowired
    private GeneradorReportePreRegistroProductorJobHelper generadorReportePreRegistroProductorJobHelper;

    @Bean
    public JobDetailFactoryBean procesadorProductorJobDetail() {
        return getInstance(ProcesadorProductorJob.class, procesadorProductorJobHelper);
    }

    @Bean
    public CronTriggerFactoryBean procesadorProductorTrigger() {
        return getInstance(procesadorProductorJobDetail(), "0 0/5 * * * ?");
    }

    @Bean
    public JobDetailFactoryBean envioRequerimientoPagoJobDetail() {
        return getInstance(EnvioRequerimientoPagoJob.class, envioRequerimientoPagoJobHelper);
    }

    @Bean
    public CronTriggerFactoryBean envioRequerimientoPagoTrigger() {
        return getInstance(envioRequerimientoPagoJobDetail(), pagoCronExpression);
    }

    @Bean
    public JobDetailFactoryBean generadorReporteRequerimientoPagoJobDetail() {
        return getInstance(GeneradorReporteRequerimientoPagoJob.class, reporteRequerimientoJobHelper);
    }

    @Bean
    public CronTriggerFactoryBean generadorReporteRequerimientoPagoTrigger() {
        return getInstance(generadorReporteRequerimientoPagoJobDetail(), "50 0/5 * * * ?");
    }

    @Bean
    public JobDetailFactoryBean procesadorProductorCicloGrupoJobDetail() {
        return getInstance(ProcesadorProductorCicloGrupoJob.class, procesadorProductorCicloGrupoJobHelper);
    }

    @Bean
    public CronTriggerFactoryBean procesadorProductorCicloGrupoTrigger() {
        return getInstance(procesadorProductorCicloGrupoJobDetail(), "20 25 20 1/1 * ? *");
    }

    @Bean
    public JobDetailFactoryBean verificadorProductorJobDetail() {
        return getInstance(VerificadorProductorJob.class, verificadorProductorJobHelper);
    }

    @Bean
    public CronTriggerFactoryBean verificadorProductorTrigger() {
        return getInstance(verificadorProductorJobDetail(), "0 0 3 1/1 * ? *");
    }

    @Bean
    public JobDetailFactoryBean validadorPredioDocumentoJobDetail() {
        return getInstance(ValidadorPredioDocumentoJob.class, validadorPredioDocumentoJobHelper);
    }

    @Bean
    public CronTriggerFactoryBean validadorPredioDocumentoTrigger() {
        return getInstance(validadorPredioDocumentoJobDetail(), "0 0/5 * * * ?");
    }

    @Bean
    public JobDetailFactoryBean validadorFacturaJobDetail() {
        return getInstance(ValidadorFacturaJob.class, validadorFacturaJobHelper);
    }

    @Bean
    public CronTriggerFactoryBean validadorFacturaTrigger() {
        return getInstance(validadorFacturaJobDetail(), "20 0/3 * * * ?");
    }

    @Bean
    public JobDetailFactoryBean procesadorFacturaValidacionPositivaJobDetail() {
        return getInstance(ProcesadorFacturaValidacionPositivaJob.class, procesadorFacturaValidacionPositivaJobHelper);
    }

    @Bean
    public CronTriggerFactoryBean procesadorFacturaValidacionPositivaTrigger() {
        return getInstance(procesadorFacturaValidacionPositivaJobDetail(), "15,45 * * * * ?");
    }

    @Bean
    public JobDetailFactoryBean generadorReporteInscripcionContratoJobDetail() {
        return getInstance(GeneradorReporteInscripcionContratoJob.class, generadorReporteInscripcionContratoJobHelper);
    }

    @Bean
    public CronTriggerFactoryBean generadorReporteInscripcionContratoTrigger() {
        return getInstance(generadorReporteInscripcionContratoJobDetail(), "0 15 8,11,14,17,20 * * ?");
    }

    @Bean
    public JobDetailFactoryBean generadorReporteInscripcionFacturaJobDetail() {
        return getInstance(GeneradorReporteInscripcionFacturaJob.class, generadorReporteInscripcionFacturaJobHelper);
    }

    @Bean
    public CronTriggerFactoryBean generadorReporteInscripcionFacturaTrigger() {
        return getInstance(generadorReporteInscripcionFacturaJobDetail(), "0 25 8,11,14,17,20 * * ?");
    }

    @Bean
    public JobDetailFactoryBean generadorReporteInscripcionProductorJobDetail() {
        return getInstance(GeneradorReporteInscripcionProductorJob.class, generadorReporteInscripcionProductorJobHelper);
    }

    @Bean
    public CronTriggerFactoryBean generadorReporteInscripcionProductorTrigger() {
        return getInstance(generadorReporteInscripcionProductorJobDetail(), "0 5 8,11,14,17,20 * * ?");
    }

    @Bean
    public JobDetailFactoryBean generadorReportePreRegistroProductorJobDetail() {
        return getInstance(GeneradorReportePreRegistroProductorJob.class, generadorReportePreRegistroProductorJobHelper);
    }

    @Bean
    public CronTriggerFactoryBean generadorReportePreRegistroProductorTrigger() {
        return getInstance(generadorReportePreRegistroProductorJobDetail(), "0 35 8,11,14,17,20 * * ?");
    }

    private JobDetailFactoryBean getInstance(Class<? extends Job> clase, JobHelper helper) {
        JobDetailFactoryBean factory = new JobDetailFactoryBean();
        factory.setJobClass(clase);
        Map<String, Object> map = new HashMap<>();
        map.put("helper", helper);
        factory.setJobDataAsMap(map);
        return factory;
    }

    private CronTriggerFactoryBean getInstance(JobDetailFactoryBean factory, String expresion) {
        CronTriggerFactoryBean trigger = new CronTriggerFactoryBean();
        trigger.setJobDetail(factory.getObject());
        trigger.setCronExpression(expresion);
        return trigger;
    }
}
