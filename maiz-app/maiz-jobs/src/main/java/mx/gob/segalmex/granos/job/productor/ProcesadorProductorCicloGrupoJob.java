/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.job.productor;

import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.pluss.cultivos.job.config.JobHelper;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 *
 * @author jurgen
 */
@Slf4j
@DisallowConcurrentExecution
public class ProcesadorProductorCicloGrupoJob extends QuartzJobBean {

    private JobHelper helper;

    public void setHelper(ProcesadorProductorCicloGrupoJobHelper helper) {
        this.helper = helper;
    }

    @Override
    protected void executeInternal(JobExecutionContext jec) throws JobExecutionException {
        helper.executeConditional("actualización de productor-ciclo-grupo de trigo oi-2021", LOGGER);
    }

}
