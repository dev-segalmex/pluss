/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.job.config;

import java.util.Objects;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import org.slf4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;

/**
 *
 * @author ismael
 */
public abstract class AbstractJobHelper implements JobHelper {

    @Autowired
    protected Environment environment;

    @Autowired
    protected BuscadorParametro buscadorParametro;

    @Override
    public final boolean executeConditional(String nombre, Logger logger) {
        if (!isActivo()) {
            return false;
        }
        if (!isNodoActivo()) {
            return false;
        }

        logger.info("Ejecutando job de {}", nombre);
        execute();
        logger.info("Finalizando job de {}", nombre);
        return true;
    }

    private boolean isNodoActivo() {
        // Obtenemos el nombre de la propiedad (jboss.node.name) de los parámetros
        String property = buscadorParametro.getValorOpcional("job:nodo:property:name");
        if (Objects.isNull(property)) {
            return false;
        }

        // Si no podemos determinar el nodo actual, entonces no está activo
        String nodoActual = environment.getProperty(property);
        if (Objects.isNull(nodoActual)) {
            return false;
        }

        // Si no podemos determinar cual debería ser el nodo activo, entonces no está activo
        String nodoActivo = buscadorParametro.getValorOpcional(getJobNodoActivoParametro());
        if (Objects.isNull(nodoActivo)) {
            return false;
        }

        // Comparamos el nodo actual con el nodo activo
        return nodoActual.equals(nodoActivo);
    }

    private boolean isActivo() {
        Boolean isActivo = buscadorParametro.getValorOpcional(getJobActivoParametro(), Boolean.class);
        return Objects.nonNull(isActivo) && isActivo;
    }

    protected abstract String getJobNodoActivoParametro();

    protected abstract String getJobActivoParametro();
}
