/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.cultivos.job.config;

import org.slf4j.Logger;

/**
 *
 * @author ismael
 */
public interface JobHelper {

    boolean executeConditional(String name, Logger logger);

    void execute();

}
