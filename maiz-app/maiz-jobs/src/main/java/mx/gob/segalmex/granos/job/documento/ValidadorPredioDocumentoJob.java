/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.job.documento;

import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.pluss.cultivos.job.config.JobHelper;
import org.quartz.DisallowConcurrentExecution;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;

/**
 *
 * @author oscar
 */
@Slf4j
@DisallowConcurrentExecution
public class ValidadorPredioDocumentoJob extends QuartzJobBean {

    private JobHelper helper;

    public void setHelper(JobHelper helper) {
        this.helper = helper;
    }

    @Override
    protected void executeInternal(JobExecutionContext jec) throws JobExecutionException {
        helper.executeConditional("validacion de predios documento", LOGGER);
    }
}
