/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.job.reporte;

import java.io.IOException;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricolaEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.cultivos.job.config.AbstractJobHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author jurgen
 */
@Slf4j
@Component
public class GeneradorReporteInscripcionContratoJobHelper extends AbstractJobHelper {

    private static final String PARAM_JOB_ACTIVO = "job:generador-reporte-contrato";

    private static final String PARAM_JOB_NODO_ACTIVO = "job:generador-reporte-contrato:nodo";

    @Autowired
    private ReporteInscripcionHelper reporteInscripcionHelper;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Override
    public void execute() {
        for (CultivoEnum c : CultivoEnum.values()) {
            for (CicloAgricolaEnum ca : CicloAgricolaEnum.values()) {
                try {
                    CicloAgricola ciclo = buscadorCatalogo.buscaElemento(CicloAgricola.class, ca.getClave());
                    Cultivo cultivo = buscadorCatalogo.buscaElemento(Cultivo.class, c.getClave());
                    reporteInscripcionHelper.generaReporte(ciclo, cultivo, "contratos");
                } catch (IOException ex) {
                    LOGGER.error("Error al generar el reporte InscripcionContrato. {}", ex);
                }
            }
        }
    }

    @Override
    protected String getJobActivoParametro() {
        return PARAM_JOB_ACTIVO;
    }

    @Override
    protected String getJobNodoActivoParametro() {
        return PARAM_JOB_NODO_ACTIVO;
    }

}
