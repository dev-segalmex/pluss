/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.job.reporte;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorPreRegistroProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.xls.PreRegistroProductorXls;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.ReferenciaReporteEnum;
import mx.gob.segalmex.pluss.modelo.productor.PreRegistroProductor;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author oscar
 */
public class ReportePreRegistroHelper {

    private BuscadorPreRegistroProductor buscadorPreRegistro;

    private PreRegistroProductorXls preRegistroProductorXls;

    private ArchivoRelacionadoHelper archivoRelacionadoHelper;

    @Transactional
    public void genera(CicloAgricola ciclo, Cultivo cultivo) throws IOException {
        ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();
        parametros.setCultivo(cultivo);
        parametros.setCiclo(ciclo);
        List<PreRegistroProductor> registros = buscadorPreRegistro.busca(parametros);
        byte[] row = preRegistroProductorXls.exporta(registros);
        InputStream inputContratos = new ByteArrayInputStream(row);
        archivoRelacionadoHelper.guardaArchivo(inputContratos, ciclo, cultivo, "pre-registros",
                ReferenciaReporteEnum.PRE_REGISTRO.getClave(), PreRegistroProductor.class);
    }

    public void setBuscadorPreRegistro(BuscadorPreRegistroProductor buscadorPreRegistro) {
        this.buscadorPreRegistro = buscadorPreRegistro;
    }

    public void setPreRegistroProductorXls(PreRegistroProductorXls preRegistroProductorXls) {
        this.preRegistroProductorXls = preRegistroProductorXls;
    }

    public void setArchivoRelacionadoHelper(ArchivoRelacionadoHelper archivoRelacionadoHelper) {
        this.archivoRelacionadoHelper = archivoRelacionadoHelper;
    }

}
