/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.job.productor;

import java.util.Calendar;
import java.util.Collections;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.granos.core.historico.BuscadorHistoricoRegistro;
import mx.gob.segalmex.granos.core.historico.ParametrosHistoricoRegistro;
import mx.gob.segalmex.granos.core.productor.ProcesadorProductor;
import mx.gob.segalmex.pluss.modelo.historico.HistoricoRegistro;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.EstatusInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcion;
import mx.gob.segalmex.pluss.modelo.inscripcion.TipoHistoricoInscripcionEnum;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.cultivos.job.config.AbstractJobHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Slf4j
@Component
public class ProcesadorProductorJobHelper extends AbstractJobHelper {

    private static final String PARAM_JOB_ACTIVO = "job:procesador-productor";

    private static final String PARAM_JOB_NODO_ACTIVO = "job:procesador-productor:nodo";

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Autowired
    private BuscadorHistoricoRegistro buscadorHistorico;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private ProcesadorProductor procesadorProductor;

    @Value("${pluss.jobs.productor.tiempo}")
    private int tiempo;

    @Override
    public void execute() {
        ParametrosHistoricoRegistro parametros = new ParametrosHistoricoRegistro();
        parametros.setEstatus(buscadorCatalogo.buscaElemento(EstatusInscripcion.class, EstatusInscripcionEnum.CORRECTO.getClave()));
        parametros.setTipo(buscadorCatalogo.buscaElemento(TipoHistoricoInscripcion.class, TipoHistoricoInscripcionEnum.FINALIZACION.getClave()));
        parametros.setMonitoreable(Boolean.TRUE);
        parametros.setClase(InscripcionProductor.class);

        List<HistoricoRegistro> historicos = buscadorHistorico.busca(parametros);
        Collections.sort(historicos, (o1, o2) -> {
            return o1.getFechaCreacion().compareTo(o2.getFechaCreacion());
        });

        Calendar inicio = Calendar.getInstance();
        inicio.add(Calendar.SECOND, -tiempo);
        for (HistoricoRegistro h : historicos) {
            if (h.getFechaCreacion().compareTo(inicio) > 0) {
                LOGGER.debug("El registro {} aún no se procesa.", ((InscripcionProductor) h.getEntidad()).getFolio());
                continue;
            }

            InscripcionProductor i = (InscripcionProductor) h.getEntidad();
            String folio = i.getFolio();
            try {
                LOGGER.info("Procesando inscripcion: {}", folio);
                procesadorProductor.procesa(h);
            } catch (Exception ouch) {
                LOGGER.error("NO fue posible procesar la inscripcion del productor: {}", folio);
                LOGGER.error("Procesando siguiente inscripcion.", ouch);
            }
        }
    }

    @Override
    protected String getJobActivoParametro() {
        return PARAM_JOB_ACTIVO;
    }

    @Override
    protected String getJobNodoActivoParametro() {
        return PARAM_JOB_NODO_ACTIVO;
    }

}
