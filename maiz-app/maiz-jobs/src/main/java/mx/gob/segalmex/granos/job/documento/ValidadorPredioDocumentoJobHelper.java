package mx.gob.segalmex.granos.job.documento;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.granos.core.productor.ProcesadorPredioDocumento;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorPredioDocumento;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosPredioDocumento;
import mx.gob.segalmex.pluss.modelo.productor.PredioDocumento;
import mx.gob.segalmex.pluss.cultivos.job.config.AbstractJobHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Slf4j
@Component
public class ValidadorPredioDocumentoJobHelper extends AbstractJobHelper {

    private static final String PARAM_JOB_ACTIVO = "job:validador-predio-documento";

    private static final String PARAM_JOB_NODO_ACTIVO = "job:validador-predio-documento:nodo";

    private static final int LIMITE = 500;

    @Autowired
    private BuscadorPredioDocumento buscadorPredioDocumento;
    @Autowired
    private ProcesadorPredioDocumento procesadorPredioDocumento;

    @Override
    public void execute() {
        ParametrosPredioDocumento params = new ParametrosPredioDocumento();
        params.setMonitoreable(Boolean.TRUE);
        List<PredioDocumento> prediosDocumento = buscadorPredioDocumento.busca(params);
        LOGGER.info("Predios documentos por validar: {}.", prediosDocumento.size());
        int index = 0;
        for (PredioDocumento pd : prediosDocumento) {
            try {
                LOGGER.info("Validando predio documento: {}", pd.getId());
                procesadorPredioDocumento.verifica(pd);
                if (++index > LIMITE) {
                    break;
                }
            } catch (Exception ouch) {
                LOGGER.error("NO fue posible validar el predio documento: {}", pd.getId());
                LOGGER.error("Validando próximo documento.", ouch);
            }
        }
        LOGGER.info("Finalizando job de validación de predios documento.");
    }

    @Override
    protected String getJobActivoParametro() {
        return PARAM_JOB_ACTIVO;
    }

    @Override
    protected String getJobNodoActivoParametro() {
        return PARAM_JOB_NODO_ACTIVO;
    }

}
