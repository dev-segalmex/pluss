/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.granos.job.productor;

import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorParametro;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersona;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersonaEnum;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorInscripcionProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.BuscadorProductor;
import mx.gob.segalmex.granos.core.productor.busqueda.ParametrosInscripcionProductor;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricola;
import mx.gob.segalmex.pluss.modelo.catalogos.CicloAgricolaEnum;
import mx.gob.segalmex.pluss.modelo.catalogos.Cultivo;
import mx.gob.segalmex.pluss.modelo.catalogos.CultivoEnum;
import mx.gob.segalmex.pluss.modelo.productor.InscripcionProductor;
import mx.gob.segalmex.pluss.modelo.productor.ProductorCiclo;
import mx.gob.segalmex.pluss.cultivos.job.config.AbstractJobHelper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author erikcamacho
 */
@Slf4j
@Component
public class VerificadorProductorJobHelper extends AbstractJobHelper {

    private static final String PARAM_JOB_ACTIVO = "job:verificador-productor";

    private static final String PARAM_JOB_NODO_ACTIVO = "job:verificador-productor";

    @Autowired
    private BuscadorParametro buscadorParametro;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private BuscadorProductor buscadorProductor;

    @Autowired
    private BuscadorInscripcionProductor buscadorInscripcionProductor;

    @Override
    public void execute() {
        for (CultivoEnum c : CultivoEnum.values()) {
            for (CicloAgricolaEnum ca : CicloAgricolaEnum.values()) {
                ParametrosInscripcionProductor parametros = new ParametrosInscripcionProductor();

                parametros.setCultivo(buscadorCatalogo.buscaElemento(Cultivo.class, c.getClave()));
                parametros.setCiclo(buscadorCatalogo.buscaElemento(CicloAgricola.class, ca.getClave()));
                parametros.setActivo(true);
                parametros.setTipoPersona(buscadorCatalogo.buscaElemento(TipoPersona.class,
                        TipoPersonaEnum.FISICA.getClave()));

                List<ProductorCiclo> productoresCiclo = buscadorProductor.buscaProductorCiclo(parametros);

                LOGGER.info("Cultivo {} ciclo {}", parametros.getCultivo().getNombre(), parametros.getCiclo().getNombre());

                for (ProductorCiclo pc : productoresCiclo) {
                    parametros.setFolio(pc.getFolio());
                    InscripcionProductor inscripcion = buscadorInscripcionProductor.buscaElemento(parametros);
                    if (!pc.getProductor().getRfc().equals(inscripcion.getDatosProductor().getRfc())) {
                        LOGGER.warn("RFC inscripción {}, productor: {} , inscripción: {}", inscripcion.getFolio(), pc.getProductor().getRfc(), inscripcion.getDatosProductor().getRfc());
                    }
                    if (!pc.getProductor().getCurp().equals(inscripcion.getDatosProductor().getCurp())) {
                        LOGGER.warn("CURP inscripción {}, productor: {} , inscripción: {}", inscripcion.getFolio(), pc.getProductor().getCurp(), inscripcion.getDatosProductor().getCurp());
                    }
                }
            }
        }

    }

    @Override
    protected String getJobActivoParametro() {
        return PARAM_JOB_ACTIVO;
    }

    @Override
    protected String getJobNodoActivoParametro() {
        return PARAM_JOB_NODO_ACTIVO;
    }
}
