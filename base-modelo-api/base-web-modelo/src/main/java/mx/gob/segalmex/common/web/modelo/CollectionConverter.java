/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import mx.gob.segalmex.pluss.modelo.base.Entidad;

/**
 * Clase utilitaria con los métodos para la conversión de colecciones de objetos en colecciones de
 * converters que representen a esos objetos y viceversa.
 *
 * @author guillermo
 * @author ismael
 */
public class CollectionConverter {

    /**
     * Constructor vacío y protegido por ser clase utilitaria.
     */
    protected CollectionConverter() {
    }

    /**
     * Convierte una colección de elementos en una lista de converters que los representan.
     *
     * @param <T> el tipo de clase que extiende de Converter.
     * @param <E> el tipo de clase que extiende de Entidad.
     * @param claseConverter la clase de los objetos de a convertir.
     * @param entidades la colección de elementos a convertir.
     * @param expandLevel el nivel de extensión que deberán tener los converters.
     *
     * @return una lista de converters de los elementos dados con el nivel de extensión indicado.
     */
    public static <T extends Converter, E extends Entidad> List<T> convert(Class<T> claseConverter,
            Collection<E> entidades, int expandLevel) {
        List<T> converters = new ArrayList<>();
        if (entidades.isEmpty()) {
            return converters;
        }

        try {
            E e = entidades.iterator().next();
            Constructor<T> constructor = claseConverter
                    .getConstructor(new Class[]{e.getClass(), int.class});
            for (E entidad : entidades) {
                converters.add(constructor.newInstance(entidad, expandLevel));
            }
        } catch (NoSuchMethodException | InstantiationException
                | IllegalAccessException | InvocationTargetException ouch) {
            throw new IllegalArgumentException("Error al convertir la lista de entidades.", ouch);
        }
        return converters;
    }

    /**
     * Convierte una colección de converters en una lista de objetos que representan.
     *
     * @param clase la clase de los objetos a convertir.
     * @param converters una colección de converters a convertir.
     * @param <T> el tipo que deberá extender de Entidad.
     *
     * @return una lista de objetos de la clase dada a partir de su representación como converters.
     */
    public static <T extends Entidad> List<T> unconvert(Class<T> clase,
            Collection<? extends Converter> converters) {
        List<T> ts = new ArrayList<>();
        for (Converter c : converters) {
            Entidad e = c.getEntity();
            if (clase.isInstance(e)) {
                T t = clase.cast(e);
                ts.add(t);
            } else {
                throw new ClassCastException("La entidad no es del tipo: " + clase);
            }
        }

        return ts;
    }

}
