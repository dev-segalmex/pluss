/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo;

import java.util.List;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Clase utilitaria que tiene como fin el enviar mensajes de respuesta del servidor al cliente.
 *
 * @author ismael
 */
@XmlRootElement(name = "respuesta")
public class RespuestaConverter {

    /**
     * Constructor de la clase.
     */
    public RespuestaConverter() {
    }

    /**
     * Mensaje genérico a enviar del lado del servidor. Este valor puede ser nulo.
     */
    private String mensaje;

    /**
     * Mensaje de error a enviar del lado del servidor. Este valor puede ser nulo.
     */
    private String mensajeError;

    /**
     * Lista de errores encontrados del lado del servidor.
     */
    private List<String> causas;

    /**
     * Valor en caso de que la respuesta contenga enlaces a archivos o URL's construídas del lado
     * del servidor y se requieran del lado del cliente.
     */
    private String url;

    /**
     * Obtiene el mensaje.
     *
     * @return el mensaje.
     */
    public String getMensaje() {
        return mensaje;
    }

    /**
     * Establece un mensaje.
     *
     * @param mensaje el mensaje.
     */
    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    /**
     * Obtiene el mensaje de error.
     *
     * @return el mensaje de error.
     */
    public String getMensajeError() {
        return mensajeError;
    }

    /**
     * Establece un mensaje de error.
     *
     * @param mensajeError el mensaje de error.
     */
    public void setMensajeError(String mensajeError) {
        this.mensajeError = mensajeError;
    }

    /**
     * Obtiene una lista de causas.
     *
     * @return una lista de causas.
     */
    public List<String> getCausas() {
        return causas;
    }

    /**
     * Establece una lista de causas.
     *
     * @param causas una lista de causas.
     */
    public void setCausas(List<String> causas) {
        this.causas = causas;
    }

    /**
     * Obtiene el valor de la URL.
     *
     * @return el valor de la URL.
     */
    public String getUrl() {
        return url;
    }

    /**
     * Establece el valor de la URL.
     *
     * @param url el valor de la URL.
     */
    public void setUrl(String url) {
        this.url = url;
    }
}
