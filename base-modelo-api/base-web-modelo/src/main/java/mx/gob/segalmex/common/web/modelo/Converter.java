/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo;

import javax.xml.bind.annotation.XmlTransient;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;

/**
 *
 * @author oscar
 */
public interface Converter {

    /**
     * Obtiene la <code>AbstractEntidad</code> que se convertirá a objetos JSON/XML.
     *
     * @return la <code>AbstractEntidad</code> que se busca convertir.
     */
    @XmlTransient
    AbstractEntidad getEntity();

}
