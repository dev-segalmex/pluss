/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.base;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlTransient;
import mx.gob.segalmex.pluss.modelo.base.AbstractCatalogo;

/**
 *
 * @author ismael
 */
public abstract class AbstractCatalogoConverter extends AbstractEntidadConverter {

    @XmlTransient
    @Override
    public abstract AbstractCatalogo getEntity();

    @Override
    @XmlTransient
    public Calendar getFechaCreacion() {
        return super.getFechaCreacion();
    }

    @Override
    @XmlTransient
    public Calendar getFechaActualizacion() {
        return super.getFechaActualizacion();
    }

    /**
     * Obtiene la clave del {@link AbstractCatalogo}.
     *
     * @return
     */
    public String getClave() {
        return getEntity().getClave();
    }

    /**
     * Asigna la clave del {@link AbstractCatalogo}.
     *
     * @param clave
     */
    public void setClave(String clave) {
        getEntity().setClave(clave);
    }

    /**
     * Recupera el nombre del {@link AbstractCatalogo}.
     *
     * @return
     */
    public String getNombre() {
        return getEntity().getNombre();
    }

    /**
     * Asigna el nombre del {@link AbstractCatalogo}.
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
        getEntity().setNombre(nombre);
    }

    /**
     * Obtiene el estado del {@link AbstractCatalogo}.
     *
     * @return
     */
    public boolean isActivo() {
        return getEntity().isActivo();
    }

    /**
     * Establece el estado del {@link AbstractCatalogo}.
     *
     * @param activo
     */
    public void setActivo(boolean activo) {
        getEntity().setActivo(activo);
    }

    /**
     * EL orden del cátalogo.
     * @return
     */
    public int getOrden() {
        return getEntity().getOrden();
    }

    /**
     *
     * @param orden
     */
    public void setOrden(int orden) {
        getEntity().setOrden(orden);
    }
}
