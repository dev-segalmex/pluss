/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.base;

import java.util.Calendar;
import javax.xml.bind.annotation.XmlTransient;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.common.web.modelo.Converter;

/**
 *
 * @author ismael
 */
public abstract class AbstractEntidadConverter implements Converter {

    @XmlTransient
    @Override
    public abstract AbstractEntidad getEntity();

    /**
     * El nivel de ampliación por omisión.
     */
    protected int expandLevel = 1;

    /**
     * Recupera el id del converter.
     *
     * @return
     */
    public Integer getId() {
        return getEntity().getId();
    }

    /**
     * Establece el id de la {@link AbstractEntidad}.
     *
     * @param id
     */
    public void setId(Integer id) {
        getEntity().setId(id);
    }

    /**
     * Obtiene la fecha de creación de la {@link AbstractEntidad}.
     *
     * @return
     */
    public Calendar getFechaCreacion() {
        return getEntity().getFechaCreacion();
    }

    /**
     * Establece la fechaCreacion de la {@link AbstractEntidad}.
     *
     * @param fechaCreacion
     */
    public void setFechaCreacion(Calendar fechaCreacion) {
        getEntity().setFechaCreacion(fechaCreacion);
    }

    /**
     * Obtiene la fechaActualizacion de la {@link AbstractEntidad}.
     *
     * @return
     */
    public Calendar getFechaActualizacion() {
        return getEntity().getFechaActualizacion();
    }

    /**
     * Establece la fechaActualizacion de la {@link AbstractEntidad}.
     *
     * @param fechaActualizacion
     */
    public void setFechaActualizacion(Calendar fechaActualizacion) {
        getEntity().setFechaActualizacion(fechaActualizacion);
    }

}
