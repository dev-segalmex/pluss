/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.base;

/**
 * Interfaz que define los métodos básicos de un catálogo.
 *
 * @author ismael
 */
public interface Catalogo extends Entidad {

    String getClave();

    String getNombre();

    boolean isActivo();

    int getOrden();

}
