/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.base;

import java.io.Serializable;
import java.util.Calendar;

/**
 * Interfaz que define los métodos básicos de un entidad.
 *
 * @author ismael
 */
public interface Entidad extends Serializable {

    Integer getId();

    Calendar getFechaCreacion();

    Calendar getFechaActualizacion();

}
