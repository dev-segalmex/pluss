/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.base;

import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Clase que implementa la interfaz <code>Entidad</code>. Todas las clases que sean entidades deberán extender esta
 * clase.
 *
 * @author ismael
 */
@MappedSuperclass
@Getter(onMethod = @__(@Override))
@Setter
public abstract class AbstractEntidad implements Entidad {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    protected Integer id;

    @Column(name = "fecha_creacion", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    protected Calendar fechaCreacion;

    @Column(name = "fecha_actualizacion", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    protected Calendar fechaActualizacion;

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", getId())
                .toString();
    }
}
