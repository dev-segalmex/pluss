/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.base;

import javax.persistence.Column;
import javax.persistence.MappedSuperclass;
import lombok.Getter;
import lombok.Setter;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Clase que implementa la interfaz <code>Catalogo</code>. Todas las clases que sean catálogos deberían extender a esta
 * clase.
 *
 * @author ismael
 */
@MappedSuperclass
@Getter(onMethod = @__(@Override))
@Setter
public abstract class AbstractCatalogo extends AbstractEntidad implements Catalogo {

    @Column(name = "clave", nullable = false, unique = true, length = 63)
    protected String clave;

    @Column(name = "nombre", nullable = false)
    protected String nombre;

    @Column(name = "activo", nullable = false)
    protected boolean activo;

    @Column(name = "orden", nullable = false)
    protected int orden;

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("id", id)
                .append("clave", clave)
                .append("nombre", nombre)
                .append("orden", orden)
                .toString();
    }

    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof AbstractCatalogo)) {
            return false;
        }

        if (this == obj) {
            return true;
        }

        AbstractCatalogo target = (AbstractCatalogo) obj;
        return new EqualsBuilder()
                .append(getClass(), target.getClass())
                .append(clave, target.clave)
                .isEquals();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder(11447, 38617)
                .append(getClass())
                .append(clave)
                .toHashCode();
    }

}
