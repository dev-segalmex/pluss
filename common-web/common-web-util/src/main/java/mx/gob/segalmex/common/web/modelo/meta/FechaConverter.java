/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.meta;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author oscar
 */
@XmlRootElement(name = "fecha")
public class FechaConverter {

    /**
     * El <code>Calendar</code> con la fecha a representar.
     */
    private Calendar entity;

    /**
     * Constructor por omisión.
     */
    public FechaConverter() {
    }

    /**
     * Constructor que crea una instancia a partir de un <code>Calendar</code> con la fecha a
     * representar.
     *
     * @param entity el <code>Calendar</code> a representar.
     */
    public FechaConverter(Calendar entity) {
        this.entity = entity;
    }

    /**
     * Obtiene el <code>Calendar</code> de la fecha a representar.
     *
     * @return el <code>Calendar</code> de la fecha a representar.
     */
    @XmlTransient
    public Calendar getEntity() {
        return entity;
    }

    /**
     * Obtiene una representación de la fecha con el formato: <i>yyyy/MM/dd</i>.
     *
     * @return una representación de la fecha con el formato: <i>yyyy/MM/dd</i>.
     */
    @XmlElement
    public String getDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");

        return dateFormat.format(getEntity().getTime());
    }

    /**
     * Obtiene una representación de la hora con el formato: <i>HH:mm:ss</i>.
     *
     * @return una representación de la hora con el formato: <i>HH:mm:ss</i>.
     */
    @XmlElement
    public String getTime() {
        DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");

        return timeFormat.format(getEntity().getTime());
    }

    /**
     * Obtiene una representación de la fecha y hora con el formato:
     * <i>EEEE dd 'de' MMMMM 'de' yyyy, HH:mm 'GMT'Z (z)</i>.
     *
     * @return una representación de la fecha y hora con el formato:
     * <i>EEEE dd 'de' MMMMM 'de' yyyy, HH:mm 'GMT'Z (z)</i>.
     */
    @XmlElement
    public String getDateTime() {
        DateFormat dateTimeFormat
                = new SimpleDateFormat("EEEE dd 'de' MMMMM 'de' yyyy, HH:mm 'GMT'Z (z)",
                        new Locale("es", "MX"));

        return dateTimeFormat.format(getEntity().getTime());
    }

}
