/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.meta;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.common.web.modelo.seguridad.UsuarioConverter;

/**
 *
 * @author oscar
 */
@XmlRootElement(name = "comun-meta")
public class ComunMetaConverter {

    /**
     * El usuario en sesión.
     */
    private Usuario registradoEntity = null;

    /**
     * Los roles del usuario.
     */
    private List<String> roles = new ArrayList<>();

    /**
     * La lista de contextos/sistemas que se tienen.
     */
    private List<String> contextos = new ArrayList<>();

    /**
     * El nombre del nodo en el que se ejecuta el recurso.
     */
    private String nodo;

    /**
     * Constructor por omisión.
     */
    public ComunMetaConverter() {
    }

    /**
     * Obtiene un <code>UsuarioConverter</code> del usuario en sesión.
     *
     * @return un <code>UsuarioConverter</code> del usuario en sesión, null si no hay un usuario en
     * sesión.
     */
    @XmlElement
    public UsuarioConverter getRegistrado() {
        if (registradoEntity != null) {
            return new UsuarioConverter(registradoEntity, 2);
        }
        return null;
    }

    /**
     * Establece el usuario en sesión.
     *
     * @param registradoEntity el usuario en sesión.
     */
    public void setRegistrado(Usuario registradoEntity) {
        this.registradoEntity = registradoEntity;
    }

    /**
     * Obtiene los roles de un {@link Usuario}
     *
     * @return una lista de roles del usuario.
     */
    @XmlElement
    public List<String> getRoles() {
        return this.roles;
    }

    /**
     * Establece la lista de roles asociados del usuario en sesión.
     *
     * @param roles la lista de roles asociados del usuario en sesión.
     */
    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    @XmlElement
    public List<String> getContextos() {
        return contextos;
    }

    public void setContextos(List<String> contextos) {
        this.contextos = contextos;
    }

    /**
     * Obtiene un <code>FechaConverter</code> con la fecha actual del servidor.
     *
     * @return un <code>FechaConverter</code> con la fecha actual del servidor.
     */
    @XmlElement
    public FechaConverter getAhora() {
        FechaConverter fecha = new FechaConverter(Calendar.getInstance());
        return fecha;
    }

    public String getNodo() {
        return nodo;
    }

    public void setNodo(String nodo) {
        this.nodo = nodo;
    }
}
