package mx.gob.segalmex.pluss.common.web.resources.meta;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import org.springframework.stereotype.Component;

/**
 * Recurso para obtener la fecha actual del sistema.
 *
 * @author ismael
 */
@Path("/fechas")
@Component
public class FechaResource {

    /**
     * Obtiene la fecha actual.
     *
     * @return la fecha actual en varios formatos.
     */
    @GET
    @Path("/ahora")
    @Produces({"application/json; charset=utf-8"})
    public String ahora() {
        Date ahora = Calendar.getInstance().getTime();
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
        DateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
        DateFormat dateTimeFormat = new SimpleDateFormat("EEEE dd 'de' MMMMM 'de' yyyy, "
                + "HH:mm 'GMT'Z (z)",
                new Locale("es", "MX"));

        return "{\"date\":\""
                + dateFormat.format(ahora) + "\", \"time\":\""
                + timeFormat.format(ahora) + "\", \"dateTime\":\""
                + dateTimeFormat.format(ahora)
                + "\"}";
    }

}
