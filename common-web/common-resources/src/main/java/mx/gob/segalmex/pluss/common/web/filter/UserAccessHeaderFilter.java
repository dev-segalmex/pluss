/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.common.web.filter;

import java.io.IOException;
import java.util.Objects;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.filter.OncePerRequestFilter;

/**
 *
 * @author ismael
 */
@Slf4j
public class UserAccessHeaderFilter extends OncePerRequestFilter {

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response,
            FilterChain filterChain) throws ServletException, IOException {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String header = StringUtils.trimToNull(request.getHeader("Segalmex-Username"));

        if (Objects.nonNull(header)) {
//            if (Objects.isNull(authentication)) {
//                LOGGER.warn("El usuario {} está fuera de sesión.", header);
//                throw new AccessDeniedException("El usuario ha finalizado su sesión.");
//            }
            if (!StringUtils.equals(header, authentication.getName())) {
                LOGGER.warn("El usuario: {} intenta tener acceso a un recurso fuera de su sesión.", header);
                throw new AccessDeniedException("El usuario en sesión y el usuario de la petición son distintos.");
            }
        }
        filterChain.doFilter(request, response);
    }

}
