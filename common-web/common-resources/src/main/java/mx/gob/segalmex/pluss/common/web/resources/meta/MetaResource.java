/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.common.web.resources.meta;

import javax.ws.rs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Path("/meta-resources")
@Component
public class MetaResource {

    @Autowired
    private ComunSubResource comunSubResource;

    @Path("/comun")
    public ComunSubResource getComunSubResource() {
        return comunSubResource;
    }

}
