package mx.gob.segalmex.pluss.common.web.config;

import java.util.List;

/**
 * Clase auxiliar para representar una respuesta de una excepción.
 *
 * @author ismael
 */
public class ErrorConverter {

    private String mensaje;

    private List<String> motivos;

    public ErrorConverter(String mensaje, List<String> motivos) {
        this.mensaje = mensaje;
        this.motivos = motivos;
    }

    public String getMensaje() {
        return mensaje;
    }

    public List<String> getMotivos() {
        return motivos;
    }

}
