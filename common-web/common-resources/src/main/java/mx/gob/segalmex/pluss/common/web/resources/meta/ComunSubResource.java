/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.common.web.resources.meta;

import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.GET;
import lombok.RequiredArgsConstructor;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.web.modelo.meta.ComunMetaConverter;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import mx.gob.segalmex.pluss.modelo.seguridad.Sistema;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 *
 * @author oscar
 */
@Component
@RequiredArgsConstructor
public class ComunSubResource {

    /**
     * La fachada del contexto de seguridad.
     */
    private final UsuarioSesionContextoHolder contextoSeguridad;

    private final BuscadorCatalogo buscadorCatalogo;

    private final Environment environment;

    @GET
    public ComunMetaConverter index() {
        Usuario registrado = contextoSeguridad.getUsuario();
        ComunMetaConverter comun = new ComunMetaConverter();
        comun.setRegistrado(registrado);
        comun.setRoles(null);
        comun.setContextos(getContextos());
        comun.setNodo(environment.getProperty("jboss.server.name"));
        return comun;
    }

    /**
     * Se encarga de regresar una lista de los contextos existentes. Cada
     * elemento es de la forma: /nombrcontexto
     *
     * @return
     */
    private List<String> getContextos() {
        List<Sistema> sistemas = buscadorCatalogo.busca(Sistema.class);
        List<String> claves = new ArrayList<>();
        for (Sistema s : sistemas) {
            claves.add("/" + s.getClave());
        }
        return claves;
    }

}
