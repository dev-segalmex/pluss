/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.common.web.config;

import com.fasterxml.jackson.annotation.JsonAutoDetect;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.PropertyAccessor;
import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.module.jaxb.JaxbAnnotationModule;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;
import javax.ws.rs.ext.ContextResolver;
import javax.ws.rs.ext.Provider;

/**
 * Provee a <a href="https://github.com/FasterXML/jackson">Jackson</a> un {@link ObjectMapper}
 * configurado para tomar las fechas en zona horaria local en lugar de GMT y omitir atributos nulos
 * o valores nulos en maps al serializar.
 *
 * @author Jesús Adolfo García Pasquel
 */
@Provider
public class JacksonConfig implements ContextResolver<ObjectMapper> {

    /**
     * Formato de fecha y hora que se está utilizando para serializar en Glassfish/Jersey.
     */
    public static final String ISO_DATE_TIME_FORMAT = "yyyy-MM-dd'T'HH:mm:ssXXX";

    /**
     * {@link ObjectMapper} configurado para tomar las fechas en zona horaria local en lugar de GMT
     * y omitir atributos nulos o valores nulos en maps al serializar.
     */
    private final ObjectMapper objectMapper;

    /**
     * Construye un nuevo ejemplar de {@link JacksonConfig}, que provee a
     * <a href="https://github.com/FasterXML/jackson">Jackson</a> un {@link ObjectMapper}
     * configurado para tomar las fechas en zona horaria local en lugar de GMT y omitir atributos
     * nulos o valores nulos en maps al serializar.
     */
    public JacksonConfig() {
        objectMapper = new ObjectMapper();
        // Considerar fechas en zona horaria local, en lugar de GMT
        objectMapper.setTimeZone(TimeZone.getDefault());
        // Omitir atributos nulos en serialización, así como valores null en maps
        objectMapper.configure(SerializationFeature.WRITE_NULL_MAP_VALUES, false);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        // Considerar únicamente propiedades públicas (no tomar directamente los campos)
        objectMapper.setVisibility(PropertyAccessor.ALL, JsonAutoDetect.Visibility.NONE);
        objectMapper.setVisibility(PropertyAccessor.GETTER, JsonAutoDetect.Visibility.PUBLIC_ONLY);
        objectMapper.setVisibility(PropertyAccessor.IS_GETTER,
                JsonAutoDetect.Visibility.PUBLIC_ONLY);
        // Usar anotaciones de JAXB
        objectMapper.registerModule(new JaxbAnnotationModule());
        // Al reconstruir objetos, ignora los atributos que están en JSON, pero no en Java
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        // Serializa Calendar en el formato ISO-8601 antes utilizado en lugar de Epoch time.
        SimpleModule module = new SimpleModule();
        module.addSerializer(Calendar.class, new CustomCalendarSerializer());
        objectMapper.registerModule(module);
    }

    /**
     * Serializa ejemplares de {@link Calendar} con el mismo formato ISO utilizado en Jersey:
     * &quot;yyyy-MM-dd'T'HH:mm:ssXXX&quot;. El formato predeterminado de Jackson, incluye
     * milisegundos, mientras este no.
     *
     * @author Jesús Adolfo García Pasquel
     */
    public static class CustomCalendarSerializer extends JsonSerializer<Calendar> {

        @Override
        public void serialize(Calendar value, JsonGenerator gen, SerializerProvider arg2)
                throws IOException, JsonProcessingException {
            SimpleDateFormat formatter
                    = new SimpleDateFormat(ISO_DATE_TIME_FORMAT);
            gen.writeString(formatter.format(value.getTime()));
        }

    }

    @Override
    public ObjectMapper getContext(Class<?> arg0) {
        return objectMapper;
    }

}
