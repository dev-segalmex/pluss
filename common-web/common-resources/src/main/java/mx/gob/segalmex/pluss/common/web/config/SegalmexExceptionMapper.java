/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.common.web.config;

import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;

/**
 *
 * @author ismael
 */
@Provider
@Slf4j
public class SegalmexExceptionMapper implements ExceptionMapper<SegalmexRuntimeException> {

    @Override
    public Response toResponse(SegalmexRuntimeException e) {
        LOGGER.error("Motivos: {}", e.getMotivos());
        LOGGER.error("Error: ", e);
        return Response.status(Response.Status.BAD_REQUEST)
                .entity(new ErrorConverter(e.getMessage(), e.getMotivos()))
                .build();
    }

}
