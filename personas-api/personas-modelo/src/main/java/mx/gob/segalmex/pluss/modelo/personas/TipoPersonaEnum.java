/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.personas;

import mx.gob.segalmex.pluss.modelo.base.EnumCatalogo;


/**
 *
 * @author ismael
 */
public enum TipoPersonaEnum implements EnumCatalogo {

    /**
     * Tipo de persona física.
     */
    FISICA("fisica"),

    /**
     * Tipo de persona moral.
     */
    MORAL("moral"),

    /**
     * Tipo de persona usuario.
     */
    USUARIO("usuario"),

    PRODUCTOR("productor"),

    REPRESENTANTE("representante");

    /**
     * La clave del tipo de persona.
     */
    private final String clave;

    private TipoPersonaEnum(String clave) {
        this.clave = clave;
    }

    @Override
    public String getClave() {
        return clave;
    }

    public static TipoPersonaEnum getInstance(String clave) {
        for (TipoPersonaEnum t : values()) {
            if (t.getClave().equals(clave)) {
                return t;
            }
        }
        throw new IllegalArgumentException("No existe un TipoPersonaEnum con la clave: " + clave);
    }

}
