/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.personas;

import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.catalogos.Pais;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "persona")
@Getter
@Setter
public class Persona extends AbstractEntidad {

    /**
     * El tipo de persona.
     */
    @ManyToOne
    @JoinColumn(name = "tipo_id", nullable = false)
    private TipoPersona tipo;

    /**
     * La clave única de la persona.
     */
    @Column(name = "clave", nullable = false)
    private String clave;

    /**
     * El nombre de la persona (puede incluir acentos y caractéres especiales).
     */
    @Column(name = "nombre", nullable = false)
    private String nombre;

    /**
     * Los apellidos de la persona (puede incluir acentos y caracteres especiales).
     */
    @Column(name = "apellidos", nullable = false)
    private String apellidos;

    /**
     * El primer apellido de la persona.
     */
    @Column(name = "primer_apellido")
    private String primerApellido;

    /**
     * El segundo apellido de la persona.
     */
    @Column(name = "segundo_apellido")
    private String segundoApellido;

    /**
     * El nombre ICAO de la persona (sin acentos ni caracteres especiales).
     */
    @Column(name = "nombre_icao", nullable = false)
    private String nombreIcao;

    /**
     * Los apellidos ICAO de la persona (sin acentos ni caracteres especiales).
     */
    @Column(name = "apellidos_icao", nullable = false)
    private String apellidosIcao;

    /**
     * El sexo de la persona.
     */
    @ManyToOne
    @JoinColumn(name = "sexo_id")
    private Sexo sexo;

    /**
     * La fecha de nacimiento de la persona.
     */
    @Temporal(TemporalType.DATE)
    @Column(name = "fecha_nacimiento")
    private Calendar fechaNacimiento;

    /**
     * La nacionalidad de la persona.
     */
    @ManyToOne
    @JoinColumn(name = "nacionalidad_id")
    private Pais nacionalidad;

}
