/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.catalogos;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersona;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;

/**
 *
 * @author cuecho
 */
@XmlRootElement(name = "tipo-persona")
public class TipoPersonaConverter extends AbstractCatalogoConverter {

    private final TipoPersona entity;

    public TipoPersonaConverter() {
        this.entity = new TipoPersona();
        this.expandLevel = 1;
    }

    public TipoPersonaConverter(TipoPersona entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad TipoPersona no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public TipoPersona getEntity() {
        return this.entity;
    }
}
