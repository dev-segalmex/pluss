/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.persona;

import java.util.Calendar;
import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.pluss.modelo.personas.Persona;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.PaisConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.SexoConverter;
import mx.gob.segalmex.common.web.modelo.catalogos.TipoPersonaConverter;

/**
 *
 * @author oscar
 */
@XmlRootElement(name = "persona")
public class PersonaConverter extends AbstractEntidadConverter {

    /**
     * La {@link Persona} a representar en este converter.
     */
    private final Persona entity;

    public PersonaConverter() {
        entity = new Persona();
        expandLevel = 1;
    }

    public PersonaConverter(Persona entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    /**
     * Recupera la entidad representada.
     *
     * @return
     */
    @Override
    public Persona getEntity() {
        return this.entity;
    }

    /**
     * Recupera la clave de la {@link Persona}.
     *
     * @return
     */
    public String getClave() {
        return getEntity().getClave();
    }

    /**
     * Estalece la clave de la persona.
     *
     * @param clave
     */
    public void setClave(String clave) {
        getEntity().setClave(clave);
    }

    public TipoPersonaConverter getTipo() {
        return expandLevel > 0 && getEntity().getTipo() != null
                ? new TipoPersonaConverter(getEntity().getTipo(), expandLevel - 1) : null;
    }

    public void setTipo(TipoPersonaConverter tipo) {
        getEntity().setTipo(tipo.getEntity());
    }

    /**
     * Obtiene el nombre de la {@link Persona}.
     *
     * @return the nombre
     */
    public String getNombre() {
        return getEntity().getNombre();
    }

    /**
     * Establece el nombre de la {@link Persona}.
     *
     * @param nombre the nombre to set
     */
    public void setNombre(String nombre) {
        getEntity().setNombre(nombre);
    }

    /**
     * Recupera los apellidos originales de la {@link Persona}.
     *
     * @return the apellidos
     */
    public String getApellidos() {
        return getEntity().getApellidos();
    }

    /**
     * Establece los apellidos de la {@link Persona}.
     *
     * @param apellidos the apellidos to set
     */
    public void setApellidos(String apellidos) {
        getEntity().setApellidos(apellidos);
    }

    /**
     * Obtiene el nombre en icao de la {@link Persona}.
     *
     * @return the nombreIcao
     */
    public String getNombreIcao() {
        return getEntity().getNombreIcao();
    }

    /**
     * Establece el nombre icao de la {@link Persona}.
     *
     * @param nombreIcao the nombreIcao to set
     */
    public void setNombreIcao(String nombreIcao) {
        getEntity().setNombreIcao(nombreIcao);
    }

    /**
     * Recupera el apellido icao de la {@link Persona}.
     *
     * @return the apellidosIcao
     */
    public String getApellidosIcao() {
        return getEntity().getApellidosIcao();
    }

    /**
     * Asigna los apellidosIcao a la {@link Persona}.
     *
     * @param apellidosIcao the apellidosIcao to set
     */
    public void setApellidosIcao(String apellidosIcao) {
        getEntity().setApellidosIcao(apellidosIcao);
    }

    /**
     * Recupera el {@link Sexi} de la {@link Persona}.
     *
     * @return the sexo
     */
    public SexoConverter getSexo() {
        return expandLevel > 0 && getEntity().getSexo() != null
                ? new SexoConverter(getEntity().getSexo(), expandLevel - 1) : null;
    }

    /**
     * Le asigna el {@link Sexo} de la {@link Persona}.
     *
     * @param sexo the sexo to set
     */
    public void setSexo(SexoConverter sexo) {
        getEntity().setSexo(sexo.getEntity());
    }

    /**
     * Obtiene la fechaNacimiento de la {@link Persona}.
     *
     * @return the fechaNacimiento
     */
    public Calendar getFechaNacimiento() {
        return getEntity().getFechaNacimiento();
    }

    /**
     * Establece la fecha nacimiento de la {@link Persona}.
     *
     * @param fechaNacimiento the fechaNacimiento to set
     */
    public void setFechaNacimiento(Calendar fechaNacimiento) {
        getEntity().setFechaNacimiento(fechaNacimiento);
    }

    /**
     *
     * @return
     */
    public PaisConverter getNacionalidad() {
        return expandLevel > 0 && getEntity().getNacionalidad() != null
                ? new PaisConverter(getEntity().getNacionalidad(), expandLevel - 1) : null;
    }

    /**
     *
     * @param nacionalidad
     */
    public void setNacionalidad(PaisConverter nacionalidad) {
        getEntity().setNacionalidad(nacionalidad.getEntity());
    }

}
