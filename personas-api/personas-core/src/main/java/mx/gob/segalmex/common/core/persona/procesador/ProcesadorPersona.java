/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.persona.procesador;

import mx.gob.segalmex.pluss.modelo.personas.Persona;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersona;

/**
 *
 * @author
 */
public interface ProcesadorPersona {

    /**
     * Registra una persona verificando si existe o no.
     *
     * @param persona la persona a registrar.
     * @param tipo el tipo de persona que se registra.
     * @return la persona registrada (podría ya existir).
     */
    Persona procesa(Persona persona, TipoPersona tipo);

}
