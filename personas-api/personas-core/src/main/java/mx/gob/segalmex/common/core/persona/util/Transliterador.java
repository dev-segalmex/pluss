/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.persona.util;

import java.util.Map;
import java.util.Objects;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author ismael
 */
public class Transliterador {

    /**
     * Indica si el transliterador será estricto cuando no encuentre una equivalencia. Por default
     * lo es y lanza una excepción.
     */
    private boolean estricto = true;

    /**
     * El mapa de equivalencias.
     */
    private Map<String, String> equivalencia;

    public void setEstricto(boolean estricto) {
        this.estricto = estricto;
    }

    public void setEquivalencia(Map<String, String> equivalencia) {
        this.equivalencia = equivalencia;
    }

    public String translitera(String base) {
        base = StringUtils.stripToNull(base);
        Objects.requireNonNull(base, "La cadena no puede ser vacía.");

        // Si es el nombre vacío, terminamos
        if (StringUtils.equals(PersonaStringUtils.NOMBRE_VACIO, base)) {
            return base;
        }

        String toUpper = StringUtils.upperCase(base);
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < toUpper.length(); i++) {
            String transliteracion = equivalencia.get(String.valueOf(toUpper.charAt(i)));
            if (transliteracion != null) {
                sb.append(transliteracion);
                continue;
            }

            if (estricto) {
                throw new IllegalArgumentException("La cadena contiene el caracter no reconocido: "
                        + toUpper.charAt(i));
            } else {
                sb.append(toUpper.charAt(i));
            }
        }

        return sb.toString().replaceAll(" +", " ");
    }

}
