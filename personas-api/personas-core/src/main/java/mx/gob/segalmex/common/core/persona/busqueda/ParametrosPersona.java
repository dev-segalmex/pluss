/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.persona.busqueda;

import java.util.Calendar;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.catalogos.Pais;
import mx.gob.segalmex.pluss.modelo.personas.Persona;
import mx.gob.segalmex.pluss.modelo.personas.Sexo;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersona;

/**
 *
 * @author ismael
 */
@Getter
@Setter
public class ParametrosPersona {

    /**
     * El identificador de la persona en la base de datos.
     */
    private Integer id;

    /**
     * El tipo de persona.
     */
    private TipoPersona tipoPersona;

    /**
     * La clave de la persona.
     */
    private String clave;

    /**
     * El nombre de la persona.
     */
    private String nombre;

    /**
     * Los apellidos de la persona.
     */
    private String apellidos;

    /**
     * El sexo de la persona.
     */
    private Sexo sexo;

    /**
     * La nacionalidad de la persona.
     */
    private Pais nacionalidad;

    /**
     * Parametro para filtrar por fecha de nacimiento. Sólo se usa en la
     * pantalla de busqueda de personas.
     */
    private Calendar fechaNacimiento;

    /**
     * Indica si se debe buscar por nombre y apellidos exactos (flexible =
     * <code>false</code>) o con nombre y apellidos que inicien con las cadenas
     * recibidas (flexible = <code>true</code>).
     */
    private boolean flexible = false;

    public static ParametrosPersona getInstance(Persona persona) {
        ParametrosPersona parametros = new ParametrosPersona();
        parametros.setNacionalidad(persona.getNacionalidad());
        parametros.setApellidos(persona.getApellidosIcao());
        parametros.setNombre(persona.getNombreIcao());
        parametros.setSexo(persona.getSexo());
        parametros.setTipoPersona(persona.getTipo());
        parametros.setId(persona.getId());

        return parametros;
    }

}
