/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.persona.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.personas.Persona;

/**
 * Métodos de busqueda sobre {@link Persona}.
 *
 * @author ismael
 */
public interface BuscadorPersona {

    /**
     * Obtiene una lista de las {@link Persona} que cumplan con los filtros establecidos en los
     * parametros recibidos.
     *
     * @param parametros los filtros a aplicar para la busqueda.
     * @return la lista de {@link Persona} encontrada.
     */
    List<Persona> busca(ParametrosPersona parametros);

    /**
     * Obtiene uno y solo un ejemplar de {@link Persona} con los parametros recibidos. Si no
     * encuentra nada manda EmptyResultDataAccessException y si encuentra mas de una lanza
     * IncorrectResultSizeDataAccessException.
     *
     * @param parametros
     * @return la {@link Persona} encontrada.
     */
    Persona buscaElemento(ParametrosPersona parametros);

    /**
     * Obtiene un ejemplar de {@link Persona} con la clave recibida.
     *
     * @param clave la clave de la {@link Persona} a recuperar.
     * @return la {@link Persona} encontrada.
     */
    Persona buscaElemento(String clave);

    /**
     * Obtiene un ejemplar de {@link Persona} con el id recibido.
     *
     * @param id el id de la {@link Persona} a encontrar.
     * @return la {@link Persona} encontrada.
     */
    Persona buscaElemento(Integer id);

}
