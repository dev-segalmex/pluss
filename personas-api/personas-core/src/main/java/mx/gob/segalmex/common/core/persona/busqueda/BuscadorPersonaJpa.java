/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.persona.busqueda;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.personas.Persona;
import org.springframework.stereotype.Repository;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;

/**
 *
 * @author ismael
 */
@Slf4j
@Repository("buscadorPersona")
public class BuscadorPersonaJpa implements BuscadorPersona {

    /**
     * El entity manager de la clase.
     */
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Persona> busca(ParametrosPersona parametros) {
        LOGGER.debug("Buscando Personas por parámetros...");
        StringBuilder sb = new StringBuilder();
        sb.append(" SELECT p FROM Persona p ");
        Map<String, Object> params = new HashMap<>();
        boolean primero = true;

        if (Objects.nonNull(parametros.getId())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "p.id", "id", parametros.getId(), params);
        }

        if (Objects.nonNull(parametros.getTipoPersona())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "p.tipo", "tipo", parametros.getTipoPersona(), params);
        }

        LOGGER.debug("Buscando con nombre/apellido flexibles {}", parametros.isFlexible());
        if (Objects.nonNull(parametros.getNombre())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            if (parametros.isFlexible()) {
                sb.append(" p.nombreIcao like :nombreIcao ");
                params.put("nombreIcao", parametros.getNombre() + "%");
            } else {
                QueryUtils.and(sb, "p.nombreIcao", "nombreIcao", parametros.getNombre(), params);
            }
        }

        if (Objects.nonNull(parametros.getApellidos())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            if (parametros.isFlexible()) {
                sb.append(" p.apellidosIcao like :apellidosIcao ");
                params.put("apellidosIcao", parametros.getApellidos() + "%");
            } else {
                QueryUtils.and(sb, "p.apellidosIcao", "apellidosIcao", parametros.getApellidos(), params);
            }
        }

        if (Objects.nonNull(parametros.getSexo())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "p.sexo", "sexo", parametros.getSexo(), params);
        }

        if (Objects.nonNull(parametros.getClave())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "p.clave", "clave", parametros.getClave(), params);
        }

        if (Objects.nonNull(parametros.getNacionalidad())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "p.nacionalidad", "nacionalidad", parametros.getNacionalidad(), params);
        }

        if (Objects.nonNull(parametros.getFechaNacimiento())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "p.fechaNacimiento", "fechaNacimiento", parametros.getFechaNacimiento(), params);
        }

        TypedQuery<Persona> query = entityManager.createQuery(sb.toString(), Persona.class);
        QueryUtils.setParametros(query, params);
        LOGGER.debug("Ejecutando query: {}", sb);

        long inicio = System.currentTimeMillis();
        List<Persona> personas = query.getResultList();
        LOGGER.debug("Se encontraron {} personas en {} ms.", personas.size(),
                System.currentTimeMillis() - inicio);

        return personas;
    }

    @Override
    public Persona buscaElemento(ParametrosPersona parametros) {
        LOGGER.debug("Buscando Persona por parámetros...");
        List<Persona> personas = busca(parametros);

        if (personas.isEmpty()) {
            throw new EmptyResultDataAccessException("No existe persona con los parámetros "
                    + "especificados.", 1);
        }

        if (personas.size() > 1) {
            throw new IncorrectResultSizeDataAccessException("Existe más de una persona con los"
                    + " parámetros especificados.", 1);
        }

        return personas.get(0);
    }

    @Override
    public Persona buscaElemento(String clave) {
        Objects.requireNonNull(clave, "La clave no puede ser nula.");
        LOGGER.debug("Buscando Persona por clave {}.", clave);
        ParametrosPersona parametros = new ParametrosPersona();
        parametros.setClave(clave);

        return buscaElemento(parametros);
    }

    @Override
    public Persona buscaElemento(Integer id) {
        Objects.requireNonNull(id, "El id no puede ser nulo.");
        LOGGER.debug("Buscando Persona por id.");
        ParametrosPersona parametros = new ParametrosPersona();
        parametros.setId(id);

        return buscaElemento(parametros);
    }

}
