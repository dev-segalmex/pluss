/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.persona.util;

import java.util.Calendar;
import java.util.Objects;
import java.util.UUID;
import mx.gob.segalmex.pluss.modelo.personas.Persona;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersona;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component
public class PersonaNormalizer {

    @Autowired
    private Transliterador transliterador;

    public Persona normaliza(Persona persona, TipoPersona tipo) {
        Persona normalizada = new Persona();
        normalizada.setApellidos(StringUtils.upperCase(persona.getApellidos()));
        normalizada.setApellidosIcao(transliterador.translitera(persona.getApellidos()));
        if (Objects.nonNull(persona.getPrimerApellido())) {
            normalizada.setPrimerApellido(transliterador.translitera(persona.getPrimerApellido()));
        }
        normalizada.setSegundoApellido(Objects.nonNull(persona.getSegundoApellido())
                ? transliterador.translitera(persona.getSegundoApellido()) : null);
        normalizada.setNombre(StringUtils.upperCase(persona.getNombre()));
        normalizada.setNombreIcao(transliterador.translitera(persona.getNombre()));
        String clave = StringUtils.stripToNull(persona.getClave());
        normalizada.setClave(Objects.isNull(clave) ? UUID.randomUUID().toString() : clave);
        normalizada.setFechaNacimiento(Objects.nonNull(persona.getFechaNacimiento())
                ? DateUtils.truncate(persona.getFechaNacimiento(), Calendar.DATE) : null);
        normalizada.setSexo(persona.getSexo());
        normalizada.setTipo(tipo);
        normalizada.setNacionalidad(persona.getNacionalidad());

        return normalizada;
    }

}
