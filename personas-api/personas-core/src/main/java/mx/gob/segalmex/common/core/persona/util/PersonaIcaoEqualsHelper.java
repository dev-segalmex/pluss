/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.persona.util;

import java.util.Calendar;
import java.util.Objects;
import mx.gob.segalmex.pluss.modelo.personas.Persona;
import org.apache.commons.lang3.builder.EqualsBuilder;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Component
public class PersonaIcaoEqualsHelper {

    public boolean isSame(Persona normalizada, Persona registrada) {
        // Convertimos la fecha de la base en una instancia de Calendar.getInstance()
        Calendar fnRegistrada = null;
        if (Objects.nonNull(registrada.getFechaNacimiento())) {
            Calendar fecha = Calendar.getInstance();
            fecha.setTimeInMillis(registrada.getFechaNacimiento().getTimeInMillis());
            fnRegistrada = fecha;
        }

        return new EqualsBuilder()
                .append(normalizada.getApellidosIcao(), registrada.getApellidosIcao())
                .append(normalizada.getNombreIcao(), registrada.getNombreIcao())
                .append(normalizada.getSexo(), registrada.getSexo())
                .append(normalizada.getNacionalidad(), registrada.getNacionalidad())
                .append(normalizada.getFechaNacimiento(), fnRegistrada)
                .isEquals();
    }

}
