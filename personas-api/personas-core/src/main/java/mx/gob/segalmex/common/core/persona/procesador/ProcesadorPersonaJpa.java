/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.persona.procesador;

import java.util.List;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.common.core.persona.busqueda.BuscadorPersona;
import mx.gob.segalmex.common.core.persona.busqueda.ParametrosPersona;
import mx.gob.segalmex.common.core.persona.util.PersonaIcaoEqualsHelper;
import mx.gob.segalmex.common.core.persona.util.PersonaNormalizer;
import mx.gob.segalmex.pluss.modelo.personas.Persona;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ismael
 */
@Slf4j
@Service("procesadorPersona")
public class ProcesadorPersonaJpa implements ProcesadorPersona {

    @Autowired
    private BuscadorPersona buscadorPersona;

    @Autowired
    private PersonaIcaoEqualsHelper equalsHelper;

    @Autowired
    private PersonaNormalizer normalizador;

    @Autowired
    private RegistroEntidad registroEntidad;

    @Transactional
    @Override
    public Persona procesa(Persona persona, TipoPersona tipo) {
        Objects.requireNonNull(tipo, "El TipoPersona no puede ser nulo.");
        Persona normalizada = normalizador.normaliza(persona, tipo);
        ParametrosPersona parametros = ParametrosPersona.getInstance(normalizada);

        List<Persona> registradas = buscadorPersona.busca(parametros);
        for (Persona registrada : registradas) {
            if (equalsHelper.isSame(normalizada, registrada)) {
                LOGGER.info("La persona ya se encontraba registrada: {}", registrada.getClave());
                return registrada;
            }
        }

        // Si no es una persona registrada, la registramos
        LOGGER.info("Registrando una nueva persona: {}", normalizada.getClave());
        registroEntidad.guarda(normalizada);
        return normalizada;
    }

}
