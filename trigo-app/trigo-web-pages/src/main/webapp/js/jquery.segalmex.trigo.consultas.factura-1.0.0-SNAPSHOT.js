/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.trigo.consultas.factura');
    var data = {};
    var handlers = {};
    var utils = {};

    $.segalmex.trigo.consultas.factura.init = function (params) {
        utils.inicializaValidaciones();
        utils.cargaCatalogos();
        $('#button-buscar').click(handlers.busca);
        $('#button-buscar-agrupado').click(handlers.buscaAgrupaciones);
        $('#button-exportar').click(handlers.exporta);
        $('#button-limpiar').click(handlers.limpia);
        $('#button-regresar-resultados').click(handlers.regresaResultados);
        $('#button-regresar-resultados-agrupaciones').click(handlers.regresaResultadosAgrupaciones);
        $('#menu-bandejas a.nav-link').click(handlers.cambiaBandeja);
        $('#ciclo').change(handlers.exportaCiclo);
    };

    utils.desHabilitaCtrls = function (disabled) {
        $('#button-exportar').prop('disabled', disabled);
        $('#button-buscar').prop('disabled', disabled);
        $('#button-limpiar').prop('disabled', disabled);
    };

    handlers.exporta = function (e) {
        e.preventDefault();
        utils.desHabilitaCtrls(true);
        utils.configuraCiclo(true);
        if (!utils.validaFiltros(e)) {
            return;
        }
        var filtros = utils.getFiltros(e);
        if (!filtros) {
            utils.desHabilitaCtrls(false);
            return;
        }
        $.segalmex.openPdf('/trigo/resources/facturas/maiz/inscripcion/csv/resultados-factura.xlsx?' + $.param(filtros));
        utils.desHabilitaCtrls(false);
        $('#cargando-resultados').hide();
    };


    handlers.exportaCiclo = function (e) {
        e.preventDefault();
        var ciclo = $.segalmex.get(data.ciclos, $(e.target).val());
        if (!ciclo) {
            $('#button-exportar-ciclo,#button-exportar-ciclo-planeacion').attr('href', '#');
            $('#button-exportar-ciclo,#button-flecha,#button-exportar-ciclo-planeacion').prop('disabled', true);
            return;
        }
        $('#button-exportar-ciclo,#button-flecha,#button-exportar-ciclo-planeacion').prop('disabled', false);
        $('#button-exportar-ciclo').attr('href', '/trigo/resources/facturas/reporte/' + ciclo.id + '.xlsx');
        $('#button-exportar-ciclo-planeacion').attr('href', '/trigo/resources/facturas/reporte/pago/planeacion/' + ciclo.id + '.xlsx');
    };

    handlers.busca = function (e) {
        e.preventDefault();
        utils.desHabilitaCtrls(true);
        utils.configuraCiclo(true);
        if (!utils.validaFiltros(e)) {
            return;
        }

        var filtros = utils.getFiltros(e);
        if (!filtros) {
            utils.desHabilitaCtrls(false);
            return;
        }

        $('#cargando-resultados').show();

        $.ajax({
            url: '/trigo/resources/facturas/maiz/inscripcion/',
            type: 'GET',
            data: filtros,
            dataType: 'json'
        }).done(function (response) {
            $('#cargando-resultados').hide();
            utils.desHabilitaCtrls(false);
            var renglones = '';
            if (response.length === 0) {
                renglones = '<tr><td colspan="8" class="text-center">Sin resultados</td></tr>';
                $('#data').html(renglones);
            } else {
                renglones = utils.creaDatatable(response);
                $('#data').html(renglones);
                utils.configuraTablaResultados();
                $('#table-resultados-busqueda').on('click', 'a.link-uuid', handlers.muestraDetalle);
            }
            data.busquedaAgrupada = false;
            $('#resultados-busqueda').slideDown();

        }).fail(function () {
            $('#cargando-resultados').hide();
            alert('Error: No fue posible realizar la consulta.');
            utils.desHabilitaCtrls(false);
        })
    };

    handlers.buscaAgrupaciones = function (e) {
        e.preventDefault();
        utils.configuraCiclo(false);
        utils.desHabilitaCtrls(true);
        $('.valid-field').limpiaErrores();
        if (!utils.validaFiltros(e)) {
            return;
        }
        var filtros = utils.getFiltros(e);
        if (!filtros) {
            utils.desHabilitaCtrls(false);
            return;
        }
        $('#cargando-resultados').show();
        $.ajax({
            url: '/trigo/resources/facturas/maiz/inscripcion/agrupada',
            type: 'GET',
            data: filtros,
            dataType: 'json'
        }).done(function (response) {
            utils.desHabilitaCtrls(false);
            $('#cargando-resultados').hide();
            data.agrupaciones = response;
            for (var i = 0; i < data.agrupaciones.length; i++) {
                data.agrupaciones[i].id = i + 1;
            }
            var renglones = '';
            if (response.length === 0) {
                renglones = '<tr><td colspan="4" class="text-center">Sin resultados</td></tr>';
                $('#data').html(renglones);
            } else {
                renglones = utils.creaDatatableAgrupaciones(data.agrupaciones);
                $('#data').html(renglones);
                $('#table-resultados-busqueda-agrupaciones').on('click', 'a.link-id', handlers.buscaAgrupados);
            }
            data.busquedaAgrupada = true;
            $('#resultados-busqueda').slideDown();
        }).fail(function () {
            utils.desHabilitaCtrls(false);
            alert('Error: No fue posible realizar la consulta.');
            $('#cargando-resultados').hide();
        });
    };

    handlers.buscaAgrupados = function (e) {
        e.preventDefault();
        var id = e.target.id.split('.')[1];
        var agrupacion = $.segalmex.get(data.agrupaciones, id);
        $('#resultados-busqueda-agrupados').show();
        $('#datos-busqueda-form,#resultados-busqueda').hide();
        $('#cargando-resultados').show();
        $('#data-agrupados').html('');
        if (parseInt(agrupacion.total) > 10000) {
            alert('Solo se podrán visualizar 10,000 registros.');
        }

        var filtros = utils.getFiltros(e);
        filtros.estatus = agrupacion.estatus.clave;
        filtros.ciclo = agrupacion.ciclo.id;
        if (agrupacion.estado) {
            filtros.estado = agrupacion.estado.id;
        } else {
            filtros.tipoFactura = 'global';
        }

        $.ajax({
            url: '/trigo/resources/facturas/maiz/inscripcion/',
            type: 'GET',
            data: filtros,
            dataType: 'json'
        }).done(function (response) {
            $('#resultados-busqueda-agrupados').show();
            $('#datos-busqueda-form,#resultados-busqueda').hide();
            var renglones = '';
            if (response.length === 0) {
                renglones = '<tr><td colspan="5" class="text-center">Sin resultados</td></tr>';
                $('#data-agrupados').html(renglones);
            } else {
                renglones = utils.creaDatatable(response);
                $('#data-agrupados').html(renglones);
                utils.configuraTablaResultados();
                $('#table-resultados-busqueda').on('click', 'a.link-uuid', handlers.muestraDetalle);
            }
            $('#cargando-resultados').hide();
        }).fail(function () {
            alert('Error: No fue posible realizar la consulta.');
            $('#cargando-resultados').hide();
        });
    };

    utils.getFiltros = function (e) {
        var datos = {
            folio: $('#folio').val(),
            estatus: $('#estatus').val(),
            fechaInicio: $('#fecha-inicio-registro').val(),
            fechaFin: $('#fecha-fin-registro').val(),
            rfcEmisor: $('#rfc-emisor').val(),
            rfcReceptor: $('#rfc-receptor').val(),
            validador: $('#validador').val(),
            tipoFactura: $('#tipo-factura').val(),
            ciclo: $('#ciclo').val(),
            uuidTfd: $('#uuid-tfd').val()
        };
        for (var prop in datos) {
            if (datos[prop] === '') {
                delete datos[prop];
            }
        }
        if (datos.estatus === '0') {
            delete datos.estatus;
        }
        if (datos.validador === '0') {
            delete datos.validador;
        }
        if (datos.tipoFactura === '0') {
            delete datos.tipoFactura;
        }
        if (datos.fechaInicio) {
            datos.fechaInicio = $.segalmex.date.fechaToIso(datos.fechaInicio);
        }
        if (datos.fechaFin) {
            datos.fechaFin = $.segalmex.date.fechaToIso(datos.fechaFin);
        }
        if (datos.ciclo === '0') {
            delete datos.ciclo;
        }

        return datos;
    };

    utils.validaFiltros = function (e) {
        $('#resultados-busqueda').hide();
        var errores = [];
        $('.valid-field').valida(errores, false);
        $.segalmex.validaFechaInicialContraFinal(errores, 'fecha-inicio-registro', 'fecha-fin-registro');
        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            utils.desHabilitaCtrls(false);
            $('#cargando-resultados').hide();
            return false;
        }

        return true;
    };

    handlers.muestraDetalle = function (e) {
        e.preventDefault();
        var uuid = e.target.id.split('.')[1];
        $.ajax({
            url: '/trigo/resources/facturas/maiz/inscripcion/' + uuid,
            data: {archivos: true, datos: true, historial: true, comentarios: true,
                uso_factura: true, productor: true, productorCiclo: true},
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            // Mostramos el detalle

            $.segalmex.trigo.inscripcion.factura.vista.muestraInscripcion(response, 'detalle-inscripcion-factura');
            $('#verificacion-inscripcion-factura').html($.segalmex.trigo.inscripcion.factura.vista.muestraVerificaciones(response));
            $('#productor-ciclco-factura').html($.segalmex.trigo.inscripcion.factura.vista.muestraProductorCiclo(response));
            $('#estatus-inscripcion-factura').html($.segalmex.trigo.inscripcion.vista.construyeEstatus(response));
            $('#historial-inscripcion-factura').html($.segalmex.trigo.inscripcion.vista.construyeHistorial(response.historial));
            $('#datos-capturados-inscripcion-factura').html($.segalmex.trigo.inscripcion.vista.construyeDatosCapturados(response.datos));
            $('#comentarios-inscripcion-factura').html($.segalmex.trigo.inscripcion.vista.construyeComentarios(response.comentarios, $.segalmex.common.pagina.comun.registrado));
            $('#pago-inscripcion-factura').html($.segalmex.trigo.inscripcion.factura.vista.muestraPago(response));
            $('#detalle-elemento').show();
            $('#datos-busqueda-form,#resultados-busqueda,#resultados-busqueda-agrupados').hide();
        }).fail(function () {
            alert('Error: No fue posible obtener el registro.');
        });
    };

    handlers.limpia = function (e) {
        utils.limpiar();
    };

    handlers.regresaResultados = function (e) {
        e.preventDefault();
        if (!data.busquedaAgrupada) {
            $('#detalle-elemento').hide();
            $('#datos-busqueda-form,#resultados-busqueda').show();
        }
        if (data.busquedaAgrupada) {
            $('#detalle-elemento').hide();
            $('#resultados-busqueda-agrupados').show();
        }

    };

    handlers.regresaResultadosAgrupaciones = function (e) {
        e.preventDefault();
        $('#resultados-busqueda-agrupados').hide();
        $('#datos-busqueda-form,#resultados-busqueda').show();
    };

    utils.inicializaValidaciones = function () {
        $(".valid-field").configura({required: false});

        $('#fecha-inicio-registro,#fecha-fin-registro').configura({
            type: 'date',
            required: false
        });
        $('#fecha-inicio-registro,#fecha-fin-registro').datepicker({
            language: "es",
            autoclose: true,
            forceParse: false,
            format: 'dd/mm/yyyy',
            yearRange: "c-10:c"
        });
        $('#folio').configura({
            pattern: /^\d{1,6}$/,
            minlength: 1,
            maxlength: 6,
            required: true
        });
        $('#rfc-emisor,#rfc-receptor').configura({
            type: 'rfc',
            required: false
        });
        $('#uuid-tfd').configura({
            pattern: /^[0-9a-f]{8}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{4}\-[0-9a-f]{12}$/,
            minlength: 36,
            maxlength: 36,
            required: false,
            textTransform: null
        });

        $('.valid-field').validacion();
    };

    utils.limpiar = function () {
        $('#estatus').val('0').change();
        $('#datos-busqueda-form input').val('');
        $('#ciclo').val('0').change();
        $('.valid-field').limpiaErrores();
        $('#resultados-busqueda').hide();
        $('#cargando-resultados').hide();
        $('#table-resultados-busqueda tbody').html('<tr><td colspan="6" class="text-center">Sin resultados</td></tr>');
        $('#detalle-inscripcion-contrato,#historial-inscripcion-contrato,#datos-capturados-inscripcion-contrato').html('');
        $('#validador').val('0').change();
        $('#tipo-factura').val('0').change();
    };

    //Solo si buscara por validador o estatus
    utils.cargaCatalogos = function () {

        $.ajax({
            url: '/trigo/resources/paginas/consultas',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            $('#button-exportar-ciclo,#button-flecha,#button-exportar-ciclo-planeacion').prop('disabled', true);
            $('#validador').actualizaCombo(response.validadores);
            $('#estatus').actualizaCombo(response.estatus, {value: 'clave'});
            $('#ciclo').actualizaCombo(response.ciclos);
            data.ciclos = response.ciclos;
        }).fail(function () {
            alert('Error: No se pudo obtener los catálogos de la página.');
        });

    };

    utils.creaDatatable = function (inscripciones) {
        var buffer = [];
        buffer.push('<table id="table-resultados-busqueda"');
        buffer.push(' class="table table-striped" width="100%">');
        buffer.push('<thead class = "table-success">');
        buffer.push('<tr class="success">');
        buffer.push('<th>No.</th>');
        buffer.push('<th>Folio</th>');
        buffer.push('<th>Fecha de registro</th>');
        buffer.push('<th>Emisor</th>');
        buffer.push('<th>RFC emisor</th>');
        buffer.push('<th>Receptor</th>');
        buffer.push('<th>RFC Receptor</th>');
        buffer.push('<th>Tipo</th>');
        buffer.push('<th>Estatus</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');
        for (var i = 0; i < inscripciones.length; i++) {
            var inscripcion = inscripciones[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push('</td>');
            buffer.push('<td><a class="link-uuid" href="#" id="uuid.' + inscripcion.uuid + '">');
            buffer.push(inscripcion.folio);
            buffer.push('</a></td>');
            buffer.push('<td>');
            buffer.push($.segalmex.date.isoToFecha(inscripcion.fechaCreacion));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(inscripcion.cfdi.nombreEmisor);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(inscripcion.cfdi.rfcEmisor);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(inscripcion.cfdi.nombreReceptor);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(inscripcion.cfdi.rfcReceptor);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(inscripcion.tipoFactura.charAt(0).toUpperCase() + inscripcion.tipoFactura.slice(1));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(inscripcion.estatus.nombre);
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        buffer.push('</tbody>');
        buffer.push('</table>');

        buffer.push('<br/>');
        return buffer.join('');

    };

    utils.configuraTablaResultados = function () {
        var aoColumns = [
            {"bSortable": false}, // No.
            {"bSortable": true}, // Folio
            {"bSortable": false}, // Fecha registro
            {"bSortable": true}, // Nombre emisor
            {"bSortable": true}, // RFC emisor
            {"bSortable": true}, // Nombre receptor
            {"bSortable": true}, // RFC receptor
            {"bSortable": true}, // Tipo
            {"bSortable": true} // Estatus
        ];
        utils.configuraTablaFactura('table-resultados-busqueda', aoColumns);
    };

    utils.configuraTablaFactura = function (tablaId, aoColumns) {
        var table = $('#' + tablaId).DataTable({
            "oLanguage": {
                "sEmptyTable": "No se encontró información para mostrar en la tabla",
                "sSearch": "Buscar ",
                "sZeroRecords": "No se encontraron resultados."
            },
            "bFilter": true,
            "bInfo": false,
            "bPaginate": false,
            "aoColumns": aoColumns,
            "aoColumnDefs": [{"bSortable": false, "aTargets": ["no-sort"]}]
        });
        table.on('order.dt search.dt', function () {
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).order([[1, 'asc']]).draw(false);
        $("#" + tablaId + "_filter input").addClass('input-buscar');
        $(".dataTables_empty").attr("colspan", "100%");
    };

    handlers.cambiaBandeja = function (e) {
        e.preventDefault();
        utils.limpiar();
        $('#div-generales,#div-folio').hide();
        var id = e.target.id.substring('muestra-'.length);
        $('#menu-bandejas a.nav-link').removeClass('active');
        $(e.target).addClass('active');
        $('#' + id).show();
        $('#button-exportar-ciclo,#button-flecha,#button-exportar-ciclo-planeacion').prop('disabled', true);
        utils.ocultaAgrupado(id);
    };

    utils.ocultaAgrupado = function (bandeja) {
        if (bandeja !== 'div-generales') {
            $('#button-buscar-agrupado').hide();
        } else {
            $('#button-buscar-agrupado').show();
        }
    };

    utils.creaDatatableAgrupaciones = function (resultados) {
        var buffer = [];
        buffer.push('<table id="table-resultados-busqueda-agrupaciones"');
        buffer.push(' class="table table-striped" width="100%">');
        buffer.push('<thead class = "table-success">');
        buffer.push('<tr class="success">');
        buffer.push('<th>Ciclo</th>');
        buffer.push('<th>Estado</th>');
        buffer.push('<th>Estatus</th>');
        buffer.push('<th>Total</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');
        var total = 0;
        for (var a = 0; a < resultados.length; a++) {
            var resultado = resultados[a];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(resultado.ciclo.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.estado ? resultado.estado.nombre : '--');
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.estatus.nombre);
            buffer.push('</td>');
            buffer.push('<td class="text-right"><a class="link-id" href="#" id="id.' + resultado.id + '">');
            buffer.push(resultado.total);
            buffer.push('</a></td>');
            buffer.push('</tr>');
            total += parseInt(resultado.total);
        }
        buffer.push('<tfoot class="table-success">');
        buffer.push('<tr>');
        buffer.push('<th colspan="3">Total</th>');
        buffer.push('<th class="text-right">' + total + '</th>');
        buffer.push('</tr>');
        buffer.push('</tfoot>');
        buffer.push('</tbody>');
        buffer.push('</table>');

        buffer.push('<br/>');
        return buffer.join('');

    };

    utils.configuraCiclo = function (requerido) {
        $('#ciclo').configura({
            required: requerido
        });
        $('#ciclo').validacion();
    };

})(jQuery);