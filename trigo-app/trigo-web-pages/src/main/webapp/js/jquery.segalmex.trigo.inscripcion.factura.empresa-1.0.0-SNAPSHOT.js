/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.trigo.inscripcion.factura.empresa');
    var data = {
        bandeja: 'recibidas',
        toneladasRecibidas: 0.0,
        toneladasVendidas: 0.0,
        toneladasAlmacenadas: 0.0,
        limiteArchivo: 24 * 1024 * 1024
    };
    var handlers = {};
    var utils = {};

    $.segalmex.trigo.inscripcion.factura.empresa.init = function () {
        $('#menu-bandejas a.nav-link').click(handlers.cambiaBandeja);
        $('#nueva-factura-button').click(handlers.nuevaFactura);
        $('#nuevo-documento-button').click(handlers.nuevoDocumento);
        $('#adjuntar-button').click(handlers.adjunta);
        $('#guardar-button').click(handlers.guarda);
        $('#regresar-button').click(handlers.regresa);
        $('.regresar-nuevo-button').click(handlers.regresa);
        $('#guardar-documento-button').click(handlers.guardaDocumento);
        $('input.form-control-file').change(handlers.verificaTamanoArchivo);
        utils.cargaFacturasEmitidas();
        utils.cargaFacturasRecibidas();
        utils.cargaDocumentosComerciales();
        utils.cargaCatalogos();
    };

    handlers.cambiaBandeja = function (e) {
        e.preventDefault();
        var id = e.target.id.substring('muestra-'.length);
        if (data.bandeja === id) {
            return;
        }
        data.bandeja = id;
        $('#recibidas-div,#emitidas-div,#documentos-div').hide();
        $('#' + data.bandeja + '-div').show();
        $('#menu-bandejas a.nav-link').removeClass('active');
        $(e.target).addClass('active');
    };

    handlers.nuevaFactura = function (e) {
        e.preventDefault();
        $('#recibidas-emitidas-div').hide();

        $('#factura-xml,#cfdi-pdf').val('');
        $('#archivo-xml-factura').show();
        $('#datos-factura').hide();
        $('#nueva-div').show();
        $('#adjuntar-button').prop('disabled', false);
    };

    handlers.nuevoDocumento = function (e) {
        $('#toneladas-documento').configura({
            required: true,
            type: 'number',
            min: 0
        });
        $('#tipo-documento').configura({
            required: true
        });
        $('#toneladas-documento,#tipo-documento').validacion();
        $('#recibidas-emitidas-div').hide();
        $('#tipo-documento').val('0');
        $('#toneladas-documento,#documento-pdf').val('');
        $('#nuevo-documento-div .valid-field').limpiaErrores();
        $('#nuevo-documento-div').show();
        $('#guardar-documento-button').prop('disabled', false);
    };

    handlers.adjunta = function (e) {
        e.preventDefault();
        $(e.target).prop('disabled', true);

        if ($('#factura-xml').val() === '') {
            alert('Error:\n\n * CFDI (XML) es requerido.');
            return;
        }

        var fd = new FormData();
        var archivo = $('#factura-xml')[0].files[0];
        fd.append('file', archivo);

        var tipo = '/global-empresa';
        $.ajax({
            url: '/trigo/resources/facturas/cfdi/' + tipo,
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false
        }).done(function (response) {
            utils.muestraCfdi(response);
            data.cfdi = response;
            // Limpiamos uuid y registrada
            data.uuid = null;
            data.registrada = null;
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Error: No se puedo agregar la factura, verifique el xml.');
            }
            $(e.target).prop('disabled', false);
        });
    };

    handlers.guarda = function (e) {
        e.preventDefault();
        if ($('#cfdi-pdf').val() === '') {
            alert('Error: CFDI (PDF) es requerido.');
            return;
        }

        var inscripcion = {
            cfdi: {uuid: data.cfdi.uuid},
            ciclo: {id: $("#ciclo-agricola").val()},
            claveArchivos: utils.getClaveCfdi()
        };
        if (data.uuid) {
            inscripcion.uuid = data.uuid;
        }

        var tipo = 'global-empresa';
        $.ajax({
            type: 'POST',
            url: '/trigo/resources/facturas/maiz/inscripcion/' + tipo,
            data: JSON.stringify(inscripcion),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            data.uuid = response.uuid;
            data.registrada = response.registrada;
            utils.subeArchivos();
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Error: La factura no se pudo registrar.');
            }
        });
    };

    utils.getClaveCfdi = function () {
        var files = $('#cfdi-pdf');

        var claves = [];
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            var id = file.id;
            var tipo = id.substring(0, id.length - '-pdf'.length);
            claves.push(tipo);
        }
        return claves.join(',');
    };

    handlers.regresa = function (e) {
        $('.nuevo-div').hide();
        $('#recibidas-emitidas-div').show();
    };

    handlers.guardaDocumento = function (e) {
        e.preventDefault();
        $(e.target).prop('disabled', true);
        var errores = [];
        $('#toneladas-documento,#tipo-documento').limpiaErrores();
        $('#toneladas-documento,#tipo-documento').valida(errores, false);
        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            $(e.target).prop('disabled', false);
            return;
        }
        if ($('#documento-pdf').val() === '') {
            alert('Adjunte un documento.');
            $(e.target).prop('disabled', false);
            return;
        }

        var documentoComercail = {
            tipo: $("#tipo-documento").val(),
            ciclo: {id: $("#ciclo-agricola").val()},
            cantidad: $('#toneladas-documento').val(),
            claveArchivos: 'documento-comercial'
        };
        $.ajax({
            url: '/trigo/resources/documento-comercial/maiz/inscripcion/',
            type: 'POST',
            data: JSON.stringify(documentoComercail),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            data.uuid = response.uuid;
            var fd = new FormData();
            var archivo = $('#documento-pdf')[0].files[0];
            fd.append('file', archivo);
            fd.tipo = 'documento-comercial';
            $.ajax({
                url: '/trigo/resources/documento-comercial/maiz/inscripcion/' + data.uuid + '/anexos/' + fd.tipo + '/pdf/',
                type: 'POST',
                data: fd,
                contentType: false,
                processData: false
            }).done(function (responseD) {
                utils.cargaDocumentosComerciales();
                alert('Se registró el documento correctamente.');
                $('#nuevo-documento-div').hide();
                $('#recibidas-emitidas-div').show();
                $(e.target).prop('disabled', false);
            }).fail(function () {
                alert('Error al subir el archivo del documento.');
                $(e.target).prop('disabled', false);
            });
        }).fail(function () {
            alert('Error: no fue posible registrar el documento.');
            $(e.target).prop('disabled', false);
        });
    };


    utils.subeArchivos = function () {
        $('#factura-xml').prop('disabled', true);
        var files = $('#cfdi-pdf');
        $('#factura-xml').prop('disabled', false);

        // Subimos archivos del documento
        for (var i = 0; i < files.length; i++) {
            var file = files[i];

            var id = file.id;
            var fd = new FormData();
            var archivo = $('#' + id)[0].files[0];
            fd.append('file', archivo);
            var tipo = id.substring(0, id.length - '-pdf'.length);

            $.ajaxq('archivosQueue', {
                url: '/trigo/resources/facturas/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/',
                type: 'POST',
                data: fd,
                contentType: false,
                processData: false
            }).done(function (response) {
                if (data.registrada) {
                    alert('Advertencia: La factura ya estaba registrada previamente.');
                } else {
                    alert('La factura ha sido registrada.');
                }
                utils.limpiar();
            }).fail(function () {
            });
        }
    }

    utils.cargaFacturasRecibidas = function () {
        $.ajaxq('facturasQueue', {
            url: '/trigo/resources/facturas/empresa/recibidas/',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            $('#recibidas-div').html(utils.construyeRecibidas(response));
        }).fail(function () {
            alert('Error: No fue posible realizar la consulta.');
        });
    };

    utils.construyeRecibidas = function (facturas) {
        var buffer = [];
        buffer.push('<table id="recibidas-table" class="table table-striped table-bordered table-hover">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<tr>');
        buffer.push('<th>#</th>');
        buffer.push('<th>Fecha</th>');
        buffer.push('<th>Nombre emisor</th>');
        buffer.push('<th>Tipo</th>');
        buffer.push('<th>Toneladas</th>');
        buffer.push('<th>Total</th>');
        buffer.push('<th>Estatus</th>');
        buffer.push('</tr></thead>');
        buffer.push('<tbody>');
        var toneladas = 0;
        var total = 0;

        if (facturas.length > 0) {
            for (var i = 0; i < facturas.length; i++) {
                var f = facturas[i];
                buffer.push('<tr>');
                buffer.push('<td>');
                buffer.push(i + 1);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push($.segalmex.date.isoToFecha(f.fecha));
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(f.nombreEmisor);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(f.tipo);
                buffer.push('</td>');
                buffer.push('<td class="text-right">');
                buffer.push(f.toneladasTotales.toFixed(3));
                buffer.push('</td>');
                buffer.push('<td class="text-right">');
                buffer.push(f.total.toFixed(3));
                buffer.push('</td>');
                buffer.push('<td class="text-center">');
                buffer.push('<span class="text-success"><i class="fas fa-check-circle"></i></span>');
                buffer.push('</td>');
                buffer.push('</tr>');
                data.toneladasRecibidas += f.toneladasTotales;
                toneladas += f.toneladasTotales;
                total += f.total;
            }
        } else {
            buffer.push('<tr><td colspan="7">Sin registros</td></tr>');
        }

        buffer.push('</tbody>');
        buffer.push('<tfoot class="thead-dark">');
        buffer.push('<tr>');
        buffer.push('<th colspan="4"></th>');
        buffer.push('<th class="text-right">' + utils.format(toneladas.toFixed(3)) + '</th>');
        buffer.push('<th class="text-right">' + utils.format(total.toFixed(3)) + '</th>');
        buffer.push('<th colspan="1"></th>');
        buffer.push('</tr>');
        buffer.push('</tfoot>');
        buffer.push('</table>');
        utils.calculaAlmacenadas();
        return buffer.join('');
    };

    utils.cargaFacturasEmitidas = function () {
        $.ajaxq('facturasQueue', {
            url: '/trigo/resources/facturas/empresa/emitidas/',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            $('#emitidas-div').html(utils.construyeEmitidas(response));
        }).fail(function () {
            alert('Error: No fue posible realizar la consulta.');
        });
    };

    utils.construyeEmitidas = function (facturas) {
        var buffer = [];
        buffer.push('<table id="emitidas-table" class="table table-striped table-bordered table-hover">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<tr>');
        buffer.push('<th>#</th>');
        buffer.push('<th>Fecha</th>');
        buffer.push('<th>Nombre receptor</th>');
        buffer.push('<th>Tipo</th>');
        buffer.push('<th>Toneladas</th>');
        buffer.push('<th>Total</th>');
        buffer.push('<th>Estatus</th>');
        buffer.push('</tr></thead>');
        buffer.push('<tbody>');
        var toneladas = 0;
        var total = 0;
        if (facturas.length > 0) {
            for (var i = 0; i < facturas.length; i++) {
                var f = facturas[i];
                buffer.push('<tr>');
                buffer.push('<td>');
                buffer.push(i + 1);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push($.segalmex.date.isoToFecha(f.fecha));
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(f.nombreReceptor);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(f.tipo);
                buffer.push('</td>');
                buffer.push('<td class="text-right">');
                buffer.push(f.toneladasTotales.toFixed(3));
                buffer.push('</td>');
                buffer.push('<td class="text-right">');
                buffer.push(f.total.toFixed(3));
                buffer.push('</td>');
                buffer.push('<td class="text-center">');
                buffer.push('<span class="text-success"><i class="fas fa-check-circle"></i></span>');
                buffer.push('</td>');
                buffer.push('</tr>');
                data.toneladasVendidas += f.toneladasTotales;
                toneladas += f.toneladasTotales;
                total += f.total;
            }
        } else {
            buffer.push('<tr><td colspan="7">Sin registros</td></tr>');
        }

        buffer.push('</tbody>');
        buffer.push('<tfoot class="thead-dark">');
        buffer.push('<tr>');
        buffer.push('<th colspan="4"></th>');
        buffer.push('<th class="text-right">' + utils.format(toneladas.toFixed(3)) + '</th>');
        buffer.push('<th class="text-right">' + utils.format(total.toFixed(3)) + '</th>');
        buffer.push('<th colspan="1"></th>');
        buffer.push('</tr>');
        buffer.push('</tfoot>');
        buffer.push('</table>');
        return buffer.join('');
    };

    utils.format = function (str) {
        return str.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    };

    utils.calculaAlmacenadas = function () {
        data.toneladasAlmacenadas = data.toneladasRecibidas - data.toneladasVendidas;
        if (data.toneladasAlmacenadas < 0) {
            data.toneladasAlmacenadas = 0.0;
        }
        $('#toneladas-recibidas').val(data.toneladasRecibidas.toFixed(3));
        $('#toneladas-vendidas').val(data.toneladasVendidas.toFixed(3));
        $('#toneladas-almacenadas').val(data.toneladasAlmacenadas.toFixed(3));
    };

    utils.muestraCfdi = function (cfdi) {
        $('#rfc-emisor').val(cfdi.rfcEmisor);
        $('#nombre-emisor').val(cfdi.nombreEmisor);
        $('#rfc-receptor').val(cfdi.rfcReceptor);
        $('#nombre-receptor').val(cfdi.nombreReceptor);
        $('#uuid-factura').val(cfdi.uuidTimbreFiscalDigital);
        $('#fecha-factura').val(cfdi.fecha ? cfdi.fecha.substring(0, cfdi.fecha.length - 6).replace('T', ' ') : '');
        $('#fecha-timbrado-factura').val(cfdi.fechaTimbrado ? cfdi.fechaTimbrado.substring(0, cfdi.fechaTimbrado.length - 6).replace('T', ' ') : '');
        $('#metodo-pago-factura').val(cfdi.metodoPago);
        $('#moneda-factura').val(cfdi.moneda);
        $('#total-factura').val(cfdi.total);
        $('#factura-persona-moral').hide();

        var buffer = [];
        for (var i = 0; i < cfdi.conceptos.length; i++) {
            var c = cfdi.conceptos[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-center">');
            buffer.push(i + 1)
            buffer.push('</td>');
            buffer.push('<td class="text-center">');
            buffer.push(c.cantidad);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(c.unidad);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(c.descripcion);
            buffer.push('</td>');
            buffer.push('<td class="text-center">');
            buffer.push(c.valorUnitario);
            buffer.push('</td>');
            buffer.push('<td class="text-center">');
            buffer.push(c.importe);
            buffer.push('</td>');

            buffer.push('</tr>');
        }
        $('#conceptos-table tbody').html(buffer.join(''));
        $('#archivo-xml-factura').hide();
        $('#datos-factura').show();
    };

    utils.limpiar = function () {
        $('#nueva-div').hide();
        $('#datos-factura').hide();
        $('#recibidas-emitidas-div').show();
    };

    utils.cargaDocumentosComerciales = function () {
        $.ajaxq('facturasQueue', {
            url: '/trigo/resources/documento-comercial/maiz/recibidos/',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            $('#documentos-div').html(utils.construyeDocumentos(response));
        }).fail(function () {
            alert('Error: No fue posible consultar los documentos comerciales.');
        });
    };

    utils.construyeDocumentos = function (documentos) {
        var buffer = [];
        buffer.push('<table id="documentos-table" class="table table-striped table-bordered table-hover">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<tr>');
        buffer.push('<th>#</th>');
        buffer.push('<th>Fecha</th>');
        buffer.push('<th>Folio</th>');
        buffer.push('<th>Tipo de documento</th>');
        buffer.push('<th>Cantidad</th>');
        buffer.push('</tr></thead>');
        buffer.push('<tbody>');
        var total = 0;

        if (documentos.length > 0) {
            for (var i = 0; i < documentos.length; i++) {
                var d = documentos[i];
                buffer.push('<tr>');
                buffer.push('<td>');
                buffer.push(i + 1);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push($.segalmex.date.isoToFecha(d.fechaCreacion));
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(d.folio);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(d.tipo === 'certificado-deposito' ? 'Certificado de depósito' : 'Inventario formal');
                buffer.push('</td>');
                buffer.push('<td class="text-right">');
                buffer.push(d.cantidad.toFixed(3));
                buffer.push('</td>');
                buffer.push('</tr>');
                total += d.cantidad;
            }
        } else {
            buffer.push('<tr><td colspan="5">Sin registros</td></tr>');
        }

        buffer.push('</tbody>');
        buffer.push('<tfoot class="thead-dark">');
        buffer.push('<tr>');
        buffer.push('<th colspan="4"></th>');
        buffer.push('<th class="text-right">' + utils.format(total.toFixed(3)) + '</th>');
        buffer.push('</tr>');
        buffer.push('</tfoot>');
        buffer.push('</table>');
        return buffer.join('');
    };

    utils.cargaCatalogos = function () {
        $.ajax({
            type: 'GET',
            url: '/trigo/resources/paginas/inscripcion-factura/empresa',
            dataType: 'json'
        }).done(function (response) {
            $('#ciclo-agricola-global').cicloAgricola({
                ciclos: response.ciclos,
                actual: response.cicloSeleccionado,
                reload: true,
                sistema: 'trigo'
            });
        }).fail(function () {
            alert('Error: Al descargar los recursos de la página.');
        });
    };

    handlers.verificaTamanoArchivo = function (e) {
        var extension = e.target.id.includes('xml') ? 'xml' : 'pdf';
        $.segalmex.archivos.verificaArchivo(e, extension, data.limiteArchivo);
    };
})(jQuery);
