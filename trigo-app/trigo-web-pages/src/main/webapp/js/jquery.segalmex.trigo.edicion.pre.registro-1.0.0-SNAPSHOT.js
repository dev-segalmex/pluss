/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.trigo.edicion.pre.registro');
    var data = {};
    var handlers = {};
    var utils = {};

    $.segalmex.trigo.edicion.pre.registro.init = function (params) {
        $.segalmex.cultivos.edicion.pre.registro.init({
            cultivo: 'trigo'
        });
        $('#button-buscar').click($.segalmex.cultivos.edicion.pre.registro.busca);
        $('#button-limpiar').click($.segalmex.cultivos.edicion.pre.registro.limpiar);
        $('#regresar-button').click($.segalmex.cultivos.edicion.pre.registro.regresar);
        $('#total-contratos').change($.segalmex.cultivos.edicion.pre.registro.habilitaTipoPrecio);
        $('#agregar-predio-button').click($.segalmex.cultivos.edicion.pre.registro.agregaPredio);
        $('#predios-table').on('click', 'button.eliminar-predio', $.segalmex.cultivos.edicion.pre.registro.eliminaPredio);
        $('#estado-predio').change($.segalmex.cultivos.edicion.pre.registro.cambiaEstadoPredio);
        $('#cobertura').change($.segalmex.cultivos.edicion.pre.registro.validaSiCobertura);
        $('#actualizar-button').click($.segalmex.cultivos.edicion.pre.registro.actualiza);
        $('#agregar-contrato-button').click($.segalmex.cultivos.edicion.pre.registro.agregaTipoPrecio);
        $('#precios-table').on('click', 'button.eliminar-precio', $.segalmex.cultivos.edicion.pre.registro.eliminaTipoPrecio);
        $('#tipo-iar').change($.segalmex.cultivos.edicion.pre.registro.validaTipoCobertura);
        $('#curp').change($.segalmex.cultivos.edicion.pre.registro.validaCurpRfc);
        $('#rfc').change($.segalmex.cultivos.edicion.pre.registro.validaCurpRfc);
        $('#tipo').change($.segalmex.cultivos.edicion.pre.registro.filtraEstados);
        $('#agregar-iar-button').click($.segalmex.cultivos.edicion.pre.registro.agregaIar);
        $('#iars-table').on('click', 'button.eliminar-iar', $.segalmex.cultivos.edicion.pre.registro.eliminaIar);
    };
})(jQuery);