/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($) {
    $.segalmex.namespace('segalmex.trigo.terminos.productor');

    var utils = {};
    var handlers = {};

    $.segalmex.trigo.terminos.productor.getTerminos = function (clave){
        var terminos = '';
        switch (clave) {
            case 'oi-2022':
                terminos =
                `<p>A través del presente registro, autorizo que los predios
                        referidos e información productiva y personal, sean considerados en el
                        Programa de Precios de Garantía a Productos Alimentarios Básicos. Me obligo a
                        proporcionar la información y documentación que me sea requerida por la SADER-SEGALMEX
                        y a notificar cualquier cambio que sufra la información proporcionada. A la vez,
                        manifiesto bajo protesta de, decir la verdad que los datos contenidos en este
                        registro son ciertos, y que adquirí un Instrumento de Administración de Riesgos (IAR).
                        Acepto la responsabilidad, en la veracidad de la información proporcionada a
                        SEGALMEX. En caso de incumplimiento total o parcial, así como de SIMULACIÓN, me
                        comprometo a devolver sin reserva alguna el incentivo recibido y a aceptar la
                        sanción administrativa que conforme a derecho proceda. Soy consciente que, en
                        caso de alteración de documentación y firmas, mi registro será cancelado. También
                        me comprometo a que en caso de que me realicen algún pago en DEMASÍA, lo
                        reintegraré completamente, a través de un depósito bancario.</p>
                <p>Estoy de acuerdo
                        en beneficiarme con el precio de Garantía del ciclo agrícola vigente, publicado
                        en el Diario Oficial de la federación 2022 y la mecánica operativa de TRIGO del
                        ciclo correspondiente. Firmo mi registro que ampara el total de toneladas de
                        trigo (grano o semilla) que comercializaré con la industria alimenticia en el
                        ciclo mencionado, y estarán reflejadas en el sistema SEGALMEX. Existirá incentivo
                        para el IAR en trigo cristalino y panificable. En caso de entrar en MODALIDAD 2 y
                        ser productor de panificable, se me brindará un incentivo para alcanzar el
                        Precio de Garantía. Lo anterior también aplica para semilla de trigo panificable
                        certificada por el SNICS. Estoy consciente que entraré en un buró de incumplimiento,
                        que limita mi participación, si no cumplo con lo estipulado en el Contrato de
                        Compra Venta firmado. Los datos personales recabados serán protegidos,
                        incorporados y tratados en el marco de la Ley General de Protección de
                        Documentos Personales en Posesión de Sujetos Obligados, con fundamento en los
                        artículos 3, 27 y 28.</p>`;
                break;
            case 'pv-2021':
                terminos =
                        `<p>
                        A través del presente registro, autorizo que los predios referidos e información productiva y personal, sean considerados en el
                        Programa de Precios de Garantía a Productos Alimentarios Básicos. Me obligo a proporcionar la información y documentación que
                        me sea requerida por la SADER-SEGALMEX y a notificar cualquier cambio que sufra la información proporcionada. A la vez,
                        manifiesto bajo protesta de, decir la verdad que los datos contenidos en este registro son ciertos. Acepto la responsabilidad,
                        en la veracidad de la información proporcionada a SEGALMEX. En caso de incumplimiento total o parcial, así como de SIMULACIÓN,
                        me comprometo a devolver sin reserva alguna el incentivo recibido y a aceptar la sanción administrativa que conforme a derecho
                        proceda. Soy consciente que, en caso de alteración de documentación y firmas, mi registro será cancelado. También me comprometo
                        a que en caso de que me realicen algún pago en DEMASÍA, lo reintegraré completamente, a través de un depósito bancario.
                        Estoy de acuerdo en beneficiarme con el precio de Garantía del ciclo agrícola vigente, publicado en el Diario Oficial de la
                        federación 2022 y la mecánica operativa de TRIGO del ciclo correspondiente. Existen precios de garantía diferenciados de
                        acuerdo a la superficie (pequeños y medianos). Al productor pequeño, de hasta 8 hectáreas, se le apoyarán máximo 50 ton.
                        Al productor mediano se le incentivarán máximo 300 toneladas. Firmo mi registro que ampara el total de toneladas de
                        trigo (grano o semilla) que obtendré en la cosecha del ciclo mencionado, y estarán reflejadas en el sistema SEGALMEX.
                        Se me brindará un incentivo para alcanzar el Precio de Garantía solamente si el Precio de referencia es menor al de garantía.
                        Lo anterior también aplica para semilla de trigo panificable certificada por el SNICS. Estoy consciente que entraré en un
                        buró de incumplimiento, que limita mi participación, si no cumplo con lo estipulado en el Contrato de Compra Venta firmado.
                        </p>
                        <p>
                        Los datos personales recabados serán protegidos, incorporados y tratados en el marco de la Ley General de Protección de
                        Documentos Personales en Posesión de Sujetos Obligados, con fundamento en los artículos 3, 27 y 28.
                        </p>`;
                break;
            case 'oi-2021':
                terminos =
                        `
                        <p>A través del presente registro, autorizo que los predios referidos e información productiva y personal,
                        sean considerados en el Programa de <strong>Precios de Garantía a Productos Alimentarios Básicos</strong>. Me obligo a proporcionar
                        la información y/o documentación que me sea requerida por la SADER-SEGALMEX y a notificar cualquier cambio que sufra
                        la información proporcionada. A la vez, manifiesto bajo protesta de decir la verdad que, <strong>los datos contenidos en este
                        registro son ciertos y reales, y que adquirí un Instrumento de Administración de Riesgos (IAR) o tengo un Contrato a
                        Precio Fijo</strong>. Acepto la responsabilidad, en la veracidad de la información proporcionada a SEGALMEX. En caso de falsificar
                        total o parcialmente la información o realizar <strong>SIMULACIÓN</strong>, me comprometo a devolver sin reserva alguna el incentivo
                        recibido y a aceptar la sanción administrativa que conforme a derecho proceda <strong>(CANCELACIÓN)</strong>. En caso de alterar la
                        documentación y/o firmas, mi registro será cancelado. En situaciones de <strong>PAGO(S) EN DEMASÍA</strong> (en cumplimiento a las
                        obligaciones aceptadas en este registro), me comprometo a reintegrar el recurso, a través de depósito bancario a “SEGALMEX”.
                        Estoy de acuerdo en beneficiarme con el Precio de Garantía y/o Incentivo para los granos de trigo, publicado en las Reglas
                        de Operación del Programa de Precios de Garantía a Productos Alimentarios Básicos, para el ejercicio fiscal 2021 y en la
                        Mecánica Operativa para los granos de trigo panificable, trigo cristalino y semilla certificada para consumo humano, ciclo
                        otoño-invierno 2020-2021. Envío mi registro <strong>FIRMADO por mi puño y letra</strong>, que ampara el total de toneladas de trigo
                        (grano o semilla) que cosecharé en el ciclo mencionado, y estarán reflejadas en el sistema <strong>SEGALMEX-Trigo</strong>.
                        En trigo panificable o semilla certificada para consumo humano, se me beneficiará con el incentivo para la adquisición de
                        IAR ($100/t), y en caso de entrar en MODALIDAD 2, adicionalmente se me brindará un incentivo para alcanzar el Precio de
                        Garantía de $5,790/t (100% del incentivo en las primeras 100 toneladas y el 50% del incentivo a las 200 toneladas adicionales).
                        En trigo cristalino, seré beneficiado únicamente con el incentivo para el IAR ($100/t) hasta las 150 toneladas, y sólo aplica en
                        los estados de Baja California, Sonora y el Bajío. Estoy consciente que podré ser incluido en un <strong>BURÓ DE INCUMPLIMIENTO</strong>, que puede
                        limitar mi participación, si no cumplo con lo estipulado en el Contrato de Compra Venta (CCV) firmado. Considero que puedo recibir
                        o no el incentivo con la información presentada, soportada y de acuerdo a los criterios de elegibilidad.</p>
                        <p>Los datos personales recabados serán protegidos, incorporados y tratados en el marco de la Ley General de Protección de
                        Documentos Personales en Posesión de Sujetos Obligados, con fundamento en los artículos 3, 27 y 28.</p>
                        `;
                break;
            default:
                terminos =
                        `<p>A través del presente registro, autorizo que los predios referidos sean considerados en el
                        Programa de Precios de Garantía a Productos Alimentarios Básicos. Me obligo a proporcionar
                        la información y/o documentación que me sea requerida por la SADER-SEGALMEX y a notificar
                        cualquier cambio que sufra la información proporcionada. A la vez, manifiesto
                        <strong>bajo protesta de decir la verdad</strong> que los datos contenidos en este registro
                        son ciertos y reales, por lo que acepto mi responsabilidad, en la veracidad de la información
                        proporcionada a SEGALMEX. En caso de incumplimiento total o parcial, así como de
                        <strong>SIMULACIÓN</strong>, me comprometo a devolver sin reserva alguna el incentivo
                        recibido y a aceptar la sanción administrativa que conforme a derecho proceda. Soy consciente
                        que, en caso de alteración de documentación y firmas, mi registro será cancelado. También me
                        comprometo a que en caso de que me realicen algún <strong>PAGO EN DEMASÍA</strong>, lo
                        reintegraré completamente, a través de un depósito bancario. Estoy de acuerdo en beneficiarme con el precio
                        de Garantía del ciclo agrícola <a class="nombre-ciclo"> </a>, publicados en el Diario Oficial de la
                        Federación 2020 y la mecánica operativa de TRIGO <a class="nombre-ciclo"> </a>. Envío mi registro que ampara el total de
                        toneladas de trigo que obtendré en la cosecha del ciclo mencionado, y estarán reflejadas
                        en el sistema SEGALMEX, cuyo monto del incentivo es la diferencia entre el Precio de Garantía de <strong>$5,790</strong> por
                        tonelada y el precio de mercado establecido como referencia por SEGALMEX. Tengo en conocimiento que de hasta 100
                        toneladas recibiré el incentivo completo (100%), y de 200 toneladas adicionales a las primeras 100 toneladas podré
                        recibir el 50% del incentivo. Lo anterior también aplica en caso de entregar semilla de trigo panificable certificada por el SNICS.
                        En caso de entregar trigo cristalino, solo podré recibir el 40% del incentivo que se brinda por el grano de trigo panificable
                        y aplica el incentivo solo para estados altamente productivos (Sonora, Baja California y el Bajío).</p>
                      <p>Los datos personales recabados serán protegidos, incorporados y tratados en el marco de la
                        Ley General de Protección de Documentos Personales en Posesión de Sujetos Obligados,
                        con fundamento en los artículos 3, 27 y 28.</p>
                        `;
        }
        return terminos;
    };


 })(jQuery);

