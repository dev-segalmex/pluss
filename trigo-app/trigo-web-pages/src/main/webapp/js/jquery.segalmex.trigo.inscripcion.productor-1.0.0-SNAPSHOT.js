/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.trigo.inscripcion.productor');
    var data = {
        contratos: [],
        predios: [],
        anexosPredio: [],
        sociedades: [],
        anexosActaConstitutiva: [],
        anexosListaSocios: [],
        contratosRegistrados: [],
        socios: [],
        rendimientos: {
        },
        volumenPredio: 0,
        superficiePredio: 0,
        snics: {foliosSnics: ''},
        tipoPersona: null,
        limiteArchivo: 24 * 1024 * 1024,
        solicitado: false,
        volumenMaximo: 0,
        rendimientoMaximo: 0,
        anexosPrediosolicitado: [],
        estadoActual: null,
        rendimientoSolValido: false,
        contratoRequerido: false,
        cicloActual: {}
    };
    var handlers = {};
    var utils = {};

    $.segalmex.trigo.inscripcion.productor.init = function (params) {
        utils.cargaCatalogos();
        utils.configuraPantalla();
    };

    utils.limpiaProductor = function () {
        $('#div-moral,#div-fisica').hide();
        $('#productor-persona-moral input,#productor-persona-fisica input,#uso-factura-persona-moral input,#socios-persona-moral input').prop('disabled', true).val('');
        $('#productor-persona-moral select,#productor-persona-fisica select,#uso-factura-persona-moral select').prop('disabled', true).val('0');
        $('#agregar-sociedad-button,#agregar-socio-button').prop('disabled', true);
        $('#menu-tabs a.nav-link').removeClass('active');
        $('#productor-persona-moral .valid-field').limpiaErrores();
        $('#div-moral .valid-field').limpiaErrores();
        $('#div-fisica .valid-field').limpiaErrores();
        data.tipoPersona = null;
    };

    utils.resetProductor = function () {
        $('#muestra-div-fisica').addClass('active');
        $('#muestra-div-fisica').click();
    };

    utils.resetPredios = function () {
        $('#muestra-punto-medio-predio').addClass('active');
        $('#muestra-punto-medio-predio').click();
    };

    handlers.cambiaTipoProductor = function (e) {
        e.preventDefault();
        utils.limpiaProductor();
        var id = e.target.id.substring('muestra-'.length);
        $(e.target).addClass('active');
        $('#' + id).show();

        switch (id) {
            case 'div-fisica':
                data.tipoPersona = "fisica";
                $('#productor-persona-fisica input,#productor-persona-fisica select').prop('disabled', false);
                $('#uso-factura-persona-moral select').prop('disabled', false);
                $('#clabe-productor').prop('disabled', false);
                $('#sexo-productor,#fecha-nacimiento-productor,#rfc-productor,#nombre-productor,#papellido-productor,#sapellido-productor').prop('disabled', true);
                $('#agregar-sociedad-button').prop('disabled', false);
                break;
            case 'div-moral':
                data.tipoPersona = "moral";
                $('#productor-persona-moral input,#productor-persona-moral select, #socios-persona-moral input').prop('disabled', false);
                $('#sexo-representante,#fecha-nacimiento-representante').prop('disabled', true);
                $('#agregar-socio-button').prop('disabled', false);
                break;
        }

        data.sociedades = [];
        data.anexosActaConstitutiva = [];
        data.anexosListaSocios = [];
        utils.construyeSociedades();
        data.socios = [];
        utils.construyeSocios();
    };

    handlers.cambiaCurpProductor = function (curp) {
        utils.cambiaCurp(curp, '#fecha-nacimiento-productor', '#sexo-productor');
    }

    handlers.cambiaCurpRepresentante = function (e) {
        utils.cambiaCurp($(e.target).val(), '#fecha-nacimiento-representante', '#sexo-representante');
    }

    handlers.agregaSocio = function (e) {
        $('#socios-persona-moral input.valid-field').limpiaErrores();
        var errores = [];
        $('#socios-persona-moral input.valid-field').valida(errores, true);
        if (errores.length > 0) {
            return;
        }
        var curp = $('#curp-socio').val();
        var curpConfirmacion = $('#confirmacion-curp-socio').val();
        if (curp !== curpConfirmacion) {
            alert('Error: La CURP y la confirmación no coinciden.');
            return;
        }

        //Valida si la lista de socios se duplica el CURP
        var duplicado = utils.curpDuplicado(curp);
        if (duplicado) {
            alert('Error: El CURP ya ha sido agregado.');
            return;
        }

        data.socios.push({curp: curp});
        utils.construyeSocios();
        utils.limpiaSocio();
    };

    handlers.eliminaSocio = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-socio-'.length), 10);
        var socios = [];
        for (var i = 0; i < data.socios.length; i++) {
            if (i !== idx) {
                socios.push(data.socios[i]);
            }
        }
        data.socios = socios;
        utils.construyeSocios();
    }

    handlers.cambiaFirmaContrato = function (e) {
        var v = $(e.target).val();
        $('#agregar-contrato-button').prop('disabled', true);
        $('#numero-contrato').typeahead('val', '');
        $('#numero-contrato,#cantidad-contratada-contrato,#cantidad-contratada-cobertura').prop('disabled', true).limpiaErrores().val('');
        $('.numero-contrato-typeahead input').css('background-color', '');
        data.contratos = [];
        utils.construyeContratos();
        switch (v) {
            case 'true':
                $('#numero-contrato,#cantidad-contratada-contrato,#cantidad-contratada-cobertura,#agregar-contrato-button').prop('disabled', false);
                break;
        }
    }

    handlers.agregaContrato = function (e) {
        var numero = $('#numero-contrato').val();
        $('#numero-contrato').typeahead('val', numero.toUpperCase());

        $('#numero-contrato,#cantidad-contratada-contrato,#cantidad-contratada-cobertura').limpiaErrores();
        var errores = [];
        $('#numero-contrato,#cantidad-contratada-contrato,#cantidad-contratada-cobertura').valida(errores, true);
        if (errores.length > 0) {
            return;
        }

        if (parseFloat($("#cantidad-contratada-cobertura").val()) > parseFloat($("#cantidad-contratada-contrato").val())) {
            alert('Error: La cantidad contratada con cobertura no puede ser mayor que la cantidad contratada.');
            return;
        }

        var numero = $('#numero-contrato').val();
        var duplicado = utils.contratoDuplicado(numero);
        if (duplicado) {
            alert('Error: El contrato ya ha sido agregado.');
            return;
        }

        var contrato = utils.contratoExistente(numero);
        if (!contrato) {
            alert('Error: El contrato no está registrado.');
            return;
        }

        var contrato = {
            numeroContrato: contrato.numeroContrato,
            folio: contrato.folio,
            empresa: contrato.nombreEmpresa,
            cantidadContratada: $('#cantidad-contratada-contrato').val(),
            cantidadContratadaCobertura: $('#cantidad-contratada-cobertura').val()
        };
        data.contratos.push(contrato);
        utils.construyeContratos();
        utils.limpiaContrato();
    }

    handlers.eliminaContrato = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-contrato-'.length), 10);
        var contratos = [];
        for (var i = 0; i < data.contratos.length; i++) {
            if (i !== idx) {
                contratos.push(data.contratos[i]);
            }
        }
        data.contratos = contratos;
        utils.construyeContratos();
    }

    handlers.cambiaTipoPosesion = function (e) {
        var v = $(e.target).val();
        var tipo = v !== '0' ? $.segalmex.get(data.tiposPosesion, v) : {clave: '0'};
        var tipos = [];
        switch (tipo.clave) {
            case 'propia':
                tipos = data.tiposDocumentoPropia;
                break;
            case 'rentada':
                tipos = data.tiposDocumentoRentada;
                break;
        }
        $('#documento-posesion-predio').actualizaCombo(tipos).val('0');
    }

    handlers.cambiaEstadoPredio = function (e) {
        utils.cambiaEstado(e.target, 'municipio-predio');
    }

    utils.sort = (l1, l2) => {
            if (l1.nombre > l2.nombre) {
                return 1;
            }
            if (l1.nombre < l2.nombre) {
                return -1;
            }
            return 0;
        }

    handlers.cambiaCodigoPostal = function (e) {
        e.preventDefault();
        var cp = $(e.target).val();
        if (/\d{5}/.test(cp)) {
            $.ajax({
                url: '/trigo/resources/localidades/',
                type: 'GET',
                data: {codigoPostal: cp},
                dataType: 'json'
            }).done(function (response) {
                if (response.length > 0) {
                    const key = 'id';
                    let estados = [...new Map(response.map(l => l.municipio.estado).map(e => [e[key], e])).values()];
                    let municipios = [...new Map(response.map(l => l.municipio).map(e => [e[key], e])).values()];
                    response.sort(utils.sort);

                    $('#estado-direccion').actualizaCombo(estados).val(response[0].municipio.estado.id);
                    $('#municipio-direccion').actualizaCombo(municipios).val(response[0].municipio.id);
                    $('#estado-direccion,#municipio-direccion').change();

                    $('#localidad-direccion').actualizaCombo(response);
                    if (response.length === 1) {
                        $('#localidad-direccion').val(response[0].id);
                        $('#localidad-direccion').change();
                    }
                } else {
                    alert('Error: No existe el Código Postal especificado.');
                    utils.resetDireccionPorCp();
                }
            }).fail(function () {
                alert('Error: No existe el Código Postal especificado.');
                utils.resetDireccionPorCp();
            });
        } else {
            utils.resetDireccionPorCp();
        }
    }

    utils.resetDireccionPorCp = function() {
        $('#estado-direccion,#municipio-direccion,#localidad-direccion').actualizaCombo([]).val('0');
        $('#estado-direccion,#municipio-direccion,#localidad-direccion').change().prop('disabled', false);
    }

    handlers.cambiaRendimiento = function () {
        var volumen = parseFloat($('#volumen-predio').val());
        var superficie = parseFloat($('#superficie-predio').val());
        var rendimiento = volumen / superficie;
        var rendimientoFxd = rendimiento.toFixed(3);
        $('#rendimiento-predio').val(!isNaN(rendimientoFxd) && rendimientoFxd !== Infinity ? rendimientoFxd : '');
    }

    handlers.cambiaTipoGeorreferencia = function (e) {
        e.preventDefault();
        $('#predio-georreferencia input.valid-field').limpiaErrores().val('');
        $('#menu-tabs-predios a.nav-link').removeClass('active');
        var id = e.target.id.substring('muestra-'.length);
        $(e.target).addClass('active');
        $('#' + id).show();
        switch (id) {
            case 'punto-medio-predio':
                $('#punto-medio-predio').show();
                $('#poligono-predio').hide();
                break;
            case 'poligono-predio':
                $('#poligono-predio').show();
                $('#punto-medio-predio').hide();
                break;
        }
    }

    handlers.agregaPredio = function (e) {
        $('#predios-productor .valid-field').limpiaErrores();
        var errores = [];
        $('#predios-productor .valid-field').valida(errores, true);
        if (errores.length > 0) {
            return;
        }
        if ($('#documentacion-predio-pdf').val() === '') {
            alert('Documentación es requerida.');
            return;
        }

        if (data.solicitado && $('#documentacion-rendimiento-solicitado-pdf').val() === '') {
            alert('Documentación permiso de rendimiento es requerido.');
            data.rendimientoSolValido = false;
            return;
        }
        data.rendimientoSolValido = true;



        data.estadoActual = $.segalmex.get(data.estadosPredio, $("#estado-predio").val());
        var tipoCultivoActual = $.segalmex.get(data.tiposCultivo, $("#tipo-cultivo-predio").val());
        data.rendimientoMaximo = utils.getRendimiento(data.estadoActual, tipoCultivoActual);
        if (data.rendimientoMaximo === 0.0) {
            alert('Error: No se puede registrar el predio.\nEl rendimiento de la región es 0.');
            return;
        }
        if (parseFloat($('#rendimiento-predio').val()) > data.rendimientoMaximo && !data.solicitado) {
            $('#documentacion-rendimiento-solicitado-pdf').val('');
            $('#rendimientoSol').html($("#rendimiento-predio").val());
            $('#edoNombre').html(data.estadoActual.nombre);
            $('#rendimineto-solicitado-modal').modal({backdrop: 'static', keyboard: false});
            $('#rendimineto-solicitado-modal').modal('show');
            return;
        }

        var anexoPredio = $('#documentacion-predio-pdf').clone();
        anexoPredio.attr('id', (data.predios.length + 1) + '_documentacion-predio-pdf');

        if (data.solicitado) {
            var anexoSolicitado = $('#documentacion-rendimiento-solicitado-pdf').clone();
            anexoSolicitado.attr('id', (data.predios.length + 1) + '_documentacion-rendimiento-solicitado-pdf');
            anexoSolicitado.idx = data.predios.length;
            data.volumenMaximo = (data.rendimientoMaximo * parseFloat($('#superficie-predio').val())).toFixed(3);
        }
        var tc = $.segalmex.get(data.tiposCultivo, $('#tipo-cultivo-predio').val());

        var predio = {
            folio: $('#folio-predio').val(),
            tipoCultivo: tc,
            tipoPosesion: {id: $('#tipo-posesion-predio').val(), nombre: $("#tipo-posesion-predio option:selected").text()},
            tipoDocumentoPosesion: {id: $('#documento-posesion-predio').val(), nombre: $("#documento-posesion-predio option:selected").text()},
            regimenHidrico: {id: $('#regimen-hidrico-predio').val(), nombre: $("#regimen-hidrico-predio option:selected").text()},
            superficie: $("#superficie-predio").val(),
            rendimiento: data.solicitado ? data.rendimientoMaximo : $("#rendimiento-predio").val(),
            volumen: data.solicitado ? data.volumenMaximo : $("#volumen-predio").val(),
            volumenSolicitado: $("#volumen-predio").val(),
            rendimientoSolicitado: $("#rendimiento-predio").val(),
            solicitado: data.solicitado ? 'solicitado' : 'no-solicitado',
            estado: {id: $("#estado-predio").val(), nombre: $("#estado-predio option:selected").text()},
            municipio: {id: $('#municipio-predio').val(), nombre: $("#municipio-predio option:selected").text()},
            localidad: $('#localidad-predio').val(),
            latitud: $('#latitud-predio').val(),
            longitud: $('#longitud-predio').val(),
            latitud1: $('#latitud-1-predio').val(),
            longitud1: $('#longitud-1-predio').val(),
            latitud2: $('#latitud-2-predio').val(),
            longitud2: $('#longitud-2-predio').val(),
            latitud3: $('#latitud-3-predio').val(),
            longitud3: $('#longitud-3-predio').val(),
            latitud4: $('#latitud-4-predio').val(),
            longitud4: $('#longitud-4-predio').val()
        };
        data.predios.push(predio);
        data.anexosPredio.push(anexoPredio);
        if (data.solicitado) {
            data.anexosPrediosolicitado.push(anexoSolicitado);
        }
        utils.limpiaPredio();
        $('#div-table-predios').html(utils.construyePredios);
        $('#predios-table').on('click', 'button.eliminar-predio', handlers.eliminaPredio);
        $('#predios-table').on('click', 'a.muestra-ubicacion', handlers.muestraUbicacion);
        utils.verificaSemilla();
    };

    handlers.muestraUbicacion = function (e) {
        e.preventDefault();
        var ubicacion = $(e.target).html();
        $('#maps').muestraMaps({
            ubicacion: ubicacion
        });
    };

    handlers.eliminaPredio = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-predio-'.length), 10);
        var predios = [];
        var anexosPredio = [];
        var anexosSolicitados = [];
        for (var i = 0; i < data.predios.length; i++) {
            if (i !== idx) {
                predios.push(data.predios[i]);
                var anexoPredio = data.anexosPredio[i];
                anexoPredio.attr('id', (anexosPredio.length + 1) + '_documentacion-predio-pdf');
                if (data.predios[i].solicitado === 'solicitado') {
                    var anexoSolicitado = utils.getAnexoPresioSolicitado(i);
                    anexoSolicitado.attr('id', (anexosPredio.length + 1) + '_documentacion-rendimiento-solicitado-pdf');
                    anexoSolicitado.idx = anexosPredio.length;
                    anexosSolicitados.push(anexoSolicitado);
                }
                anexosPredio.push(anexoPredio);
            }
        }
        data.predios = predios;
        data.anexosPredio = anexosPredio;
        data.anexosPrediosolicitado = anexosSolicitados;
        $('#div-table-predios').html(utils.construyePredios);
        $('#predios-table').on('click', 'button.eliminar-predio', handlers.eliminaPredio);
        $('#predios-table').on('click', 'a.muestra-ubicacion', handlers.muestraUbicacion);
        utils.verificaSemilla();
    };

    utils.getAnexoPresioSolicitado = function (idx) {
        for (var i = 0; i < data.anexosPrediosolicitado.length; i++) {
            var anexo = data.anexosPrediosolicitado[i];
            if (anexo.idx === idx) {
                return anexo;
            }
        }
    };

    handlers.cambiaUsoFactura = function (e) {
        var v = $(e.target).val();
        $('#factura-nombre-sociedad,#factura-rfc-sociedad,#confirmacion-factura-rfc-sociedad,#agregar-sociedad-button').prop('disabled', true);
        $('#uso-factura-persona-moral input.form-control-file').prop('disabled', true);
        $('#factura-nombre-sociedad,#factura-rfc-sociedad,#confirmacion-factura-rfc-sociedad').val('');
        switch (v) {
            case 'true':
                $('#factura-nombre-sociedad,#factura-rfc-sociedad,#confirmacion-factura-rfc-sociedad,#agregar-sociedad-button').prop('disabled', false);
                $('#uso-factura-persona-moral input.form-control-file').prop('disabled', false);
                break;
            case 'false':
                data.anexosActaConstitutiva = [];
                data.anexosListaSocios = [];
                break;
        }
        data.sociedades = [];
        utils.construyeSociedades();
    }

    handlers.agregaSociedad = function (e) {
        $('#uso-factura-persona-moral .valid-field').limpiaErrores();
        var errores = [];
        $('#uso-factura-persona-moral .valid-field').valida(errores, true);
        if (errores.length > 0) {
            return;
        }
        var rfc = $('#factura-rfc-sociedad').val();
        var confirmacion = $('#confirmacion-factura-rfc-sociedad').val();
        if (rfc !== confirmacion) {
            alert('Error: El RFC y la confirmación no coinciden.');
            return;
        }

        if (utils.sociedadDuplicada(rfc)) {
            alert('Error: El RFC ya ha sido agregado.');
            return;
        }

        if ($('#acta-constitutiva-sociedad-pdf').val() === '') {
            alert('Acta constitutiva de la sociedad (PDF) es requerida.');
            return;
        }
        if ($('#lista-socios-pdf').val() === '') {
            alert('Lista de socios (PDF) es requerida.');
            return;
        }

        var anexoActaConstitutiva = $('#acta-constitutiva-sociedad-pdf').clone();
        anexoActaConstitutiva.attr('id', data.sociedades.length + '_acta-constitutiva-sociedad-pdf');

        var anexoListaSocios = $('#lista-socios-pdf').clone();
        anexoListaSocios.attr('id', data.sociedades.length + '_lista-socios-pdf');

        var sociedad = {
            rfc: rfc,
            nombre: $('#factura-nombre-sociedad').val()
        };
        data.sociedades.push(sociedad);
        data.anexosActaConstitutiva.push(anexoActaConstitutiva);
        data.anexosListaSocios.push(anexoListaSocios);
        utils.limpiaSociedad();
        utils.construyeSociedades();
    }

    handlers.eliminaSociedad = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-sociedad-'.length), 10);
        var sociedades = [];
        var anexosActaConstitutiva = [];
        var anexosListaSocios = [];
        for (var i = 0; i < data.sociedades.length; i++) {
            if (i !== idx) {
                sociedades.push(data.sociedades[i]);
                var anexoActa = data.anexosActaConstitutiva[i];
                anexoActa.attr('id', anexosActaConstitutiva.length + '_acta-constitutiva-sociedad-pdf');
                anexosActaConstitutiva.push(anexoActa);
                var anexoLista = data.anexosListaSocios[i];
                anexoLista.attr('id', anexosListaSocios.length + '_lista-socios-pdf');
                anexosListaSocios.push(anexoLista);
            }
        }
        data.sociedades = sociedades;
        data.anexosActaConstitutiva = anexosActaConstitutiva;
        data.anexosListaSocios = anexosListaSocios;
        utils.construyeSociedades();
    };

    handlers.guarda = function (e) {
        e.preventDefault();
        $('#socios-persona-moral .valid-field').prop('disabled', true);
        var campos = $('#datos-productor .valid-field,#datos-contacto .valid-field, #domicilio-productor .valid-field,'
                + '#firmo-contrato,#datos-bancarios-productor .valid-field,#uso-factura-productor,#datos-beneficiario .valid-field,'
                + '#folio-semilla-snics,#toneladas-snics,#nombre-snics,#rfc-snics,#cantidad-predios,'
                + '#encargado-registro .valid-field');
        campos.limpiaErrores();
        var errores = [];
        campos.valida(errores, true);
        $('#socios-persona-moral .valid-field').prop('disabled', false);
        if (errores.length > 0) {
            return;
        }

        if (data.predios.length === 0) {
            alert('Error: Es necesario agregar al menos un predio.');
            return;
        }

        var predios = parseInt($('#cantidad-predios').val(), 10);
        if (predios !== data.predios.length) {
            alert('Error: La cantidad de predios es diferente a los predios agregados.');
            return;
        }

        if (data.tipoPersona === 'moral' && data.socios.length === 0) {
            alert('Error: Es necesario agregar todos los socios que forman parte de la sociedad.');
            return;
        }
        switch (data.tipoPersona) {
            case 'moral':
                if ($('#curp-representante').val() !== $('#confirmacion-curp-representante').val()) {
                    alert('Error: La confirmación del CURP del representante no coincide.');
                    return;
                }
                break;
            case 'fisica':
                if ($('#curp-productor').val() === '') {
                    alert('Error: * CURP este campo es obligatorio.');
                    return;
                }
                break;
        }

        if ($('#clabe-productor').val() === '') {
            alert('Error: * CLABE este campo es obligatorio.');
            return;
        }

        //Verificamos si la seccion de contratos es visible
//        if ($('#contratos-productor-div').is(':visible')) {
//            var contratos = $('#firmo-contrato').val();
//            if (contratos === 'true' && data.contratos.length === 0) {
//                alert('Error: Es necesario agregar al menos un contrato.');
//                return;
//            }
//            if (contratos === 'false' && data.contratoRequerido) {
//                alert('Error: Es necesario agregar al menos un contrato.');
//                return;
//            }
//        }

        if (data.contratos.length === 0) {
            alert("Es necesario que el productor participe al menos en un contrato.");
            return;
        }

        var usoFactura = $('#uso-factura-productor').val();
        if (usoFactura === 'true' && data.sociedades.length === 0) {
            alert('Error: Es necesario agregar al menos una sociedad de la cual hace uso de facturas.');
            return;
        }
        if ($('#datos-semilla').is(':visible') && data.snics.foliosSnics === '') {
            alert('Error: Es necesario agregar al menos un folio de SNICS');
            return;
        }

        if (utils.archivosSeleccionados()) {
            return;
        }

        if (data.tipoPersona === 'fisica') {
            if ($('#curp-productor').val() === $('#curp-beneficiario').val()) {
                alert('Error: El CURP del productor no puede ser igual al del beneficiario.');
                return;
            }
        } else {
            if ($('#curp-representante').val() === $('#curp-beneficiario').val()) {
                alert('Error: El CURP del representante no puede ser igual al del beneficiario.');
                return;
            }
        }


        $('#condiciones-modal').modal('show');
    };

    utils.archivosSeleccionados = function () {
        $('#documentacion-predio-pdf,#documentacion-rendimiento-solicitado-pdf').prop('disabled', true);
        $('#uso-factura-persona-moral input.form-control-file').prop('disabled', true);
        var files = $('input.form-control-file:enabled');
        $('#documentacion-predio-pdf,#documentacion-rendimiento-solicitado-pdf').prop('disabled', false);
        $('#uso-factura-persona-moral input.form-control-file').prop('disabled', false);

        var texto = [];
        var errores = false;
        for (var i = 0; i < files.length; i++) {
            if ($(files[i]).val() === '') {
                errores = true;
                texto.push(' * Seleccione el archivo para: ' + $('label[for=' + files[i].id + ']').html() + '\n');
            }
        }
        if (errores) {
            alert('Verifique lo siguiente:\n\n' + texto.join(''));
        }

        return errores;
    }

    handlers.limpia = function (e) {
        if (confirm('¿Está seguro de limpiar todos los datos capturados?')) {
            utils.limpiar();
            $('#curp-productor').focus();
        }
    }

    handlers.aceptoCondiciones = function (e) {
        e.preventDefault();
        $('#si-acepto-condiciones,#no-acepto-condiciones').prop('disabled', true);
        utils.guardaInscripcion();
    }

    handlers.noAceptoCondiciones = function (e) {
        $('#condiciones-modal').modal('hide');
        $('#guardar-button').html('Guardar').prop('disabled', false);
    }

    handlers.cierraDescarga = function (e) {
        $('#guardar-button').html('Guardar').prop('disabled', false);
        $('.estatus-pdf').html('');
        utils.limpiar();
        $('#curp-productor').focus();
    };

    handlers.verificaTamanoArchivo = function (e) {
        $.segalmex.archivos.verificaArchivo(e, 'pdf', data.limiteArchivo);
    };

    utils.guardaInscripcion = function () {
        $('#si-acepto-condiciones,#no-acepto-condiciones').prop('disabled', true);
        var inscripcion = utils.generaDatos();
        if (data.uuid) {
            inscripcion.uuid = data.uuid;
            return;
        }
        $.ajax({
            url: '/trigo/resources/productores/maiz/inscripcion/',
            type: 'POST',
            data: JSON.stringify(inscripcion),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            // Guardamos el registro
            data.uuid = response.uuid;
            $('#condiciones-modal').modal('hide');
            $('#datos-semilla').hide();
            $('#carga-archivos').cargaArchivos({
                archivos: utils.getArchivos(),
                urlComprobante: '/trigo/resources/productores/maiz/inscripcion/' + data.uuid + '/inscripcion-productor.pdf',
                callBackCerrar: handlers.cierraDescarga,
                indicaciones: `Por favor espere mientras se suben los documentos anexos al
                    registro con folio <strong>${response.folio}</strong>.<br/> Al finalizar
                    podrá descargar el registro, el cual deberá imprimir, firmar y subir
                    al sistema en la pantalla de <strong>Capturas</strong>.`
            });
            $('#si-acepto-condiciones,#no-acepto-condiciones').prop('disabled', false);
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Ocurrió un error al guardar el registro. Vuelva a ingresar en una nueva ventana en caso de que haya finalizado su sesión e intente nuevamente.');
            }
            $('#si-acepto-condiciones,#no-acepto-condiciones').prop('disabled', false);
        });
    };

    utils.cambiaCurp = function (curp, idFecha, idGenero) {
        curp = curp.toUpperCase();
        var fn = '';
        var sx = '0';
        if (/^[A-Z]{4}\d{6}[M|H]{1}[A-Z]{5}[A-Z0-9]{1}\d{1}$/.test(curp)) {
            var anio = $.isNumeric(curp.substring(16, 17)) ? "19" : "20";
            fn = curp.substring(8, 10) + curp.substring(6, 8) + anio + curp.substring(4, 6);
            var sexo = curp.substring(10, 11).toLowerCase();
            switch (sexo) {
                case 'H':
                case 'h':
                    sx = 'masculino';
                    break;
                case 'M':
                case 'm':
                    sx = 'femenino';
                    break;
            }
        }
        $(idFecha).val(fn);
        $(idFecha).change();
        $(idGenero).val(sx);
    };

    utils.inicializaValidaciones = function () {
        $(".valid-field").configura();
        $('#nombre-sociedad,#factura-nombre-sociedad').configura({
            type: 'name-moral'
        });
        $('#curp-representante,#confirmacion-curp-representante,#curp-productor,'
                + '#curp-socio,#confirmacion-curp-socio,'
                + '#curp-beneficiario').configura({
            type: 'curp'
        });
        $('#rfc-sucursal,#rfc-sociedad,#factura-rfc-sociedad,#confirmacion-factura-rfc-sociedad').configura({
            type: 'rfc-moral'
        });
        $('#rfc-productor').configura({
            type: 'rfc-fisica'
        });
        $('#nombre-representante,#papellido-representante,#sapellido-representante,'
                + '#nombre-productor,#papellido-productor,#sapellido-productor,'
                + '#nombre-beneficiario,#apellidos-beneficiario').configura({
            type: 'nombre-icao'
        }).configura({textTransform: 'upper'});
        ;
        $('#sapellido-representante,#sapellido-productor').configura({
            required: false
        });
        ;
        $('#fecha-nacimiento-representante,#fecha-nacimiento-productor').configura({
            type: 'date',
            max: $.segalmex.fecha,
            before: true
        });
        $('#fecha-nacimiento-representante,#fecha-nacimiento-productor').datepicker({
            language: "es",
            autoclose: true,
            forceParse: false,
            format: 'dd/mm/yyyy',
            yearRange: "c-10:c"
        });
        $('#numero-telefono').configura({
            pattern: /^\d{10}$/,
            minlength: 10,
            maxlength: 10
        });
        $('#correo-electronico').configura({
            type: 'email'
        });
        $('#calle-direccion,#numero-exterior-direccion,#numero-interior-direccion,#folio-predio,#localidad-predio,#nombre-snics').configura({
            type: 'name-moral'
        });
        $('#numero-interior-direccion').configura({
            required: false
        });
        $('#cp-direccion').configura({
            pattern: /^\d{5}$/,
            minlength: 5,
            maxlength: 5
        });
        $('#numero-contrato,#confirmacion-numero-contrato').configura({
            pattern: /^[A-Z]{3}-[A-Z]{2}[0-9]{2}-[A-Z0-9]{3}-\d{6}-(P|C|S|D|E)-\d{3}$/,
            minlength: 25,
            maxlength: 25
        }).configura({textTransform: 'upper'});
        $('#cantidad-contratada-contrato,#cantidad-contratada-cobertura,#rendimiento-predio,#cantidad-predios').configura({
            type: 'number',
            min: 0
        });
        $('#volumen-predio').configura({
            type: 'number',
            min: 0.5
        });
        $('#superficie-predio').configura({
            type: 'number',
            min: 0.1
        });
        $('#predio-georreferencia input.valid-field').configura({
            type: 'point'
        });
        $('#latitud-predio,#latitud-1-predio,#latitud-2-predio,#latitud-3-predio,#latitud-4-predio').configura({
            min: 14.5,
            max: 32.72
        });
        $('#longitud-predio,#longitud-1-predio,#longitud-2-predio,#longitud-3-predio,#longitud-4-predio').configura({
            min: -117.2,
            max: -86.7
        });
        $('#numero-cuenta-productor').configura({
            pattern: /^\d{7,14}$/,
            minlength: 7,
            maxlength: 14
        });
        $('#clabe-productor').configura({
            pattern: /^\d{18}$/,
            minlength: 18,
            maxlength: 18
        });
        $('#rfc-snics').configura({
            type: 'rfc',
            required: true
        });
        $('#toneladas-snics').configura({
            type: 'number',
            required: true
        });
        $('#folio-semilla-snics').configura({
            pattern: /^[0-9]{5}-[A-Z]{3}-[A-Z]{3}-[0-9]{6}$/,
            required: true
        });
        $('#cantidad-folios-snics').configura({
            type: 'number',
            maxlength: 10,
            required: true
        });
        $('.valid-field').validacion();
    };

    utils.cargaCatalogos = function () {
        $.ajax({
            type: 'GET',
            url: '/trigo/resources/paginas/inscripcion-productor/',
            dataType: 'json'
        }).done(function (response) {
            if (response.sucursal === undefined) {
                alert('Error: No cuentas con una sucursal para registrar.');
                return;
            }

            data.rendimientos = response.rendimientos;
            data.tiposCultivo = utils.filtraCultivos(response.tipos, response.cicloSeleccionado);
            data.tiposPersona = response.tiposPersona;
            data.estadosPredio = response.estadosPredio;
            data.tiposPosesion = response.tiposPosesion;
            data.tiposDocumentoPropia = response.tiposDocumentoPropia;
            data.tiposDocumentoRentada = response.tiposDocumentoRentada;
            data.municipios = response.municipios;
            data.sexos = response.sexos;
            data.bancos = response.bancos;
            data.volumenPredio = parseFloat(response.volumenPredio.valor);
            data.superficiePredio = parseFloat(response.superficiePredio.valor);
            data.contratoRequerido = response.contratoRequerido;
            data.cicloActual = response.cicloSeleccionado;
            $('#cultivo').actualizaCombo(response.cultivos).prop('disabled', true);
            $('#cultivo').val($.segalmex.get(response.cultivos, 'trigo', 'clave').id);
            $('#ciclo-agricola-global').cicloAgricola({
                ciclos: response.ciclos,
                actual: response.cicloSeleccionado,
                reload: true,
                sistema: 'trigo'
            });
            $('#tipo-cultivo-predio').actualizaCombo(data.tiposCultivo);
            $('#entidad-sucursal').actualizaCombo(response.estados);
            $('#estado-predio').actualizaCombo(response.estadosPredio);
            $('#banco-productor').actualizaCombo(response.bancos);
            $('#tipo-posesion-predio').actualizaCombo(response.tiposPosesion);
            $('#regimen-hidrico-predio').actualizaCombo(response.regimenes);
            $('#tipo-documento-representante,#tipo-documento-productor').actualizaCombo(response.tiposDocumentoIdentificacion);
            $('#sexo-productor,#sexo-representante').actualizaCombo(response.sexos, {value: 'clave'});
            $('#parentesco-beneficiario').actualizaCombo(response.parentescos);
            // datos de la sucursal
            $('#nombre-sucursal').val(response.sucursal.empresa.nombre);
            $('#rfc-sucursal').val(response.sucursal.empresa.rfc);
            $('#entidad-sucursal').val(response.sucursal.estado.id).prop('disabled', true);
            $('#municipio-sucursal').val(response.sucursal.municipio.nombre);
            $('#localidad-sucursal').val(response.sucursal.localidad);
            $('#grupos-indigenas').actualizaCombo(response.gruposIndigenas);
            $('#nivel-estudio').actualizaCombo(response.nivelEstudios);
            $('#productor-persona-moral select,#productor-persona-fisica select').prop('disabled', true);
            utils.aplicaContrato(response.aplicaContrato);
            utils.aplicaPersonaMoral(response.aplicaPersonaMoral);
            utils.inicializaValidaciones();
            utils.resetProductor();
            utils.resetPredios();
            $('.bancarios').prop('disabled', true);
            $('#curp-productor,#clabe-productor').prop('disabled', false);
            $('#grupos-indigenas').prop('disabled', true);
            if (response.cicloSeleccionado) {
                $('#div-terminos').html($.segalmex.trigo.terminos.productor.getTerminos(response.cicloSeleccionado.clave));
                $('.nombre-ciclo').html(response.cicloSeleccionado.nombre);
            }
            if (response.registroCerrado) {
                alert("El registro de productores cerró el " + response.registroCerrado.valor);
            }

        }).fail(function () {
            alert('Error: Al descargar los recursos de la página.');
        });
    };

    utils.aplicaContrato = function (aplicaContrato) {
        if (aplicaContrato) {
            utils.cargaContratos();
            $('#contratos-productor-div').show();
        }
    };

    utils.aplicaPersonaMoral = function (aplicaPersonaMoral) {
        if (aplicaPersonaMoral) {
            $('#muestra-div-moral').show();
        }
    };

    utils.cargaContratos = function () {
        $.ajax({
            type: 'GET',
            url: '/trigo/resources/contratos/maiz/simplificados/',
            dataType: 'json'
        }).done(function (response) {
            data.contratosRegistrados = response;
            $('#numero-contrato').typeahead({
                hint: true,
                highlight: true,
                minLength: 3
            }, {
                name: 'contratos',
                source: utils.contratosMatcher(data.contratosRegistrados)
            });
        })
    }

    utils.configuraPantalla = function () {
        $('#productor-persona-moral input,#productor-persona-fisica input').prop('disabled', true);
        $('#curp-representante').change(handlers.cambiaCurpRepresentante);
        $('#menu-tabs a.nav-link').click(handlers.cambiaTipoProductor);
        //$('#firmo-contrato').change(handlers.cambiaFirmaContrato);
        $('#tipo-posesion-predio').change(handlers.cambiaTipoPosesion);
        $('#estado-predio').change(handlers.cambiaEstadoPredio);
        $('#cp-direccion').change(handlers.cambiaCodigoPostal);
        $('#agregar-socio-button').click(handlers.agregaSocio);
        $('#socios-table').on('click', 'button.eliminar-socio', handlers.eliminaSocio);
        $('#volumen-predio,#superficie-predio').change(handlers.cambiaRendimiento);
        $('#agregar-predio-button').click(handlers.agregaPredio);
        $('#predios-table').on('click', 'button.eliminar-predio', handlers.eliminaPredio);
        $('#agregar-contrato-button').click(handlers.agregaContrato);
        $('#contratos-table').on('click', 'button.eliminar-contrato', handlers.eliminaContrato);
        $('#uso-factura-productor').change(handlers.cambiaUsoFactura);
        $('#agregar-sociedad-button').click(handlers.agregaSociedad);
        $('#sociedades-table').on('click', 'button.eliminar-sociedad', handlers.eliminaSociedad);
        $('#guardar-button').click(handlers.guarda);
        $('#limpiar-button').click(handlers.limpia);
        $('#no-acepto-condiciones').click(handlers.noAceptoCondiciones);
        $('#si-acepto-condiciones').click(handlers.aceptoCondiciones);
        $('#menu-tabs-predios a.nav-link').click(handlers.cambiaTipoGeorreferencia);
        $('#curp-representante,#confirmacion-curp-representante,#curp-socio,#confirmacion-curp-socio,#numero-contrato,#numero-cuenta-productor,'
                + '#factura-rfc-sociedad,#confirmacion-factura-rfc-sociedad').bind('cut copy paste', function (e) {
            e.preventDefault();
        });
        $('input.form-control-file').change(handlers.verificaTamanoArchivo);
        $('#agregar-folio-snics').click(handlers.agregaFolioSnics);
        $('#eliminar-folio-snics').click(handlers.eliminaFoliosSnics);
        $('#div-table-predios').html(utils.construyePredios);
        $('#predios-table').on('click', 'button.eliminar-predio', handlers.eliminaPredio);
        $('#clabe-productor').change(utils.cambiaClabe);
        $('#curp-productor').change(utils.confirmaCurp);
        $('#aceptar-rendimiento-solicitado').click(utils.confirmaRendimientoSolicitado);
        $('#cancelar-rendimiento-solicitado').click(utils.cancelarRendimientoSolicitado);
        $('#pertenece-grupo-indigena').change(utils.cambiaGrupoIndigena);
    };

    utils.confirmaRendimientoSolicitado = function () {
        data.solicitado = true;
        handlers.agregaPredio();
        if (data.rendimientoSolValido) {
            $('#rendimineto-solicitado-modal').modal('hide');
        }
    };

    utils.cancelarRendimientoSolicitado = function () {
        data.solicitado = false;
        data.rendimientoSolValido = false;
        $('#rendimineto-solicitado-modal').modal('hide');
    };

    handlers.eliminaFoliosSnics = function () {
        $('#label-folios-snics').html('');
        data.snics.foliosSnics = '';
    };

    handlers.agregaFolioSnics = function () {
        $('#cantidad-folios-snics').limpiaErrores();
        var errores = [];
        $('#cantidad-folios-snics').valida(errores, true);
        if (errores.length > 0) {
            return;
        }
        if (data.snics.foliosSnics !== '') {
            var folios = data.snics.foliosSnics.split(',');
            var folio = $('#cantidad-folios-snics').val();
            for (var i = 0; i < folios.length; i++) {
                if (folios[i] === folio) {
                    alert('Error: Folio ya registrado.');
                    return;
                }
            }
        }
        data.snics.foliosSnics = data.snics.foliosSnics === ''
                ? $('#cantidad-folios-snics').val() : data.snics.foliosSnics + ', ' + $('#cantidad-folios-snics').val();
        $('#cantidad-folios-snics').val('');
        $('#label-folios-snics').html('Folios Agregados: ' + data.snics.foliosSnics);
    };

    utils.contratosMatcher = function (contratos) {
        return function (q, cb) {
            var matches = [];
            // regex used to determine if a string contains the substring `q`
            var substrRegex = new RegExp(q, 'i');
            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(contratos, function (i, contrato) {
                if (substrRegex.test(contrato.numeroContrato)) {
                    matches.push(contrato.numeroContrato);
                }
            });
            cb(matches);
        };
    }

    utils.cambiaEstado = function (target, idMunicipio) {
        var v = $(target).val();
        var estado = v !== '0' ? $.segalmex.get(data.municipios, v) : {clave: '0'};
        var municipios = [];
        if (estado.clave !== '0') {
            for (var i = 0; i < data.municipios.length; i++) {
                var m = data.municipios[i];
                if (m.estado.id === estado.id) {
                    municipios.push(m);
                }
            }
        }
        $('#' + idMunicipio).actualizaCombo(municipios).val('0');
    }

    utils.generaDatos = function () {
        var inscripcion = {
            cultivo: {id: $("#cultivo").val()},
            ciclo: {id: $("#ciclo-agricola").val()},
            datosProductor: utils.generaDatosProductor(),
            numeroTelefono: $('#numero-telefono').val(),
            correoElectronico: $('#correo-electronico').val(),
            domicilio: {
                calle: $('#calle-direccion').val(),
                numeroExterior: $('#numero-exterior-direccion').val(),
                numeroInterior: $('#numero-interior-direccion').val(),
                codigoPostal: $('#cp-direccion').val(),
                estado: {id: $('#estado-direccion').val()},
                municipio: {id: $('#municipio-direccion').val()},
                catalogoLocalidad: {id: $('#localidad-direccion').val()}
            },
            contratos: data.contratos,
            predios: data.predios,
            cuentaBancaria: {
                numero: $('#numero-cuenta-productor').val(),
                clabe: $('#clabe-productor').val(),
                banco: {id: $('#banco-productor').val()}
            },
            empresas: data.sociedades,
            nombreBeneficiario: $('#nombre-beneficiario').val(),
            apellidosBeneficiario: $('#apellidos-beneficiario').val(),
            curpBeneficiario: $('#curp-beneficiario').val(),
            parentesco: {id: $('#parentesco-beneficiario').val()},
            encargado: $('#nombre-encargado-registro').val(),
            claveArchivos: utils.getClavesArchivos()
        };
        if (data.snics.foliosSnics !== '') {
            inscripcion.informacionSemilla = {
                snics: $('#folio-semilla-snics').val(),
                toneladas: $('#toneladas-snics').val(),
                cantidadFolios: data.snics.foliosSnics,
                nombre: $('#nombre-snics').val(),
                rfc: $('#rfc-snics').val()
            };
        }
        if ($('#pertenece-grupo-indigena').val() === 'true') {
            inscripcion.grupoIndigena = {id: $('#grupos-indigenas').val()};
        }
        inscripcion.nivelEstudio = {id: $('#nivel-estudio').val()};
        return inscripcion;
    };

    utils.generaDatosProductor = function () {
        var tipo = $.segalmex.get(data.tiposPersona, data.tipoPersona, 'clave');
        switch (tipo.clave) {
            case 'fisica':
                return {
                    tipoPersona: tipo,
                    curp: $('#curp-productor').val(),
                    rfc: $('#rfc-productor').val(),
                    nombre: $('#nombre-productor').val(),
                    primerApellido: $('#papellido-productor').val(),
                    segundoApellido: $('#sapellido-productor').val(),
                    fechaNacimiento: $.segalmex.date.fechaToIso($('#fecha-nacimiento-productor').val()),
                    sexo: {clave: $('#sexo-productor').val()},
                    tipoDocumento: {id: $('#tipo-documento-productor').val()},
                    numeroDocumento: $('#numero-documento-productor').val()
                }
                ;
            case 'moral':
                return {
                    tipoPersona: tipo,
                    rfc: $('#rfc-sociedad').val(),
                    curp: $('#curp-representante').val(),
                    nombreMoral: $('#nombre-sociedad').val(),
                    nombre: $('#nombre-representante').val(),
                    primerApellido: $('#papellido-representante').val(),
                    segundoApellido: $('#sapellido-representante').val(),
                    fechaNacimiento: $.segalmex.date.fechaToIso($('#fecha-nacimiento-representante').val()),
                    sexo: {clave: $('#sexo-representante').val()},
                    tipoDocumento: {id: $('#tipo-documento-representante').val()},
                    numeroDocumento: $('#numero-documento-representante').val(),
                    socios: data.socios
                };
            default:
                return null;
        }
    }

    utils.contratoDuplicado = function (numero) {
        for (var i = 0; i < data.contratos.length; i++) {
            if (data.contratos[i].numeroContrato === numero) {
                return true;
            }
        }
        return false;
    }

    utils.contratoExistente = function (numero) {
        for (var i = 0; i < data.contratosRegistrados.length; i++) {
            if (data.contratosRegistrados[i].numeroContrato === numero) {
                return data.contratosRegistrados[i];
            }
        }
        return null;
    }

    utils.limpiaSocio = function () {
        $('#socios-persona-moral input.valid-field').limpiaErrores().val('');
    };

    utils.construyeSocios = function () {
        var buffer = [];
        for (var i = 0; i < data.socios.length; i++) {
            var socio = data.socios[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-right">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(socio.curp);
            buffer.push('</td>');
            buffer.push('<td class="text-center"><button id="eliminar-socio-');
            buffer.push(i);
            buffer.push('" class="btn btn-danger btn-sm eliminar-socio"><i class="fa fa-minus-circle"></i></button></td>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        $('#socios-table tbody').html(buffer.join(''));
    };

    utils.limpiaContrato = function () {
        $('#numero-contrato').typeahead('val', '');
        $('#numero-contrato,#cantidad-contratada-contrato,#cantidad-contratada-cobertura').limpiaErrores();
        $('#cantidad-contratada-contrato,#cantidad-contratada-cobertura').val('');
    }

    utils.construyeContratos = function () {
        var buffer = [];
        if (data.contratos.length === 0) {
            return $('#contratos-table tbody').html('<tr><th colspan="6" class="text-center">Sin información</th><tr>');
        }
        for (var i = 0; i < data.contratos.length; i++) {
            var contrato = data.contratos[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-right">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(contrato.numeroContrato);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(contrato.folio);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(contrato.empresa);
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(contrato.cantidadContratada);
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(contrato.cantidadContratadaCobertura);
//            buffer.push('</td>');
//            buffer.push('<td class="text-center"><button id="eliminar-contrato-');
//            buffer.push(i);
//            buffer.push('" class="btn btn-danger btn-sm eliminar-contrato"><i class="fa fa-minus-circle"></i></button>');
//            buffer.push('</td>');
            buffer.push('</tr>');
        }
        $('#contratos-table tbody').html(buffer.join(''));
    };

    utils.limpiaPredio = function () {
        var disabled = $('#tipo-cultivo-predio').prop('disabled');
        var valor = $('#tipo-cultivo-predio').val();
        $('#menu-tabs-predios a.nav-link').removeClass('active');
        utils.resetPredios();
        $('#predios-productor input.valid-field').limpiaErrores().val('');
        $('#predios-productor select.valid-field').limpiaErrores().val('0');
        $('#documentacion-predio-pdf').val('');
        $('#tipo-cultivo-predio').prop('disabled', disabled);
        if (disabled) {
            $('#tipo-cultivo-predio').val(valor);
        }
        data.solicitado = false;
        data.volumenMaximo = 0;
        data.rendimientoMaximo = 0;
        data.estadoActual = null;
    };

    utils.construyePredios = function () {
        var buffer = [];
        buffer.push('<div class="table-responsive">');
        buffer.push('<table id="predios-table" class="table table-bordered table-hover table-striped">');
        buffer.push('<thead class="thead-dark"><tr>');
        buffer.push('<th>#</th>');
        buffer.push('<th>Folio</th>');
        buffer.push('<th>Tipo cultivo</th>');
        buffer.push('<th>Posesión</th>');
        buffer.push('<th>R. hídrico</th>');
        buffer.push('<th>Municipio, Estado</th>');
        buffer.push('<th>Localidad</th>');
        buffer.push('<th>Ubicación</th>');
        buffer.push('<th>V&nbsp;(t)</th>');
        buffer.push('<th>S&nbsp;(ha)</th>');
        buffer.push('<th>R&nbsp;(t/ha)</th>');
        buffer.push('<th><i class="fa fa-minus-circle"></i></th>');
        buffer.push('</tr></thead>');
        buffer.push('<tbody>');
        if (data.predios.length > 0) {
            for (var i = 0; i < data.predios.length; i++) {
                var predio = data.predios[i];
                buffer.push('<tr>');
                buffer.push('<td class="text-right">');
                buffer.push(i + 1);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(predio.folio);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(predio.tipoCultivo.nombre);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(predio.tipoPosesion.nombre);
                buffer.push(', ');
                buffer.push(predio.tipoDocumentoPosesion.nombre);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(predio.regimenHidrico.nombre);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(predio.municipio.nombre);
                buffer.push(', ');
                buffer.push(predio.estado.nombre);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(predio.localidad);
                buffer.push('</td>');
                buffer.push('<td>');
                if (predio.latitud !== '') {
                    buffer.push('<a class="muestra-ubicacion" href="#">[' + predio.latitud + ',' + predio.longitud + ']</a>');
                } else {
                    buffer.push('<a class="muestra-ubicacion" href="#">[' + predio.latitud1 + ',' + predio.longitud1 + ']</a>,');
                    buffer.push('<a class="muestra-ubicacion" href="#">[' + predio.latitud2 + ',' + predio.longitud2 + ']</a>,');
                    buffer.push('<a class="muestra-ubicacion" href="#">[' + predio.latitud3 + ',' + predio.longitud3 + ']</a>,');
                    buffer.push('<a class="muestra-ubicacion" href="#">[' + predio.latitud4 + ',' + predio.longitud4 + ']</a>');
                }
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(predio.solicitado === 'solicitado' ? (predio.volumen + utils.setRojo(predio.volumenSolicitado)) : predio.volumen);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(predio.superficie);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(predio.solicitado === 'solicitado' ? (predio.rendimiento + utils.setRojo(predio.rendimientoSolicitado)) : predio.rendimiento);
                buffer.push('</td>');
                buffer.push('<td class="text-center"><button id="eliminar-predio-');
                buffer.push(i);
                buffer.push('" class="btn btn-danger btn-sm eliminar-predio"><i class="fa fa-minus-circle"></i></button></td>');
                buffer.push('</td>');
                buffer.push('</tr>');
            }
        }
        buffer.push('</tbody>');
        buffer.push(data.predios.length > 0 ? utils.agregaTotales(data.predios) : '');
        buffer.push('</table>');
        buffer.push('</div>');
        buffer.push('<br/>');

        return buffer.join('');
    };

    utils.setRojo = function (val) {
        return '\n<p style="color:Red;"> (' + val + ')</p>';
    };

    utils.sociedadDuplicada = function (rfc) {
        for (var i = 0; i < data.sociedades.length; i++) {
            if (data.sociedades[i].rfc === rfc) {
                return true;
            }
        }
        return false;
    }

    utils.limpiaSociedad = function () {
        $('#uso-factura-persona-moral input.valid-field').limpiaErrores().val('');
        $('#uso-factura-persona-moral input.form-control-file').val('');
    }

    utils.construyeSociedades = function () {
        var buffer = [];
        for (var i = 0; i < data.sociedades.length; i++) {
            var sociedad = data.sociedades[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-right">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(sociedad.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(sociedad.rfc);
            buffer.push('</td>');
            buffer.push('<td class="text-center"><button id="eliminar-sociedad-');
            buffer.push(i);
            buffer.push('" class="btn btn-danger btn-sm eliminar-sociedad"><i class="fa fa-minus-circle"></i></button></td>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        $('#sociedades-table tbody').html(buffer.join(''));
    }

    utils.getClavesArchivos = function () {
        var archivos = utils.getArchivos();
        var claves = [];
        for (var i = 0; i < archivos.length; i++) {
            var file = archivos[i];
            var id = file.id;
            var tipo = id.substring(0, id.length - '-pdf'.length);
            claves.push(tipo);
        }
        return claves.join(',');
    };

    utils.getArchivos = function () {
        $('#documentacion-predio-pdf,#acta-constitutiva-sociedad-pdf,#lista-socios-pdf,#documentacion-rendimiento-solicitado-pdf').prop('disabled', true);
        var files = $('input.form-control-file:enabled');
        $('#documentacion-predio-pdf,#acta-constitutiva-sociedad-pdf,#lista-socios-pdf,#documentacion-rendimiento-solicitado-pdf').prop('disabled', false);
        var fds = [];

        // Agregamos archivos del documento
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            var id = file.id;
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/trigo/resources/productores/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            var archivo = $('#' + id)[0].files[0];
            fd.append('file', archivo);
            var etiqueta = $('label[for=' + file.id + ']').html();
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }

        // Agregamos archivos predios.
        for (i = 0; i < data.anexosPredio.length; i++) {
            var anexo = data.anexosPredio[i];
            var id = anexo.attr('id');
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/trigo/resources/productores/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            var etiqueta = 'Documentación predio (PDF) [' + (i + 1) + ']';
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }
        // Agregamos archivos de maximos solicitados.
        for (i = 0; i < data.anexosPrediosolicitado.length; i++) {
            var anexo = data.anexosPrediosolicitado[i];
            var id = anexo.attr('id');
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/trigo/resources/productores/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            var etiqueta = 'Documentación permiso rendimiento superior (PDF) [' + (i + 1) + ']';
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }

        // Agregamos archivos actas constitutivas.
        for (i = 0; i < data.anexosActaConstitutiva.length; i++) {
            var anexo = data.anexosActaConstitutiva[i];
            var id = anexo.attr('id');
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/trigo/resources/productores/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            var etiqueta = 'Acta constitutiva (PDF) [' + (i + 1) + ']';
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }

        // Agregamos archivos lista de socios.
        for (i = 0; i < data.anexosListaSocios.length; i++) {
            var anexo = data.anexosListaSocios[i];
            var id = anexo.attr('id');
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/trigo/resources/productores/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            var etiqueta = 'Lista de socios (PDF) [' + (i + 1) + ']';
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }

        return fds;
    };

    utils.limpiar = function () {
        data.contratos = [];
        data.predios = [];
        data.sociedades = [];
        data.socios = [];
        data.anexosPredio = [];
        data.anexosPrediosolicitado = [];
        data.anexosActaConstitutiva = [];
        data.anexosListaSocios = [];
        data.uuid = null;
        $('#div-table-predios').html(utils.construyePredios);
        utils.verificaSemilla();
        // Reseteamos los valores de los select con lógica a 0 (Seleccione)
        $('#domicilio-productor,#estado-direccion,#firmo-contrato,#tipo-posesion-predio').val('0').change().limpiaErrores();
        utils.resetProductor();
        utils.resetPredios();
        $('input.form-control-file').val('');
        $('#datos-contacto input.valid-field').val('');
        $('#domicilio-productor input.valid-field').val('');
        utils.limpiaContrato();
        utils.limpiaPredio();
        $('#datos-bancarios-productor select').val('0').limpiaErrores();
        $('#datos-bancarios-productor input').val('').limpiaErrores();
        $('#datos-beneficiario input').val('').limpiaErrores();
        $('#nombre-encargado-registro').val('').limpiaErrores();
        $('#datos-beneficiario select').val('0').limpiaErrores();
        $('#productor-persona-moral input').val('').limpiaErrores();
        $('#productor-persona-moral select').val('0').limpiaErrores();
        $('#datos-contacto input').val('').limpiaErrores();
        $('#datos-contacto select').val('0').limpiaErrores();
        $('#domicilio-productor input').val('').limpiaErrores();
        $('#domicilio-productor select').val('0').limpiaErrores();
        $('#cantidad-predios').val('').limpiaErrores();
        $('body,html').animate({scrollTop: 0}, 500);
    };

    utils.curpDuplicado = function (curp) {
        for (var i = 0; i < data.socios.length; i++) {
            if (data.socios[i].curp === curp) {
                return true;
            }
        }
        return false;
    };

    utils.getRendimiento = function (estado, tipoCultivo) {
        for (var i = 0; i < data.rendimientos.length; i++) {
            var r = data.rendimientos[i];
            var clave = r.clave + ':';
            if (clave.includes(tipoCultivo.clave) && clave.includes(':' + estado.clave + ':')) {
                return parseFloat(r.valor);
            }
        }
        return 0;
    };

    utils.verificaSemilla = function () {
        for (var i = 0; i < data.predios.length; i++) {
            var c = data.predios[i].tipoCultivo;
            if (c.clave === 'trigo-semilla') {
                $('#constancia-snics-pdf').prop('disabled', false);
                $('#datos-semilla').show();
                return;
            }
        }
        $('#constancia-snics-pdf').prop('disabled', true);
        $('#datos-semilla input.valid-field').val('').limpiaErrores();
        $('#datos-semilla').hide();
        handlers.eliminaFoliosSnics();
    };


    utils.agregaTotales = function () {
        var volumenTot = 0;
        var volumenSolTot = 0;
        var superficieTot = 0;
        var rendimientoTot = 0;
        var rendimientoSolTot = 0;
        for (var p in data.predios) {
            volumenTot += parseFloat(data.predios[p].volumen);
            volumenSolTot += parseFloat(data.predios[p].volumenSolicitado);
            superficieTot += parseFloat(data.predios[p].superficie);
        }

        rendimientoTot = parseFloat(volumenTot / superficieTot);
        rendimientoSolTot += parseFloat(volumenSolTot / superficieTot);
        var isSolicitado = volumenTot !== volumenSolTot;
        var buffer = [];
        buffer.push('<tfoot class="thead-dark">');
        buffer.push('<tr>');
        buffer.push('<th colspan="8">Total</th>');
        buffer.push('<th class="text-right">' + volumenTot.toFixed(2)
                + (isSolicitado ? utils.setRojo(volumenSolTot.toFixed(2)) : '') + '</th>');
        buffer.push('<th class="text-right">' + superficieTot.toFixed(2) + '</th>');
        buffer.push('<th class="text-right">' + rendimientoTot.toFixed(2)
                + (isSolicitado ? utils.setRojo(rendimientoSolTot.toFixed(2)) : '') + '</th>');
        buffer.push('<th colspan="1"></th>');
        buffer.push('</tr>');
        buffer.push('</tfoot>');
        return buffer.join('');
    };

    utils.cambiaClabe = function () {
        var clabe = $('#clabe-productor').val();
        if (clabe !== '') {
            var banco = $.segalmex.get(data.bancos, clabe.substring(0, 3), 'clave');
            if (!banco) {
                alert('Error: El número de CLABE no pertenece a ningún banco.');
                $('#clabe-productor,#numero-cuenta-productor').val('');
                $('#banco-productor').val('0');
                return;
            }
            $('#clabe-productor').val(clabe);
            $('#banco-productor').val(banco.id);
            $('#numero-cuenta-productor').val(clabe.substring(6, clabe.length - 1));
        }

    };

    utils.confirmaCurp = function () {
        var curp = $('#curp-productor').val();
        $('#rfc-productor,#nombre-productor,#papellido-productor,#sapellido-productor').prop('disabled', true).val('').limpiaErrores();
        data.contratos = [];
        utils.construyeContratos();
        if (curp !== '') {
            var ciclo = $("#ciclo-agricola").val();
            var url = '/trigo/resources/productores/curp/' + curp;
            //Si el ciclo seleccionado es OI-2022 buscamos su pre registro.
            if (data.cicloActual.clave === 'oi-2022') {
                url = '/trigo/resources/productores/maiz/pre-registro/curp/' + curp;
            }
            $.ajax({
                url: url,
                data: {ciclo: ciclo},
                type: 'GET',
                dataType: 'json'
            }).done(function (response) {
                $('#curp-productor').val(response.curp);
                $('#rfc-productor').val(response.rfc);
                $('#nombre-productor').val(response.nombre ? response.nombre : response.persona.nombre);
                $('#papellido-productor').val(response.primerApellido ? response.primerApellido : response.persona.primerApellido);
                $('#sapellido-productor').val(response.segundoApellido ? response.segundoApellido : response.persona ? response.persona.segundoApellido : '');
                $('#numero-telefono').val(response.telefono ? response.telefono : '');
                $('#correo-electronico').val(response.correoElectronico ? response.correoElectronico : '');
                $('#rfc-productor,#nombre-productor,#papellido-productor,#sapellido-productor').prop('disabled', true);
                handlers.cambiaCurpProductor(response.curp);
                if (response.parentesco && response.parentesco.clave !== 'no-disponible') {
                    $('#nombre-beneficiario').val(response.nombreBeneficiario);
                    $('#apellidos-beneficiario').val(response.apellidosBeneficiario);
                    $('#curp-beneficiario').val(response.curpBeneficiario);
                    $('#curp-beneficiario-confirmacion').val(response.curpBeneficiario);
                    $('#parentesco-beneficiario').val(response.parentesco.id);
                }
                if (response.activo === false) {
                    alert('El productor ya no esta activo.');
                }
                $('#guardar-button').prop('disabled', false);
                utils.getContratosProductor(response);
            }).fail(function () {
                if (data.cicloActual.clave === 'oi-2022') {
                    $('#guardar-button').prop('disabled', true);
                    alert('Error: No se encontró la información del pre registro del productor.');
                } else {
                    $('#curp-productor').val(curp);
                    handlers.cambiaCurpProductor(curp);
                    $('#rfc-productor,#nombre-productor,#papellido-productor,#sapellido-productor').prop('disabled', false).val('').limpiaErrores();
                    $('#guardar-button').prop('disabled', false);
                }
            });
        }
    };

    utils.isInvalidaVolumen = function () {
        var volumen = 0;
        for (var i = 0; i < data.predios.length; i++) {
            volumen += parseFloat(data.predios[i].volumen);
        }
        volumen += parseFloat($("#volumen-predio").val());
        return volumen > data.volumenPredio;
    };

    utils.isInvalidaSuperficie = function () {
        var superficie = 0;
        for (var i = 0; i < data.predios.length; i++) {
            superficie += parseFloat(data.predios[i].superficie);
        }
        superficie += parseFloat($("#superficie-predio").val());
        return superficie > data.superficiePredio;
    };

    utils.cambiaGrupoIndigena = function () {
        var pertenece = $('#pertenece-grupo-indigena').val();
        $('#grupos-indigenas').prop('disabled', true).val('0').limpiaErrores();
        if (pertenece === 'true') {
            $('#grupos-indigenas').prop('disabled', false);
        }
    };

    utils.filtraCultivos = function (tipos, ciclo) {
        var tiposCultivo = [];
        if (ciclo === undefined) {
            return tiposCultivo;
        }
        for (var i = 0; i < tipos.length; i++) {
            var tc = tipos[i];
            if (ciclo.clave === 'pv-2021' && tc.clave === 'trigo-cristalino') {
                continue;
            } else {
                tiposCultivo.push(tc);
            }
        }
        return tiposCultivo;
    };

    utils.getContratosProductor = function (productor) {
        var datos = {
            curp: productor.curp,
            rfc: productor.rfc
        };
        $.ajax({
            url: '/trigo/resources/contratos/maiz/contratos-productor/',
            type: 'GET',
            data: datos,
            dataType: 'json'
        }).done(function (response) {
            if (response.length > 0) {
                for (var i = 0; i < response.length; i++) {
                    var cp = response[i];
                    var c = {
                        numeroContrato: cp.numeroContrato,
                        folio: cp.folioContrato,
                        empresa: cp.nombreEmpresaContrato,
                        cantidadContratada: cp.volumenContrato,
                        cantidadContratadaCobertura: cp.volumenIar
                    };
                    data.contratos.push(c);
                }
            }
            utils.construyeContratos();
        }).fail(function () {
            alert('Error: No se encontró información de contratos del productor.');
        });
    };
})(jQuery);