/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.trigo.inscripcion.factura');
    var data = {
        fechaLimitePV: '2020-08-31 23:59:59',
        comprobantes: [],
        totalToneladas: 0,
        totalVolumenComprobantes: 0,
        limiteArchivo: 24 * 1024 * 1024,
        faltaComprobantes: false,
        aplicaAjuste: false
    };
    var handlers = {};
    var utils = {};

    $.segalmex.trigo.inscripcion.factura.init = function () {
        utils.cargaCatalogos();
        utils.inicializaValidaciones();
        $('#adjuntar-button').click(handlers.adjunta);
        $('#regresar-button').click(handlers.regresa);
        $('#guardar-button').click(handlers.guarda);
        $('#cancelar-modal-button').click(handlers.cancelaModal);
        $('#guardar-modal-button').click(handlers.guardaFacturaMoral);
        $('#limpiar-button').click(utils.limpiar);
        $('#facturas-relacionadas-global table tbody').on('click', 'input:checkbox', handlers.seleccionaFactura);
        $('#facturas-relacionadas-global table #todas-checkbox').click(handlers.seleccionaTodos);
        $('#agregar-comprobante-button').click(utils.agregaComprobante);
        $('#comprobante-recepcion-pdf,#cfdi-pdf,#recibo-etiqueta-pdf,#factura-xml').change(handlers.verificaTamanoArchivo);
        $('#div-table-comprobantes').html(utils.creaTablaComprobantes());
        $('#cancelar-comprobantes').click(utils.cancelaComprobantes);
        $('#aceptar-comprobantes').click(utils.aceptaComprobantes);
        $('#precio-tonelada-real').change(handlers.cambiaAjuste);
        $('#tipo-cultivo').change(utils.cambioTipoCultivo);
    };

    handlers.cambiaAjuste = function () {
        var precioTonelada = parseFloat($('#precio-tonelada').val());
        var real = parseFloat($('#precio-tonelada-real').val());
        var ajuste = real - precioTonelada;
        var realFxd = ajuste.toFixed(3);
        $('#precio-ajuste').val(!isNaN(realFxd) && realFxd !== Infinity ? realFxd : '');
    };

    handlers.adjunta = function (e) {
        var tipo = $('#tipo-factura').val();
        if (tipo === '0') {
            alert('Error:\n\n * Tipo de factura es requerido.');
            return;
        }

        if ($('#factura-xml').val() === '') {
            alert('Error:\n\n * CFDI (XML) es requerido.');
            return;
        }

        var fd = new FormData();
        var archivo = $('#factura-xml')[0].files[0];
        fd.append('file', archivo);

        $.ajax({
            url: '/trigo/resources/facturas/cfdi/' + tipo + '?sucursales=false',
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false
        }).done(function (response) {
            data.totalToneladas = parseFloat(response.totalToneladas).toFixed(3);
            data.precioTonelada = response.precioTonelada ? response.precioTonelada : 0;
            utils.muestraCfdi(response);
            data.cfdi = response;
            // Limpiamos uuid y registrada
            data.uuid = null;
            data.registrada = null;
            utils.configuraPrecioTonelada(data.aplicaAjuste);
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Error: No se pudo agregar la factura, verifique el xml.');
            }
        });
    };

    handlers.regresa = function (e) {
        $('#archivo-xml-factura').show();
        $('#datos-factura').hide();
        $('#factura-invalida').hide();
        utils.limpiar();
    };

    handlers.seleccionaFactura = function (e) {
        $('#todas-checkbox').prop('checked', false);
        var toneladas = 0;
        $('#facturas-relacionadas-global table tbody input:checked').each(function () {
            toneladas += parseFloat($(this).val());
        });
        $('#total-toneladas').val(toneladas.toFixed(3));
    }

    handlers.seleccionaTodos = function (e) {
        var checked = $(e.target).is(':checked');
        $('#facturas-relacionadas-global table tbody input:checkbox').prop('checked', checked);
        var toneladas = 0;
        $('#facturas-relacionadas-global table tbody input:checked').each(function () {
            toneladas += parseFloat($(this).val());
        });
        $('#total-toneladas').val(toneladas.toFixed(3));
    }

    handlers.guarda = function (e) {
        $('#factura-persona-moral .valid-field,#numero-contrato,#firmo-contrato,'
                + '#fecha-pago,#tipo-cultivo,#estado-factura,#cantidad-comprobantes,#precio-tonelada-real').limpiaErrores();
        var errores = [];
        $('#factura-persona-moral .valid-field,#numero-contrato,#firmo-contrato,'
                + '#fecha-pago,#tipo-cultivo,#estado-factura,#cantidad-comprobantes,#precio-tonelada-real').valida(errores, true);
        $('#guardar-button').html("Guardando...").prop('disabled', true);
        if (errores.length > 0) {
            $('#guardar-button').html("Guardar").prop('disabled', false);
            return;
        }
        if (data.cfdi.totalToneladas === undefined) {
            alert('Error: La unidad de los conceptos del CFDI no está en Toneladas o Kilogramos.');
            $('#guardar-button').html("Guardar").prop('disabled', false);
            return;
        }

        var comprobantes = parseInt($('#cantidad-comprobantes').val(), 10);
        if (comprobantes !== data.comprobantes.length && data.cfdi.tipo === 'productor') {
            alert('Error: La cantidad de comprobantes es diferente a los comprobantes agregados.');
            $('#guardar-button').html("Guardar").prop('disabled', false);
            return;
        }

        var inscripcion = {
            cfdi: {uuid: data.cfdi.uuid}
        };
        if (data.uuid) {
            inscripcion.uuid = data.uuid;
        }

        if (data.cfdi.rfcReceptor === 'XAXX010101000') {
            alert('Error: El RFC del receptor es de venta al público.');
            $('#guardar-button').html("Guardar").prop('disabled', false);
            return;
        }

        if ($.segalmex.archivos.validaArchivos('#div-documentos input.form-control-file:enabled')) {
            $('#guardar-button').html("Guardar").prop('disabled', false);
            return;
        }

        if (data.comprobantes.length > 0) {
            if (parseFloat(data.totalVolumenComprobantes.toFixed(3)) > parseFloat(data.totalToneladas)) {
                alert('Error: El volumen comprobado (' + data.totalVolumenComprobantes.toFixed(3) + ') es mayor a la cantidad total de los conceptos (' + data.totalToneladas + ').');
                $('#guardar-button').html("Guardar").prop('disabled', false);
                return;
            }

            if (parseFloat(data.totalVolumenComprobantes.toFixed(3)) < parseFloat(data.totalToneladas) && !data.faltaComprobantes) {
                $('#comprobantes-modal').modal('show');
                $('#guardar-button').html("Guardar").prop('disabled', false);
                return;
            }
        }

        switch (data.cfdi.tipo) {
            case 'global':
                utils.generaInscripcionFacturaGlobal(data.cfdi, inscripcion);
                break;
            case 'productor':
                utils.generaInscripcionFacturaProductor(data.cfdi, inscripcion);
                break;
            default:
                throw 'Error en tipo de CFDI';
        }
    };

    handlers.verificaTamanoArchivo = function (e) {
        var extension = e.target.id.includes('xml') ? 'xml' : 'pdf';
        $.segalmex.archivos.verificaArchivo(e, extension, data.limiteArchivo);
    };

    utils.generaInscripcionFacturaProductor = function (cfdi, inscripcion) {
        if ($('#firmo-contrato').val() === 'true' && $('#firmo-contrato').is(':visible')) {
            var numero = $('#numero-contrato').val();
            var contrato = utils.contratoExistente(numero);
            if (!contrato) {
                alert('Error: El contrato no está registrado.');
                $('#guardar-button').html("Guardar").prop('disabled', false);
                return;
            }
            if (contrato.estatus.clave !== 'positiva') {
                if (!confirm('La inscripcion ' + contrato.folio
                        + ' del contrato número ' + contrato.numeroContrato
                        + ' no está validado positivamente. ¿Desea continuar?')) {
                    $('#guardar-button').html("Guardar").prop('disabled', false);
                    return;
                }
            }
        }

        switch (cfdi.rfcEmisor.length) {
            case 12: // Factura emitida por una persona moral
                if (data.cfdi.totalToneladas && parseFloat($('#cantidad-factura').val()) > data.cfdi.totalToneladas) {
                    alert('Error: La cantidad seleccionada es mayor que el total de toneladas de la factura.');
                    $('#guardar-button').html("Guardar").prop('disabled', false);
                    return;
                }
                inscripcion.cantidad = $('#cantidad-factura').val();
                inscripcion.asociado = {id: $('#asociado').val()};
                break;
            case 13: // Factura emitida por una persona fisica
                inscripcion.productorCiclo = {};
                inscripcion.productorCiclo.productor = {rfc: cfdi.rfcEmisor};
                break;
            default:
                alert('Error: El RFC es incorrecto.');
                $('#guardar-button').html("Guardar").prop('disabled', false);
                return;

        }
        inscripcion.cfdi = {uuid: cfdi.uuid};
        inscripcion.contrato = $('#numero-contrato').val();
        inscripcion.tipoCultivo = {id: $('#tipo-cultivo').val()};
        inscripcion.region = {id: $('#estado-factura').val()};
        inscripcion.fechaPago = $.segalmex.date.fechaToIso($('#fecha-pago').val());
        inscripcion.comprobantes = data.comprobantes;
        inscripcion.numeroComprobantes = data.comprobantes.length;
        inscripcion.cantidadComprobada = data.totalVolumenComprobantes;
        inscripcion.precioToneladaReal = $('#precio-tonelada-real').val();
        utils.enviaInscripcion(inscripcion, 'productor');
    };

    utils.generaInscripcionFacturaGlobal = function (cfdi, inscripcion) {
        var seleccionados = $('#facturas-relacionadas-global table tbody input:checked');
        if (seleccionados.length === 0) {
            alert('Error: Es necesario seleccionar al menos una factura.');
            $('#guardar-button').html("Guardar").prop('disabled', false);
            return;
        }
        var facturas = [];
        seleccionados.each(function () {
            var uuid = this.id.substring(0, this.id.length - '-factura-check'.length);
            facturas.push(uuid);
        });

        var total = parseFloat($('#total-toneladas').val());
        if (total > cfdi.totalToneladas) {
            alert('Error: El total de toneladas de la factura global es menor al total de las facturas de productores seleccionadas.');
            $('#guardar-button').html("Guardar").prop('disabled', false);
            return;
        }

        switch (cfdi.rfcReceptor.length) {
            case 12:
            case 13:
                inscripcion.cfdi = {uuid: cfdi.uuid};
                inscripcion.facturasUuid = facturas;
                utils.enviaInscripcion(inscripcion, 'global');
                break
            default:
                $('#guardar-button').html("Guardar").prop('disabled', false);
                alert('Error: El RFC es incorrecto.');
        }
    };

    utils.enviaInscripcion = function (inscripcion, tipo) {
        inscripcion.ciclo = {id: $("#ciclo-agricola").val()};
        inscripcion.claveArchivos = utils.getClavesArchivos();
        $.ajax({
            type: 'POST',
            url: '/trigo/resources/facturas/maiz/inscripcion/' + tipo,
            data: JSON.stringify(inscripcion),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            data.uuid = response.uuid;
            data.registrada = response.registrada;
            data.totalToneladas = 0;
            $('#carga-archivos').cargaArchivos({
                archivos: utils.getArchivos(),
                callBackCerrar: utils.cierraPlugin,
                btnDescarga: false,
                indicaciones: `Por favor espere mientras se anexan los documentos al registro con folio: <strong>${response.folio}.</strong>`
            });
            $('#guardar-button').html("Guardar").prop('disabled', false);
        }).fail(function (jqXHR) {
            $('#guardar-button').html("Guardar").prop('disabled', false);
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Error: La factura no se pudo registrar.');
            }

        });
    };

    utils.getClavesArchivos = function () {
        $('#factura-xml').prop('disabled', true);
        var archivos = $('input.form-control-file:enabled');
        $('#factura-xml').prop('disabled', false);
        var claves = [];
        for (var i = 0; i < archivos.length; i++) {
            var file = archivos[i];
            var id = file.id;
            var tipo = id.substring(0, id.length - '-pdf'.length);
            claves.push(tipo);
        }
        return claves.join(',');
    };

    utils.getArchivos = function () {
        $('#factura-xml').prop('disabled', true);
        var files = $('input.form-control-file:enabled');
        $('#factura-xml').prop('disabled', false);

        var fds = [];
        // Agregamos archivos anexos.
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            var id = file.id;
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/trigo/resources/facturas/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            var archivo = $('#' + id)[0].files[0];
            fd.append('file', archivo);
            var etiqueta = $('label[for=' + file.id + ']').html();
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }
        return  fds;

    };

    utils.cierraPlugin = function () {
        if (data.registrada) {
            alert('Advertencia: La factura ya estaba registrada previamente.');
        }
        $('#datos-factura').hide();
        $('#archivo-xml-factura').show();
        utils.limpiar();
    };

    utils.cargaCatalogos = function () {
        $.ajax({
            type: 'GET',
            url: '/trigo/resources/paginas/inscripcion-factura/',
            dataType: 'json'
        }).done(function (response) {
            if (response.sucursal === undefined) {
                alert('Error: No cuentas con una sucursal para registrar.');
                return;
            }

            $('#cultivo').actualizaCombo(response.cultivos).prop('disabled', true);
            $('#cultivo').val($.segalmex.get(response.cultivos, 'trigo', 'clave').id);
            $('#ciclo-agricola-global').cicloAgricola({
                ciclos: response.ciclos,
                actual: response.cicloSeleccionado,
                reload: true,
                sistema: 'trigo'
            });
            $('#entidad-sucursal').actualizaCombo(response.estados).prop('disabled', true);

            // datos de la sucursal
            $('#nombre-sucursal').val(response.sucursal.empresa.nombre);
            $('#rfc-sucursal').val(response.sucursal.empresa.rfc);
            $('#entidad-sucursal').val(response.sucursal.estado.id).prop('disabled', true);
            $('#municipio-sucursal').val(response.sucursal.municipio.nombre);
            $('#localidad-sucursal').val(response.sucursal.localidad);
            $('#tipo-cultivo').actualizaCombo(response.tipos);
            data.tiposCultivo = response.tipos;
            $('#estado-factura').actualizaCombo([]);
            data.regiones = response.regiones;
            utils.aplicaContrato(response.aplicaContrato);
            data.sucursal = response.sucursal;
            data.aplicaAjuste = response.aplicaAjusteFactura;
            utils.aplicaReciboEtiqueta(response.aplicaReciboEtiqueta);
            if (response.registroCerrado) {
                alert("El registro de facturas cerró el " + response.registroCerrado.valor);
            }
        }).fail(function () {
            alert('Error: Al descargar los recursos de la página.');
        });
    };

    utils.cargaContratos = function () {
        $.ajax({
            type: 'GET',
            url: '/trigo/resources/contratos/maiz/simplificados/',
            dataType: 'json'
        }).done(function (response) {
            data.contratosRegistrados = response;
            $('#numero-contrato').typeahead({
                hint: true,
                highlight: true,
                minLength: 3
            }, {
                name: 'contratos',
                source: utils.contratosMatcher(data.contratosRegistrados)
            });
        })
    }

    utils.contratosMatcher = function (contratos) {
        return function (q, cb) {
            var matches = [];
            // regex used to determine if a string contains the substring `q`
            var substrRegex = new RegExp(q, 'i');
            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(contratos, function (i, contrato) {
                if (substrRegex.test(contrato.numeroContrato)) {
                    matches.push(contrato.numeroContrato);
                }
            });
            cb(matches);
        };
    };

    utils.contratoExistente = function (numero) {
        for (var i = 0; i < data.contratosRegistrados.length; i++) {
            if (data.contratosRegistrados[i].numeroContrato === numero) {
                return data.contratosRegistrados[i];
            }
        }
        return null;
    };

    utils.inicializaValidaciones = function () {
        $('.valid-field').configura();
        $('#cantidad-factura').configura({
            type: 'number',
            min: 0
        });
        $('#numero-contrato').configura({
            pattern: /^[A-Z]{3}-[A-Z]{2}[0-9]{2}-[A-Z0-9]{3}-\d{6}-(P|C|S|D|E)-\d{3}$/,
            minlength: 25,
            maxlength: 25
        });
        var fechaMin = new Date($.segalmex.fecha);
        fechaMin.setFullYear(fechaMin.getFullYear() - 1);
        var fechaMax = new Date($.segalmex.fecha);
        $('#fecha-pago').configura({
            type: 'date',
            min: fechaMin,
            max: fechaMax,
            required: true
        });
        $('#fecha-pago').datepicker({
            language: "es",
            autoclose: true,
            forceParse: false,
            format: 'dd/mm/yyyy',
            yearRange: "c-10:c"
        });

        $('#volumen-comprobante,#cantidad-comprobantes').configura({
            required: true,
            type: 'number',
            min: 0
        });

        $('#descripcion-comprobante').configura({
            required: true,
            maxlength: 127
        });

        $('.valid-field').validacion();
    };

    utils.muestraCfdi = function (cfdi) {
        $('#rfc-emisor').val(cfdi.rfcEmisor);
        $('#nombre-emisor').val(cfdi.nombreEmisor);
        $('#rfc-receptor').val(cfdi.rfcReceptor);
        $('#nombre-receptor').val(cfdi.nombreReceptor);
        $('#uuid-factura').val(cfdi.uuidTimbreFiscalDigital);
        $('#fecha-factura').val(cfdi.fecha ? cfdi.fecha.substring(0, cfdi.fecha.length - 6).replace('T', ' ') : '');
        $('#fecha-timbrado-factura').val(cfdi.fechaTimbrado ? cfdi.fechaTimbrado.substring(0, cfdi.fechaTimbrado.length - 6).replace('T', ' ') : '');
        $('#metodo-pago-factura').val(cfdi.metodoPago);
        $('#moneda-factura').val(cfdi.moneda);
        $('#total-factura').val(cfdi.total);
        $('#subtotal-factura').val(cfdi.subtotal ? cfdi.subtotal : 0);
        $('#precio-tonelada').val(data.precioTonelada);
        $('#factura-persona-moral').hide();

        var buffer = [];
        for (var i = 0; i < cfdi.conceptos.length; i++) {
            var c = cfdi.conceptos[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-center">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td class="text-center">');
            buffer.push(c.cantidad);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(c.unidad);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(c.descripcion);
            buffer.push('</td>');
            buffer.push('<td class="text-center">');
            buffer.push(c.valorUnitario);
            buffer.push('</td>');
            buffer.push('<td class="text-center">');
            buffer.push(c.importe);
            buffer.push('</td>');
            buffer.push('<td class="text-center">');
            buffer.push(c.claveProductoServicio);
            buffer.push('</td>');
            buffer.push('<td class="text-center">');
            buffer.push(c.estatus === 'positivo'
                    ? '<i class="fas fa-check-circle text-success"></i>'
                    : '<i class="fas fa-exclamation-circle text-warning"></i>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        $('#conceptos-table tbody').html(buffer.join(''));

        $('#archivo-xml-factura').hide();
        $('#factura-xml').val(null);

        $('#guardar-button').prop('disabled', true);
        $('#div-alert .alert').hide();
        $('#firmo-contrato,#fecha-pago,#tipo-cultivo,#estado-factura,#numero-contrato').prop('disabled', true);
        switch (cfdi.tipo) {
            case 'productor':
                if (!cfdi.productor && !cfdi.asociados) {
                    $('#factura-productor-no-autorizada-alert').show();
                    break;
                }
                if (cfdi.asociados) {
                    $('#asociado').actualizaCombo(cfdi.asociados, {nombre: 'label'});
                    $('#factura-persona-moral').show();
                }
                $('#firmo-contrato,#fecha-pago,#tipo-cultivo,#estado-factura,#numero-contrato').prop('disabled', false);
                $('#factura-productor-alert').show();
                $('#div-comprobantes,#div-precio-tonelada').show();
                $('#guardar-button').prop('disabled', false);
                break;
            case 'global':
                $('#facturas-relacionadas-global table tbody').html(utils.muestraFacturasNuevas(cfdi));
                $('#factura-global-alert,#facturas-relacionadas-global').show();
                $('#guardar-button').prop('disabled', false);
                $('#div-comprobantes,#div-precio-tonelada').hide();
                break;
            default:
                alert('Error en el tipo de factura.');

        }
        $('#datos-factura').show();
    };

    utils.muestraFacturasNuevas = function (cfdi) {
        if (!cfdi.facturas) {
            return '<tr><td colspan="7" class="text-center">Sin facturas de productores <strong>validadas</strong> para asociar a la factura global.</td></tr>';
        }

        var buffer = [];
        for (var i = 0; i < cfdi.facturas.length; i++) {
            var factura = cfdi.facturas[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push('<input type="checkbox" id="' + factura.uuid + '-factura-check" class="factura-check" value="' + factura.toneladasTotales + '"/>')
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(factura.rfcEmisor);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(factura.nombreEmisor);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(factura.uuidTimbreFiscalDigital);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push($.segalmex.date.isoToFechaHora(factura.fecha));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(factura.toneladasTotales);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(factura.total);
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        return buffer.join('');
    };

    utils.limpiar = function () {
        $('#tipo-cultivo').val('0').limpiaErrores().change();
        $('#tipo-factura,#estado-factura').val('0').limpiaErrores();
        $('#recibo-etiqueta-pdf').val('').prop('disabled', true);
        $('#factura-xml,#cfdi-pdf,#numero-contrato,#cantidad-factura,#fecha-pago').val('').limpiaErrores();
        $('#numero-contrato').typeahead('val', '').prop('disabled', false).limpiaErrores();
        $('#todas-checkbox').prop('checked', false);
        $('#total-toneladas').val('0.0');
        $('#cantidad-comprobantes,#precio-ajuste,#precio-tonelada-real,#precio-tonelada').limpiaErrores().val('');
        $('#facturas-relacionadas-global table tbody').html('');
        $('#facturas-relacionadas-global').hide();
        utils.reiniciaComprobantes();
        data.faltaComprobantes = false;
    };

    utils.reiniciaComprobantes = function () {
        data.comprobantes = [];
        $('#div-table-comprobantes').html(utils.creaTablaComprobantes());
        utils.limpiaComprobantes();
        utils.verificaComprobantes();
    };

    utils.agregaComprobante = function () {
        $('#descripcion-comprobante,#volumen-comprobante').limpiaErrores();
        var errores = [];
        $('#descripcion-comprobante,#volumen-comprobante').valida(errores, true);
        if (errores.length > 0) {
            return;
        }
        var comprobante = {
            descripcion: $('#descripcion-comprobante').val(),
            volumen: parseFloat($('#volumen-comprobante').val()).toFixed(3)
        };
        data.comprobantes.push(comprobante);
        $('#div-table-comprobantes').html(utils.creaTablaComprobantes());
        $('#comprobantes-table').on('click', 'button.eliminar-comprobante', handlers.eliminaComprobante);
        utils.limpiaComprobantes();
        utils.verificaComprobantes();

    };

    utils.verificaComprobantes = function () {
        if (data.comprobantes.length > 0) {
            $('#comprobante-recepcion-pdf').prop('disabled', false);
            var totalVolumenComprobantes = parseFloat(data.totalVolumenComprobantes.toFixed(3));
            if (totalVolumenComprobantes < parseFloat(data.totalToneladas)) {
                $('#faltantes-alert').show();
                var resta = parseFloat(data.totalToneladas) - totalVolumenComprobantes;
                $('.faltantes').html(resta.toFixed(3));
            } else {
                $('#faltantes-alert').hide();
                $('.faltantes').html('');
            }
        } else {
            $('#comprobante-recepcion-pdf').val('').prop('disabled', true);
            $('#faltantes-alert').hide();
            $('.faltantes').html('');
        }

    };

    utils.creaTablaComprobantes = function () {
        var buffer = `
        <div class="table-responsive">
            <table id = "comprobantes-table" class="table table-striped table-bordered table-hover">
              <thead class="thead-dark">
                <tr>
                  <th>#</th>
                  <th>Descripción</th>
                  <th>Volumen (t)</th>
                  <th><i class="fa fa-minus-circle"></i></th>
                </tr>
              </thead>
              <tbody>
              </tbody>`;

        data.totalVolumenComprobantes = 0;
        var a = 1;
        for (var i = 0; i < data.comprobantes.length; i++) {
            var c = data.comprobantes[i];
            buffer += `
                <tr>
                <td>${a++}</td>
                <td>${c.descripcion}</td>
                <td class="text-right">${c.volumen}</td>
                <td class="text-center">
                    <button id="eliminar-comprobante-${i}" class="btn btn-danger btn-sm eliminar-comprobante">
                    <i class="fa fa-minus-circle"></i></button></td>
                </tr>`;
            data.totalVolumenComprobantes += parseFloat(c.volumen);
        }
        buffer += `
                </tbody>${data.comprobantes.length > 0 ? utils.agregaTotales(data.comprobantes) : ''}
                </table>
                </div>
                <br/>
        `;
        return buffer;
    };

    handlers.eliminaComprobante = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-comprobante-'.length), 10);
        var comprobantes = [];
        for (var i = 0; i < data.comprobantes.length; i++) {
            if (i !== idx) {
                comprobantes.push(data.comprobantes[i]);
            }
        }
        data.comprobantes = comprobantes;
        $('#div-table-comprobantes').html(utils.creaTablaComprobantes());
        $('#comprobantes-table').on('click', 'button.eliminar-comprobante', handlers.eliminaComprobante);
        utils.verificaComprobantes();
    };

    utils.limpiaComprobantes = function () {
        $('#descripcion-comprobante,#volumen-comprobante').val('');
        $('#descripcion-comprobante,#volumen-comprobante').limpiaErrores();
    };

    utils.aplicaContrato = function (aplicaContrato, ciclos) {
        if (aplicaContrato) {
            utils.cargaContratos();
            $('.aplica-contrato').show();
        }
    };

    utils.agregaTotales = function (comprobantes) {
        var volumenTot = 0;
        for (var c in comprobantes) {
            volumenTot += parseFloat(comprobantes[c].volumen);
        }
        var buffer = [];
        buffer.push('<tfoot class="thead-dark">');
        buffer.push('<tr>');
        buffer.push('<th colspan="2">Total</th>');
        buffer.push('<th class="text-right">' + volumenTot.toFixed(3) + '</th>');
        buffer.push('<th colspan="1"></th>');
        buffer.push('</tr>');
        buffer.push('</tfoot>');
        return buffer.join('');
    };

    utils.cancelaComprobantes = function () {
        data.faltaComprobantes = false;
        $('#comprobantes-modal').modal('hide');
    };


    utils.aceptaComprobantes = function () {
        $('#comprobantes-modal').modal('hide');
        data.faltaComprobantes = true;
        handlers.guarda();
    };

    utils.configuraPrecioTonelada = function (aplica) {
        $('#precio-tonelada-real').configura({
            required: true,
            type: 'number',
            min: data.precioTonelada
        });
        $('#precio-tonelada-real').validacion();
        if (aplica) {
            $('#precio-tonelada-real').prop('disabled', false).val(data.precioTonelada);
        } else {
            $('#precio-tonelada-real').prop('disabled', true).val(data.precioTonelada);
            $('#precio-ajuste').val('0');
        }
        $('#precio-tonelada-real').change();
    };

    utils.aplicaReciboEtiqueta = function (aplica) {
        $('#div-recibo-etiqueta').hide();
        if (aplica) {
            $('#div-recibo-etiqueta').show();
        }
    };

    utils.cambioTipoCultivo = function (e) {
        e.preventDefault();
        utils.habilitaReciboEtiqueta(e.target);
        utils.cambiaEstado(e.target);

    };

    utils.cambiaEstado = function (target) {
        var v = $(target).val();
        var tp = v !== '0' ? $.segalmex.get(data.tiposCultivo, v) : {clave: '0'};
        var regiones = [];
        if (tp.clave !== '0') {
            for (var i = 0; i < data.regiones.length; i++) {
                var r = data.regiones[i];
                if (r.tipoCultivo.id === tp.id) {
                    regiones.push(r);
                }
            }
        }
        $('#estado-factura').actualizaCombo(regiones).val('0');
    };

    utils.habilitaReciboEtiqueta = function (target) {
        var tc = $.segalmex.get(data.tiposCultivo, $(target).val());
        if (tc && tc.clave === 'trigo-semilla' && $('#div-recibo-etiqueta').is(':visible')) {
            $('#recibo-etiqueta-pdf').val('').prop('disabled', false);
            return;
        }
        $('#recibo-etiqueta-pdf').val('').prop('disabled', true);
    };
})(jQuery);
