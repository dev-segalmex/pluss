/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.trigo.inscripcion.contrato');
    var data = {
        baseAcordada: {
        },
        precioFuturo: {
        },
        tiposEmpresa: [],
        valorContrato: 136.078,
        coberturas: [],
        anexosContratoMarco: [],
        anexosEstadoCuenta: [],
        anexosCartaConfirmacion: [],
        anexosPoderMandato: [],
        limiteArchivo: 24 * 1024 * 1024,
        estadosDestino: [],
        productores: [],
        claveBC: '2',
        claveSonora: '26'
    };
    var handlers = {};
    var utils = {};

    $.segalmex.trigo.inscripcion.contrato.init = function () {
        utils.inicializaValidaciones();
        utils.cargaCatalogos();
        utils.configuraPantalla();
    };

    handlers.cambiaTipoCultivo = function (e) {
        var val = $(e.target).val();
        var abreviatura = 'X';
        switch (val) {
            case '0':
                break;
            default:
                var tipo = $.segalmex.get(data.tipos, val);
                abreviatura = tipo.abreviatura;
                break;
        }
        $('#contrato-tipo-cultivo-text').html(abreviatura);
        utils.verificaSemilla();
    }

    handlers.cambiaEntidadContrato = function (e) {
        var seleccionada = $(e.target).val();
        var baseAcordada = 0;
        var precioFuturo = 0;

        $('#tipo-precio').val('0');
        $('#precio-futuro,#base-acordada,#precio-fijo').val('0');

        var abreviatura = 'XXX';
        if (seleccionada !== '0') {
            $('#tipo-precio').prop('disabled', false);
            var estado = $.segalmex.get(data.estadosPredio, seleccionada);
            abreviatura = estado.abreviatura;
            baseAcordada = utils.getBaseAcordada(estado);
            precioFuturo = utils.getPrecioFuturo(estado);
        } else {
            $('#tipo-precio').prop('disabled', true);
        }
        $('#contrato-entidad-text').html(abreviatura);

        $('#base-acordada').configura({min: baseAcordada});
        $.validation.init($('#base-acordada')[0]);

        $('#base-acordada').val(baseAcordada);
        $('#precio-futuro').val(precioFuturo);
        handlers.cambiaPrecioContratado();
    }

    handlers.cambiaFechaContrato = function (e) {
        var val = $(e.target).val();
        var ff = '000000';
        if (/^\d{2}\/\d{2}\/\d{4}$/.test(val)) {
            ff = val.substring(0, 2) + val.substring(3, 5) + val.substring(8, 10);
        }
        $('#contrato-fecha-text').html(ff);
    }

    handlers.cambiaConsecutivoContrato = function (e) {
        var consecutivo = '000' + $(e.target).val();
        $('#contrato-consecutivo-text').html(consecutivo.substring(consecutivo.length - 3, consecutivo.length));
    }

    handlers.guardaInscripcion = function () {
        $('#contrato-div .valid-field,#folio-snics,#cantidad-iar').limpiaErrores();
        $('#guardar-button').html("Guardando...").prop('disabled', true);
        var errores = [];

        $('#contrato-div .valid-field,#folio-snics,#cantidad-iar').valida(errores, true);

        if (errores.length !== 0) {
            $('#' + errores[0].campo).focus();
            $('#guardar-button').html("Guardar").prop('disabled', false);
            return;
        }

        if ($('#destino-grano-contrato').val().length === 0) {
            alert('Error: Es necesario seleccionar al menos un estado destino.');
            $('#guardar-button').html('Guardar').prop('disabled', false);
            return;
        }

        if ($.segalmex.archivos.validaArchivos('#div-documentos input.form-control-file:enabled')) {
            $('#guardar-button').html('Guardar').prop('disabled', false);
            return;
        }

        if ($('#cantidad-iar').is(":visible")) {
            if ($('#cantidad-iar').val() === '0') {
                alert('* Es necesario agregar al menos un IAR.');
                $('#guardar-button').html('Guardar').prop('disabled', false);
                return;
            }
            var cantidad = parseInt($('#cantidad-iar').val());
            if (cantidad !== data.coberturas.length) {
                alert('Error: La cantidad de coberturas (IAR) es diferente de las coberturas agregadas.');
                $('#guardar-button').html('Guardar').prop('disabled', false);
                return;
            }
        }
        if ($('#snics').is(":visible")) {
            if ($('#snics-pdf').val() === '') {
                alert('Error: Es necesario agregar el archivo de constancia del SNICS.');
                $('#guardar-button').html('Guardar').prop('disabled', false);
                return;
            }
        }

        //Validaciones contra la sección de productores.
        var superficieTotal = 0.0;
        var volumenTotal = 0.0;
        var noProductores = parseInt($('#numero-productores').val());
        var volumenContratado = parseFloat($('#volumen-contratado').val());
        var superficieContratada = parseFloat($('#superficie-contratada').val());
        for (var i = 0; i < data.productores.length; i++) {
            superficieTotal += parseFloat(data.productores[i].superficie);
            volumenTotal += parseFloat(data.productores[i].volumenContrato);
        }
        if (noProductores !== data.productores.length) {
            alert('La cantidad de productores que respaldan el contrato es diferente a los productores agregados.');
            $('#guardar-button').html('Guardar').prop('disabled', false);
            return;
        }
        if (parseFloat(volumenContratado.toFixed(3)) !== parseFloat(volumenTotal.toFixed(3))) {
            alert('El volumen total contratado debe ser igual al volumen total de los productores.');
            $('#guardar-button').html('Guardar').prop('disabled', false);
            return;
        }
        if (parseFloat(superficieContratada.toFixed(3)) !== parseFloat(superficieTotal.toFixed(3))) {
            alert('La superficie total contratada debe ser igual a la superficie total de los productores.');
            $('#guardar-button').html('Guardar').prop('disabled', false);
            return;
        }

        $('#condiciones-modal').modal('show');
    };

    handlers.aceptoCondiciones = function (e) {
        $('#si-acepto-condiciones,#no-acepto-condiciones').prop('disabled', true);
        utils.guardaInscripcion();
    }

    handlers.noAceptoCondiciones = function (e) {
        $('#condiciones-modal').modal('hide');
        $('#guardar-button').html('Guardar').prop('disabled', false);
    }

    handlers.tipoEmpresaVendedor = function (e) {
        utils.tipoEmpresaChange($(e.target).val(), '#nombre-vendedor', '#rfc-vendedor');
    };

    handlers.tipoEmpresaComprador = function (e) {
        utils.tipoEmpresaChange($(e.target).val(), '#nombre-comprador', '#rfc-comprador');
    };

    handlers.cerrarDescarga = function (e) {
        $('#guardar-button').html('Guardar').prop('disabled', false);
        $('.estatus-pdf').html('');
        utils.limpiar();
        $('#tipo-cultivo').focus();
        utils.limpiaCobertura();
    };

    handlers.cambiaTieneCobertura = function (e) {
        e.preventDefault();
        var val = $(e.target).val();
        var tiposPrecio = [];
        $('#cobertura-form').hide();
        switch (val) {
            case 'true':
                for (var i = 0; i < data.tiposPrecio.length; i++) {
                    switch (data.tiposPrecio[i].clave) {
//                        case 'precio-cerrado':
//                        case 'precio-cerrado-pesos':
//                            break;
                        default:
                            tiposPrecio.push(data.tiposPrecio[i]);
                    }
                }
                $('#cobertura-form').show();
                break;
            case 'false':
                for (var i = 0; i < data.tiposPrecio.length; i++) {
                    switch (data.tiposPrecio[i].clave) {
                        case 'precio-cerrado':
                        case 'precio-cerrado-pesos':
                            tiposPrecio.push(data.tiposPrecio[i]);
                    }
                }
                data.coberturas = [];
                data.anexosContratoMarco = [];
                data.anexosEstadoCuenta = [];
                data.anexosCartaConfirmacion = [];
                data.anexosPoderMandato = [];
                utils.construyeCoberturas();
                utils.limpiaCobertura();
                $('#cantidad-iar').val('0');
                break;
        }
        $('#tipo-precio').actualizaCombo(tiposPrecio);
        $('#precio-futuro,#base-acordada,#precio-fijo').attr('placeholder', 'dólares / tonelada').prop('disabled', true).limpiaErrores().val('0');

    };

    utils.getBaseAcordada = function (estado) {
        return estado && data.baseAcordada['estado' + estado.clave] ? data.baseAcordada['estado' + estado.clave] : 0;
    }

    utils.getPrecioFuturo = function (estado) {
        return estado && data.precioFuturo['estado' + estado.clave] ? data.precioFuturo['estado' + estado.clave] : 0;
    }

    handlers.cambiaTipoPrecio = function (e) {
        var estado = $.segalmex.get(data.estadosPredio, $('#entidad-contrato').val());
        var seleccionada = $(e.target).val();

        $('#precio-futuro,#base-acordada,#precio-fijo').attr('placeholder', 'dólares / tonelada').prop('disabled', true).limpiaErrores().val('0');
        var tipoPrecio = seleccionada !== '0' ? $.segalmex.get(data.tiposPrecio, seleccionada) : {clave: '0'};
        switch (tipoPrecio.clave) {
            case 'precio-cerrado':
                $('#base-acordada').val(utils.getBaseAcordada(estado));
                $('#precio-futuro').prop('disabled', false).val(utils.getPrecioFuturo(estado));
                $('#base-acordada').prop('disabled', false);
                break;
            case 'precio-cerrado-pesos':
                $('#precio-futuro,#base-acordada,#precio-fijo').attr('placeholder', 'pesos mexicanos / tonelada');
                $('#precio-fijo').prop('disabled', false);
                break;
            case 'precio-abierto':
                $('#precio-futuro,#base-acordada').prop('disabled', false).val('0');
                break;
        }
        handlers.cambiaPrecioContratado();
    }

    utils.limpiaCobertura = function () {
        $('#cobertura-div select').val('0').limpiaErrores();
        $('#cobertura-div input').val('').limpiaErrores();
    };

    handlers.cambiaPrecioContratado = function (e) {
        var precioFuturo = $('#precio-futuro').val();
        var baseAcordada = $('#base-acordada').val();
        var precioFijo = parseFloat(precioFuturo) + parseFloat(baseAcordada);
        if (!isNaN(precioFijo)) {
            $('#precio-fijo').val(precioFijo.toFixed(2));
        } else {
            $('#precio-fijo').val('0');
        }
    }

    handlers.verificaTamanoArchivo = function (e) {
        var extension = e.target.id.includes('xlsx') ? 'xlsx' : 'pdf';
        $.segalmex.archivos.verificaArchivo(e, extension, data.limiteArchivos);
    };

    handlers.calculaVolumenCobertura = function (e) {
        var numContratos = $('#cantidad-contratos-iar').val();
        var volumenIar = (numContratos * data.valorContrato).toFixed(3);
        $('#volumen-iar').val(volumenIar);
    };

    handlers.calculaTipoCambioPrecio = function (e) {
        $('#tipo-cambio-precio').val(utils.calculaTipoCambio($('#precio-pesos').val(), $('#precio-dolares').val()).toFixed(2));
    };

    handlers.calculaTipoCambioPrima = function (e) {
        $('#tipo-cambio-prima').val(utils.calculaTipoCambio($('#prima-pesos').val(), $('#prima-dolares').val()).toFixed(2));
    };

    handlers.calculaTipoCambioComision = function (e) {
        $('#tipo-cambio-comisiones').val(utils.calculaTipoCambio($('#comisiones-pesos').val(), $('#comisiones-dolares').val()).toFixed(2));
    };

    utils.calculaTipoCambio = function (spesos, sdolares) {
        var pesos = parseFloat(spesos);
        var dolares = parseFloat(sdolares);

        if (pesos > 0 && dolares > 0) {
            return pesos / dolares;
        }
        return 0;
    }

    handlers.agregaCobertura = function (e) {
        e.preventDefault();
        $(e.target).prop('disabled', true);
        $('#cobertura-div .valid-field').limpiaErrores();
        var errores = [];
        $('#cobertura-div .valid-field').valida(errores, true);
        if (errores.length > 0) {
            $(e.target).prop('disabled', false);
            return;
        }
        if (utils.validaArchivos('#cobertura-div input.form-control-file:enabled')) {
            $(e.target).prop('disabled', false);
            return;
        }

        var cobertura = {
            numero: $('#numero-cuenta-iar').val(),
            entidadCobertura: {id: $('#correduria-iar').val(), nombre: $("#correduria-iar option:selected").text()},
            tipoComprador: {id: $('#comprador-iar').val(), nombre: $("#comprador-iar option:selected").text()},
            nombreComprador: $('#nombre-comprador-iar').val(),
            fechaCompra: $.segalmex.date.fechaToIso($('#fecha-compra-iar').val()),
            fechaVencimiento: $.segalmex.date.fechaToIso($('#fecha-vencimiento-iar').val()),
            tipoOperacionCobertura: {id: $('#tipo-operacion-iar').val(), nombre: $("#tipo-operacion-iar option:selected").text()},
            numeroContratos: $('#cantidad-contratos-iar').val(),
            numeroContratosToneladas: $('#volumen-iar').val(),
            precioEjercicioDolares: $('#precio-dolares').val(),
            precioEjercicioPesos: $('#precio-pesos').val(),
            primaDolares: $('#prima-dolares').val(),
            primaPesos: $('#prima-pesos').val(),
            comisionesDolares: $('#comisiones-dolares').val(),
            comisionesPesos: $('#comisiones-pesos').val()
        };
        data.anexosContratoMarco.push(utils.generaAnexo('contrato-marco-iar-pdf', data.coberturas.length));
        data.anexosEstadoCuenta.push(utils.generaAnexo('estado-cuenta-iar-pdf', data.coberturas.length));
        data.anexosCartaConfirmacion.push(utils.generaAnexo('carta-confirmacion-iar-pdf', data.coberturas.length));
        data.anexosPoderMandato.push(utils.generaAnexo('poder-mandato-iar-pdf', data.coberturas.length));
        data.coberturas.push(cobertura);
        utils.limpiaCobertura();
        utils.construyeCoberturas();
        $(e.target).prop('disabled', false);
    };

    utils.tipoEmpresaChange = function (seleccionada, idNombre, idRfc) {
        $(idNombre).val('').prop('disabled', seleccionada === '0').limpiaErrores();
        $(idRfc).val('').prop('disabled', seleccionada === '0').limpiaErrores();

        if (seleccionada === '0') {
            return;
        }

        var tipoEmpresa = $.segalmex.get(data.tiposEmpresa, seleccionada);
        switch (tipoEmpresa.clave) {
            case 'comercializadora-persona-fisica':
            case 'productor':
                $(idRfc).configura({type: 'rfc-fisica', maxlength: 13});
                $(idNombre).configura({type: 'nombre-original'}).configura({textTransform: 'upper'});
                break;
            default:
                $(idRfc).configura({type: 'rfc-moral', maxlength: 12});
                $(idNombre).configura({type: 'name-moral'});
                break;
        }
        $.validation.init($(idRfc)[0]);
        $.validation.init($(idNombre)[0]);
    };

    utils.guardaInscripcion = function () {
        var inscripcion = utils.generaDatos();
        // Si hay un uuid, lo ponemos para indicar que es un reeenvio
        if (data.uuid) {
            inscripcion.uuid = data.uuid;
        }
        $.ajax({
            url: '/trigo/resources/contratos/maiz/inscripcion/',
            type: 'POST',
            data: JSON.stringify(inscripcion),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            // Guardamos el registro
            data.uuid = response.uuid;
            $('#condiciones-modal').modal('hide');
            $('#si-acepto-condiciones,#no-acepto-condiciones').prop('disabled', false);
            $('#carga-archivos').cargaArchivos({
                archivos: utils.getArchivos(),
                urlComprobante: '/trigo/resources/contratos/maiz/inscripcion/' + data.uuid + '/inscripcion-contrato.pdf',
                callBackCerrar: handlers.cerrarDescarga,
                indicaciones: `Por favor espere mientras se suben los documentos anexos al
                    registro con folio <strong>${response.folio}</strong>.<br/> Al finalizar podrá descargar el PDF.`
            });
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {

                alert("Ocurrió un error al guardar el registro.");
            }
            $('#si-acepto-condiciones,#no-acepto-condiciones').prop('disabled', false);
        });
    };

    utils.inicializaValidaciones = function () {
        var fechaMinima = new Date($.segalmex.fecha);
        fechaMinima.setFullYear(2021);
        fechaMinima.setMonth(2);
        fechaMinima.setDate(01);
        $(".valid-field").configura();

        $('#no-contrato').configura({
            pattern: /^\d{1,3}$/,
            minlength: 1,
            maxlength: 3
        });
        $('#fecha-contrato,#fecha-compra-iar').configura({
            type: 'date',
            max: $.segalmex.fecha
        });
        $('#fecha-contrato,#fecha-compra-iar,#fecha-vencimiento-iar').datepicker({
            language: "es",
            autoclose: true,
            forceParse: false,
            format: 'dd/mm/yyyy',
            yearRange: "c-10:c"
        });
        $('#fecha-vencimiento-iar').configura({
            type: 'date',
            min: fechaMinima
        });

        $('#numero-cuenta-iar').configura({
            type: 'name-moral'
        });

        var nombresPropios = ['#nombre-comprador', '#nombre-vendedor'];
        $(nombresPropios.join(",")).configura({
            type: "nombre-icao"
        });
        var numeros = [
            "#superficie-contratada", '#volumen-contratado', '#rendimiento-contratado',
            '#precio-futuro', '#base-acordada', '#precio-fijo', '#cantidad-iar',
            '#cantidad-contratos-iar', '#volumen-iar', '#precio-dolares', '#precio-pesos', '#prima-dolares',
            '#prima-pesos', '#comisiones-dolares', '#comisiones-pesos'
        ];
        $(numeros.join(',')).configura({
            type: "number",
            min: 0
        });
        $('#numero-productores').configura({
            type: 'number',
            min: 1,
            max: 2000,
            maxlength: 4
        });

        var rfcFisicos = ['#rfc-comprador', '#rfc-vendedor'];
        $(rfcFisicos.join(',')).configura({
            type: "rfc"
        });

        $('#folio-snics').configura({
            required: true,
            pattern: /^[0-9]{5}-[A-Z]{3}-[A-Z]{3}-[0-9]{6}$/
        });

        $('#nombre-productor,#papellido-productor,#sapellido-productor').configura({
            type: 'nombre-icao'
        }).configura({textTransform: 'upper'});
        ;
        $('#sapellido-productor').configura({
            required: false
        });
        $('#superficie-contrato,#volumen-contrato,#volumen-contrato-iar').configura({
            type: 'number',
            min: 0.1
        });
        $('#curp-productor').configura({
            type: 'curp'
        });
        $('#rfc-productor').configura({
            type: 'rfc-fisica'
        });

        $('.valid-field').validacion();
    };

    utils.setBaseAcordada = function (bases) {
        if (!bases) {
            return;
        }
        for (var i = 0; i < bases.length; i++) {
            var a = 'estado' + bases[i].clave.substring(13, 15);
            data.baseAcordada[a] = parseFloat(bases[i].valor);
        }
    };

    utils.setPrecioFuturo = function (precios) {
        if (!precios) {
            return;
        }
        for (var i = 0; i < precios.length; i++) {
            var a = 'estado' + precios[i].clave.substring(15, 17);
            data.precioFuturo[a] = parseFloat(precios[i].valor);
        }
    };

    utils.cargaCatalogos = function () {
        $.ajax({
            type: 'GET',
            url: '/trigo/resources/paginas/inscripcion/',
            dataType: 'json'
        }).done(function (response) {
            if (response.sucursal === undefined) {
                alert('Error: No cuentas con una sucursal para registrar.');
                return;
            }
            utils.setBaseAcordada(response.bases);
            utils.setPrecioFuturo(response.precios);
            data.ciclos = response.ciclos;
            data.cicloSeleccionado = response.cicloSeleccionado;
            data.tipos = response.tipos;
            data.tiposPersona = response.tiposPersona;
            data.estadosPredio = response.estadosPredio;
            data.tiposEmpresa = response.tiposEmpresaComprador.concat(response.tiposEmpresaVendedor);
            data.tiposPrecio = response.tiposPrecio;

            $('#cultivo').actualizaCombo(response.cultivos).prop('disabled', true);
            $('#cultivo').val($.segalmex.get(response.cultivos, 'trigo', 'clave').id);
            $('#ciclo-agricola-global').cicloAgricola({
                ciclos: response.ciclos,
                actual: response.cicloSeleccionado,
                reload: true,
                sistema: 'trigo'
            });
            $('#tipo-cultivo').actualizaCombo(response.tipos);
            $('#tipo-precio').actualizaCombo(response.tiposPrecio).prop('disabled', true);
            $('#entidad-sucursal').actualizaCombo(response.estados);
            data.estadosDestino = response.estados;
            $('#destino-grano-contrato').actualizaCombo(response.estados, {omitFirst: true});
            $('#destino-grano-contrato').selectpicker({
                deselectAllText: "Limpiar",
                selectAllText: "Seleccionar todos",
                actionsBox: true,
                title: "Seleccione entidades",
                countSelectedText: utils.cuentaEntidades,
                selectedTextFormat: 'count'
            });
            $('#entidad-contrato').actualizaCombo(response.estadosPredio);
            $('#tipo-empresa-comprador').actualizaCombo(response.tiposEmpresaComprador);
            $('#tipo-empresa-vendedor').actualizaCombo(response.tiposEmpresaVendedor);
            $('#correduria-iar').actualizaCombo(response.coberturas);
            $('#comprador-iar').actualizaCombo(response.tiposComprador);
            $('#tipo-operacion-iar').actualizaCombo(response.tiposOperacion);
            $('#destino-grano-contrato').on('changed.bs.select', utils.cambiaLabelEntidades);
            $('#tipo-cultivo').change(utils.filtraEstados);
            if (response.cicloSeleccionado) {
                $('#div-terminos').html($.segalmex.trigo.terminos.contrato.getTerminos(response.cicloSeleccionado));
            }
            utils.setDatosEmpresaSucursal(response.sucursal);
            utils.setCicloSeleccionado();
            if (response.registroCerrado) {
                alert("El registro de contratos cerró el " + response.registroCerrado.valor);
            }
        }).fail(function () {
            alert('Error: Al descargar los recursos de la página.');
        });
    };

    utils.setCicloSeleccionado = function () {
        if (!data.ciclos) {
            return;
        }
        var ciclo = data.cicloSeleccionado;
        if (ciclo) {
            $('#contrato-ciclo-text').html(ciclo.abreviatura);
            $('#condiciones-ciclo').html(ciclo.nombre);
            if (ciclo.clave === 'oi-2020') {
                $('#div-tiene-cobertura').hide();
                utils.limpiaCobertura();
            }
        }
    };

    utils.setDatosEmpresaSucursal = function (sucursal) {
        var rfc = sucursal.empresa.rfc;
        var homoclave = rfc.substring(rfc.length - 3, rfc.length);
        $('#nombre-empresa').val(sucursal.empresa.nombre);
        $('#rfc-empresa').val(rfc);
        $('#entidad-sucursal').val(sucursal.estado.id).prop('disabled', true);
        $('#municipio-sucursal').val(sucursal.municipio.nombre);
        $('#localidad-sucursal').val(sucursal.localidad);

        $('#contrato-homoclave-text').html(homoclave);
    };

    utils.configuraPantalla = function () {
        $('#tipo-cultivo').change(handlers.cambiaTipoCultivo);
        $('#entidad-contrato').change(handlers.cambiaEntidadContrato);
        $('#no-contrato').change(handlers.cambiaConsecutivoContrato);
        $('#fecha-contrato').change(handlers.cambiaFechaContrato);
        $('#limpiar-button').click(utils.limpiar);
        $('#tipo-empresa-comprador').change(handlers.tipoEmpresaComprador);
        $('#tipo-empresa-vendedor').change(handlers.tipoEmpresaVendedor);
        $('#no-acepto-condiciones').click(handlers.noAceptoCondiciones);
        $('#si-acepto-condiciones').click(handlers.aceptoCondiciones);
        $('#tiene-cobertura').change(handlers.cambiaTieneCobertura);
        $('#tipo-precio').change(handlers.cambiaTipoPrecio);
        // $('#tipo-precio').change(handlers.verificaAplicaCobertura);
        $('#precio-futuro').change(handlers.cambiaPrecioContratado);
        $('#base-acordada').change(handlers.cambiaPrecioContratado);
        $('#guardar-button').click(handlers.guardaInscripcion);
        // $('#tiene-cobertura').change(handlers.verificaAplicaCobertura);
        $('#agregar-cobertura-button').click(handlers.agregaCobertura);
        $('#cantidad-contratos-iar').change(handlers.calculaVolumenCobertura);
        $('#cobertura-table').on('click', 'button.eliminar-cobertura', handlers.eliminaCobertura);
        $('#precio-dolares').change(handlers.calculaTipoCambioPrecio);
        $('#precio-pesos').change(handlers.calculaTipoCambioPrecio);
        $('#prima-dolares').change(handlers.calculaTipoCambioPrima);
        $('#prima-pesos').change(handlers.calculaTipoCambioPrima);
        $('#comisiones-dolares').change(handlers.calculaTipoCambioComision);
        $('#comisiones-pesos').change(handlers.calculaTipoCambioComision);
        $('input.form-control-file').change(handlers.verificaTamanoArchivo);
        $('#agregar-productor-button').click(handlers.agregaProductor);
        $('#productor-detalle-table').on('click', 'a.link-detalle-contrato-productor', handlers.muestraDetalleContratoProductor);
        $('#subir-contrato-xls').click(utils.subirXlsxContratoProductor);
        $('#enviar-contrato-productor-xlsx').click(utils.enviarContratoPRoductorXlsx);
        $('#button-eliminar-productores').click(handlers.eliminaContratosProductores);
        $('#rfc-comprador').change(utils.getComprador);
        $('#rfc-vendedor').change(utils.getVendedor);
    };

    utils.generaDatos = function () {
        var inscripcion = {
            cultivo: {id: $("#cultivo").val()},
            ciclo: {id: $("#ciclo-agricola").val()},
            tipoCultivo: {id: $("#tipo-cultivo").val()},
            numeroContrato: $("#no-contrato").val(),
            fechaFirma: $.segalmex.date.fechaToIso($("#fecha-contrato").val()),
            tipoEmpresaComprador: {id: $("#tipo-empresa-comprador").val()},
            nombreComprador: $("#nombre-comprador").val(),
            rfcComprador: $("#rfc-comprador").val(),
            tipoEmpresaVendedor: {id: $("#tipo-empresa-vendedor").val()},
            nombreVendedor: $("#nombre-vendedor").val(),
            rfcVendedor: $("#rfc-vendedor").val(),
            numeroProductores: $('#numero-productores').val(),
            estadosDestino: [],
            cobertura: $("#tiene-cobertura").val() !== '0' ? $("#tiene-cobertura").val() : null,
            estado: {id: $("#entidad-contrato").val(), nombre: $("#entidad-contrato option:selected").text()},
            superficie: $("#superficie-contratada").val(),
            volumenTotal: $("#volumen-contratado").val(),
            tipoPrecio: $.segalmex.get(data.tiposPrecio, $('#tipo-precio').val()),
            precioFuturo: $("#precio-futuro").val(),
            baseAcordada: $("#base-acordada").val(),
            precioFijo: $("#precio-fijo").val(),
            coberturas: data.coberturas || [],
            claveArchivos: utils.getClavesArchivos(),
            contratosProductor: data.productores
        };

        if ($('#snics').is(":visible")) {
            inscripcion.folioSnics = $('#folio-snics').val();
        }

        var estados = $('#destino-grano-contrato').val();
        $.each(estados, function () {
            inscripcion.estadosDestino.push({estado: {id: this}});
        });
        return inscripcion;
    };

    utils.limpiar = function () {
        $('#tipo-cultivo,#tipo-empresa-comprador,#tipo-empresa-vendedor,#tipo-precio,#entidad-contrato,#cantidad-iar,#tiene-cobertura').val('0').change();
        $('#no-contrato,#fecha-contrato,#numero-productores,#nombre-comprador,#rfc-comprador,#nombre-vendedor,#rfc-vendedor,#contrato-pdf'
                + ',#curps-xlsx-tmp,#volumen-contratado,#superficie-contratada').val('');
        $('#fecha-contrato,#no-contrato').change();
        $('#folio-snics,#snics-pdf').val('');
        $('.valid-field').limpiaErrores();
        $('#destino-grano-contrato').selectpicker('deselectAll');
        data.uuid = null;
        data.coberturas = [];
        data.anexosContratoMarco = [];
        data.anexosEstadoCuenta = [];
        data.anexosCartaConfirmacion = [];
        data.anexosPoderMandato = [];
        utils.construyeCoberturas();
        data.productores = [];
        $('#productor-detalle-table tbody').html('');
        $('#detalle-div').html('');
    };

    utils.validaArchivos = function (expresion) {
        var files = $(expresion);
        var texto = [];
        var errores = false;
        for (var i = 0; i < files.length; i++) {
            if ($(files[i]).val() === '') {
                errores = true;
                texto.push(' * Seleccione el archivo para: ' + $('label[for=' + files[i].id + ']').html() + '\n');
            }
        }
        if (errores) {
            alert('Verifique lo siguiente:\n\n' + texto.join(''));
        }
        return errores;
    };

    utils.generaAnexo = function (id, index) {
        var anexo = $('#' + id).clone();
        anexo.attr('id', index + '_' + id);
        return anexo;
    };

    utils.construyeCoberturas = function () {
        var buffer = [];
        for (var i = 0; i < data.coberturas.length; i++) {
            var c = data.coberturas[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-right">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(c.numero);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(c.entidadCobertura.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(c.tipoComprador.nombre + '-' + c.nombreComprador);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push($.segalmex.date.isoToFecha(c.fechaCompra));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push($.segalmex.date.isoToFecha(c.fechaVencimiento));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(c.tipoOperacionCobertura.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(c.numeroContratos);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push('<ul>');
            buffer.push('<li>');
            buffer.push(c.precioEjercicioDolares + ' USD');
            buffer.push('</li>');
            buffer.push('<li>');
            buffer.push(c.precioEjercicioPesos + ' MXN');
            buffer.push('</li>');
            buffer.push('</ul>');
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push('<ul>');
            buffer.push('<li>');
            buffer.push(c.primaDolares + ' USD');
            buffer.push('</li>');
            buffer.push('<li>');
            buffer.push(c.primaPesos + ' MXN');
            buffer.push('</li>');
            buffer.push('</ul>');
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push('<ul>');
            buffer.push('<li>');
            buffer.push(c.comisionesDolares + ' USD');
            buffer.push('</li>');
            buffer.push('<li>');
            buffer.push(c.comisionesPesos + ' MXN');
            buffer.push('</li>');
            buffer.push('</ul>');
            buffer.push('</td>');
            buffer.push('<td class="text-center"><button id="eliminar-cobertura-');
            buffer.push(i);
            buffer.push('" class="btn btn-danger btn-sm eliminar-cobertura"><i class="fa fa-minus-circle"></i></button></td>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }

        $('#cobertura-table tbody').html(buffer.join(''));
    };

    handlers.eliminaCobertura = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-cobertura-'.length), 10);
        var coberturas = [];
        var anexosContratoMarco = [];
        var anexosEstadoCuenta = [];
        var anexosCartaConfirmacion = [];
        var anexosPoderMandato = [];

        for (var i = 0; i < data.coberturas.length; i++) {
            if (i !== idx) {
                coberturas.push(data.coberturas[i]);
                anexosContratoMarco.push(data.anexosContratoMarco[i]);
                anexosEstadoCuenta.push(data.anexosEstadoCuenta[i]);
                anexosCartaConfirmacion.push(data.anexosCartaConfirmacion[i]);
                anexosPoderMandato.push(data.anexosPoderMandato[i]);
            }
        }
        data.coberturas = coberturas;
        data.anexosContratoMarco = anexosContratoMarco;
        data.anexosEstadoCuenta = anexosEstadoCuenta;
        data.anexosCartaConfirmacion = anexosCartaConfirmacion;
        data.anexosPoderMandato = anexosPoderMandato;
        utils.construyeCoberturas();
    };

    utils.getClavesArchivos = function () {
        var archivos = utils.getArchivos();
        var claves = [];
        for (var i = 0; i < archivos.length; i++) {
            var file = archivos[i];
            var id = file.id;
            var tipo = id.substring(0, id.length - '-pdf'.length);
            claves.push(tipo);
        }
        return claves.join(',');
    };

    utils.getArchivos = function () {
        $('#contrato-marco-iar-pdf,#estado-cuenta-iar-pdf,#carta-confirmacion-iar-pdf,#poder-mandato-iar-pdf,#curps-xlsx-tmp').prop('disabled', true);
        var files = $('input.form-control-file:enabled');
        $('#contrato-marco-iar-pdf,#estado-cuenta-iar-pdf,#carta-confirmacion-iar-pdf,#poder-mandato-iar-pdf,#curps-xlsx-tmp').prop('disabled', false);

        var fds = [];
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            var id = file.id;
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/trigo/resources/contratos/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            var archivo = $('#' + id)[0].files[0];
            fd.append('file', archivo);
            var etiqueta = $('label[for=' + file.id + ']').html();
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }

        for (i = 0; i < data.anexosContratoMarco.length; i++) {
            var anexo = data.anexosContratoMarco[i];
            var id = anexo.attr('id');
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/trigo/resources/contratos/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            var etiqueta = 'Documentación contrato marco (PDF) [' + (i + 1) + ']';
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }

        for (i = 0; i < data.anexosEstadoCuenta.length; i++) {
            var anexo = data.anexosEstadoCuenta[i];
            var id = anexo.attr('id');
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/trigo/resources/contratos/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            var etiqueta = 'Documentación estado de cuenta (PDF) [' + (i + 1) + ']';
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }

        for (i = 0; i < data.anexosCartaConfirmacion.length; i++) {
            var anexo = data.anexosCartaConfirmacion[i];
            var id = anexo.attr('id');
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/trigo/resources/contratos/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            var etiqueta = 'Documentación carta confirmación compra (PDF) [' + (i + 1) + ']';
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }

        for (i = 0; i < data.anexosPoderMandato.length; i++) {
            var anexo = data.anexosPoderMandato[i];
            var id = anexo.attr('id');
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/trigo/resources/contratos/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            var etiqueta = 'Documentación poder o mandato (PDF) [' + (i + 1) + ']';
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }

        return fds;
    };
    utils.cuentaEntidades = function () {
        var entidades = $('#destino-grano-contrato').val();
        return entidades.length + ' entidades seleccionadas';
    };

    utils.cambiaLabelEntidades = function () {
        var entidades = $('#destino-grano-contrato').val();
        if (entidades.length > 1) {
            var labelDestinos = '';
            $.each(entidades, function () {
                var par = this;
                for (var i = 0; i < data.estadosDestino.length; i++) {
                    var dpar = data.estadosDestino[i];
                    if (parseInt(dpar.id) === parseInt(par)) {
                        labelDestinos = labelDestinos === '' ? dpar.nombre : labelDestinos + ', ' + dpar.nombre;
                    }
                }
            });
            $('#destinos-label').html(labelDestinos);
        } else {
            $('#destinos-label').html('');
        }
    };

    utils.verificaSemilla = function () {
        var tipo = parseInt($('#tipo-cultivo').val());
        $('#folio-snics,#snics-pdf').val('');
        $('.valid-field').limpiaErrores();
        for (var i = 0; i < data.tipos.length; i++) {
            var a = data.tipos[i];
            if (a.id === tipo) {
                if (a.clave === 'trigo-semilla') {
                    $('#snics-pdf').prop('disabled', false);
                    $('#snics').show();
                    return;
                }
            }
        }
        $('#snics-pdf').prop('disabled', true);
        $('#snics').hide();
    };

    handlers.agregaProductor = function (e) {
        e.preventDefault();
        $('#form-productor-nuevo .valid-field').limpiaErrores();
        var errores = [];
        $('#form-productor-nuevo .valid-field').valida(errores, true);
        if (errores.length > 0) {
            return;
        }
        if (parseFloat($('#volumen-contrato-iar').val()) > parseFloat($('#volumen-contrato').val())) {
            alert('El volumen contratado en el CCV no puede ser menor que el volumen cubierto por el I.A.R.');
            return;
        }

        var productor = {
            nombre: $('#nombre-productor').val(),
            primerApellido: $('#papellido-productor').val(),
            segundoApellido: $('#sapellido-productor').val(),
            curp: $('#curp-productor').val(),
            rfc: $('#rfc-productor').val(),
            superficie: $('#superficie-contrato').val(),
            volumenContrato: $('#volumen-contrato').val(),
            volumenIar: $('#volumen-contrato-iar').val()
        };

        if (utils.validaProductor(productor)) {
            return;
        }
        data.productores.push(productor);
        utils.agregaDetalleContratoProductor();
        utils.limpiaContratoProductor();
        $('#detalle-div').html(utils.generaTablaDetalleContratosProductor);
        utils.configuraTablaResultados();
        $('#detalle-productores-table').on('click', 'button.eliminar-productor', handlers.eliminaProductor);
    };

    utils.validaProductor = function (productor) {
        if (data.productores.length > 0) {
            for (var i = 0; i < data.productores.length; i++) {
                if (productor.curp === data.productores[i].curp) {
                    alert('El productor con CURP ' + productor.curp + ' ya fue agregado.');
                    return  true;
                }
                if (productor.rfc === data.productores[i].rfc) {
                    alert('El productor con RFC ' + productor.rfc + ' ya fue agregado.');
                    return  true;
                }
            }
        }
        return false;
    };

    handlers.muestraDetalleContratoProductor = function (e) {
        e.preventDefault();
        $('#detalle-contrato-productor-modal').modal('show').focus();
    };

    utils.configuraTablaResultados = function () {
        var aoColumns = [{"bSortable": false}, {"bSortable": true},
            {"bSortable": true}, {"bSortable": true}, {"bSortable": false}, {"bSortable": false},
            {"bSortable": false}, {"bSortable": false}, {"bSortable": false}];
        utils.configuraTablaContratosProductor('detalle-productores-table', aoColumns);
    };

    utils.configuraTablaContratosProductor = function (tablaId, aoColumns) {
        var table = $('#' + tablaId).DataTable({
            "oLanguage": {
                "sEmptyTable": "No se encontró información para mostrar en la tabla",
                "sSearch": "Buscar ",
                "sZeroRecords": "No se encontraron resultados."
            },
            "bFilter": true,
            "bInfo": false,
            "bPaginate": false,
            "aoColumns": aoColumns,
            "aoColumnDefs": [{"bSortable": false, "aTargets": ["no-sort"]}]
        });
        table.on('order.dt search.dt', function () {
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).order([[1, 'asc']]).draw(false);
        $("#" + tablaId + "_filter input").addClass('input-buscar');
        $(".dataTables_empty").attr("colspan", "100%");
    };

    handlers.eliminaProductor = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-productor-'.length), 10);
        var productores = [];
        for (var i = 0; i < data.productores.length; i++) {
            if (i !== idx) {
                productores.push(data.productores[i]);
            }
        }
        data.productores = productores;
        if (data.productores.length === 0) {
            $('#detalle-contrato-productor-modal').modal('hide');
            $('#productor-detalle-table tbody').html('');
        } else {
            $('#detalle-div').html(utils.generaTablaDetalleContratosProductor);
            utils.configuraTablaResultados();
            utils.agregaDetalleContratoProductor();
            $('#detalle-productores-table').on('click', 'button.eliminar-productor', handlers.eliminaProductor);
        }
    };

    utils.generaTablaDetalleContratosProductor = function () {
        var buffer = [];
        buffer.push('<div class="table-responsive">');
        buffer.push('<table id="detalle-productores-table" class="table table-bordered table-hover table-striped" style="width:100%">');
        buffer.push('<thead class="thead-dark"><tr>');
        buffer.push('<th>#</th>');
        buffer.push('<th>Productor</th>');
        buffer.push('<th>CURP</th>');
        buffer.push('<th>RFC</th>');
        buffer.push('<th>Superficie (ha)</th>');
        buffer.push('<th>Volumen CCV (t)</th>');
        buffer.push('<th>Volumen IAR (t)</th>');
        buffer.push('<th><i class="fa fa-minus-circle"></i></th>');
        buffer.push('<th>Correcto</th>');
        buffer.push('</tr></thead>');
        buffer.push('<tbody>');
        for (var i = 0; i < data.productores.length; i++) {
            var p = data.productores[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-right">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(utils.marcaRegistroIncorrecto(utils.getNombreCompleto(p), p.correcto));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(utils.marcaRegistroIncorrecto(p.curp, p.correcto));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(utils.marcaRegistroIncorrecto(p.rfc, p.correcto));
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(utils.marcaRegistroIncorrecto($.segalmex.util.format(p.superficie, 3), p.correcto));
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(utils.marcaRegistroIncorrecto($.segalmex.util.format(p.volumenContrato, 3), p.correcto));
            buffer.push('</td>');
            buffer.push('<td class="text-right">');
            buffer.push(utils.marcaRegistroIncorrecto($.segalmex.util.format(p.volumenIar, 3), p.correcto));
            buffer.push('</td>');
            buffer.push('<td class="text-center"><button id="eliminar-productor-');
            buffer.push(i);
            buffer.push('" class="btn btn-danger btn-sm eliminar-productor"><i class="fa fa-minus-circle"></i></button></td>');
            buffer.push('</td>');
            buffer.push('<td class="text-center">');
            buffer.push(p.correcto !== undefined && !p.correcto ? '<i title="' + p.error + '" class="fas fa-times-circle text-danger"></i>' : '<i class="fas fa-check-circle text-success"></i>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        buffer.push('</tbody>');
        buffer.push('</table>');
        buffer.push('</div>');
        buffer.push('<br/>');
        return buffer.join('');
    };

    utils.marcaRegistroIncorrecto = function (valor, correcto) {
        if (correcto !== undefined && !correcto) {
            return '<b><p style="color:Red;">' + valor + '</p></b>';
        } else {
            return valor;
        }
    };

    utils.agregaDetalleContratoProductor = function () {
        var buffer = [];
        var totalSuperficie = 0.0;
        var totalVolumenContrato = 0.0;
        var totalVolumenIar = 0.0;
        var totalProductores = 0;
        for (var i = 0; i < data.productores.length; i++) {
            totalProductores++;
            totalSuperficie += parseFloat(data.productores[i].superficie);
            totalVolumenContrato += parseFloat(data.productores[i].volumenContrato);
            totalVolumenIar += parseFloat(data.productores[i].volumenIar);
        }
        buffer.push('<tr>');
        buffer.push('<td><a class="link-detalle-contrato-productor" href="#">');
        buffer.push(totalProductores);
        buffer.push('</a></td>');
        buffer.push('<td><a class="link-detalle-contrato-productor" href="#">');
        buffer.push(totalSuperficie.toFixed(3));
        buffer.push('</a></td>');
        buffer.push('<td><a class="link-detalle-contrato-productor" href="#">');
        buffer.push(totalVolumenContrato.toFixed(3));
        buffer.push('</a></td>');
        buffer.push('<td><a class="link-detalle-contrato-productor" href="#">');
        buffer.push(totalVolumenIar.toFixed(3));
        buffer.push('</a></td>');
        buffer.push('</tr>');
        $('#productor-detalle-table tbody').html(buffer.join(''));
    };

    utils.limpiaContratoProductor = function () {
        $('#form-productor-nuevo input.valid-field').val('').limpiaErrores();
    };

    utils.subirXlsxContratoProductor = function () {
        $('#curps-xlsx-tmp').val('');
        $('#contrato-productor-modal').modal('show');
    };

    utils.enviarContratoPRoductorXlsx = function () {
        $('#enviar-contrato-productor-xlsx').prop('disabled', true);
        if ($('#curps-xlsx-tmp').val() === '') {
            alert('El archivo CURPs de productores (XLSX) es obligatorio.');
            $('#enviar-contrato-productor-xlsx').prop('disabled', false);
            return;
        }
        var fd = new FormData();
        var archivo = $('#curps-xlsx-tmp')[0].files[0];
        fd.append('file', archivo);
        $.ajax({
            url: '/trigo/resources/contratos/maiz/inscripcion/productor/curp-xlsx',
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false
        }).done(function (response) {
            data.productores = data.productores.concat(response);
            $('#contrato-productor-modal').modal('hide');
            utils.agregaDetalleContratoProductor();
            $('#detalle-div').html(utils.generaTablaDetalleContratosProductor);
            utils.configuraTablaResultados();
            $('#detalle-productores-table').on('click', 'button.eliminar-productor', handlers.eliminaProductor);
            $('#enviar-contrato-productor-xlsx').prop('disabled', false);
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Error, no fue posible enviar el archivo.');
            }
            $('#enviar-contrato-productor-xlsx').prop('disabled', false);
        });
    };

    handlers.eliminaContratosProductores = function (e) {
        e.preventDefault();
        if (!confirm("¿Está seguro de eliminar todos los productores agregados?")) {
            return;
        }
        data.productores = [];
        $('#detalle-div').html('');
        $('#productor-detalle-table tbody').html('');
        $('#contrato-productor-modal').modal('hide');
    };

    utils.getNombreCompleto = function (p) {
        var nombre = p.nombre;
        var pApellido = p.primerApellido ? p.primerApellido : '';
        ;
        var sApellido = p.segundoApellido ? p.segundoApellido : '';
        return nombre + ' ' + pApellido + ' ' + sApellido;
    };

    utils.filtraEstados = function () {
        if (data.cicloSeleccionado.clave === 'oi-2022' && $('#tipo-cultivo').val() !== '0') {
            $('#entidad-contrato').val('0').change().limpiaErrores();
            var v = parseInt($('#tipo-cultivo').val());
            var tc;
            var estados = [];
            for (var i = 0; i < data.tipos.length; i++) {
                if (v === data.tipos[i].id) {
                    tc = data.tipos[i];
                }
            }
            switch (tc.clave) {
                case 'trigo-cristalino':
                    for (var i = 0; i < data.estadosPredio.length; i++) {
                        if (data.estadosPredio[i].clave === data.claveBC || data.estadosPredio[i].clave === data.claveSonora) {
                            estados.push(data.estadosPredio[i]);
                        }
                    }
                    $('#entidad-contrato').actualizaCombo(estados);
                    break;
                default :
                    $('#entidad-contrato').actualizaCombo(data.estadosPredio);
            }
        }
    };

    utils.getComprador = function () {
        utils.getEmpresa($('#rfc-comprador').val(), "nombre-comprador");
    };

    utils.getVendedor = function () {
        utils.getEmpresa($('#rfc-vendedor').val(), "nombre-vendedor");
    };

    utils.getEmpresa = function (rfc, target) {
        if (rfc === '') {
            return;
        }
        $.ajax({
            type: 'GET',
            url: '/trigo/resources/empresas/administracion-empresa/rfc/' + rfc,
            dataType: 'json'
        }).done(function (response) {
            $("#" + target).val(response.nombre.toUpperCase());
        }).fail(function () {
            $("#" + target).val('');
            // no hay empresa con el rfc especificado
        });
    };
})(jQuery);