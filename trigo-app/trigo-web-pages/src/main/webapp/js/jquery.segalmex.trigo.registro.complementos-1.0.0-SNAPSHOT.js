/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.trigo.registro.complementos');
    var data = {};
    var utils = {};
    var handlers = {};

    $.segalmex.trigo.registro.complementos.init = function () {
        utils.inicializaValidaciones();
        $('#button-buscar').click(handlers.busca);
        $('#button-limpiar').click(handlers.limpia);
        $('#button-regresar-resultados').click(handlers.regresaResultados);
        $('#button-complemento').click(handlers.solicitaComplemento);
    };

    handlers.solicitaComplemento = function (e) {
        e.preventDefault();
        $('#button-complemento').prop('disabled', true);
        $.ajax({
            url: '/trigo/resources/contratos/maiz/inscripcion/' + data.uuid + '/complemento/',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('Se ha enviado la solicitud de complemento.');
            $('#button-complemento').prop('disabled', false);
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert("Ocurrió un error al enviar complemento.");
            }
            $('#button-complemento').prop('disabled', false);
        });
    };

    handlers.busca = function (e) {
        e.preventDefault();
        $('#button-buscar').prop('disabled', true).html('Buscando...');
        $('#resultados-busqueda').hide();
        var errores = [];
        $('.valid-field').valida(errores, false);
        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            $('#button-buscar').prop('disabled', false).html('Buscar');
            return;
        }

        if ($('#no-contrato').val() === '' && $('#folio').val() === '') {
            utils.desHabilitaCtrls(false);
            alert('Ingrese un folio o número de contrato.');
            $('#button-buscar').prop('disabled', false).html('Buscar');
            return;
        }

        var datos = {
            folio: $('#folio').val(),
            numeroContrato: $('#no-contrato').val()
        };

        for (var prop in datos) {
            if (datos[prop] === '') {
                delete datos[prop];
            }
        }

        $('#cargando-resultados').show();
        utils.buscaRegistros(datos);
        $('#cargando-resultados,#div-regresar,#resultados-busqueda-agrupados').hide();
        $('#button-buscar').prop('disabled', false).html('Buscar');
    };

    utils.buscaRegistros = function (datos) {
        $.ajax({
            url: '/trigo/resources/contratos/maiz/inscripcion/',
            type: 'GET',
            data: datos,
            dataType: 'json'
        }).done(function (response) {
            var renglones = '';
            if (response.length === 0) {
                renglones = '<tr><td colspan="10" class="text-center">Sin resultados</td></tr>';
                $('#data').html(renglones);
            } else {
                $('#data').html(utils.creaDatatable(response));
                utils.configuraTablaResultados();
                $('#table-resultados-busqueda').on('click', 'a.link-uuid', handlers.muestraDetalle);
            }
            $('#resultados-busqueda').slideDown();
        }).fail(function () {
            $('#cargando-resultados').hide();
            alert('Error: No fue posible realizar la consulta.');
        });
    };

    handlers.muestraDetalle = function (e) {
        e.preventDefault();
        var uuid = e.target.id.split('.')[1];
        data.uuid = uuid;
        $.ajax({
            url: '/trigo/resources/contratos/maiz/inscripcion/' + uuid,
            data: {archivos: true, datos: true, historial: true},
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            // Mostramos el detalle
            $('#detalle-inscripcion-contrato').html($.segalmex.trigo.inscripcion.vista.construyeInscripcion(response));
            $('#estatus-inscripcion-contrato').html($.segalmex.trigo.inscripcion.vista.construyeEstatus(response));
            $('#historial-inscripcion-contrato').html($.segalmex.trigo.inscripcion.vista.construyeHistorial(response.historial));
            $('#detalle-elemento').show();
            $('#div-busqueda,#cargando-resultados,#resultados-busqueda').hide();
            $('#button-complemento').prop('disabled', true);
            if (response.estatus.clave === 'positiva') {
                $('#button-complemento').prop('disabled', false);
            }
        }).fail(function () {
            alert('Error: No fue posible obtener el registro.');
        });
    };

    handlers.limpia = function (e) {
        utils.limpiar();
    };

    handlers.regresaResultados = function (e) {
        e.preventDefault();
        $('#detalle-elemento').hide();
        $('#div-busqueda,#resultados-busqueda').show();

    };

    utils.construyeRenglones = function (resultados) {
        var buffer = [];
        for (var i = 0; i < resultados.length; i++) {
            var resultado = resultados[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td><a class="link-uuid" href="#" id="uuid.' + resultado.uuid + '">');
            buffer.push(resultado.folio);
            buffer.push('</a></td>');
            buffer.push('<td>');
            buffer.push($.segalmex.date.isoToFecha(resultado.fechaCreacion));
            buffer.push('</td>');
            buffer.push('<td>');
            if (resultado.contrato) {
                buffer.push('<a href="/trigo/resources/archivos/maiz/' + resultado.contrato.uuid + '/'
                        + resultado.contrato.nombre + '" target="_blank">');
                buffer.push(resultado.numeroContrato);
                buffer.push('</a>');
            } else {
                buffer.push(resultado.numeroContrato);
            }
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.sucursal.empresa.rfc);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.sucursal.empresa.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.estado.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.volumenTotal);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.tipoCultivo.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.estatus.nombre);
            buffer.push('</td>');
            buffer.push('</tr>');
        }

        return buffer.join('\n');
    };

    utils.inicializaValidaciones = function () {
        $(".valid-field").configura();

        $('#no-contrato').configura({
            pattern: /^[A-Z]{3}-[A-Z]{2}[0-9]{2}-[A-Z0-9]{3}-\d{6}-(P|C|S|D|E)-\d{3}$/,
            minlength: 25,
            maxlength: 25,
            required: false
        });

        $('#folio').configura({
            pattern: /^\d{1,6}$/,
            minlength: 1,
            maxlength: 6,
            required: false
        });

        $('.valid-field').validacion();
    };

    utils.limpiar = function () {
        $('#div-busqueda input').val('');
        $('.valid-field').limpiaErrores();
        $('#resultados-busqueda,#cargando-resultados').hide();
        $('#table-resultados-busqueda tbody').html('<tr><td colspan="6" class="text-center">Sin resultados</td></tr>');
        $('#detalle-inscripcion-contrato,#historial-inscripcion-contrato,#datos-capturados-inscripcion-contrato').html('');
    };

    utils.creaDatatable = function (resultados) {
        var buffer = [];
        buffer.push('<table id="table-resultados-busqueda"');
        buffer.push(' class="table table-striped" width="100%">');
        buffer.push('<thead class = "table-success">');
        buffer.push('<tr class="success">');
        buffer.push('<th>No.</th>');
        buffer.push('<th>Folio</th>');
        buffer.push('<th>Fecha de registro</th>');
        buffer.push('<th>No. de contrato</th>');
        buffer.push('<th>RFC de la empresa</th>');
        buffer.push('<th>Empresa</th>');
        buffer.push('<th>Estado</th>');
        buffer.push('<th>Toneladas</th>');
        buffer.push('<th>Tipo</th>');
        buffer.push('<th>Estatus</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');
        for (var i = 0; i < resultados.length; i++) {
            var resultado = resultados[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push('</td>');
            buffer.push('<td><a class="link-uuid" href="#" id="uuid.' + resultado.uuid + '">');
            buffer.push(resultado.folio);
            buffer.push('</a></td>');
            buffer.push('<td>');
            buffer.push($.segalmex.date.isoToFecha(resultado.fechaCreacion));
            buffer.push('</td>');
            buffer.push('<td>');
            if (resultado.contrato) {
                buffer.push('<a href="/trigo/resources/archivos/trigo/' + resultado.contrato.uuid + '/'
                        + resultado.contrato.nombre + '" target="_blank">');
                buffer.push(resultado.numeroContrato);
                buffer.push('</a>');
            } else {
                buffer.push(resultado.numeroContrato);
            }
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.sucursal.empresa.rfc);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.sucursal.empresa.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.estado.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.volumenTotal);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.tipoCultivo.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(resultado.estatus.nombre);
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        buffer.push('</tbody>');
        buffer.push('</table>');

        buffer.push('<br/>');
        return buffer.join('');

    };

    utils.configuraTablaResultados = function () {
        var aoColumns = [{"bSortable": false}, {"bSortable": true}, {"bSortable": false},
            {"bSortable": true}, {"bSortable": true}, {"bSortable": true}, {"bSortable": true},
            {"bSortable": true}, {"bSortable": true}, {"bSortable": true}];
        utils.configuraTabla('table-resultados-busqueda', aoColumns);
    };

    utils.configuraTabla = function (tablaId, aoColumns) {
        var table = $('#' + tablaId).DataTable({
            "oLanguage": {
                "sEmptyTable": "No se encontró información para mostrar en la tabla",
                "sSearch": "Buscar ",
                "sZeroRecords": "No se encontraron resultados."
            },
            "bFilter": true,
            "bInfo": false,
            "bPaginate": false,
            "aoColumns": aoColumns,
            "aoColumnDefs": [{"bSortable": false, "aTargets": ["no-sort"]}]
        });
        table.on('order.dt search.dt', function () {
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).order([[1, 'asc']]).draw(false);
        $("#" + tablaId + "_filter input").addClass('input-buscar');
        $(".dataTables_empty").attr("colspan", "100%");
    };
})(jQuery);