(function ($) {
    $.segalmex.namespace('segalmex.trigo.inscripcion.vista');
    var utils = {};

    $.segalmex.trigo.inscripcion.vista.construyeInscripcion = function (inscripcion, correccion) {
        var buffer = [];
        buffer.push('<div class="card mb-4 shadow-sm">');
        buffer.push('<h4 class="card-header">Detalle del Registro de Contrato</h4>');

        buffer.push('<div class="card-body">'); // card-body
        buffer.push('<h3 class="card-title">Folio: ');
        buffer.push(inscripcion.folio);
        buffer.push('</h3>');
        buffer.push('<h4>Programa de Precios de Garantía a Productos Alimentarios Básicos</h4>');

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Cultivo:</strong><br/>');
        buffer.push(inscripcion.cultivo.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Ciclo agrícola:</strong><br/>');
        buffer.push(inscripcion.ciclo.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Tipo:</strong><br/>');
        buffer.push(inscripcion.tipoCultivo.nombre);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<h4>Datos de la empresa</h4>');
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-8">');
        buffer.push('<strong>Nombre:</strong><br/> ');
        buffer.push(inscripcion.sucursal.empresa.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>RFC:</strong><br/> ');
        buffer.push(inscripcion.sucursal.empresa.rfc);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Entidad:</strong><br/> ');
        buffer.push(inscripcion.sucursal.estado.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Municipio:</strong><br/> ');
        buffer.push(inscripcion.sucursal.municipio.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Localidad:</strong><br/> ');
        buffer.push(inscripcion.sucursal.localidad);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<h4>Acerca del Contrato de Compra - Venta (CCV)</h4>');
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>No. de contrato:</strong><br/>');
        buffer.push(inscripcion.numeroContrato);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Fecha de firma:</strong><br/>');
        buffer.push($.segalmex.date.isoToFecha(inscripcion.fechaFirma));
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>No. de productores:</strong><br/>');
        buffer.push(inscripcion.numeroProductores);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Tipo IAR:</strong><br/>');
        buffer.push(inscripcion.coberturas && inscripcion.coberturas.length > 0 ? 'Cobertura' : 'Contrato a precio fijo');
        buffer.push('</div>');
        buffer.push('</div>'); // end-row

        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-12">');
        buffer.push('<strong>Entidad de destino del grano:</strong><br/>');
        buffer.push(inscripcion.estadosDestinoTexto);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<h5>Comprador del trigo</h5>');
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Tipo de empresa:</strong><br/>');
        buffer.push(inscripcion.tipoEmpresaComprador.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Nombre:</strong><br/>');
        buffer.push(inscripcion.nombreComprador);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>RFC:</strong><br/>');
        buffer.push(inscripcion.rfcComprador);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');

        buffer.push('<h5>Vendedor del trigo</h5>');
        buffer.push('<div class="row">'); // row
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Tipo de empresa:</strong><br/>');
        buffer.push(inscripcion.tipoEmpresaVendedor.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Nombre:</strong><br/>');
        buffer.push(inscripcion.nombreVendedor);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>RFC:</strong><br/>');
        buffer.push(inscripcion.rfcVendedor);
        buffer.push('</div>');
        buffer.push('</div>'); // end-row
        buffer.push('<br/>');
        if (inscripcion.folioSnics) {
            buffer.push('<h4>Semilla cerificada SNICS</h4>');
            buffer.push('<div class="row">'); // row
            buffer.push('<div class="col-md-4">');
            buffer.push('<strong>No. de folio de registro en el DPOCS:</strong><br/>');
            buffer.push(inscripcion.folioSnics);
            buffer.push('</div>');
            buffer.push('</div>'); // end-row
            buffer.push('<br/>');
        }
        buffer.push('<h4>Información de entidad y precio contratado</h4>');
        buffer.push('<table class="table table-bordered table-striped">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<th>Entidad</th>');
        buffer.push('<th>Superficie</th>');
        buffer.push('<th>Volumen</th>');
        buffer.push('<th>Tipo precio</th>');
        buffer.push('<th>PFT</th>');
        buffer.push('<th>B</th>');
        buffer.push('<th>PFT + B</th>');
        buffer.push('</thead>');
        buffer.push('<tbody>');
        buffer.push($.segalmex.trigo.inscripcion.vista.construyeTablaPredios(inscripcion));
        buffer.push('<tbody>');
        buffer.push('</table>');
        if (inscripcion.coberturas) {
            buffer.push('<h4>Coberturas</h4>');
            buffer.push($.segalmex.trigo.inscripcion.vista.construyeTablaCobertura(inscripcion.coberturas));
            buffer.push('<br/>');
        }
        //sección de contratoProductor
        if (correccion && inscripcion.contratosProductor.length > 0) {
            var totales = utils.agregaDetalleContratoProductor(inscripcion.contratosProductor);
            buffer.push('<h4>Datos de los productores que participan en el contrato</h4>');
            buffer.push('<div class="row">'); // row
            buffer.push('<div class="col-md-4">');
            buffer.push('<strong>Productores:</strong><br/>');
            buffer.push(totales.totalProductores);
            buffer.push('</div>');
            buffer.push('<div class="col-md-4">');
            buffer.push('<strong>Total superficie (ha):</strong><br/>');
            buffer.push(totales.totalSuperficie.toFixed(3));
            buffer.push('</div>');
            buffer.push('<div class="col-md-4">');
            buffer.push('<strong>Total volumen CCV:</strong><br/>');
            buffer.push(totales.totalVolumenContrato.toFixed(3));
            buffer.push('</div>');
            buffer.push('</div>'); // end-row
            buffer.push('<div class="row">'); // row
            buffer.push('<div class="col-md-4">');
            buffer.push('<strong>Total volumen IAR:</strong><br/>');
            buffer.push(totales.totalVolumenIar.toFixed(3));
            buffer.push('</div>');
            buffer.push('</div>');
            buffer.push('<div class="text-right">');
            buffer.push('<button type="button" id="button-ver-productores" class="btn btn-outline-secondary">Ver productores</button>');
            buffer.push('</div>');
            buffer.push('<br/>');
        }
        if (inscripcion.archivos) {
            buffer.push('<h4>Documentación anexa</h4>');
            buffer.push('<ul>');
            for (var i = 0; i < inscripcion.archivos.length; i++) {
                var a = inscripcion.archivos[i];
                buffer.push('<li><a href="/trigo/resources/archivos/trigo/' + a.uuid + '/'
                        + a.nombre + '" target="_blank">');
                buffer.push(a.nombreOriginal + ' [' + a.tipo + '] ');
                buffer.push(' <i class="fas fa-file"></i></a></li>');
            }
            buffer.push('</ul>');
        }
        buffer.push('<div class="text-right">');
        buffer.push('<a role="button" class="btn btn-outline-success" href="/trigo/resources/contratos/maiz/inscripcion/'
                + inscripcion.uuid + '/inscripcion-contrato.pdf" target="_blank"><i class="fas fa-file-pdf"></i> Descargar registro de contrato</a>');
        buffer.push('</div>');
        buffer.push('</div>'); // end-card-body
        buffer.push('</div>'); // end-card

        return buffer.join('\n');
    };
    $.segalmex.trigo.inscripcion.vista.construyeTablaCobertura = function (cobertura) {
        var buffer = [];
        buffer.push('<div class="table-responsive">');
        buffer.push('<table id="cobertura-table" class="table table-bordered table-hover table-striped">');
        buffer.push('<thead class="thead-dark">');
        buffer.push('<tr>');
        buffer.push('<th>#</th>');
        buffer.push('<th>No.</th>');
        buffer.push('<th>Correduría u operador</th>');
        buffer.push('<th>Comprador</th>');
        buffer.push('<th>Fecha compra</th>');
        buffer.push('<th>Fecha vencimiento</th>');
        buffer.push('<th>Tipo de operación</th>');
        buffer.push('<th>Número contratos</th>');
        buffer.push('<th>Precio ejercicio</th>');
        buffer.push('<th>Prima</th>');
        buffer.push('<th>Comisiones</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');
        for (var i = 0; i < cobertura.length; i++) {
            var c = cobertura[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-right">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(c.numero);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(c.entidadCobertura.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(c.tipoComprador.nombre + '-' + c.nombreComprador);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push($.segalmex.date.isoToFecha(c.fechaCompra));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push($.segalmex.date.isoToFecha(c.fechaVencimiento));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(c.tipoOperacionCobertura.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(c.numeroContratos);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push('<ul>');
            buffer.push('<li type="circle">');
            buffer.push(c.precioEjercicioDolares + ' USD');
            buffer.push('</li>');
            buffer.push('<li type="circle">');
            buffer.push(c.precioEjercicioPesos + ' MXN');
            buffer.push('</li>');
            buffer.push('</ul>');
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push('<ul>');
            buffer.push('<li type="circle">');
            buffer.push(c.primaDolares + ' USD');
            buffer.push('</li>');
            buffer.push('<li type="circle">');
            buffer.push(c.primaPesos + ' MXN');
            buffer.push('</li>');
            buffer.push('</ul>');
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push('<ul>');
            buffer.push('<li type="circle">');
            buffer.push(c.comisionesDolares + ' USD');
            buffer.push('</li>');
            buffer.push('<li type="circle">');
            buffer.push(c.comisionesPesos + ' MXN');
            buffer.push('</li>');
            buffer.push('</ul>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        buffer.push('</tbody>');
        buffer.push('</table>');
        buffer.push('</div>');
        return buffer.join('');
    };

    $.segalmex.trigo.inscripcion.vista.construyeTablaPredios = function (inscripcion, params) {
        params = params || {};
        var buffer = [];
        buffer.push('<tr>');
        if (params.incluirIndex) {
            buffer.push('<td>');
            buffer.push(1);
            buffer.push('</td>');
        }
        buffer.push('<td>');
        buffer.push(inscripcion.estado.nombre);
        buffer.push('</td>');
        buffer.push('<td class="text-right">');
        buffer.push(inscripcion.superficie);
        buffer.push('</td>');
        buffer.push('<td class="text-right">');
        buffer.push(inscripcion.volumenTotal);
        buffer.push('</td>');
        buffer.push('<td>');
        buffer.push(inscripcion.tipoPrecio.nombre);
        buffer.push('</td>');
        buffer.push('<td class="text-right">');
        buffer.push(inscripcion.precioFuturo !== undefined ? inscripcion.precioFuturo : '--');
        buffer.push('</td>');
        buffer.push('<td class="text-right">');
        buffer.push(inscripcion.baseAcordada !== undefined ? inscripcion.baseAcordada : '--');
        buffer.push('</td>');
        buffer.push('<td class="text-right">');
        buffer.push(inscripcion.precioFijo !== undefined ? inscripcion.precioFijo : '--');
        buffer.push('</td>');
        if (params.incluirEliminar) {
            buffer.push('<td class="text-center"><button  id="eliminar-predio-');
            buffer.push(inscripcion.estado.id);
            buffer.push('" class="btn btn-danger btn-sm eliminarPredio"><i class="fa fa-minus-circle"></i></button></td>');
            buffer.push('</td>');
        }
        buffer.push('</tr>');

        return buffer.join('');
    };

    utils.agregaDetalleContratoProductor = function (productores) {
        var datos = {
            totalSuperficie: 0.0,
            totalVolumenContrato: 0.0,
            totalVolumenIar: 0.0,
            totalProductores: 0
        };
        for (var i = 0; i < productores.length; i++) {
            datos.totalProductores++;
            datos.totalSuperficie += parseFloat(productores[i].superficie);
            datos.totalVolumenContrato += parseFloat(productores[i].volumenContrato);
            datos.totalVolumenIar += parseFloat(productores[i].volumenIar);
        }
        return datos;
    };

    $.segalmex.trigo.inscripcion.vista.generaTablaDetalleContratosProductor = function (contratosProductor, correccion) {
        if (contratosProductor.length > 0) {
            var buffer = [];
            buffer.push('<div class="table-responsive">');
            buffer.push('<table id="detalle-productores-table" class="table table-bordered table-hover table-striped" style="width:100%;">');
            buffer.push('<thead class="thead-dark"><tr>');
            buffer.push('<th>#</th>');
            buffer.push('<th>Productor</th>');
            buffer.push('<th>CURP</th>');
            buffer.push('<th>RFC</th>');
            buffer.push('<th>Superficie (ha)</th>');
            buffer.push('<th>Volumen CCV (t)</th>');
            buffer.push('<th>Volumen IAR (t)</th>');
            if (correccion) {
                buffer.push('<th><i class="fa fa-minus-circle"></i></th>');
                buffer.push('<th>Correcto</th>');
            }
            buffer.push('</tr></thead>');
            buffer.push('<tbody>');
            for (var i = 0; i < contratosProductor.length; i++) {
                var p = contratosProductor[i];
                buffer.push('<tr>');
                buffer.push('<td class="text-right">');
                buffer.push(i + 1);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(utils.getNombreCompleto(p));
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(p.curp);
                buffer.push('</td>');
                buffer.push('<td>');
                buffer.push(p.rfc);
                buffer.push('</td>');
                buffer.push('<td class="text-right">');
                buffer.push($.segalmex.util.format(p.superficie, 3));
                buffer.push('</td>');
                buffer.push('<td class="text-right">');
                buffer.push($.segalmex.util.format(p.volumenContrato, 3));
                buffer.push('</td>');
                buffer.push('<td class="text-right">');
                buffer.push($.segalmex.util.format(p.volumenIar, 3));
                buffer.push('</td>');
                if (correccion) {
                    buffer.push('<td class="text-center"><button id="eliminar-productor-');
                    buffer.push(i);
                    buffer.push('" class="btn btn-danger btn-sm eliminar-productor"><i class="fa fa-minus-circle"></i></button></td>');
                    buffer.push('</td>');
                    buffer.push('<td class="text-center">');
                    buffer.push(p.correcto !== undefined && !p.correcto ? '<i title="' + p.error + '" class="fas fa-times-circle text-danger"></i>' : '<i class="fas fa-check-circle text-success"></i>');
                    buffer.push('</td>');
                }
                buffer.push('</tr>');
            }
            buffer.push('</tbody>');
            buffer.push('</table>');
            buffer.push('</div>');
            buffer.push('<br/>');
            return buffer.join('');
        }
    };

    utils.getNombreCompleto = function (p) {
        var nombre = p.nombre;
        var pApellido = p.primerApellido ? p.primerApellido : '';
        ;
        var sApellido = p.segundoApellido ? p.segundoApellido : '';
        return nombre + ' ' + pApellido + ' ' + sApellido;
    };
})(jQuery);