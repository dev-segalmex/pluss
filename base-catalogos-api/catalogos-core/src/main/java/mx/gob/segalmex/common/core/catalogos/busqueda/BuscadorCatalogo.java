/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.catalogos.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.base.Catalogo;
import mx.gob.segalmex.pluss.modelo.base.EnumCatalogo;

/**
 *
 * @author ismael
 */
public interface BuscadorCatalogo {

    <T extends Catalogo> List<T> busca(ParametrosCatalogo<T> parametros);

    <T extends Catalogo> T buscaElemento(ParametrosCatalogo<T> parametros);

    <T extends Catalogo> List<T> busca(Class<T> clase);

    <T extends Catalogo> T buscaElemento(Class<T> clase, String clave);

    <T extends Catalogo> T buscaElemento(Class<T> clase, Integer id);

    <T extends Catalogo> T buscaElemento(Class<T> clase, EnumCatalogo e);

}
