/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.catalogos.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;

/**
 * Interfaz para definir busquedas sobre la entidad {@link Parametro}.
 *
 * @author oscar
 */
public interface BuscadorParametro {

    /**
     * Se encarga de obtener todos los parámetros de un grupo.
     *
     * @param grupo el grupo de parámetros que se desea obtener.
     * @return una lista con los parámetros del grupo recibido.
     */
    List<Parametro> busca(String grupo);

    /**
     * Se encarga de obtener todos los parámetros.
     *
     * @param activos
     * @return una lista con todos los parámetros.
     */
    List<Parametro> busca(Boolean activos);

    /**
     * Se encarga de obtener un parámetro por su clave.
     *
     * @param clave la clave del parámetro.
     * @return el parámetro obtenido.
     */
    Parametro buscaElemento(String clave);

    /**
     * Se encarga de obtener un parámetro por su id.
     * @param id el ide del parámetro.
     * @return  el parámetro obtenido.
     */
    Parametro buscaElemento(Integer id);

    /**
     * Busca un parámetro que pudiera no existir, por lo que no lanza
     * excepciones en el caso de que no exista y regresa <code>null</code>.
     *
     * @param clave la clave del parámetro que se busca.
     * @return el parámetro buscado o <code>null</code> en caso de que no
     * exista.
     */
    Parametro buscaElementoOpcional(String clave);

    /**
     * Se encarga de obtener una lista de valores parámetro ya convertida al
     * tipo de la clase recibida.
     *
     * @param <T>
     * @param grupo
     * @param clase
     * @return una lista de los valores de los parámetros del tipo de la clase
     * recibida.
     */
    <T> List<T> getValores(String grupo, Class<T> clase);

    /**
     * Se encarga de obtener el valor de un parámetro ya convertido al tipo de
     * la clase recibida. Regresa null en caso se que no exista.
     *
     * @param <T>
     * @param clave
     * @param clase
     * @return el valor del parámetro ya convertido en el tipo especificado.
     */
    <T> T getValorOpcional(String clave, Class<T> clase);

    String getValorOpcional(String clave);

}
