/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.catalogos.busqueda;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 *
 * @author oscar
 */
@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ParametrosUsoCatalogo {

    private Integer id;

    private Class clase;

    private String clave;

    private String uso;

    private boolean usoAproximado = false;

    private Boolean activo = Boolean.TRUE;

    private Integer orden;

}
