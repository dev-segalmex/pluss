package mx.gob.segalmex.common.core.catalogos.busqueda;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.base.Catalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.UsoCatalogo;

/**
 * Interfaz para definir los métodos del uso de catálogos.
 *
 * @author ismael
 */
public interface BuscadorUsoCatalogo {

    <T extends Catalogo> T buscaElemento(Class<T> clase, String clave, String uso);

    <T extends Catalogo> List<T> busca(Class<T> clase, String uso);

    List<UsoCatalogo> buscaUsos(ParametrosUsoCatalogo parametros);

    List<UsoCatalogo> buscaUsos(Class clase, String uso);

    UsoCatalogo buscaElementoUso(ParametrosUsoCatalogo parametros);

    UsoCatalogo buscaElementoUso(Class clase, String clave, String uso);

}
