/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.catalogos.busqueda;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.base.Catalogo;
import mx.gob.segalmex.pluss.modelo.base.EnumCatalogo;
import org.apache.commons.lang3.StringUtils;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ismael
 */
@Repository("buscadorCatalogo")
@Slf4j
public class BuscadorCatalogoJpa implements BuscadorCatalogo {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public <T extends Catalogo> List<T> busca(ParametrosCatalogo<T> parametros) {
        StringBuilder sb = new StringBuilder("SELECT c FROM ");
        sb.append(parametros.getClase().getName());
        sb.append(" c ");

        Map<String, Object> params = new HashMap<>();
        boolean first = true;
        if (parametros.getClave() != null) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "c.clave", "clave", parametros.getClave(), params);
        }

        if (parametros.getId() != null) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "c.id", "id", parametros.getId(), params);
        }

        //hacemos que busque sólo activos
        if (Objects.nonNull(parametros.getSoloActivos()) && parametros.getSoloActivos()) {
            first = QueryUtils.agregaWhereAnd(first, sb);
            QueryUtils.and(sb, "c.activo", "activo", true, params);
        }

        if (Objects.nonNull(parametros.getExtraParams()) && !parametros.getExtraParams().isEmpty()) {
            int i = 0;
            for (Map.Entry<String, Object> e : parametros.getExtraParams().entrySet()) {
                String nombre = "p" + i;
                first = QueryUtils.agregaWhereAnd(first, sb);
                QueryUtils.and(sb, "c." + e.getKey(), nombre, e.getValue(), params);
                i++;
            }
        }

        sb.append("ORDER BY c.orden, c.nombre ");
        LOGGER.debug("El query '{}' tiene parámetros: {}", sb, !first);
        TypedQuery<T> q = entityManager.createQuery(sb.toString(), parametros.getClase());
        QueryUtils.setParametros(q, params);

        return q.getResultList();
    }

    @Override
    public <T extends Catalogo> T buscaElemento(ParametrosCatalogo<T> parametros) {
        List<T> resultados = busca(parametros);

        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontró el elemento de catálogo.", 1);
        }

        if (resultados.size() > 1) {
            throw new IncorrectResultSizeDataAccessException("Se encontró más "
                    + "de un elemnto de catálogo.", 1);
        }

        return resultados.get(0);
    }

    @Override
    public <T extends Catalogo> List<T> busca(Class<T> clase) {
        ParametrosCatalogo<T> parametros = new ParametrosCatalogo<>();
        parametros.setClase(clase);
        parametros.setSoloActivos(Boolean.TRUE);
        return busca(parametros);
    }

    @Override
    public <T extends Catalogo> T buscaElemento(Class<T> clase, String clave) {
        if (StringUtils.trimToNull(clave) == null) {
            throw new IllegalArgumentException("La clave no puede ser null.");
        }
        ParametrosCatalogo<T> parametros = new ParametrosCatalogo<>();
        parametros.setClase(clase);
        parametros.setClave(clave);
        LOGGER.debug("Buscando {} con clave: {}", clase, clave);
        return buscaElemento(parametros);
    }

    @Override
    public <T extends Catalogo> T buscaElemento(Class<T> clase, Integer id) {
        if (id == null) {
            throw new IllegalArgumentException("El id no puede ser null.");
        }

        ParametrosCatalogo<T> parametros = new ParametrosCatalogo<>();
        parametros.setClase(clase);
        parametros.setId(id);
        LOGGER.debug("Buscando {} con clave: {}", clase, id);
        return buscaElemento(parametros);
    }

    @Override
    public <T extends Catalogo> T buscaElemento(Class<T> clase, EnumCatalogo e) {
        return buscaElemento(clase, e.getClave());
    }

}
