package mx.gob.segalmex.common.core.catalogos.busqueda;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.base.Catalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.UsoCatalogo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ismael
 */
@Repository("buscadorUsoCatalogo")
@Slf4j
public class BuscadorUsoCatalogoJpa implements BuscadorUsoCatalogo {

    private static final String ACTIVO = "--";

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Override
    public <T extends Catalogo> T buscaElemento(Class<T> clase, String clave, String uso) {
        // Verificamos que exista el uso
        buscaElementoUso(clase, clave, uso);
        // Y si existe buscamos el catálogo
        return buscadorCatalogo.buscaElemento(clase, clave);
    }

    @Override
    public <T extends Catalogo> List<T> busca(Class<T> clase, String uso) {
        return buscaUsos(clase, uso).stream()
                .map(uc -> buscadorCatalogo.buscaElemento(clase, uc.getClave()))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public UsoCatalogo buscaElementoUso(Class clase, String clave, String uso) {
        Objects.requireNonNull(clase, "La clase no puede ser nula.");
        Objects.requireNonNull(clave, "La clave no puede ser nulo.");
        Objects.requireNonNull(uso, "El uso no puede ser nulo.");

        ParametrosUsoCatalogo parametros = ParametrosUsoCatalogo.builder()
                .clase(clase)
                .clave(clave)
                .uso(uso)
                .activo(Boolean.TRUE)
                .build();
        return buscaElementoUso(parametros);
    }

    @Override
    public List<UsoCatalogo> buscaUsos(ParametrosUsoCatalogo parametros) {
        StringBuilder sb = new StringBuilder();
        sb.append(" SELECT uc FROM UsoCatalogo uc ");
        Map<String, Object> params = new HashMap<>();
        boolean primero = true;

        if (Objects.nonNull(parametros.getClase())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "uc.clase", "clase", parametros.getClase().getName(), params);
        }

        if (Objects.nonNull(parametros.getId())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "uc.id", "id", parametros.getId(), params);
        }

        if (Objects.nonNull(parametros.getClave())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "uc.clave", "clave", parametros.getClave(), params);
        }

        if (Objects.nonNull(parametros.getActivo()) && parametros.getActivo()) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "uc.estatusFechaBaja", "estatusFechaBaja", ACTIVO, params);
        }

        if (Objects.nonNull(parametros.getUso())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            sb.append(" uc.uso ");
            sb.append(parametros.isUsoAproximado() ? " like :uso " : " = :uso ");
            params.put("uso", parametros.getUso() + (parametros.isUsoAproximado() ? "%" : ""));
        }

        if (Objects.nonNull(parametros.getOrden())) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "uc.orden", "orden", parametros.getOrden(), params);
        }
        sb.append("ORDER BY uc.orden ");

        TypedQuery<UsoCatalogo> query = entityManager.createQuery(sb.toString(), UsoCatalogo.class);
        QueryUtils.setParametros(query, params);

        long inicio = System.currentTimeMillis();
        List<UsoCatalogo> usos = query.getResultList();
        LOGGER.debug("Se encontraron {} Usos en {} ms.", usos.size(),
                System.currentTimeMillis() - inicio);
        return usos;
    }

    @Override
    public List<UsoCatalogo> buscaUsos(Class clase, String uso) {
        Objects.requireNonNull(clase, "La clase no puede ser nula.");
        Objects.requireNonNull(uso, "El uso no puede ser nulo.");

        ParametrosUsoCatalogo parametros = ParametrosUsoCatalogo.builder()
                .clase(clase)
                .uso(uso)
                .activo(Boolean.TRUE)
                .build();
        return buscaUsos(parametros);
    }

    @Override
    public UsoCatalogo buscaElementoUso(ParametrosUsoCatalogo parametros) {
        List<UsoCatalogo> resultados = buscaUsos(parametros);

        if (resultados.isEmpty()) {
            throw new EmptyResultDataAccessException("No se encontró el elemento del uso catálogo.", 1);
        }

        if (resultados.size() > 1) {
            throw new IncorrectResultSizeDataAccessException("Se encontró más "
                    + "de un elemnto del uso catálogo.", 1);
        }

        return resultados.get(0);
    }

}
