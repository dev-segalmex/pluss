/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.catalogos.busqueda;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.util.QueryUtils;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author oscar
 */
@Repository("buscadorParametro")
@Slf4j
public class BuscadorParametroJpa implements BuscadorParametro {

    private static final String ACTIVO = "--";

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Parametro> busca(String grupo) {
        Objects.requireNonNull(grupo, "El grupo no puede ser nulo.");

        TypedQuery<Parametro> query = entityManager
                .createQuery("SELECT p FROM Parametro p "
                        + "WHERE p.grupo = :grupo "
                        + "AND p.estatusFechaBaja = :estatusFechaBaja ORDER BY p.orden", Parametro.class)
                .setParameter("grupo", grupo)
                .setParameter("estatusFechaBaja", ACTIVO);
        return query.getResultList();
    }

    @Override
    public List<Parametro> busca(Boolean activos) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT p FROM Parametro p ");
        Map<String, Object> params = new HashMap<>();
        boolean primero = true;

        if (Objects.nonNull(activos) && activos) {
            primero = QueryUtils.agregaWhereAnd(primero, sb);
            QueryUtils.and(sb, "p.estatusFechaBaja", "estatusFechaBaja", ACTIVO, params);
        }

        TypedQuery<Parametro> query = entityManager.createQuery(sb.toString(), Parametro.class);
        QueryUtils.setParametros(query, params);

        return query.getResultList();

    }

    @Override
    public Parametro buscaElemento(String clave) {
        Objects.requireNonNull(clave, "La clave no puede ser nula.");

        LOGGER.debug("Buscando clave: {}", clave);
        TypedQuery<Parametro> query = entityManager
                .createQuery("SELECT p FROM Parametro p "
                        + "WHERE p.clave = :clave "
                        + "AND p.estatusFechaBaja = :estatusFechaBaja", Parametro.class)
                .setParameter("clave", clave)
                .setParameter("estatusFechaBaja", ACTIVO);
        return query.getSingleResult();

    }

    @Override
    public Parametro buscaElemento(Integer id) {
        Objects.requireNonNull(id, "El id no puede ser nulo.");
        TypedQuery<Parametro> query = entityManager
                .createQuery("SELECT p FROM Parametro p "
                        + "WHERE p.id = :id ", Parametro.class)
                .setParameter("id", id);
        return query.getSingleResult();
    }

    @Override
    public Parametro buscaElementoOpcional(String clave) {
        try {
            LOGGER.debug("Buscando clave: {}", clave);
            return buscaElemento(clave);
        } catch (EmptyResultDataAccessException | NoResultException ouch) {
            LOGGER.warn("No se encontró la clave {} regresando null.", clave);
            return null;
        }
    }

    @Override
    public <T> List<T> getValores(String grupo, Class<T> clase) {
        return busca(grupo)
                .stream()
                .map(p -> getInstance(clase, p.getValor()))
                .collect(Collectors.toCollection(ArrayList::new));
    }

    @Override
    public <T> T getValorOpcional(String clave, Class<T> clase) {
        Parametro param = buscaElementoOpcional(clave);
        if (Objects.isNull(param)) {
            return null;
        }

        return getInstance(clase, param.getValor());
    }

    @Override
    public String getValorOpcional(String clave) {
        Parametro param = buscaElementoOpcional(clave);
        if (Objects.isNull(param)) {
            return null;
        }
        return param.getValor();
    }

    public <T> T getInstance(Class<T> clase, String valor) {
        try {
            Constructor<T> constructor = clase.getConstructor(new Class[]{String.class});
            return constructor.newInstance(valor);
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException
                | IllegalArgumentException | InvocationTargetException ouch) {
            LOGGER.error("Ocurrió un error al construir los valores.", ouch);
            throw new IllegalArgumentException("Error al construir los valores.", ouch);

        }
    }

}
