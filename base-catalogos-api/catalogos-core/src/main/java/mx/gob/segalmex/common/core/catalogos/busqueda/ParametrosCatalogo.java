/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.catalogos.busqueda;

import java.util.HashMap;
import java.util.Map;
import mx.gob.segalmex.pluss.modelo.base.Catalogo;

/**
 *
 * @param <T>
 * @author ismael
 */
public class ParametrosCatalogo<T extends Catalogo> {

    private Class<T> clase;

    private String clave;

    private Integer id;

    private Boolean soloActivos;

    /**
     * La lista de valores adicionales para filtrar un catalogo.
     */
    private Map<String, Object> extraParams = new HashMap<>();

    public Class<T> getClase() {
        return clase;
    }

    public void setClase(Class<T> clase) {
        this.clase = clase;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Boolean getSoloActivos() {
        return soloActivos;
    }

    public void setSoloActivos(Boolean soloActivos) {
        this.soloActivos = soloActivos;
    }

    public Map<String, Object> getExtraParams() {
        return extraParams;
    }

    public void setExtraParams(Map<String, Object> extraParams) {
        this.extraParams = extraParams;
    }

}
