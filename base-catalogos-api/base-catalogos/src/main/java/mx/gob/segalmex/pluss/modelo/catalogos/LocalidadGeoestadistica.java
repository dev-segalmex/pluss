package mx.gob.segalmex.pluss.modelo.catalogos;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractCatalogo;

@Entity
@Table(name = "localidad_geoestadistica")
@Getter
@Setter
public class LocalidadGeoestadistica extends AbstractCatalogo {

    @ManyToOne
    @JoinColumn(name = "municipio_id", nullable = false)
    private Municipio municipio;

    @Column(name = "ambito", length = 31)
    private String ambito;

    @Column(name = "latitud", precision = 19, scale = 8)
    private BigDecimal latitud;

    @Column(name = "longitud", precision = 19, scale = 8)
    private BigDecimal longitud;

    @Column(name = "altitud")
    private BigDecimal altitud;

    @Column(name = "clave_carta", length = 31)
    private String claveCarta;

}
