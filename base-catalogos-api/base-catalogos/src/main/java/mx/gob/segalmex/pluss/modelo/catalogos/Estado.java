package mx.gob.segalmex.pluss.modelo.catalogos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractCatalogo;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "estado")
@Getter
@Setter
public class Estado extends AbstractCatalogo {

    /**
     * La abreviatura que se utiliza para el estado.
     */
    @Column(name = "abreviatura")
    private String abreviatura;

}
