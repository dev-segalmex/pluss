package mx.gob.segalmex.pluss.modelo.catalogos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;

/**
 * Clase para indicar el uso de los diferentes catálogos.
 *
 * @author ismael
 */
@Entity
@Table(name = "uso_catalogo", uniqueConstraints = {@UniqueConstraint(columnNames = {"clase", "clave", "uso", "estatus_fecha_baja"})})
@Getter
@Setter
public class UsoCatalogo extends AbstractEntidad {

    /**
     * Indica la clase a la que pertenece el catálogo.
     */
    @Column(name = "clase", nullable = false, length = 127)
    private String clase;

    /**
     * Indica la clave del catálogo.
     */
    @Column(name = "clave", nullable = false, length = 63)
    private String clave;

    /**
     * Indica el uso del catálogo.
     */
    @Column(name = "uso", nullable = false, length = 63)
    private String uso;

    /**
     * La fecha de la baja del UsoCatalogo, o "--" sí esta activo.
     */
    @Column(name = "estatus_fecha_baja", nullable = false, length = 31)
    private String estatusFechaBaja = "--";

    /**
     * El orden del catálogo respecto a otros.
     */
    @Column(name = "orden", nullable = false)
    private int orden;

}
