package mx.gob.segalmex.pluss.modelo.catalogos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractCatalogo;

@Entity
@Table(name = "localidad")
@Getter
@Setter
public class Localidad extends AbstractCatalogo {

    @ManyToOne
    @JoinColumn(name = "municipio_id", nullable = false)
    private Municipio municipio;

    @Column(name = "codigo_postal", nullable = false)
    private String codigoPostal;
}
