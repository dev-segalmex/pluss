/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.catalogos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;

/**
 *
 * @author jurgen
 */
@Entity
@Table(name = "parametro", uniqueConstraints = {@UniqueConstraint(columnNames = {"clave", "estatus_fecha_baja"})})
@Getter
@Setter
public class Parametro extends AbstractEntidad {

    /**
     * El grupo al que pertenece el parametro.
     */
    @Column(name = "grupo", length = 63)
    private String grupo;

    /**
     * El valor del parametro.
     */
    @Column(name = "valor")
    private String valor;

    /**
     * La clave del parametro.
     */
    @Column(name = "clave", length = 63)
    private String clave;

    /**
     * El orden en los parámetros.
     */
    @Column(name = "orden")
    private int orden;

    /**
     * La descripción del Parámetro (Para que sirve).
     */
    @Column(name = "descripcion")
    private String descripcion;

    /**
     * La fecha de la baja del Parámetro, o "--" sí esta activo.
     */
    @Column(name = "estatus_fecha_baja", nullable = false, length = 63)
    private String estatusFechaBaja = "--";

}
