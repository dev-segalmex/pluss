/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.catalogos;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoDocumento;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "tipo-documento")
public class TipoDocumentoConverter extends AbstractCatalogoConverter {

    private final TipoDocumento entity;

    public TipoDocumentoConverter() {
        entity = new TipoDocumento();
        expandLevel = 1;
    }

    public TipoDocumentoConverter(TipoDocumento entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad TipoDocumento no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public TipoDocumento getEntity() {
        return this.entity;
    }

}
