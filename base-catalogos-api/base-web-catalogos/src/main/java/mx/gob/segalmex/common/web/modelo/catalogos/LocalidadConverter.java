package mx.gob.segalmex.common.web.modelo.catalogos;

import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;
import mx.gob.segalmex.pluss.modelo.catalogos.Localidad;

public class LocalidadConverter extends AbstractCatalogoConverter {

    private final Localidad entity;

    public LocalidadConverter() {
        entity = new Localidad();
        expandLevel = 1;
    }

    public LocalidadConverter(Localidad entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Localidad no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public Localidad getEntity() {
        return entity;
    }

    @XmlElement
    public MunicipioConverter getMunicipio() {
        return Objects.nonNull(entity.getMunicipio()) && expandLevel > 0
                ? new MunicipioConverter(entity.getMunicipio(), expandLevel - 1) : null;
    }

    @XmlElement
    public String getCodigoPostal() {
        return entity.getCodigoPostal();
    }

}
