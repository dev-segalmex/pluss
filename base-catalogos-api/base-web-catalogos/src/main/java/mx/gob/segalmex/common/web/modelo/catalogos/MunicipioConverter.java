/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.catalogos;

import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.pluss.modelo.catalogos.Municipio;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "municipio")
public class MunicipioConverter extends AbstractCatalogoConverter {

     /**
     * La entidad de donde se obtendrá la información.
     */
    private final Municipio entity;

    /**
     * Constructor sin parámetros. Este constructor es obligatorio.
     */
    public MunicipioConverter() {
        entity = new Municipio();
        expandLevel = 1;
    }

    /**
     * Constructor de la clase.
     *
     * @param entity que se representa.
     * @param expandLevel de la gráfica de relaciones.
     */
    public MunicipioConverter(Municipio entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Municipio no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public Municipio getEntity() {
        return entity;
    }

    @XmlElement
    public EstadoConverter getEstado() {
        return Objects.nonNull(entity.getEstado()) && expandLevel > 0
                ? new EstadoConverter(entity.getEstado(), expandLevel - 1) : null;
    }
}
