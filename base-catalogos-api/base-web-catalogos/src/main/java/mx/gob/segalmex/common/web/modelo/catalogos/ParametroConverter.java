/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.catalogos;

import java.util.Objects;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;

/**
 *
 * @author oscar
 */
public class ParametroConverter extends AbstractEntidadConverter {

    /**
     * La entidad que respalda el converter.
     */
    private final Parametro entity;

    public ParametroConverter() {
        entity = new Parametro();
        expandLevel = 1;
    }

    public ParametroConverter(Parametro entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Parametro no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public Parametro getEntity() {
        return entity;
    }

    public String getGrupo() {
        return getEntity().getGrupo();
    }

    public void setGrupo(String grupo) {
        getEntity().setGrupo(grupo);
    }

    public String getValor() {
        return getEntity().getValor();
    }

    public void setValor(String valor) {
        getEntity().setValor(valor);
    }

    public String getClave() {
        return getEntity().getClave();
    }

    public void setClave(String clave) {
        getEntity().setClave(clave);
    }

    public String getDescripcion() {
        return getEntity().getDescripcion();
    }

    public void setDescripcion(String descripcion) {
        getEntity().setDescripcion(descripcion);
    }

    public int getOrden() {
        return getEntity().getOrden();
    }

    public void setOrden(int orden) {
        getEntity().setOrden(orden);
    }
}
