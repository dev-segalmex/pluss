/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.catalogos;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.pluss.modelo.catalogos.Banco;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "banco")
public class BancoConverter extends AbstractCatalogoConverter {

    private final Banco entity;

    public BancoConverter() {
        entity = new Banco();
        expandLevel = 1;
    }

    public BancoConverter(Banco entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Banco no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public Banco getEntity() {
        return this.entity;
    }

}
