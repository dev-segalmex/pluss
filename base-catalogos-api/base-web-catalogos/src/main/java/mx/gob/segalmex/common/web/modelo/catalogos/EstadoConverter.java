/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.catalogos;

import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;

/**
 *
 * @author oscar
 */
@XmlRootElement(name = "estado")
public class EstadoConverter extends AbstractCatalogoConverter {

    /**
     * La entidad de donde se obtendrá la información.
     */
    private final Estado entity;

    /**
     * Constructor sin parámetros. Este constructor es obligatorio.
     */
    public EstadoConverter() {
        entity = new Estado();
        expandLevel = 1;
    }

    /**
     * Constructor de la clase.
     *
     * @param entity que se representa.
     * @param expandLevel de la gráfica de relaciones.
     */
    public EstadoConverter(Estado entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Estado no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public Estado getEntity() {
        return this.entity;
    }

    @XmlElement
    public String getAbreviatura() {
        return entity.getAbreviatura();
    }
}
