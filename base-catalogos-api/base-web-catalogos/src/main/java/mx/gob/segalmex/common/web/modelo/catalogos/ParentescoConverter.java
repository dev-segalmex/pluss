/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.catalogos;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.pluss.modelo.catalogos.Parentesco;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;

/**
 *
 * @author ismael
 */
@XmlRootElement(name = "parentesco")
public class ParentescoConverter extends AbstractCatalogoConverter {

    private final Parentesco entity;

    public ParentescoConverter() {
        entity = new Parentesco();
        expandLevel = 1;
    }

    public ParentescoConverter(Parentesco entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Parentesco no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    /**
     * Recupera la entidad representada.
     *
     * @return
     */
    @Override
    public Parentesco getEntity() {
        return this.entity;
    }

}
