/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.catalogos;

import java.util.Objects;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.pluss.modelo.catalogos.Pais;
import mx.gob.segalmex.common.web.modelo.base.AbstractCatalogoConverter;

/**
 *
 * @author oscar
 */
@XmlRootElement(name = "pais")
public class PaisConverter extends AbstractCatalogoConverter {

    /**
     * La entidad de donde se obtendrá la información.
     */
    private final Pais entity;

    /**
     * Constructor sin parámetros. Este constructor es obligatorio.
     */
    public PaisConverter() {
        entity = new Pais();
        expandLevel = 1;
    }

    /**
     * Constructor de la clase.
     *
     * @param entity que se representa.
     * @param expandLevel de la gráfica de relaciones.
     */
    public PaisConverter(Pais entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Pais no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    @Override
    public Pais getEntity() {
        return this.entity;
    }


}
