/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.arroz.pre.registro');
    var data = {};
    var handlers = {};
    var utils = {};

    $.segalmex.arroz.pre.registro.init = function (params) {
        $.segalmex.cultivos.pre.registro.init({
            cultivo: 'arroz'
        });

        $('#guardar-button').click(handlers.guarda);
        $('#si-acepto-condiciones').click($.segalmex.cultivos.pre.registro.aceptoCondiciones);
        $('#no-acepto-condiciones').click($.segalmex.cultivos.pre.registro.noAceptoCondiciones);
        $('#limpiar-button').click($.segalmex.cultivos.pre.registro.limpiar);
        $('#agregar-predio-button').click($.segalmex.cultivos.pre.registro.agregaPredio);
        $('#estado-predio').change($.segalmex.cultivos.pre.registro.cambiaEstadoPredio);
        $('#predios-table').on('click', 'button.eliminar-predio', $.segalmex.cultivos.pre.registro.eliminaPredio);
        $('#cerrar-descarga').click($.segalmex.cultivos.pre.registro.cerrarDescarga);
        $('#cobertura').change($.segalmex.cultivos.pre.registro.eligeCobertura);
        $('#contratos-firmados').change($.segalmex.cultivos.pre.registro.eligeContrato);
        $('#volumen-predio,#superficie-predio').change($.segalmex.cultivos.pre.registro.calculaRendimiento);
        $('#especifique-cobertura').on('changed.bs.select', $.segalmex.cultivos.pre.registro.cambiaLabelCoberturas);
        $('#reimprimir-button').click($.segalmex.cultivos.pre.registro.reimprime);
        $('#reimpresion-confirmar').click($.segalmex.cultivos.pre.registro.confirmaReimpresion);
        $('#curp').change($.segalmex.cultivos.pre.registro.validaCurpRfc);
        $('#rfc').change($.segalmex.cultivos.pre.registro.validaCurpRfc);
    };

    handlers.guarda = function (e) {
        e.preventDefault();
        $(e.target).html("Guardar").prop('disabled', true);
        $('#predios-div .valid-field').prop('disabled', true);
        var errores = [];
        $('.valid-field').valida(errores, true);
        $('#predios-div .valid-field').prop('disabled', false);
        if (errores.length > 0) {
            $(e.target).html("Guardar").prop('disabled', false);
            $('#rendimiento').prop('disabled', true);
            return;
        }
        $.segalmex.cultivos.pre.registro.guarda();
    };
})(jQuery);


