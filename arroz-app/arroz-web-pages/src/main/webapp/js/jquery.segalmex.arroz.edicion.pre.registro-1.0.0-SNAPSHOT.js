/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.arroz.edicion.pre.registro');
    var data = {};
    var handlers = {};
    var utils = {};

    $.segalmex.arroz.edicion.pre.registro.init = function (params) {
        $.segalmex.cultivos.edicion.pre.registro.init({
            cultivo: 'arroz'
        });
        $('#button-buscar').click($.segalmex.cultivos.edicion.pre.registro.busca);
        $('#button-limpiar').click($.segalmex.cultivos.edicion.pre.registro.limpiar);
        $('#regresar-button').click($.segalmex.cultivos.edicion.pre.registro.regresar);
        $('#contratos-firmados').change($.segalmex.cultivos.edicion.pre.registro.eligeContrato);
        $('#agregar-predio-button').click($.segalmex.cultivos.edicion.pre.registro.agregaPredio);
        $('#predios-table').on('click', 'button.eliminar-predio', $.segalmex.cultivos.edicion.pre.registro.eliminaPredio);
        $('#estado-predio').change($.segalmex.cultivos.edicion.pre.registro.cambiaEstadoPredio);
        $('#cobertura').change($.segalmex.cultivos.edicion.pre.registro.deshabilitaCobertura);
        $('#actualizar-button').click($.segalmex.cultivos.edicion.pre.registro.actualiza);
        $('#curp').change($.segalmex.cultivos.edicion.pre.registro.validaCurpRfc);
        $('#rfc').change($.segalmex.cultivos.edicion.pre.registro.validaCurpRfc);
    };
})(jQuery);