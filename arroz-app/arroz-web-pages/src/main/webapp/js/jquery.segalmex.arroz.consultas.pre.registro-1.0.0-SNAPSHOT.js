/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.arroz.consultas.pre.registro');

    $.segalmex.arroz.consultas.pre.registro.init = function (params) {
        $.segalmex.cultivos.consultas.pre.registro.init({cultivo: 'arroz'});
        $('#button-limpiar').click($.segalmex.cultivos.consultas.pre.registro.limpiar);
        $('#button-buscar').click($.segalmex.cultivos.consultas.pre.registro.busca);
        $('#button-regresar-resultados').click($.segalmex.cultivos.consultas.pre.registro.regresaResultados);
        $('#datos-busqueda-form').keypress(function (event) {
            if (event.which === 13) {
                event.preventDefault();
                $('#button-buscar').click();
            }
        });
        $('#button-exportar').click($.segalmex.cultivos.consultas.pre.registro.exporta);
        $('#ciclo').change($.segalmex.cultivos.consultas.pre.registro.exportaCiclo);
        $('#button-buscar-agrupado').click($.segalmex.cultivos.consultas.pre.registro.buscaAgrupado);
        $('#button-regresar-resultados-agrupaciones').click($.segalmex.cultivos.consultas.pre.registro.regresaResultadosAgrupaciones);
    };
})(jQuery);