/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


(function ($) {
    $.segalmex.namespace('segalmex.arroz.inscripcion.correccion');

    var data = {
        tipoInscripcion: 'productores',
        catalogos: null,
        catalogosProductor: null,
        catalogosFactura: null,
        entradas: [],
        requiereAnexos: false,
        inscripcion: null,
        rendimientos: {},
        edicionPredios: [],
        edicionAnexosPredio: [],
        edicionSociedades: [],
        anexosActaConstitutiva: [],
        anexosListaSocios: [],
        prediosPermitidos: null,
        empresasPermitidos: null,
        uuid: '',
        archivosCargados: [],
        solicitado: false,
        rendimientoSolValido: false,
        volumenMaximo: 0,
        rendimientoMaximo: 0,
        estadoActual: null,
        volumenPredio: 0,
        superficiePredio: 0,
        anexosPrediosolicitado: [],
        indexPredios: 0,
        indexEmpresas: 0,
        precioTonelada: 0,
        limiteArchivo: 24 * 1024 * 1024
    };
    var handlers = {};
    var utils = {};

    $.segalmex.arroz.inscripcion.correccion.init = function () {
        $.segalmex.common.bandejas.init({
            configuracion: utils.configuracionBandejas(),
            namespace: $.segalmex.arroz.inscripcion.correccion
        });
        $('#menu-bandejas a.nav-link').click(handlers.cambiaBandeja);
        $('#menu-edicion a.nav-link').click(handlers.cambiaSeccionEdicion);
        $('#button-corrige').click(handlers.corrige);
        $('#button-regresar').click(handlers.regresar);
        $('#actualizar-button').click(handlers.actualiza);
        $('#nuevo-comentario-button').click(handlers.nuevoComentario);
        $('#agregar-comentario-button').click(handlers.agregaComentario);
        $('#adjuntar-button').click(handlers.adjuntar);
        $('#subir-button').click(handlers.subirArchivos);
        $('#enviar-button').click(handlers.enviar);
        $('#inscripcion-datos-capturados-incorrectos').on('click', 'button.btn-comentario', handlers.muestraComentario);
        $('#contenido-comentario').configura({
            minlength: 1,
            maxlength: 2047,
            allowSpace: true,
            textTransform: null,
            pattern: /^[A-Za-z0-9ÑñÁÉÍÓÚáéíóúÄËÏÖÜäëïöü@\.,:\n\-\(\)" ]*$/
        }).validacion();
        //Configuracion para la parte de edición
        utils.inicializaValidaciones();
        $('#edicion-tipo-posesion-predio').change(handlers.cambiaTipoPosesion);
        $('#edicion-estado-predio').change(handlers.cambiaEstadoPredio);
        $('#edicion-volumen-predio,#edicion-superficie-predio').change(handlers.cambiaRendimiento);
        $('#menu-tabs-predios a.nav-link').click(handlers.cambiaTipoGeorreferencia);
        $('#edicion-predios-table').on('click', 'button.eliminar-predio', handlers.eliminaPredio);
        $('#edicion-predios-table').on('click', 'a.muestra-ubicacion', handlers.muestraUbicacion);
        $('#agregar-predio-button').click(handlers.agregaPredio);
        $('#agregar-sociedad-button').click(handlers.agregaSociedad);
        $('#sociedades-table').on('click', 'button.eliminar-sociedad', handlers.eliminaSociedad);
        $('#button-guarda-edicion').click(handlers.guardaEdicion);
        $('#cerrar-descarga').click(handlers.cierraDescarga);
        //rendimiento solicitado
        $('#aceptar-rendimiento-solicitado').click(utils.confirmaRendimientoSolicitado);
        $('#cancelar-rendimiento-solicitado').click(utils.cancelarRendimientoSolicitado);
        $('#btn-mostrar-correctos').click(handlers.muestraCorrectos);

        $.ajax({
            type: 'GET',
            url: '/arroz/resources/paginas/inscripcion-productor/',
            dataType: 'json'
        }).done(function (response) {
            data.catalogos = response;
            data.tiposCultivo = response.tipos;
            //cargamos los catalogos para los combos de factura
            $.ajax({
                type: 'GET',
                url: '/arroz/resources/paginas/inscripcion-factura/',
                dataType: 'json'
            }).done(function (response) {
                data.catalogosFactura = response;
            }).fail(function () {
                alert('Error: Al descargar los recursos para factura.');
            });

            $('#ciclo-agricola-global').cicloAgricola({
                ciclos: response.ciclos,
                actual: response.cicloSeleccionado,
                reload: true,
                sistema: 'arroz',
                showButton: false,
                onlyRead: true
            });
        }).fail(function () {
            alert('Error: Al descargar los recursos de la página.');
        });
        $('#detalle-elemento-documentacion input.form-control-file').change(handlers.verificaTamanoArchivo);
    };

    handlers.enviar = function (e) {
        e.preventDefault();
        for (var i = 0; i < data.archivosCargados.length; i++) {
            var a = data.archivosCargados[i];
            if (a.cargado === false) {
                alert('Error: es necesario subir todos los archivos anexos pendientes.');
                return;
            }
        }
        $('#enviar-modal').modal('show');
    };

    handlers.subirArchivos = function (e) {
        e.preventDefault();
        var archivos = [];
        var files = $('#archivos-cargados input.form-control-file');
        for (var i = 0; i < files.length; i++) {
            var a = files[i];
            if (a.files.length !== 0) {
                archivos.push(a);
            }
        }
        if (archivos.length === 0) {
            alert('Se requiere seleccionar al menos un archivo para subir.');
            return;
        }
        $('#div-carga-archivos').cargaArchivos({
            archivos: utils.getArchivos(archivos),
            callBackCerrar: handlers.cierraPluginArchivos,
            btnDescarga: false,
            indicaciones: 'Por favor espere mientras se suben los documentos anexos.'
        });
    };

    handlers.cierraPluginArchivos = function () {
        var actual = $.segalmex.common.bandejas.actual();
        var datos = {
            datos: true,
            comentarios: true,
            archivosCargados: true,
            expand: 3
        };

        $.ajax({
            url: '/arroz/resources/productores/maiz/inscripcion/' + data.uuid,
            data: datos,
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            // Construimos el detalle
            utils.construyeInscripcion(response, 'productores');
            $('#botones-entrada button.btn,#div-carta-acuerdo,.detalle-elemento-form').hide();
            $('#button-regresar,#nuevo-comentario-button').show();
            $('#carta-acuerdo-tmp-pdf').prop('disabled', true);
            switch (actual) {
                case 'edicion':
                    $('#button-guarda-edicion').show();
                    break;
                case 'documentacion':
                    if (response.empresas.length > 0) {
                        $('#div-carta-acuerdo').show();
                        $('#carta-acuerdo-tmp-pdf').prop('disabled', false);
                    }
                    $('#subir-button').show();
                    $('#enviar-button').show();
                    break;
                case 'correccion':
                    $('#button-corrige').show();
                    if (data.tipoInscripcion !== 'facturas') {
                        $('#actualizar-button').show();
                    }
                    var correctos = utils.filtraDatosCapturados(response.datos, true);
                    var inCorrectos = utils.filtraDatosCapturados(response.datos, false);
                    $('#inscripcion-datos-capturados-incorrectos tbody').html(utils.creaTablaCaptura(inCorrectos));
                    $('#inscripcion-datos-capturados-correctos tbody').html(utils.creaTablaCaptura(correctos));
                    utils.configuraTablaCaptura(response.datos);
                    break;
            }

            $('#lista-entradas').hide();
            $('#detalle-elemento-' + actual).show();
            $('#detalles-entrada,#botones-entrada').show();
        }).fail(function () {
            alert('Error: No fue posible obtener el registro.');
        });
    };

    $.segalmex.arroz.inscripcion.correccion.cargaDatos = function (fn) {
        $.ajax({
            url: '/arroz/resources/paginas/bandejas/correccion/' + data.tipoInscripcion,
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            data.entradas = response;
            fn(response);
        }).fail(function () {
            alert('Error: No se pudo obtener la lista de registros de productores.');
        });
        return data.entradas;
    };

    $.segalmex.arroz.inscripcion.correccion.limpiar = function () {
        data.inscripcion = null;
        data.requiereAnexos = false;
        $('#detalle-elemento').html('');
        $('#detalles-entrada,#botones-entrada').hide();
    };

    $.segalmex.arroz.inscripcion.correccion.mostrar = function (e) {
        e.preventDefault();
        var folio = e.target.id.substring('link-id-'.length);
        data.inscripcion = $.segalmex.get(data.entradas, folio, 'folio');
        var actual = $.segalmex.common.bandejas.actual();

        // Obtenemos la inscripcion
        var tipoInscripcion = data.tipoInscripcion;
        var datos = {
            datos: true,
            comentarios: true,
            expand: 3
        };
        if (tipoInscripcion === 'productores') {
            datos.archivosCargados = true;
        }
        $.ajax({
            url: '/arroz/resources/' + tipoInscripcion + '/maiz/inscripcion/' + data.inscripcion.uuid,
            data: datos,
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            $('#div-datos-capturados-correctos').hide();
            $('#btn-mostrar-correctos').html('Mostrar <span class="fas fa-check-circle text-secondary" aria-hidden="true">');
            // Construimos el detalle
            utils.construyeInscripcion(response, tipoInscripcion);
            $('#botones-entrada button.btn,#div-carta-acuerdo,.detalle-elemento-form').hide();
            $('#button-regresar,#nuevo-comentario-button').show();
            $('#carta-acuerdo-tmp-pdf').prop('disabled', true);

            switch (tipoInscripcion) {
                case "facturas":
                    data.precioTonelada = response.precioTonelada;
                    utils.configuraBandeja(actual, response);
                    break;
                case "productores":
                    utils.cargaCatalogosProductor(actual, response);
                    break;
            }
            $('#detalle-elemento-edicion input.form-control-file').change(handlers.verificaTamanoArchivo);
        }).fail(function () {
            alert('Error: No fue posible obtener el registro.');
        });
    };

    utils.configuraBandeja = function (actual, inscricpcion) {
        switch (actual) {
            case 'edicion':
                $('#button-guarda-edicion').show();
                $('#div-tabla-anexos').hide();
                $('#menu-edicion a.nav-link').removeClass('active');
                $('#muestra-predios-productor').addClass('active');
                $('#muestra-predios-productor').click();
                utils.resetPredios();
                break;
            case 'documentacion':
                if (inscricpcion.empresas.length > 0) {
                    $('#carta-acuerdo-tmp-pdf').prop('disabled', false);
                    $('#div-carta-acuerdo').show();
                }
                $('#subir-button').show();
                $('#enviar-button').show();
                $('#div-tabla-anexos').show();
                break;
            case 'correccion':
                $('#button-corrige').show();
                if (data.tipoInscripcion !== 'facturas') {
                    $('#actualizar-button').show();
                }
                var correctos = utils.filtraDatosCapturados(inscricpcion.datos, true);
                var inCorrectos = utils.filtraDatosCapturados(inscricpcion.datos, false);
                $('#inscripcion-datos-capturados-incorrectos tbody').html(utils.creaTablaCaptura(inCorrectos));
                $('#inscripcion-datos-capturados-correctos tbody').html(utils.creaTablaCaptura(correctos));
                utils.configuraTablaCaptura(inscricpcion.datos);
                $('#div-tabla-anexos').hide();
                break;
        }

        $('#lista-entradas').hide();
        $('#detalle-elemento-' + actual).show();
        $('#detalles-entrada,#botones-entrada').show();
    };

    utils.filtraDatosCapturados = function (datos, correcto) {
        var datosFiltrados = [];
        for (var i = 0; i < datos.length; i++) {
            var d = datos[i];
            if (d.correcto && correcto) {
                datosFiltrados.push(d);
            } else if (!d.correcto && !correcto) {
                datosFiltrados.push(d);
            }
        }
        return datosFiltrados;
    };

    $.segalmex.arroz.inscripcion.correccion.construyeTabla = function (lista) {
        return $.segalmex.common.bandejas.vista.construyeTabla({
            id: 'table-inscripciones',
            lista: lista,
            link: true,
            btnActualizar: true,
            tipoInscripcion: data.tipoInscripcion
        });
    };

    $.segalmex.arroz.inscripcion.correccion.configuraTabla = function () {
        $.segalmex.common.bandejas.vista.configuraTablaInscripcion('table-inscripciones', data.tipoInscripcion);
    };

    handlers.cambiaBandeja = function (e) {
        e.preventDefault();
        var id = e.target.id.substring('muestra-'.length);
        if (data.tipoInscripcion === id) {
            return;
        }
        data.tipoInscripcion = id;
        data.folio = null;

        $('#menu-bandejas a.nav-link').removeClass('active');
        $(e.target).addClass('active');
        $.segalmex.common.bandejas.init({
            configuracion: utils.configuracionBandejas(),
            namespace: $.segalmex.arroz.inscripcion.correccion
        });
    };

    handlers.cambiaSeccionEdicion = function (e) {
        e.preventDefault();
        var id = e.target.id.substring('muestra-'.length);
        $('#menu-edicion a.nav-link').removeClass('active');
        $(e.target).addClass('active');
        $('.tab-edicion').hide();
        $('#' + id).show();
    };

    handlers.verificaTamanoArchivo = function (e) {
        var extension = e.target.id.includes('xlsx') ? 'xlsx' : (e.target.id.includes('xml') ? 'xml' : 'pdf');
        $.segalmex.archivos.verificaArchivo(e, extension, data.limiteArchivo);
    };

    handlers.adjuntar = function (e) {
        e.preventDefault();
        $('#botones-entrada button').prop('disabled', true);

        var files = $('#detalle-elemento-documentacion input.form-control-file:enabled');
        var texto = [];
        var errores = false;
        var fds = [];
        for (var i = 0; i < files.length; i++) {
            if ($(files[i]).val() === '') {
                errores = true;
                texto.push(' * Seleccione el archivo PDF para: ' + $('label[for=' + files[i].id + ']').html() + '\n');
            } else {
                var id = files[i].id;
                var tipo = id.substring(0, id.length - '-tmp-pdf'.length);
                var fd = new FormData();
                fd.append('file', files[i].files[0]);
                fds.push({id: id, fd: fd, tipo: tipo});
            }
        }
        if (errores) {
            alert('Verifique lo siguiente:\n\n' + texto.join(''));
            $('#botones-entrada button').prop('disabled', false);
            return;
        }

        var fails = 0;
        for (i = 0; i < fds.length; i++) {
            (function (dato, ultimo) {
                $.ajaxq('archivosQueue', {
                    url: '/arroz/resources/productores/maiz/inscripcion/' + data.inscripcion.uuid + '/anexos/' + dato.tipo + '/pdf/',
                    type: 'POST',
                    data: dato.fd,
                    contentType: false,
                    processData: false
                }).done(function () {
                }).fail(function () {
                    fails++;
                }).always(function () {
                    // Si no es el último, continuamos
                    if (!ultimo) {
                        return;
                    }
                    // Si no hubo fallos, lo indicamos
                    if (fails === 0) {
                        alert('Los archivos del registro se han agregado correctamente.');
                        $('#botones-entrada button').prop('disabled', false);

                        $.segalmex.common.bandejas.actualizar();
                        $('#enviar-modal').modal('hide');
                        $('.detalle-elemento-form').hide();
                        $('#lista-entradas').show();
                    } else {
                        alert('Error: No fue posible registrar el archivo.');
                        $('#botones-entrada button').prop('disabled', false);
                    }
                });
            })(fds[i], i === fds.length - 1);
        }
    }

    handlers.corrige = function (e) {
        e.preventDefault();
        $('#botones-entrada button').prop('disabled', true);

        if (!utils.validaTablaCaptura()) {
            $('#botones-entrada button').prop('disabled', false);
            return;
        }

        var mensaje = [];
        $('#inscripcion-datos-capturados-incorrectos input.form-control-file').each(function () {
            if ($(this).val() === '') {
                mensaje.push(' * El archivo ' + $(this).attr('id') + ' es requerido.');
            }
            data.requiereAnexos = true;
        });

        if (mensaje.length > 0) {
            alert('Error:\n\n' + mensaje.join('\n'));
            $('#botones-entrada button').prop('disabled', false);
            return;
        }

        var uuid = data.inscripcion.uuid;
        if (data.requiereAnexos) {

            var files = $('#inscripcion-datos-capturados-incorrectos input.form-control-file');
            var fds = [];

            // Armamos los archivos para ser cargados por el plugin.
            for (var i = 0; i < files.length; i++) {
                var file = files[i];
                var id = file.id;
                var tipo = id.substring(0, id.length - '-pdf'.length);
                var url = '/arroz/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + uuid + '/anexos/' + tipo + '/pdf/';
                var fd = new FormData();
                var archivo = $('#' + id)[0].files[0];
                fd.append('file', archivo);
                var etiqueta = $('label[for=' + file.id + ']').html();
                fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
            }
            $('#div-carga-archivos').cargaArchivos({
                archivos: fds,
                callBackCerrar: function () {
                    var capturados = utils.getDatosCapturados();
                    utils.enviaCorreccion(data.inscripcion.uuid, capturados, false);
                },
                btnDescarga: false,
                indicaciones: 'Por favor espere mientras se suben los documentos anexos.'
            });
        } else {
            var capturados = utils.getDatosCapturados();
            utils.enviaCorreccion(data.inscripcion.uuid, capturados, false);
        }
    };

    handlers.actualiza = function (e) {
        e.preventDefault();
        $('#botones-entrada button').prop('disabled', true);

        if (!utils.validaTablaCaptura()) {
            $('#botones-entrada button').prop('disabled', false);
            return;
        }

        var capturados = utils.getDatosCapturados();
        utils.enviaCorreccion(data.inscripcion.uuid, capturados, true);
    };

    handlers.nuevoComentario = function (e) {
        $('#comentario-nuevo-modal').modal('show');
        $('#contenido-comentario').val('');
    }

    handlers.agregaComentario = function (e) {
        $('#contenido-comentario').limpiaErrores();
        var errores = [];
        $('#contenido-comentario').valida(errores, true);
        if (errores.length > 0) {
            return;
        }

        var comentario = {
            tipo: $.segalmex.common.bandejas.actual(),
            contenido: $('#contenido-comentario').val()
        }

        $.ajax({
            url: '/arroz/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + data.inscripcion.uuid + '/comentario/',
            data: JSON.stringify(comentario),
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            $('#comentario-nuevo-modal').modal('hide');
        }).fail(function () {
            alert('Error: No fue posible agregar el comentario.');
            $('#botones-entrada button').prop('disabled', false);
        });
    }

    handlers.muestraComentario = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var clave = id.substring('btn-comentario-'.length);
        $('#comentario-modal .modal-body p').html($('#comentario-' + clave).val());
        $('#comentario-modal').modal('show');
    }

    utils.enviaCorreccion = function (uuid, capturados, soloActualizar) {
        var url = '/arroz/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + uuid + '/validacion/';
        if (soloActualizar) {
            url += 'actualizacion/';
        } else {
            url += 'negativa/'
        }

        $.ajax({
            url: url,
            data: JSON.stringify(capturados),
            type: 'PUT',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            alert('El registro ha sido ' + (soloActualizar ? 'actualizado.' : 'enviado a revalidación.'));
            $('#botones-entrada button').prop('disabled', false);

            if (!soloActualizar) {
                $.segalmex.common.bandejas.actualizar();
                $('.detalle-elemento-form').hide();
                $('#detalle-elemento-comentarios').html('');
                $('#lista-entradas').show();
            }
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Error: No fue posible ' + (soloActualizar ? 'actualizar el registro.' : 'corregir el registro.'));
            }
            $('#botones-entrada button').prop('disabled', false);
        });
    }

    handlers.regresar = function (e) {
        e.preventDefault();

        $.segalmex.arroz.inscripcion.correccion.limpiar();
        $('#lista-entradas').show();
    };

    utils.getDatosCapturados = function () {
        var datos = $('#inscripcion-datos-capturados-incorrectos .dato-valor');
        var datosCorrectos = $('#inscripcion-datos-capturados-correctos .dato-valor');
        for (var i = 0; i < datosCorrectos.length; i++) {
            datos.push(datosCorrectos[i]);
        }
        var dcs = [];
        for (var i = 0; i < datos.length; i++) {
            var dato = datos[i];
            var valor = $(dato).val();
            var clave = dato.id;
            var d = {clave: clave, valor: valor};
            if ($(dato).is('select')) {
                d.valorTexto = $(dato).find('option:selected').text();
            }
            dcs.push(d);
        }
        return dcs;
    };

    utils.configuracionBandejas = function () {
        switch (data.tipoInscripcion) {
            case 'productores':
                return {
                    idBandeja: 'entradas-bandeja',
                    idLista: 'lista-entradas',
                    defaultBandeja: 'documentacion',
                    entradas: [
                        {nombre: 'Edición', titulo: 'Edición de productores', prefijoId: 'edicion', descripcion: 'Lista de registros de productores para agregar predios y/o uso de facturas de persona moral.'},
                        {nombre: 'Documentación', titulo: 'Documentación de productores', prefijoId: 'documentacion', descripcion: 'Lista de registros de productores para agregar documentación.'},
                        {nombre: 'Corrección', titulo: 'Corrección de productores', prefijoId: 'correccion', descripcion: 'Lista de registros de productores para corrección.'}
                    ]
                };
            case 'facturas':
                return {
                    idBandeja: 'entradas-bandeja',
                    idLista: 'lista-entradas',
                    defaultBandeja: 'correccion',
                    entradas: [
                        {nombre: 'Corrección', titulo: 'Corrección de facturas', prefijoId: 'correccion', descripcion: 'Lista de registros de facturas para corrección.'}
                    ]
                };
        }
    };

    utils.creaTablaCaptura = function (datos) {
        var buffer = [];
        var grupo = 'general';
        for (var i = 0; i < datos.length; i++) {
            var dato = datos[i];
            if (dato.grupo === undefined) {
                dato.grupo = 'General';
            }

            if (dato.grupo !== grupo) {
                buffer.push('<tr class="table-secondary"><th colspan="4">' + dato.grupo + '</th></tr>');
                grupo = dato.grupo;
            }
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push('<label for="' + dato.clave + '">' + dato.nombre + '</label>');
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(utils.creaCampoCaptura(dato));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(dato.correcto
                    ? '<i class="fas fa-check-circle text-success"></i>'
                    : '<i class="fas fa-times-circle text-danger"></i>')
            buffer.push('</td>');
            buffer.push('<td><div class="form-inline">');
            buffer.push('<input id="comentario-' + dato.clave
                    + '" type="text" class="form-control" readonly="readonly" value="'
                    + (dato.comentario ? dato.comentario : '') + '"/>');
            if (dato.comentario) {
                buffer.push('<button type="button" id="btn-comentario-' + dato.clave + '" class="btn-comentario btn btn-outline-secondary ml-2"><i class="fas fa-comment-alt"></i></button>');
            }
            buffer.push('</div></td>');
            buffer.push('</tr>');
        }

        return buffer.join('\n');
    }

    utils.creaCampoCaptura = function (dato) {
        var buffer = [];

        switch (dato.tipo) {
            case 'texto':
                buffer.push('<input id="');
                buffer.push(dato.clave);
                buffer.push('" type="text" class="form-control dato-valor valid-field"/>');
                break;
            case 'catalogo':
                buffer.push('<select id="');
                buffer.push(dato.clave);
                buffer.push('" class="form-control valid-field dato-valor">');
                buffer.push('<option value="0">Seleccione</option>');
                buffer.push('</select>');
                break;
            case 'archivo':
                buffer.push('<input id="');
                buffer.push(dato.clave);
                buffer.push('" ');
                if (dato.correcto) {
                    buffer.push('type="text" class="form-control dato-valor" value="');
                    buffer.push(dato.valor);
                    buffer.push('" disabled="disabled" />');
                } else {
                    buffer.push('type="file" class="form-control-file" accept="application/pdf" aria-describedby="' + dato.clave + '-ayuda"/>');
                    buffer.push('<small id="' + dato.clave + '-ayuda" class="form-text text-muted">El archivo no deberá ser mayor a 24 MB (25164800 bytes).</small>')
                }
                break;
        }

        return buffer.join('');
    }

    utils.configuraTablaCaptura = function (datos) {
        // SE AGREGA LA FECHA MINIMA PARA LA FECHA DE VENCIMIENTO DE LAS COBERTURAS
        var fechaMinima = new Date($.segalmex.fecha);
        fechaMinima.setFullYear(2021);
        fechaMinima.setMonth(2);
        fechaMinima.setDate(01);
        // Iteramos los datos capturados y ponemos los seleccionados en los combos
        for (var i = 0; i < datos.length; i++) {
            var dato = datos[i];
            var valores = [];
            var tmp = dato.clave.split('_');
            var clave = tmp.length === 1 ? tmp[0] : tmp[1];
            var params = null;
            var catalogos = utils.getCatalogos();

            switch (clave) {
                case 'tipo':
                case 'tipo-cultivo-predio':
                case 'tipo-cultivo':
                    valores = catalogos.tipos;
                    break;
                case 'entidad-produce':
                case 'estado-predio':
                    valores = catalogos.estadosPredio;
                    break;
                case 'entidad-destino':
                case 'estado-domicilio':
                    valores = catalogos.estados;
                    break;
                case 'estado-factura':
                    valores = catalogos.estadosCultivo;
                    break;
                case 'municipio-predio':
                    valores = utils.filtraMunicipios(catalogos.municipios, dato.valor);
                    break;
                case 'tipo-empresa-comprador':
                    valores = catalogos.tiposEmpresaComprador;
                    break;
                case 'tipo-empresa-vendedor':
                    valores = catalogos.tiposEmpresaVendedor;
                    break;
                case 'tipo-precio':
                    valores = catalogos.tiposPrecio;
                    break;
                case 'tipo-identificacion-productor':
                case 'tipo-identificacion-representante':
                    valores = catalogos.tiposDocumentoIdentificacion;
                    break;
                case 'tipo-posesion-predio':
                    valores = catalogos.tiposPosesion;
                    break;
                case 'documento-posesion-predio':
                    valores = catalogos.tiposDocumentoPropia.concat(catalogos.tiposDocumentoRentada);
                    break;
                case 'regimen-hidrico-predio':
                    valores = catalogos.regimenes;
                    break;
                case 'banco-productor':
                    valores = catalogos.bancos;
                    break;
                case 'parentesco-beneficiario':
                    valores = catalogos.parentescos;
                    break;
                case 'entidad-cobertura':
                    valores = catalogos.coberturas;
                    break;
                case 'tipo-comprador':
                    valores = catalogos.tiposComprador;
                    break;
                case 'tipo-operacion':
                    valores = catalogos.tiposOperacion;
                    break;
                case 'fecha-firma':
                case 'fecha-compra':
                    params = {
                        type: 'date'
                    };
                    break;
                case 'fecha-vencimiento':
                    params = {
                        type: 'date',
                        min: fechaMinima
                    };
                    break;
                case 'numero-productores':
                    params = {
                        type: 'number',
                        min: 1,
                        max: 2000,
                        maxlength: 4
                    };
                    break;
                case 'nombre-comprador':
                case 'nombre-vendedor':
                case 'nombre-representante':
                case 'papellido-representante':
                case 'sapellido-representante':
                case 'calle-domicilio':
                case 'numero-exterior-domicilio':
                case 'numero-interior-domicilio':
                case 'localidad-domicilio':
                case 'folio-predio':
                case 'localidad-predio':
                case 'nombre-sociedad':
                    params = {
                        type: 'name-moral'
                    };
                    break;
                case 'nombre-productor':
                case 'papellido-productor':
                case 'sapellido-productor':
                case 'nombre-beneficiario':
                case 'apellidos-beneficiario':
                    params = {
                        type: 'nombre-icao',
                        textTransform: 'upper'
                    };
                    break;
                case 'rfc-comprador':
                case 'rfc-vendedor':
                    params = {
                        type: 'rfc',
                        minlength: 12,
                        maxlength: 13
                    };
                    break;
                case 'rfc-productor':
                    params = {
                        type: 'rfc-fisica'
                    };
                    break;
                case 'rfc-sociedad':
                case 'factura-rfc-sociedad':
                    params = {
                        type: 'rfc-moral'
                    };
                    break;
                case 'cantidad-contratada':
                case 'cantidad-contratada-cobertura':
                case 'superficie-total':
                case 'volumen-total':
                case 'superficie-predio':
                case 'volumen-obtenido':
                case 'rendimiento':
                case 'precio-futuro':
                case 'base-acordada':
                case 'precio-fijo':
                case 'numero':
                case 'precio-ejercicio-dolares':
                case 'precio-ejercicio-pesos':
                case 'prima-dolares':
                case 'prima-pesos':
                case 'comisiones-dolares':
                case 'comisiones-pesos':
                case 'cantidad-comprobante-pago':
                    params = {
                        type: "number",
                        min: 0
                    };
                    break;
                case 'precio-tonelada-real':
                    params = {
                        type: "number",
                        min: data.precioTonelada
                    };
                    break;
                case 'curp-productor':
                case 'curp-representante':
                case 'curp-beneficiario':
                    params = {
                        type: 'curp'
                    };
                    break;
                case 'numero-telefono':
                    params = {
                        pattern: /^\d{10}$/,
                        minlength: 10,
                        maxlength: 10
                    }
                    break;
                case 'correo-electronico':
                    params = {
                        type: 'email'
                    }
                    break;
                case 'codigo-postal-domicilio':
                    params = {
                        pattern: /^\d{5}$/,
                        minlength: 5,
                        maxlength: 5
                    }
                    break;
                case 'latitud-predio':
                case 'latitud-1-predio':
                case 'latitud-2-predio':
                case 'latitud-3-predio':
                case 'latitud-4-predio':
                    params = {
                        type: 'point',
                        min: 14.5,
                        max: 32.72
                    };
                    break;
                case 'longitud-predio':
                case 'longitud-1-predio':
                case 'longitud-2-predio':
                case 'longitud-3-predio':
                case 'longitud-4-predio':
                    params = {
                        type: 'point',
                        min: -117.2,
                        max: -86.7
                    }
                    break;
                case 'clabe-productor':
                    params = {
                        pattern: /^\d{18}$/,
                        minlength: 18,
                        maxlength: 18
                    }
                    break;
                case 'numero-cuenta-productor':
                    params = {
                        pattern: /^\d{7,14}$/,
                        minlength: 7,
                        maxlength: 14
                    }
                    break;
                    break;
            }
            switch (dato.tipo) {
                case 'catalogo':
                    $('#' + dato.clave).actualizaCombo(valores, {value: 'clave'}).prop('disabled', dato.correcto).val(dato.valor).configura(params);
                    if (dato.clave.includes('estado-')) {
                        $('#' + dato.clave).change(handlers.cambiaEstadoCorreccion);
                    }
                    break;
                case 'texto':
                    $('#' + dato.clave).prop('disabled', dato.correcto).val(dato.valor).configura(params);
                    break;
                case 'archivo':
                    if (dato.correcto) {
                        $('#' + dato.clave).prop('disabled', dato.correcto).val(dato.valor);
                    }
                    break;
            }
        }

        $('#inscripcion-datos-capturados-incorrectos tbody .valid-field').validacion();
        $('#inscripcion-datos-capturados-incorrectos tbody input.form-control-file').change(handlers.verificaTamanoArchivo);
    };

    handlers.cambiaEstadoCorreccion = function (e) {
        var v = $(e.target).val();
        var estado = v !== '0' ? $.segalmex.get(data.estadosPredio, v) : {clave: '0'};
        var municipios = [];
        if (estado.clave !== '0') {
            for (var i = 0; i < data.municipiosEdicion.length; i++) {
                var m = data.municipiosEdicion[i];
                if (m.estado.id === estado.id) {
                    municipios.push(m);
                }
            }
        }
        var id = e.target.id.split('_')[0];
        $('#' + id + '_municipio-predio').actualizaCombo(municipios, {value: 'clave'}).val('0');
    };

    utils.getCatalogos = function () {
        switch (data.tipoInscripcion) {
            case 'productores':
                return data.catalogosProductor;
            case 'facturas':
                return data.catalogosFactura;
        }
        return null;
    };

    utils.filtraMunicipios = function (municipios, clave) {
        var estado = clave.length === 5 ? clave.substring(0, 2) : clave.substring(0, 1);
        var filtrados = [];
        for (var i = 0; i < municipios.length; i++) {
            var m = municipios[i];
            if (m.estado.clave === estado) {
                filtrados.push(m);
            }
        }
        return filtrados;
    }

    utils.validaTablaCaptura = function () {
        $('#inscripcion-datos-capturados-incorrectos tbody .valid-field').limpiaErrores();

        var errores = [];
        $('#inscripcion-datos-capturados-incorrectos tbody .valid-field').valida(errores, true);

        return errores.length === 0;
    };

    utils.construyeInscripcion = function (inscripcion, tipoInscripcion) {
        switch (tipoInscripcion) {
            case 'productores':
                utils.limpiaEdicion();
                $.segalmex.arroz.inscripcion.productor.vista.muestraInscripcion(inscripcion, 'detalle-elemento');
                utils.muestraPermitidos(inscripcion);
                data.claveArchivos = inscripcion.claveArchivos;
                data.uuid = inscripcion.uuid;
                data.archivosCargados = inscripcion.archivosCargados;
                $('#div-tabla-anexos').html('');
                if ($.segalmex.common.bandejas.actual() === 'documentacion') {
                    $('#div-tabla-anexos').html(inscripcion.archivosCargados ? utils.construyeTablaAnexos(inscripcion.archivosCargados) : '');
                    $('#archivos-cargados input.form-control-file').change(handlers.verificaTamanoArchivo);
                }
                break;
            case 'facturas':
                $('#detalle-elemento').html($.segalmex.arroz.inscripcion.factura.vista.construyeInscripcion(inscripcion));
                break;
            default:
                alert('Registro desconocido');
        }
        $('#detalle-elemento-comentarios').html($.segalmex.arroz.inscripcion.vista.construyeComentarios(inscripcion.comentarios, $.segalmex.common.pagina.comun.registrado));
    };

    utils.inicializaValidaciones = function () {
        $(".valid-field").configura();

        $('#edicion-rendimiento-predio,#edicion-cantidad-contratada-cobertura').configura({
            type: 'number',
            min: 0
        });

        $('#edicion-folio-predio,#edicion-localidad-predio,#nombre-sociedad,#factura-nombre-sociedad').configura({
            type: 'name-moral'
        });
        $('#factura-rfc-sociedad,#confirmacion-factura-rfc-sociedad').configura({
            type: 'rfc-moral'
        });
        $('#edicion-volumen-predio,#edicion-superficie-predio').configura({
            type: 'number',
            min: 0.1
        });

        $('#predio-georreferencia input.valid-field').configura({
            type: 'point'
        });

        $('#latitud-predio,#latitud-1-predio,#latitud-2-predio,#latitud-3-predio,#latitud-4-predio').configura({
            min: 14.5,
            max: 32.72
        });
        $('#longitud-predio,#longitud-1-predio,#longitud-2-predio,#longitud-3-predio,#longitud-4-predio').configura({
            min: -117.2,
            max: -86.7
        });
        $('.valid-field').validacion();
    };

    //----------------------------AGREGAR PREDIOS-------------------------------
    handlers.cambiaTipoPosesion = function (e) {
        var v = $(e.target).val();
        var tipo = v !== '0' ? $.segalmex.get(data.tiposPosesion, v) : {clave: '0'};
        var tipos = [];
        switch (tipo.clave) {
            case 'propia':
                tipos = data.tiposDocumentoPropia;
                break;
            case 'rentada':
                tipos = data.tiposDocumentoRentada;
                break;
        }
        $('#edicion-documento-posesion-predio').actualizaCombo(tipos).val('0');
    };

    handlers.cambiaEstadoPredio = function (e) {
        utils.cambiaEstadoEdicion(e.target);
    };

    utils.cambiaEstadoEdicion = function (target) {
        var v = $(target).val();
        var estado = v !== '0' ? $.segalmex.get(data.estadosPredio, v) : {clave: '0'};
        var municipios = [];
        if (estado.clave !== '0') {
            for (var i = 0; i < data.municipiosEdicion.length; i++) {
                var m = data.municipiosEdicion[i];
                if (m.estado.id === estado.id) {
                    municipios.push(m);
                }
            }
        }
        $('#edicion-municipio-predio').actualizaCombo(municipios).val('0');
    };

    handlers.cambiaRendimiento = function () {
        var volumen = parseFloat($('#edicion-volumen-predio').val());
        var superficie = parseFloat($('#edicion-superficie-predio').val());
        var rendimiento = volumen / superficie;
        var rendimientoFxd = rendimiento.toFixed(3);
        $('#edicion-rendimiento-predio').val(!isNaN(rendimientoFxd) && rendimientoFxd !== Infinity ? rendimientoFxd : '');
    };

    utils.getRendimiento = function (estado, tipoCultivo) {
        for (var i = 0; i < data.rendimientos.length; i++) {
            var r = data.rendimientos[i];
            var clave = r.clave + ':';
            if (clave.includes(tipoCultivo.clave) && clave.includes(':' + estado.clave + ':')) {
                return parseFloat(r.valor);
            }
        }
        return 0;
    };

    handlers.cambiaTipoGeorreferencia = function (e) {
        e.preventDefault();
        $('#predio-georreferencia input.valid-field').limpiaErrores().val('');
        $('#menu-tabs-predios a.nav-link').removeClass('active');
        var id = e.target.id.substring('muestra-'.length);
        $(e.target).addClass('active');
        $('#' + id).show();
        switch (id) {
            case 'punto-medio-predio':
                $('#punto-medio-predio').show();
                $('#poligono-predio').hide();
                break;
            case 'poligono-predio':
                $('#poligono-predio').show();
                $('#punto-medio-predio').hide();
                break;
        }
    };

    handlers.agregaPredio = function (e) {
        $('#predios-productor-valid .valid-field').limpiaErrores();
        var errores = [];
        $('#predios-productor-valid .valid-field').valida(errores, true);
        if (errores.length > 0) {
            return;
        }
        if ($('#documentacion-predio-pdf').val() === '') {
            alert('Documentación es requerida.');
            return;
        }

        if (data.edicionPredios.length >= data.prediosPermitidos) {
            alert('Error: No se pueden agregar más predios.');
            return;
        }

        if (data.solicitado && $('#documentacion-rendimiento-solicitado-pdf').val() === '') {
            alert('Documentación permiso de rendimiento es requerido.');
            data.rendimientoSolValido = false;
            return;
        }
        data.rendimientoSolValido = true;
        data.estadoActual = $.segalmex.get(data.estadosPredio, $("#edicion-estado-predio").val());
        var tipoCultivoActual = $.segalmex.get(data.tiposCultivo, $("#edicion-tipo-cultivo-predio").val());
        data.rendimientoMaximo = utils.getRendimiento(data.estadoActual, tipoCultivoActual);
        if (parseFloat($('#edicion-rendimiento-predio').val()) > data.rendimientoMaximo && !data.solicitado) {
            $('#documentacion-rendimiento-solicitado-pdf').val('');
            $('#rendimientoSol').html($("#edicion-rendimiento-predio").val());
            $('#rendimientoMax').html(parseFloat(data.rendimientoMaximo).toFixed(3));
            $('#edoNombre').html(data.estadoActual.nombre);
            $('#rendimineto-solicitado-modal').modal('show');
            return;
        }
        var idx = data.indexPredios + data.edicionPredios.length + 1;
        var anexoPredio = $('#documentacion-predio-pdf').clone();
        anexoPredio.attr('id', idx + '_documentacion-predio-pdf');

        if (data.solicitado) {
            var anexoSolicitado = $('#documentacion-rendimiento-solicitado-pdf').clone();
            anexoSolicitado.attr('id', idx + '_documentacion-rendimiento-solicitado-pdf');
            anexoSolicitado.idx = data.edicionPredios.length;
            data.volumenMaximo = (data.rendimientoMaximo * parseFloat($('#edicion-superficie-predio').val())).toFixed(3);
        }
        var tc = $.segalmex.get(data.tiposCultivo, $('#edicion-tipo-cultivo-predio').val());

        var predio = {
            folio: $('#edicion-folio-predio').val(),
            tipoCultivo: tc,
            tipoPosesion: {id: $('#edicion-tipo-posesion-predio').val(), nombre: $("#edicion-tipo-posesion-predio option:selected").text()},
            tipoDocumentoPosesion: {id: $('#edicion-documento-posesion-predio').val(), nombre: $("#edicion-documento-posesion-predio option:selected").text()},
            regimenHidrico: {id: $('#edicion-regimen-hidrico-predio').val(), nombre: $("#edicion-regimen-hidrico-predio option:selected").text()},
            superficie: parseFloat($("#edicion-superficie-predio").val()),
            rendimiento: data.solicitado ? parseFloat(data.rendimientoMaximo) : parseFloat($("#edicion-rendimiento-predio").val()),
            volumen: data.solicitado ? parseFloat(data.volumenMaximo) : parseFloat($("#edicion-volumen-predio").val()),
            volumenSolicitado: parseFloat($("#edicion-volumen-predio").val()),
            rendimientoSolicitado: parseFloat($("#edicion-rendimiento-predio").val()),
            solicitado: data.solicitado ? 'solicitado' : 'no-solicitado',
            estado: {id: $("#edicion-estado-predio").val(), nombre: $("#edicion-estado-predio option:selected").text()},
            municipio: {id: $('#edicion-municipio-predio').val(), nombre: $("#edicion-municipio-predio option:selected").text()},
            localidad: $('#edicion-localidad-predio').val(),
            latitud: $('#latitud-predio').val(),
            longitud: $('#longitud-predio').val(),
            latitud1: $('#latitud-1-predio').val(),
            longitud1: $('#longitud-1-predio').val(),
            latitud2: $('#latitud-2-predio').val(),
            longitud2: $('#longitud-2-predio').val(),
            latitud3: $('#latitud-3-predio').val(),
            longitud3: $('#longitud-3-predio').val(),
            latitud4: $('#latitud-4-predio').val(),
            longitud4: $('#longitud-4-predio').val()
        };
        data.edicionPredios.push(predio);
        data.edicionAnexosPredio.push(anexoPredio);
        if (data.solicitado) {
            data.anexosPrediosolicitado.push(anexoSolicitado);
        }
        utils.limpiaPredio();
        utils.construyePredios();
    };

    utils.limpiaPredio = function () {
        var disabled = $('#edicion-tipo-cultivo-predio').prop('disabled');
        var valor = $('#edicion-tipo-cultivo-predio').val();
        $('#menu-tabs-predios a.nav-link').removeClass('active');
        utils.resetPredios();
        $('#predios-productor input.valid-field').limpiaErrores().val('');
        $('#predios-productor select.valid-field').limpiaErrores().val('0');
        $('#documentacion-predio-pdf').val('');
        $('#edicion-tipo-cultivo-predio').prop('disabled', disabled);
        if (disabled) {
            $('#tipo-cultivo-predio').val(valor);
        }
        data.solicitado = false;
        data.volumenMaximo = 0;
        data.rendimientoMaximo = 0;
        data.estadoActual = null;
    };

    utils.construyePredios = function () {
        var buffer = [];
        if (data.edicionPredios.length === 0) {
            buffer.push('<tr><td class="text-center" colspan="12">No hay predios agregados.</td></tr>');
            return $('#edicion-predios-table tbody').html(buffer.join(''));
        }
        for (var i = 0; i < data.edicionPredios.length; i++) {
            var predio = data.edicionPredios[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-right">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.folio);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.tipoCultivo.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.tipoPosesion.nombre);
            buffer.push(', ');
            buffer.push(predio.tipoDocumentoPosesion.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.regimenHidrico.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.municipio.nombre);
            buffer.push(', ');
            buffer.push(predio.estado.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.localidad);
            buffer.push('</td>');
            buffer.push('<td>');
            if (predio.latitud !== '') {
                buffer.push('<a class="muestra-ubicacion" href="#">[' + predio.latitud + ',' + predio.longitud + ']</a>');
            } else {
                buffer.push('<a class="muestra-ubicacion" href="#">[' + predio.latitud1 + ',' + predio.longitud1 + ']</a>,');
                buffer.push('<a class="muestra-ubicacion" href="#">[' + predio.latitud2 + ',' + predio.longitud2 + ']</a>,');
                buffer.push('<a class="muestra-ubicacion" href="#">[' + predio.latitud3 + ',' + predio.longitud3 + ']</a>,');
                buffer.push('<a class="muestra-ubicacion" href="#">[' + predio.latitud4 + ',' + predio.longitud4 + ']</a>');
            }
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.solicitado === 'solicitado' ? (predio.volumen.toFixed(3)) + utils.setRojo(predio.volumenSolicitado.toFixed(3)) : predio.volumen.toFixed(3));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.superficie.toFixed(3));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(predio.solicitado === 'solicitado' ? (predio.rendimiento.toFixed(3) + utils.setRojo(predio.rendimientoSolicitado.toFixed(3))) : predio.rendimiento.toFixed(3));
            buffer.push('</td>');
            buffer.push('<td class="text-center"><button id="eliminar-predio-');
            buffer.push(i);
            buffer.push('" class="btn btn-danger btn-sm eliminar-predio"><i class="fa fa-minus-circle"></i></button></td>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        $('#edicion-predios-table tbody').html(buffer.join(''));
    };

    handlers.eliminaPredio = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-predio-'.length), 10);
        var predios = [];
        var anexosPredio = [];
        var anexosSolicitados = [];
        for (var i = 0; i < data.edicionPredios.length; i++) {
            if (i !== idx) {
                predios.push(data.edicionPredios[i]);
                var anexoPredio = data.edicionAnexosPredio[i];
                anexoPredio.attr('id', (data.indexPredios + anexosPredio.length + 1) + '_documentacion-predio-pdf');
                if (data.edicionPredios[i].solicitado === 'solicitado') {
                    var anexoSolicitado = utils.getAnexoPredioSolicitado(i);
                    anexoSolicitado.attr('id', (data.indexPredios + anexosPredio.length + 1) + '_documentacion-rendimiento-solicitado-pdf');
                    anexoSolicitado.idx = anexosPredio.length;
                    anexosSolicitados.push(anexoSolicitado);
                }
                anexosPredio.push(anexoPredio);
            }
        }
        data.edicionPredios = predios;
        data.edicionAnexosPredio = anexosPredio;
        data.anexosPrediosolicitado = anexosSolicitados;
        utils.construyePredios();
        utils.resetPredios();
    };

    handlers.muestraUbicacion = function (e) {
        e.preventDefault();
        var ubicacion = $(e.target).html();
        $('#maps').muestraMaps({
            ubicacion: ubicacion
        });
    };

    //--------------------------AGREGAR FACTURAS--------------------------------

    handlers.agregaSociedad = function (e) {
        $('#uso-factura-persona-moral .valid-field').limpiaErrores();
        var errores = [];
        $('#uso-factura-persona-moral .valid-field').valida(errores, true);
        if (errores.length > 0) {
            return;
        }
        var rfc = $('#factura-rfc-sociedad').val();
        var confirmacion = $('#confirmacion-factura-rfc-sociedad').val();
        if (rfc !== confirmacion) {
            alert('Error: El RFC y la confirmación no coinciden.');
            return;
        }

        if (utils.sociedadDuplicada(rfc)) {
            alert('Error: El RFC ya ha sido agregado.');
            return;
        }

        if ($('#acta-constitutiva-sociedad-pdf').val() === '') {
            alert('Acta constitutiva de la sociedad (PDF) es requerida.');
            return;
        }
        if ($('#lista-socios-pdf').val() === '') {
            alert('Lista de socios (PDF) es requerida.');
            return;
        }

        if (data.edicionSociedades.length >= data.empresasPermitidos) {
            alert('Error: No se pueden agregar más empresas.');
            return;
        }

        var idx = data.indexEmpresas + data.edicionSociedades.length;
        var anexoActaConstitutiva = $('#acta-constitutiva-sociedad-pdf').clone();
        anexoActaConstitutiva.attr('id', idx + '_acta-constitutiva-sociedad-pdf');

        var anexoListaSocios = $('#lista-socios-pdf').clone();
        anexoListaSocios.attr('id', idx + '_lista-socios-pdf');

        var sociedad = {
            rfc: rfc,
            nombre: $('#factura-nombre-sociedad').val()
        };
        data.edicionSociedades.push(sociedad);
        data.anexosActaConstitutiva.push(anexoActaConstitutiva);
        data.anexosListaSocios.push(anexoListaSocios);
        utils.limpiaSociedad();
        utils.construyeSociedades();
    };

    utils.sociedadDuplicada = function (rfc) {
        for (var i = 0; i < data.edicionSociedades.length; i++) {
            if (data.edicionSociedades[i].rfc === rfc) {
                return true;
            }
        }
        return false;
    };

    utils.limpiaSociedad = function () {
        $('#uso-factura-persona-moral input.valid-field').limpiaErrores().val('');
        $('#uso-factura-persona-moral input.form-control-file').val('');
    };

    utils.construyeSociedades = function () {
        var buffer = [];
        if (data.edicionSociedades.length === 0) {
            buffer.push('<tr><td class="text-center" colspan="6">Sin sociedades</td></tr>');
            return $('#sociedades-table tbody').html(buffer.join(''));
        }
        for (var i = 0; i < data.edicionSociedades.length; i++) {
            var sociedad = data.edicionSociedades[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-right">');
            buffer.push(i + 1);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(sociedad.nombre);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(sociedad.rfc);
            buffer.push('</td>');
            buffer.push('<td class="text-center"><button id="eliminar-sociedad-');
            buffer.push(i);
            buffer.push('" class="btn btn-danger btn-sm eliminar-sociedad"><i class="fa fa-minus-circle"></i></button></td>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        $('#sociedades-table tbody').html(buffer.join(''));
    };

    handlers.eliminaSociedad = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-sociedad-'.length), 10);
        var sociedades = [];
        var anexosActaConstitutiva = [];
        var anexosListaSocios = [];
        for (var i = 0; i < data.edicionSociedades.length; i++) {
            if (i !== idx) {
                sociedades.push(data.edicionSociedades[i]);
                var indiceEmpresas = data.indexEmpresas + anexosActaConstitutiva.length;
                var anexoActa = data.anexosActaConstitutiva[i];
                var anexoSocio = data.anexosListaSocios[i];
                anexoActa.attr('id', indiceEmpresas + '_acta-constitutiva-sociedad-pdf');
                anexoSocio.attr('id', indiceEmpresas + '_lista-socios-pdf');
                anexosActaConstitutiva.push(anexoActa);
                anexosListaSocios.push(anexoSocio);
            }
        }
        data.edicionSociedades = sociedades;
        data.anexosActaConstitutiva = anexosActaConstitutiva;
        data.anexosListaSocios = anexosListaSocios;
        utils.construyeSociedades();
    };

    utils.muestraPermitidos = function (inscripcion) {
        data.prediosPermitidos = inscripcion.numeroPredios - utils.getActivos(inscripcion.predios);
        data.empresasPermitidos = inscripcion.numeroEmpresas - utils.getActivos(inscripcion.empresas);
        $('#predios-permitidos').html(data.prediosPermitidos);
        $('#uso-facturas-permitidos').html(data.empresasPermitidos);
        utils.resetPredios();
    };

    utils.getActivos = function (registros) {
        var activos = 0;
        for (var i = 0; i < registros.length; i++) {
            var r = registros[i];
            if (!r.fechaCancelacion) {
                activos++;
            }
        }
        return activos;
    };

    //GUARDAMOS LA EDICION CON LAS NUEVAS LISTAS----------------------------
    handlers.guardaEdicion = function (e) {
        e.preventDefault();
        if (data.edicionPredios.length !== data.prediosPermitidos) {
            alert('Error: Aún no se agregan todos los predios permitidos.');
            $('#button-guarda-edicion').prop('disabled', false);
            return;
        }
        if (data.edicionSociedades.length !== data.empresasPermitidos) {
            alert('Error: Aún no se agregan todas las empresas permitidas.');
            $('#button-guarda-edicion').prop('disabled', false);
            return;
        }

        var registro = {
            predios: data.edicionPredios,
            empresas: data.edicionSociedades,
            claveArchivos: utils.getClavesArchivosEdicion()
        };

        $.ajax({
            url: '/arroz/resources/' + data.tipoInscripcion + '/maiz/inscripcion/' + data.inscripcion.uuid + '/edicion/',
            data: JSON.stringify(registro),
            type: 'PUT',
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            $('#div-carga-archivos').cargaArchivos({
                archivos: utils.getArchivosEdicion(),
                urlComprobante: '/arroz/resources/productores/maiz/inscripcion/' + data.uuid + '/inscripcion-productor.pdf',
                callBackCerrar: handlers.cierraDescarga
            });
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Ocurrió un error al editar el registro.');
            }
        });
        $('#button-guarda-edicion').prop('disabled', false);
    };

    handlers.cierraDescarga = function (e) {
        $.segalmex.common.bandejas.actualizar();
        $('.detalle-elemento-form').hide();
        $('#detalle-elemento-comentarios').html('');
        $('#lista-entradas').show();
        $('#descarga-modal').modal('hide');
    };

    utils.limpiaEdicion = function () {
        data.edicionPredios = [];
        data.edicionSociedades = [];
        data.edicionAnexosPredio = [];
        data.anexosActaConstitutiva = [];
        data.anexosListaSocios = [];
        data.anexosPrediosolicitado = [];
        utils.construyePredios();
        utils.construyeSociedades();
        utils.resetPredios();
        $('#edicion-cantidad-contratada-cobertura').val('').limpiaErrores();
        $('#predios-productor-valid input').val('').limpiaErrores();
        $('#predios-productor-valid select').val('0').limpiaErrores();
        $('#uso-factura-persona-moral input').val('').limpiaErrores();
    };

    utils.construyeTablaAnexos = function (anexos) {
        var buffer = `
                <h4>Archivos anexos</h4>
                <div class="alert alert-info">
                <p>La siguiente tabla muestra los anexos que debe llevar el registro:</p>
                <ul class="mb-1">
                  <li><strong class="text-success"><i class="fas fa-check-circle"></i></strong> Archivo registrado en sistema</li>
                  <li><strong class="text-danger"><i class="fas fa-times-circle"></i></strong> Archivo pendiente de subir a sistema</li>
                </ul>
                </div>
                <div class="table-responsive">
                <table id="archivos-cargados" class="table table-bordered table-striped">
                <thead class="thead-dark"><tr>
                <th>#</th>
                <th>Nombre</th>
                <th>Estatus</th>
                </tr></thead>
                <tbody>
        `;

        for (var i = 0; i < anexos.length; i++) {
            var anexo = anexos[i];
            buffer += `
                <tr>
                <td>${i + 1}</td>
                <td>
                <label for="${anexo.tipo}-pdf" class="control-label">${utils.getNombreArchivo(anexo.tipo)}</label>
                <input id="${anexo.tipo}-pdf" type="file" class="form-control-file" accept="application/pdf"/>
                ${anexo.cargado ?
                    '<a href="/arroz/resources/archivos/arroz/' + anexo.uuid + '/' + anexo.nombre + '?inline=true" target="_blank" > ' + anexo.nombre + '</a>'
                    : '' }
                </td>
                <td>${anexo.cargado ? '<strong class="text-success"><i class="fas fa-check-circle"></i> Enviado</strong></td>'
                    : '<strong class="text-danger"><i class="fas fa-times-circle"></i> Pendiente</strong></td>'}
                </tr>
            `;
        }
        buffer += `<tbody>
                  </table>
                  </div><br/>
        `;
        return buffer;
    };

    utils.getNombreArchivo = function (archivo) {
        var nombreArchivo = '';
        switch (utils.clasifica(archivo)) {
            case "curp-productor":
                nombreArchivo = 'CURP del productor (PDF)';
                break;
            case "rfc-productor":
                nombreArchivo = 'RFC del productor (PDF)';
                break;
            case "documento-identificacion-productor":
                nombreArchivo = 'Documento de identificación (PDF)';
                break;
            case "datos-bancarios":
                nombreArchivo = ' Datos bancarios (PDF)';
                break;
            case "documentacion-predio":
                var n = archivo.split('_')[0];
                nombreArchivo = 'Documentación predio (PDF) [' + n + ']';
                break;
            case "acta-constitutiva-sociedad":
                var n = archivo.split('_')[0];
                var c = parseInt(n) + 1;
                nombreArchivo = 'Acta constitutiva (PDF) [' + c + ']';
                break;
            case "lista-socios":
                var n = archivo.split('_')[0];
                var c = parseInt(n) + 1;
                nombreArchivo = 'Lista de socios (PDF) [' + c + ']';
                break;
            case "documentacion-rendimiento-solicitado":
                var n = archivo.split('_')[0];
                nombreArchivo = 'Documentación permiso rendimiento superior (PDF) [' + n + ']';
                break;
            default :
                nombreArchivo = archivo;
        }
        return nombreArchivo;
    };

    utils.clasifica = function (nombreArchivo) {
        if (nombreArchivo.includes('documentacion-predio')) {
            return 'documentacion-predio';
        }
        if (nombreArchivo.includes('acta-constitutiva-sociedad')) {
            return 'acta-constitutiva-sociedad';
        }
        if (nombreArchivo.includes('lista-socios')) {
            return 'lista-socios';
        }
        if (nombreArchivo.includes('documentacion-rendimiento-solicitado')) {
            return 'documentacion-rendimiento-solicitado';
        }
        return nombreArchivo;
    };

    utils.getArchivos = function (archivos) {
        var fds = [];
        // Agregamos los archivos faltantes o que se reemplazaran.
        for (var i = 0; i < archivos.length; i++) {
            var file = archivos[i];
            var id = file.id;
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/arroz/resources/productores/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            var archivo = $('#' + id)[0].files[0];
            fd.append('file', archivo);
            var etiqueta = $('label[for=' + file.id + ']').html();
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }
        return fds;
    };
    //Vamos agregando lo nuevo de InscripcionProductor
    //cambiamos a tabs la georreferencia de los predios

    utils.resetPredios = function () {
        $('#muestra-punto-medio-predio').addClass('active');
        $('#muestra-punto-medio-predio').click();
    };

    //cargamos los catalogos para la edicion del registro de a cuerdo a su ciclo
    utils.cargaCatalogosProductor = function (bandeja, inscripcion) {
        $.ajax({
            type: 'GET',
            url: '/arroz/resources/paginas/inscripcion-productor/edicion/' + inscripcion.ciclo.clave,
            dataType: 'json'
        }).done(function (response) {
            data.indexPredios = inscripcion.predios.length;
            data.indexEmpresas = inscripcion.empresas.length;
            data.catalogosProductor = response;
            data.estadosPredio = response.estadosPredio;
            data.tiposPosesion = response.tiposPosesion;
            data.tiposDocumentoPropia = response.tiposDocumentoPropia;
            data.tiposDocumentoRentada = response.tiposDocumentoRentada;
            data.municipiosEdicion = response.municipios;
            $('#edicion-tipo-cultivo-predio').actualizaCombo(response.tipos);
            $('#edicion-tipo-posesion-predio').actualizaCombo(response.tiposPosesion);
            $('#edicion-regimen-hidrico-predio').actualizaCombo(response.regimenes);
            $('#edicion-estado-predio').actualizaCombo(response.estadosPredio);
            data.volumenPredio = parseFloat(response.volumenPredio.valor);
            data.superficiePredio = parseFloat(response.superficiePredio.valor);
            utils.actualizaMaximosPredio(inscripcion.predios);
            data.rendimientos = response.rendimientos;
            utils.configuraBandeja(bandeja, inscripcion);
        }).fail(function () {
            alert('Error: Al descargar los recursos para la edición.');
        });
    };

    //Métodos para confirmar/cancelar rendimiento solicitado.
    utils.confirmaRendimientoSolicitado = function () {
        data.solicitado = true;
        handlers.agregaPredio();
        if (data.rendimientoSolValido) {
            $('#rendimineto-solicitado-modal').modal('hide');
        }
    };

    utils.cancelarRendimientoSolicitado = function () {
        data.solicitado = false;
        data.rendimientoSolValido = false;
        $('#rendimineto-solicitado-modal').modal('hide');
    };

    //actualizamos los valores maximos de acuerdo a los predios que ya estaban agregados
    utils.actualizaMaximosPredio = function (predios) {
        var volumenTotal = 0.0;
        var superficieTotal = 0.0;
        for (var i = 0; i < predios.length; i++) {
            var p = predios[i];
            volumenTotal += parseFloat(p.volumen);
            superficieTotal += parseFloat(p.superficie);
        }
        data.volumenPredio = data.volumenPredio - volumenTotal;
        data.superficiePredio = data.superficiePredio - superficieTotal;
    };

    utils.setRojo = function (val) {
        return '\n<p style="color:Red;"> (' + val + ')</p>';
    };

    utils.getAnexoPredioSolicitado = function (idx) {
        for (var i = 0; i < data.anexosPrediosolicitado.length; i++) {
            var anexo = data.anexosPrediosolicitado[i];
            if (anexo.idx === idx) {
                return anexo;
            }
        }
    };

    utils.getClavesArchivosEdicion = function () {
        var archivos = utils.getArchivosEdicion();
        var claves = data.claveArchivos.split(',');
        for (var i = 0; i < archivos.length; i++) {
            var file = archivos[i];
            var id = file.id;
            var tipo = id.substring(0, id.length - '-pdf'.length);
            claves.push(tipo);
        }
        return claves.join(',');
    };

    utils.getArchivosEdicion = function () {
        var fds = [];
        // Agregamos archivos predios.
        for (var i = 0; i < data.edicionAnexosPredio.length; i++) {
            var anexo = data.edicionAnexosPredio[i];
            var id = anexo.attr('id');
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/arroz/resources/productores/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            var etiqueta = 'Documentación predio (PDF) [' + (i + 1) + ']';
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }

        // Agregamos archivos actas constitutivas.
        for (i = 0; i < data.anexosActaConstitutiva.length; i++) {
            var anexo = data.anexosActaConstitutiva[i];
            var id = anexo.attr('id');
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/arroz/resources/productores/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            var etiqueta = 'Acta constitutiva (PDF) [' + (i + 1) + ']';
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }

        // Agregamos archivos lista de socios.
        for (i = 0; i < data.anexosListaSocios.length; i++) {
            var anexo = data.anexosListaSocios[i];
            var id = anexo.attr('id');
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/arroz/resources/productores/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            var etiqueta = 'Lista de socios (PDF) [' + (i + 1) + ']';
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }

        // Agregamos archivos de maximos solicitados.
        for (i = 0; i < data.anexosPrediosolicitado.length; i++) {
            var anexo = data.anexosPrediosolicitado[i];
            var id = anexo.attr('id');
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/arroz/resources/productores/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            var etiqueta = 'Documentación permiso rendimiento superior (PDF) [' + (i + 1) + ']';
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }

        return fds;
    };

    handlers.muestraCorrectos = function (e) {
        e.preventDefault();
        if ($('#div-datos-capturados-correctos').is(":visible")) {
            $('#btn-mostrar-correctos').html('Mostrar <span class="fas fa-check-circle text-secondary" aria-hidden="true">');
            $('#div-datos-capturados-correctos').hide();
        } else {
            $('#btn-mostrar-correctos').html('Ocultar <span class="fas fa-check-circle text-secondary" aria-hidden="true">');
            $('#div-datos-capturados-correctos').show();
        }
    };

})(jQuery);