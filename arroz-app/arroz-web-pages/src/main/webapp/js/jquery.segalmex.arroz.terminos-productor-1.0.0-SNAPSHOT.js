/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($) {
    $.segalmex.namespace('segalmex.arroz.terminos.productor');

    var utils = {};
    var handlers = {};
    
    $.segalmex.arroz.terminos.productor.getTerminos = function (clave){
        var terminos = '';
        switch (clave) {
            case 'oi-2022':
                terminos =
                `<p>A través del presente registro, autorizo que los predios referidos e información productiva
                        y personal, sean considerados en el Programa de Precios de Garantía a Productos Alimentarios
                        Básicos. Me obligo a proporcionar la información y documentación que me sea requerida
                        por la SADER-SEGALMEX y a notificar cualquier cambio que sufra la información
                        proporcionada. A la vez, manifiesto bajo protesta de, decir la verdad que los datos
                        contenidos en este registro son ciertos. Acepto la responsabilidad, en la veracidad
                        de la información proporcionada a SEGALMEX. En caso de incumplimiento total o parcial, así
                        como de SIMULACIÓN, me comprometo a devolver sin reserva alguna el incentivo recibido y
                        a aceptar la sanción administrativa que conforme a derecho proceda. Soy consciente que, en
                        caso de alteración de documentación y firmas, mi registro será cancelado. También me
                        comprometo a que en caso de que me realicen algún pago en DEMASÍA, lo reintegraré completamente,
                        a través de un depósito bancario.</p>
                <p>Estoy de acuerdo en beneficiarme con el precio de
                        Garantía del ciclo vigente, publicados en el Diario Oficial de la federación 2022 y la
                        mecánica operativa de Arroz del ciclo correspondiente. Envío mi registro que ampara el
                        total de toneladas de arroz (grano o semilla certificada) que obtendré en la cosecha del
                        ciclo mencionado, y estarán reflejadas en el sistema SEGALMEX. Lo anterior también
                        aplica para semilla de arroz certificada por el SNICS. Los datos personales recabados
                        serán protegidos, incorporados y tratados en el marco de la Ley General de Protección
                        de Documentos Personales en Posesión de Sujetos Obligados, con fundamento en los
                        artículos 3, 27 y 28.</p>`;
                break;
            case 'pv-2021':
                terminos =
                `<p>
                    A través del presente registro, autorizo que los predios referidos e información productiva y personal, sean considerados
                    en el Programa de Precios de Garantía a Productos Alimentarios Básicos. Me obligo a proporcionar la información y documentación
                    que me sea requerida por la SADER-SEGALMEX y a notificar cualquier cambio que sufra la información proporcionada.
                    A la vez, manifiesto bajo protesta de, decir la verdad que los datos contenidos en este registro son ciertos.
                    Acepto la responsabilidad, en la veracidad de la información proporcionada a SEGALMEX. En caso de incumplimiento total o parcial,
                    así como de SIMULACIÓN, me comprometo a devolver sin reserva alguna el incentivo recibido y a aceptar la sanción administrativa
                    que conforme a derecho proceda. Soy consciente que, en caso de alteración de documentación y firmas, mi registro será cancelado.
                    También me comprometo a que en caso de que me realicen algún pago en DEMASÍA, lo reintegraré completamente, a través de un
                    depósito bancario. Estoy de acuerdo en beneficiarme con el precio de Garantía del ciclo agrícola vigente, publicados en el
                    Diario Oficial de la federación 2022 y la mecánica operativa de Arroz del ciclo correspondiente. Envío mi registro que ampara
                    el total de toneladas de arroz (grano o semilla certificada) que obtendré en la cosecha del ciclo mencionado, y estarán
                    reflejadas en el sistema SEGALMEX. Existen precios de garantía diferenciados de acuerdo a la superficie (pequeños y medianos).
                    Al productor pequeño, de hasta 8 hectáreas, se le apoyarán máximo 80 ton. Al productor mediano se le incentivarán máximo 300 toneladas.
                    En medianos, de 1 a 120 toneladas recibiré el incentivo completo (100%), y de 180 toneladas adicionales podré recibir el 50%
                    del incentivo. Lo anterior también aplica para semilla de arroz certificada por el SNICS
                </p>
                <p>
                    Los datos personales recabados serán protegidos, incorporados y tratados en el marco de la Ley General de
                    Protección de Documentos Personales en Posesión de Sujetos Obligados, con fundamento en los artículos 3, 27 y 28.
                </p>`;
                break;
            default:
                terminos =
                        `<p>A través del presente registro, autorizo que los predios referidos sean considerados en el Programa de Precios de Garantía a Productos Alimentarios Básicos.
                        Me obligo a proporcionar la información y/o documentación que me sea requerida por la SADER-SEGALMEX y a notificar cualquier cambio que sufra la información proporcionada.
                        A la vez, manifiesto <strong>bajo protesta de decir la verdad</strong> que los datos contenidos en este registro son ciertos y reales, por lo que acepto mi responsabilidad,
                        en la veracidad de la información proporcionada a SEGALMEX. En caso de incumplimiento total o parcial, así como de <strong>SIMULACIÓN</strong>, me comprometo a devolver
                        sin reserva alguna el incentivo recibido y a aceptar la sanción administrativa que conforme a derecho proceda. Soy consciente que, en caso de alteración de documentación
                        y/o firmas, mi registro será cancelado. También me comprometo a que en caso de que me realicen algún <strong>PAGO EN DEMASÍA</strong>, lo reintegraré completamente, a través de un depósito bancario.
                        Estoy de acuerdo en beneficiarme con el Precio de Garantía del ciclo agrícola <a class="nombre-ciclo">nombre-ciclo</a>, publicados en el Diario Oficial de la federación 2020
                        y la mecánica operativa de arroz <a class="nombre-ciclo">nombre-ciclo</a>. Envío mi registro que ampara el total de toneladas de <strong>arroz</strong> que obtendré en la cosecha del ciclo mencionado,
                        y estarán reflejadas en el sistema SEGALMEX, cuyo monto del incentivo es la diferencia entre el Precio de Garantía de <strong>$6,120</strong> por tonelada y el precio de mercado establecido
                        como referencia por SEGALMEX. Tengo en conocimiento que de hasta 120 toneladas recibiré el incentivo completo(100 %), y de 180 toneladas adicionales a las primeras 120 toneladas
                        podré recibir el 50% del incentivo.</p>
                        <p>Los datos personales recabados serán protegidos, incorporados y tratados en el marco de la Ley General de Protección de Documentos Personales en
                        Posesión de Sujetos Obligados, con fundamento en los artículos 3, 27 y 28.</p>
                        `;
        }
        return terminos;
    };
    
 })(jQuery);

