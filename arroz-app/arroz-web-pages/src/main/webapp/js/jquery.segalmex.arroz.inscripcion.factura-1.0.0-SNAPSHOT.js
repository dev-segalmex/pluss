/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {
    $.segalmex.namespace('segalmex.arroz.inscripcion.factura');
    var data = {
        comprobantes: [],
        anexosComprobantes: [],
        totalToneladas: 0,
        totalVolumenComprobantes: 0,
        limiteArchivo: 24 * 1024 * 1024,
        faltaComprobantes: false,
        precioTonelada: 0.0,
        aplicaAjuste: false,
        fechaValidacionNayarit: '2021-06-12',
        claveNayarit: '18'
    };
    var handlers = {};
    var utils = {};

    $.segalmex.arroz.inscripcion.factura.init = function () {
        utils.cargaCatalogos();
        utils.inicializaValidaciones();
        $('#tipo-factura').change(handlers.cambiaTipoFactura);
        $('#adjuntar-button').click(handlers.adjunta);
        $('#regresar-button').click(handlers.regresa);
        $('#guardar-button').click(handlers.guarda);
        $('#cancelar-modal-button').click(handlers.cancelaModal);
        $('#limpiar-button').click(utils.limpiar);
        $('#facturas-relacionadas-global table tbody').on('click', 'input:checkbox', handlers.seleccionaFactura);
        $('#facturas-relacionadas-global table #todas-checkbox').click(handlers.seleccionaTodos);
        $('#agregar-comprobante-button').click(utils.agregaComprobante);
        $('#comprobante-recepcion-pdf,#cfdi-pdf,#comprobante-pago-pdf,#factura-xml').change(handlers.verificaTamanoArchivo);
        $('#div-table-comprobantes').html(utils.creaTablaComprobantes());
        $('#cancelar-comprobantes').click(utils.cancelaComprobantes);
        $('#aceptar-comprobantes').click(utils.aceptaComprobantes);
        $('#precio-tonelada-real').change(utils.calculaPrecioAjuste);
        $('#tipo-cultivo').change(utils.cambiaRegion);
        $('#estado-factura').change(utils.configuraFechas);
        $('#tipo-cultivo').change(utils.muestraWarComprobante);
        $('#estado-factura').change(utils.muestraWarComprobante);
    };

    handlers.cambiaTipoFactura = function (e) {
        var val = $(e.target).val();
        $('#mostrar-facturas').val('false').prop('disabled', true);
        switch (val) {
            case 'global':
                $('#mostrar-facturas').prop('disabled', false);
                break;
        }
    };

    handlers.adjunta = function (e) {
        var tipo = $('#tipo-factura').val();
        if (tipo === '0') {
            alert('Error:\n\n * Tipo de factura es requerido.');
            return;
        }

        if ($('#factura-xml').val() === '') {
            alert('Error:\n\n * CFDI (XML) es requerido.');
            return;
        }

        var fd = new FormData();
        var archivo = $('#factura-xml')[0].files[0];
        fd.append('file', archivo);

        $.ajax({
            url: '/arroz/resources/facturas/cfdi/' + tipo + '?sucursales=' + $('#mostrar-facturas').val(),
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false
        }).done(function (response) {
            data.totalToneladas = parseFloat(response.totalToneladas).toFixed(3);
            data.precioTonelada = response.precioTonelada ? response.precioTonelada : 0;
            utils.muestraCfdi(response);
            data.cfdi = response;
            // Limpiamos uuid y registrada
            data.uuid = null;
            data.registrada = null;
            utils.configuraPrecioTonelada(data.aplicaAjuste);
        }).fail(function (jqXHR) {
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Error: No se pudo agregar la factura, verifique el xml.');
            }
        });
    };

    handlers.regresa = function (e) {
        $('#archivo-xml-factura').show();
        $('#datos-factura').hide();
        $('#factura-invalida').hide();
        utils.limpiar();
    };

    handlers.seleccionaFactura = function (e) {
        $('#todas-checkbox').prop('checked', false);
        var toneladas = 0;
        $('#facturas-relacionadas-global table tbody input:checked').each(function () {
            toneladas += parseFloat($(this).val());
        });
        $('#total-toneladas').val(toneladas.toFixed(3));
    }

    handlers.seleccionaTodos = function (e) {
        var checked = $(e.target).is(':checked');
        $('#facturas-relacionadas-global table tbody input:checkbox').prop('checked', checked);
        var toneladas = 0;
        $('#facturas-relacionadas-global table tbody input:checked').each(function () {
            toneladas += parseFloat($(this).val());
        });
        $('#total-toneladas').val(toneladas.toFixed(3));
    };

    handlers.guarda = function (e) {
        $('#factura-persona-moral .valid-field,#fecha-pago,#tipo-cultivo,#estado-factura,#cantidad-comprobantes,#cantidad-comprobante-pago,#precio-tonelada-real').limpiaErrores();
        var errores = [];
        $('#factura-persona-moral .valid-field,#fecha-pago,#tipo-cultivo,#estado-factura,#cantidad-comprobantes,#cantidad-comprobante-pago,#precio-tonelada-real').valida(errores, true);
        $('#guardar-button').html("Guardando...").prop('disabled', true);
        if (errores.length > 0) {
            $('#guardar-button').html("Guardar").prop('disabled', false);
            return;
        }
        if (data.cfdi.totalToneladas === undefined) {
            alert('Error: La unidad de los conceptos del CFDI no está en Toneladas o Kilogramos.');
            $('#guardar-button').html("Guardar").prop('disabled', false);
            return;
        }

        if (data.comprobantes.length === 0) {
            alert('Error: Es necesario agregar al menos un comprobante de recepción.');
            $('#guardar-button').html("Guardar").prop('disabled', false);
            return;
        }

        var comprobantes = parseInt($('#cantidad-comprobantes').val(), 10);
        if (comprobantes !== data.comprobantes.length) {
            alert('Error: La cantidad de comprobantes es diferente a los comprobantes agregados.');
            $('#guardar-button').html("Guardar").prop('disabled', false);
            return;
        }

        var inscripcion = {
            cfdi: {uuid: data.cfdi.uuid}
        };
        if (data.uuid) {
            inscripcion.uuid = data.uuid;
        }

        if (data.cfdi.rfcReceptor === 'XAXX010101000') {
            alert('Error: El RFC del receptor es de venta al público.');
            $('#guardar-button').html("Guardar").prop('disabled', false);
            return;
        }

        if ($.segalmex.archivos.validaArchivos('#div-documentos input.form-control-file:enabled,#div-comprobante-pago input.form-control-file:enabled')) {
            $('#guardar-button').html("Guardar").prop('disabled', false);
            return;
        }

        if (data.comprobantes.length > 0) {
            var totalRedondeo = data.totalVolumenComprobantes.toFixed(3);
            var total = parseFloat(totalRedondeo);
            if (total > parseFloat(data.totalToneladas).toFixed(3)) {
                alert('Error: El volumen comprobado (' + utils.format(parseFloat(data.totalVolumenComprobantes), 3) +
                        ') es mayor a la cantidad total de los conceptos ('
                        + utils.format(parseFloat(data.totalToneladas), 3) + ').');
                $('#guardar-button').html("Guardar").prop('disabled', false);
                return;
            }
            if (total < parseFloat(data.totalToneladas) && !data.faltaComprobantes) {
                $('#comprobantes-modal').modal('show');
                $('#guardar-button').html("Guardar").prop('disabled', false);
                return;
            }
        }
        if (parseFloat($('#cantidad-comprobante-pago').val()) === 0) {
            alert('Error: La cantidad del comprobante de pago no puede ser igual a 0');
            $('#guardar-button').html("Guardar").prop('disabled', false);
            return;
        }

        switch (data.cfdi.tipo) {
            case 'global':
                utils.generaInscripcionFacturaGlobal(data.cfdi, inscripcion);
                break;
            case 'productor':
                utils.generaInscripcionFacturaProductor(data.cfdi, inscripcion);
                break;
            default:
                throw 'Error en tipo de CFDI';
        }
    };

    handlers.verificaTamanoArchivo = function (e) {
        var extension = e.target.id.includes('xml') ? 'xml' : 'pdf';
        $.segalmex.archivos.verificaArchivo(e, extension, data.limiteArchivo);
    };

    utils.generaInscripcionFacturaProductor = function (cfdi, inscripcion) {

        switch (cfdi.rfcEmisor.length) {
            case 12: // Factura emitida por una persona moral
                if (data.cfdi.totalToneladas && parseFloat($('#cantidad-factura').val()) > parseFloat(data.cfdi.totalToneladas)) {
                    alert('Error: La cantidad seleccionada (' + utils.format(parseFloat($('#cantidad-factura').val()), 3) +
                            ') es mayor que el total de la factura ('
                            + utils.format(parseFloat(data.cfdi.totalToneladas), 3) + ').');
                    $('#guardar-button').html("Guardar").prop('disabled', false);
                    return;
                }
                inscripcion.cantidad = $('#cantidad-factura').val();
                inscripcion.asociado = {id: $('#asociado').val()};
                break;
            case 13: // Factura emitida por una persona fisica
                inscripcion.productorCiclo = {};
                inscripcion.productorCiclo.productor = {rfc: cfdi.rfcEmisor};
                break;
            default:
                alert('Error: El RFC es incorrecto.');
                $('#guardar-button').html("Guardar").prop('disabled', false);
                return;

        }
        inscripcion.cfdi = {uuid: cfdi.uuid};
        inscripcion.tipoCultivo = {id: $('#tipo-cultivo').val()};
        inscripcion.region = {id: $('#estado-factura').val()};
        inscripcion.fechaPago = $.segalmex.date.fechaToIso($('#fecha-pago').val());
        inscripcion.comprobantes = data.comprobantes;
        inscripcion.numeroComprobantes = data.comprobantes.length;
        inscripcion.cantidadComprobada = data.totalVolumenComprobantes;
        inscripcion.cantidadComprobantePago = $('#cantidad-comprobante-pago').val();
        inscripcion.precioToneladaReal = $('#precio-tonelada-real').val();
        utils.enviaInscripcion(inscripcion, 'productor');
    };

    utils.generaInscripcionFacturaGlobal = function (cfdi, inscripcion) {
        var seleccionados = $('#facturas-relacionadas-global table tbody input:checked');
        if (seleccionados.length === 0) {
            alert('Error: Es necesario seleccionar al menos una factura.');
            return;
        }
        var facturas = [];
        seleccionados.each(function () {
            var uuid = this.id.substring(0, this.id.length - '-factura-check'.length);
            facturas.push(uuid);
        });

        var total = parseFloat($('#total-toneladas').val());
        if (total > cfdi.totalToneladas) {
            alert('Error: El total de toneladas de la factura global es menor al total de las facturas de productores seleccionadas.');
            return;
        }

        switch (cfdi.rfcReceptor.length) {
            case 12:
            case 13:
                inscripcion.cfdi = {uuid: cfdi.uuid};
                inscripcion.facturasUuid = facturas;
                utils.enviaInscripcion(inscripcion, 'global');
                break
            default:
                alert('Error: El RFC es incorrecto.');
        }
    };

    utils.enviaInscripcion = function (inscripcion, tipo) {
        inscripcion.ciclo = {id: $("#ciclo-agricola").val()};
        inscripcion.claveArchivos = utils.getClavesArchivos();
        $.ajax({
            type: 'POST',
            url: '/arroz/resources/facturas/maiz/inscripcion/' + tipo,
            data: JSON.stringify(inscripcion),
            dataType: 'json',
            contentType: 'application/json'
        }).done(function (response) {
            data.uuid = response.uuid;
            data.registrada = response.registrada;
            data.totalToneladas = 0;
            $('#carga-archivos').cargaArchivos({
                archivos: utils.getArchivos(),
                callBackCerrar: utils.cierraPlugin,
                btnDescarga: false,
                indicaciones: `Por favor espere mientras se anexan los documentos al registro con folio: <strong>${response.folio}.</strong>`
            });
            $('#guardar-button').html("Guardar").prop('disabled', false);
        }).fail(function (jqXHR) {
            $('#guardar-button').html("Guardar").prop('disabled', false);
            if (jqXHR.responseJSON) {
                alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
            } else {
                alert('Error: La factura no se pudo registrar.');
            }

        });
    };

    utils.getClavesArchivos = function () {
        $('#factura-xml').prop('disabled', true);
        var archivos = $('input.form-control-file:enabled');
        $('#factura-xml').prop('disabled', false);
        var claves = [];
        for (var i = 0; i < archivos.length; i++) {
            var file = archivos[i];
            var id = file.id;
            var tipo = id.substring(0, id.length - '-pdf'.length);
            claves.push(tipo);
        }
        return claves.join(',');
    };

    utils.getArchivos = function () {
        $('#factura-xml,#comprobante-recepcion-pdf').prop('disabled', true);
        var files = $('input.form-control-file:enabled');
        $('#factura-xml,#comprobante-recepcion-pdf').prop('disabled', false);

        var fds = [];
        // Agregamos archivos anexos.
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            var id = file.id;
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/arroz/resources/facturas/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            var archivo = $('#' + id)[0].files[0];
            fd.append('file', archivo);
            var etiqueta = $('label[for=' + file.id + ']').html();
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }

        for (i = 0; i < data.anexosComprobantes.length; i++) {
            var anexo = data.anexosComprobantes[i];
            var id = anexo.attr('id');
            var tipo = id.substring(0, id.length - '-pdf'.length);
            var url = '/arroz/resources/facturas/maiz/inscripcion/' + data.uuid + '/anexos/' + tipo + '/pdf/';
            var fd = new FormData();
            fd.append('file', anexo[0].files[0]);
            var etiqueta = 'Comprobante recepción (PDF) [' + (i + 1) + ']';
            fds.push({id: id, fd: fd, url: url, etiqueta: etiqueta});
        }

        return  fds;

    };

    utils.cierraPlugin = function () {
        if (data.registrada) {
            alert('Advertencia: La factura ya estaba registrada previamente.');
        }
        $('#datos-factura').hide();
        $('#archivo-xml-factura').show();
        utils.limpiar();
    };

    utils.cargaCatalogos = function () {
        $.ajax({
            type: 'GET',
            url: '/arroz/resources/paginas/inscripcion-factura/',
            dataType: 'json'
        }).done(function (response) {
            if (response.sucursal === undefined) {
                alert('Error: No cuentas con una sucursal para registrar.');
                return;
            }

            $('#cultivo').actualizaCombo(response.cultivos).prop('disabled', true);
            $('#cultivo').val($.segalmex.get(response.cultivos, 'arroz', 'clave').id);
            $('#ciclo-agricola-global').cicloAgricola({
                ciclos: response.ciclos,
                actual: response.cicloSeleccionado,
                reload: true,
                sistema: 'arroz'
            });
            $('#entidad-sucursal').actualizaCombo(response.estados).prop('disabled', true);

            // datos de la sucursal
            $('#nombre-sucursal').val(response.sucursal.empresa.nombre);
            $('#rfc-sucursal').val(response.sucursal.empresa.rfc);
            $('#entidad-sucursal').val(response.sucursal.estado.id).prop('disabled', true);
            $('#municipio-sucursal').val(response.sucursal.municipio.nombre);
            $('#localidad-sucursal').val(response.sucursal.localidad);
            $('#tipo-cultivo').actualizaCombo(response.tipos);
            data.sucursal = response.sucursal;
            data.aplicaAjuste = response.aplicaAjusteFactura;
            data.regiones = response.regiones;
            data.tiposCultivo = response.tipos;
            data.fechaValidacion = response.fechaComprobantes;
            if (response.registroCerrado) {
                alert("El registro de facturas cerró el " + response.registroCerrado.valor);
            }
        }).fail(function () {
            alert('Error: Al descargar los recursos de la página.');
        });
    };

    utils.inicializaValidaciones = function () {
        $('.valid-field').configura();
        $('#cantidad-factura').configura({
            type: 'number',
            min: 0
        });

        var fechaMin = new Date($.segalmex.fecha);
        fechaMin.setFullYear(fechaMin.getFullYear() - 1);
        var fechaMax = new Date($.segalmex.fecha);
        $('#fecha-pago').configura({
            type: 'date',
            min: fechaMin,
            max: fechaMax,
            required: true
        });
        $('#fecha-comprobante').configura({
            type: 'date',
            required: true
        });
        $('#fecha-pago,#fecha-comprobante').datepicker({
            language: "es",
            autoclose: true,
            forceParse: false,
            format: 'dd/mm/yyyy',
            yearRange: "c-10:c"
        });

        $('#volumen-comprobante,#cantidad-comprobantes').configura({
            required: true,
            type: 'number',
            min: 0
        });

        $('#descripcion-comprobante').configura({
            required: true,
            maxlength: 127
        });

        $('#cantidad-comprobante-pago').configura({
            type: 'number',
            required: true,
            min: 0
        });

        $('.valid-field').validacion();
    };

    utils.muestraCfdi = function (cfdi) {
        $('#rfc-emisor').val(cfdi.rfcEmisor);
        $('#nombre-emisor').val(cfdi.nombreEmisor);
        $('#rfc-receptor').val(cfdi.rfcReceptor);
        $('#nombre-receptor').val(cfdi.nombreReceptor);
        $('#uuid-factura').val(cfdi.uuidTimbreFiscalDigital);
        $('#fecha-factura').val(cfdi.fecha ? cfdi.fecha.substring(0, cfdi.fecha.length - 6).replace('T', ' ') : '');
        $('#fecha-timbrado-factura').val(cfdi.fechaTimbrado ? cfdi.fechaTimbrado.substring(0, cfdi.fechaTimbrado.length - 6).replace('T', ' ') : '');
        $('#metodo-pago-factura').val(cfdi.metodoPago);
        $('#moneda-factura').val(cfdi.moneda);
        $('#total-factura').val(cfdi.total);
        $('#subtotal-factura').val(cfdi.subtotal ? cfdi.subtotal : 0);
        $('#precio-tonelada').val(data.precioTonelada);
        $('#factura-persona-moral').hide();

        var buffer = [];
        for (var i = 0; i < cfdi.conceptos.length; i++) {
            var c = cfdi.conceptos[i];
            buffer.push('<tr>');
            buffer.push('<td class="text-center">');
            buffer.push(i + 1)
            buffer.push('</td>');
            buffer.push('<td class="text-center">');
            buffer.push(c.cantidad);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(c.unidad);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(c.descripcion);
            buffer.push('</td>');
            buffer.push('<td class="text-center">');
            buffer.push(c.valorUnitario);
            buffer.push('</td>');
            buffer.push('<td class="text-center">');
            buffer.push(c.importe);
            buffer.push('</td>');
            buffer.push('<td class="text-center">');
            buffer.push(c.claveProductoServicio);
            buffer.push('</td>');
            buffer.push('<td class="text-center">');
            buffer.push(c.estatus === 'positivo'
                    ? '<i class="fas fa-check-circle text-success"></i>'
                    : '<i class="fas fa-exclamation-circle text-warning"></i>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        $('#conceptos-table tbody').html(buffer.join(''));

        $('#archivo-xml-factura').hide();
        $('#factura-xml').val(null);

        $('#guardar-button').prop('disabled', true);
        $('#div-alert .alert').hide();
        $('#fecha-pago,#tipo-cultivo,#estado-factura').prop('disabled', true);
        switch (cfdi.tipo) {
            case 'productor':
                if (!cfdi.productor && !cfdi.asociados) {
                    $('#factura-productor-no-autorizada-alert').show();
                    break;
                }
                if (cfdi.asociados) {
                    $('#asociado').actualizaCombo(cfdi.asociados, {nombre: 'label'});
                    $('#factura-persona-moral').show();
                }
                $('#fecha-pago,#tipo-cultivo,#estado-factura').prop('disabled', false);
                $('#factura-productor-alert').show();
                $('#guardar-button').prop('disabled', false);
                break;
            case 'global':
                $('#facturas-relacionadas-global table tbody').html(utils.muestraFacturasNuevas(cfdi));
                $('#factura-global-alert,#facturas-relacionadas-global').show();
                $('#guardar-button').prop('disabled', false);
                break;
            default:
                alert('Error en el tipo de factura.');

        }
        $('#datos-factura').show();
    }

    utils.muestraFacturasNuevas = function (cfdi) {
        if (!cfdi.facturas) {
            return '<tr><td colspan="7" class="text-center">Sin facturas de productores <strong>validadas</strong> para asociar a la factura global.</td></tr>';
        }

        var buffer = [];
        for (var i = 0; i < cfdi.facturas.length; i++) {
            var factura = cfdi.facturas[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push('<input type="checkbox" id="' + factura.uuid + '-factura-check" class="factura-check" value="' + factura.toneladasTotales + '"/>')
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(factura.rfcEmisor);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(factura.nombreEmisor);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(factura.uuidTimbreFiscalDigital);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push($.segalmex.date.isoToFechaHora(factura.fecha));
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(factura.toneladasTotales);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(factura.total);
            buffer.push('</td>');
            buffer.push('</tr>');
        }
        return buffer.join('');
    };

    utils.limpiar = function () {
        $('#tipo-cultivo,#estado-factura').val('0').limpiaErrores();
        $('#estado-factura').actualizaCombo([]);
        $('#mostrar-facturas').val('false').prop('disabled', true);
        $('#factura-xml,#cfdi-pdf,#cantidad-factura,#fecha-pago,' +
                '#comprobante-recepcion-pdf,#comprobante-pago-pdf').val('').limpiaErrores();
        $('#todas-checkbox').prop('checked', false);
        $('#total-toneladas').val('0.0');
        $('#precio-tonelada').val('');
        $('#precio-ajuste').val('0');
        $('#precio-tonelada-real').val('');
        $('#cantidad-comprobantes,#cantidad-comprobante-pago').limpiaErrores().val('');
        $('#facturas-relacionadas-global table tbody').html('');
        $('#facturas-relacionadas-global').hide();

        utils.reiniciaComprobantes();
        data.faltaComprobantes = false;
    };

    utils.reiniciaComprobantes = function () {
        data.comprobantes = [];
        data.anexosComprobantes = [];
        $('#div-table-comprobantes').html(utils.creaTablaComprobantes());
        utils.limpiaComprobantes();
        utils.verificaComprobantes();
    };

    utils.agregaComprobante = function () {
        $('#descripcion-comprobante,#volumen-comprobante,#fecha-comprobante').limpiaErrores();
        var errores = [];
        $('#descripcion-comprobante,#volumen-comprobante,#fecha-comprobante').valida(errores, true);
        if (errores.length > 0) {
            return;
        }
        var d = $('#comprobante-recepcion-pdf').val();
        if ($('#comprobante-recepcion-pdf').val() === '') {
            alert('Es necesario agregar el archivo para Comprobantes de recepción (PDF).');
            return;
        }

        var comprobante = {
            descripcion: $('#descripcion-comprobante').val(),
            volumen: parseFloat($('#volumen-comprobante').val()).toFixed(3),
            fechaComprobante: $.segalmex.date.fechaToIso($('#fecha-comprobante').val())

        };

        data.comprobantes.push(comprobante);
        //agregamos el PDF del comprobante de pago
        var anexoComprobante = $('#comprobante-recepcion-pdf').clone();
        anexoComprobante.attr('id', data.comprobantes.length + '_comprobante-recepcion-pdf');
        data.anexosComprobantes.push(anexoComprobante);


        $('#div-table-comprobantes').html(utils.creaTablaComprobantes());
        $('#comprobantes-table').on('click', 'button.eliminar-comprobante', handlers.eliminaComprobante);
        utils.limpiaComprobantes();
        utils.verificaComprobantes();
        utils.configuraFechas();
    };

    utils.verificaComprobantes = function () {
        if (data.comprobantes.length > 0) {
            var totalRedondeo = data.totalVolumenComprobantes.toFixed(3);
            var total = parseFloat(totalRedondeo);
            $('#comprobante-recepcion-pdf').prop('disabled', false);
            if (total < parseFloat(data.totalToneladas)) {
                $('#faltantes-alert').show();
                var resta = parseFloat(data.totalToneladas) - parseFloat(data.totalVolumenComprobantes);
                $('.faltantes').html(utils.format(resta, 3));
            } else {
                $('#faltantes-alert').hide();
                $('.faltantes').html('');
            }
        } else {
            $('#faltantes-alert').hide();
            $('.faltantes').html('');
        }
    };

    utils.creaTablaComprobantes = function () {
        var buffer = `
        <div class="table-responsive">
            <table id = "comprobantes-table" class="table table-striped table-bordered table-hover">
              <thead class="thead-dark">
                <tr>
                  <th>#</th>
                  <th>Descripción</th>
                  <th>Volumen (Toneladas)</th>
                  <th>Fecha</th>
                  <th><i class="fa fa-minus-circle"></i></th>
                </tr>
              </thead>
              <tbody>
              </tbody>`;

        data.totalVolumenComprobantes = 0;
        var a = 1;
        for (var i = 0; i < data.comprobantes.length; i++) {
            var c = data.comprobantes[i];
            buffer += `
                <tr>
                <td>${a++}</td>
                <td>${c.descripcion}</td>
                <td class="text-right">${utils.format(parseFloat(c.volumen), 3)}</td>
                <td>${c.fechaComprobante}</td>
                <td class="text-center">
                    <button id="eliminar-comprobante-${i}" class="btn btn-danger btn-sm eliminar-comprobante">
                    <i class="fa fa-minus-circle"></i></button></td>
                </tr>`;
            var volumen = parseFloat(c.volumen)
            data.totalVolumenComprobantes = parseFloat(data.totalVolumenComprobantes) + parseFloat(volumen.toFixed(3));
        }
        buffer += `
                </tbody>${data.comprobantes.length > 0 ? utils.agregaTotales(data.comprobantes) : ''}
                </table>
                </div>
                <br/>
        `;
        return buffer;
    };

    handlers.eliminaComprobante = function (e) {
        var id = e.target.id ? e.target.id : e.target.parentElement.id;
        var idx = Number.parseInt(id.substring('eliminar-comprobante-'.length), 10);
        var comprobantes = [];
        var anexos = [];
        for (var i = 0; i < data.comprobantes.length; i++) {
            if (i !== idx) {
                comprobantes.push(data.comprobantes[i]);
                var anexo = data.anexosComprobantes[i];
                anexo.attr('id', (anexos.length + 1) + '_comprobante-recepcion-pdf');
                anexos.push(anexo);
            }
        }
        data.comprobantes = comprobantes;
        data.anexosComprobantes = anexos;

        $('#div-table-comprobantes').html(utils.creaTablaComprobantes());
        $('#comprobantes-table').on('click', 'button.eliminar-comprobante', handlers.eliminaComprobante);
        utils.verificaComprobantes();
        utils.configuraFechas();
    };

    utils.limpiaComprobantes = function () {
        $('#descripcion-comprobante,#volumen-comprobante,#fecha-comprobante').val('').limpiaErrores();
        $('#comprobante-recepcion-pdf').val('');
    };

    utils.format = function (str, fix) {
        return str.toFixed(fix).toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
    };

    utils.agregaTotales = function (comprobantes) {
        var volumenTot = 0;
        for (var c in comprobantes) {
            volumenTot += parseFloat(comprobantes[c].volumen);
        }
        var buffer = [];
        buffer.push('<tfoot class="thead-dark">');
        buffer.push('<tr>');
        buffer.push('<th colspan="2">Total</th>');
        buffer.push('<th class="text-right">' + utils.format(parseFloat(volumenTot), 3) + '</th>');
        buffer.push('<th colspan="2"></th>');
        buffer.push('</tr>');
        buffer.push('</tfoot>');
        return buffer.join('');
    };

    utils.cancelaComprobantes = function () {
        data.faltaComprobantes = false;
        $('#comprobantes-modal').modal('hide');
    };


    utils.aceptaComprobantes = function () {
        $('#comprobantes-modal').modal('hide');
        data.faltaComprobantes = true;
        handlers.guarda();
    };

    utils.calculaPrecioAjuste = function () {
        var ajuste = 0.0;
        var precioReal = $('#precio-tonelada-real').val();
        ajuste = precioReal === '' ? ajuste : (parseFloat(precioReal) - parseFloat(data.precioTonelada));
        $('#precio-ajuste').val(!isNaN(ajuste) && ajuste !== Infinity ? ajuste.toFixed(3) : '');
    };

    utils.configuraPrecioTonelada = function (aplica) {
        $('#precio-tonelada-real').configura({
            type: 'number',
            min: data.precioTonelada,
            required: true
        });
        $('#precio-tonelada-real').validacion();
        if (aplica) {
            $('#precio-tonelada-real').prop('disabled', false).val('');
        } else {
            $('#precio-tonelada-real').prop('disabled', true).val(data.precioTonelada);
            $('#precio-ajuste').prop('disabled', true).val('0');
            $('#precio-tonelada-real').change();
        }
    };

    utils.cambiaRegion = function (e) {
        var v = $(e.target).val();
        var tp = v !== '0' ? $.segalmex.get(data.tiposCultivo, v) : {clave: '0'};
        var regiones = [];
        if (tp.clave !== '0') {
            for (var i = 0; i < data.regiones.length; i++) {
                var region = data.regiones[i];
                if (region.tipoCultivo.clave === tp.clave) {
                    regiones.push(region);
                }
            }
        }
        $('#estado-factura').actualizaCombo(regiones).val('0').prop('disabled', false);
    };

    utils.configuraFechas = function () {
        $('#fecha-comprobante').configura({
            min: null,
            max: null
        });
        var e = $('#estado-factura').val();
        var region = $.segalmex.get(data.regiones, e);
        var estado = region !== null ? region.estado : undefined;
        var tipoCultivo = $.segalmex.get(data.tiposCultivo, $('#tipo-cultivo').val());
        if (data.comprobantes.length > 0 && estado !== undefined && estado.clave === data.claveNayarit && tipoCultivo.clave === 'arroz-largo') {
            var fecha = $.segalmex.date.isoToFecha(data.comprobantes[0].fechaComprobante);
            var fechaValida = $.segalmex.date.isoToFecha(data.fechaValidacionNayarit);
            if (fecha > fechaValida) {
                $('#fecha-comprobante').configura({
                    min: '2021-06-13'
                });
            } else {
                $('#fecha-comprobante').configura({
                    max: '2021-06-12'
                });
            }
        }
    };

    utils.muestraWarComprobante = function () {
        $('#div-warning-primer').hide();
        var e = $('#estado-factura').val();
        var region = $.segalmex.get(data.regiones, e);
        var estado = region !== null ? region.estado : undefined;
        var tipoCultivo = $.segalmex.get(data.tiposCultivo, $('#tipo-cultivo').val());
        if (estado !== undefined && estado.clave === data.claveNayarit && tipoCultivo.clave === 'arroz-largo'){
            $('#div-warning-primer').show();
        }
    };
})(jQuery);
