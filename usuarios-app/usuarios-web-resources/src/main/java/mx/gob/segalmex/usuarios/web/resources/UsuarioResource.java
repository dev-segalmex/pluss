/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.usuarios.web.resources;

import java.util.List;
import java.util.Objects;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.seguridad.ProcesadorUsuario;
import mx.gob.segalmex.common.core.seguridad.busqueda.BuscadorPermiso;
import mx.gob.segalmex.common.core.seguridad.busqueda.BuscadorUsuario;
import mx.gob.segalmex.common.core.seguridad.busqueda.ParametrosUsuario;
import mx.gob.segalmex.pluss.modelo.personas.Persona;
import mx.gob.segalmex.pluss.modelo.personas.Sexo;
import mx.gob.segalmex.pluss.modelo.seguridad.Area;
import mx.gob.segalmex.pluss.modelo.seguridad.Permiso;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.RespuestaConverter;
import mx.gob.segalmex.common.web.modelo.seguridad.UsuarioConverter;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Slf4j
@Component
@Path("/usuario")
public class UsuarioResource {

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private ProcesadorUsuario procesadorUsuario;

    @Autowired
    private UsuarioSesionContextoHolder contextoSeguridad;

    @Autowired
    private BuscadorUsuario buscadorUsuario;

    @Autowired
    private PermisoSubResource permisoSubResource;

    @Autowired
    private BuscadorPermiso buscadorPermiso;

    @Path("/{id}/permiso")
    public PermisoSubResource getPermisoSubResource() {
        return permisoSubResource;
    }

    @GET
    @Produces({"application/xml", "application/json"})
    @Consumes({"application/json"})
    public List<UsuarioConverter> index() {
        LOGGER.debug("Buscando todos los usuarios...");
        List<Usuario> usuarios = buscadorCatalogo.busca(Usuario.class);
        return CollectionConverter.convert(UsuarioConverter.class, usuarios, 2);
    }

    @GET
    @Path("/{clave}")
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public UsuarioConverter show(@PathParam("clave") String clave) {
        if (Objects.isNull(clave)) {
            LOGGER.info("No se puede buscar usuario con clave nula.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        Usuario usuario = buscadorCatalogo.buscaElemento(Usuario.class, clave);
        return new UsuarioConverter(usuario, 3);
    }

    @GET
    @Path("/{clave}")
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public UsuarioConverter getPermisos(@PathParam("clave") String clave) {
        if (Objects.isNull(clave)) {
            LOGGER.info("No se puede buscar usuario con clave nula.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        LOGGER.debug("Buscando todos los permisos de {}", clave);
        Usuario usuario = buscadorCatalogo.buscaElemento(Usuario.class, clave);
        List<Permiso> permisos = buscadorPermiso.buscaAutorizados(usuario, null);
        UsuarioConverter uc = new UsuarioConverter(usuario, 2);
        uc.setPermisos(permisos);

        return uc;
    }

    @GET
    @Path("/{id}/administracion")
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public UsuarioConverter getUsuario(@PathParam("id") Integer id) {

        if (Objects.isNull(id)) {
            LOGGER.info("No se puede buscar usuario con id nulo.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        Usuario usuario = buscadorCatalogo.buscaElemento(Usuario.class, id);

        return new UsuarioConverter(usuario, 3);
    }

    @GET
    @Path("/administracion")
    @Produces({"application/xml", "application/json"})
    @Consumes({"application/json"})
    public List<UsuarioConverter> showUsuarios(@QueryParam("usuario") String usuario,
            @QueryParam("nombres") String nombres, @QueryParam("apellidos") String apellidos) {
        LOGGER.debug("Buscando usuarios...");

        Persona persona = new Persona();
        persona.setApellidos(apellidos);
        persona.setNombre(nombres);

        ParametrosUsuario parametrosUsuario = new ParametrosUsuario();
        parametrosUsuario.setNombreUsuario(usuario);
        parametrosUsuario.setApellidos(apellidos);
        parametrosUsuario.setNombre(nombres);

        List<Usuario> usuarios = buscadorUsuario.busca(parametrosUsuario);

        return CollectionConverter.convert(UsuarioConverter.class, usuarios, 2);
    }

    @POST
    @Consumes("application/json")
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public RespuestaConverter guardaUsuario(UsuarioConverter usuarioConverter) {
        if (Objects.isNull(usuarioConverter)) {
            LOGGER.info("No se puede crear usuarios sin datos");
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }

        RespuestaConverter respuesta = new RespuestaConverter();
        Usuario usuario = usuarioConverter.getEntity();

        try {
            Usuario existente = buscadorUsuario.buscaElemento(usuario.getClave());
            respuesta.setMensajeError("Ya existe una persona con el mismo nombre de usuario: "
                    + existente.getClave());

            return respuesta;
        } catch (EmptyResultDataAccessException exception) {
            LOGGER.info("Creando nuevo usuario...");
        }

        procesadorUsuario.guarda(usuario, usuario.getPersona());
        respuesta.setMensaje("Se guardó correctamente el usuario.");

        return respuesta;
    }

    @PUT
    @Path("/{username}")
    @Consumes("application/json")
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public RespuestaConverter actualizaUsuario(@PathParam("username") String username, UsuarioConverter usuarioActualizado) {
        if (Objects.isNull(username)) {
            LOGGER.info("No se puede actualizar un usuario sin su clave.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        if (Objects.isNull(usuarioActualizado)) {
            LOGGER.info("No se puede actualizar un usuario sin datos.");
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }
        Usuario usuarioViejo = buscadorUsuario.buscaElemento(username);

        Usuario usuarioNuevo = usuarioActualizado.getEntity();

        Persona personaNueva = usuarioNuevo.getPersona();
        personaNueva.setApellidos(usuarioNuevo.getPersona().getApellidos());
        personaNueva.setNombre(usuarioNuevo.getPersona().getNombre());
        personaNueva.setSexo(buscadorCatalogo.buscaElemento(Sexo.class, usuarioNuevo.getPersona().getSexo().getId()));
        personaNueva.setFechaNacimiento(usuarioNuevo.getPersona().getFechaNacimiento());

        usuarioViejo.setAreaUbicacion(usuarioNuevo.getAreaUbicacion());
        usuarioViejo.setPuesto(usuarioNuevo.getPuesto());

        RespuestaConverter respuesta = new RespuestaConverter();

        procesadorUsuario.actualiza(usuarioViejo, personaNueva);

        respuesta.setMensaje("Se actualizó correctamente el usuario.");
        return respuesta;
    }

    @PUT
    @Path("/password")
    @Consumes("application/json")
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public RespuestaConverter actualizaPassword(UsuarioConverter usuarioConverter) {
        Usuario usuario = contextoSeguridad.getUsuario();
        RespuestaConverter respuesta = new RespuestaConverter();
        procesadorUsuario.actualizaPassword(usuario, usuarioConverter.getPlainPasswordActual(),
                usuarioConverter.getPlainPassword());
        respuesta.setMensaje("Se actualizó correctamente la contraseña.");
        return respuesta;
    }

    @PUT
    @Path("/{clave}/password")
    @Consumes("application/json")
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public RespuestaConverter password(@PathParam("clave") String clave,
            UsuarioConverter usuarioConverter) {
        if (Objects.isNull(clave)) {
            LOGGER.info("No se puede actualizar un usuario sin su clave.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        if (Objects.isNull(usuarioConverter.getPlainPassword())) {
            LOGGER.info("No se puede establecer password nulo.");
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }

        Usuario usuario = buscadorCatalogo.buscaElemento(Usuario.class, clave);
        RespuestaConverter respuesta = new RespuestaConverter();
        procesadorUsuario.actualizaPassword(usuario, usuarioConverter.getPlainPassword());
        respuesta.setMensaje("Se actualizó correctamente la contraseña.");
        return respuesta;
    }

    @PUT
    @Path("/{clave}/bloqueado/{bloqueado}")
    @Consumes("application/json")
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public RespuestaConverter bloquea(@PathParam("clave") String clave, @PathParam("bloqueado") Boolean bloqueado) {
        if (Objects.isNull(clave)) {
            LOGGER.info("No se puede actualizar un usuario sin su clave.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        if (Objects.isNull(bloqueado)) {
            LOGGER.info("No se puede establecer estatus indeterminado.");
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }

        Usuario usuario = buscadorCatalogo.buscaElemento(Usuario.class, clave);
        RespuestaConverter respuesta = new RespuestaConverter();
        procesadorUsuario.bloquea(usuario, bloqueado);
        respuesta.setMensaje("Se ha " + (bloqueado ? "bloqueado" : "desbloqueado") + " el usuario.");
        return respuesta;
    }

    @PUT
    @Path("/area/{id}")
    @Consumes("application/json")
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public RespuestaConverter actualizaArea(@PathParam("id") Integer idArea) {
        Usuario usuario = contextoSeguridad.getUsuario();
        Area area = buscadorCatalogo.buscaElemento(Area.class, idArea);
        RespuestaConverter respuesta = new RespuestaConverter();
        procesadorUsuario.actualizaAreaUbicacion(usuario, area);
        respuesta.setMensaje("Se actualizó correctamente el área de ubicación.");
        return respuesta;
    }

}
