package mx.gob.segalmex.usuarios.web.resources.paginas;

import javax.ws.rs.Path;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Path("/paginas")
@Component
public class PaginaResource {

    @Autowired
    private UsuarioPaginaSubResource usuarioPaginaSubResource;

    @Path("/usuario")
    public UsuarioPaginaSubResource getUsuarioPaginaSubResource() {
        return usuarioPaginaSubResource;
    }

}
