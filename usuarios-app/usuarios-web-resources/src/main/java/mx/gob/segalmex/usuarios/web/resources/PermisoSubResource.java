/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.usuarios.web.resources;

import java.util.List;
import java.util.Objects;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.seguridad.ProcesadorPermiso;
import mx.gob.segalmex.common.core.seguridad.busqueda.BuscadorPermiso;
import mx.gob.segalmex.common.core.seguridad.busqueda.ParametrosPermiso;
import mx.gob.segalmex.pluss.modelo.seguridad.Permiso;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.RespuestaConverter;
import mx.gob.segalmex.common.web.modelo.seguridad.PermisoConverter;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Slf4j
@Component
public class PermisoSubResource {

    @Autowired
    private BuscadorPermiso buscadorPermiso;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private ProcesadorPermiso procesadorPermiso;

    @Autowired
    private UsuarioSesionContextoHolder contextoSeguridad;

    @GET
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public List<PermisoConverter> getPermisos(@PathParam("id") Integer id) {
        if (Objects.isNull(id)) {
            LOGGER.error("No se puede realizar la búsqueda sin usuario.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        Usuario usuario = buscadorCatalogo.buscaElemento(Usuario.class, id);
        LOGGER.debug("Buscando permisos para {}", usuario.getClave());
        List<Permiso> permisos = buscadorPermiso.buscaAutorizados(usuario, null);

        return CollectionConverter.convert(PermisoConverter.class, permisos, 2);
    }

    @POST
    @Consumes({"application/json;charset=utf-8", "application/xml"})
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public RespuestaConverter create(@PathParam("id") Integer id, PermisoConverter pc) {
        if (Objects.isNull(id)) {
            LOGGER.error("No se puede crear permiso sin usuario.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        if (Objects.isNull(pc)) {
            LOGGER.error("No se puede crear permiso con datos nulos.");
            throw new WebApplicationException(Response.Status.INTERNAL_SERVER_ERROR);
        }

        Usuario usuario = buscadorCatalogo.buscaElemento(Usuario.class, id);
        procesadorPermiso.agrega(usuario, pc.getEntity(), contextoSeguridad.getUsuario());
        RespuestaConverter respuesta = new RespuestaConverter();
        respuesta.setMensaje("Se ha agregado el permiso exitosamente.");
        return respuesta;
    }

    @DELETE
    @Path("/{idPermiso}")
    @Produces({"application/json;charset=utf-8"})
    public RespuestaConverter delete(@PathParam("id") Integer idUsuario, @PathParam("idPermiso") Integer id) {
        if (Objects.isNull(idUsuario)) {
            LOGGER.error("No se puede eliminar permiso sin usuario.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }

        if (Objects.isNull(id)) {
            LOGGER.error("No se puede eliminar un permiso nulo.");
            throw new WebApplicationException(Response.Status.NOT_FOUND);
        }
        LOGGER.debug("Eliminando permiso...");
        Usuario usuario = buscadorCatalogo.buscaElemento(Usuario.class, idUsuario);
        ParametrosPermiso parametros = new ParametrosPermiso();
        parametros.setId(id);
        parametros.setUsuario(usuario);
        Permiso permiso = buscadorPermiso.buscaElemento(parametros);
        procesadorPermiso.elimina(permiso, contextoSeguridad.getUsuario());
        RespuestaConverter respuesta = new RespuestaConverter();
        respuesta.setMensaje("El permiso se ha eliminado exitosamente.");
        return respuesta;
    }

}
