/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.usuarios.web.resources.paginas;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.seguridad.busqueda.BuscadorPermiso;
import mx.gob.segalmex.pluss.modelo.catalogos.Pais;
import mx.gob.segalmex.pluss.modelo.personas.Persona;
import mx.gob.segalmex.pluss.modelo.personas.Sexo;
import mx.gob.segalmex.pluss.modelo.seguridad.Accion;
import mx.gob.segalmex.pluss.modelo.seguridad.Area;
import mx.gob.segalmex.pluss.modelo.seguridad.Permiso;
import mx.gob.segalmex.pluss.modelo.seguridad.TipoAccionEnum;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import mx.gob.segalmex.common.web.modelo.CollectionConverter;
import mx.gob.segalmex.common.web.modelo.seguridad.AdministracionConverter;
import mx.gob.segalmex.common.web.modelo.seguridad.AdministracionUsuarioConverter;
import mx.gob.segalmex.common.web.modelo.seguridad.AreaConverter;
import mx.gob.segalmex.pluss.common.web.seguridad.UsuarioSesionContextoHolder;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Slf4j
@Component
public class UsuarioPaginaSubResource {

    @Autowired
    private BuscadorPermiso buscadorPermiso;

    @Autowired
    private UsuarioSesionContextoHolder contextoSeguridad;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @GET
    @Path("/areas")
    @Consumes("application/json")
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public List<AreaConverter> getAreas() {
        LOGGER.debug("Recuperando áreas permitidas del usuario.");
        Usuario usuarioSesion = contextoSeguridad.getUsuario();
        List<Permiso> permisos = buscadorPermiso.buscaAutorizados(usuarioSesion, null);
        Set<Area> areas = new HashSet<>();
        for (Permiso p : permisos) {
            if (Objects.nonNull(p.getArea())) {
                areas.add(p.getArea());
            }
        }
        areas.add(usuarioSesion.getAreaUbicacion());
        return CollectionConverter.convert(AreaConverter.class, areas, 2);
    }

    @GET
    @Path("/permisos")
    @Consumes("application/json")
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public AdministracionConverter getCatalogos() {
        LOGGER.debug("Recuperando catálogos para administración de permisos");
        AdministracionConverter ac = new AdministracionConverter();
        List<Accion> locales = new ArrayList<>();
        List<Accion> globales = new ArrayList<>();
        List<Accion> acciones = buscadorCatalogo.busca(Accion.class);
        for (Accion a : acciones) {
            if (StringUtils.equals(TipoAccionEnum.AMBAS.getClave(), a.getTipoAccion())) {
                locales.add(a);
                globales.add(a);
                continue;
            }
            if (StringUtils.equals(TipoAccionEnum.LOCAL.getClave(), a.getTipoAccion())) {
                locales.add(a);
                continue;
            }
            if (StringUtils.equals(TipoAccionEnum.GLOBAL.getClave(), a.getTipoAccion())) {
                globales.add(a);
            }

        }
        ac.setLocales(locales);
        ac.setGlobales(globales);
        ac.setAreas(buscadorCatalogo.busca(Area.class));
        ac.setUsuarios(simplifica(buscadorCatalogo.busca(Usuario.class)));
        return ac;
    }

    @GET
    @Path("/administracion")
    @Consumes("application/json")
    @Produces({"application/json;charset=utf-8", "application/xml"})
    public AdministracionUsuarioConverter getCatalogosAdministracion() {
        AdministracionUsuarioConverter administracionConverter = new AdministracionUsuarioConverter();
        administracionConverter.setSexo(buscadorCatalogo.busca(Sexo.class));
        administracionConverter.setArea(buscadorCatalogo.busca(Area.class));
        administracionConverter.setPais(buscadorCatalogo.busca(Pais.class));
        return administracionConverter;
    }

    private List<Usuario> simplifica(List<Usuario> usuarios) {
        List<Usuario> simplificados = new ArrayList<>();
        for (Usuario u : usuarios) {
            Usuario simplificado = new Usuario();
            simplificado.setNombre(u.getNombre());
            simplificado.setClave(u.getClave());
            simplificado.setId(u.getId());
            simplificado.setActivo(u.isActivo());

            Persona p = new Persona();
            p.setNombre(u.getPersona().getNombre());
            p.setApellidos(u.getPersona().getApellidos());
            p.setSexo(new Sexo());
            p.getSexo().setNombre(u.getPersona().getSexo().getNombre());
            simplificado.setPersona(p);

            simplificados.add(simplificado);
        }
        return simplificados;
    }
}
