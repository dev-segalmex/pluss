(function ($) {
    $.segalmex.namespace('segalmex.common.usuarios.administracion');

    var data = {
        claveUsuario: null,
        catalogos: null
    };
    var handlers = {};
    var util = {};

    $.segalmex.common.usuarios.administracion.init = function () {
        util.configuraPantalla();
        util.inicializaValidaciones();
        util.cargaCatalogos();

        $('#formularioBusqueda').keypress(function (event) {
            if (event.which == 13) {
                event.preventDefault();
                $("#buscar").click();
            }
        });
        $('#actualizarPasswordDialog').modal({show: false, backdrop: 'static', keyboard: false, modal: true});
        setTimeout(function () {
            $('#usuario').focus();
        }, 1000);
    };

    util.inicializaValidaciones = function () {

        var required = ['.valid-field'];
        $(required.join(',')).configura();

        $(required.join(',')).validacion();
    };

    util.configuraPantalla = function () {

        var hoy = new Date($.segalmex.fecha);
        var minNacimiento = new Date($.segalmex.fecha);
        minNacimiento.setFullYear(minNacimiento.getFullYear() - 100);
        $('#fechaNacimientoNuevo').configura({
            type: 'date',
            required: true,
            min: minNacimiento,
            max: hoy,
            mensajes: {min: 'el rango no puede ser mayor a 100 aÃ±os.'}
        });

        $('#fechaNacimientoNuevo').datepicker({
            language: "es",
            autoclose: true,
            forceParse: false,
            format: 'dd/mm/yyyy',
            changeYear: true,
            yearRange: "c-100:c"
        });

        util.inicializaFecha();

        var nombresPropios = ["#nombreNuevo", '#apellidosNuevo'];
        $(nombresPropios.join(",")).configura({
            type: "nombre-icao"
        });

        $("#usuario").configura({required: false, type: 'email'});
        $("#usuarioNuevo").configura({type: 'email'});
        $("#contrasena,#verificacion,#cap-contrasena,#cap-verificacion").configura({required: true, textTransform: 'none', allowSpace: false});

        var restringidos = ['#contrasena', '#verificacion', '#cap-contrasena', '#cap-verificacion'];
        $(restringidos.join(',')).bind('cut copy paste', function (e) {
            e.preventDefault();
        });

        $('#buscar').click(handlers.buscar);
        $('#regresar').click(handlers.regresar);
        $('#regresarDetalle').click(handlers.regresar);
        $('#guardar').click(handlers.guardar);
        $('#actualizar').click(handlers.actualizar);
        $('#actualizarPassword').click(handlers.actualizarPassword);
        $('#nuevoUsuario').click(handlers.nuevoUsuario);
        $('#btnVerTodo').click(util.muestraTodos);
        $('#buton-capturar-contrasena').click(util.muestraModalContrasena);
    };

    util.muestraModalContrasena = function (e) {
        e.preventDefault();
        $('#confirmarPassword').click(util.confirmaContrasena);
        $('#cap-contrasena,#cap-verificacion').val('').limpiaErrores();
        $('#capturarPasswordDialog').modal('show');
        setTimeout(function () {
            $('#cap-contrasena').focus();
        }, 500);
    };

    util.confirmaContrasena = function (e) {
        e.stopImmediatePropagation();
        $('#confirmarPassword').html('Confirmando...').prop('disabled', true);
        var errores = [];
        $('#cap-contrasena,#cap-verificacion').valida(errores, true);
        if (errores.length > 0) {
            $('#confirmarPassword').html('Confirmar').prop('disabled', false);
            return;
        }

        var password = $('#cap-contrasena').val();
        var confirmacionPassword = $('#cap-verificacion').val();
        if (password !== confirmacionPassword) {
            alert('Error: La contraseña y la verificación no coinciden.');
            $('#confirmarPassword').html('Confirmar').prop('disabled', false);
            return;
        }

        $('#contrasena-nueva').val(password);

        $('#confirmarPassword').html('Confirmar').prop('disabled', false);
        $('#capturarPasswordDialog').modal('hide');

    };

    util.inicializaFecha = function () {
        $('#fechaNacimientoNuevo').datepicker('setDate', new Date());
        $('#fechaNacimientoNuevo').datepicker('update');
        $('#fechaNacimientoNuevo').val('');
    };

    util.cargaCatalogos = function () {
        $.ajax({
            url: '/usuarios/resources/paginas/usuario/administracion',
            type: 'GET',
            dataType: 'json',
            async: false,
            contentType: 'application/json',
            success: function (response) {
                data.catalogos = response;

                $('#sexoNuevo').actualizaCombo(data.catalogos.sexo);
                $('#areaNuevo').actualizaCombo(data.catalogos.area);
                $('#paisNacimientoNuevo').actualizaCombo(data.catalogos.pais);

            },
            error: function () {
                alert('Error: No se pudieron cargar los catalogos.');
            }
        });
    };

    handlers.buscar = function () {
        $('.valid-field').limpiaErrores();
        $('#buscar').html("Buscando...").attr('disabled', 'disabled');


        if ($.trim($("#usuario").val()) === ''
                && $.trim($("#nombre").val()) === ''
                && $.trim($("#apellidos").val()) === '') {
            alert('Se debe de seleccionar al menos una opción de búsqueda');
            $('#buscar').html('Buscar').removeAttr('disabled');
            return false;
        }

        util.enviaBusqueda();
        $('#buscar').html("Buscar").removeAttr('disabled');
    };

    handlers.regresar = function () {
        $('#formularioBusqueda').get(0).reset();
        $('#botones').slideUp();
        $('#divEdicion').slideUp();
        $('#divDetalle').slideUp();
        $("#formulario").slideDown();
        $('#formularioBusqueda').slideDown();
        $('#buscar').html('Buscar').removeAttr('disabled');
        $("#nombreNuevo").removeAttr('disabled');
        $("#apellidosNuevo").removeAttr('disabled');
        $("#usuarioNuevo").removeAttr('disabled');
    };

    handlers.guardar = function () {
        $('.valid-field').limpiaErrores();
        $('#guardar').html("Guardando...").attr('disabled', 'disabled');
        var errores = [];

        $('.valid-field').valida(errores, false);

        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            $('#guardar').html("Guardar").removeAttr('disabled');
            return false;
        }

        var nuevoUsuario = util.generaUsuario();

        $.ajaxq('usuerQueue', {
            url: '/usuarios/resources/usuario/',
            type: 'POST',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(nuevoUsuario)
        }).done(function (response, status) {

            if (nuevoUsuario.plainPassword) {
                util.guardaContrasenia(nuevoUsuario.clave, nuevoUsuario);
            }

            if (response.mensajeError) {
                $('#guardar').html('Guardar').removeAttr('disabled');
                alert(response.mensajeError);

            } else {
                $('#guardar').html('Guardar').removeAttr('disabled');
                alert(response.mensaje);
                handlers.regresar();
            }

        }).fail(function (data, status) {
            alert("Ocurrió un error al guardar el usuario");
            $('#guardar').html('Guardar').removeAttr('disabled');
        });
    };

    util.guardaContrasenia = function (correo, usuario) {
        $.ajaxq('usuerQueue', {
            url: "/usuarios/resources/usuario/" + correo + "/password",
            type: 'PUT',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(usuario)
        }).done(function (response) {
        }).fail(function (data) {
        });
    };

    util.generaUsuario = function () {
        var usuario = {};
        var persona = {};

        persona.apellidos = $('#apellidosNuevo').val();
        persona.nombre = $('#nombreNuevo').val();
        persona.fechaNacimiento = $.segalmex.date.fechaToIso($('#fechaNacimientoNuevo').val());
        persona.sexo = {id: $('#sexoNuevo').val()};
        persona.paisNacimiento = {id: $('#paisNacimientoNuevo').val()};
        persona.nacionalidad = {id: $('#paisNacimientoNuevo').val()};

        usuario.clave = $('#usuarioNuevo').val();
        usuario.nombre = $('#usuarioNuevo').val();
        usuario.areaUbicacion = {id: $('#areaNuevo').val()};
        usuario.persona = persona;
        usuario.puesto = $('#puestoNuevo').val();
        usuario.plainPassword = $('#contrasena-nueva').val().trim() !== '' ? $('#contrasena-nueva').val() : null;

        return usuario;

    };

    handlers.actualizar = function () {
        $('.valid-field').limpiaErrores();
        $('#actualizar').html("Actualizando...").attr('disabled', 'disabled');
        var errores = [];

        $('.valid-field').valida(errores, false);

        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            $('#actualizar').html("Actualizar").removeAttr('disabled');
            return false;
        }

        var usuario = util.generaUsuario();

        if (util.validaIdentico()) {
            alert("Los datos del usuario son identicos");
            $('#actualizar').html('Actualizar').removeAttr('disabled');

            return false;
        }

        $.ajaxq('usuerQueue', {
            url: "/usuarios/resources/usuario/" + usuario.clave,
            type: 'PUT',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(usuario)
        }).done(function (response, status) {
            if(usuario.plainPassword){
                util.guardaContrasenia(usuario.clave, usuario);
            }
            $('#actualizar').html('Actualizar').removeAttr('disabled');
            alert("El usuario se ha actualizado con éxito.");
            handlers.regresar();

        }).fail(function (data, status) {
            alert("Ocurrió un error al guardar el usuario");
            $('#actualizar').html('Actualizar').removeAttr('disabled');
        });

    };

    util.enviaBusqueda = function () {

        var busqueda = {};
        $("#nombre").val() && (busqueda.nombres = $("#nombre").val());
        $("#apellidos").val() && (busqueda.apellidos = $("#apellidos").val());
        $("#usuario").val() && (busqueda.usuario = $("#usuario").val());

        $.ajax({
            url: '/usuarios/resources/usuario/administracion',
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            data: busqueda,
            success: function (response, status) {
                $('#buscar').html('Buscar').removeAttr('disabled');
                var usuarios = response;
                if (!response || !response.length) {
                    util.busquedaSinResultados();
                } else {
                    util.muestraUsuarios(usuarios);
                }
            },
            error: function (data, status) {
                alert("Ocurrió un error al buscar los usuarios");
                $('#buscar').html('Buscar').removeAttr('disabled');
            }
        });
    };

    util.muestraTodos = function () {
        $('#btnVerTodo').html("Mostrando...").attr('disabled', 'disabled');
        $.ajax({
            url: '/usuarios/resources/usuario',
            type: 'GET',
            dataType: 'json',
            contentType: 'application/json',
            success: function (response, status) {
                $('#btnVerTodo').html('Ver todos').removeAttr('disabled');
                util.muestraUsuarios(response);

            },
            error: function (data, status) {
                alert("Ocurrió un error al buscar los usuarios");
                $('#btnVerTodo').html('Ver todos').removeAttr('disabled');
            }
        });
    };

    util.busquedaSinResultados = function () {
        if (confirm("No se encontraron usuarios con los parámetros de búsqueda.\n"
                + "¿Desea crear uno nuevo?")) {
            handlers.nuevo();
        } else {
            util.limpiar();
        }
    };

    handlers.nuevoUsuario = function (event) {
        $.each(data.catalogos.pais, function () {
            if (this.clave === 'MX') {
                $("#paisNacimientoNuevo").val(this.id);
            }
        });
        $("#formularioBusqueda").hide();
        $("#divDetalle").hide();
        $("#edicionDetalle").hide();
        $("#divEdicion").slideDown();
        $("#botones").slideDown();
        $("#actualizar").hide();
        $("#guardar").show();
        util.inicializaFecha();
        util.limpiarEdicion();

    }

    handlers.nuevo = function (event) {
        $.each(data.catalogos.pais, function () {
            if (this.clave === 'MX') {
                $("#paisNacimientoNuevo").val(this.id);
            }
        });
        $("#formularioBusqueda").slideUp();
        $("#divDetalle").slideUp();
        $("#edicionDetalle").slideUp();
        $("#divEdicion").slideDown();
        $("#botones").slideDown();
        $("#actualizar").hide();
        $("#guardar").show();
        util.inicializaFecha();
        util.limpiarEdicion();

        if ($("#nombre").val()) {
            $("#nombreNuevo").val($("#nombre").val())
            $("#nombreNuevo").attr('disabled', 'disabled');
        }
        if ($("#apellidos").val()) {
            $("#apellidosNuevo").val($("#apellidos").val())
            $("#apellidosNuevo").attr('disabled', 'disabled');
        }
        if ($("#usuario").val()) {
            $("#usuarioNuevo").val($("#usuario").val())
            $("#usuarioNuevo").attr('disabled', 'disabled');
        }
    };

    handlers.editar = function (event) {

        $.ajax({
            url: "/usuarios/resources/usuario/" + event.target.id + "/administracion",
            type: 'GET',
            contentType: 'application/json',
            dataType: 'json',
            async: false,
            success: function (response, status) {
                data.usuario = response;

                $("#formularioBusqueda").slideUp();
                $("#divDetalle").slideUp();
                $("#divEdicion").slideDown();
                $("#botones").slideDown();
                $("#actualizar").show();
                $("#guardar").hide();
                $('.valid-field').limpiaErrores();
                $("#edicionDetalle").html($.segalmex.common.usuarios.vista.muestraUsuario(data.usuario));

                $("#nombreNuevo").val(data.usuario.persona.nombre);
                $("#apellidosNuevo").val(data.usuario.persona.apellidos);
                $("#sexoNuevo").val(data.usuario.persona.sexo.id);
                $("#fechaNacimientoNuevo").val($.segalmex.date.isoToFecha(data.usuario.persona.fechaNacimiento));
                $("#usuarioNuevo").val(data.usuario.nombre);
                $("#usuarioNuevo").attr('disabled', 'disabled');
                $("#areaNuevo").val(data.usuario.areaUbicacion.id);
                $("#puestoNuevo").val(data.usuario.puesto);
                $('#paisNacimientoNuevo').val(data.usuario.persona.nacionalidad.id);
                $('#contrasena-nueva').val('');
            },
            error: function (data, status) {
                alert("Ocurrió un error al buscar el usuario");

            }
        });

    };

    util.muestraUsuarios = function (usuarios) {
        $("#formularioBusqueda").slideUp();
        $("#resultadoBusqueda").slideDown();
        $("#divEdicion").slideUp();
        $("#divDetalle").slideDown();

        $("#resultadoBusqueda").html($.segalmex.common.usuarios.vista.muestraUsuarios(usuarios, true));
        var aoColumns = [{"bSortable": false}, {"bSortable": true}, {"bSortable": true},
            {"bSortable": true}, {"bSortable": true}, {"bSortable": false}];
        $.segalmex.common.usuarios.vista.configuraTablaUsuario(aoColumns);
        $(".edita-usuario").click(handlers.editar);
        $('.actualizar-password').click(handlers.mostrarDialogoactualizarPassword);
        $('.bloquea-usuario').click(handlers.bloquearUsuario);
        $('.desbloquea-usuario').click(handlers.desbloquearUsuario);
    };

    handlers.mostrarDialogoactualizarPassword = function (event) {
        $('#actualizarPasswordDialog').modal('show');
        data.claveUsuario = $.segalmex.common.usuarios.vista.obtenIdEvento(event);
    };

    handlers.bloquearUsuario = function (event) {
        data.claveUsuario = $.segalmex.common.usuarios.vista.obtenIdEvento(event);
        util.cambiaEstatusUsuario(data.claveUsuario, true);
    };

    handlers.desbloquearUsuario = function (event) {
        data.claveUsuario = $.segalmex.common.usuarios.vista.obtenIdEvento(event);
        util.cambiaEstatusUsuario(data.claveUsuario, false);

    };

    util.cambiaEstatusUsuario = function (usuario, bloqueado) {
        $.ajax({
            url: "/usuarios/resources/usuario/" + data.claveUsuario + "/bloqueado/" + bloqueado,
            type: 'PUT',
            dataType: 'json',
            contentType: 'application/json',
            success: function (response, status) {
                alert(response.mensaje);
                handlers.regresar();
            },
            error: function (data, status) {
                alert("Ocurrió un error al modificar al usuario.");
            }
        });
    };

    handlers.actualizarPassword = function (event) {
        $('.valid-field').limpiaErrores();
        $('#actualizarPassword').html("Actualizando...").attr('disabled', 'disabled');

        var errores = [];
        $('.valid-field').valida(errores, false);

        if ($('#contrasena').val() !== $('#verificacion').val()) {
            errores.push({campo: 'contrasena', mensaje: 'no coincide la contraseña con la verificación.'});
        }

        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            $('#actualizarPassword').html("Actualizar").removeAttr('disabled');
            return false;
        }


        var usuario = {
            plainPassword: $('#contrasena').val()
        };

        $.ajax({
            url: "/usuarios/resources/usuario/" + data.claveUsuario + "/password",
            type: 'PUT',
            dataType: 'json',
            contentType: 'application/json',
            data: JSON.stringify(usuario),
            success: function (response, status) {
                $('#actualizarPassword').html('Actualizar').removeAttr('disabled');
                alert(response.mensaje);
                $('#actualizarPasswordDialog').modal('hide');
                $('#contrasena').val("");
                $('#verificacion').val("");

            },
            error: function (data, status) {
                alert("Ocurrió un error al actualizar la contraseña");
                $('#actualizarPassword').html('Actualizar').removeAttr('disabled');
            }
        });

    };

    util.limpiar = function (event) {
        $('#formularioBusqueda').get(0).reset();
        $('#buscar').html('Buscar').removeAttr('disabled');
    };

    util.limpiarEdicion = function (event) {
        $("#nombreNuevo,#apellidosNuevo,#fechaNacimientoNuevo,#usuarioNuevo,#puestoNuevo,#contrasena-nueva").val("").limpiaErrores();
        $("#sexoNuevo,#areaNuevo").val(0).limpiaErrores();
        $("#usuarioNuevo").removeAttr('disabled');
    };

    util.validaIdentico = function () {
        var identico = false;

        if (($("#nombreNuevo").val().toUpperCase() === data.usuario.persona.nombre)
                && ($("#apellidosNuevo").val().toUpperCase() === data.usuario.persona.apellidos)
                && ($("#sexoNuevo").val() === data.usuario.persona.sexo.id.toString())
                && ($('#fechaNacimientoNuevo').val() === $.segalmex.date.isoToFecha((data.usuario.persona.fechaNacimiento)))
                && ($('#paisNacimientoNuevo').val() === data.usuario.persona.nacionalidad.id.toString())
                && ($('#areaNuevo').val() === data.usuario.areaUbicacion.id.toString())
                && ($('#puestoNuevo').val() === data.usuario.puesto)
                && $('#contrasena-nueva').val().trim() === '') {
            identico = true;
        }

        return identico;
    };

})(jQuery);