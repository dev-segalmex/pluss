(function ($) {

    $.segalmex.namespace('segalmex.common.usuarios.permisos');

    var util = {}, handlers = {};
    var catalogos, permisos = [], usuario = null;
    var ids = {
        divUsuarios: $("#divUsuarios"),
        divDetalle: $("#divDetalle"),
        divUsuario: $("#divUsuario"),
        tipo: $("#tipo"),
        accion: $("#accion"),
        area: $("#area"),
        btnRegresar: $("#regresar"),
        btnAgregar: $("#agregar"),
        divUsuarioPermisos: $('#divUsuarioPermisos'),
        limpiar: $('#limpiar')
    };

    $.segalmex.common.usuarios.permisos.init = function () {
        util.cargaCatalogos();
        util.configuraPantalla();
    };

    util.configuraPantalla = function () {
        $('.valid-field').configura({required: true});
        $('.valid-field').validacion();
        ids.tipo.change(util.actualizaAcciones);
        ids.btnAgregar.click(handlers.agregaPermiso);
        ids.btnRegresar.click(util.regresar);
        ids.limpiar.click(util.limpiaCombos);
    };

    util.regresar = function () {
        util.cargaCatalogos();
        util.limpiaCombos();
        ids.divDetalle.slideUp();
        ids.divUsuarios.slideDown();
    };

    util.limpiaCombos = function () {
        $('.valid-field').limpiaErrores();
        $('.valid-field').val(0);
    };

    util.cargaCatalogos = function () {
        $.ajax({
            url: '/usuarios/resources/paginas/usuario/permisos',
            type: 'GET',
            dataType: 'json',
            async: false,
            contentType: 'application/json',
            success: function (response) {
                catalogos = response;
                util.creaSistemas();
                ids.divUsuarios.html($.segalmex.common.usuarios.vista.muestraUsuarios(response.usuarios, false));
                var aoColumns = [{"bSortable": false}, {"bSortable": true}, {"bSortable": true},
                    {"bSortable": true}, {"bSortable": true}];
                $.segalmex.common.usuarios.vista.configuraTablaUsuario(aoColumns);
                $('.edita-usuario').click(util.seleccionUsuario);
            },
            error: function () {
                alert('Error: No se pudieron cargar los catálogos.');
            }
        });
    };

    util.creaSistemas = function () {
        var sistemas = [];
        for (var i = 0; i < catalogos.globales.length; i++) {
            var cat = catalogos.globales[i];
            if (!sistemas.includes(cat.sistema)) {
                sistemas.push(catalogos.globales[i].sistema);
            }
        }
        for (var i = 0; i < catalogos.locales.length; i++) {
            var cat = catalogos.locales[i];
            if (!sistemas.includes(cat.sistema)) {
                sistemas.push(catalogos.locales[i].sistema);
            }
        }
        catalogos.sistemas = sistemas;
        util.creaTabs(sistemas);
        util.actualizaContadores();
    };

    util.creaTabs = function (sitemas) {
        var buffer = [];

        buffer.push('<div id="estatus-tabs" class="list-group">');

        $.each(sitemas, function () {
            buffer.push('<a href="#" class="estatus-tab list-group-item list-group-item-action d-flex justify-content-between align-items-center" ');
            buffer.push('id="');
            buffer.push(this);
            buffer.push('-tab" > ');
            buffer.push(this);
            buffer.push(' &nbsp; <span id="');
            buffer.push(this);
            buffer.push('-badge" class="badge badge-pill badge-secondary">');
            buffer.push();
            buffer.push('</span>');
            buffer.push('</a>');
        });

        buffer.push('</div>');

        $('#estatus-tabs').delegate('a', 'click', util.muestraBandeja);

        $("#sistemas-div").html(buffer.join(''));
    };

    util.muestraBandeja = function (event) {
        event.preventDefault();
        var id = event.target.id;
        if (event.target.localName === 'span') {
            id = event.target.parentElement.id;
        }
        var prefijo = id.substring(0, id.length - '-tab'.length);
        $(".detalle-bandeja").hide();
        util.inicializaBandeja(prefijo);
    };

    util.inicializaBandeja = function (idBandeja) {
        $(".estatus-tab").removeClass("list-group-item-success");
        $("#" + idBandeja + "-tab").addClass("list-group-item-success");
        ids.tipo.removeAttr('disabled');
        util.actualizaListas(idBandeja);
    };

    util.actualizaListas = function (idBandeja) {
        util.vaciarCombos();
        ids.tipo.val(0);
        var locales = [], globales = [];
        for (var u in catalogos.globales) {
            if (catalogos.globales[u].sistema === idBandeja) {
                globales.push(catalogos.globales[u]);
            }
        }
        for (var u in catalogos.locales) {
            if (catalogos.locales[u].sistema === idBandeja) {
                locales.push(catalogos.locales[u]);
            }
        }
        catalogos.globalesSistema = globales;
        catalogos.localesSistema = locales;
    };

    util.actualizaContadores = function () {
        for (var a in catalogos.sistemas) {
            var cantidad = 0;
            for (var u in catalogos.globales) {
                if (catalogos.globales[u].sistema === catalogos.sistemas[a]) {
                    cantidad++;
                }
            }
            for (var u in catalogos.locales) {
                if (catalogos.locales[u].sistema === catalogos.sistemas[a]) {
                    cantidad++;
                }
            }
            $('#' + catalogos.sistemas[a] + '-badge').html(cantidad);
        }
    };

    util.seleccionUsuario = function (event) {
        event.preventDefault();
        var idUsuario = event.target.id;
        usuario = util.getUsuario(idUsuario);
        util.pintaPermisos(usuario.id);
        ids.divUsuarios.slideUp();
        ids.divDetalle.slideDown();
        ids.tipo.attr('disabled', 'disabled');
        util.vaciarCombos();
    };

    util.vaciarCombos = function (){
        ids.accion.actualizaCombo([]).attr('disabled', 'disabled');
        ids.area.actualizaCombo([]).attr('disabled', 'disabled');
    };

    util.getUsuario = function (id) {
        for (var u in catalogos.usuarios) {
            if (catalogos.usuarios[u].id === parseInt(id)) {
                return catalogos.usuarios[u];
            }
        }
    };

    util.pintaPermisos = function (id) {
        $.ajax({
            url: '/usuarios/resources/usuario/' + id + '/permiso',
            type: 'GET',
            contentType: 'application/json',
            dataType: 'json',
            success: function (response) {
                permisos = response;
                ids.divUsuario.html($.segalmex.common.usuarios.vista.muestraUsuario(usuario));
                ids.divUsuarioPermisos.html($.segalmex.common.usuarios.vista.muestraUsuarioPermisos(usuario, permisos));
                $.segalmex.common.usuarios.vista.configuraTablaUsuarioPermisos();
                $('.elimina-permiso').click(handlers.eliminaPermiso);
            },
            error: function (jqXHR) {
                if (jqXHR.status === 404) {
                    alert("No se encontraron los permisos del usuario.");
                } else {
                    alert("Ocurrió un error interno. Por favor intentelo más tarde.");
                }
            },
            async: false
        });

    };

    handlers.eliminaPermiso = function (event) {
        event.preventDefault();
        if (!confirm("Advertencia ¿Está seguro que desea elimnar este permiso?")) {
            return;
        }

        var idPermiso = $.segalmex.common.usuarios.vista.obtenIdEvento(event);
        $.ajax({
            url: '/usuarios/resources/usuario/' + usuario.id + '/permiso/' + idPermiso,
            type: 'DELETE',
            contentType: 'application/json',
            dataType: 'json',
            success: function (response) {
                alert('Éxito: Se ha eliminado el permiso.');
                util.pintaPermisos(usuario.id);
            },
            error: function (jqXHR) {
                if (jqXHR.status === 404) {
                    alert("No se encontraron los datos necesarios para eliminar el permiso.");
                } else {
                    alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
                }
            }
        });
    };

    handlers.agregaPermiso = function (event) {
        event.preventDefault();
        ids.btnAgregar.html("Agregando...").attr('disabled', 'disabled');
        $('.valid-field').limpiaErrores();
        var errores = [];
        $('.valid-field').valida(errores, false);
        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            ids.btnAgregar.html("Agregar").removeAttr('disabled');
            return false;
        } else {
            util.enviaPermiso();
        }

        ids.btnAgregar.html("Agregar permiso").removeAttr('disabled');
    };

    util.enviaPermiso = function () {
        var permiso = {};
        if (ids.accion.val() !== '0') {
            permiso.accion = {id: ids.accion.val()};
        } else {
            alert('Es necesario una acción para ser agregada.');
            return;
        }
        if (ids.area.val() !== '0') {
            permiso.area = {id: ids.area.val()};
        }
        $.ajax({
            url: '/usuarios/resources/usuario/' + usuario.id + '/permiso',
            type: 'POST',
            contentType: 'application/json',
            dataType: 'json',
            data: JSON.stringify(permiso),
            success: function (response) {
                alert('Éxito: Se ha agregado el permiso.');
                util.pintaPermisos(usuario.id);
                util.limpiaCombos();
            },
            error: function (jqXHR) {
                if (jqXHR.status === 404) {
                    alert("No se encontraron los datos necesarios para agregar el permiso.");
                } else {
                    alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
                }
            },
            async: false
        });
    };

    util.actualizaAcciones = function () {
        if (Number(ids.tipo.val()) === 0) {
            ids.accion.actualizaCombo([]);
            ids.area.actualizaCombo([]);
            return;
        }
        if (Number(ids.tipo.val()) === 1) {
            util.ordena(catalogos.localesSistema);
            ids.accion.actualizaCombo(catalogos.localesSistema);
            ids.area.actualizaCombo(catalogos.areas);
            ids.area.configura({required: true});
            ids.area.validacion();
        } else {
            util.ordena(catalogos.globalesSistema);
            ids.accion.actualizaCombo(catalogos.globalesSistema);
            ids.area.actualizaCombo([]);
            ids.area.configura({required: false});
            ids.area.validacion();
        }
    };

    util.ordena = function (arreglo) {
        if (arreglo.length > 0) {
            arreglo.sort(function (elementoA, elementoB) {
                var palabraA = elementoA.nombre;
                var palabraB = elementoB.nombre;
                if (palabraA < palabraB) {
                    return -1;
                }
                if (palabraA > palabraB) {
                    return 1;
                }
                return 0;
            });
        }
    };

})(jQuery);
