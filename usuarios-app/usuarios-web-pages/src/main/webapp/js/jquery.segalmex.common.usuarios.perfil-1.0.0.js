(function ($) {

    $.segalmex.namespace('segalmex.common.usuarios.perfil');

    var data = {
        required: ['#passwordActual,#passwordNuevo,#passwordConfirma'],
        usuario: null
    };
    var handlers = {};
    var util = {};
    var catalogos, permisos = [], usuario = null;


    $.segalmex.common.usuarios.perfil.init = function () {
        $(data.required.join(',')).configura({
            required: true,
            allowSpace: false
        });
        $('#passwordNuevo').configura({
            minlength: 8
        });
        $('#passwordConfirma').configura({
            minlength: 8
        });
        $('#area').configura({required: true});
        $('#area').validacion();
        $(data.required.join(',')).validacion();
        util.cargaCatalogos();
        util.cargaUsuario();
        util.configuraAcciones();
        util.pintaPermisos(usuario.id);
    };

    util.configuraAcciones = function () {
        $('#confirmar-password-button').click(handlers.actualizaPassword);
        $('#buton-capturar-password').click(util.capturaPassword);
        $('#button-actualizar-area').click(handlers.actualizaArea);
        setTimeout(function () {
            $('#passwordActual').focus();
        }, 1500);
    };
    
    util.capturaPassword = function (e) {
        e.preventDefault();
        $('#passwordActual,#passwordNuevo,#passwordConfirma').val('').limpiaErrores();
        $('#capturar-password-modal').modal('show');
        setTimeout(function () {
            $('#passwordActual').focus();
        }, 500);
    };

    util.cargaUsuario = function () {
        usuario = $.segalmex.common.pagina.comun.registrado;
        $('#detalle-usuario').html($.segalmex.common.usuarios.vista.muestraUsuario(usuario));
        $('#detalle-usuario').slideDown();
        $('#area').val($.segalmex.common.pagina.comun.registrado.areaUbicacion.id);
    };


    util.pintaPermisos = function (id) {

        $.ajax({
            url: '/usuarios/resources/usuario/' + id + '/permiso',
            type: 'GET',
            contentType: 'application/json',
            dataType: 'json',
            success: function (response) {
                permisos = response;
                $("#permisos-usuario").html($.segalmex.common.usuarios.vista.muestraPermisos(permisos));
                $("#permisos-usuario").slideDown();
            },
            error: function (jqXHR) {
                if (jqXHR.status === 404) {
                    alert("No se encontraron los permisos del usuario.");
                } else {
                    alert("Ocurrió un error interno. Por favor intentelo más tarde.");
                }
            },
            async: false
        });

    };

    handlers.actualizaPassword = function (event) {
        $('#confirmar-password-button').html("Actualizando...").prop('disabled', true);
        $(data.required.join(',')).limpiaErrores();
        var errores = [];
        $(data.required.join(',')).valida(errores, false);
        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            $('#confirmar-password-button').html("Actualizar").prop('disabled', false);
            return false;
        }
        if ($('#passwordNuevo').val() !== $('#passwordConfirma').val()) {
            errores.push({campo: 'passwordConfirma', mensaje: 'no coincide con la contraseña nueva capturada.'});
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            $('#confirmar-password-button').html("Actualizar").prop('disabled', false);
            return false;
        }
        var usuario = {
            plainPassword: $('#passwordNuevo').val(),
            plainPasswordActual: $('#passwordActual').val()
        };

        $.ajax({
            url: '/usuarios/resources/usuario/password',
            type: 'PUT',
            data: JSON.stringify(usuario),
            dataType: 'json',
            async: false,
            contentType: 'application/json',
            success: function (response) {
                $('#password').val(usuario.plainPassword);
                alert('Éxito: ' + response.mensaje);
                $('#confirmar-password-button').html("Actualizar").prop('disabled', false);
                $('#capturar-password-modal').modal('hide');
            },
            error: function (jqXHR) {
                $('#confirmar-password-button').html("Actualizar").prop('disabled', false);
                if (jqXHR.status === 404) {
                    alert("No se encontro el usuario para actualizar.");
                } else if (jqXHR.status === 500) {
                    alert('Ocurrió un error en el servidor intentelo mas tarde nuevamente.');
                } else {
                    alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
                }
            }
        });
    };

    handlers.actualizaArea = function () {
        $('#button-actualizar-area').html("Actualizando...").prop('disabled', true);
        $('#area').limpiaErrores();
        var errores = [];
        $('#area').valida(errores, false);
        if (errores.length !== 0) {
            $.validation.showErrors(errores);
            $('#' + errores[0].campo).focus();
            $('#button-actualizar-area').html("Actualizar").prop('disabled', false);
            return false;
        }

        $.ajax({
            url: '/usuarios/resources/usuario/area/' + $('#area').val(),
            type: 'PUT',
            dataType: 'json',
            async: false,
            contentType: 'application/json',
            success: function (response) {
                alert('Éxito: ' + response.mensaje);
                $('#button-actualizar-area').html("Actualizar").prop('disabled', false);
            },
            error: function (jqXHR) {
                $('#button-actualizar-area').html("Actualizar").prop('disabled', false);
                if (jqXHR.status === 404) {
                    alert("No se encontro el usuario para actualizar.");
                } else if (jqXHR.status === 500) {
                    alert('Ocurrió un error en el servidor intentelo mas tarde nuevamente.');
                } else {
                    alert($.segalmex.common.vista.generaMensajeExcepcion(jqXHR.responseJSON));
                }
            }
        });
    };

    util.cargaCatalogos = function () {
        $.ajax({
            url: '/usuarios/resources/paginas/usuario/areas',
            type: 'GET',
            dataType: 'json',
            async: false,
            contentType: 'application/json',
            success: function (response) {
                $('#area').actualizaCombo(response);
            },
            error: function (jqXHR, textStatus) {
                alert('Error No se pudieron obtener los catálogos.');
            }
        });
    };

})(jQuery);