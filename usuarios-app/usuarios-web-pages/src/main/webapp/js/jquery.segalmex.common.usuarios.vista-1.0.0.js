(function ($) {
    $.segalmex.namespace('segalmex.common.usuarios.vista');

    $.segalmex.common.usuarios.vista.muestraPermisos  = function (permisos) {

        var buffer = '';

        buffer = `
            <div class="card mb-4 shadow-sm">
            <h4 class="card-header">Permisos del usuario</h4>
            <div class ="card-body">
            <table id="tblPermisos" class="table table-striped" width="100%">
                <thead class = "table-success">
                    <tr class="success">
                        <th>#</th>
                        <th>Accion</th>
                        <th>Tipo</th>
                        <th>Area</th>
                        <th>Fecha</th>
                    </tr>
                </thead>
                <tbody>
        `;
        var i=1;
        $.each(permisos, function () {
            buffer += `
                <tr class="text-left">
                    <td>${i++}</td>
                    <td>${this.accion.sistema+' - '+this.accion.nombre}</td>
                    <td>${this.area ? this.area.nombre : 'Global'}</td>
                    <td>${this.area ? this.area.nombre : '-'}</td>
                    <td>${$.segalmex.date.isoToFechaHora(this.fechaAutorizacion)}</td>
                </tr>

            `;
        });
        buffer += `
            </tbody>
            </table>
        `;
        return buffer;
    };

    $.segalmex.common.usuarios.vista.muestraUsuario = function (usuario) {
        var buffer = [];

        buffer.push('<div class="card mb-4 shadow-sm">');
        buffer.push('<h4 class="card-header">Datos del usuario</h4>');
        buffer.push('<div class ="card-body">');

        buffer.push('<div class="row">');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Nombre(s)</strong><br/>');
        buffer.push(usuario.persona.nombre);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Apellido(s)</strong><br/>');
        buffer.push(usuario.persona.apellidos);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Sexo</strong><br/>');
        buffer.push(usuario.persona.sexo ? usuario.persona.sexo.nombre : '--');
        buffer.push('</div>');
        buffer.push('</div>');

        buffer.push('<div class="row">');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Usuario</strong><br/>');
        buffer.push(usuario.clave);
        buffer.push('</div>');
        buffer.push('<div class="col-md-4">');
        buffer.push('<strong>Estatus</strong><br/>');
        buffer.push(usuario.activo ? 'Activo' : 'Inactivo');
        buffer.push('</div>');

        buffer.push('</div>');//row

        buffer.push('</div>');//card-body
        buffer.push('</div>');//bs-callout-primary


        return buffer.join('');
    };

    $.segalmex.common.usuarios.vista.muestraUsuarios = function (usuarios, accion) {
        var buffer = [];

        buffer.push('<h2>Usuarios del sistema</h2>');
        buffer.push('<br/>');
        buffer.push('<table id="tblUsuarios"');
        buffer.push(' class="table table-striped" width="100%">');
        buffer.push('<thead class = "table-success">');
        buffer.push('<tr class="success">');
        buffer.push('<th>#</th>');
        buffer.push('<th>Usuario</th>');
        buffer.push('<th>Apellido(s)</th>');
        buffer.push('<th>Nombre(s)</th>');
        buffer.push('<th>Sexo</th>');
        if (accion) {
            buffer.push('<th>Acción</th>');
        }

        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');

        $.each(usuarios, function () {
            buffer.push('<tr class="text-left">');

            buffer.push('<td>');
            buffer.push('</td>');

            buffer.push('<td>');
            buffer.push('<a href="#" title="ver permisos" class="edita-usuario" id="');
            buffer.push(this.id);
            buffer.push('">');
            buffer.push(this.clave);
            buffer.push('</a>');

            buffer.push('</td>');

            buffer.push('<td>');
            buffer.push(this.persona.apellidos.toUpperCase());
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(this.persona.nombre.toUpperCase());
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(this.persona.sexo.nombre);
            buffer.push('</td>');
            if (accion) {
                buffer.push('<td class="text-center">');
                buffer.push('<div id="');
                buffer.push(this.clave);
                buffer.push('" class="actualizar-password btn btn-outline-primary" title="Actualizar contraseña" data-toggle="tooltip" data-placement="bottom">');
                buffer.push('<span title="Actualizar contraseña" class="fas fa-key"></span>');
                buffer.push('</div>');

                buffer.push(' ');
                if (this.fechaBloqueo && this.fechaBloqueo !== undefined) {
                    buffer.push('<div id="');
                    buffer.push(this.clave);
                    buffer.push('" class="desbloquea-usuario btn btn-outline-success" title="Desbloquear usuario" data-toggle="tooltip" data-placement="bottom">');
                    buffer.push('<span title="Desbloquaer usuario" class="fas fa-unlock"></span>');
                    buffer.push('</div>');
                } else {
                    buffer.push('<div id="');
                    buffer.push(this.clave);
                    buffer.push('" class="bloquea-usuario btn btn-outline-danger" title="Bloquear usuario" data-toggle="tooltip" data-placement="bottom">');
                    buffer.push('<i title="Bloquear usuario" class="fas fa-lock"></i>');
                    buffer.push('</div>');
                }
            }
            buffer.push('</td>');
            buffer.push('</tr>');
        });

        buffer.push('</tbody>');
        buffer.push('</table>');

        return buffer.join('');
    };

    $.segalmex.common.usuarios.vista.configuraTablaUsuario = function (aoColumns) {
        $.segalmex.common.usuarios.vista.configuraTabla('tblUsuarios', aoColumns);
    };

    $.segalmex.common.usuarios.vista.configuraTabla = function (tablaId, aoColumns) {
        var table = $('#' + tablaId).DataTable({
            "oLanguage": {
                "sEmptyTable": "No se encontró información para mostrar en la tabla",
                "sSearch": "Buscar ",
                "sZeroRecords": "No se encontraron resultados."
            },
            "bFilter": true,
            "bInfo": false,
            "bPaginate": false,
            "aoColumns": aoColumns,
            "aoColumnDefs": [{"bSortable": false, "aTargets": ["no-sort"]}]
        });
        table.on('order.dt search.dt', function () {
            table.column(0, {search: 'applied', order: 'applied'}).nodes().each(function (cell, i) {
                cell.innerHTML = i + 1;
            });
        }).order([[1, 'asc']]).draw(false);
        $("#" + tablaId + "_filter input").addClass('input-buscar');
        $(".dataTables_empty").attr("colspan", "100%");
    };

    $.segalmex.common.usuarios.vista.muestraUsuarioPermisos = function (usuario, permisos) {
        var buffer = [];
        buffer.push('<table id="tblPermisosUsuario"');
        buffer.push(' class="table table-striped" width="100%">');
        buffer.push('<thead class = "table-success">');
        buffer.push('<tr class="success">');
        buffer.push('<th>#</th>');
        buffer.push('<th>Nombre</th>');
        buffer.push('<th>Área</th>');
        buffer.push('<th>Acción</th>');
        buffer.push('</tr>');
        buffer.push('</thead>');
        buffer.push('<tbody>');

        $.each(permisos, function () {
            buffer.push('<tr class="text-left">');

            buffer.push('<td>');
            buffer.push('</td>');

            buffer.push('<td>');
            buffer.push(this.accion.sistema + ' - ' + this.accion.nombre);
            buffer.push('</td>');

            buffer.push('<td>');
            buffer.push(this.area ? this.area.nombre : 'Global');
            buffer.push('</td>');
            buffer.push('<td class="text-center">');
            buffer.push('<div id="');
            buffer.push(this.id);
            buffer.push('" class="elimina-permiso btn btn-outline-danger" title="Eliminar" data-toggle="tooltip" data-placement="bottom">');
            buffer.push('<span class="fas fa-trash" title="Eliminar" aria-hidden="true"></span>');
            buffer.push('</div>');
            buffer.push('</td>');

            buffer.push('</tr>');
        });

        buffer.push('</tbody>');
        buffer.push('</table>');

        buffer.push('<br/>');
        return buffer.join('');

    };

    $.segalmex.common.usuarios.vista.configuraTablaUsuarioPermisos = function () {
        var aoColumns = [{"bSortable": false}, {"bSortable": true}, {"bSortable": true},
            {"bSortable": false}];
        $.segalmex.common.usuarios.vista.configuraTabla('tblPermisosUsuario', aoColumns);
    };

    $.segalmex.common.usuarios.vista.obtenIdEvento = function (event) {
        return event.target.id || event.target.parentElement.id;
    };

})(jQuery);
