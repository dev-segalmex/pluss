/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

(function ($) {

    var utils = {};

    /**
     * Función auxiliar que da formato a un campo de fechas, convirtiendo a dd/mm/yyyy.
     *
     * @param event el evento que generó el cambio.
     * @param before si la fecha debe ser anterior a la actual.
     * @param current la fecha actual.
     */
    var dateFormat = function (event, before, current) {
        var element = $(event.target);
        var date = element.val();
        if (date) {
            if (/^\d{8}$/.test(date)) {
                element.val(date.substring(0, 2) + '/' + date.substring(2, 4) + '/' + date.substring(4, 8));
            } else if (/^\d{6}$/.test(date)) {
                var dec = current.getFullYear() % 100;
                var milenio = current.getFullYear() - dec;

                var aa = parseInt(date.substring(4, 6), 10);
                var mm = parseInt(date.substring(2, 4), 10);
                var dd = parseInt(date.substring(0, 2), 10);

                if (dec + 20 >= aa) {
                    aa += milenio;
                } else {
                    aa += milenio - 100;
                }

                var ddmmaa = new Date();
                ddmmaa.setFullYear(aa, mm - 1, dd);
                if ((ddmmaa > current) && before) {
                    aa -= 100;
                }
                element.val(date.substring(0, 2) + '/' + date.substring(2, 4) + '/' + aa);
                dateFormat(event, before, current);
            } else if (/^\d{2}\/\d{2}\/\d{2}$/.test(date)) {
                element.val(date.substring(0, 2) + date.substring(3, 5) + date.substring(6, 8));
                dateFormat(event, before, current);
            } else if (/^\d{1}\/\d{2}\/\d{2}$/.test(date)) {
                element.val('0' + date.substring(0, 1) + date.substring(2, 4) + date.substring(5, 7));
                dateFormat(event, before, current);
            } else if (/^\d{2}\/\d{1}\/\d{2}$/.test(date)) {
                element.val(date.substring(0, 2) + '0' + date.substring(3, 5) + date.substring(6, 8));
                dateFormat(event, before, current);
            }
        }
    }

    var pointFormat = function(e) {
        var element = $(e.target);
        var point = element.val();
        if (!point) {
            return;
        }

        if (/^(\-){0,1}\d{1,3},\d{1,2},\d{1,2}(\.\d{1,10}){0,1}$/.test(point)) {
            var gms = point.split(',');
            var g = parseInt(gms[0], 10);
            var m = parseInt(gms[1], 10);
            var s = parseFloat(gms[2]);

            var d = (m / 60) + (s / 3600);
            if (g >= 0) {
                d = g + d;
            } else {
                d = g - d;
            }
            var ds = ('' + d).split('.');
            var gd = ds[0];
            var dd = '';
            if (ds.length > 1) {
                if (ds[1].length > 7) {
                    dd = '.' + ds[1].substring(0, 7);
                } else {
                    dd = '.' + ds[1];
                }
            }
            element.val(gd + dd);
        }
    }

    /**
     * Función auxiliar que da formato a un campo de horas, convirtiendo a hh:mm.
     *
     * @param event el evento que generó el cambio.
     */
    var hourFormat = function (event) {
        var element = $(event.target);
        var hour = element.val();
        if (hour) {
            if (/^\d{3}$/.test(hour)) {
                element.val('0' + hour.substring(0, 1) + ':' + hour.substring(1, 3));
            } else if (/^\d{4}$/.test(hour)) {
                element.val(hour.substring(0, 2) + ':' + hour.substring(2, 4));
                hourFormat(event);
            }
        }
    }

    /**
     * Función auxiliar que configura un elemento del DOM.
     *
     * @param element el elemento del DOM.
     * @param params los parámetros para modificar la configuración.
     */
    var configuraElemento = function (element, params) {
        var configuracion = $.data(element, 'configuracion');

        // Si no hay configuración, craemos una y la ponemos
        if (!configuracion) {
            var type = (element.type === ('select-one' || 'select-multiple')) ? 'select' : element.type;
            var label = $("label[for=" + element.id + "]").html();
            var required = true;
            if (!label) {
                console.log('No hay etiqueta para: ' + element.id);
            }
            configuracion = {
                type: type,
                label: {es: filtraTextoEtiqueta(label)},
                required: required,
                mensajes: {},
                force: false // validar incluso si el elemento esta oculto. Default: false.
            };

            // Establecemos la longitud mínima y máxima por default, así como las validaciones y normalizaciones
            if (type === 'text') {
                configuracion['maxlength'] = 40;
                configuracion['minlength'] = 1;
                configuracion['pattern'] = /^[A-Za-z0-9 ]+$/;
                configuracion['textTransform'] = 'upper';
                configuracion['allowSpace'] = true;
            } else if (type === 'textarea') {
                configuracion['maxlength'] = 255;
                configuracion['minlength'] = 1;
                configuracion['textTransform'] = 'upper';
                configuracion['allowSpace'] = true;
            }
            $.data(element, 'configuracion', configuracion);
        }

        // Revisamos los parámetros y sobreescribimos dependiendo del tipo
        if (params) {
            if (params.lang) {
                lang = params.lang;
            }
            for (var param in params) {
                if (param === 'type') {
                    var tipo = params[param];
                    if (tipo == 'email') {
                        configuracion['minlength'] = null;
                        configuracion['maxlength'] = null;
                        configuracion['pattern'] = /^([\w-]+(?:\.[\w-]+)*)@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$/;
                        configuracion['textTransform'] = 'lower';
                        configuracion['allowSpace'] = false;
                    } else if (tipo == 'date') {
                        configuracion['minlength'] = 10;
                        configuracion['maxlength'] = 10;
                        configuracion['pattern'] = /^\d{2}\/\d{2}\/\d{4}$/;
                        configuracion['textTransform'] = null;
                        configuracion['allowSpace'] = false;
                    } else if (tipo == 'number') {
                        configuracion['minlength'] = 1;
                        configuracion['maxlength'] = 10;
                        configuracion['pattern'] = /^(\-){0,1}\d{1,10}(\.\d{1,10}){0,1}$/;
                        configuracion['textTransform'] = null;
                        configuracion['allowSpace'] = false;
                    } else if (tipo === 'point') {
                        configuracion['minlength'] = 1;
                        configuracion['maxlength'] = 16;
                        configuracion['pattern'] = /^(\-){0,1}(\d{1,10}(\.\d{1,10}){0,1}|\d{1,3},\d{1,2},\d{1,2}(\.\d{1,10}){0,1})$/;
                        configuracion['textTransform'] = null;
                        configuracion['allowSpace'] = false;
                    } else if (tipo == 'name') {
                        configuracion['pattern'] = /^([A-Za-zÑñÁÉÍÓÚáéíóúÀÈÌÒÙàèìòùÄËÏÖÜäëïöüß'\-\. ]+|_)$/;
                        configuracion['textTransform'] || (configuracion['textTransform'] = 'upper');
                        configuracion['allowSpace'] = true;
                        configuracion['minlength'] = 1;
                        configuracion['maxlength'] = 50;
                    } else if (tipo == 'curp') {
                        configuracion['pattern'] = /^([A-Z][AEIOUX][A-Z]{2}\d{2}(?:0[1-9]|1[0-2])(?:0[1-9]|[12]\d|3[01])[HM](?:AS|B[CS]|C[CLMSH]|D[FG]|G[TR]|HG|JC|M[CNS]|N[ETL]|OC|PL|Q[TR]|S[PLR]|T[CSL]|VZ|YN|ZS)[B-DF-HJ-NP-TV-Z]{3}[A-Z\d])(\d)$/;
                        configuracion['textTransform'] = 'upper';
                        configuracion['allowSpace'] = false;
                        configuracion['minlength'] = 18;
                        configuracion['maxlength'] = 18;
                    } else if (tipo == 'hour') {
                        configuracion['pattern'] = /^\d{2}:\d{2}$/;
                        configuracion['textTransform'] = null;
                        configuracion['allowSpace'] = false;
                        configuracion['minlength'] = 5;
                        configuracion['maxlength'] = 5;
                    } else if (tipo == 'rfc-fisica') {
                        configuracion['pattern'] = /^([A-ZÑ]{4})\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])([A-Z0-9]{2})([A0-9])$/;
                        configuracion['textTransform'] = 'upper';
                        configuracion['allowSpace'] = false;
                        configuracion['minlength'] = 13;
                        configuracion['maxlength'] = 13;
                    } else if (tipo == 'rfc-moral') {
                        configuracion['pattern'] = /^([A-ZÑ&]{3})\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])([A-Z0-9]{2})([A0-9])$/;
                        configuracion['textTransform'] = 'upper';
                        configuracion['allowSpace'] = false;
                        configuracion['minlength'] = 12;
                        configuracion['maxlength'] = 12;
                    } else if (tipo == 'rfc') {
                        configuracion['pattern'] = /^([A-ZÑ&]{3,4})\d{2}(0[1-9]|1[012])(0[1-9]|[12][0-9]|3[01])([A-Z0-9]{2})([A0-9])$/;
                        configuracion['textTransform'] = 'upper';
                        configuracion['allowSpace'] = false;
                        configuracion['minlength'] = 12;
                        configuracion['maxlength'] = 13;
                    } else if (tipo == 'name-moral') {
                        configuracion['pattern'] = /^[-A-Z0-9ÑÁÉÍÓÚáéíóú&\.,"' \/\\\+]*$/;
                        configuracion['textTransform'] = 'upper';
                        configuracion['allowSpace'] = true;
                        configuracion['minlength'] = 1;
                        configuracion['maxlength'] = 127;
                    } else if (tipo == 'nombre-original') {
                        configuracion['pattern'] = /^([0-9A-Za-zÑñÁÉÍÓÚáéíóúÀÈÌÒÙàèìòùÄËÏÖÜäëïöüß"'\-\.\, ]+|_)$/;
                        configuracion['textTransform'] = null;
                        configuracion['allowSpace'] = true;
                        configuracion['minlength'] = 1;
                        configuracion['maxlength'] = 100;
                    } else if (tipo == 'nombre-icao') {
                        configuracion['pattern'] = /^(?:[A-Za-zÑñÁÉÍÓÚáéíóúÀÈÌÒÙàèìòùÄËÏÖÜäëïöüÆÂÃÅÊÎÔÕØÛßÇç]+'?(?:(?:\. |, |[ '-])[A-Za-zÑñÁÉÍÓÚáéíóúÀÈÌÒÙàèìòùÄËÏÖÜäëïöüÆÂÃÅÊÎÔÕØÛßÇç]+'?)*|_)$/;
                        configuracion['textTransform'] = null;
                        configuracion['allowSpace'] = true;
                        configuracion['minlength'] = 1;
                        configuracion['maxlength'] = 100;
                    } else if (tipo == 'comentario') {
                        configuracion['textTransform'] = null;
                        configuracion['allowSpace'] = true;
                        configuracion['minlength'] = 1;
                        configuracion['maxlength'] = 4000;
                    } else if (tipo == 'folio') {
                        configuracion['minlength'] = 1;
                        configuracion['maxlength'] = 13;
                        configuracion['pattern'] = /^\d{1,13}$/;
                        configuracion['textTransform'] = null;
                        configuracion['allowSpace'] = false;
                    }
                    break;
                }
            }
        }

        $.extend(configuracion, params || {});
    };

    /*
     * Elimina los espacios en blanco contiguos, así como retornos de carro
     * en la cadena que se pasa como argumento. Usado para evitar que aparezcan
     * con formato incorrecto las etiquetas en el diálogo de validación.
     */
    var filtraTextoEtiqueta = function (cadenaEtiqueta) {
        var txt = cadenaEtiqueta.replace(/\n/g, ' ').replace(/\s+/g, ' ');
        return $.trim(txt);
    }

    /**
     * Objeto que agrupa las funciones de validación.
     */
    if (!$.validation) {
        $.validation = {};
    }
    $.validation.today = $.validation.today ? $.validation.today : new Date();
    $.validation.errorClass = $.validation.errorClass ? $.validation.errorClass : 'error-field';
    $.validation.errorLabelClass = $.validation.errorLabelClass ? $.validation.errorLabelClass : 'error-label';
    $.validation.errorMessageClass = $.validation.errorMessageClass ? $.validation.errorMessageClass : 'error-message';

    /**
     * Verifica si un elemento es válido a partir de la configuración proporcionada.
     *
     * @param element el elemento a validar
     * @return el id y la causa por la cual no es válido, null si es válido.
     */
    $.validation.isValid = function (element) {
        var id = element.id;
        var campo = $(element);
        var valor = campo.val();
        var configuracion = $.data(element, 'configuracion');
        if (!configuracion) {
            console.log('No hay configuración para: ' + id);
        }

        // Si no está deshabilitado y es visible, lo validamos
        if (!campo.attr('disabled') && campo.is(':visible') || configuracion.force) {
            switch (configuracion.type) {
                case 'date':
                    if (configuracion.required && !valor) {
                        return {campo: id, mensaje: utils.obtenTraduccion(traduccion, 'requerido', lang)};
                    }

                    if (valor) {
                        if (configuracion.pattern && !configuracion.pattern.test(valor)) {
                            return {campo: id, mensaje: utils.obtenTraduccion(traduccion, 'noCumpleFormato', lang)};
                        }

                        var dd = parseInt(valor.substring(0, 2), 10);
                        var mm = parseInt(valor.substring(3, 5), 10);
                        var yyyy = parseInt(valor.substring(6, 10), 10);

                        // Verificamos si pusieron fechas muy grande en IE (bug)
                        if (yyyy > 2100 && !$.support.cssFloat) {
                            return {campo: id, mensaje: utils.obtenTraduccion(traduccion, 'fechaNoValida', lang)};
                        }

                        var date = new Date()
                        date.setFullYear(yyyy, mm - 1, dd);
                        if (!(date.getDate() == dd
                                && date.getMonth() == (mm - 1)
                                && date.getFullYear() == yyyy)) {
                            return {campo: id, mensaje: utils.obtenTraduccion(traduccion, 'fechaNoValida', lang)};
                        }

                        if (configuracion.min) {
                            var mensajes = configuracion.mensajes;
                            var min = null;
                            if (configuracion.min instanceof Date) {
                                min = configuracion.min;
                            } else {
                                var dmin = parseInt(configuracion.min.substring(8, 10), 10);
                                var mmin = parseInt(configuracion.min.substring(5, 7), 10);
                                var ymin = parseInt(configuracion.min.substring(0, 4), 10);

                                min = new Date();
                                min.setFullYear(ymin, mmin - 1, dmin);
                            }
                            min.setHours(0, 0, 0, 0);

                            if (date < min) {
                                return {campo: id, mensaje: (mensajes.min ? mensajes.min : utils.obtenTraduccion(traduccion, 'fechaMenor', lang))}
                            }
                        }

                        if (configuracion.max) {
                            var max = null;
                            if (configuracion.max instanceof Date) {
                                max = configuracion.max;
                            } else {
                                var dmax = parseInt(configuracion.max.substring(8, 10), 10);
                                var mmax = parseInt(configuracion.max.substring(5, 7), 10);
                                var ymax = parseInt(configuracion.max.substring(0, 4), 10);
                                max = new Date();
                                max.setFullYear(ymax, mmax - 1, dmax);
                            }
                            max.setHours(23, 59, 59, 999);
                            if (date > max) {
                                return {campo: id, mensaje: utils.obtenTraduccion(traduccion, 'fechaMayor', lang)}
                            }
                        }
                    }
                    break;
                case 'point':
                case 'number':
                    if (configuracion.required && !valor) {
                        return {campo: id, mensaje: utils.obtenTraduccion(traduccion, 'requerido', lang)};
                    }

                    if (valor) {
                        if (configuracion.pattern && !configuracion.pattern.test(valor)) {
                            return {campo: id, mensaje: utils.obtenTraduccion(traduccion, 'numeroNoValido', lang)};
                        }
                        var num = parseFloat(valor);
                        if (configuracion.min != null && num < configuracion.min) {
                            return {campo: id, mensaje: utils.obtenTraduccion(traduccion, 'numeroMenor', lang)};
                        }

                        if (configuracion.max != null && num > configuracion.max) {
                            return {campo: id, mensaje: utils.obtenTraduccion(traduccion, 'numeroMayor', lang)};
                        }
                    }
                    break;
                case 'select':
                    if (configuracion.required && valor == '0') {
                        return {campo: id, mensaje: utils.obtenTraduccion(traduccion, 'requerido', lang)}
                    }
                    break;
                case 'hour':
                    if (configuracion.required && !valor) {
                        return {campo: id, mensaje: utils.obtenTraduccion(traduccion, 'requerido', lang)}
                    }
                    if (valor) {
                        if (configuracion.pattern && !configuracion.pattern.test(valor)) {
                            return {campo: id, mensaje: utils.obtenTraduccion(traduccion, 'noCumpleFormato', lang)};
                        }
                    }
                    var hours = parseInt(valor.substring(0, 2), 10);
                    var minutes = parseInt(valor.substring(3, 5), 10);
                    if (hours < 0 || hours > 23 || minutes < 0 || minutes > 59) {
                        return {campo: id, mensaje: utils.obtenTraduccion(traduccion, 'horaNoValida', lang)};
                    }
                    break;
                case 'comentario':
                    if (configuracion.required && !valor) {
                        return {campo: id, mensaje: utils.obtenTraduccion(traduccion, 'requerido', lang)};
                    }

                    if (valor) {
                        if (configuracion.maxlength != null && valor.length > configuracion.maxlength) {
                            return {campo: id, mensaje: 'no puede ser mayor a ' + configuracion.maxlength
                                        + ' caracteres.\n Usted ha ingresado ' + valor.length + ' caracteres.'};
                        }
                    }
                    break;
                default:
                    if (configuracion.required && !valor) {
                        return {campo: id, mensaje: utils.obtenTraduccion(traduccion, 'requerido', lang)};
                    }

                    if (valor) {
                        if (configuracion.pattern && !configuracion.pattern.test(valor)) {
                            return {campo: id, mensaje: utils.obtenTraduccion(traduccion, 'noCumpleFormato', lang)};
                        }
                        if (configuracion.minlength != null && valor.length < configuracion.minlength) {
                            return {campo: id, mensaje: utils.obtenTraduccion(traduccion, 'longitudMinima', lang) + configuracion.minlength + '.'};
                        }

                        if (configuracion.maxlength != null && valor.length > configuracion.maxlength) {
                            return {campo: id, mensaje: utils.obtenTraduccion(traduccion, 'longitudMaxima', lang) + configuracion.maxlength + '.'};
                        }
                    }
            }
        }
        return null;
    };

    /**
     * Función que da formato a un elemento.
     *
     * @param element el elemento del dom.
     */
    $.validation.format = function (element) {
        var campo = $(element);
        var valor = campo.val();
        var configuracion = $.data(element, 'configuracion');

        // Si hay un valor
        if (valor) {
            // Vemos si hay que convertir a mayúsculas, minúsculas o nada
            if (configuracion && configuracion.textTransform) {
                switch (configuracion.textTransform) {
                    case 'upper':
                        valor = valor.toUpperCase();
                        break;
                    case 'lower':
                        valor = valor.toLowerCase();
                        break;
                }
            }

            // Hacemos trim
            valor = valor.replace(/^\s*|\s*$/gi, '');

            //  Si solo contiene un guin medio se cambia por un guin bajo.
            if (valor == "-") {
                valor = "_";
            }

            // Normalizamos campos de texto
            if (configuracion && configuracion.type == 'text') {
                valor = valor.replace(/Á/gi, 'A');
                valor = valor.replace(/É/gi, 'E');
                valor = valor.replace(/Í/gi, 'I');
                valor = valor.replace(/Ó/gi, 'O');
                valor = valor.replace(/Ú/gi, 'U');
                valor = valor.replace(/Ñ/gi, 'N');
                valor = valor.replace(/á/gi, 'A');
                valor = valor.replace(/é/gi, 'E');
                valor = valor.replace(/í/gi, 'I');
                valor = valor.replace(/ó/gi, 'O');
                valor = valor.replace(/ú/gi, 'U');
                valor = valor.replace(/ñ/gi, 'N');
            }

            if (configuracion && configuracion.normalizeDash) {
                valor = valor.replace(/\-+/g, ' ');
            }

            // Si la normalización permite espacios
            if (configuracion && configuracion.allowSpace) {
                valor = valor.replace(/( )+/gi, ' ');
            } else {
                valor = valor.replace(/\s+/gi, '');
            }

            // Actualizamos el valor
            campo.val(valor);
        }
    }

    /**
     * Función que muestra los errores en la página y un mensaje con los detalles.
     *
     * @param errores la lista de errores de la forma {id, mensaje}.
     */
    $.validation.showErrors = function (errores) {
        if (errores && errores.length > 0) {
            var mensaje = utils.obtenTraduccion(traduccion, 'mensaje', lang);

            for (var i = 0; i < errores.length; i++) {
                var element = $('#' + errores[i].campo).get(0);
                var configuracion = $.data(element, 'configuracion');

                $(element).addClass(this.errorClass);
                $('label[for="' + errores[i].campo + '"]').addClass(this.errorLabelClass);

                mensaje += ' * ' + configuracion.label.es + ' '
                        + errores[i].mensaje + '\n';
            }

            alert(mensaje);
        }
    }

    /**
     * Función que inicializa un elemento a partir de su configuración, por lo que asume que ya
     * se hizo una configuración previa. Por default esta función agrega los eventos para validar al
     * cambiar, se puede volver a llamar agregan 'true' como segundo parámetro.
     *
     * @param element el elemento del dom que se desea configurar.
     * @param events indica si se deben de agregar los eventos.
     */
    $.validation.init = function (element, events) {
        var configuracion = $.data(element, 'configuracion');

        // Si no está configurado, no hacemos nada
        if (configuracion == null) {
            return;
        }

        var jElement = $(element);
        switch (configuracion.type) {
            case 'date':
                jElement.attr('maxlength', configuracion.maxlength);

                // Si se agregan los eventos
                if (events) {
                    jElement.change(function (event) {
                        dateFormat(event, $.data(this, 'configuracion').before, $.validation.today);
                        var error = $.validation.isValid(this);
                        if (!error) {
                            $(this).removeClass($.validation.errorClass);
                            $('label[for="' + this.id + '"]').removeClass($.validation.errorLabelClass);
                            if ($.gobmx) {
                                $(this).parent().find('.valid-field').limpiaErrores();
                            }
                        } else {
                            $(this).addClass($.validation.errorClass);
                            $('label[for="' + this.id + '"]').addClass($.validation.errorLabelClass);
                            if ($.gobmx) {
                                $(this).parent().find('.valid-field').limpiaErrores();
                                $.gobmx.showErrors([error]);
                            }
                        }
                    });
                }
                break;
            case 'point':
                jElement.attr('maxlength', configuracion.maxlength);
                if (events) {
                    jElement.change(function (e) {
                        pointFormat(e) ;
                        var error = $.validation.isValid(this);
                        if (!error) {
                            $(this).removeClass($.validation.errorClass);
                            $('label[for="' + this.id + '"]').removeClass($.validation.errorLabelClass);
                            if ($.gobmx) {
                                $(this).parent().find('.valid-field').limpiaErrores();
                            }
                        } else {
                            $(this).addClass($.validation.errorClass);
                            $('label[for="' + this.id + '"]').addClass($.validation.errorLabelClass);
                            if ($.gobmx) {
                                $(this).parent().find('.valid-field').limpiaErrores();
                                $.gobmx.showErrors([error]);
                            }
                        }
                    });
                }
                break;
            case 'select':
                // Si se agregan los eventos
                if (events) {
                    jElement.change(function (event) {
                        var error = $.validation.isValid(this);
                        if (!error) {
                            $(this).removeClass($.validation.errorClass);
                            $('label[for="' + this.id + '"]').removeClass($.validation.errorLabelClass);
                            if ($.gobmx) {
                                $(this).parent().find('.valid-field').limpiaErrores();
                            }
                        } else {
                            $(this).addClass($.validation.errorClass);
                            $('label[for="' + this.id + '"]').addClass($.validation.errorLabelClass);
                            if ($.gobmx) {
                                $(this).parent().find('.valid-field').limpiaErrores();
                                $.gobmx.showErrors([error]);
                            }
                        }
                    });
                }
                break;
            case 'hour':
                jElement.attr('maxlength', configuracion.maxlength);

                // Si se agregan los eventos
                if (events) {
                    jElement.change(function (event) {
                        hourFormat(event);
                        var error = $.validation.isValid(this);
                        if (!error) {
                            $(this).removeClass($.validation.errorClass);
                            $('label[for="' + this.id + '"]').removeClass($.validation.errorLabelClass);
                            if ($.gobmx) {
                                $(this).parent().find('.valid-field').limpiaErrores();
                            }
                        } else {
                            $(this).addClass($.validation.errorClass);
                            $('label[for="' + this.id + '"]').addClass($.validation.errorLabelClass);
                            if ($.gobmx) {
                                $(this).parent().find('.valid-field').limpiaErrores();
                                $.gobmx.showErrors([error]);
                            }
                        }
                    });
                }
                break;
            default:
                if (configuracion.maxlength != null) {
                    jElement.attr('maxlength', configuracion.maxlength);

                }

                // Si se agregan los eventos.
                if (events) {
                    jElement.change(function (event) {
                        $.validation.format(event.target);
                        var error = $.validation.isValid(this);
                        if (!error) {
                            $(this).removeClass($.validation.errorClass);
                            $('label[for="' + this.id + '"]').removeClass($.validation.errorLabelClass);
                            if ($.gobmx) {
                                $(this).parent().find('.valid-field').limpiaErrores();
                            }
                        } else {
                            $(this).addClass($.validation.errorClass);
                            $('label[for="' + this.id + '"]').addClass($.validation.errorLabelClass);
                            if ($.gobmx) {
                                $(this).parent().find('.valid-field').limpiaErrores();
                                $.gobmx.showErrors([error]);
                            }
                        }
                    });
                }
        }

        // Vemos si es requerido
        var label2 = configuracion.label.es;
        label2 = label2.replace(":", "");
        $('label[for="' + element.id + '"]').html(configuracion.required
                ? (label2 + '<span class="required-tag">*</span>: ')
                : (configuracion.label.es));
    }

    /**
     * Función encargada de inicializar la validación, configurando los campos y agregando los
     * listeners encargados de marcar los campos en error.
     *
     * @return el arreglo de objetos de jquery que se inicializa.
     */
    $.fn.validacion = function () {
        return this.each(function () {
            $.validation.init(this, true);
        })
    }

    /**
     * Función que configura (o reconfigura) los elementos seleccionados.
     *
     * @param params un mapa con los parámetros de configuración. Si es nulo genera una
     * configuración por default para cada elemento.
     * @return un arreglo con los objetos de jquery que fueron configurados.
     */
    $.fn.configura = function (params) {
        return this.filter('input:text,input:not([attr="type"]),input:password,select,textarea').each(function () {
            configuraElemento(this, params);
        });
    }

    /**
     * Función que valida los elementos seleccionados. Como efecto lateral se pueden mostrar los
     * errores o no.
     *
     * @param errores el arreglo de errores.
     * @param muestraErrores indica si una vez que se validan, se muestran los errores.
     */
    $.fn.valida = function (errores, muestraErrores) {
        if (muestraErrores) {
            errores = errores != null ? errores : [];
        }

        this.filter('input:text,input:not([attr="type"]),input:password,select,textarea').each(function () {
            var error = $.validation.isValid(this);
            if (error) {
                errores.push(error);
            }
        });

        if (muestraErrores) {
            $.validation.showErrors(errores);
        }
    }

    /**
     * Limpia los errores eliminando la clase por default o la que se pase como parámetro.
     *
     * @param errorClass el nombre de la clase de error, si es null, usa 'error-field'.
     * @return un arreglo de objetos jQuery.
     */
    $.fn.limpiaErrores = function (errorClass, errorLabelClass) {
        errorClass = errorClass ? errorClass : $.validation.errorClass;
        errorLabelClass = errorLabelClass ? errorLabelClass : $.validation.errorLabelClass;

        var elements = this.filter('input:text,input:not([attr="type"]),select,textarea');

        $.each(elements, function () {
            $('label[for="' + this.id + '"]').removeClass(errorLabelClass);
            $('#' + this.id).parent().find("." + $.validation.errorMessageClass).remove();
        });

        return elements.removeClass(errorClass);
    }



    var lang = 'es';
    var traduccion = {mensaje: {en: 'Please verify these spaces \n\n', ru: 'Проверьте следующие поля: \n\n', pt: 'Verifique os seguintes campos: \n\n', es: 'Verifique los siguientes campos:\n\n'}};
    traduccion.requerido = {en: 'Required', ru: 'Требуется', pt: 'É necessário', es: 'este campo es obligatorio'};
    traduccion.noCumpleFormato = {en: 'Does not meet the format.', ru: 'Не соответствует формату', pt: 'Não está em conformidade com o formato', es: 'no cumple con el formato.', ja: 'メールアドレスのフォーマットが正しくありません。'};
    traduccion.fechaNoValida = {en: 'Is not a valid date', ru: 'Эта дата не допустима', pt: 'Não é uma data válida', es: 'no es una fecha válida.'};
    traduccion.fechaMenor = {en: 'Is not a valid date', ru: 'Ниже минимальной даты', pt: 'É menor que a data mínima.', es: 'es menor a la fecha mínima.', ja: '最低期日以下の日付になっています。'};
    traduccion.fechaMayor = {en: 'Is not a valid date', ru: 'Выше максимальной даты', pt: 'É maior que a data máxima.', es: 'es mayor a la fecha máxima.', ja: '最高期日以上の日付になっています。'};
    traduccion.numeroNoValido = {en: 'Is not a valid number', ru: 'Номер не действителен', pt: 'Não é um número válido', es: 'no es un número válido.'};
    traduccion.numeroMenor = {en: 'Is lower than the minimum number', ru: 'Ниже минимального числа', pt: 'É inferior ao mínimo', es: 'es menor al número mínimo.'};
    traduccion.numeroMayor = {en: 'Is higher than the maximum number', ru: 'Выще максимального числа', pt: 'É mais do que o máximo', es: 'es mayor al número máximo.'};
    traduccion.horaNoValida = {en: 'Is not a valid hour', ru: 'Это время не является допустимым', pt: 'Não é uma hora válida', es: 'no es una hora válida.'};
    traduccion.longitudMinima = {en: 'Does not meet the minimum length', ru: 'Не соответствует минимальной длине', pt: 'Não satifaz  o  comprimento mínimo', es: 'no cumple con la longitud mínima.'};
    traduccion.longitudMaxima = {en: 'Does not meet the maximum length', ru: 'Не соответствует максимальной длине', pt: 'Não satifaz o comprimento máximo', es: 'no cumple con la longitud máxima.'};

    utils.obtenTraduccion = function (traducciones, propiedad, idioma) {
        var traduccion = traducciones[propiedad] ? (traducciones[propiedad][idioma] ?
                traducciones[propiedad][idioma] : traducciones[propiedad]['es']) : null;
        return traduccion;
    };


})(jQuery);