/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($) {

    var data = {
        archivos: [],
        urlComprobante: 'https://localhost',
        callBackCerrar: function () {
            alert("Error", 'Se debe de configurar una función para cerrar.');
        },
        pendientes: [],
        adjuntos: 0,
        indicaciones: 'Por favor espere mientras se suben los documentos anexos. Al finalizar podrá descargar el registro, el cual deberá imprimir, firmar y subir al sistema en la pantalla de <strong>Capturas</strong>.',
        btnDescarga: true
    };

    $.segalmex.namespace('segalmex.archivos');

    var handlers = {};
    var utils = {};

    $.segalmex.archivos.validaArchivos = function (expresion) {
        var files = $(expresion);
        var texto = [];
        var errores = false;
        for (var i = 0; i < files.length; i++) {
            if ($(files[i]).val() === '') {
                errores = true;
                texto.push(' * Seleccione el archivo para: ' + $('label[for=' + files[i].id + ']').html() + '\n');
            }
        }
        if (errores) {
            alert('Verifique lo siguiente:\n\n' + texto.join(''));
        }
        return errores;
    };

    $.segalmex.archivos.verificaArchivo = function (e, extension, maximo) {
        var tamano = e.target.files[0].size;
        var error = '';
        var val = $(e.target).val();
        var re = new RegExp('\\.' + extension + '$', 'i');
        if (!re.test(val)) {
            error = ' * La extensión del archivo no es ' + extension.toUpperCase() + '.';
        }
        if (tamano !== undefined && tamano + 1024 >= maximo) {
            error += (error ? '\n' : '') + ' * El tamaño del archivo es mayor a ' + (maximo / (1024 * 1024)) + ' MB.'
        }
        if (error) {
            alert('Error:\n\n' + error);
            $(e.target).val('');
        }
    };

    $.segalmex.archivos.generaAnexo = function (id, index) {
        var anexo = $('#' + id).clone();
        anexo.attr('id', index + '_' + id);
        return anexo;
    };

    utils.getTemplate = function () {
        return `<div id="descarga-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="descarga-title" aria-hidden="true" data-backdrop="static" style="overflow-y: auto;">
        <div class="modal-dialog modal-lg">
          <div class="modal-content">
            <div class="modal-header">
              <h5 id="descarga-title">Carga de documentos</h5>
            </div>
            <div class="modal-body">
              ${data.indicaciones}
              <table id="anexos-table" class="table table-bordered no-archivos">
                <thead class="thead-dark">
                  <tr>
                    <th>#</th>
                    <th>Nombre</th>
                    <th class="text-center">Estatus</th>
                  </tr>
                </thead>
                <tbody>
                </tbody>
              </table>
              <br/>
              <div class="progress no-archivos">
                <div class="progress-bar progress-bar-striped bg-success" role="progressbar" style="width: 0%" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100">0%</div>
              </div>
              <br/>
              <div class="information"></div>
            </div>
            <div class="modal-footer">
              <button id="cerrar-descarga" type="button" class="btn btn-outline-secondary">Cerrar</button>
              <button id="reenviar-archivos" type="button" class="btn btn-outline-secondary" disabled="disabled">Reenviar archivos</button>
              <a id="descargar-inscripcion-link" href="#" role="button" class="btn btn-outline-success disabled">Descargar registro</a>
            </div>
          </div>
        </div>
      </div>`;
    };

    utils.construyeArchivos = function (files) {
        var buffer = [];
        var index = 1;
        for (var i = 0; i < files.length; i++) {
            var file = files[i];
            buffer.push('<tr>');
            buffer.push('<td>');
            buffer.push(index++);
            buffer.push('</td>');
            buffer.push('<td>');
            buffer.push(file.etiqueta);
            buffer.push('</td>');
            buffer.push('<td id="estatus-' + file.id + '" class="text-center">');
            buffer.push('<i class="fas fa-upload"></i>');
            buffer.push('</td>');
            buffer.push('</tr>');
        }

        return buffer.join('\n');

    };

    /**
     * Función para cargar/enviar (al servidor), una lista de archivos asociados a un registro/inscripción.
     * @param {type} params un objeto con su configuración.
     * params: {archivos : la lista de archivos a enviar/cargar, deben traer id, FormData, urlEnvioArchivo, etiquetaMostrarEnElModal.
     urlComprobante: la url para descargar un PDF derivado de la inscripción/registro.
     callBackCerrar: función que debe ejecutarse al cerrar el modal de carga archivos.
     }
     * @returns el modal de carga de archivos.
     */
    $.fn.cargaArchivos = function (params) {
        data = $.extend({}, data, params);
        this.each(function () {
            var e = $(this);
            e.html(utils.getTemplate());
            data.btnDescarga ? $('#descargar-inscripcion-link').show() : $('#descargar-inscripcion-link').hide();
            if (params.archivos.length === 0) {
                $('.no-archivos').hide();
            }
        });

        $('#descarga-modal .modal-footer button.btn').prop('disabled', true);
        $('#anexos-table tbody').html(utils.construyeArchivos(data.archivos));
        $('#descarga-modal').modal('show'); //intercambio de orden, promero armas luego muestras
        $('#reenviar-archivos').click(handlers.reenviaArchivos);
        $('#cerrar-descarga').click(handlers.cerrar);
        utils.procesaArchivos(data.archivos);
        return this;
    };

    utils.procesaArchivos = function (archivos) {
        if (archivos.length === 0) {
            $('#cerrar-descarga').prop('disabled', false);
            $('#descargar-inscripcion-link').removeClass('disabled');
            $('#descargar-inscripcion-link').attr('href', data.urlComprobante);
        }

        var pendientesTmp = [];
        for (var i = 0; i < archivos.length; i++) {
            (function (dato, ultimo) {
                $.ajaxq('archivosQueue', {
                    url: dato.url,
                    type: 'POST',
                    data: dato.fd,
                    contentType: false,
                    processData: false
                }).done(function () {
                    utils.cargaBarraProgreso(++data.adjuntos, data.archivos.length);
                    $('#estatus-' + dato.id).html('<i class="fas fa-check-circle text-success"></i> Adjuntado');
                }).fail(function () {
                    pendientesTmp.push(dato);
                    $('#estatus-' + dato.id).html('<i class="fas fa-times-circle text-danger"></i> No adjuntado');
                }).always(function () {
                    if (ultimo) {
                        if (pendientesTmp.length === 0) {
                            $('#cerrar-descarga').prop('disabled', false);
                            $('#descargar-inscripcion-link').removeClass('disabled');
                            $('#descargar-inscripcion-link').attr('href', data.urlComprobante);
                        } else {
                            $('#reenviar-archivos').prop('disabled', false);
                        }
                    }
                });
            })(archivos[i], i === archivos.length - 1);
        }
        data.pendientes = pendientesTmp;
    };

    utils.cargaBarraProgreso = function (actual, total) {
        if (total === 0) {
            $('.progress-bar').css('width', 100 + '%');
            $('.progress-bar').attr('aria-valuenow', 100);
            $('.progress-bar').text('Completado ' + 100 + '%');
        } else {
            var progreso = Math.trunc(actual * 100 / total);
            $('.progress-bar').css('width', progreso + '%');
            $('.progress-bar').attr('aria-valuenow', progreso);
            $('.progress-bar').text('Completado ' + progreso + '%');
        }
    };

    handlers.reenviaArchivos = function (e) {
        e.preventDefault();
        $('#descarga-modal .modal-footer button.btn').prop('disabled', true);
        utils.procesaArchivos(data.pendientes);
    };

    handlers.cerrar = function (e) {
        e.preventDefault();
        $('#descargar-inscripcion-link').attr('href', '#').addClass('disabled');
        $('#descarga-modal').modal('hide');
        data.pendientes = [];
        data.adjuntos = 0;
        data.callBackCerrar();
    };


})(jQuery);