(function ($) {
    $.segalmex.namespace('segalmex.common.pagina');

    var data = {};
    var handlers = {};
    var utils = {};

    $.segalmex.common.pagina.init = function (params) {
        data.context = '/' + window.location.pathname.split('/')[1];
        params = params || {};
        utils.buildFooter();
        if (params.publico) {
            $('main').before('<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">'
                    + '<div class="container">'
                    + '<a class="navbar-brand" href="#"><img height="32" src="/util/images/segalmex-brand.png" alt="Segalmex" /> Segalmex</a>'
                    + '</div>'
                    + '</nav>');
            return;
        }
        utils.buildMenu();
        utils.getComun(false);
        $('#salir-link').click(handlers.salir);
        if (params.dateTimer) {
            utils.dateTimer();
        }
    }

    /**
     * Mantiene actualizada la fecha y hora cada 5 minutos. Permite que la sesión no se pierda.
     */
    utils.dateTimer = function () {
        setInterval(function () {
            utils.getComun(true);
        }, 300000);
    }

    handlers.salir = function (e) {
        e.preventDefault();
        if (confirm('¿Realmente desea salir del sistema?')) {
            for (var contexto in $.segalmex.contextos) {
                if ($.segalmex.contextos[contexto] !== data.context) {
                    $.get($.segalmex.contextos[contexto] + '/j_spring_security_logout');
                }
            }
            setTimeout(function () {
                window.location.href = data.context + '/j_spring_security_logout';
            }, 1500);
        }

    }

    utils.buildMenu = function () {
        var menu = '<nav class="navbar navbar-expand-md navbar-dark bg-dark fixed-top">'
                + '<div class="container">'
                + '<a class="navbar-brand" href="#"><img height="32" src="/util/images/segalmex-brand.png" alt="Segalmex" /> Segalmex</a>'
                + '<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#menu-granos" aria-controls="menu-granos" aria-expanded="false" aria-label="Toggle navigation">'
                + '<span class="navbar-toggler-icon"></span>'
                + '</button>'
                + '<div class="collapse navbar-collapse" id="menu-granos">'
                + '<ul class="navbar-nav mr-auto">'
                + '<li class="nav-item">'
                + '<a class="nav-link" href="/pluss/principal.html">Inicio</a>'
                + '</li>'
                + '<li class="nav-item dropdown">'
                + '<a class="nav-link dropdown-toggle" href="#" id="dropdown01" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Precios Garantía</a>'
                + '<ul class="dropdown-menu" aria-labelledby="dropdown01">'
                + '<li><a class="dropdown-item" href="/cultivos/principal.html">Cultivos</li>'
                + '<li><a class="dropdown-item" href="/maiz/principal.html">Maíz</a></li>'
                + '<li><a class="dropdown-item" href="/trigo/principal.html">Trigo</a></li>'
                + '<li><a class="dropdown-item" href="/arroz/principal.html">Arroz</a></li>'
                + '</ul>'
                + '</li>'
                + '<li class="nav-item">'
                + '<a class="nav-link" href="/usuarios/principal.html">Usuarios</a>'
                + '</li>'
                + '</ul>'
                + '<div class="my-2 my-lg-0">'
                + '<ul class="navbar-nav mr-auto">'
                + '<li class="nav-item">'
                + '<a class="nav-link" href="/usuarios/perfil.html"><i class="fas fa-user"></i> <span id="nombre-usuario-menu"></span></a>'
                + '</li>'
                + '<li class="nav-item">'
                + '<a id="salir-link" class="nav-link" href="#"><i class="fas fa-sign-in-alt"></i> Salir</a>'
                + '</li>'
                + '</ul>'
                + '</div>'
                + '</div>'
                + '</nav>';
        $('main').before(menu);
    }

    utils.buildFooter = function () {
        $('main').after('<footer id="main-footer">'
                + '<div class="container"><p>&nbsp;</p><p>&nbsp;</p></div>'
                + '<div class="container-fluid footer-pleca"></div>'
                + '</footer>');
    }

    utils.getComun = async function (async) {
        if (async) {
            getCommonResources(false);
        } else {
            await getCommonResources(true);
        }
    }

    async function getCommonResources(addHeader) {
        $.ajax({
            url: data.context + '/resources/meta-resources/comun',
            type: 'GET',
            dataType: 'json'
        }).done(function (response) {
            if (!$.segalmex.common.pagina.comun) {
                $.segalmex.common.pagina.comun = response;
                $('#nombre-usuario-menu').html(response.registrado.nombre);
            }
            if (addHeader && response.registrado) {
                $(document).ajaxSend(function (e, request, settings) {
                    request.setRequestHeader('Segalmex-Username', response.registrado.nombre);
                });
            }
            $.segalmex.fecha = response.ahora.date;
            $.segalmex.contextos = response.contextos;
            $.segalmex.dateTime = response.ahora.dateTime;
        }).fail(function (jqXHR, textStatus) {
            if (textStatus === 'parsererror' && jqXHR.status === 200 && jqXHR.responseText.indexOf('<!doctype html>') === 0) {
                alert('Su sesión ha finalizado. Ingrese nuevamente.');
            }
        });
    }
})(jQuery)
