(function ($) {
    var data = {
        ubicacion: ''
    };

    var utils = {};

    utils.construyeModalGmaps = function () {
        var buffer = [];
        buffer.push('<div id="gmaps-modal" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="maps-title" aria-hidden="true" style="display:none;">');
        buffer.push('<div class="modal-dialog modal-lg">');
        buffer.push('<div class="modal-content">');
        buffer.push('<div class="modal-header">');
        buffer.push('<h5 id="gmaps-title">Ubicación</h5>');
        buffer.push('<button type="button" class="close" data-dismiss="modal" aria-label="Cerrar">');
        buffer.push('<span aria-hidden="true">&times;</span>');
        buffer.push('</button>');
        buffer.push('</div>');
        buffer.push('<div class="modal-body">');
        buffer.push('<div class="mapouter">');
        buffer.push('<div class="gmap_canvas">');
        buffer.push('</div>');
        buffer.push('</div>');
        buffer.push('</div>');
        buffer.push('<div class="modal-footer">');
        buffer.push('<button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>');
        buffer.push('</div>');
        buffer.push('</div>');
        buffer.push('</div>');
        buffer.push('</div>');
        return buffer.join('\n');
    };


    $.fn.muestraMaps = function (params) {
        data = $.extend({}, data, params);
        this.each(function () {
            var e = $(this);
            e.html(utils.construyeModalGmaps());
            data.ubicacion = data.ubicacion.substring(1, data.ubicacion.length - 1);
            $('#gmaps-modal div.gmap_canvas').html('<iframe src="https://www.google.com/maps/embed/v1/place?key=AIzaSyAFehJTlBU0txJqJFm_blMBpipC_ijNNMU&q=' + data.ubicacion + '" frameborder="0" style="border:0" scrolling="no" marginheight="0" marginwidth="0"></iframe>');
            $('#gmaps-modal').modal('show');
        });
        return this;
    };

})(jQuery);


