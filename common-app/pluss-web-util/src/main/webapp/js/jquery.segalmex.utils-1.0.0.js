(function ($) {
    if (!$.segalmex) {
        $.segalmex = {};
    }

    if (!$.segalmex.date) {
        $.segalmex.date = {};
    }

    $.segalmex.date.fechaToIso = function (fecha) {
        if (/\d{2}\/\d{2}\/\d{4}/.test(fecha)) {
            var iso = fecha.substring(6, 10) + '-' + fecha.substring(3, 5) + '-' + fecha.substring(0, 2);
            return iso;
        }
        return null;
    };

    $.segalmex.date.isoToFecha = function (iso) {
        var nueva = iso;
        if (/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{3}){0,1}-\d{2}:\d{2}/.test(iso)) {
            nueva = iso.substring(8, 10) + '/' + iso.substring(5, 7) + '/' + iso.substring(0, 4);
        }
        return nueva;
    };

    $.segalmex.date.isoToFechaHora = function (iso) {
        var nueva = iso;
        if (/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{3}){0,1}-\d{2}:\d{2}/.test(iso)) {
            nueva = iso.substring(8, 10) + '/' + iso.substring(5, 7) + '/' + iso.substring(0, 4) +
                    '  ' + iso.substring(11, 16);
        }
        return nueva;
    };

    $.segalmex.date.isoToIsoHora = function (iso) {
        var nueva = iso;
        if (/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{3}){0,1}-\d{2}:\d{2}/.test(iso)) {
            nueva = iso.substring(0, 4) + '-' + iso.substring(5, 7) + '-' + iso.substring(8, 10) +
                    '  ' + iso.substring(11, 16);
        }
        return nueva;
    };

    $.segalmex.date.isoToIsoFecha = function (iso) {
        var nueva = iso;
        if (/\d{4}-\d{2}-\d{2}T\d{2}:\d{2}:\d{2}(\.\d{3}){0,1}-\d{2}:\d{2}/.test(iso)) {
            nueva = iso.substring(0, 4) + '-' + iso.substring(5, 7) + '-' + iso.substring(8, 10);
        }
        return nueva;
    };

    $.segalmex.date.isoToHora = function (iso) {
        var nueva = iso;
        if (/\d{2}:\d{2}/.test(iso)) {
            nueva = iso.substring(11, 16);
        }
        return nueva;
    };

    $.segalmex.date.fechaHoraToIso = function (fecha, hora) {
        if (/\d{2}\/\d{2}\/\d{4}/.test(fecha)) {
            var iso = fecha.substring(6, 10) + '-' + fecha.substring(3, 5) + '-' + fecha.substring(0, 2) + 'T';

            if (hora) {
                var temp = [];
                temp = hora.split(':');
                hora = (temp[0].length < 2 ? '0' + temp[0] : temp[0]) + ':' + temp[1];
            }

            iso += hora ? hora : '00:00';
            iso += ':00-05:00';
            return iso;
        }
        return null;
    };

    $.segalmex.date.agregaDias = function (hoy, dias) {
        var dint = parseInt(hoy.substring(0, 2), 10);
        var mint = parseInt(hoy.substring(3, 5), 10) - 1;
        var yint = parseInt(hoy.substring(6, 10), 10);

        var extra = new Date(yint, mint, dint);
        extra.setDate(extra.getDate() + dias);

        var yyyy = extra.getFullYear();
        var mm = extra.getMonth() + 1;
        var dd = extra.getDate();
        var fecha = yyyy + '/' + (mm > 9 ? mm : '0' + mm) + '/' + (dd > 9 ? dd : '0' + dd);
        return fecha;
    };

    /**
     *Regresa un entero representando la edad que tiene una persona con
     *fecha de nacimiento especificada en formato iso.
     */
    $.segalmex.date.edad = function (fecha) {
        if (!/^\d{4}-\d{2}-\d{2}/.test(fecha)) {
            return -1;
        }

        var actual = new Date();
        var anac = fecha.substring(0, 4);
        var mnac = fecha.substring(5, 7);
        var dnac = fecha.substring(8, 10);

        var edad = actual.getFullYear() - anac;
        if (mnac > (actual.getMonth() + 1) ||
                (mnac == (actual.getMonth() + 1) && dnac > actual.getDate())) {
            edad--;
        }
        return edad;
    };

    /**
     * Filtra una arreglo de objetos a partir de una propiedad.
     *
     * @param valores
     * @param propiedad
     * @return un arreglo de la misma longitud del que pasaron.
     */
    $.segalmex.filtraPropiedad = function (valores, propiedad) {
        var length = valores.length;
        var resultado = [];
        for (var i = 0; i < length; i++) {
            resultado.push(valores[i][propiedad]);
        }
        return resultado;
    };

    $.segalmex.contains = function (valores, valor, propiedad) {
        propiedad = !propiedad ? 'id' : propiedad;

        for (var i = 0; i < valores.length; i++) {
            if (valores[i][propiedad] == valor[propiedad]) {
                return true;
            }
        }
        return false;
    };

    $.segalmex.unique = function (valores, propiedad) {
        propiedad = !propiedad ? 'id' : propiedad;

        var unicos = [];
        for (var i = 0; i < valores.length; i++) {
            var tmp = valores[i];
            if ($.isArray(tmp)) {
                for (var j = 0; j < tmp.length; j++) {
                    if (!$.segalmex.contains(unicos, tmp[j])) {
                        unicos.push(tmp[j]);
                    }
                }
            } else {
                if (!$.segalmex.contains(unicos, tmp, propiedad)) {
                    unicos.push(tmp);
                }
            }
        }
        return unicos;
    };

    $.segalmex.get = function (valores, valor, propiedad) {
        propiedad = !propiedad ? 'id' : propiedad;

        var size = valores.length;
        for (var i = 0; i < size; i++) {
            if (valores[i][propiedad] == valor) {
                return valores[i];
            }
        }
        return null;
    };

    /**
     * Función que actualiza un combo a partir del arreglo de valores que se le pasa. Por default
     * utiliza el nombre de la propiedad id para el valor de la opción y nombre para el valor de la
     * etiqueta.
     *
     * @param valores el arreglo de valores con los que se construye el combo.
     * @param params las opciones en donde se especifica el valor que debería ser usando en la
     * opción y un arreglo de nombre de propiedades para construir la etiqueta.
     * @param label --
     * @return el conjunto de combos actualizados. Si los combos fueron vacíos se deshabilitan.
     */
    $.fn.actualizaCombo = function (valores, params) {
        valores = valores ? valores : [];
        params = params || {};
        var etiqueta = params.first ? params.first : 'Seleccione';

        var normaliza = false;
        if (params.normaliza !== undefined) {
            normaliza = params.normaliza;
        }

        var combo = [];
        if (!params.omitFirst) {
            combo.push('<option value="0">' + etiqueta + '</option>');
        }

        for (var i = 0; i < valores.length; i++) {
            var valor = valores[i];
            combo.push('<option value="');
            combo.push(params.value ? valor[params.value] : valor.id);
            combo.push('">');
            var nombre = params.nombre ? valor[params.nombre] : valor.nombre;
            if (normaliza) {
                nombre = $.segalmex.normalizaTexto(nombre);
            }
            combo.push(nombre);
            combo.push('</option>');
        }
        var html = combo.join('');
        return this.filter('select').each(function () {
            this.options.length = 0;
            if (combo.length === 1) {
                $(this).html(html).disable();
            } else {
                $(this).html(html).enable();
            }
        });
    };

    $.fn.disable = function () {
        return this.prop('disabled', true).addClass('disabled');
    };

    $.fn.enable = function () {
        return this.prop('disabled', false).removeClass('disabled');
    };

    /**
     * Obtiene la etiqueta seleccionada de un combo box
     */
    $.fn.getLabel = function () {
        var a = "";
        this.each(function () {
            if (this.tagName === 'SELECT') {
                var selectElement = this;
                a = selectElement.options[selectElement.options.selectedIndex].text;
            }
        });
        return a;
    };

    /**
     * Genera el mensaje a mostrar dependiendo de la respuesta del servidor
     */
    $.segalmex.generaMensajeResponse = function (respuesta) {
        var texto = "";
        if (respuesta.mensaje) {
            texto = respuesta.mensaje;
        } else {
            texto = respuesta.mensajeError ? respuesta.mensajeError : "";
            if (respuesta.causas && respuesta.causas.length > 0) {
                texto = texto + "\n"
                $.each(respuesta.causas, function () {
                    texto = texto + "- " + this + "\n";
                });
            }
        }
        return texto;
    };


    /**
     * Método que realiza una sustitución de caracteres especiales a una cadena.
     *
     * @param nombre la cadena a normalizar.
     * @return una cadena normalizada.
     */
    $.segalmex.normalizaTexto = function (texto) {
        var acentos = /[ÁÉÍÓÚáéíóú]/;
        if (acentos.test(texto)) {
            var valor = texto;
            valor = valor.replace(/Á/g, 'A');
            valor = valor.replace(/É/g, 'E');
            valor = valor.replace(/Í/g, 'I');
            valor = valor.replace(/Ó/g, 'O');
            valor = valor.replace(/Ú/g, 'U');
            valor = valor.replace(/á/g, 'a');
            valor = valor.replace(/é/g, 'e');
            valor = valor.replace(/í/g, 'i');
            valor = valor.replace(/ó/g, 'o');
            valor = valor.replace(/ú/g, 'u');

            return valor;
        } else {
            return texto;
        }
    };

    $.segalmex.namespace = function (ns) {
        var names = ns.split('.');
        var base = $;
        for (var i = 0; i < names.length; i++) {
            if (!base[names[i]]) {
                base[names[i]] = {};
            }
            base = base[names[i]];
        }
    };

    $.segalmex.getParameterByName = function (name, url) {
        if (!url) {
            url = window.location.href;
        }
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)');
        var results = regex.exec(url);
        if (!results) {
            return null;
        }
        if (!results[2]) {
            return '';
        }
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    /**
     * Función util para verificar que las fechas no estan cruzadas.
     * @param {type} errores el arreglo de los errores.
     * @param {type} idFechaInicial identificador para la fecha inicial.
     * @param {type} idFechaFin identificador para la fecha fin.
     * @returns {undefined}
     */
    $.segalmex.validaFechaInicialContraFinal = function (errores, idFechaInicial, idFechaFin) {
        //verificamos que no sean vacios
        if (($('#' + idFechaInicial).val() !== '') && ($('#' + idFechaFin).val() !== '')) {
            var fechaInicial = $.segalmex.date.fechaToIso($('#' + idFechaInicial).val());
            var fechaFinal = $.segalmex.date.fechaToIso($('#' + idFechaFin).val());

            var inicialValida = false;
            var finalValida = false;
            //verificamos que las fechas tengan formato correcto
            if (/^\d{4}-\d{2}-\d{2}/.test(fechaInicial)) {
                inicialValida = true;
            }

            if (/^\d{4}-\d{2}-\d{2}/.test(fechaFinal)) {
                finalValida = true;
            }

            //si tienen formato correcto las fechas, verificamos que la fecha inicial no sea mayor a la final.
            if (inicialValida && finalValida) {
                fechaInicial = fechaInicial.replace(/-/g, '/');
                fechaFinal = fechaFinal.replace(/-/g, '/');
                if (fechaInicial > fechaFinal) {
                    errores.push({campo: idFechaInicial, mensaje: 'debe ser menor ó igual a la fecha final.'});
                }
            }
        }
    };

    $.segalmex.openPdf = function (url) {
        var explorer = !$.support.cssFloat;
        if (explorer) {
            var a = $('<a id="anchorin-pdf-reloaded" target="_blank" style="display:none"></a>');
            $("body").append(a);
            a.attr("href", url)[0].click();
            a.remove();
        } else { // Para el resto de los navegadores.
            window.open(url);
        }
    };

})(jQuery);
