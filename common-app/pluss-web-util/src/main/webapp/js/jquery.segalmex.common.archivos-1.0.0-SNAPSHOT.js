/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
(function ($) {
    $.segalmex.namespace('segalmex.archivos');

    $.segalmex.archivos.validaArchivos = function (expresion) {
        var files = $(expresion);
        var texto = [];
        var errores = false;
        for (var i = 0; i < files.length; i++) {
            if ($(files[i]).val() === '') {
                errores = true;
                texto.push(' * Seleccione el archivo para: ' + $('label[for=' + files[i].id + ']').html() + '\n');
            }
        }
        if (errores) {
            alert('Verifique lo siguiente:\n\n' + texto.join(''));
        }
        return errores;
    };

    $.segalmex.archivos.verificaArchivo = function (e, extension, maximo) {
        var tamano = e.target.files[0].size;
        var error = '';
        var val = $(e.target).val();
        var re = new RegExp('\\.' + extension + '$', 'i');
        if (!re.test(val)) {
            error = ' * La extensión del archivo no es PDF.';
        }
        if (tamano !== undefined && tamano + 1024 >= maximo) {
            error += (error ? '\n' : '') + ' * El tamaño del archivo es mayor a ' + (maximo / (1024 * 1024)) + ' MB.'
        }
        if (error) {
            alert('Error:\n\n' + error);
            $(e.target).val('');
        }
    };

    $.segalmex.archivos.generaAnexo = function (id, index) {
        var anexo = $('#' + id).clone();
        anexo.attr('id', index + '_' + id);
        return anexo;
    }

})(jQuery)