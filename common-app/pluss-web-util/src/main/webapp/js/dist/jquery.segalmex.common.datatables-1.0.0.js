(function ($) {
    $.extend($.fn.dataTableExt.oSort, {
        "date-custom-pre": function (a) {
            var gemasDate = a.split('/'); //Split para sacar el día, mes, año
            if (gemasDate.length !== 3) {
                return 0;
            }
            return (gemasDate[2] + gemasDate[1] + gemasDate[0]) * 1;
        },
        "date-custom-asc": function (a, b) {
            return ((a < b) ? -1 : ((a > b) ? 1 : 0));
        },
        "date-custom-desc": function (a, b) {
            return ((a < b) ? 1 : ((a > b) ? -1 : 0));
        },
        "date-time-custom-pre": function (a) {
            var fechaHora = $.trim(a).split(' '); //Obtenemos la fecha y el tiempo
            var gemasDate = fechaHora[0].split('/');//Obtenemos el día, mes, año
            var gemasTime = fechaHora[2].split(':');//Obtenemos la hora y minuto. Nota: se toma la posición dos porque en el isoToFecha le dan doble espacio
            return (gemasDate[2] + gemasDate[1] + gemasDate[0] + gemasTime[0] + gemasTime[1]) * 1;
        },
        "date-time-custom-asc": function (a, b) {
            return ((a < b) ? -1 : ((a > b) ? 1 : 0));
        },
        "date-time-custom-desc": function (a, b) {
            return ((a < b) ? 1 : ((a > b) ? -1 : 0));
        }
    });
})(jQuery);