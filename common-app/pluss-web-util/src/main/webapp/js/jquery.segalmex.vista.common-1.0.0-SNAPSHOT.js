(function ($) {
    $.segalmex.namespace('segalmex.common.vista');
    $.segalmex.namespace('segalmex.util');

    $.segalmex.common.vista.generaMensajeExcepcion = function (response) {
        var texto = "";
        if (response.motivos) {
            $.each(response.motivos, function () {
                texto += "• " + this + "\n";
            });
        }
        var mensaje = "";
        if (texto === "") {
            mensaje = response.mensaje;
        } else {
            mensaje = response.mensaje + "\n\n" + texto;
        }
        return mensaje;
    };

    $.segalmex.util.format = function (str, fix) {
        if (isNaN(str)) {
            return '--';
        }
        let num = (typeof str === 'string') ? parseFloat(str) : str;
        let dec = fix ? fix : 3;
        return num.toFixed(dec).toString().replace(/(\d)(?=(\d{dec})+(?!\d))/g, '$1,');
    };

})(jQuery);