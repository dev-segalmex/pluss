(function ($) {
    var data = {
        id: 'ciclo-agricola',
        ciclos: [],
        reload: false,
        sistema: 'maiz',
        showButton: true,
        onlyRead: false
    };

    var handlers = {};
    var utils = {};

    handlers.modificaCicloAgricola = function (e) {
        e.preventDefault();
        $(e.target).prop('disabled', true);
        var val = $('#ciclo-agricola-global-select').val();
        switch (val) {
            case '0':
                alert('Especifique un ciclo agrícola.');
                $(e.target).prop('disabled', false);
                break;
            default:
                var ciclo = $.segalmex.get(data.ciclos, val);
                if (data.actual && data.actual.id === ciclo.id) {
                    alert('Especifique un ciclo agrícola distinto al actual.');
                    $(e.target).prop('disabled', false);
                    return;
                }

                $.ajax({
                    url: '/' + data.sistema + '/resources/cultivos/ciclo-agricola/' + ciclo.clave + '/seleccion/',
                    type: 'POST',
                    dataType: 'json'
                }).done(function (response) {
                    if (data.reload) {
                        window.location.reload();
                    }
                    $(e.target).prop('disabled', false);
                }).fail(function () {
                    alert('Error: No fue posible modificar el ciclo agrícola seleccionado.');
                    $(e.target).prop('disabled', false);
                });
        }
    };

    utils.getTemplate = function () {
        return '<div class="card mb-4">'
                + '<h3 class="card-header">Ciclo agrícola</h3>'
                + '<div class="card-body">'
                + '<div class="input-group input-group-lg">'
                + '<select id="' + data.id + '" class="form-control" disabled="disabled">'
                + '<option value="0">Seleccione</option>'
                + '</select>'
                + '<div class="input-group-append">'
                + '<button id="mostrar-modal-ciclo-agricola-global" class="btn btn-outline-secondary" type="button">Modificar</button>'
                + '</div>'
                + '</div>'
                + '</div>'
                + '</div>'
                + '<div id="ciclo-agricola-global-modal"  class="modal fade" tabindex="-1" role="dialog" aria-labelledby="ciclo-agricola-title" aria-hidden="true" data-backdrop="static" style="display:none;">'
                + '<div class="modal-dialog">'
                + '<div class="modal-content">'
                + '<div class="modal-header">'
                + '<h5 id="ciclo-agricola-title" class="modal-title">Modificar ciclo agrícola</h5>'
                + '</div>'
                + '<div class="modal-body">'
                + '<div class="row">'
                + '<div class="col-md-12 form-group">'
                + '<label for="ciclo-agricola-global-select" class="control-label">Especifique el ciclo agrícola</label>'
                + '<select id="ciclo-agricola-global-select" class="form-control">'
                + '<option value="0">Seleccione</option>'
                + '</select>'
                + '</div>'
                + '</div>'
                + '</div>'
                + '<div class="modal-footer">'
                + '<button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cerrar</button>'
                + '<button id="ciclo-agricola-global-button" type="button" class="btn btn-outline-primary">Modificar</button>'
                + '</div>'
                + '</div>'
                + '</div>'
                + '</div>';
    };


    $.fn.cicloAgricola = function (params) {
        data = $.extend({}, data, params);
        this.each(function () {
            var e = $(this);
            e.html(utils.getTemplate());
            $('#' + data.id).actualizaCombo(data.ciclos).prop('disabled', true);
            $('#ciclo-agricola-global-select').actualizaCombo(data.ciclos);

            // Verificamos si el ciclo actual es una opción válida de los ciclos permitidos
            if (data.actual) {
                var permitido = $.segalmex.get(data.ciclos, data.actual.id);
                if (!permitido) {
                    data.actual = null;
                }
            }

            if (data.actual) {
                $('#' + data.id).val(data.actual.id);
                $('#ciclo-agricola-global-select').val(data.actual.id);
            } else if (!data.onlyRead){
                $('#ciclo-agricola-global-modal').modal('show');
            }
        });
        $('#mostrar-modal-ciclo-agricola-global').click(function (e) {
            e.preventDefault();
            if (data.actual) {
                $('#ciclo-agricola-global-select').val(data.actual.id);
            }
            $('#ciclo-agricola-global-modal').modal('show');
        });
        $('#ciclo-agricola-global-button').click(handlers.modificaCicloAgricola);
        $('#mostrar-modal-ciclo-agricola-global').toggle(data.showButton);
        return this;
    };

})(jQuery);