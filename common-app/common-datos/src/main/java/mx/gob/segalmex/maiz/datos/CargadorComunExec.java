package mx.gob.segalmex.maiz.datos;


import mx.gob.segalmex.common.core.datos.ProcesadorCargadorDatos;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Clase principal que ejecutará la carga de catalogos.
 *
 * @author luiz
 */
public class CargadorComunExec {

    public static void main(String[] args) {
        ApplicationContext context = new ClassPathXmlApplicationContext(new String[]{
            "datosComunApplicationContext.xml",
            "execDatosComunApplicationContext.xml",
            "mysqlPersistenceApplicationContext.xml"
        });
        ProcesadorCargadorDatos cargador = context
                .getBean("procesadorCargadorDatosDefault", ProcesadorCargadorDatos.class);
        cargador.exec();
    }
}
