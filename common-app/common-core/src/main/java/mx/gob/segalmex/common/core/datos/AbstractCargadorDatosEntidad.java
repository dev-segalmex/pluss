package mx.gob.segalmex.common.core.datos;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import org.apache.commons.lang3.StringUtils;
import org.springframework.transaction.annotation.Transactional;

/**
 * Clase que implementa la carga de entidades a partir de un archivo.
 *
 * @param <T> el tipo de los objetos que pueden ser persistidos.
 * @author ismael
 */
@Slf4j
public abstract class AbstractCargadorDatosEntidad<T extends AbstractEntidad> implements CargadorDatos<T> {

    /**
     * El archivo de donde carga la información.
     */
    private String archivo;

    /**
     * Establece el archivo de donde carga la información.
     *
     * @param archivo el archivo de donde carga la información.
     */
    public void setArchivo(String archivo) {
        this.archivo = archivo;
    }

    @Transactional
    @Override
    public void carga() {
        long inicio = System.currentTimeMillis();
        new BufferedReader(new InputStreamReader(AbstractCargadorDatosEntidad.class.getResourceAsStream(archivo)))
                .lines()
                .filter(StringUtils::isNotBlank)
                .filter(l -> !StringUtils.startsWith(l,"#"))
                .map(l -> getInstance(l.split("\\|")))
                .filter(t -> !existe(t))
                .forEach(t -> {
                    LOGGER.info("Almacenando entidad: {}", t);
                    registra(t);
                });
        LOGGER.info("Cargando entidades en: {} ms.", System.currentTimeMillis() - inicio);
    }

}
