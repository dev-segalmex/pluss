package mx.gob.segalmex.common.core.datos.catalogos;

import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.TipoDocumento;

/**
 *
 * @author ismael
 */
public class CargadorDatosTipoDocumento extends AbstractCargadorDatosCatalogo<TipoDocumento> {

    @Override
    public TipoDocumento getInstance(String[] elementos) {
        TipoDocumento tipo = new TipoDocumento();
        tipo.setClave(elementos[0]);
        tipo.setNombre(elementos[1]);
        tipo.setActivo(true);
        tipo.setOrden(Integer.parseInt(elementos[2]));
        return tipo;
    }

}
