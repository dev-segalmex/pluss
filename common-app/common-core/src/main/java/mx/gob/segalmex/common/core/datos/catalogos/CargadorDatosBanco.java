package mx.gob.segalmex.common.core.datos.catalogos;

import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.Banco;

/**
 *
 * @author ismael
 */
public class CargadorDatosBanco extends AbstractCargadorDatosCatalogo<Banco> {

    @Override
    public Banco getInstance(String[] elementos) {
        Banco banco = new Banco();
        banco.setClave(elementos[0]);
        banco.setNombre(elementos[1]);
        banco.setActivo(true);
        banco.setOrden(Integer.parseInt(elementos[2]));
        return banco;
    }

}
