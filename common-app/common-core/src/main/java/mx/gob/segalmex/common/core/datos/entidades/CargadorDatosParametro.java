/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.datos.entidades;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosEntidad;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.pluss.modelo.catalogos.Parametro;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author cuecho
 */
public class CargadorDatosParametro extends AbstractCargadorDatosEntidad<Parametro> {

    @Autowired
    private RegistroEntidad registroEntidad;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Parametro getInstance(String[] elementos) {
        Parametro p = new Parametro();
        p.setGrupo(elementos[0]);
        p.setClave(elementos[1]);
        p.setValor(elementos[2]);
        p.setOrden(Integer.valueOf(elementos[3]));
        p.setDescripcion(elementos[4]);
        return p;
    }

    @Override
    public boolean existe(Parametro elemento) {
        try {
            entityManager.createQuery("SELECT p FROM Parametro p WHERE p.clave = :clave and p.estatusFechaBaja = :estatusBaja").
                    setParameter("clave", elemento.getClave()).
                    setParameter("estatusBaja", "--").
                    getSingleResult();
        } catch (NoResultException ouch) {
            return false;
        }
        return true;
    }

    @Override
    public void registra(Parametro elemento) {
        registroEntidad.guarda(elemento);
    }

}
