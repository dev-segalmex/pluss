package mx.gob.segalmex.common.core.datos.catalogos;

import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.seguridad.Area;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ismaelhz
 */
public class CargadorDatosArea extends AbstractCargadorDatosCatalogo<Area> {

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Override
    public Area getInstance(String[] elementos) {
        Area area = new Area();
        area.setClave(elementos[0]);
        area.setNombre(elementos[1]);
        area.setActivo(true);
        area.setOrden(Integer.parseInt(elementos[2]));
        area.setEstado(buscadorCatalogo.buscaElemento(Estado.class, elementos[3]));

        return area;
    }

}
