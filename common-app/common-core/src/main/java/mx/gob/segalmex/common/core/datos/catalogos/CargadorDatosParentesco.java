package mx.gob.segalmex.common.core.datos.catalogos;

import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.Parentesco;

/**
 *
 * @author ismael
 */
public class CargadorDatosParentesco extends AbstractCargadorDatosCatalogo<Parentesco> {

    @Override
    public Parentesco getInstance(String[] elementos) {
        Parentesco parentesco = new Parentesco();
        parentesco.setClave(elementos[0]);
        parentesco.setNombre(elementos[1]);
        parentesco.setActivo(true);
        parentesco.setOrden(Integer.parseInt(elementos[2]));

        return parentesco;
    }

}
