package mx.gob.segalmex.common.core.datos.catalogos;

import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.Pais;

/**
 *
 * @author ismael
 */
public class CargadorDatosPais extends AbstractCargadorDatosCatalogo<Pais> {

    @Override
    public Pais getInstance(String[] elementos) {
        Pais pais = new Pais();
        pais.setNombre(elementos[2]);
        pais.setClave(elementos[0]);
        pais.setActivo(true);
        pais.setOrden(0);
        return pais;
    }

}
