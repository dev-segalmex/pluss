package mx.gob.segalmex.common.core.datos.catalogos;

import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;


/**
 *
 * @author ismaelhz
 */
public class CargadorDatosEstado extends AbstractCargadorDatosCatalogo<Estado> {

    @Override
    public Estado getInstance(String[] elementos) {
        Estado estado = new Estado();
        estado.setClave(elementos[0]);
        estado.setNombre(elementos[1]);
        estado.setActivo(true);
        estado.setOrden(Integer.parseInt(elementos[2]));
        estado.setAbreviatura(elementos[3]);
        return estado;
    }

}
