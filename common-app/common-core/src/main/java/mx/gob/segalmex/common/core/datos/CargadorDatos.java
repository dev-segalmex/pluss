package mx.gob.segalmex.common.core.datos;

import mx.gob.segalmex.pluss.modelo.base.Entidad;


/**
 * Interfaz que define los métodos para realizar la carga de datos.
 *
 * @param <T> el tipo de objetos que extiende de <code>Entidad</code> que pueden ser cargados a la
 * base de datos.
 * @author ismael
 */
public interface CargadorDatos<T extends Entidad> {

    /**
     * Crea una instancia de T a partir de los elementos del arreglo.
     *
     * @param elementos los elementos con los que sea crea el arreglo.
     * @return la nueva instancia con los valores especificados en los elementos.
     */
    T getInstance(String[] elementos);

    /**
     * Método que realiza la carga de datos.
     */
    void carga();

    /**
     * Verifica si existe el elemento especificado.
     *
     * @param elemento
     * @return
     */
    boolean existe(T elemento);

    /**
     * Realiza el registro de un elemento.
     *
     * @param elemento
     */
    void registra(T elemento);

}
