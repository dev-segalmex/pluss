package mx.gob.segalmex.common.core.datos.catalogos;

import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.Estado;
import mx.gob.segalmex.pluss.modelo.catalogos.Municipio;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ismael
 */
public class CargadorDatosMunicipio extends AbstractCargadorDatosCatalogo<Municipio> {

    private Map<String, Estado> estados;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Override
    public Municipio getInstance(String[] elementos) {
        if (Objects.isNull(estados)) {
            estados = buscadorCatalogo.busca(Estado.class).stream()
                    .collect(Collectors.toMap(Estado::getClave, Function.identity()));
        }

        Municipio municipio = new Municipio();
        municipio.setActivo(true);
        municipio.setClave(elementos[0]);
        municipio.setNombre(elementos[1]);
        municipio.setOrden(100);
        municipio.setEstado(estados.get(elementos[0].substring(0, elementos[0].length() - 3)));
        return municipio;
    }

}
