package mx.gob.segalmex.common.core.datos.catalogos;

import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.seguridad.Accion;

/**
 *
 * @author ismaelhz
 */
public class CargadorDatosAccion extends AbstractCargadorDatosCatalogo<Accion> {

    @Override
    public Accion getInstance(String[] elementos) {
        Accion accion = new Accion();
        accion.setClave(elementos[0]);
        accion.setNombre(elementos[1]);
        accion.setActivo(true);
        accion.setOrden(Integer.parseInt(elementos[2]));
        accion.setTipoAccion(elementos[3]);
        accion.setSistema(elementos[4]);
        accion.setDescripcion(elementos[5]);
        return accion;
    }

}
