package mx.gob.segalmex.common.core.datos.entidades;

import javax.persistence.NoResultException;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorUsoCatalogo;
import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosEntidad;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.pluss.modelo.base.Catalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.UsoCatalogo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;

/**
 *
 * @author ismael
 */
@Slf4j
public class CargadorDatosUsoCatalogo extends AbstractCargadorDatosEntidad<UsoCatalogo> {

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private BuscadorUsoCatalogo buscadorUso;

    @Override
    public UsoCatalogo getInstance(String[] elementos) {
        UsoCatalogo uso = new UsoCatalogo();
        uso.setClase(elementos[0]);
        uso.setClave(elementos[1]);
        uso.setUso(elementos[2]);
        uso.setOrden(Integer.parseInt(elementos[3]));
        uso.setEstatusFechaBaja("--");
        return uso;
    }

    @Override
    public boolean existe(UsoCatalogo elemento) {
        Catalogo c = (Catalogo) createObject(elemento.getClase());
        LOGGER.debug("Catalogo: {}", c);
        try {
            LOGGER.debug("Uso: {}", elemento);
            buscadorUso.buscaElementoUso(c.getClass(), elemento.getClave(), elemento.getUso());
        } catch (NoResultException | EmptyResultDataAccessException ouch) {
            return false;
        }
        return true;
    }

    @Override
    public void registra(UsoCatalogo uso) {
        registroEntidad.guarda(uso);
    }

    private static Object createObject(String className) {
        Object object = null;
        try {
            Class classDefinition = Class.forName(className);
            object = classDefinition.newInstance();
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
        return object;
    }
}
