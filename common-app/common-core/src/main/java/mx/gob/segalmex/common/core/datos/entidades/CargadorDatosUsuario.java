package mx.gob.segalmex.common.core.datos.entidades;

import java.util.Calendar;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosEntidad;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.pluss.modelo.personas.Persona;
import mx.gob.segalmex.pluss.modelo.seguridad.Area;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;

/**
 *
 * @author luiz
 */
public class CargadorDatosUsuario extends AbstractCargadorDatosEntidad<Usuario> {

    @Autowired
    private RegistroEntidad registroEntidad;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Override
    public Usuario getInstance(String[] elementos) {
        Usuario usuario = new Usuario();
        usuario.setActivo(true);
        usuario.setNombre(elementos[0]);
        usuario.setClave(elementos[0]);
        usuario.setFechaActivo(Calendar.getInstance());
        usuario.setFechaCreacion(Calendar.getInstance());
        usuario.setPersona((Persona) entityManager.createQuery("SELECT p FROM Persona p WHERE p.clave = :clave ").setParameter("clave", elementos[1]).getSingleResult());
        usuario.setAreaUbicacion(buscadorCatalogo.buscaElemento(Area.class, elementos[2]));
        usuario.setPassword(elementos[3]);
        usuario.setPuesto(elementos[4]);
        return usuario;
    }

    @Override
    public boolean existe(Usuario elemento) {
        try {
            buscadorCatalogo.buscaElemento(Usuario.class, elemento.getClave());
            return true;
        } catch (EmptyResultDataAccessException ouch) {
            return false;
        }
    }

    @Override
    public void registra(Usuario usuario) {
        registroEntidad.guarda(usuario);
    }

}
