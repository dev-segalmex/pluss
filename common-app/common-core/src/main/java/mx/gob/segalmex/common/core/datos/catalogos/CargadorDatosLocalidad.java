package mx.gob.segalmex.common.core.datos.catalogos;

import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.Localidad;
import mx.gob.segalmex.pluss.modelo.catalogos.Municipio;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class CargadorDatosLocalidad extends AbstractCargadorDatosCatalogo<Localidad> {

    private Map<String, Municipio> municipios;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Override
    public Localidad getInstance(String[] elementos) {
        if (Objects.isNull(municipios)) {
            municipios = buscadorCatalogo.busca(Municipio.class).stream()
                    .collect(Collectors.toMap(Municipio::getClave, Function.identity()));
        }

        Localidad localidad = new Localidad();
        localidad.setActivo(true);
        localidad.setOrden(1000);
        localidad.setClave(elementos[0]);
        localidad.setCodigoPostal(elementos[1]);
        localidad.setNombre(elementos[2]);
        localidad.setMunicipio(municipios.get(Long.valueOf(elementos[3]).toString() + elementos[4]));
        if (Objects.isNull(localidad.getMunicipio())) {
            LOGGER.error("No existe municipio con clave: {}", Long.valueOf(elementos[3]).toString() + elementos[4]);
            throw new IllegalArgumentException("Municipio desconocido");
        }
        return localidad;
    }
}
