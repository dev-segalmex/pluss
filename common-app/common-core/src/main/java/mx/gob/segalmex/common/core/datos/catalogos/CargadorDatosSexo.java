package mx.gob.segalmex.common.core.datos.catalogos;

import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.personas.Sexo;

/**
 *
 * @author ismael
 */
public class CargadorDatosSexo extends AbstractCargadorDatosCatalogo<Sexo> {

    @Override
    public Sexo getInstance(String[] elementos) {
        Sexo sexo = new Sexo();
        sexo.setClave(elementos[0]);
        sexo.setNombre(elementos[1]);
        sexo.setActivo(true);
        sexo.setOrden(Integer.parseInt(elementos[2]));
        return sexo;
    }

}
