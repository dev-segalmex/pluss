package mx.gob.segalmex.common.core.datos.entidades;

import java.util.Calendar;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosEntidad;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.pluss.modelo.seguridad.Accion;
import mx.gob.segalmex.pluss.modelo.seguridad.Area;
import mx.gob.segalmex.pluss.modelo.seguridad.EstatusPermisoEnum;
import mx.gob.segalmex.pluss.modelo.seguridad.Permiso;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author ismael
 */
public class CargadorDatosPermiso extends AbstractCargadorDatosEntidad<Permiso> {

    @Autowired
    private RegistroEntidad registroEntidad;

    @PersistenceContext
    private EntityManager entityManager;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Override
    public Permiso getInstance(String[] elementos) {
        Permiso permiso = new Permiso();
        permiso.setEstatus(EstatusPermisoEnum.ACTIVO.getClave());
        permiso.setUsuario(buscadorCatalogo.buscaElemento(Usuario.class, elementos[0]));
        permiso.setAccion(buscadorCatalogo.buscaElemento(Accion.class, elementos[1]));
        permiso.setUsuarioAutoriza(permiso.getUsuario());
        permiso.setArea(StringUtils.equals("global", permiso.getAccion().getTipoAccion()) ? null
                : buscadorCatalogo.buscaElemento(Area.class, elementos[2]));
        permiso.setFechaAutorizacion(Calendar.getInstance());
        permiso.setFechaCreacion(Calendar.getInstance());
        permiso.setFechaActualizacion(Calendar.getInstance());
        return permiso;
    }

    @Override
    public boolean existe(Permiso elemento) {
        try {
            if (Objects.isNull(elemento.getArea())) {
                entityManager.createQuery("SELECT p FROM Permiso p WHERE p.usuario = :usuario AND p.accion = :accion AND p.area IS NULL AND p.estatus = :estatus ")
                        .setParameter("usuario", elemento.getUsuario())
                        .setParameter("estatus", "activo")
                        .setParameter("accion", elemento.getAccion()).getSingleResult();
            } else {
                entityManager.createQuery("SELECT p FROM Permiso p WHERE p.usuario = :usuario AND p.accion = :accion AND p.area = :area AND p.estatus = :estatus ")
                        .setParameter("usuario", elemento.getUsuario())
                        .setParameter("accion", elemento.getAccion())
                        .setParameter("estatus", "activo")
                        .setParameter("area", elemento.getArea()).getSingleResult();
            }
        } catch (NoResultException ouch) {
            return false;
        }
        return true;
    }

    @Override
    public void registra(Permiso permiso) {
        registroEntidad.guarda(permiso);
    }

}
