package mx.gob.segalmex.common.core.datos;

import java.util.List;

/**
 *
 * @author ismael
 */
public class ProcesadorCargadorDatos {

    private List<CargadorDatos> cargadores;

    public void setCargadores(List<CargadorDatos> cargadores) {
        this.cargadores = cargadores;
    }

    public void exec() {
        for (CargadorDatos c : cargadores) {
            c.carga();
        }
    }

}
