package mx.gob.segalmex.common.core.datos;

import java.util.Objects;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.pluss.modelo.base.AbstractCatalogo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.transaction.annotation.Transactional;

/**
 * @param <T>
 * @author ismael
 */
public abstract class AbstractCargadorDatosCatalogo<T extends AbstractCatalogo>
        extends AbstractCargadorDatosEntidad<T> {

    /**
     * El logger de la clase.
     */
    private static final Logger LOGGER
            = LoggerFactory.getLogger(AbstractCargadorDatosCatalogo.class);

    @Autowired
    private mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo buscadorCatalogo;

    @Autowired
    private RegistroEntidad registroEntidad;

    @Override
    public boolean existe(T elemento) {
        try {
            return Objects.nonNull(buscadorCatalogo.buscaElemento(elemento.getClass(), elemento.getClave()));
        } catch (EmptyResultDataAccessException ouch) {
            LOGGER.warn("El elemento de catálogo {} no existe con clave: {}",
                    elemento.getClass(), elemento.getClave());
            return false;
        }
    }

    @Transactional
    @Override
    public void registra(T elemento) {
        registroEntidad.guarda(elemento);
    }

}
