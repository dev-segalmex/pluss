package mx.gob.segalmex.common.core.datos.catalogos;

import java.math.BigDecimal;
import java.util.Map;
import java.util.Objects;
import java.util.function.Function;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.catalogos.LocalidadGeoestadistica;
import mx.gob.segalmex.pluss.modelo.catalogos.Municipio;
import org.springframework.beans.factory.annotation.Autowired;

@Slf4j
public class CargadorDatosLocalidadGeoestadistica extends AbstractCargadorDatosCatalogo<LocalidadGeoestadistica> {

    private Map<String, Municipio> municipios;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @Override
    public LocalidadGeoestadistica getInstance(String[] elementos) {
        if (Objects.isNull(municipios)) {
            municipios = buscadorCatalogo.busca(Municipio.class).stream()
                    .collect(Collectors.toMap(Municipio::getClave, Function.identity()));
        }

        String[] linea = elementos[0]
                .substring(1, elementos[0].length() - 1)
                .replace("\",\"", "|")
                .split("\\|");

        LocalidadGeoestadistica localidad = new LocalidadGeoestadistica();
        localidad.setActivo(true);
        localidad.setOrden(1000);
        localidad.setClave(linea[0]);
        localidad.setNombre(linea[7]);
        localidad.setAmbito(linea[8]);
        localidad.setLatitud(new BigDecimal(linea[11]));
        localidad.setLongitud(new BigDecimal(linea[12]));
        localidad.setAltitud(new BigDecimal(linea[13]));
        localidad.setClaveCarta(linea[14]);
        localidad.setMunicipio(municipios.get(Long.valueOf(linea[1]).toString() + linea[4]));
        if (Objects.isNull(localidad.getMunicipio())) {
            LOGGER.error("No existe municipio con clave: {}", Long.valueOf(linea[1]).toString() + linea[4]);
            throw new IllegalArgumentException("Municipio desconocido");
        }
        return localidad;
    }
}