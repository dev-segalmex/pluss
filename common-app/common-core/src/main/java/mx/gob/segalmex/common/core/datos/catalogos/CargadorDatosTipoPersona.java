/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.datos.catalogos;

import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosCatalogo;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersona;

/**
 *
 * @author cuecho
 */
public class CargadorDatosTipoPersona extends AbstractCargadorDatosCatalogo<TipoPersona> {

    @Override
    public TipoPersona getInstance(String[] elementos) {
        TipoPersona tp = new TipoPersona();
        tp.setClave(elementos[0]);
        tp.setNombre(elementos[1]);
        tp.setActivo(true);
        tp.setOrden(Integer.parseInt(elementos[2]));
        return tp;
    }

}
