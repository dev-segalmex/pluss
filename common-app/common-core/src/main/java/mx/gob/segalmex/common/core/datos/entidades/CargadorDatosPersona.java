package mx.gob.segalmex.common.core.datos.entidades;

import java.util.GregorianCalendar;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import mx.gob.segalmex.common.core.catalogos.busqueda.BuscadorCatalogo;
import mx.gob.segalmex.common.core.datos.AbstractCargadorDatosEntidad;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.pluss.modelo.catalogos.Pais;
import mx.gob.segalmex.pluss.modelo.personas.Persona;
import mx.gob.segalmex.pluss.modelo.personas.Sexo;
import mx.gob.segalmex.pluss.modelo.personas.TipoPersona;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author luiz
 */
public class CargadorDatosPersona extends AbstractCargadorDatosEntidad<Persona> {

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private BuscadorCatalogo buscadorCatalogo;

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public Persona getInstance(String[] elementos) {
        Persona persona = new Persona();
        persona.setNombre(elementos[1]);
        persona.setNombreIcao(elementos[1]);
        persona.setApellidos(elementos[0]);
        persona.setApellidosIcao(elementos[0]);
        persona.setSexo(buscadorCatalogo.buscaElemento(Sexo.class, elementos[2]));
        persona.setTipo(buscadorCatalogo.buscaElemento(TipoPersona.class, elementos[3]));
        persona.setClave(elementos[4]);
        persona.setFechaNacimiento(
                new GregorianCalendar(Integer.parseInt(elementos[5]), Integer.parseInt(elementos[6]), Integer.parseInt(elementos[7])));
        persona.setNacionalidad(buscadorCatalogo.buscaElemento(Pais.class, elementos[8]));
        return persona;
    }

    @Override
    public boolean existe(Persona elemento) {
        try {
            entityManager.createQuery("SELECT p FROM Persona p WHERE p.clave = :clave ").
                    setParameter("clave", elemento.getClave()).
                    getSingleResult();
        } catch (NoResultException ouch) {
            return false;
        }
        return true;
    }

    @Override
    public void registra(Persona persona) {
        registroEntidad.guarda(persona);
    }

}
