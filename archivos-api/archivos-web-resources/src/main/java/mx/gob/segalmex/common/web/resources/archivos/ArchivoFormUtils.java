/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.resources.archivos;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.archivos.ArchivoUtils;
import mx.gob.segalmex.common.core.exception.SegalmexRuntimeException;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;

/**
 *
 * @author ismael
 */
@Slf4j
public class ArchivoFormUtils {

    public static final String getFilename(MultipartFormDataInput dataInput, String extension) {
        List<InputPart> parts = dataInput.getFormDataMap().get("file");
        for (InputPart part : parts) {
            String[] cd = part.getHeaders().getFirst("Content-Disposition").split(";");
            for (String filename : cd) {
                if (filename.trim().startsWith("filename")) {
                    String[] name = filename.split("=");
                    String finalName = ArchivoUtils.normalizaNombre(name[1].trim().replaceAll("\"", ""));
                    LOGGER.debug("El nombre del archivo: {}", finalName);
                    return finalName;
                }
            }
        }
        LOGGER.error("No se pudo obtener el nombre del archivo, estableciendo aleatorio.");
        throw new SegalmexRuntimeException("Error al subir archivo.", "No fue posible obtener el nombre del archivo.");
    }

    public static final InputStream getInputStrem(MultipartFormDataInput dataInput) {
        try {
            List<InputPart> parts = dataInput.getFormDataMap().get("file");
            for (InputPart part : parts) {
                return part.getBody(InputStream.class, null);
            }
        } catch (IOException ouch) {
            throw new IllegalArgumentException("Error al obtener el archivo.", ouch);
        }
        throw new IllegalArgumentException("Error al obtener el archivo: no file.");
    }

}
