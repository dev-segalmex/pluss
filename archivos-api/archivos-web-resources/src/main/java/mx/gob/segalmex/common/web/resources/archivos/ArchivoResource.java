package mx.gob.segalmex.common.web.resources.archivos;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.StreamingOutput;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.archivos.BuscadorArchivo;
import mx.gob.segalmex.common.core.archivos.ManejadorArchivo;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Component;

/**
 *
 * @author ismael
 */
@Path("/archivos")
@Component
@Slf4j
public class ArchivoResource {

    @Autowired
    private ManejadorArchivo manejadorArchivo;

    @Autowired
    private BuscadorArchivo buscadorArchivo;

    @GET
    @Path("/{sistema}/{uuid}/{nombre}")
    public Response getArchivo(@DefaultValue("default") @PathParam("sistema") String sistema,
            @PathParam("uuid") String uuid, @PathParam("nombre") String nombre, @QueryParam("inline") @DefaultValue("false") boolean inline) {
        Archivo archivo = new Archivo();
        archivo.setNombre(nombre);
        archivo.setSistema(sistema);
        archivo.setUuid(uuid);

        try {
            Archivo encontrado = buscadorArchivo.busca(archivo);
            InputStream is = manejadorArchivo.obtenStream(encontrado);

            StreamingOutput so = (OutputStream out) -> {
                IOUtils.copy(is, out);
                is.close();
                out.close();
            };
            String attachment = inline ? "inline" : "attachment";

            return Response.ok(so, getMediaType(nombre))
                    .header("Content-Disposition", attachment + ";filename="
                            + encontrado.getNombreOriginal())
                    .build();
        } catch (EmptyResultDataAccessException | IOException ouch) {
            LOGGER.error("El archivo {}/{} no fue encontrado.", uuid, nombre);
            throw new WebApplicationException(ouch, 404);
        }
    }

    private String getMediaType(String filename) {
        filename = StringUtils.lowerCase(filename);
        String extension = filename.substring(filename.lastIndexOf(".") + 1);

        switch (extension) {
            case "pdf":
                return "application/pdf";
            case "txt":
                return MediaType.TEXT_PLAIN;
            case "jpeg":
            case "jpg":
                return "image/jpeg";
            case "png":
                return "image/png";
            default:
                return MediaType.APPLICATION_OCTET_STREAM;
        }
    }

}
