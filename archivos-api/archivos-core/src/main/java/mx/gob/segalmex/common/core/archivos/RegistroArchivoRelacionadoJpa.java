/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.archivos;

import java.io.InputStream;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import mx.gob.segalmex.pluss.modelo.archivos.ArchivoRelacionado;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ismael
 */
@Repository("registroArchivoRelacionado")
public class RegistroArchivoRelacionadoJpa implements RegistroArchivoRelacionado {

    @Autowired
    private ManejadorArchivo manejadorArchivo;

    @Autowired
    private RegistroEntidad registroEntidad;

    @Autowired
    private BuscadorArchivoRelacionado buscadorArchivoRelacionado;

    @Transactional
    @Override
    public void registra(Archivo archivo, ArchivoRelacionado ar, InputStream is) {
        buscadorArchivoRelacionado.busca(ar.getClase(), ar.getReferencia(), ar.getTipo()).forEach(a -> {
            a.setActivo(false);
            registroEntidad.actualiza(a);
        });
        manejadorArchivo.guarda(archivo, is);
        registroEntidad.guarda(ar);
    }

}
