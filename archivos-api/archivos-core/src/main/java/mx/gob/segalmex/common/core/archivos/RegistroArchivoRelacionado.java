/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.archivos;

import java.io.IOException;
import java.io.InputStream;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import mx.gob.segalmex.pluss.modelo.archivos.ArchivoRelacionado;

/**
 *
 * @author ismael
 */
public interface RegistroArchivoRelacionado {

    void registra(Archivo archivo, ArchivoRelacionado relacionado, InputStream is) throws IOException;
}
