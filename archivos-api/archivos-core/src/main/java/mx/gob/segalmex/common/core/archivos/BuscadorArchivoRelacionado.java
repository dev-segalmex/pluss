/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.archivos;

import java.util.List;
import mx.gob.segalmex.pluss.modelo.archivos.ArchivoRelacionado;

/**
 *
 * @author ismael
 */
public interface BuscadorArchivoRelacionado {

    List<ArchivoRelacionado> busca(Class clase, String referencia);

    List<ArchivoRelacionado> busca(String clase, String referencia, String tipo);

    ArchivoRelacionado buscaElemento(String clase, String referencia, String tipo);
}
