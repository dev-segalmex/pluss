/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.archivos;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ismael
 */
@Repository
public class BuscadorArchivoJpa implements BuscadorArchivo {

    /**
     * El entity manager de la clase.
     */
    @PersistenceContext
    private EntityManager entityManager;

    /**
     * {@inheritDoc}
     *
     * @param archivo {@inheritDoc}
     * @return {@inheritDoc}
     */
    @Override
    public Archivo busca(Archivo archivo) {
        return (Archivo) entityManager
                .createQuery("SELECT a FROM Archivo a "
                        + "WHERE a.nombre = :nombre "
                        + "AND a.uuid = :uuid "
                        + "AND a.sistema = :sistema")
                .setParameter("nombre", archivo.getNombre())
                .setParameter("uuid", archivo.getUuid())
                .setParameter("sistema", archivo.getSistema())
                .getSingleResult();
    }

}
