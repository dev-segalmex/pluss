/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.archivos;

import mx.gob.segalmex.pluss.modelo.archivos.Archivo;

/**
 *
 * @author ismael
 */
public interface BuscadorArchivo {

    /**
     * Buscar un archivo con base en el archivo que se pasa como parámetro.
     *
     * @param archivo el archivo con los parámetros de <code>sistema</code>, <code>uuid</code> y
     * <code>nombre</code>.
     * @return el archivo que coincide en <code>sistema</code>, <code>uuid</code> y
     * <code>nombre</code>.
     */
    Archivo busca(Archivo archivo);

}
