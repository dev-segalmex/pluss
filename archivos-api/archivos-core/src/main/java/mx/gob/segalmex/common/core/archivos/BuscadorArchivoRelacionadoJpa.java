/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.archivos;

import java.util.List;
import java.util.Objects;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import mx.gob.segalmex.pluss.modelo.archivos.ArchivoRelacionado;
import org.springframework.stereotype.Repository;

/**
 *
 * @author ismael
 */
@Repository("buscadorArchivoRelacionado")
public class BuscadorArchivoRelacionadoJpa implements BuscadorArchivoRelacionado {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<ArchivoRelacionado> busca(Class clase, String referencia) {
        Objects.requireNonNull(clase, "La clase no puede ser nula.");
        Objects.requireNonNull(referencia, "La referencia no puede ser nula.");
        return entityManager
                .createQuery("SELECT a FROM ArchivoRelacionado a "
                        + "WHERE "
                        + "a.clase = :clase "
                        + "AND a.referencia = :referencia "
                        + "AND a.activo = :activo "
                        + "ORDER BY a.id ", ArchivoRelacionado.class)
                .setParameter("clase", clase.getSimpleName())
                .setParameter("referencia", referencia)
                .setParameter("activo", Boolean.TRUE)
                .getResultList();
    }

    @Override
    public List<ArchivoRelacionado> busca(String clase, String referencia, String tipo) {
        Objects.requireNonNull(clase, "La clase no puede ser nula.");
        Objects.requireNonNull(referencia, "La referencia no puede ser nula.");
        Objects.requireNonNull(tipo, "El tipo no puede ser nulo.");
        return entityManager
                .createQuery("SELECT a FROM ArchivoRelacionado a "
                        + "WHERE "
                        + "a.clase = :clase "
                        + "AND a.referencia = :referencia "
                        + "AND a.activo = :activo "
                        + "AND a.tipo = :tipo "
                        + "ORDER BY a.id ", ArchivoRelacionado.class)
                .setParameter("clase", clase)
                .setParameter("referencia", referencia)
                .setParameter("activo", Boolean.TRUE)
                .setParameter("tipo", tipo)
                .getResultList();
    }

    @Override
    public ArchivoRelacionado buscaElemento(String clase, String referencia, String tipo) {
        Objects.requireNonNull(clase, "La clase no puede ser nula.");
        Objects.requireNonNull(referencia, "La referencia no puede ser nula.");
        Objects.requireNonNull(tipo, "El tipo no puede ser nulo.");
        return entityManager
                .createQuery("SELECT a FROM ArchivoRelacionado a "
                        + "WHERE "
                        + "a.clase = :clase "
                        + "AND a.referencia = :referencia "
                        + "AND a.activo = :activo "
                        + "AND a.tipo = :tipo "
                        + "ORDER BY a.id ", ArchivoRelacionado.class)
                .setParameter("clase", clase)
                .setParameter("referencia", referencia)
                .setParameter("activo", Boolean.TRUE)
                .setParameter("tipo", tipo)
                .getSingleResult();
    }
}
