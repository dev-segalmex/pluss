/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.archivos;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.List;
import java.util.Objects;
import java.util.UUID;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.common.core.persistencia.RegistroEntidad;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ismael
 */
@Repository("manejadorArchivo")
@Slf4j
public class ManejadorArchivoJpa implements ManejadorArchivo {

    @Autowired
    private RegistroEntidad registroEntidad;

    /**
     * La ruta base para buscar archivos.
     */
    private String base;

    /**
     * Establece la ruta base para buscar archivos.
     *
     * @param base la ruta base para buscar archivos.
     */
    public void setBase(String base) {
        this.base = base;
    }

    /**
     * {@inheritDoc}
     *
     * @param archivo {@inheritDoc}
     * @return {@inheritDoc}
     */
    @Override
    public byte[] obten(Archivo archivo) throws IOException {
        try {
            String ruta = getRuta(archivo, false);

            return FileUtils.readFileToByteArray(new File(ruta));
        } catch (IOException ouch) {
            LOGGER.error("Error al tratar de leer el archivo.", ouch);
            throw ouch;
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param archivo {@inheritDoc}
     * @return {@inheritDoc}
     * @throws IOException {@inheritDoc}
     */
    @Override
    public InputStream obtenStream(Archivo archivo) throws IOException {
        String ruta = getRuta(archivo, false);
        return new FileInputStream(new File(ruta));
    }

    /**
     * {@inheritDoc}
     *
     * @param archivo {@inheritDoc}
     * @param data {@inheritDoc}
     * @return {@inheritDoc}
     */
    @Transactional
    @Override
    public Archivo actualiza(Archivo archivo, byte[] data) {
        try {
            String ruta = getRuta(archivo, false);

            String timestamp = String.valueOf(Calendar.getInstance().getTimeInMillis());

            FileUtils.moveFile(new File(ruta), new File(ruta + "." + timestamp));
            FileUtils.writeByteArrayToFile(new File(ruta), data);

            registroEntidad.actualiza(archivo);
            return archivo;
        } catch (IOException ouch) {
            LOGGER.error("Error al actualizar el archivo.", ouch);
            throw new IllegalStateException(ouch);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param archivo {@inheritDoc}
     */
    @Transactional
    @Override
    public void elimina(Archivo archivo) {
        try {
            LOGGER.debug("Marcando como eliminado el archivo con id: {}.", archivo.getId());

            String ruta = getRuta(archivo, false);
            String timestamp = String.valueOf(Calendar.getInstance().getTimeInMillis());
            FileUtils.moveFile(new File(ruta), new File(ruta + "." + timestamp + ".deleted"));

            archivo.setActivo(false);
            registroEntidad.actualiza(archivo);
        } catch (IOException ouch) {
            LOGGER.error("Error al eliminar el archivo.", ouch);
            throw new IllegalStateException(ouch);
        }
    }

    /**
     * {@inheritDoc}
     *
     * @param archivo {@inheritDoc}
     * @param data {@inheritDoc}
     * @return {@inheritDoc}
     */
    @Transactional
    @Override
    public Archivo guarda(Archivo archivo, byte[] data) {
        try {
            guarda(archivo);
            FileUtils.writeByteArrayToFile(new File(getRuta(archivo, false)), data);

            registroEntidad.guarda(archivo);
            return archivo;
        } catch (IOException ouch) {
            LOGGER.error("Error al guardar el archivo.", ouch);
            throw new RuntimeException(ouch);
        }
    }

    @Transactional
    @Override
    public Archivo guarda(Archivo archivo, InputStream input) {
        try {
            guarda(archivo);
            FileUtils.copyInputStreamToFile(input, new File(getRuta(archivo, false)));
            registroEntidad.guarda(archivo);
            return archivo;
        } catch (IOException ouch) {
            LOGGER.error("Error al guardar el archivo.", ouch);
            throw new RuntimeException(ouch);
        }
    }



    @Override
    public Archivo guarda(Archivo archivo, List<StringBuilder> data) {
        try {
            guarda(archivo);
            FileUtils.writeLines(new File(getRuta(archivo, false)), data);

            registroEntidad.guarda(archivo);
            return archivo;
        } catch (IOException ouch) {
            LOGGER.error("Error al guardar el archivo.", ouch);
            throw new RuntimeException(ouch);
        }
    }

    private void guarda(Archivo archivo) throws IOException {
        archivo.setActivo(true);
        archivo.setUuid(UUID.randomUUID().toString());
        String nombre = ArchivoUtils.normalizaNombre(archivo.getNombre());

        archivo.setNombre(System.currentTimeMillis() + "_" + nombre);

        String rutaDirectorio = getRuta(archivo, true);
        FileUtils.forceMkdir(new File(rutaDirectorio));
    }

    /**
     * Genera la ruta en el sistema de archivos.
     *
     * @param archivo el archivo de donde se obtiene la información.
     * @param directorio indica si únicamente es el directorio o incluye el nombre del archivo.
     * @return la cadena con la ruta del archivo o el directorio únicamente.
     */
    private String getRuta(Archivo archivo, boolean directorio) {
        Objects.requireNonNull(base, "Se requiere definir una base de archivos.");
        return base
                + archivo.getSistema()
                + (archivo.getSubdirectorio().equals("/") ? "" : "/" + archivo.getSubdirectorio())
                + "/"
                + (directorio ? "" : archivo.getNombre());
    }

}
