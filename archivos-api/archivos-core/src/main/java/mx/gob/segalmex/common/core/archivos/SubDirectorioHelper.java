package mx.gob.segalmex.common.core.archivos;

import java.util.Objects;
import org.apache.commons.lang3.StringUtils;

/**
 * Calse utilitaria que ayudara a construir el subdirectorio donde se guardaran los archivos con
 * folios de 13 dígitos
 *
 * @author luiz
 */
public class SubDirectorioHelper {

    /**
     * Tamaño para el formato
     */
    private static final Integer TAMANO_FORMATO = 13;
    /**
     * Tamaño para el subdirectorio
     */
    private static final Integer TAMANO_SUBDIRECTORIO = 10;

    /**
     * Obtiene la ruta de un subsirectorio a partir del folio
     *
     * @param folio folio de 13 digitos
     * @return
     */
    public static final String getSubdirectorio(String folio) {
        Objects.requireNonNull(StringUtils.trimToNull(folio), "El folio no puede ser nulo");

        // Le damos el formato de 13 dígitos
        folio = StringUtils.leftPad(folio, TAMANO_FORMATO, "0");

        StringBuilder buffer = new StringBuilder("/");
        buffer.append(folio.substring(0, 8));
        buffer.append("XXXXX/");
        buffer.append(folio.substring(0, TAMANO_SUBDIRECTORIO));
        buffer.append("XXX/");
        buffer.append(folio);

        return buffer.toString();
    }

    /**
     * Genera el subdirectorio a partir del padre e hijo y el número de folio.
     *
     * @param padre la raiz de los directorios (ciclo).
     * @param hijo  el tipo de los directorios
     * @param folio el número de folio del registro
     * @return el subdirectorio donde se deben guardar los archivos adjuntos
     */
    public static final String getSubdirectorio(String padre, String hijo, String folio) {
        StringBuilder subdir = new StringBuilder("/");
        subdir.append(padre);
        subdir.append("/");
        subdir.append(hijo);
        subdir.append(getSubdirectorio(folio));

        return subdir.toString();
    }
}
