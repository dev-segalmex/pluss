/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.core.archivos;

import java.util.Arrays;
import java.util.List;
import java.util.Objects;
import lombok.extern.slf4j.Slf4j;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author ismael
 */
@Slf4j
public final class ArchivoUtils {

    private static final String BASE = "/";

    /**
     * El alfabeto de caracteres permitidos en los nombres de los archivos.
     */
    private static final String[] ALFABETO = {
        "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
        "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z",
        "0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
        "-", "_", "."
    };

    /**
     * Constructor de la clase
     */
    private ArchivoUtils() {
    }

    public static Archivo getInstance(String nombre, String sistema, String subdirectorio, boolean temporal) {
        Objects.requireNonNull(StringUtils.trimToNull(nombre), "El nombre no puede ser nulo.");
        Objects.requireNonNull(StringUtils.trimToNull(sistema), "El sistema no puede ser nulo.");

        Archivo archivo = new Archivo();
        archivo.setActivo(true);
        archivo.setSistema(sistema);
        archivo.setSubdirectorio(StringUtils.defaultIfEmpty(subdirectorio, BASE));

        nombre = StringUtils.replace(nombre, "\\", "/");
        String[] palabras = StringUtils.split(nombre, "/");
        nombre = palabras[palabras.length - 1];
        nombre = StringUtils.replace(nombre, " ", "_");
        nombre = StringUtils.replace(nombre, ":", "");
        archivo.setNombreOriginal(normalizaNombre(nombre));
        archivo.setNombre(StringUtils.lowerCase(archivo.getNombreOriginal()));

        if (temporal) {
            archivo.setTemporal(Boolean.TRUE);
        }

        return archivo;
    }

    /**
     * Obtiene la URL desde donde se puede descargar un archivo desde el recurso de archivos:
     * /<code>sistema</code>/<code>uuid</code>/<code>nombre</code>.
     *
     * @param archivo el archivo del que se desea obtener la URL.
     * @return la url del archivo.
     */
    public static String getUrl(Archivo archivo) {
        StringBuilder buffer = new StringBuilder("/");
        buffer.append(archivo.getSistema());
        buffer.append("/");
        buffer.append(archivo.getUuid());
        buffer.append("/");
        buffer.append(archivo.getNombre());

        return buffer.toString();
    }

    /**
     * Normaliza un nombre de archivo convirtiéndolo a minúsculas, sustituyendo espacios por guiones
     * bajos y eliminando caracteres especiales no incluidos en <code>ALFABETO</code>.
     *
     * @param nombre el nombre a normalizar.
     * @return el nombre normalizado.
     */
    public static String normalizaNombre(String nombre) {
        LOGGER.debug("Normalizando nombre: {}", nombre);
        nombre = StringUtils.stripToNull(nombre);
        Objects.requireNonNull(nombre, "El nombre del archivo no puede ser nulo.");
        nombre = StringUtils.replace(nombre, " ", "_");

        StringBuilder sb = new StringBuilder();
        List<String> alfabeto = Arrays.asList(ALFABETO);
        for (int i = 0; i < nombre.length(); i++) {
            char c = nombre.charAt(i);
            if (alfabeto.contains(String.valueOf(c))) {
                sb.append(c);
            }
        }

        return sb.toString();
    }

}
