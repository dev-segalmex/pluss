package mx.gob.segalmex.common.core.archivos;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;

/**
 *
 * @author ismael
 */
public interface ManejadorArchivo {

    /**
     * Obtiene el arreglo de bytes con la información asociada al archivo.
     *
     * @param archivo la información para obtener el archivo.
     * @return el arreglo de bytes con el archivo.
     */
    byte[] obten(Archivo archivo) throws IOException;

    /**
     * Obtiene un <code>InputStream</code> con la representación binaria del archivo.
     *
     * @param archivo contiene la información para obtener el archivo.
     * @return el <code>InputStream</code> con la representación binaria del archivo.
     * @throws IOException si ocurre un error al obtener el archivo.
     */
    InputStream obtenStream(Archivo archivo) throws IOException;

    /**
     * Guarda un nuevo archivo.
     *
     * @param archivo la información del archivo a guardar.
     * @param data el arreglo de bytes con la información.
     * @return el archivo almacenado.
     */
    Archivo guarda(Archivo archivo, byte[] data);

    Archivo guarda(Archivo archivo, InputStream input);

    /**
     * Guarda un nuevo archivo.
     *
     * @param archivo la información del archivo a guardar.
     * @param data la lista de contenido para el archivo.
     * @return el archivo almacenado.
     */
    Archivo guarda(Archivo archivo, List<StringBuilder> data);

    /**
     * Actualiza la información del archivo.
     *
     * @param archivo el archivo a actualizar.
     * @param data los nuevos datos del archivo.
     * @return el archivo actualizado.
     */
    Archivo actualiza(Archivo archivo, byte[] data);

    /**
     * Elimina un archivo.
     *
     * @param archivo el archivo a eliminar.
     */
    void elimina(Archivo archivo);

}
