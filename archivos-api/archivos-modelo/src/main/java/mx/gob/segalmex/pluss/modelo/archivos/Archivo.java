/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.archivos;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import org.apache.commons.lang3.builder.ToStringBuilder;

/**
 * Clase que representa un archivo almacenado en el sistema de archivos. La ubicación real se
 * construye a partir de la ruta base de archivos, el sistema asociado, el subdirectorio y el nombre
 * del archivo.
 *
 * @author ismael
 */
@Entity
@Table(name = "archivo")
@Getter
@Setter
public class Archivo extends AbstractEntidad {

    /**
     * El nombre del archivo en el sistema de archivos. Este nombre debería estar normalizado, es
     * decir sin espacios blancos ni caractéres especiales.
     */
    @Column(name = "nombre", nullable = false)
    private String nombre;

    /**
     * El nombre original con el que subieron el archivo, sin diagonales. Este nombre se puede usar
     * para cuestiones de presentación.
     */
    @Column(name = "nombre_original", nullable = false)
    private String nombreOriginal;

    /**
     * Indica si el archivo se encuentra activo.
     */
    @Column(name = "activo")
    private boolean activo;

    /**
     * El uuid del archivo mediante el cual los clientes externos acceden al archivo en conjunto con
     * el nombre.
     */
    @Column(name = "uuid", nullable = false)
    private String uuid;

    /**
     * El sistema al que pertenece este archivo.
     */
    @Column(name = "sistema", nullable = false)
    private String sistema;

    /**
     * <p>
     * El subdirectorio en el que se encuentra ubicado el archivo a partir del directorio del
     * sistema. Esta propiedad es relativa a <code>ruta-general-base</code>/<code>>sistema</code>.
     * La ruta deberá guardarse con <code>/</code> al inicio, para permitir que se creen archivos en
     * la raíz del directorio <code>sistema</code>.</p>
     *
     * <p>
     * Los subdirectorios deberán ser definidos por la lógica de cada sistema.</p>
     */
    @Column(name = "subdirectorio", nullable = false)
    private String subdirectorio;

    /**
     * La descripción opcional del archivo.
     */
    @Column(name = "descripcion")
    private String descripcion;

    /**
     * Indica si el archivo es de índole temporal.
     */
    @Column(name = "temporal")
    private boolean temporal;

    @Transient
    private String tipo;

    /**
     * Campo a utilizar para saber si se cargo o no el archivo.
     */
    @Transient
    private boolean cargado;

    @Override
    public String toString() {
        return new ToStringBuilder(this)
                .append("nombre", nombre)
                .append("subdirectorio", subdirectorio)
                .append("uuid", uuid)
                .append("activo", activo)
                .toString();
    }

}
