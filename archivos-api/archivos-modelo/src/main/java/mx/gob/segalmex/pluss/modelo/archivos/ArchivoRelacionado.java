/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.pluss.modelo.archivos;

import java.util.Calendar;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import lombok.Getter;
import lombok.Setter;
import mx.gob.segalmex.pluss.modelo.base.AbstractEntidad;
import mx.gob.segalmex.pluss.modelo.seguridad.Usuario;

/**
 *
 * @author ismael
 */
@Entity
@Table(name = "archivo_relacionado")
@Getter
@Setter
public class ArchivoRelacionado extends AbstractEntidad {

    /**
     * La clase a la que pertence la evidencia.
     */
    @Column(name = "clase", nullable = false, length = 63)
    private String clase;

    /**
     * La referencia a la que pertenece la evidencia.
     */
    @Column(name = "referencia", length = 63)
    private String referencia;

    /**
     * La información del archivo.
     */
    @ManyToOne
    @JoinColumn(name = "archivo_id", nullable = false)
    private Archivo archivo;

    /**
     * El tipo de evidencia.
     */
    @Column(name = "tipo", nullable = false, length = 63)
    private String tipo;

    /**
     * El usuario que registró el archivo.
     */
    @ManyToOne
    @JoinColumn(name = "usuario_registra_id")
    private Usuario usuarioRegistra;

    /**
     * El usuario que elimina el archivo, en caso de que aplique.
     */
    @ManyToOne
    @JoinColumn(name = "usuario_elimina_id")
    private Usuario usuarioElimina;

    /**
     * La fecha de eliminación de la evidencia.
     */
    @Column(name = "fecha_eliminado")
    @Temporal(TemporalType.TIMESTAMP)
    private Calendar fechaEliminado;

    /**
     * Indica si la evidencia se encuentra activa o no.
     */
    @Column(name = "activo", nullable = false)
    private boolean activo = true;

}
