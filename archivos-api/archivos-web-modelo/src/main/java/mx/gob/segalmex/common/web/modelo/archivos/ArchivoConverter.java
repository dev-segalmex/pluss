/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mx.gob.segalmex.common.web.modelo.archivos;

import java.util.Objects;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import mx.gob.segalmex.pluss.modelo.archivos.Archivo;
import mx.gob.segalmex.common.web.modelo.base.AbstractEntidadConverter;

/**
 *
 * @author oscar
 */
@XmlRootElement(name = "archivo")
public class ArchivoConverter extends AbstractEntidadConverter {

    /**
     * La {@link Archivo} a representar en este converter.
     */
    private final Archivo entity;

    public ArchivoConverter() {
        entity = new Archivo();
        expandLevel = 1;
    }

    public ArchivoConverter(Archivo entity, int expandLevel) {
        Objects.requireNonNull(entity, "La entidad Archivo no puede ser nula.");
        this.entity = entity;
        this.expandLevel = expandLevel;
    }

    /**
     * Recupera la entidad representada.
     *
     * @return
     */
    @Override
    public Archivo getEntity() {
        return this.entity;
    }

    public String getNombre() {
        return getEntity().getNombre();
    }

    /**
     *
     * @param nombre
     */
    public void setNombre(String nombre) {
        getEntity().setNombre(nombre);
    }

    /**
     *
     * @return
     */
    public String getNombreOriginal() {
        return getEntity().getNombreOriginal();
    }

    /**
     *
     * @param nombreOriginal
     */
    public void setNombreOriginal(String nombreOriginal) {
        getEntity().setNombreOriginal(nombreOriginal);
    }

    /**
     *
     * @return
     */
    public boolean isActivo() {
        return getEntity().isActivo();
    }

    /**
     *
     * @param activo
     */
    public void setActivo(boolean activo) {
        getEntity().setActivo(activo);
    }

    /**
     *
     * @return
     */
    public String getUuid() {
        return getEntity().getUuid();
    }

    /**
     *
     * @param uuid
     */
    public void setUuid(String uuid) {
        getEntity().setUuid(uuid);
    }

    public String getSistema() {
        return getEntity().getSistema();
    }

    public void setSistema(String sistema) {
        getEntity().setSistema(sistema);
    }

    /**
     *
     * @return
     */
    public String getSubdirectorio() {
        return getEntity().getSubdirectorio();
    }

    /**
     *
     * @param subdirectorio
     */
    public void setSubdirectorio(String subdirectorio) {
        getEntity().setSubdirectorio(subdirectorio);
    }

    @XmlElement
    public String getTipo() {
        return entity.getTipo();
    }

    @XmlElement
    public boolean isCargado() {
        return entity.isCargado();
    }

}
